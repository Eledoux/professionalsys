#<ImportModules>
import collections
import copy
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class InitiaterClass(object):
	
	def __init__(self):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""

		#<DefineSpecificDict>
		self.IsInitiatingBool=True
		#</DefineSpecificDict>

		#Hook methods (integrativ loop)
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="init"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsInitiatingBool==False:
						return 

					#Call the HookingMethod
					HookingMethod(self)

#</DefineClass>

