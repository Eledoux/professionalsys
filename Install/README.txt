

    #/*##################################/
    # SysModule, this is the first Module
    # to be installed for using the Sys
    # framework. This little piece of code
    # just helps here to relocalize every
    # customized written Modules in the
    # <%Thing%>Sys (placed anywhere
    # by the user) to be getted easily by the
    # deep-installed SysModule in
    # the Python lib site-packages
    #/##################################*/

#Install the SysModule

On a Terminal :

$pip install SysModule

#Uninstall the SysModule

On a Terminal :

$pip uninstall SysModule
