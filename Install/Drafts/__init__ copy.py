######################################################################
#Define Python modules

import copy
import importlib
import inspect
import json
import itertools
import multiprocessing
import numpy
import operator
import os
import re
import sys

######################################################################
#Define functions that build the SYS framework
def _map(_Function,_List):

	#Mapping with multiprocessing or not
	if SYS.IsMultiprocessingBool:
		if SYS.Module.Pool==None:
			SYS.Module.Pool=multiprocessing.Pool(processes=SYS.ProcessesMaxInt)
		return SYS.Module.Pool.map(_Function,_List)

	#Else return map default
	return map(_Function,_List)

def getLocalFolderPathString():
	'''
		<Help>
			Get the Name of the actual Script that is readed by Python
		</Help>
	
		<Test>
			#Print the LocalFolderPathString
			print(SYS.getLocalFolderPathString())
		</Test>
	'''
	#return '/'.join(inspect.currentframe().f_back.f_code.co_filename.split('/')[:-1])+"/"
	return '/'.join(__file__.split('/')[:-1])+"/"

def getWordStringsListWithString(_String):
	'''
		<Help>
			Split a String into its Words
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS

			#Print some examples
			print("The Words in the String SuperMan are :"+str(SYS.getWordStringsListWithString("SuperMan")));
			print("The Words in the String \"FeelLike-A-RainBow\" are :"+str(SYS.getWordStringsListWithString("FeelLike-A-RainBow")));
			print("The Words in the String \"MySYSHopeItsWorkingHELLO\" are :"+str(SYS.getWordStringsListWithString("MySYSHopeItsWorkingHELLO")));
		</Test>	
	'''
	
	#Get all the Strings with an upper char at the first index
	if type(_String) in [str,unicode]:
		StringsList=filter(None,sys.modules['re'].split("([A-Z][^A-Z]*)",_String));

		#Join the abreviated Strings
		if len(StringsList)>1:

			WordsInt=0
			PreviousString=StringsList[WordsInt]
			while WordsInt<len(StringsList)-1:
			
				#Increment the Word
				WordsInt+=1
				
				#Define the Next String
				NextString=StringsList[WordsInt]
			
				#Check for Upper Abreviations
				if len(NextString)==1:
					if (all(map(lambda Char:Char.upper()==Char,list(PreviousString)))) and (NextString.upper()==NextString):
					
						#Join Previous and NextString
						StringsList[WordsInt-1]=PreviousString+NextString

						#Remove the NextString
						StringsList.pop(WordsInt)
				
						#Increment
						WordsInt-=1

				#Shift the Previous String
				if WordsInt<len(StringsList):
					PreviousString=StringsList[WordsInt]

		#Return StringsList
		return StringsList

	#Return [] by default
	else:
		return [""]

def getMySYSFolderPathString():
	'''
		<Help>
			Get the SYSFolderPathString from the os cwd
		</Help>
	
		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Print the SYSFolderPathString
			print("The SYSFolder is at : "+SYS.getSYSFolderPathString())
		</Test>
	'''
	
	#Get the cwd
	CurrentWorkDirectoryPathString=sys.modules['os'].getcwd()
	
	#Get the FolderStringsList
	FolderStringsList=CurrentWorkDirectoryPathString.split('/')
	
	#Init SYSFolderPathString
	SYSFolderPathString=""
	
	#Scan each FolderPathString and find the one with "SYS" in the end
	for FolderString in FolderStringsList:
		SYSFolderPathString+=FolderString+"/";
		WordStringsList=getWordStringsListWithString(FolderString)
		if len(WordStringsList)>0:
			if WordStringsList[-1]=="SYS":
				break;

	#Return
	return SYSFolderPathString

def writeUpdatedDictWithUpdatingDict(_DictPathString,_Dict,_UpdatingDict):

	#Update the AttributeStringToModuleStringDict
	IsUpdatedBool=False
	for KeyString,ValueVariable in _UpdatingDict.items():

		if KeyString not in _Dict:

			#Look if it is a new Key
			IsUpdatedBool=True
			_Dict[KeyString]=ValueVariable

		elif _Dict[KeyString]!=ValueVariable:
			
			#Look if it is an updated Key
			IsUpdatedBool=True
			_Dict[KeyString]=ValueVariable

	#Rewrite the FunctionStringToTypeStringDict if there was at least one change
	if IsUpdatedBool==True:
		DictFile=open(_DictPathString,'w')
		json.dump(_Dict,DictFile,indent=2,sort_keys=True)
		DictFile.close()

def writePreVariableStringToPostVariableStringDict(_PreVariableString,_KeyStringsList,_PostVariableString,_ValueString):

	#Define the DictString
	DictTypeString=_PreVariableString+"To"+_PostVariableString+"Dict";

	#Define the DictPathString
	DictPathTypeString=DictTypeString+"PathString"

	#Set the DictFolderPathString
	setattr(SYS,DictPathTypeString,SYS.ModulesFolderPathString+DictTypeString+".json")

	#Init an empty json if there is not one
	if sys.modules['os'].path.isfile(getattr(SYS,DictPathTypeString))==False:

		#Define an default empty Dict
		setattr(SYS,DictTypeString,{})

		#Write a Default empty json dict
		sys.modules['json'].dump(getattr(SYS,DictTypeString),open(getattr(SYS,DictPathTypeString),'w'))
	
	#Get the already one
	setattr(
				SYS,
				DictTypeString,
				sys.modules['json'].load(open(getattr(SYS,DictPathTypeString),'r'))
			)

	#Update the actual setted Attributes
	SYS.writeUpdatedDictWithUpdatingDict(
										getattr(SYS,DictPathTypeString),
										getattr(SYS,DictTypeString),
										dict(
											map(
												lambda KeyString:(KeyString,_ValueString),
												_KeyStringsList
												)
											)
									)

def getAttributeStringsListWithModule(_Module):
	'''
		<Help>
			Get the Name of the Attributes of a Module
		</Help>

		<Test>
			#Print the AttirbuteStrings of the Sys
			print("The AttributeStrings in the module Sys are : ");
			print(Sys.getAttributeStringsListWithModule(Sys));
		</Test>
	'''
	
	return map(lambda Item: Item[1].__name__ if hasattr(Item[1],'__name__') else Item[0],filter(lambda Item:type(Item[1]).__name__=="function" or Item[0][0].upper()==Item[0][0],_Module.__dict__.items()))

def getTypeStringWithFolderPathString(_FolderPathString):
	'''
		<Help>
			Get the ScriptStringAndScriptPathString from a FolderPathString
			FolderPathString has to present the TypeString in its name after the last "_"
		</Help>

		<Test>
		</Test>
	'''

	#Get the TypeString from the FolderPathStringsList
	FolderPathStringsList=_FolderPathString.split("/")[-2].split("_")
	if len(FolderPathStringsList)>1:
		TypeString="_".join(FolderPathStringsList[1:])
	else:
		TypeString=_FolderPathString.split("/")[-2]
	#Look for the ScriptPathString
	ScriptString=TypeString+".py"
	ScriptPathString=_FolderPathString+ScriptString
	if os.path.isfile(ScriptPathString):
		return TypeString

def getClassStringWithTypeString(_TypeString):
	'''
		<Help>
			Get the ClassString With the TypeString
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Print the ClassString of the Node Object
			print("ClassString of the Node Object is :");
			print(SYS.getClassStringWithTypeString("Node"));
		</Test>
	'''

	#Return 
	return _TypeString+"Class"

def getTypeStringWithClassString(_TypeString):
	'''
		<Help>
			Get the TypeString With the ClassString
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Print the ClassString of the Node Object
			print("TypeString of the Node ClasString is :");
			print(SYS.getTypeStringWithClassString(SYS.NodeClass.__name__));
		</Test>
	'''

	#Return 
	return ''.join(SYS.getWordStringsListWithString(_TypeString)[:-1])

def getLocalDictWithKwargsDictAndDefaultItemsDict(_KwargsDict,_DefaultItemsDict):
	'''
		<Help>
			Filter a Kwargs dict with Default possible Items
		</Help>
	
		<Test>
			#Define a Kwargs
			Kwargs={'Thing':0,'Stuff':1}
			#
			#Define a DefaultItemsDict
			DefaultItemsDict={'Thing':None,'OtherStuff':0}
			#
			#Get the LocalDict
			print(_.getLocalDictWithKwargsAndDefaultItemsDict(Kwargs,DefaultItemsDict))
		</Test>
	'''
	
	#Init skeleton
	LocalDict={}
	
	#Accumulate Kwargs
	for Key,DefaultValue in _DefaultItemsDict.items():
		LocalDict[Key]=_Kwargs[Key] if _KwargsDict.has_key(Key) else DefaultValue
	
	#Return
	return LocalDict

def getChildStringsListWithParentStringToChildStringsListDictAndParentString(_ParentStringToChildStringsListDict,_ParentString,**_KwargsDict):
	'''
		<Help>
			Get all the repeated BaseNames but aligned...... in one dimension
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Invent Fictif relationships
			SYS.DictionnaryFirstBaseTypeStringsList=["SYS"];
			SYS.ParentFirstBaseTypeStringsList=["Dictionnary"];
			SYS.DirFirstBaseTypeStringsList=["SYS"];
			SYS.ScriptFirstBaseTypeStringsList=["Dir","Parent"];
			#
			#Print the BasesList of the Script Object
			print("BaseTypeStringsList of the Script is : "+str(SYS.getBaseTypeStringsListWithString("Script")));
		</Test>
	'''
	
	#Set possible default local Items
	LocalDict=SYS.getLocalDictWithKwargsDictAndDefaultItemsDict(_KwargsDict,
				{
					'ChildStringsList':[]
				 }
				)
	
	#Look if there is this Key in this layer
	if _ParentString in _ParentStringToChildStringsListDict:
		
		#Add to the 'ChildStringsList'
		LocalDict['ChildStringsList']+=_ParentStringToChildStringsListDict[_ParentString]
	
		#Look to younger Children
		for ChildStringInt,ChildString in enumerate(_ParentStringToChildStringsListDict[_ParentString]):
			
			#If this Children have also Children
			if ChildString in _ParentStringToChildStringsListDict:
			
				#Recursive Call
				LocalDict['ChildStringsList']+=getChildStringsListWithParentStringToChildStringsListDictAndParentString(_ParentStringToChildStringsListDict,ChildString)

	#Return the cumulated BaseNames
	return LocalDict['ChildStringsList']

def getChildStringsSetListWithParentStringToChildStringsListDictAndStringAndParentString(_ParentStringToChildStringsListDict,_ParentString):
	'''
		<Help>
			Get the set of Bases from the list
		</Help>

		</Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Invent Fictif relationships
			SYS.DictionnaryFirstBaseTypeStringsList=["SYS"];
			SYS.ParentFirstBaseTypeStringsList=["Dictionnary"];
			SYS.DirFirstBaseTypeStringsList=["SYS"];
			SYS.ScriptFirstBaseTypeStringsList=["Dir","Parent"];
			#
			#Print the BaseTypeStringsOrderedSet of the Script Object
			print("BaseTypeStringsOrderedSet of the Script is : "+str(SYS.getBaseTypeStringsOrderedSetWithString("Script")));
		</Test>
	'''
	
	#Get the BaseNamesList
	ChildStringsList=getChildStringsListWithParentStringToChildStringsListDictAndParentString(_ParentStringToChildStringsListDict,_ParentString);
	
	#Init the BasesOrderSet
	ChildStringsSetList=[]
	
	#Scan among the Children
	for ChildString in ChildStringsList:
		if ChildString in ChildStringsSetList:
		
			#Get the Idx
			ChildInt=ChildStringsSetList.index(ChildString)
			
			#Delete at this Idx
			ChildStringsSetList.pop(ChildInt)
			
		#Put it in last
		ChildStringsSetList.append(ChildString)

	#Return
	return ChildStringsSetList

def getChildStringsDictWithParentStringToChildStringsListDictAndParentString(_ParentStringToChildStringsListDict,_ParentString,**_KwargsDict):
	'''
		<Help>
			Get all the Children with a Dict hierarchy
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Print the ParentTypeStringToChildTypeStringsListDict
			print("ParentTypeStringToChildTypeStringsListDict is :");
			print(SYS.ParentTypeStringToChildTypeStringsListDict);
			#
			#Print the Listed Children
			print("\nListed Children are : ");
			print(SYS.getChildStringsListWithParentStringToChildStringsListDictAndString(SYS.ParentTypeStringToChildTypeStringsListDict,'Variable'));
		</Test>
	'''
	
	#Set possible default local Items
	LocalDict=SYS.getLocalDictWithKwargsDictAndDefaultItemsDict(_KwargsDict,
				{
					'ChildStringsDict':[],
				 }
				)
	
	#Look if there is this Key in this layer
	if _ParentString in _ParentStringToChildStringsListDict:
		
		#Add to the 'ChildStringsList'
		LocalDict['ChildStringsDict']+=_ParentStringToChildStringsListDict[_ParentString]
	
		#Look to younger Children
		for ChildStringInt,ChildString in enumerate(_ParentStringToChildStringsListDict[_ParentString]):
			
			#If this Children have also Children
			if ChildString in _ParentStringToChildStringsListDict:
			
				#Recursive Call
				LocalDict['ChildStringsDict'][ChildStringInt]=dict([(ChildString,getChildStringsDictWithParentStringToChildStringsListDictAndParentString(_ParentStringToChildStringsListDict,ChildString))])

	#Return the cumulated BaseNames
	return LocalDict['ChildStringsDict']

def getFunctionsListWithModule(_Module):
	'''
		<Help>
			Get the Method of a module
		</Help>
	
		<Test>
			#Print the Attirbutes of the Sys
			print("The Functions in the module _ are : ");
			print(Sys.getFunctionsListWithModule(Sys));
		</Test>
	'''
	return filter(lambda Value:type(Value).__name__=="function",_Module.__dict__.values())

def getAttributeStringsListWithModule(_Module):
	'''
		<Help>
			Get the Name of the Attributes of a Module
		</Help>

		<Test>
			#Print the AttirbuteStrings of the Sys
			print("The AttributeStrings in the module Sys are : ");
			print(Sys.getAttributeStringsListWithModule(Sys));
		</Test>
	'''
	
	return map(lambda Item: Item[1].__name__ if hasattr(Item[1],'__name__') else Item[0],filter(lambda Item:type(Item[1]).__name__=="function" or Item[0][0].upper()==Item[0][0],_Module.__dict__.items()))

def importModulesWithTypeString(_TypeString):

	#Load the module
	importlib.import_module(_TypeString)
	
	#do a direct Link of <TypeString> Module to the SYS
	setattr(SYS.Module,_TypeString,sys.modules[_TypeString])

	#Do a direct Link of the <TypeString>Class if it exists to the SYS scope
	ClassString=getClassStringWithTypeString(_TypeString)
	if hasattr(sys.modules[_TypeString],ClassString):

		#Set the link
		setattr(SYS.Module,ClassString,getattr(sys.modules[_TypeString],ClassString))
		
		#Set the TypeStringToFirstBaseTypeStringsListLoadedDict for this Class
		SYS.TypeStringToFirstBaseTypeStringsListLoadedDict[_TypeString]=map(lambda Base:getTypeStringWithClassString(Base.__name__),getattr(sys.modules[_TypeString],ClassString).__bases__)
	
	#Update the TypeStringToBaseTypeStringsSetListLoadedDict
	if _TypeString not in SYS.TypeStringToBaseTypeStringsSetListLoadedDict:
		SYS.TypeStringToBaseTypeStringsSetListLoadedDict[_TypeString]=getChildStringsSetListWithParentStringToChildStringsListDictAndStringAndParentString(SYS.TypeStringToFirstBaseTypeStringsListLoadedDict,_TypeString)

	#Update the TypeStringToBaseTypeStringsSetDictLoadedDict
	if _TypeString not in SYS.TypeStringToBaseTypeStringsSetDictLoadedDict:
		SYS.TypeStringToBaseTypeStringsSetDictLoadedDict[_TypeString]=getChildStringsDictWithParentStringToChildStringsListDictAndParentString(
						SYS.TypeStringToFirstBaseTypeStringsListLoadedDict,_TypeString)

	#Look if the TypeString has a particular PluralString
	if hasattr(sys.modules[_TypeString],"PluralString"):
		PluralString=getattr(sys.modules[_TypeString],"PluralString")
	else:
		PluralString=_TypeString+'s'
	writePreVariableStringToPostVariableStringDict("SingularString",[_TypeString],"PluralStringInstalled",PluralString)
	writePreVariableStringToPostVariableStringDict("PluralString",[PluralString],"SingularStringInstalled",_TypeString)

	#Write/Update the OrdedIntToTypeStringInstalledDict				
	TypeStringInt=str(len(SYS.TypeStringToOrderedIntInstalledDict))
	if len(SYS.TypeStringToOrderedIntInstalledDict)<10:
		TypeStringInt="0"+TypeStringInt
	if _TypeString not in SYS.TypeStringToOrderedIntInstalledDict:
		writePreVariableStringToPostVariableStringDict("TypeString",[_TypeString],"OrderedIntInstalled",TypeStringInt)

	#Do an Instance Test
	#if hasattr(sys.modules[_TypeString],ClassString):	
	#	getattr(sys.modules[_TypeString],ClassString)()

def importModulesWithFolderPathStringAndIsParsedBool(_FolderPathString,_IsParsedBool):
	'''
		<Help>
			Load the PackageDict SYS.TypeStringToPackageDictLoadedDict
		</Help>
	
		<Test>
		</Test>
	'''

	#Get the ScriptPathString if it exists
	TypeString=getTypeStringWithFolderPathString(_FolderPathString)
	if TypeString!=None:
		
		#Load the Path
		sys.path.append(_FolderPathString)
		
		#Load the Modules
		importModulesWithTypeString(TypeString)
	
	#Parse if it is sayed
	if _IsParsedBool:

		#Define the ModulesPathString
		ModulesPathString=_FolderPathString+"Modules/"

		#Look for deeper Modules to install
		if os.path.isdir(ModulesPathString):
	
			#Look for deeper Objects
			for FolderString in os.listdir(ModulesPathString):

				# _ in the front of a FolderTypeString is avoided
				if FolderString[0]!="_":

					#Define the SubFolderPathString 
					SubFolderPathString=ModulesPathString+FolderString+"/"
					if len(SubFolderPathString)>0:
						if os.path.isdir(SubFolderPathString):
							SYS.importModulesWithFolderPathStringAndIsParsedBool(SubFolderPathString,True)

def getCommonPrefixStringWithStringsList(_StringsList):
	'''
		<Help>
			Find the longest string that is a prefix of all the strings
		</Help>

		<Test>
			#Load the SpecificModule
			SYS.importModuleWithTypeString("Reader");

			#print the CommonStringPrefixString of two Strings
			print("Prefix shared between \"We share this in common\" and \"We share this not in common\" is :");
			print(SYS.getCommonPrefixStringWithStringsList(["We share this in common","We share this not in common"]));</Test>
		</Test>
	'''
	if not _StringsList:
		return ""
	PrefixString=_StringsList[0]
	for String in _StringsList:
		if len(String)<len(PrefixString):
			PrefixString=PrefixString[:len(String)]
		if not PrefixString:
			return ""
		for Int in xrange(len(PrefixString)):
			if PrefixString[Int] != String[Int]:
				PrefixString=PrefixString[:Int]
				break
	return PrefixString

def getCommonSuffixStringWithStringsList(_StringsList):
	'''
		<Help>
			Find the longest string that is a suffix of all the strings
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS

			#print the CommonStringSuffixString of two Strings
			print("Suffix shared between \"We share this in common\" and \"We share this not in common\" is :");
			print(SYS.getCommonSuffixStringWithStringsList(["We share this in common","We share this not in common"]));
		</Test>
	'''
	if not _StringsList:
		return ""
	SuffixString=_StringsList[0]
	for String in _StringsList[1:]:
		if len(SuffixString)>len(String):
			SuffixString=SuffixString[-len(String):]
		if not SuffixString:
			return ""
		for Int in xrange(1,len(SuffixString)):
			if SuffixString[-Int] != String[-Int]:
				if Int==1:
					SuffixString=""
				else:
					SuffixString=SuffixString[-Int+1:]
				break
	return SuffixString

def getBasesListWithClass(_Class,**_KwargsDict):
	'''
		<Help>
			Get all the Bases for a Class
		</Help>

		<Test>
		</Test>	
	'''
	
	#Set possible default local Items
	LocalDict=getLocalDictWithKwargsDictAndDefaultItemsDict(_KwargsDict,
				{
					'BasesList':[],
				 }
				);
	
	#Check that there are bases
	if hasattr(_Class,'__bases__'):
		
		#Scan the Base
		for Base in _Class.__bases__: 

			#Get the Base
			LocalDict['BasesList']+=[Base];
			
			#Look inside if there is a Base
			if hasattr(LocalDict['BasesList'][-1],'__bases__'):
				if len(LocalDict['BasesList'][-1].__bases__)>0:
					LocalDict['BasesList']+=getBasesListWithClass(Base);
			
	#Return the cumulated Bases
	return LocalDict['BasesList'];

def getPrintedPointerString(_Pointer):
	'''
		<Help>
			Get a simplified print of a Pointer to avoid circular prints
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Print the Pointer version of a Node Object
			print(SYS.getPrintedPointerString(SYS.NodeClass()));
		</Test>
	'''
	return "<"+(_Pointer.__name__ if hasattr(_Pointer,__name__) else "")+" ("+_Pointer.__class__.__name__+"), "+str(id(_Pointer))+">";

def getReadedStringsListWithReaderAndString(_Reader,_String,**Kwargs):
	'''
		<Help>
			Get the ReadedStringsList Between the Begin and the End Flags
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Get a String
			String="print('Hello World');\nThing=3;\nprint('Goodbye World');\n";
			#
			#Print the String
			print("\n String is :");
			print('"');
			print(String);
			print('"');
			#
			#GetPickedString
			PickedStringsList=SYS.getReadedStringsListWithReaderAndString({'Flag':{'BeginString':"print(",'EndString':");\n"},'StringsInt':2},String);
			#
			#Print the list
			print("\n PickedStringsList With String is :");
			print(PickedStringsList);
		</Test>
	'''
	
	#Set the PickedStringsList
	PickedStringsList=[]
	
	#CountsInt
	if _Reader['StringsInt']=="All":
		CountsInt=1
	else:
		CountsInt=_Reader['StringsInt']

	#Define the Flag
	Flag=_Reader['Flag']

	#Scan the Strings
	StringInt=0
	while StringInt<CountsInt:
	
		#Get With the Beginning
		BeginSplittedPickedString=_String.split(Flag['BeginString'])
		
		#Check that there is this Flag
		if len(BeginSplittedPickedString)>1:
		
			#Join the End
			PickedString=Flag['BeginString'].join(BeginSplittedPickedString[1:])
			
			#SplittedPickedString by the EndString
			EndSplittedPickedString=PickedString.split(Flag['EndString'])
			
			#Record if it not the EndString in the FlagsDict !
			if EndSplittedPickedString[0]!='",\'EndString\':"':
			
				#Record the PickedString
				PickedStringsList.append(EndSplittedPickedString[0])

			#Set the new String
			_String=Flag['EndString'].join(EndSplittedPickedString[1:])

		#Increment
		StringInt+=1

		#Special All Case
		if _Reader['StringsInt']=="All" and len(BeginSplittedPickedString)>1:
			StringInt-=1
	
	#Return '' by default
	return PickedStringsList

def getExecStringWithString(_String):

	#Check that it is a String
	if type(_String) in [str,unicode]:

		#Read between "<Exec>" and "</Exec>"
		ReadedStringsList=SYS.getReadedStringsListWithReaderAndString({'Flag':SYS.FlagsDict['ExecFlag'],'StringsInt':1},_String)

		#Look if something was found
		if len(ReadedStringsList)==1:
					
			#Take the only possible ExcutionString
			ExecString=ReadedStringsList[0]

			#Return the ExecString
			if len(ExecString)>0:
				return ExecString

def getKeyStringWithKeyVariable(_KeyVariable):

	if type(_KeyVariable) in [str,unicode]:

		#Maybe it is an Exec String
		ExecString=getExecStringWithString(_KeyVariable)
		if ExecString!=None:
			exec "_KeyVariable="+ExecString in locals()
		return _KeyVariable
	elif type(_KeyVariable) in [int,float]:
		return str(_KeyVariable)
	elif type(_KeyVariable) == list:
		return '_'.join(_KeyVariable)
	elif type(_KeyVariable) ==tuple:
		return '_'.join(list(getKeyStringWithKeyVariable(_KeyVariable)))
	elif hasattr(_KeyVariable,"__class__"):
		if "TypeString" in _KeyVariable.__class__:
			return _KeyVariable.__class__.TypeString
	elif type(_KeyVariable) == dict:
		return map(lambda Item:Item[0]+'And'+getKeyStringWithKeyVariable(Item[1]),_KeyVariable.items())

def getKeyVariableWithKeyVariableString(_KeyVariableString):

	#Define the "_" splitted _KeyString
	StringsList=_KeyVariableString.split("_")

	#Return the splitted or not
	if len(StringsList)>0:
		return StringsList
	else:
		return [_KeyString]

def getVariableWithDictatedVariableAndKeyList(_DictatedVariable,_KeyList):
	'''
		<Help>
			Universal getItem that looks if there is a specific getitem for the Instance
			and also handle SluggerNamesList Key for dict
		</Help>

		<Test>
			#Load SYS module
			import ShareYourSystem as SYS
			#
			#Set a Dict
			Dict={
				   'ThisDict':{
								'ThingsDict':{'Thing':0},
								'MyObjectsList':[{'ID':0}]
							},
					'Stuff':"Sad",
					'SimpleList':[1,2]
				};
			#
			#Print the Dict
			print("The Dict is : ");
			print(Dict);
			#
			#Get the ValueVariable with a KeyString
			print("\n Getted With [] Key :");
			print(SYS.getVariableWithDictatedVariableAndKeyList(Dict,[]));
			#
			#Get the ValueVariable with a SluggerStringsList
			print("\n Getted With ['ThisDict','ThingsDict'] Key :");
			print(SYS.getVariableWithDictatedVariableAndKeyList(Dict,['ThisDict','ThingsDict']));
			#
			#Get the GettedValueWithKey with a SluggerNamesList with Idx
			print("\n Getted With ['ThisDict','MyObjectsList',0] Key :");
			print(SYS.getVariableWithDictatedVariableAndKeyList(Dict,['ThisDict','MyObjectsList',0]));
		</Test>
	'''

	if type(_KeyList)==list:

		#Empty list case : return the Object
		if len(_KeyList)==0:
			return _DictatedVariable;
		elif len(_KeyList)==1:

			#One Variable List case : return the associated Value at the Int
			if type(_DictatedVariable) in [list,tuple]:
				if type(_KeyList[0])==int:
					if _KeyList[0]<len(_DictatedVariable):
						return _DictatedVariable[_KeyList[0]]

			elif _KeyList[0] in _DictatedVariable:

				#One Variable Dict case : return the associated Value at the KeyString
				return _DictatedVariable[_KeyList[0]]
		else:
			
			#Multi Variables case : recursive call with the reduced list
			if _KeyList[0] in _DictatedVariable:
				return getVariableWithDictatedVariableAndKeyList(_DictatedVariable[_KeyList[0]],_KeyList[1:])

	#Return by default "NotFound"
	return "NotFound"

def getVariableWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable):
	if type(_KeyVariable)==list:
		return getVariableWithDictatedVariableAndKeyList(_DictatedVariable,_KeyVariable)
	elif type(_KeyVariable) in  [str,unicode]:
		return _DictatedVariable[_KeyVariable] if _KeyVariable in _DictatedVariable else None

def setWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable,_ValueVariable):
	'''
		<Help>
			Define a set Method with a NameStringsList as a Key for dict
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Set a Dict
			Dict={
				   'ThisDict':{
								'ThingsDict':{'Thing':0},
								'MyObjectsList':[{'ID':0}]
							},
					'Stuff':"Sad",
					'SimpleList':[1,2]
				};
			#
			#setWithSlugger with a SluggerNamesList Key
			SYS.setVariableWithDictatedVariableAndKeyVariable(Dict,['ThisDict','ThingsDict','OtherThing'],1);
			#
			#Print this setted Dict at ['ThisDict','ThingsDict','OtherThing'] equal to 1
			print("The Dict before setting is : ");
			print(Dict);
			#
			#setWithSlugger with a SluggerNamesList Key with Idx
			SYS.setVariableWithDictatedVariableAndKeyVariable(Dict,['ThisDict','MyObjectsList',0,'ID'],1);
			#
			#Print this setted Dict at ['ThisDict','MyObjectsList',0,'ID'] equal to 1
			print("\n The Dict after setting at ['ThisDict','MyObjectsList',0,'ID'] equal to 1 is : ");
			print(Dict);
		</Test>
	'''
	#Special dict case for also handling SluggerNamesList Key
	if type(_DictatedVariable)==dict:
		if type(_KeyVariable)==list:
			LastSlugger=getVariableWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable[:-1])
			setWithDictatedVariableAndKeyVariable(LastSlugger,_KeyVariable[-1],_ValueVariable)
			return
		else:
			_DictatedVariable[_KeyVariable]=_ValueVariable
			return

	#List Case
	if type(_DictatedVariable)==list:
		if type(_KeyVariable)==list():
			NextSlugger=getVariableWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable[0])
			setWithDictatedVariableAndKeyVariable(NextSlugger,_KeyVariable[1:],_ValueVariable)
			return
		else:
			_DictatedVariable[_KeyVariable]=_ValueVariable
			return

def getIsTypedVariableBoolWithTypeStringAndClass(_TypeString,_Class):

	#Get the BaseStringsSetList
	ClassAndBaseStringsSetList=[_Class.__name__]+map(lambda Base:Base.__name__,list(set(getBasesListWithClass(_Class))))

	#Check that it has the ChildClass inside
	if getClassStringWithTypeString(_TypeString) in ClassAndBaseStringsSetList:

		#Return 
		return True

	#Return False by default
	return False

def getIsBaseTypeStringBoolWithBaseTypeStringAndTypeString(_BaseTypeString,_TypeString):

	#Get the Class
	Class=getattr(SYS,getClassStringWithTypeString(_TypeString))

	#Look for the getIsTypedVariableBoolWithTypeStringAndClass
	if Class!=None:
		return getIsTypedVariableBoolWithTypeStringAndClass(_BaseTypeString,getattr(SYS,getClassStringWithTypeString(_TypeString)))
	else:
		return False

def getIsTypedVariableBoolWithTypeStringAndVariable(_TypeString,_Variable):

	#Check if it has a Class
	if hasattr(_Variable,"__class__"):

		#Call the getIsTypedVariableBoolWithClass
		return getIsTypedVariableBoolWithTypeStringAndClass(_TypeString,_Variable.__class__)

def getIsPythonTypeStringBool(_String):

	#Check that it is a string
	if type(_String) in [str,unicode]:

		#Get the KeyVariableStringsList
		KeyVariableStringsList=SYS.getWordStringsListWithString(_String)

		#See if it respects the conditions
		if len(KeyVariableStringsList)>0:
			if KeyVariableStringsList[-1] not in PythonTypeStringsList:
				return True

		#Return False by default
		return False

def getDictWithVariable(_Variable):
	if type(_Variable)==dict:
		return _Variable
	elif hasattr(_Variable,'__dict__'):
		return _Variable.__dict__
	else:
		return {}

def getTypedVariableWithVariableAndTypeString(_Variable,_TypeString):

	#Case where it is already the case
	if SYS.getIsTypedVariableBoolWithTypeStringAndVariable(_TypeString,_Variable):
		return _Variable
	else:

		#Update the Type
		ClassString=getClassStringWithTypeString(_TypeString)
		if type(ClassString) in [str,unicode]:
			if callable(getattr(SYS,ClassString)):

				#Get the VariableDict
				VariableDict=copy.deepcopy(getDictWithVariable(_Variable))

				#Get ta new Instance of the new Type
				Variable=getattr(SYS,ClassString)()
				
				#Update
				map(lambda Item:
					Variable.__setitem__(Item[0],Item[1]) 
					if Item[0] not in Variable.TypeStringToKeyStringsListDict['Core']+[
					'SkippedKeyStringsList'
					] else None,VariableDict.items())

				#Return Variable
				return Variable

	#Return the Variable by default
	return _Variable

def getTypedVariableWithVariableAndTypeStringTuple(_VariableAndTypeStringTuple):
	return getTypedVariableWithVariableAndTypeString(_VariableAndTypeStringTuple[0],_VariableAndTypeStringTuple[1])

def getSingularStringWithPluralString(_PluralString):
	'''
		<Help>
			Get Singular Names managing exceptions defined in the Settings
		</Help>
	
		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Get A Singular Name
			print("Plural Name is Things, Singular Name is "+SYS.getSingularStringWithPluralString("Things"));
		</Test>
	'''

	#Get all the Plural Strings that are defined
	PluralStringsList=SYS.SingularStringToPluralStringInstalledDict.values()

	#Return 
	if _PluralString in PluralStringsList:
		return SYS.SingularStringToPluralStringInstalledDict.keys()[PluralStringsList.index(_PluralString)]
	else:
		return _PluralString[:-1]

def getPluralStringWithSingularString(_SingularString):

	if _SingularString in SYS.SingularStringToPluralStringInstalledDict:
		return SYS.SingularStringToPluralStringInstalledDict[_SingularString]
	else:
		return _SingularString+"s"

def getFlippedDict(_Dict):
	'''
		<Help>
			Flip a StringsDict putting the Values as the Keys
		</Help>
	
		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Define a Dict
			Dict={"a":"0","b":"1","c":"2"};
			#
			#Print the Dict before Flipping
			print("The Dict before flipping is : ");
			print(Dict);
			#Print the Dict after Flipping
			print("\nThe Dict after flipping is : ");
			print(getFlippedDict(Dict));
		</Test>
	'''

	if all(map(lambda ValueVariable:type(ValueVariable) in [str,unicode],_Dict.values())):
		return dict(map(lambda ItemTuple:(ItemTuple[1],ItemTuple[0]),_Dict.items()))

def renameWithDict(_Dict,_OldKeyString,_NewKeyString):
	if _OldKeyString!=_NewKeyString:
		if _OldKeyString in _Dict:

			#Set with the new key
			_Dict[_NewKeyString]=_Dict[_OldKeyString]

			#Delete the _OldKey
			del _Dict[_OldKeyString]

def getIsKwargsFunctionBool(_Function):
	'''
		<Help>
		Return if this Function accepts Kwargs
		</Help>
		
		<Test>
		#Load ShareYourSystem as SYS
		import ShareYourSystem as SYS
		#
		#Define a Function
		def foo(**Kwargs):
		pass;
		#Print
		print(SYS.IsKwargsFunction(foo));
		</Test>
	'''
	
	#Define the FunctionTypeString
	FunctionTypeString=type(_Function).__name__
	
	#Init the VarKwargsDict
	VarKwargsDict=None
	
	#Get directly the Args with the function Type
	if FunctionTypeString=="function":
		
		#Find the Arguments Types
		ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(_Function)
	
	elif FunctionTypeString=='builtin_function_or_method':
		return False
	elif hasattr(_Function,"__func__"):
		
		#Find the Arguments Types
		ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(_Function.__func__)
	
	#Return if Kwargs is accepted or not
	return VarKwargsDict!=None;

def callWithFunction(_Function,*_Args,**_Kwargs):
	'''
		<Help>
		Set the Class of an associated Object
		</Help>
		
		<Test>
	'''
	if getIsKwargsFunctionBool(_Function):
		OutputVariable=_Function(*_Args,**_Kwargs)
	else:
		OutputVariable=_Function(*_Args)
	
	#Return the OutputVariable if there is one
	return OutputVariable

def getIndicesIntsTuplesListWithIntsList(_IntsList):
	import itertools
	return list(itertools.product(*map(lambda LengthInt:xrange(LengthInt),_IntsList)))

def getKeepedDictWithDictatingVariableAndKeepedKeyStringsList(_DictatingVariable,_KeepedKeyStringsList):
	return dict(map(lambda FilteredKeepedKeyString:(FilteredKeepedKeyString,getattr(_DictatingVariable,FilteredKeepedKeyString)),filter(lambda KeepedKeyString:hasattr(_DictatingVariable,KeepedKeyString),_KeepedKeyStringsList)))

def getArrayedIntsListWithArrayedLengthIntsListAndProdIntsListAndRankedInt(_ArrayedLengthIntsList,_ProdIntsList,_RankedInt):

	#Define the EuclidianInt
	EuclidianInt=_RankedInt

	#Init the GridedIntsList for this TuplingInt
	ArrayedIntsList=[0]*len(_ArrayedLengthIntsList)

	#Build the Euclidian List of corresponding Int
	for TableInt in xrange(len(_ArrayedLengthIntsList)):

		if TableInt==len(_ArrayedLengthIntsList)-1:
			ArrayedIntsList[TableInt]=EuclidianInt%_ArrayedLengthIntsList[TableInt]
		else:
			ArrayedIntsList[TableInt]=EuclidianInt/_ProdIntsList[TableInt]
			EuclidianInt=EuclidianInt%_ProdIntsList[TableInt]

	#Return
	return ArrayedIntsList

def getArrayedIntsListWithArrayedLengthIntsListAndRankedInt(_ArrayedLengthIntsList,_RankedInt):

	#Get the ProdIntsList
	ProdIntsList=map(lambda Int:reduce(operator.mul,_ArrayedLengthIntsList[Int+1:]),xrange(len(_ArrayedLengthIntsList)-1))

	#Return 
	return getArrayedIntsListWithArrayedLengthIntsListAndProdIntsListAndRankedInt(_ArrayedLengthIntsList,ProdIntsList,_RankedInt)

def getPermutationIntWithCoordinateIntsListAndVariablesInt(_CoordinateIntsList,_VariablesInt):
	'''
		<Help>
			From a CoordinateIntsList it returns the PermutationInt
		</Help>

		<Test>
			import ShareYourSystem as SYS
			import itertools
			a=list(itertools.product([0,1,2],repeat=2))
			print(a)
			Int=SYS.getPermutationIntWithCoordinateIntsListAndVariablesInt([2,1],3)
			print(a[Int])
		</Test>
	'''
	#Define the DegreeInt
	LengthInt=len(_CoordinateIntsList)
	ProdLengthIntsList=[0]*LengthInt

	#Map over
	map(lambda Int:ProdLengthIntsList.__setitem__(Int,(_VariablesInt)*ProdLengthIntsList[Int-1] if Int>0 else 1),xrange(LengthInt))
	ProdLengthIntsList.reverse()

	#Inner Product
	RankedIntsList=map(operator.mul,_CoordinateIntsList,ProdLengthIntsList)

	#Return
	return sum(RankedIntsList)

def getInstanceWithTypeString(_TypeString):
	ClassString=getClassStringWithTypeString(_TypeString)
	if hasattr(SYS,ClassString):
		return getattr(SYS,ClassString)()

def getIsListsListBool(_ListsList):

	#Check if it is a List of Lists
	if type(_ListsList)==list:
		if len(_ListsList)>0:
			return all(map(lambda ListedVariable:type(ListedVariable)==list,_ListsList))

	#Return False by Default
	return False

def getStructuredDictsListWithIntsListsList(_IntsListsList):

		#Transform the ListedVariablesIntsList into list of empty dicts
		return map(lambda IntOrList:
			map(lambda Int:{},xrange(IntOrList)) 
			if type(IntOrList)==int 
			else getStructuredDictsListWithIntsListsList(IntOrList),
			_IntsListsList)
	
def getConcatenatedList(_List):

	#Init the ConcatenatedList
	ConcatenatedList=[]

	#Concatenate
	map(lambda ListOrString:ConcatenatedList.extend(ListOrString) if type(ListOrString)==list else ConcatenatedList.append(ListOrString),_List)
	
	#Return
	return ConcatenatedList

def getPathStringsListWithDict(_DictOrString,**_KwargsDict):

	#Define the LinkString
	LinkString="<ParentToChild>"

	#Init the PathStringsList if not already
	if "PathStringsList" not in _KwargsDict:
		if type(_DictOrString)==dict:
			_KwargsDict['PathStringsList']=_DictOrString.keys()
		elif type(_DictOrString) in [str,unicode]:
			_KwargsDict['PathStringsList']=[_DictOrString]
	
	if type(_DictOrString)==dict:

		#Call recursively the Function to build the Paths
		List=map(
			lambda PathString:
			map(lambda ChildDictOrString:PathString+LinkString+ChildDictOrString 
										if type(ChildDictOrString) in [str,unicode]
										else
											map(lambda ChildPathString:
												PathString+LinkString+ChildPathString,
												#ChildPathString,
												getPathStringsListWithDict(ChildDictOrString))
				,_DictOrString[PathString]) 
			if len(_DictOrString[PathString])>0 
			else 
			PathString,
			_KwargsDict['PathStringsList'])

		#Concatenate
		_List=getConcatenatedList(getConcatenatedList(List))
		_KwargsDict['PathStringsList']=_List

	#Return _KwargsDict['PathStringsList']
	return _KwargsDict['PathStringsList']

def getPathListsListWithDict(_Dict):

	#Define the LinkString
	LinkString="<ParentToChild>"

	#Return
	return map(lambda PathString:PathString.split(LinkString),SYS.getPathStringsListWithDict(_Dict))

def getReversedListsList(_PathListsList):

	#Reverse for each
	map(lambda PathList:PathList.reverse(),_PathListsList)

	#return 
	return _PathListsList

def getReversedPathListsListWithDict(_Dict):
	return getReversedListsList(getPathListsListWithDict(_Dict))

def getLengthSortedListsList(_ListsList):
	return sorted(_ListsList,key=len)

def getLengthSortedPathListsListWithDict(_Dict):
	return getLengthSortedListsList(getReversedListsList(getPathListsListWithDict(_Dict)))

def getReversedPathStringsListWithDict(_Dict):

	#Define the LinkString
	LinkString="<ParentToChild>"
	
	return map(lambda ReversedPathList:
		LinkString.join(ReversedPathList),
		getLengthSortedListsList(getReversedPathListsList(getPathListsListWithDict(_Dict))))

def getReversedPathListWithDict(_Dict):
	return getLengthSortedListsList(getReversedListsList(getPathListsListWithDict(_Dict)))

def getBuildingTuplesListWithDict(_Dict):
	return map(lambda ReversedPathString:(ReversedPathString,{}),getReversedPathListWithDict(_Dict))

def setWithDictAndBuildingTuple(_Dict,_BuildingTuple):

	if type(_BuildingTuple) in [list,tuple]:
		if type(_BuildingTuple)==list:
			if len(_BuildingTuple)!=2:
				return
		PathList=_BuildingTuple[0]
		if len(PathList)>0:
			try:
				ChildVariable=_Dict[PathList[0]]
			except KeyError:
				_Dict[PathList[0]]={}
				ChildVariable=_Dict[PathList[0]]
			setWithDictAndBuildingTuple(ChildVariable,(PathList[1:],_BuildingTuple[1]))
		elif len(PathList)==1:
			_Dict[PathList[0]]=_BuildingTuple[1]
		else:
			return

def getBuildedDictWithBuildingTuplesList(_BuildingTuplesList,**_KwargsDict):
	
	#Init the BuildedDict if not already
	if "BuildedDict" not in _KwargsDict:
		_KwargsDict['BuildedDict']={}

	#Call recursively the function
	map(lambda BuildingTuple:
			setWithDictAndBuildingTuple(_KwargsDict['BuildedDict'],BuildingTuple),
			_BuildingTuplesList
		)

	#Return the BuildedDict
	return _KwargsDict['BuildedDict']

def getShiftedListWithShiftingInt(_List,_ShiftingInt):
	'''
		<Test>
			print(SYS.getShiftedListWithShiftingInt([1,2,3],0))
			print(SYS.getShiftedListWithShiftingInt([1,2,3],1))
			print(SYS.getShiftedListWithShiftingInt([1,2,3],2))
			print(SYS.getShiftedListWithShiftingInt([1,2,3],3))
		</Test>
	'''
	return list(itertools.islice(itertools.cycle(_List),_ShiftingInt,_ShiftingInt+len(_List)))

def getAnchoredShiftedListWithAnchoringVariable(_List,_AnchoringVariable):
	try:
		return getShiftedListWithShiftingInt(_List,_List.index(_AnchoringVariable))
	except ValueError:
		return _List

def getCuttedListWithCuttingVariable(_List,_CuttingVariable):

	'''
		<Test>
			print(getCuttedListWithCuttingVariable(["1","2","3","4"],"1"))
		</Test>
	'''
	try:
		CuttedInt=_List.index(_CuttingVariable)
		CuttedList=_List[:CuttedInt+1] if CuttedInt<len(_List) else _List
		CuttedList.reverse()
		return CuttedList
	except:
		return []

def getCuttedListsListWithCuttingVariable(_ListsList,_CuttingVariable):
	return filter(lambda List:List!=[],map(lambda List:
		getCuttedListWithCuttingVariable(List,_CuttingVariable),
		_ListsList))

def getCuttedPathListsListWithDictAndCuttingVariable(_Dict,_CuttingVariable):
	return getCuttedListsListWithCuttingVariable(SYS.getPathListsListWithDict(_Dict),_CuttingVariable)

def getBuildingTuplesListWithDictAndBuildingString(_Dict,_BuildingString):
	return map(lambda CuttedPathList:
		(CuttedPathList,{}),
		getCuttedPathListsListWithDictAndCuttingVariable(_Dict,_BuildingString))

def getChildReBuildedDictWithBuildingString(_Dict,_BuildingString):
	'''
		<Test>
			#Rebuild with a Default Dict
			Dict={"3":[{"2a":[{"1a":["0a","0b"]},"1b"],"2b":[{"1b":["0a"],"1c":["0b"]}]}],"0a":[]}
			print(SYS.getChildReBuildedDictWithBuildingString(Dict,"0a"))
			#
			#Rebuild with the BaseTypes Structure
			Dict=getChildReBuildedDictWithBuildingString(SYS.TypeStringToBaseTypeStringsSetDictLoadedDict,"Structure")
		</Test>
	'''
	return SYS.getBuildedDictWithBuildingTuplesList(getBuildingTuplesListWithDictAndBuildingString(_Dict,_BuildingString))

def getParentReBuildedDict(_Dict):
	return SYS.getBuildedDictWithBuildingTuplesList(SYS.getBuildingTuplesListWithDict(_Dict))

def getKeyStringsListWithVariable(_Variable,**_KwargsDict):

	#Init the KeyStringsList if not already
	if "KeyStringsList" not in _KwargsDict:
		_KwargsDict['KeyStringsList']=[]

	#Get the Dict
	Dict=SYS.getDictWithVariable(_Variable)

	#Get the KeyStrings
	map(lambda KeyString:_KwargsDict['KeyStringsList'].append(KeyString),Dict.keys())
	
	#Call in deeper layers
	map(lambda ChildVariable:_KwargsDict['KeyStringsList'].extend(getKeyStringsListWithVariable(ChildVariable)) 
		if SYS.getDictWithVariable(ChildVariable)!={}
		else None,Dict.values())

	#Return _KwargsDict['KeyStringsList']
	return _KwargsDict['KeyStringsList']

def getDerivedTypeStringsListWithTypeString(_TypeString):
	return getKeyStringsListWithVariable(
		getChildReBuildedDictWithBuildingString(
			SYS.TypeStringToBaseTypeStringsSetDictLoadedDict,_TypeString
			)[_TypeString])

def getDerivedTypeStringToSpecificTypeStringsSetListTuplesListWithTypeString(_TypeString):

	#Get the BaseTypeStringsSetList
	BaseTypeStringsSetList=set([_TypeString]+SYS.TypeStringToBaseTypeStringsSetListLoadedDict[_TypeString])

	#Get the FirstDerivedTypeStringToTypeStringsSetListTuplesList
	DerivedTypeStringToTypeStringsSetListTuplesList=map(
			lambda DerivedTypeString:
				(
					DerivedTypeString,
					set(SYS.TypeStringToBaseTypeStringsSetListLoadedDict[DerivedTypeString])
				),
		getDerivedTypeStringsListWithTypeString(_TypeString)
			)

	#Keep the ones with their SpecificBaseTypes
	map(lambda ItemTuple:ItemTuple[1].difference_update(BaseTypeStringsSetList),DerivedTypeStringToTypeStringsSetListTuplesList)

	#Add
	map(lambda ItemTuple:ItemTuple[1].add(ItemTuple[0]),DerivedTypeStringToTypeStringsSetListTuplesList)

	#Return
	return DerivedTypeStringToTypeStringsSetListTuplesList

def getTypedStringToBaseTypeStringsListTuplesListWithTypeStringAndBaseTypeStringsList(_TypeString,_BaseTypeStringsList):
	'''
		<Test>
			print(SYS.getDerivedTypeStringToSpecificTypeStringsSetListTuplesListWithTypeString("Dictionnary"));
		</Test>
	'''

	#Get the TypeStringBaseTypeStringsSetList of the _TypeString
	TypeStringBaseTypeStringsSetList=SYS.TypeStringToBaseTypeStringsSetListLoadedDict[_TypeString]

	#Filter the BaseTypeStrings that are already in the _TypeString
	_BaseTypeStringsList=filter(lambda BaseTypeString:BaseTypeString not in TypeStringBaseTypeStringsSetList,_BaseTypeStringsList)

	#Get the FirstDerivedTypeStringToBaseTypeStringsSetListDict
	DerivedTypeStringToSpecificTypeStringsSetListTuplesList=[(_TypeString,set([]))]+getDerivedTypeStringToSpecificTypeStringsSetListTuplesListWithTypeString(_TypeString)

	#Transform into a set
	if type(_BaseTypeStringsList)!=set:
		BaseTypeStringsSetList=set(_BaseTypeStringsList)

	#Look for the subset containing the _BaseTypeStringsList
	SubsetDerivedTypeStringToBaseTypeStringsSetListTuplesList=filter(lambda ItemTuple:BaseTypeStringsSetList.issubset(set(ItemTuple[1])),
			DerivedTypeStringToSpecificTypeStringsSetListTuplesList)

	#Return the Subset
	return SubsetDerivedTypeStringToBaseTypeStringsSetListTuplesList

def getTypedStringWithTypeStringAndBaseTypeStringsList(_TypeString,_BaseTypeStringsList):
	'''
		<Test>
			print(SYS.getTypedStringWithTypeStringAndBaseTypeStringsList("Dictionnary",[]));
		</Test>
	'''
	try:
		return getTypedStringToBaseTypeStringsListTuplesListWithTypeStringAndBaseTypeStringsList(_TypeString,_BaseTypeStringsList)[0][0]
	except IndexError:
		return ""


def getFlattenedListWithVariablesList(_VariablesList):
	'''
		<Test>
			print(SYS.getFlattenedListWithVariablesList([(1,2),5]));
		</Test>
	'''

	return reduce(
					lambda x,y:
						x+list(y) if type(y)==tuple 
						else list(x)+[y] if type(x) in [list,tuple] 
						else [x,y],_VariablesList
				)

def getPermutedIntsListWithPermutedIntAndPermutingInt(_PermutedInt,_PermutingInt):
	'''
		<Test>
				PermutedIntsList=SYS.getPermutedIntsListWithPermutedIntAndPermutingInt(2,12);
				print(PermutedIntsList);
				print(2**12,len(PermutedIntsList));
		</Test>
	'''
	return 	reduce(
					lambda x,y:
						map(
								lambda IntOrTuple:
										getFlattenedListWithVariablesList(list(IntOrTuple)) 
										if type(IntOrTuple)==tuple
										else IntOrTuple,
										itertools.product(x,y)
							),
						map(lambda Int:xrange(_PermutedInt),xrange(_PermutingInt))
				)

def getOpenedProcessIdStringsListWithProcessNameString(_ProcessNameString):

	return map(
				lambda StringsList:
				StringsList[1]
				,
				map(
					lambda StringsList:
					filter(lambda String:String!='',StringsList)
					,filter(lambda StringsList:
							_ProcessNameString in StringsList and '-u' in StringsList,
							map(lambda CommandString:
										CommandString.split(' '),
										os.popen("ps -ef | grep "+_ProcessNameString).read().split('\n')
								)
							)
				)
			)

def getDictedDictWithVariable(_Variable):

	if hasattr(_Variable,'keys'):

		#Do the recursive call
		return dict(
					map(
							lambda KeyString:
							(KeyString,getDictedDictWithVariable(_Variable[KeyString])),
							_Variable.keys()
						)
					)
	else:

		#Return just the Variable either
		return _Variable

def setGroupWithIndexStringAndDatabaseFileAndDataItemTuple(_IndexString,_DatabaseFile,_DataItemTuple):

	#Set the group
	if _DataItemTuple[0] not in _DatabaseFile:
		_DatabaseFile.create_group(_DataItemTuple[0]) 

	#set deeper in this group
	setDataWithIndexStringAndDatabaseFileAndDataDict(_IndexString,_DatabaseFile[_DataItemTuple[0]],_DataItemTuple[1])

def setDataSetWithIndexStringAndDatabaseFileAndDataItemTuple(_IndexString,_DatabaseFile,_DataItemTuple):

	#Set the group
	if _DataItemTuple[0] not in _DatabaseFile:
		_DatabaseFile.create_group(_DataItemTuple[0]) 

	#set deeper in this group
	_DatabaseFile[_DataItemTuple[0]].create_dataset(_IndexString,data=_DataItemTuple[1])

def setDataWithIndexStringAndDatabaseFileAndDataDict(_IndexString,_DatabaseFile,_DataDict):

	map(
			lambda DataItemTuple:
			#Dict Case
			setGroupWithIndexStringAndDatabaseFileAndDataItemTuple(_IndexString,_DatabaseFile,DataItemTuple) 
			if type(DataItemTuple[1])==dict 
			else
				map(
					lambda IntAndListedVariable:
					setGroupWithIndexStringAndDatabaseFileAndDataItemTuple("_"+str(IntAndListedVariable[0]),_DatabaseFile,IntAndListedVariable[1]),
					enumerate(DataItemTuple[1])
					)
					if type(DataItemTuple[1])==list and all(map(lambda ListedVariable:type(ListedVariable)==dict,DataItemTuple[1]))
					else
						None
						if type(DataItemTuple[1])==list and any(map(lambda ListedVariable:type(ListedVariable)!=dict,DataItemTuple[1]))
						else
							_DatabaseFile.__setitem__(DataItemTuple[0],DataItemTuple[1])
							if DataItemTuple[0]=="_"
							else
							setDataSetWithIndexStringAndDatabaseFileAndDataItemTuple(_IndexString,_DatabaseFile,DataItemTuple),
			_DataDict.items()
		) 

def getLastIndexIntWithDatabaseFileAndDataDict(_DatabaseFile,_DataDict):

	return max(
				map(
						lambda DataString:
						max(
								map(
									lambda KeyString:
									(int)(KeyString),
									_DatabaseFile[DataString].keys()
								) 
							) if all(map(lambda String:String.isdigit(),
									_DatabaseFile[DataString].keys()))
							else
							getLastIndexIntWithDatabaseFileAndDataDict(_DatabaseFile[DataString],_DataDict[DataString]),
						_DataDict.keys()
					)
			)


def getArgumentFloatWithComplex(_Complex):

	#Define the RealFloat and the ImagFloat
	RealFloat=numpy.real(_Complex)
	ImagFloat=numpy.imag(_Complex)

	#Case where it is not a pure real
	if ImagFloat!=0.:

		#Compute the DenominatorFloat
		DenominatorFloat=numpy.sqrt(ImagFloat**2+RealFloat**2)+RealFloat

		if DenominatorFloat!=0.:
			
			#Return the division
			return 2.*numpy.arctan(ImagFloat/(DenominatorFloat))

	#Return 0
	return 0.

def getSplitListsListWithSplittedListAndFunctionPointer(_SplittedList,_FunctionPointer):

	#Init the SplitListsList
	SplitListsList=[[],[]]

	#do the map with side effect
	map(
			lambda ListedVariable:
			SplitListsList[0].append(ListedVariable) 
			if _FunctionPointer(ListedVariable) 
			else SplitListsList[1].append(ListedVariable),
			_SplittedList
		)

	#Return
	return SplitListsList

def groupby(_FunctionPointer,_List):
	return getSplitListsListWithSplittedListAndFunctionPointer(_List,_FunctionPointer)

def getTaggedTuplesListWithDictAndTagString(_Dict,_TagString):

	#Get the TaggedTuplesList
	return filter(
		lambda ItemTuple:
		SYS.getCommonPrefixStringWithStringsList([ItemTuple[0],_TagString])==_TagString,
		_Dict.items()
		)

#def filterByTag(_Dict,_TagString):
#	return getTaggedTuplesListWithDictAndTagString(_Dict,_TagString)

######################################################################
#Define the SYSClass that gives to the SYS module Class useful __getattr__,__setattr__ methods
class SYSClass(object):
	def __init__(self,_Module):
		'''
			<Help>
				Init an Object that Wrap an Instance (useful for Module) for setting it like a Class Instance
			</Help>
		
			<Test>
				#Print a SYS __dict__
				SYS=SYS.SYSClass();
				print(SYS.__dict__);
			</Test>
		'''
		
		#Wrap the Module
		self.__dict__['Module']=_Module;
		
		#Set a Pointer to itself
		self.__dict__['SYS']=self

	def __getattr__(self,_KeyString):
		'''
			<Help>
				Get an Attribute safely in the SYS Wrapper
				and build shortcut get function names for accessing
				the loaded Variables
			</Help>
		
			<Test>
				#Print a SYS __dict__
				SYS=SYS.SYSClass();
				print(SYS.__dict__);
			</Test>
		'''
		#Get safely existing Values
		if hasattr(self.Module,_KeyString):
			return getattr(self.Module,_KeyString)
		
		else:

			#Check if it is a FunctionString From a not yet setted Module
			if _KeyString in self.Module.AttributeStringToTypeStringInstalledDict:
				self.Module.importModulesWithFolderPathStringAndIsParsedBool(SYS.Module.TypeStringToFolderPathStringInstalledDict[SYS.Module.AttributeStringToTypeStringInstalledDict[_KeyString]],False)
				return getattr(self.Module,_KeyString)

			#Print no found
			#print("SYS didn't find ValueVariable associated with _KeyString : "+str(_KeyString)) 

	def __repr__(self):
		'''
			<Help>
				Represent the SYS Wrapper Object by displaying
				all its Loaded Variables
			</Help>
		
			<Test>
				#Print a SYS __dict__
				SYS=SYS.SYSClass();
				print(SYS.__dict__);
			</Test>
		'''

		RepresentingString=""
		for LoadedVariableString in SYS.LoadedVariableStringsList:
			RepresentingString+="TypeStringTo"+LoadedVariableString+"LoadedDict is :"
			RepresentingString+=str(getattr(SYS,"TypeStringTo"+LoadedVariableString+"LoadedDict"))
			RepresentingString+="\n"

		return RepresentingString

	def __setattr__(self,_KeyString,_ValueVariable):
		
		#Add to the _ValueVariable the _KeyString as a VariableString if it has __setitem__
		if hasattr(_ValueVariable,'__setitem__'):
			_ValueVariable.__setitem__("VariableString",_KeyString)

		#Set to the Module
		setattr(self.Module,_KeyString,_ValueVariable)

#Define the CoreClass that helps each SYS Objects to have boilerplate get,set,delete,rename,call,repr,update methods calling each base methods in a sorted manner
class CoreClass():
	
	#Define a Local link to SYS
	SYS=sys.modules[__module__]

	#Define a generic init Method
	def __init__(self):
		'''
			<Help>
				Generic Init Method for an Instance in ShareYourSystem
			</Help>

			<Test>

			</Test>
		'''

		#Set the TypeString
		self.TypeString=getTypeStringWithClassString(self.__class__.__name__)

		#Define the CalledTypeStringsList
		self.BeforeBasesCalledTypeStringsList=[self.TypeString]+SYS.TypeStringToBaseTypeStringsSetListLoadedDict[self.TypeString]
		self.AfterBasesCalledTypeStringsList=[self.TypeString]+SYS.TypeStringToBaseTypeStringsSetListLoadedDict[self.TypeString]
		self.AfterBasesCalledTypeStringsList.reverse()

		#Define the FrozenMethodStringsLists
		self.FrozenMethodStringsList=[]

		#Define the Is<CommandingString>Bool
		for	CommandedString in SYS.CommandStringToCommandedStringDict.values():
			setattr(self,"Is"+CommandedString[:-2]+"ingBool",True)

		#Init the TypeStringToIs<CommandedString>BoolDict
		for CommandedString in SYS.CommandStringToCommandedStringDict.values():

			#Get the TypeStringToIsCommandedStringBoolClassDictString
			TypeStringToIsCommandedStringBoolDictString="TypeStringToIs"+CommandedString+"BoolDict"

			#Init
			setattr(self,TypeStringToIsCommandedStringBoolDictString,{})

			#Fill with True
			for CalledTypeString in self.BeforeBasesCalledTypeStringsList:
				getattr(self,TypeStringToIsCommandedStringBoolDictString)[CalledTypeString]=True

		#Define the TypeStringToKeyStringsListInstanceDict
		self.TypeStringToKeyStringsListDict={'Core':self.keys()+["TypeStringToKeyStringsListDict"]}

		#Sort the HookMethodStrings
		CommandString="init"
		for OrderString in ["Before","After"]:
			
			#Call the specific BeforeAfterBases initItem For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):

				#Is this Specific Method on ?
				if getattr(self,"TypeStringToIs"+SYS.CommandStringToCommandedStringDict[CommandString]+"BoolDict")[CalledTypeString]:

					#Call with the MethodString
					HookMethodString=CommandString+OrderString+"BasesWith"+CalledTypeString
					if hasattr(self,HookMethodString):
						if HookMethodString not in self.FrozenMethodStringsList:		
							getattr(self,HookMethodString)()

	#Define a generic getItem Method
	def __getitem__(self,_KeyVariable,**_KwargsDict):
		'''
			<Help>
				Generic GetItem Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#Define the Command String
		CommandString="get"

		#Init the ValueVariable
		ValueVariable=None

		#Refresh the IsDeletingBool
		self.IsGettingBool=True

		#Sort the OrderString Calls
		for OrderString in ["Before","After"]:

			#Call the specific Before getItem For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):

				#Is this Specific Method on ?
				if getattr(self,"TypeStringToIs"+SYS.CommandStringToCommandedStringDict[CommandString]+"BoolDict")[CalledTypeString]:

					#Look if the Global IsBool is ok
					if self.IsGettingBool==False:
						return ValueVariable

					#Call the MethodString
					HookMethodString=CommandString+OrderString+"BasesWith"+CalledTypeString
					if hasattr(self,HookMethodString):
						if HookMethodString not in self.FrozenMethodStringsList:
							ValueVariable=getattr(self,HookMethodString)(_KeyVariable)

					#Look if the ValueVariable is already getted
					if ValueVariable!=None:
						return ValueVariable

			#Do the minimal getitem
			if type(_KeyVariable) in [str,unicode]:

				#Get Safely the Value
				if _KeyVariable in self.__dict__:
					return self.__dict__[_KeyVariable]

	#Define a generic delItem Method
	def __delitem__(self,_KeyVariable,**_KwargsDict):
		'''
			<Help>
				Generic DelItem Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#Define the Command String
		CommandString="delete"

		#Init the ValueVariable
		OutputVariable=None

		#Refresh the IsDeletingBool
		self.IsDeletingBool=True

		#Sort the OrderString Calls
		for OrderString in ["Before","After"]:

			#Call the specific Function For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):

				#Is this Specific Method on ?
				if getattr(self,"TypeStringToIs"+SYS.CommandStringToCommandedStringDict[CommandString]+"BoolDict")[CalledTypeString]:

					#Call the MethodString
					HookMethodString=CommandString+OrderString+"BasesWith"+CalledTypeString
					if hasattr(self,HookMethodString):
						if HookMethodString not in self.FrozenMethodStringsList:
							OutputVariable=getattr(self,HookMethodString)(_KeyVariable)

					#Look if the Global IsBool is ok
					if self.IsDeletingBool==False:
						return 
				
			#Do the minimal delete Item
			if type(_KeyVariable) in [str,unicode]:
				if hasattr(self,_KeyVariable):
					self.__dict__.__delitem__(_KeyVariable)

	#Define a generic rename Method
	def rename(self,_OldKeyVariable,_NewKeyVariable,**_KwargsDict):
		'''
			<Help>
				Generic Rename Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#Define the Command String
		CommandString="rename"

		#Init the OutputVariable
		OutputVariable=None

		#Refresh the IsRenamingBool
		self.IsRenamingBool=True

		#Sort the OrderString Calls
		for OrderString in ["Before","After"]:

			#Call the specific Function For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):
				
				#Is this Specific Method on ?
				if getattr(self,"TypeStringToIs"+SYS.CommandStringToCommandedStringDict[CommandString]+"BoolDict")[CalledTypeString]:

					#Call the MethodString
					HookMethodString=CommandString+OrderString+"BasesWith"+CalledTypeString
					if hasattr(self,HookMethodString):
						if HookMethodString not in self.FrozenMethodStringsList:
							OutputVariable=getattr(self,HookMethodString)(_OldKeyVariable,_NewKeyVariable)

					#End the Setting
					if self.IsRenamingBool==False:
						return

					#Look if we have to modify the Item
					if OutputVariable!=None and type(OutputVariable)==tuple:
						_OldKeyVariable,_NewKeyVariable=OutputVariable
						
			#Do the minimal rename Item
			if type(_NewKeyVariable) in [str,unicode]:
				if type(_OldKeyVariable) in [str,unicode]:
					renameWithDict(self.__dict__,_OldKeyVariable,_NewKeyVariable)
					return 

	#Define a generic refresh Method
	"""
	def refresh(self,_KeyVariable,**_KwargsDict):
		'''
			<Help>
				Generic Refresh Method for calling the Hooked Methods to a certain set Item
			</Help>

			<Test>
			</Test>
		'''

		#Define the Command String
		CommandString="set"

		#Init the OutputVariable
		OutputVariable=None

		#Sort the OrderString Calls
		for OrderString in ["Before","After"]:

			#Call the specific Function For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):

				#Is this Specific Method on ?
				if all(map(lambda Item:getattr(Item[0],"TypeStringToIs"+SYS.CommandStringToCommandedStringDict[CommandString]+"Bool"+Item[1]+"Dict")[CalledTypeString],[(self,"Instance"),(self.__class__,"Class")])):

					#Look for Bindings
					HookString=CommandString+getKeyStringWithKeyVariable(_KeyVariable)+OrderString+"BasesWith"+CalledTypeString 
					if HookString in self.HookStringToMethodDict:
						if callable(self.HookStringToMethodDict[HookString]):
							self.HookStringToMethodDict[HookString]()
	"""

	#Define a generic setitem Method
	def __setitem__(self,_KeyVariable,_ValueVariable,**_KwargsDict):
		'''
			<Help>
				Generic Setitem Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#Define the Command String
		CommandString="set"

		#Init the OutputVariable
		OutputVariable=None

		#Reset the IsSettingBool
		self.IsSettingBool=True

		#Sort the HookMethodStrings
		for OrderString in ["Before","After"]:

			#Call the specific Function For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):

				#Is this Specific Method on ?
				if getattr(self,"TypeStringToIs"+SYS.CommandStringToCommandedStringDict[CommandString]+"BoolDict")[CalledTypeString]:

					#Call the MethodString
					HookMethodString=CommandString+OrderString+"BasesWith"+CalledTypeString
					if hasattr(self,HookMethodString):
						if HookMethodString not in self.FrozenMethodStringsList:
							OutputVariable=getattr(self,HookMethodString)(_KeyVariable,_ValueVariable)

					#End the Setting
					if self.IsSettingBool==False:
						return

					#Look if we have to modify the Item
					if OutputVariable!=None and type(OutputVariable)==tuple:
						_KeyVariable,_ValueVariable=OutputVariable

					#Look for BindingHookMethods
					BindingHookMethodString=CommandString+getKeyStringWithKeyVariable(_KeyVariable)+OrderString+"BasesWith"+CalledTypeString 
					if hasattr(self,BindingHookMethodString):
						if BindingHookMethodString not in self.FrozenMethodStringsList:
							if callable(getattr(self,BindingHookMethodString)):

								#Call the BindingHookMethodString
								OutputVariable=getattr(self,BindingHookMethodString)()

								#End the Setting
								if OutputVariable==(None,None):
									return

								#Look if we have to modify the Item
								if OutputVariable!=None and type(OutputVariable)==tuple:
									_KeyVariable,_ValueVariable=OutputVariable

			#Do the minimal setitem
			if OrderString=="Before":
				if type(_KeyVariable) in [str,unicode]:
					if len(_KeyVariable)>0:
						if _KeyVariable[0]!="x":
							self.__dict__[_KeyVariable]=_ValueVariable

	#Define a generic add Method
	def __add__(self,_AddedVariable,**_KwargsDict):
		'''
			<Help>
				Generic AddItem Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#Define the Command String
		CommandString="add"

		#Init the OutputVariable
		OutputVariable=None

		#Reset the IsAddingBool
		self.IsAddingBool=True

		#Sort the OrderString Calls
		for OrderString in ["Before","After"]:

			#Call the specific Function For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):

				#Is this Specific Method on ?
				if getattr(self,"TypeStringToIs"+SYS.CommandStringToCommandedStringDict[CommandString]+"BoolDict")[CalledTypeString]:

					#Call the MethodString
					HookMethodString=CommandString+OrderString+"BasesWith"+CalledTypeString
					if hasattr(self,HookMethodString):
						if HookMethodString not in self.FrozenMethodStringsList:
							OutputVariable=getattr(self,HookMethodString)(_AddedVariable)

					#End the Setting
					if self.IsAddingBool==False:
						break

					#Look if we have to modify the Item
					if OutputVariable!=None:
						_AddedVariable=OutputVariable

			#End the Setting
			if self.IsAddingBool==False:
				break

		#Return self to use it directly in one command line
		return self

	#Define a generic len Method
	def __len__(self):
		'''
			<Help>
				Generic len Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#Define the Command String
		CommandString="measure"

		#Init the OutputVariable and the MeasureInt
		OutputVariable=None
		_MeasureVariable=0

		#Sort the OrderString Calls
		for OrderString in ["Before","After"]:
		
			#Call the specific Before setItem For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):

				#Is this Specific Method on ?
				if getattr(self,"TypeStringToIs"+SYS.CommandStringToCommandedStringDict[CommandString]+"BoolDict")[CalledTypeString]:

					#Call the MethodString
					HookMethodString=CommandString+OrderString+"BasesWith"+CalledTypeString
					if hasattr(self,HookMethodString):
						if HookMethodString not in self.FrozenMethodStringsList:
							OutputVariable=getattr(self,HookMethodString)(_MeasureVariable)

					#Look if we have to modify the Item
					if OutputVariable!=None:
						_MeasureVariable=OutputVariable

					#End the Measure
					if self.IsMeasuringBool==False:
						return (int)(_MeasureVariable)

		#Reset the IsMeasuringBool
		self.IsMeasuringBool=True

		#Set the Transversal Bindings
		return (int)(_MeasureVariable)


	#Define a generic call Method
	def __call__(self,*_ArgsList,**_KwargsDict):
		'''
			<Help>
				Generic Call Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#Define the Command String
		CommandString="call"

		#Init the OutputVariable
		OutputVariable=None

		#Sort the OrderString Calls
		for OrderString in ["Before","After"]:
		
			#Call the specific Before setItem For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):

				#Is this Specific Method on ?
				if getattr(self,"TypeStringToIs"+SYS.CommandStringToCommandedStringDict[CommandString]+"BoolDict")[CalledTypeString]:

					#Call the MethodString
					HookMethodString=CommandString+OrderString+"BasesWith"+CalledTypeString
					if hasattr(self,HookMethodString):
						if HookMethodString not in self.FrozenMethodStringsList:
							OutputVariable=getattr(self,HookMethodString)(*_ArgsList,**_KwargsDict)

					#Modify the arguments
					if type(OutputVariable)==dict:
						if "_ArgsList" in OutputVariable:
							_ArgsList=OutputVariable["_ArgsList"]
						if "_KwargsDict" in OutputVariable:
							_KwargsDict=OutputVariable["_KwargsDict"]

					#End the Call
					if self.IsCallingBool==False:
						return OutputVariable

		#Reset the IsCallingBool
		self.IsCallingBool=True

		#Set the Transversal Bindings
		return OutputVariable

	def keys(self):
		'''
			<Help>
				Return the keys of the __dict__
			</Help>

			<Test>
			</Test>
		'''
		return self.__dict__.keys()

	def values(self):
		'''
			<Help>
				Return the values from the filtered keys
			</Help>

			<Test>
			</Test>
		'''
		return map(lambda KeyString: self.__dict__[KeyString],self.keys());

	def items(self):
		'''
			<Help>
				Return the items from the filtered keys
			</Help>

			<Test>
				#Load ShareYourSystem as SYS
				import ShareYourSystem as SYS
				#
				#Define a Dicter
				Dicter=SYS.DicterClass().update({'HumorString':"Happy"});
				#
				#Print the Items of the Dicter
				print("The Items of the Dicter are :");
				print(Dicter.items());
			</Test>
		'''
		return map(lambda KeyString: (KeyString,self.__dict__[KeyString]),self.keys())

	def __iter__(self):
		'''
			<Help>
				Iter on the selected Items 
			</Help>

			<Test>
				#Load ShareYourSystem as SYS
				import ShareYourSystem as SYS
				#
				#Define a Dicter
				Dicter=SYS.DicterClass().update({'HumorString':"Happy"});
				#
				#Print some examples
				print("HumorString is in the Dicter iter ?");
				print("HumorString" in Dicter);
				print("ColorString is in the Dicter iter ?");
				print("ColorString" in Dicter);
			</Test>
		'''

		#Init the IteratedList
		IteratedList=None

		#Init OutputList
		OutputList=None
		
		#Sort the OrderString Calls
		CommandString="iter"
		for OrderString in ["Before","After"]:

			#Call the specific Function For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):

				#Is this Specific Method on ?
				if getattr(self,"TypeStringToIs"+SYS.CommandStringToCommandedStringDict[CommandString]+"BoolDict")[CalledTypeString]:

					#Call the MethodString
					HookMethodString=CommandString+OrderString+"BasesWith"+CalledTypeString
					if hasattr(self,HookMethodString):
						if HookMethodString not in self.FrozenMethodStringsList:
							OutputList=getattr(self,HookMethodString)(IteratedList)

					#Look if we have to modify the Item
					if OutputList!=None and type(OutputList)==list:
						IteratedList=OutputList

					#Break the Loop if if the IsIteratingBool is False
					if self.IsIteratingBool==False:
						break

			#Break the Loop if if the IsIteratingBool is False
			if self.IsIteratingBool==False:
				break

		#Reset the IsIteratingBool
		self.IsIteratingBool=True

		#return the IteratedList or the minimal iter of the items() list
		if IteratedList!=None:
			return IteratedList.__iter__()
		else:
			return self.items().__iter__() 

	def update(self,_DictOrTuplesList):
		'''
			<Help>
				Each Item of the Dict are setted with 
				the specific __setitem__ of the Object
			</Help>

			<Test>
				#Get a Dicter Instance that is directly updated
				HappyDicter=SYS.DicterClass().update({'HumorString':"Happy"});
				#Print the HappyDicter
				print("The Happy Dicter __dict__ is :");
				print(HappyDicter.__dict__);
			</Test>
		'''

		
		#Check that it is a good format
		KeyVariableAndValueVariableTuple=None
		if hasattr(_DictOrTuplesList,"items"):
			KeyVariableAndValueVariableTuple=_DictOrTuplesList.items()
		else:
			try:
				Test=dict(_DictOrTuplesList)
				KeyVariableAndValueVariableTuple=_DictOrTuplesList
			except:
				print("WARNING : update : The format is not good for _DictOrTuplesList")

		if KeyVariableAndValueVariableTuple!=None:

			#Call the __setitem__ for each Item
			map(
				lambda KeyVariableAndValueVariableTuple: 
				self.__setitem__(KeyVariableAndValueVariableTuple[0],KeyVariableAndValueVariableTuple[1]),
				KeyVariableAndValueVariableTuple
				)
				
		#Return 
		return self

	def __repr__(self):
		'''
			<Help>
				Generic Repr Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#Define the Command String
		CommandString="repr"

		#Init the OutputVariable
		OutputVariable=None
		PrintedDict=self.__dict__
		SelfString=""
		
		#Sort the OrderString Calls
		for OrderString in ["Before","After"]:

			#Call the specific Function For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):

				#Is this Specific Method on ?
				if getattr(self,"TypeStringToIs"+SYS.CommandStringToCommandedStringDict[CommandString]+"BoolDict")[CalledTypeString]:

					#Call the MethodString
					HookMethodString=CommandString+OrderString+"BasesWith"+CalledTypeString
					if hasattr(self,HookMethodString):
						if HookMethodString not in self.FrozenMethodStringsList:
							OutputVariable=getattr(self,HookMethodString)(SelfString,PrintedDict)

					#Look if we have to modify the Item
					if OutputVariable!=None and type(OutputVariable)==tuple:
						SelfString,PrintedDict=OutputVariable

		#return the Print
		if SelfString!=None:
			return getPrintedPointerString(self)+SelfString
		else:
			return ""

	#def hdf5

#Define the TypeStringTo<> Dicts
LoadedVariableStringsList=[
							"BaseTypeStringsSetList",
							"BaseTypeStringsSetDict",
							"Class",
							"FirstBaseTypeStringsList",
							"FolderPathString",
							"SpecificKeyStringsList"
						]
for LoadedVariableString in LoadedVariableStringsList:
	setattr(sys.modules[__name__],"TypeStringTo"+LoadedVariableString+"LoadedDict",{})

#Define the FlagsDict
FlagsDict={
				'ExecFlag':{'BeginString':"<Exec>",'EndString':"</Exec>"},
				'FunctionHeaderFlag':{'BeginString':"\ndef ",'EndString':":\n"},
				'FunctionHelpFlag':{'BeginString':"<Help>",'EndString':"</Help>"},
				'FunctionNameFlag':{'BeginString':"def ",'EndString':"("},
				'FunctionTestFlag':{'BeginString':"<Test>",'EndString':"</Test>"},
				'FunctionFlag':{'BeginString':"def ",'EndString':"\ndef "},
				'PrintFlag':{'BeginString':"print(",'EndString':");\n",'RegBeginString':"print\(",'RegEndString':");\n"},
				'TestFlag':{'BeginString':"<Test>\n",'EndString':"</Test>"}
		 	}

#Define the SYSType Dicts
PythonTypeStringsList=["Class","Dict","Float","Function","Int","List","Module","Object"]
TypeStringToPythonType=dict(map(lambda TypeString:(TypeString,TypeString[0].lower()+TypeString[1:]),PythonTypeStringsList))
ParentTypeStringToChildTypeStringsListDict={
											'Dicter':["Dict","Instance"],
											'Instance':["Module","Class","Object"],
											'Object':["Node"],
											'Variable':["Dicter","Float","Int","List","String"]
										}
		
#Init the CommandString to the CommandedString Dict
CommandStringToCommandedStringDict={
										"add":"Added",
										"call":"Called",
										"delete":"Deleted",
										"init":"Initiated",
										"iter":"Iterated",
										"get":"Getted",
										"measure":"Measured",
										"rename":"Renamed",
										"repr":"Represented",
										"set":"Setted"
}
CommandStringToCommandingStringDict=dict(map(lambda Item:(Item[0],Item[1][:-2]+"ing"),CommandStringToCommandedStringDict.items()))


#Get the LocalFolderPathString
SitePackageSYSFolderPathString=getLocalFolderPathString()

#The SYS Module becomes a Wrapped Class Instance and other Modules refers to this Wrapped Instance.
sys.modules[__name__]=SYSClass(sys.modules[__name__])

#Define a Shortcut for ShareYourSystem
SYS=sys.modules[__name__]

#Init a Multiprocessing Pool to None
SYS.Module.Pool=None
SYS.Module.IsMultiprocessingBool=False
SYS.Module.ProcessesMaxInt=10

#The Personal 'MySYS' Folder PathString
MySYSFolderPathString=getMySYSFolderPathString()
InstallFolderPathString=MySYSFolderPathString+"Install/"+__name__+"/"
ModulesFolderPathString=MySYSFolderPathString+"Modules/"

#Init the Singular to Plural ExceptionNames Dict
SYS.Module.writePreVariableStringToPostVariableStringDict("SingularString",[__name__],"PluralStringInstalled",__name__+"s")

#Init the Singular to Plural ExceptionNames Dict
SYS.Module.writePreVariableStringToPostVariableStringDict("PluralString",[__name__+"s"],"SingularStringInstalled",__name__)

#Write/Update the AttributeStringToVariableStringDicts
SYS.Module.writePreVariableStringToPostVariableStringDict("TypeString",["Core"],"OrderedIntInstalled","0")

#update the TypeStringToBaseTypeStringsSetListLoadedDict
SYS.TypeStringToBaseTypeStringsSetListLoadedDict['Core']=[]

#Write/Update the AttributeStringToVariableStringDicts
SYS.Module.writePreVariableStringToPostVariableStringDict("AttributeString",getAttributeStringsListWithModule(sys.modules[__name__]),"TypeStringInstalled",__name__)

#Write/Update the TypeStringToFolderPathStringInstalledDict
SYS.Module.writePreVariableStringToPostVariableStringDict("TypeString",["ShareYourSystem"],"FolderPathStringInstalled",SitePackageSYSFolderPathString)

#Load already some modules in the Init Folder
SYS.Module.importModulesWithFolderPathStringAndIsParsedBool(SYS.MySYSFolderPathString+"Modules/Init/",True)

#Do an Instance Test of the CoreClass
#SYS.CoreClass()

