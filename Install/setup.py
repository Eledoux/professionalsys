#Define Python Modules
from distutils.core import setup
import os

#Execute a Setup
setup(
		author='Erwan Ledoux',
		author_email='erwan.ledoux@ens.fr',
		name='ShareYourSystem',
		packages=[
			 		'ShareYourSystem',
			 		'ShareYourSystem.Object',
			 		'ShareYourSystem.Object.Initiater',
			 		'ShareYourSystem.Object.Representer'
      	],
      	package_data={
          'ShareYourSystem':[
          		'Package.json'
          ]
		},
		url='http://shareyoursystem.ouvaton.org',
		version='1.0',
      )

