######################################################################
#Define Python modules

import collections
import copy
import functools
#import imp
import importlib
import inspect
import json
import itertools
import multiprocessing
import numpy
import operator
import os
import re
import unittest
import sys
import six
import tables

######################################################################
#Define functions that build the SYS framework

def filter_(_Function,_List):
	if sys.version[0]==2:
		return filter(_Function,_List)
	else:
		return list(filter(_Function,_List))

def range_(*ArgsList):
	if sys.version[0]==2:
		return range(*ArgsList).__iter__()
	else:
		return range(*ArgsList)

def map_(_Function,_List):
	if sys.version[0]==2:
		map(_Function,_List)
	else:
		return list(map(_Function,_List))

def getIsInBool(_ContainedVariable,_ContainingVariable):
	return operator.__contains__(_ContainingVariable,_ContainedVariable)

def getIsInListBool(_ContainedVariable,_ContainingList):

	#Debug
	'''
	print('getIsInListBool function')
	print('_ContainedVariable is ',_ContainedVariable)
	print('_ContainingList is ',_ContainingList)
	print('')
	'''

	return any(
				map(
					lambda __ListedVariable:
					getIsEqualBool(_ContainedVariable, __ListedVariable),
					_ContainingList
				)
			)

def getDebuggedStringWithDebuggingVariable(_DebuggingVariable):

	#Set the DebuggedString with the _DebuggingVariable
	if type(_DebuggingVariable)==str:
		return _DebuggingVariable
	elif type(_DebuggingVariable)==tuple:
		return '\n'.join(
						map(
									lambda __KeyString:
									_DebuggingVariable[0]+__KeyString+' is '+SYS.represent(
										_DebuggingVariable[1][__KeyString]),
									_DebuggingVariable[2]
							)
						)

	elif type(_DebuggingVariable)==list:
		return '\n'.join(map(getDebuggedStringWithDebuggingVariable,_DebuggingVariable))

def getUnSerializedTuple(_Variable,_SerializedList):
	return tuple([_SerializedList[0],type(_Variable[_SerializedList[0]])(_SerializedList[1])])

def where(_RowingDictsList,_FilteringTuplesList,**_KwargsDict):


	if 'IsInCheckingBool' in _KwargsDict and _KwargsDict['IsInCheckingBool']:

		return map(
						lambda __RowingDict:
						#Say True for the ones that respect all the conditions
						__RowingDict
						if all(
							map(
								lambda __FilteringTuple:
								__FilteringTuple[1][0](__RowingDict[__FilteringTuple[0]],__FilteringTuple[1][1])
								#Check that the Key exists...Can maybe make slower the process....
								if __FilteringTuple[0] in __RowingDict else False
								,_FilteringTuplesList
								)
							)
						#Set None either
						else None,
						_RowingDictsList
					)

	else:

		return map(
						lambda __RowingDict:
						#Say True for the ones that respect all the conditions
						__RowingDict
						if all(
							map(
								lambda __FilteringTuple:
								__FilteringTuple[1][0](__RowingDict[__FilteringTuple[0]],__FilteringTuple[1][1])
								,_FilteringTuplesList
								)
							)
						#Set None either
						else None,
						_RowingDictsList
					)

def getRowedDictsListWithTable(_Table):
	return map(
			lambda __Row:
			dict(
				zip(
					_Table.colnames,
					map(
						lambda __ColumnString:
						__Row[__ColumnString],
						_Table.colnames
					)
				)
			),
			_Table.iterrows()
		)

'''
def getJoinedRowedDictsList(_RowedDictsList):
	return map(
				lambda __RowedDict:
				dict(
						__RowedDict,
						**{
							'JoinedList':[
										__RowedDict['TabularedInt'],
										__RowedDict['ModeledInt']
									]
						}
					),
				_RowedDictsList
			)
'''

def getJoinedRowedDictsListWithJoinedListKeyString(_RowedDictsList,_JoinedListKeyString):
	map(
			lambda __RowedDict:
			__RowedDict.__setitem__(
						_JoinedListKeyString,
						[
							__RowedDict['TabularedInt'],
							__RowedDict['ModeledInt']
						]
			) if __RowedDict!=None else None,
			_RowedDictsList
	)
	return _RowedDictsList


def getSortedDictWithSortedTuplesList(_SortedTuplesList):
	SortedDict={}
	map(	
			lambda __ItemTuple:
			SortedDict[__ItemTuple[0]].append(__ItemTuple[1]) if __ItemTuple[0] in SortedDict
			else SortedDict.__setitem__(__ItemTuple[0],[__ItemTuple[1]]),
			_SortedTuplesList
		)
	return SortedDict

def get(_Dict,_DictString,_IterInt):
	Iter=getattr(_Dict,'iter'+_DictString)()
	for Int in range_(_IterInt):
		Iter.next()
	return Iter.next()
	
def getCurrentFolderPathString():
	return SYS.sys.modules['os'].getcwd()+'/'

def writeTestWithFileStringAndTestString(_FileString,_TestString):
	File=open(getCurrentFolderPathString()+_FileString+'.txt','w')
	
	File.write(TestString)
	SYS.RepresentingDict['IsIdBool']=True
	File.close()

def _map(_Function,_List):

	#Mapping with multiprocessing or not
	if SYS.IsMultiprocessingBool:
		if SYS.Module.Pool==None:
			SYS.Module.Pool=multiprocessing.Pool(processes=SYS.ProcessesMaxInt)
		return SYS.Module.Pool.map(_Function,_List)

	#Else return map default
	return map(_Function,_List)

def getLocalFolderPathString():
	'''
		<Help>
			Get the Name of the actual Script that is readed by Python
		</Help>
	
		<Test>
			#Print the LocalFolderPathString
			print(SYS.getLocalFolderPathString())
		</Test>
	'''
	#return '/'.join(inspect.currentframe().f_back.f_code.co_filename.split('/')[:-1])+"/"
	return '/'.join(__file__.split('/')[:-1])+"/"

def getWordStringsListWithString(_String):
	'''
		<Help>
			Split a String into its Words
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS

			#Print some examples
			print("The Words in the String SuperMan are :"+str(SYS.getWordStringsListWithString("SuperMan")));
			print("The Words in the String \"FeelLike-A-RainBow\" are :"+str(SYS.getWordStringsListWithString("FeelLike-A-RainBow")));
			print("The Words in the String \"MySYSHopeItsWorkingHELLO\" are :"+str(SYS.getWordStringsListWithString("MySYSHopeItsWorkingHELLO")));
		</Test>	
	'''
	
	#Get all the Strings with an upper char at the first index
	if type(_String) in StringTypesList:
		StringsList=filter_(None,sys.modules['re'].split("([A-Z][^A-Z]*)",_String))

		#Join the abreviated Strings
		if len(StringsList)>1:

			WordsInt=0
			PreviousString=StringsList[WordsInt]
			while WordsInt<len(StringsList)-1:
			
				#Increment the Word
				WordsInt+=1
				
				#Define the Next String
				NextString=StringsList[WordsInt]
			
				#Check for Upper Abreviations
				if len(NextString)==1:
					if (all(map(lambda Char:Char.upper()==Char,list(PreviousString)))) and (NextString.upper()==NextString):
					
						#Join Previous and NextString
						StringsList[WordsInt-1]=PreviousString+NextString

						#Remove the NextString
						StringsList.pop(WordsInt)
				
						#Increment
						WordsInt-=1

				#Shift the Previous String
				if WordsInt<len(StringsList):
					PreviousString=StringsList[WordsInt]

		#Return StringsList
		return StringsList

	#Return [] by default
	else:
		return [""]

def getSYSFolderPathString():
	'''
		<Help>
			Get the SYSFolderPathString from the os cwd
		</Help>
	
		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Print the SYSFolderPathString
			print("The SYSFolder is at : "+SYS.getSYSFolderPathString())
		</Test>
	'''
	
	#Get the cwd
	CurrentWorkDirectoryPathString=sys.modules['os'].getcwd()
	
	#Get the FolderStringsList
	FolderStringsList=CurrentWorkDirectoryPathString.split('/')
	
	#Init SYSFolderPathString
	SYSFolderPathString=""
	
	#Scan each FolderPathString and find the one with "SYS" in the end
	for FolderString in FolderStringsList:
		SYSFolderPathString+=FolderString+"/";
		WordStringsList=getWordStringsListWithString(FolderString)
		if len(WordStringsList)>0:
			if WordStringsList[-1]=="SYS":
				break;

	#Return
	return SYSFolderPathString

def writeUpdatedDictWithUpdatingDict(_DictPathString,_Dict,_UpdatingDict):

	#Update the AttributeStringToModuleStringDict
	IsUpdatedBool=False
	for KeyString,ValueVariable in _UpdatingDict.items():

		if KeyString not in _Dict:

			#Look if it is a new Key
			IsUpdatedBool=True
			_Dict[KeyString]=ValueVariable

		elif _Dict[KeyString]!=ValueVariable:
			
			#Look if it is an updated Key
			IsUpdatedBool=True
			_Dict[KeyString]=ValueVariable

	#Rewrite the FunctionStringToTypeStringDict if there was at least one change
	if IsUpdatedBool==True:
		DictFile=open(_DictPathString,'w')
		json.dump(_Dict,DictFile,indent=2,sort_keys=True)
		DictFile.close()

def writePreVariableStringToPostVariableStringDict(_PreVariableString,_KeyStringsList,_PostVariableString,_ValueString):

	#Define the DictString
	DictTypeString=_PreVariableString+"To"+_PostVariableString+"Dict";

	#Define the DictPathString
	DictPathTypeString=DictTypeString+"PathString"

	#Set the DictFolderPathString
	setattr(SYS,DictPathTypeString,SYS.ModulesFolderPathString+DictTypeString+".json")

	#Init an empty json if there is not one
	if sys.modules['os'].path.isfile(getattr(SYS,DictPathTypeString))==False:

		#Define an default empty Dict
		setattr(SYS,DictTypeString,{})

		#Write a Default empty json dict
		sys.modules['json'].dump(getattr(SYS,DictTypeString),open(getattr(SYS,DictPathTypeString),'w'))
	
	#Get the already one
	setattr(
				SYS,
				DictTypeString,
				sys.modules['json'].load(open(getattr(SYS,DictPathTypeString),'r'))
			)

	#Update the actual setted Attributes
	SYS.writeUpdatedDictWithUpdatingDict(
										getattr(SYS,DictPathTypeString),
										getattr(SYS,DictTypeString),
										dict(
											map(
												lambda KeyString:(KeyString,_ValueString),
												_KeyStringsList
												)
											)
									)

def getAttributeStringsListWithModule(_Module):
	'''
		<Help>
			Get the Name of the Attributes of a Module
		</Help>

		<Test>
			#Print the AttirbuteStrings of the Sys
			print("The AttributeStrings in the module Sys are : ");
			print(Sys.getAttributeStringsListWithModule(Sys));
		</Test>
	'''
	
	return map(lambda Item: Item[1].__name__ if hasattr(Item[1],'__name__') else Item[0],filter_(lambda Item:type(Item[1]).__name__=="function" or Item[0][0].upper()==Item[0][0],_Module.__dict__.items()))

def getTypeStringWithFolderPathString(_FolderPathString):
	'''
		<Help>
			Get the ScriptStringAndScriptPathString from a FolderPathString
			FolderPathString has to present the TypeString in its name after the last "_"
		</Help>

		<Test>
		</Test>
	'''

	#Get the TypeString from the FolderPathStringsList
	FolderPathStringsList=_FolderPathString.split("/")[-2].split("_")
	if len(FolderPathStringsList)>1:
		TypeString="_".join(FolderPathStringsList[1:])
	else:
		TypeString=_FolderPathString.split("/")[-2]
	#Look for the ScriptPathString
	ScriptString=TypeString+".py"
	ScriptPathString=_FolderPathString+ScriptString
	if os.path.isfile(ScriptPathString):
		return TypeString

def getClassStringWithTypeString(_TypeString):
	'''
		<Help>
			Get the ClassString With the TypeString
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Print the ClassString of the Node Object
			print("ClassString of the Node Object is :");
			print(SYS.getClassStringWithTypeString("Node"));
		</Test>
	'''

	#Return 
	return _TypeString+"Class"

def getTypeStringWithClassString(_TypeString):
	'''
		<Help>
			Get the TypeString With the ClassString
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Print the ClassString of the Node Object
			print("TypeString of the Node ClasString is :");
			print(SYS.getTypeStringWithClassString(SYS.NodeClass.__name__));
		</Test>
	'''

	#Return 
	return ''.join(SYS.getWordStringsListWithString(_TypeString)[:-1])

def getLocalDictWithKwargsDictAndDefaultItemsDict(_KwargsDict,_DefaultItemsDict):
	'''
		<Help>
			Filter a Kwargs dict with Default possible Items
		</Help>
	
		<Test>
			#Define a Kwargs
			Kwargs={'Thing':0,'Stuff':1}
			#
			#Define a DefaultItemsDict
			DefaultItemsDict={'Thing':None,'OtherStuff':0}
			#
			#Get the LocalDict
			print(_.getLocalDictWithKwargsAndDefaultItemsDict(Kwargs,DefaultItemsDict))
		</Test>
	'''
	
	#Init skeleton
	LocalDict={}
	
	#Accumulate Kwargs
	for Key,DefaultValue in _DefaultItemsDict.items():
		LocalDict[Key]=_Kwargs[Key] if Key in _KwargsDict else DefaultValue
	
	#Return
	return LocalDict

def getChildStringsListWithParentStringToChildStringsListDictAndParentString(_ParentStringToChildStringsListDict,_ParentString,**_KwargsDict):
	'''
		<Help>
			Get all the repeated BaseNames but aligned...... in one dimension
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Invent Fictif relationships
			SYS.DictionnaryFirstBaseTypeStringsList=["SYS"];
			SYS.ParentFirstBaseTypeStringsList=["Dictionnary"];
			SYS.DirFirstBaseTypeStringsList=["SYS"];
			SYS.ScriptFirstBaseTypeStringsList=["Dir","Parent"];
			#
			#Print the BasesList of the Script Object
			print("BaseTypeStringsList of the Script is : "+str(SYS.getBaseTypeStringsListWithString("Script")));
		</Test>
	'''
	
	#Set possible default local Items
	LocalDict=SYS.getLocalDictWithKwargsDictAndDefaultItemsDict(_KwargsDict,
				{
					'ChildStringsList':[]
				 }
				)
	
	#Look if there is this Key in this layer
	if _ParentString in _ParentStringToChildStringsListDict:
		
		#Add to the 'ChildStringsList'
		LocalDict['ChildStringsList']+=_ParentStringToChildStringsListDict[_ParentString]
	
		#Look to younger Children
		for ChildStringInt,ChildString in enumerate(_ParentStringToChildStringsListDict[_ParentString]):
			
			#If this Children have also Children
			if ChildString in _ParentStringToChildStringsListDict:
			
				#Recursive Call
				LocalDict['ChildStringsList']+=getChildStringsListWithParentStringToChildStringsListDictAndParentString(_ParentStringToChildStringsListDict,ChildString)

	#Return the cumulated BaseNames
	return LocalDict['ChildStringsList']

def getChildStringsSetListWithParentStringToChildStringsListDictAndStringAndParentString(_ParentStringToChildStringsListDict,_ParentString):
	'''
		<Help>
			Get the set of Bases from the list
		</Help>

		</Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Invent Fictif relationships
			SYS.DictionnaryFirstBaseTypeStringsList=["SYS"];
			SYS.ParentFirstBaseTypeStringsList=["Dictionnary"];
			SYS.DirFirstBaseTypeStringsList=["SYS"];
			SYS.ScriptFirstBaseTypeStringsList=["Dir","Parent"];
			#
			#Print the BaseTypeStringsOrderedSet of the Script Object
			print("BaseTypeStringsOrderedSet of the Script is : "+str(SYS.getBaseTypeStringsOrderedSetWithString("Script")));
		</Test>
	'''
	
	#Get the BaseNamesList
	ChildStringsList=getChildStringsListWithParentStringToChildStringsListDictAndParentString(_ParentStringToChildStringsListDict,_ParentString);
	
	#Init the BasesOrderSet
	ChildStringsSetList=[]
	
	#Scan among the Children
	for ChildString in ChildStringsList:
		if ChildString in ChildStringsSetList:
		
			#Get the Idx
			ChildInt=ChildStringsSetList.index(ChildString)
			
			#Delete at this Idx
			ChildStringsSetList.pop(ChildInt)
			
		#Put it in last
		ChildStringsSetList.append(ChildString)

	#Return
	return ChildStringsSetList

def getChildStringsDictWithParentStringToChildStringsListDictAndParentString(_ParentStringToChildStringsListDict,_ParentString,**_KwargsDict):
	'''
		<Help>
			Get all the Children with a Dict hierarchy
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Print the ParentTypeStringToChildTypeStringsListDict
			print("ParentTypeStringToChildTypeStringsListDict is :");
			print(SYS.ParentTypeStringToChildTypeStringsListDict);
			#
			#Print the Listed Children
			print("\nListed Children are : ");
			print(SYS.getChildStringsListWithParentStringToChildStringsListDictAndString(SYS.ParentTypeStringToChildTypeStringsListDict,'Variable'));
		</Test>
	'''
	
	#Set possible default local Items
	LocalDict=SYS.getLocalDictWithKwargsDictAndDefaultItemsDict(_KwargsDict,
				{
					'ChildStringsDict':[],
				 }
				)
	
	#Look if there is this Key in this layer
	if _ParentString in _ParentStringToChildStringsListDict:
		
		#Add to the 'ChildStringsList'
		LocalDict['ChildStringsDict']+=_ParentStringToChildStringsListDict[_ParentString]
	
		#Look to younger Children
		for ChildStringInt,ChildString in enumerate(_ParentStringToChildStringsListDict[_ParentString]):
			
			#If this Children have also Children
			if ChildString in _ParentStringToChildStringsListDict:
			
				#Recursive Call
				LocalDict['ChildStringsDict'][ChildStringInt]=dict([(ChildString,getChildStringsDictWithParentStringToChildStringsListDictAndParentString(_ParentStringToChildStringsListDict,ChildString))])

	#Return the cumulated BaseNames
	return LocalDict['ChildStringsDict']

def getFunctionsListWithModule(_Module):
	'''
		<Help>
			Get the getDone of a module
		</Help>
	
		<Test>
			#Print the Attirbutes of the Sys
			print("The Functions in the module _ are : ");
			print(Sys.getFunctionsListWithModule(Sys));
		</Test>
	'''
	return filter_(lambda Value:type(Value).__name__=="function",_Module.__dict__.values())

def getAttributeStringsListWithModule(_Module):
	'''
		<Help>
			Get the Name of the Attributes of a Module
		</Help>

		<Test>
			#Print the AttirbuteStrings of the Sys
			print("The AttributeStrings in the module Sys are : ");
			print(Sys.getAttributeStringsListWithModule(Sys));
		</Test>
	'''
	
	return map(lambda Item: Item[1].__name__ if hasattr(Item[1],'__name__') else Item[0],filter_(lambda Item:type(Item[1]).__name__=="function" or Item[0][0].upper()==Item[0][0],_Module.__dict__.items()))

def setHookingMethodStringToMethodsListDictWithClass(_Class):
	_Class.HookingMethodStringsList=SYS.getHookingMethodStringsListWithClass(_Class)
	_Class.BeforeHookingMethodStringsList,_Class.AfterHookingMethodStringsList=groupby(
			lambda HookingMethodString:"Before" in HookingMethodString,
			_Class.HookingMethodStringsList
			)
	_Class.HookingMethodStringToMethodsListDict=getHookingMethodStringToMethodsListWithClass(_Class)

def setBindingMethodStringToMethodsListDictWithClass(_Class):
	_Class.BindingMethodStringToMethodsListDict=getBindingMethodStringToMethodsListDictWithClass(_Class)
	_Class.BindingMethodStringToMethodTuplesListDict={}
	map(
			lambda BindingMethodString:
			insertBindingMethodsWithBindingMethodStringAndClass(
				BindingMethodString,
				_Class),
			_Class.BindingMethodStringToMethodsListDict.keys()
	)

def setClassWithClass(_Class):

	#Give a pointer to the module

	#Define the BeforeClassesList and AfterClassesList
	_Class.BeforeClassesList=list(_Class.__mro__[:-1])
	_Class.AfterClassesList=list(sys.modules['copy'].copy(_Class.BeforeClassesList))
	_Class.AfterClassesList.reverse()

	#Look for HookingMethodStringsList and BindingMethodStringsList
	setHookingMethodStringToMethodsListDictWithClass(_Class)
	setBindingMethodStringToMethodsListDictWithClass(_Class)

	#Set the args of the methods
	SYS.setArgsDictWithClass(_Class)

	#Set the TypeString
	_Class.TypeString=getTypeStringWithClassString(_Class.__name__)

	#Look for specific Dict
	_Class.SpecificKeyStringsList=[]

	#Define the SourceString
	SourceString=inspect.getsource(_Class)
	
	#Check that there is a '<DefineSpecificDict>' part
	_Class.SpecificKeyStringsList=[]
	_Class.RepresentingNotGettingStringsList=[]
	if '<DefineSpecificDict>' in SourceString:

		#Define the DefineSpecificDictString
		DefineSpecificDictString=getStringsListWithBeginStringAndEndStringAndStringsIntAndString(
							'<DefineSpecificDict>',
							'</DefineSpecificDict>',
							1,
							SourceString
						)[0]

		#Check that there is at least one definition
		if 'self.' in DefineSpecificDictString:
			_Class.SpecificKeyStringsList=map(
				lambda _KeyString:
				_KeyString.split('=')[0],
				filter_(
						lambda _ExpressionString:
							"=" in _ExpressionString and _ExpressionString[0] not in ['\n'],
							DefineSpecificDictString.split('self.')
						)
				)

			_Class.RepresentingNotGettingStringsList=map(
				lambda _KeyString:
				_KeyString.split('=')[0],
				filter_(
						lambda _ExpressionString:
							"<NotRepresented>" in _ExpressionString and "=" in _ExpressionString and _ExpressionString[0] not in ['\n'],
							DefineSpecificDictString.split('self.')
						)
				)


def getTableClassStringWithTypeString(_TypeString):
	return _TypeString+'TableClass'

def importModulesWithTypeStringAndFolderPathString(_TypeString,_FolderPathString):

	#Debug
	'''
	print('importModulesWithTypeString')
	print('_TypeString is ',_TypeString)
	print('')
	'''

	#Load the module
	importlib.import_module(_TypeString,_FolderPathString)
	#imp.load_source(_TypeString,_FolderPathString)
	
	#Build a short alias for the module
	Module=sys.modules[_TypeString]

	#do a direct Link of <TypeString> Module to the SYS
	setattr(SYS.Module,_TypeString,Module)

	#Do a direct Link of the <TypeString>Class if it exists to the SYS scope
	ClassString=getClassStringWithTypeString(_TypeString)

	#Debug
	'''
	print('Module is ',Module)
	print(ClassString,hasattr(Module,ClassString))
	print('')
	'''

	#Check
	if hasattr(Module,ClassString):

		#Set the link
		setattr(SYS.Module,ClassString,getattr(Module,ClassString))
		
		#Set the TypeStringToFirstBaseTypeStringsListLoadedDict for this Class
		SYS.TypeStringToFirstBaseTypeStringsListLoadedDict[_TypeString]=map(lambda Base:getTypeStringWithClassString(Base.__name__),getattr(sys.modules[_TypeString],ClassString).__bases__)
	
	#Update the TypeStringToBaseTypeStringsSetListLoadedDict
	if _TypeString not in SYS.TypeStringToBaseTypeStringsSetListLoadedDict:
		SYS.TypeStringToBaseTypeStringsSetListLoadedDict[_TypeString]=getChildStringsSetListWithParentStringToChildStringsListDictAndStringAndParentString(SYS.TypeStringToFirstBaseTypeStringsListLoadedDict,_TypeString)

	#Update the TypeStringToBaseTypeStringsSetDictLoadedDict
	if _TypeString not in SYS.TypeStringToBaseTypeStringsSetDictLoadedDict:
		SYS.TypeStringToBaseTypeStringsSetDictLoadedDict[_TypeString]=getChildStringsDictWithParentStringToChildStringsListDictAndParentString(
						SYS.TypeStringToFirstBaseTypeStringsListLoadedDict,_TypeString)

	#Look if the TypeString has a particular PluralString
	if hasattr(Module,"PluralString"):
		PluralString=getattr(Module,"PluralString")
	else:
		PluralString=_TypeString+'s'
	writePreVariableStringToPostVariableStringDict("SingularString",[_TypeString],"PluralStringInstalled",PluralString)
	writePreVariableStringToPostVariableStringDict("PluralString",[PluralString],"SingularStringInstalled",_TypeString)

	#Write/Update the OrdedIntToTypeStringInstalledDict	
	if SYS.TypeStringToOrderedIntInstalledDict==None:
		SYS.TypeStringToOrderedIntInstalledDict={}
	TypeStringInt=str(len(SYS.TypeStringToOrderedIntInstalledDict))
	if len(SYS.TypeStringToOrderedIntInstalledDict)<10:
		TypeStringInt="0"+TypeStringInt
	if _TypeString not in SYS.TypeStringToOrderedIntInstalledDict:
		writePreVariableStringToPostVariableStringDict("TypeString",[_TypeString],"OrderedIntInstalled",TypeStringInt)

	#Look in the possible associated Class
	if hasattr(Module,ClassString):	

		#Set the Class
		setClassWithClass(getattr(Module,ClassString))

	#Get all the AttestingFunctionStringsList
	try:
		Module.AttestingFunctionStringsList=SYS.getStringsListWithBeginStringAndEndStringAndStringsIntAndString(
			'\ndef ','():',
			"All",
			SYS.getStringsListWithBeginStringAndEndStringAndStringsIntAndString(
						'<DefineAttestingFunctions>',
						'</DefineAttestingFunctions>',
						1,
						inspect.getsource(Module)
		)[0])
		#Module.AttestingFunctionsList=map(
		#									lambda __AttestingFunctionString:
		#									getattr(Module,__AttestingFunctionString),
		#									Module.AttestingFunctionStringsList
		#								)
	except:
		Module.AttestingFunctionStringsList=[]

	'''
	#Look for TabularingTuplesList that defines TableClasses
	if hasattr(Module,'TabularingTuplesList')==False:
		Module.TabularingTuplesList=[]

	Module.TabularingDict={}
	Module.TableClassesDict={}
	for TabularingTuple in Module.TabularingTuplesList:

		#Init at the [TabularingTuple[0]
		Module.TabularingDict[TabularingTuple[0]]={}

		#Define a short alias
		ModelingDict=Module.TabularingDict[TabularingTuple[0]]

		#Listify the Module.ModelingTuplesList
		ModelingDict.update(
			zip(
				[
						'ColumningStringsList',
						'GettingStringsList',
						'ColsList',
						'ScanningListsList'
				],
				zip(*TabularingTuple[1])
				)
			)

		#Remove the Scanning with None or []
		ModelingDict['RealScanningIndexIntsList']=map(
				lambda __IntAndScanningList:
				__IntAndScanningList[0],
				filter_(
					lambda __IntAndScanningList:
					type(__IntAndScanningList[1])==list and len(__IntAndScanningList[1])>0,
					enumerate(ModelingDict['ScanningListsList'])
				)
			)
		ModelingDict['RealScanningColumningStringsList']=map(
							ModelingDict['ColumningStringsList'].__getitem__,
							ModelingDict['RealScanningIndexIntsList']
						)
		ModelingDict['RealScanningGettingStringsList']=map(
							ModelingDict['GettingStringsList'].__getitem__,
							ModelingDict['RealScanningIndexIntsList']
						)
		ModelingDict['RealScanningListsList']=map(
							ModelingDict['ScanningListsList'].__getitem__,
							ModelingDict['RealScanningIndexIntsList']
						)

		#Define the class
		class LocalTableClass(tables.IsDescription):

			#Add an tagged Int (just like a unique KEY in mysql...) 
			locals().__setitem__(TabularingTuple[0].split('Table')[0]+'Int',tables.Int64Col())

			#Get the OutputingKeyStringsList for making them like a IsDescription Type
			for ColumningString,Col in zip(
								ModelingDict['ColumningStringsList'],
								ModelingDict['ColsList']
							):

				#Set the Col 
				locals().__setitem__(ColumningString,Col)
				
			#Del ColumningString,ColClass
			try:
				del ColumningString,Col
			except:
				pass

		#Give a name of this local defined class
		LocalTableClass.__name__=SYS.getClassStringWithTypeString(TabularingTuple[0])

		#Set in the TableClassesDict
		Module.TableClassesDict[LocalTableClass.__name__]=LocalTableClass
		setattr(SYS,LocalTableClass.__name__,LocalTableClass)
		'''
		
	#Look for other Tables
	''' 
	OtherTableClassStringsList=filter_(lambda __AttributeString:'TableClass' in __AttributeString,dir(Module))
	map(
			lambda __OtherTableClassString:
			setattr(SYS,__OtherTableClassString,getattr(Module,__OtherTableClassString)),
			OtherTableClassStringsList
		)
	Module.TableClassesDict=dict(
				map(
					lambda __OtherTableClassString:
					(__OtherTableClassString,getattr(Module,__OtherTableClassString)),
					OtherTableClassStringsList
				)+[
					(LocalTableClass.__name__,LocalTableClass)
				]
			)
	#Set to the Module and SYS
	setattr(Module,TableClassString,LocalTableClass)
	setattr(SYS,TableClassString,LocalTableClass)
	'''

def getMroKeyStringsListWithClass(_Class):
	return list(
				itertools.chain(*filter_(
					lambda SpecificKeyStringsList:
					SpecificKeyStringsList!=None,
					map(
						lambda _MroClass:
						_MroClass.SpecificKeyStringsList 
						if hasattr(_MroClass,'SpecificKeyStringsList')
						else None,
						_Class.__mro__
						)
					)
				)
			)
			

def importModulesWithFolderPathStringAndIsParsedBool(_FolderPathString,_IsParsedBool):
	'''
		<Help>
			Load the PackageDict SYS.TypeStringToPackageDictLoadedDict
		</Help>
	
		<Test>
		</Test>
	'''

	#Get the ScriptPathString if it exists
	TypeString=getTypeStringWithFolderPathString(_FolderPathString)
	if TypeString!=None:
		
		#Load the Path
		sys.path.append(_FolderPathString)
		
		#Load the Modules
		importModulesWithTypeStringAndFolderPathString(TypeString,_FolderPathString)
	
	#Parse if it is sayed
	if _IsParsedBool:

		#Define the ModulesPathString
		ModulesPathString=_FolderPathString+"Modules/"

		#Look for deeper Modules to install
		if os.path.isdir(ModulesPathString):
	
			#Debug
			'''
			print(sorted(os.listdir(ModulesPathString)))
			print('')
			'''

			#Look for deeper Objects
			for FolderString in sorted(os.listdir(ModulesPathString)):

				# _ in the front of a FolderTypeString is avoided
				if FolderString[0]!="_":

					#Define the SubFolderPathString 
					SubFolderPathString=ModulesPathString+FolderString+"/"
					if len(SubFolderPathString)>0:

						#Debug
						'''
						print(SubFolderPathString,os.path.isdir(SubFolderPathString))
						print('')
						'''

						if os.path.isdir(SubFolderPathString):
							SYS.importModulesWithFolderPathStringAndIsParsedBool(SubFolderPathString,True)

def getCommonPrefixStringWithStringsList(_StringsList):
	'''
		<Help>
			Find the longest string that is a prefix of all the strings
		</Help>

		<Test>
			#Load the SpecificModule
			SYS.importModuleWithTypeString("Reader");

			#print the CommonStringPrefixString of two Strings
			print("Prefix shared between \"We share this in common\" and \"We share this not in common\" is :");
			print(SYS.getCommonPrefixStringWithStringsList(["We share this in common","We share this not in common"]));</Test>
		</Test>
	'''
	if not _StringsList:
		return ""
	PrefixString=_StringsList[0]
	for String in _StringsList:
		if len(String)<len(PrefixString):
			PrefixString=PrefixString[:len(String)]
		if not PrefixString:
			return ""
		for Int in range_(len(PrefixString)):
			if PrefixString[Int] != String[Int]:
				PrefixString=PrefixString[:Int]
				break
	return PrefixString

def getCommonSuffixStringWithStringsList(_StringsList):
	'''
		<Help>
			Find the longest string that is a suffix of all the strings
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS

			#print the CommonStringSuffixString of two Strings
			print("Suffix shared between \"We share this in common\" and \"We share this not in common\" is :");
			print(SYS.getCommonSuffixStringWithStringsList(["We share this in common","We share this not in common"]));
		</Test>
	'''

	if not _StringsList:
		return ""
	SuffixString=_StringsList[0]
	for String in _StringsList[1:]:
		if len(SuffixString)>len(String):
			SuffixString=SuffixString[-len(String):]
		if not SuffixString:
			return ""
		for Int in range_(1,len(SuffixString)):
			if SuffixString[-Int] != String[-Int]:
				if Int==1:
					SuffixString=""
				else:
					SuffixString=SuffixString[-Int+1:]
				break
	return SuffixString

def getBasesListWithClass(_Class,**_KwargsDict):
	'''
		<Help>
			Get all the Bases for a Class
		</Help>

		<Test>
		</Test>	
	'''
	
	#Set possible default local Items
	LocalDict=getLocalDictWithKwargsDictAndDefaultItemsDict(_KwargsDict,
				{
					'BasesList':[],
				 }
				);
	
	#Check that there are bases
	if hasattr(_Class,'__bases__'):
		
		#Scan the Base
		for Base in _Class.__bases__: 

			#Get the Base
			LocalDict['BasesList']+=[Base];
			
			#Look inside if there is a Base
			if hasattr(LocalDict['BasesList'][-1],'__bases__'):
				if len(LocalDict['BasesList'][-1].__bases__)>0:
					LocalDict['BasesList']+=getBasesListWithClass(Base);
			
	#Return the cumulated Bases
	return LocalDict['BasesList'];

def getStringsListWithBeginStringAndEndStringAndStringsIntAndString(
		_BeginString,_EndString,_StringsInt,_String,**Kwargs):
	
	#Set the PickedStringsList
	PickedStringsList=[]
	
	#CountsInt
	if _StringsInt=="All":
		CountsInt=1
	else:
		CountsInt=_StringsInt

	#Scan the Strings
	StringInt=0
	while StringInt<CountsInt:
	
		#Get With the Beginning
		BeginSplittedPickedString=_String.split(_BeginString)
		
		#Check that there is this Flag
		if len(BeginSplittedPickedString)>1:
		
			#Join the End
			PickedString=_BeginString.join(BeginSplittedPickedString[1:])
			
			#SplittedPickedString by the EndString
			EndSplittedPickedString=PickedString.split(_EndString)
			
			#Record if it not the EndString in the FlagsDict !
			if EndSplittedPickedString[0]!='",\'EndString\':"':
			
				#Record the PickedString
				PickedStringsList.append(EndSplittedPickedString[0])

			#Set the new String
			_String=_EndString.join(EndSplittedPickedString[1:])

		#Increment
		StringInt+=1

		#Special All Case
		if _StringsInt=="All" and len(BeginSplittedPickedString)>1:
			StringInt-=1
	
	#Return '' by default
	return PickedStringsList


def getReadedStringsListWithReaderAndString(_Reader,_String,**Kwargs):
	'''
		<Help>
			Get the ReadedStringsList Between the Begin and the End Flags
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Get a String
			String="print('Hello World');\nThing=3;\nprint('Goodbye World');\n";
			#
			#Print the String
			print("\n String is :");
			print('"');
			print(String);
			print('"');
			#
			#GetPickedString
			PickedStringsList=SYS.getReadedStringsListWithReaderAndString({'Flag':{'BeginString':"print(",'EndString':");\n"},'StringsInt':2},String);
			#
			#Print the list
			print("\n PickedStringsList With String is :");
			print(PickedStringsList);
		</Test>
	'''
	
	#Set the PickedStringsList
	PickedStringsList=[]
	
	#CountsInt
	if _Reader['StringsInt']=="All":
		CountsInt=1
	else:
		CountsInt=_Reader['StringsInt']

	#Define the Flag
	Flag=_Reader['Flag']

	#Scan the Strings
	StringInt=0
	while StringInt<CountsInt:
	
		#Get With the Beginning
		BeginSplittedPickedString=_String.split(Flag['BeginString'])
		
		#Check that there is this Flag
		if len(BeginSplittedPickedString)>1:
		
			#Join the End
			PickedString=Flag['BeginString'].join(BeginSplittedPickedString[1:])
			
			#SplittedPickedString by the EndString
			EndSplittedPickedString=PickedString.split(Flag['EndString'])
			
			#Record if it not the EndString in the FlagsDict !
			if EndSplittedPickedString[0]!='",\'EndString\':"':
			
				#Record the PickedString
				PickedStringsList.append(EndSplittedPickedString[0])

			#Set the new String
			_String=Flag['EndString'].join(EndSplittedPickedString[1:])

		#Increment
		StringInt+=1

		#Special All Case
		if _Reader['StringsInt']=="All" and len(BeginSplittedPickedString)>1:
			StringInt-=1
	
	#Return '' by default
	return PickedStringsList

def getExecStringWithString(_String):

	#Check that it is a String
	if type(_String) in StringTypesList:

		#Read between "<Exec>" and "</Exec>"
		ReadedStringsList=SYS.getReadedStringsListWithReaderAndString({'Flag':SYS.FlagsDict['ExecFlag'],'StringsInt':1},_String)

		#Look if something was found
		if len(ReadedStringsList)==1:
					
			#Take the only possible ExcutionString
			ExecString=ReadedStringsList[0]

			#Return the ExecString
			if len(ExecString)>0:
				return ExecString

def getKeyStringWithKeyVariable(_KeyVariable):

	if type(_KeyVariable) in StringTypesList:

		#Maybe it is an Exec String
		ExecString=getExecStringWithString(_KeyVariable)
		if ExecString!=None:
			six.exec_("_KeyVariable="+ExecString,locals())
		return _KeyVariable
	elif type(_KeyVariable) in [int,float]:
		return str(_KeyVariable)
	elif type(_KeyVariable) == list:
		return '_'.join(_KeyVariable)
	elif type(_KeyVariable) ==tuple:
		return '_'.join(list(getKeyStringWithKeyVariable(_KeyVariable)))
	elif hasattr(_KeyVariable,"__class__"):
		if "TypeString" in _KeyVariable.__class__:
			return _KeyVariable.__class__.TypeString
	elif type(_KeyVariable) == dict:
		return map(lambda Item:Item[0]+'And'+getKeyStringWithKeyVariable(Item[1]),_KeyVariable.items())

def getKeyVariableWithKeyVariableString(_KeyVariableString):

	#Define the "_" splitted _KeyString
	StringsList=_KeyVariableString.split("_")

	#Return the splitted or not
	if len(StringsList)>0:
		return StringsList
	else:
		return [_KeyString]

def getVariableWithDictatedVariableAndKeyList(_DictatedVariable,_KeyList):
	'''
		<Help>
			Universal getItem that looks if there is a specific getitem for the Instance
			and also handle SluggerNamesList Key for dict
		</Help>

		<Test>
			#Load SYS module
			import ShareYourSystem as SYS
			#
			#Set a Dict
			Dict={
				   'ThisDict':{
								'ThingsDict':{'Thing':0},
								'MyObjectsList':[{'ID':0}]
							},
					'Stuff':"Sad",
					'SimpleList':[1,2]
				};
			#
			#Print the Dict
			print("The Dict is : ");
			print(Dict);
			#
			#Get the ValueVariable with a KeyString
			print("\n Getted With [] Key :");
			print(SYS.getVariableWithDictatedVariableAndKeyList(Dict,[]));
			#
			#Get the ValueVariable with a SluggerStringsList
			print("\n Getted With ['ThisDict','ThingsDict'] Key :");
			print(SYS.getVariableWithDictatedVariableAndKeyList(Dict,['ThisDict','ThingsDict']));
			#
			#Get the GettedValueWithKey with a SluggerNamesList with Idx
			print("\n Getted With ['ThisDict','MyObjectsList',0] Key :");
			print(SYS.getVariableWithDictatedVariableAndKeyList(Dict,['ThisDict','MyObjectsList',0]));
		</Test>
	'''

	if type(_KeyList)==list:

		#Empty list case : return the Object
		if len(_KeyList)==0:
			return _DictatedVariable;
		elif len(_KeyList)==1:

			#One Variable List case : return the associated Value at the Int
			if type(_DictatedVariable) in [list,tuple]:
				if type(_KeyList[0])==int:
					if _KeyList[0]<len(_DictatedVariable):
						return _DictatedVariable[_KeyList[0]]

			elif _KeyList[0] in _DictatedVariable:

				#One Variable Dict case : return the associated Value at the KeyString
				return _DictatedVariable[_KeyList[0]]
		else:
			
			#Multi Variables case : recursive call with the reduced list
			if _KeyList[0] in _DictatedVariable:
				return getVariableWithDictatedVariableAndKeyList(_DictatedVariable[_KeyList[0]],_KeyList[1:])

	#Return by default "NotFound"
	return "NotFound"

def getVariableWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable):
	if type(_KeyVariable)==list:
		return getVariableWithDictatedVariableAndKeyList(_DictatedVariable,_KeyVariable)
	elif type(_KeyVariable) in  StringTypesList:
		return _DictatedVariable[_KeyVariable] if _KeyVariable in _DictatedVariable else None

def setWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable,_ValueVariable,**_KwargsDict):
	'''
		<Help>
			Define a set Method with a NameStringsList as a Key for dict
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Set a Dict
			Dict={
				   'ThisDict':{
								'ThingsDict':{'Thing':0},
								'MyObjectsList':[{'ID':0}]
							},
					'Stuff':"Sad",
					'SimpleList':[1,2]
				};
			#
			#setWithSlugger with a SluggerNamesList Key
			SYS.setVariableWithDictatedVariableAndKeyVariable(Dict,['ThisDict','ThingsDict','OtherThing'],1);
			#
			#Print this setted Dict at ['ThisDict','ThingsDict','OtherThing'] equal to 1
			print("The Dict before setting is : ");
			print(Dict);
			#
			#setWithSlugger with a SluggerNamesList Key with Idx
			SYS.setVariableWithDictatedVariableAndKeyVariable(Dict,['ThisDict','MyObjectsList',0,'ID'],1);
			#
			#Print this setted Dict at ['ThisDict','MyObjectsList',0,'ID'] equal to 1
			print("\n The Dict after setting at ['ThisDict','MyObjectsList',0,'ID'] equal to 1 is : ");
			print(Dict);
		</Test>
	'''

	if 'DeepShortString' not in _KwargsDict:
		DeepShortString='/'
	else:
		DeepShortString=_KwargsDict['DeepShortString']

	#Special dict case for also handling SluggerNamesList Key
	if type(_DictatedVariable) in [dict,collections.OrderedDict]:

		#Set with a list
		if type(_KeyVariable)==list:
			if len(_KeyVariable)>0:
				if _KeyVariable[0]==DeepShortString:
					if _KeyVariable==[DeepShortString]:
						LastSlugger=_DictatedVariable
						LastSlugger.update(_ValueVariable)
						return
					else:
						_KeyVariable=_KeyVariable[1:]
				
				LastSlugger=getVariableWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable[:-1])
				setWithDictatedVariableAndKeyVariable(LastSlugger,_KeyVariable[-1],_ValueVariable)
				return
		else:

			#Call a method of the dict
			if (_KeyVariable[1].isalpha() or _KeyVariable[1:3]=="__") and  _KeyVariable[1].lower()==_KeyVariable[1]:
				getattr(_DictatedVariable,_KeyVariable[1:])(_ValueVariable)
				return 

			#Set deeply in the dict
			elif getCommonPrefixStringWithStringsList([_KeyVariable,DeepShortString])==DeepShortString:
				_KeyVariable=DeepShortString.join(_KeyVariable.split(DeepShortString)[1:])

			#Set in the dict
			_DictatedVariable[_KeyVariable]=_ValueVariable

			#Return 
			return

	#List Case
	if type(_DictatedVariable)==list:
		if type(_KeyVariable)==list():
			NextSlugger=getVariableWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable[0])
			setWithDictatedVariableAndKeyVariable(NextSlugger,_KeyVariable[1:],_ValueVariable)
			return
		else:
			_DictatedVariable[_KeyVariable]=_ValueVariable
			return

def getIsTypedVariableBoolWithTypeStringAndClass(_TypeString,_Class):

	#Get the BaseStringsSetList
	ClassAndBaseStringsSetList=[_Class.__name__]+map(lambda Base:Base.__name__,list(set(getBasesListWithClass(_Class))))

	#Check that it has the ChildClass inside
	if getClassStringWithTypeString(_TypeString) in ClassAndBaseStringsSetList:

		#Return 
		return True

	#Return False by default
	return False

def getIsBaseTypeStringBoolWithBaseTypeStringAndTypeString(_BaseTypeString,_TypeString):

	#Get the Class
	Class=getattr(SYS,getClassStringWithTypeString(_TypeString))

	#Look for the getIsTypedVariableBoolWithTypeStringAndClass
	if Class!=None:
		return getIsTypedVariableBoolWithTypeStringAndClass(_BaseTypeString,getattr(SYS,getClassStringWithTypeString(_TypeString)))
	else:
		return False

def getIsTypedVariableBoolWithTypeStringAndVariable(_TypeString,_Variable):

	#Check if it has a Class
	if hasattr(_Variable,"__class__"):

		#Call the getIsTypedVariableBoolWithClass
		return getIsTypedVariableBoolWithTypeStringAndClass(_TypeString,_Variable.__class__)

def getIsPythonTypeStringBool(_String):

	#Check that it is a string
	if type(_String) in StringTypesList:

		#Get the KeyVariableStringsList
		KeyVariableStringsList=SYS.getWordStringsListWithString(_String)

		#See if it respects the conditions
		if len(KeyVariableStringsList)>0:
			if KeyVariableStringsList[-1] not in PythonTypeStringsList:
				return True

		#Return False by default
		return False

def getDictWithVariable(_Variable):
	if type(_Variable)==dict:
		return _Variable
	elif hasattr(_Variable,'__dict__'):
		return _Variable.__dict__
	else:
		return {}

def getTypedVariableWithVariableAndTypeString(_Variable,_TypeString):

	#Case where it is already the case
	if SYS.getIsTypedVariableBoolWithTypeStringAndVariable(_TypeString,_Variable):
		return _Variable
	else:

		#Update the Type
		ClassString=getClassStringWithTypeString(_TypeString)
		if type(ClassString) in StringTypesList:
			if callable(getattr(SYS,ClassString)):

				#Get the VariableDict
				VariableDict=copy.deepcopy(getDictWithVariable(_Variable))

				#Get ta new Instance of the new Type
				Variable=getattr(SYS,ClassString)()
				
				#Update
				global FirstTypeString
				map(lambda Item:
					Variable.__setitem__(Item[0],Item[1]) 
					if Item[0] not in Variable.TypeStringToKeyStringsListDict[FirstTypeString]+[
					'SkippedKeyStringsList'
					] else None,VariableDict.items())

				#Return Variable
				return Variable

	#Return the Variable by default
	return _Variable

def getTypedVariableWithVariableAndTypeStringTuple(_VariableAndTypeStringTuple):
	return getTypedVariableWithVariableAndTypeString(_VariableAndTypeStringTuple[0],_VariableAndTypeStringTuple[1])

def getWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable):

	#Get for the dict case
	if type(_DictatedVariable) in [dict,collections.OrderedDict]:
		if _KeyVariable in _DictatedVariable:
			return _DictatedVariable[_KeyVariable]
	elif hasattr(_DictatedVariable,'__getitem__') and type(_DictatedVariable) not in StringTypesList:

		#Get for an other getitemable case
		return _DictatedVariable[_KeyVariable]

def getDictWithDictatedVariable(_DictatedVariable):

	#Get for the dict case
	if type(_DictatedVariable) in [dict,collections.OrderedDict]:
		return _DictatedVariable
	elif hasattr(_DictatedVariable,'__dict__'):
		return _DictatedVariable.__dict__


def getSingularStringWithPluralString(_PluralString):
	'''
		<Help>
			Get Singular Names managing exceptions defined in the Settings
		</Help>
	
		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Get A Singular Name
			print("Plural Name is Things, Singular Name is "+SYS.getSingularStringWithPluralString("Things"));
		</Test>
	'''

	#Get all the Plural Strings that are defined
	PluralStringsList=SYS.SingularStringToPluralStringInstalledDict.values()

	#Return 
	if _PluralString in PluralStringsList:
		return SYS.SingularStringToPluralStringInstalledDict.keys()[PluralStringsList.index(_PluralString)]
	else:
		return _PluralString[:-1]

def getPluralStringWithSingularString(_SingularString):

	if _SingularString in SYS.SingularStringToPluralStringInstalledDict:
		return SYS.SingularStringToPluralStringInstalledDict[_SingularString]
	else:
		return _SingularString+"s"

def getFlippedDict(_Dict):
	'''
		<Help>
			Flip a StringsDict putting the Values as the Keys
		</Help>
	
		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Define a Dict
			Dict={"a":"0","b":"1","c":"2"};
			#
			#Print the Dict before Flipping
			print("The Dict before flipping is : ");
			print(Dict);
			#Print the Dict after Flipping
			print("\nThe Dict after flipping is : ");
			print(getFlippedDict(Dict));
		</Test>
	'''

	if all(map(lambda ValueVariable:type(ValueVariable) in StringTypesList,_Dict.values())):
		return dict(map(lambda ItemTuple:(ItemTuple[1],ItemTuple[0]),_Dict.items()))

def renameWithDict(_Dict,_OldKeyString,_NewKeyString):
	if _OldKeyString!=_NewKeyString:
		if _OldKeyString in _Dict:

			#Set with the new key
			_Dict[_NewKeyString]=_Dict[_OldKeyString]

			#Delete the _OldKey
			del _Dict[_OldKeyString]

def getIsKwargsFunctionBool(_Function):
	'''
		<Help>
		Return if this Function accepts Kwargs
		</Help>
		
		<Test>
		#Load ShareYourSystem as SYS
		import ShareYourSystem as SYS
		#
		#Define a Function
		def foo(**Kwargs):
		pass;
		#Print
		print(SYS.IsKwargsFunction(foo));
		</Test>
	'''
	
	#Define the FunctionTypeString
	FunctionTypeString=type(_Function).__name__
	
	#Init the VarKwargsDict
	VarKwargsDict=None
	
	#Get directly the Args with the function Type
	if FunctionTypeString=="function":
		
		#Find the Arguments Types
		ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(_Function)
	
	elif FunctionTypeString=='builtin_function_or_method':
		return False
	elif hasattr(_Function,"__func__"):
		
		#Find the Arguments Types
		ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(_Function.__func__)
	
	#Return if Kwargs is accepted or not
	return VarKwargsDict!=None;

def callWithFunction(_Function,*_Args,**_Kwargs):
	'''
		<Help>
		Set the Class of an associated Object
		</Help>
		
		<Test>
	'''
	if getIsKwargsFunctionBool(_Function):
		OutputVariable=_Function(*_Args,**_Kwargs)
	else:
		OutputVariable=_Function(*_Args)
	
	#Return the OutputVariable if there is one
	return OutputVariable

def getIndicesIntsTuplesListWithIntsList(_IntsList):
	import itertools
	return list(itertools.product(*map(lambda LengthInt:range_(LengthInt),_IntsList)))

def getKeepedDictWithDictatingVariableAndKeepedKeyStringsList(_DictatingVariable,_KeepedKeyStringsList):
	return dict(map(lambda FilteredKeepedKeyString:(FilteredKeepedKeyString,getattr(_DictatingVariable,FilteredKeepedKeyString)),filter_(lambda KeepedKeyString:hasattr(_DictatingVariable,KeepedKeyString),_KeepedKeyStringsList)))

def getArrayedIntsListWithArrayedLengthIntsListAndProdIntsListAndRankedInt(_ArrayedLengthIntsList,_ProdIntsList,_RankedInt):

	#Define the EuclidianInt
	EuclidianInt=_RankedInt

	#Init the GridedIntsList for this TuplingInt
	ArrayedIntsList=[0]*len(_ArrayedLengthIntsList)

	#Build the Euclidian List of corresponding Int
	for TableInt in range_(len(_ArrayedLengthIntsList)):

		if TableInt==len(_ArrayedLengthIntsList)-1:
			ArrayedIntsList[TableInt]=EuclidianInt%_ArrayedLengthIntsList[TableInt]
		else:
			ArrayedIntsList[TableInt]=EuclidianInt/_ProdIntsList[TableInt]
			EuclidianInt=EuclidianInt%_ProdIntsList[TableInt]

	#Return
	return ArrayedIntsList

def getArrayedIntsListWithArrayedLengthIntsListAndRankedInt(_ArrayedLengthIntsList,_RankedInt):

	#Get the ProdIntsList
	ProdIntsList=map(lambda Int:functools.reduce(operator.mul,_ArrayedLengthIntsList[Int+1:]),range_(len(_ArrayedLengthIntsList)-1))

	#Return 
	return getArrayedIntsListWithArrayedLengthIntsListAndProdIntsListAndRankedInt(_ArrayedLengthIntsList,ProdIntsList,_RankedInt)

def getPermutationIntWithCoordinateIntsListAndVariablesInt(_CoordinateIntsList,_VariablesInt):
	'''
		<Help>
			From a CoordinateIntsList it returns the PermutationInt
		</Help>

		<Test>
			import ShareYourSystem as SYS
			import itertools
			a=list(itertools.product([0,1,2],repeat=2))
			print(a)
			Int=SYS.getPermutationIntWithCoordinateIntsListAndVariablesInt([2,1],3)
			print(a[Int])
		</Test>
	'''
	#Define the DegreeInt
	LengthInt=len(_CoordinateIntsList)
	ProdLengthIntsList=[0]*LengthInt

	#Map over
	map(lambda Int:ProdLengthIntsList.__setitem__(Int,(_VariablesInt)*ProdLengthIntsList[Int-1] if Int>0 else 1),range_(LengthInt))
	ProdLengthIntsList.reverse()

	#Inner Product
	RankedIntsList=map(operator.mul,_CoordinateIntsList,ProdLengthIntsList)

	#Return
	return sum(RankedIntsList)

def getInstanceWithTypeString(_TypeString):
	ClassString=getClassStringWithTypeString(_TypeString)
	if hasattr(SYS,ClassString):
		return getattr(SYS,ClassString)()

def getIsListsListBool(_ListsList):

	#Check if it is a List of Lists
	if type(_ListsList)==list:
		if len(_ListsList)>0:
			return all(map(lambda ListedVariable:type(ListedVariable)==list,_ListsList))

	#Return False by Default
	return False

def getStructuredDictsListWithIntsListsList(_IntsListsList):

		#Transform the ListedVariablesIntsList into list of empty dicts
		return map(lambda IntOrList:
			map(lambda Int:{},range_(IntOrList)) 
			if type(IntOrList)==int 
			else getStructuredDictsListWithIntsListsList(IntOrList),
			_IntsListsList)
	
def getConcatenatedList(_List):

	#Init the ConcatenatedList
	ConcatenatedList=[]

	#Concatenate
	map(lambda ListOrString:ConcatenatedList.extend(ListOrString) if type(ListOrString)==list else ConcatenatedList.append(ListOrString),_List)
	
	#Return
	return ConcatenatedList

def getPathStringsListWithDict(_DictOrString,**_KwargsDict):

	#Define the LinkString
	LinkString="<ParentToChild>"

	#Init the PathStringsList if not already
	if "PathStringsList" not in _KwargsDict:
		if type(_DictOrString) in [dict,collections.OrderedDict]:
			_KwargsDict['PathStringsList']=_DictOrString.keys()
		elif type(_DictOrString) in StringTypesList:
			_KwargsDict['PathStringsList']=[_DictOrString]
	
	if type(_DictOrString) in [dict,collections.OrderedDict]:

		#Call recursively the Function to build the Paths
		List=map(
					lambda PathString:
					map(
							lambda ChildDictOrString:
							PathString+LinkString+ChildDictOrString 
							if type(ChildDictOrString) in StringTypesList
							else map(
										lambda ChildPathString:
										PathString+LinkString+ChildPathString,
										#ChildPathString,
										getPathStringsListWithDict(ChildDictOrString)
									)
							,_DictOrString[PathString]
						) 
						if type(_DictOrString[PathString]) in [dict,collections.OrderedDict] and len(_DictOrString[PathString])>0 
						else 
						PathString,
						_KwargsDict['PathStringsList']
				)

		#Concatenate
		_List=getConcatenatedList(getConcatenatedList(List))
		_KwargsDict['PathStringsList']=_List

	#Return _KwargsDict['PathStringsList']
	return _KwargsDict['PathStringsList']

def getPathListsListWithDict(_Dict):

	#Define the LinkString
	LinkString="<ParentToChild>"

	#Return
	return map(lambda PathString:PathString.split(LinkString),SYS.getPathStringsListWithDict(_Dict))

def getReversedListsList(_PathListsList):

	#Reverse for each
	map(lambda PathList:PathList.reverse(),_PathListsList)

	#return 
	return _PathListsList

def getReversedPathListsListWithDict(_Dict):
	return getReversedListsList(getPathListsListWithDict(_Dict))

def getLengthSortedListsList(_ListsList):
	return sorted(_ListsList,key=len)

def getLengthSortedPathListsListWithDict(_Dict):
	return getLengthSortedListsList(getReversedListsList(getPathListsListWithDict(_Dict)))

def getReversedPathStringsListWithDict(_Dict):

	#Define the LinkString
	LinkString="<ParentToChild>"
	
	return map(lambda ReversedPathList:
		LinkString.join(ReversedPathList),
		getLengthSortedListsList(getReversedPathListsList(getPathListsListWithDict(_Dict))))

def getReversedPathListWithDict(_Dict):
	return getLengthSortedListsList(getReversedListsList(getPathListsListWithDict(_Dict)))

def getBuildingTuplesListWithDict(_Dict):
	return map(lambda ReversedPathString:(ReversedPathString,{}),getReversedPathListWithDict(_Dict))

def setWithDictAndBuildingTuple(_Dict,_BuildingTuple):

	if type(_BuildingTuple) in [list,tuple]:
		if type(_BuildingTuple)==list:
			if len(_BuildingTuple)!=2:
				return
		PathList=_BuildingTuple[0]
		if len(PathList)>0:
			try:
				ChildVariable=_Dict[PathList[0]]
			except KeyError:
				_Dict[PathList[0]]={}
				ChildVariable=_Dict[PathList[0]]
			setWithDictAndBuildingTuple(ChildVariable,(PathList[1:],_BuildingTuple[1]))
		elif len(PathList)==1:
			_Dict[PathList[0]]=_BuildingTuple[1]
		else:
			return

def getBuildedDictWithBuildingTuplesList(_BuildingTuplesList,**_KwargsDict):
	
	#Init the BuildedDict if not already
	if "BuildedDict" not in _KwargsDict:
		_KwargsDict['BuildedDict']={}

	#Call recursively the function
	map(lambda BuildingTuple:
			setWithDictAndBuildingTuple(_KwargsDict['BuildedDict'],BuildingTuple),
			_BuildingTuplesList
		)

	#Return the BuildedDict
	return _KwargsDict['BuildedDict']

def getShiftedListWithShiftingInt(_List,_ShiftingInt):
	'''
		<Test>
			print(SYS.getShiftedListWithShiftingInt([1,2,3],0))
			print(SYS.getShiftedListWithShiftingInt([1,2,3],1))
			print(SYS.getShiftedListWithShiftingInt([1,2,3],2))
			print(SYS.getShiftedListWithShiftingInt([1,2,3],3))
		</Test>
	'''
	return list(itertools.islice(itertools.cycle(_List),_ShiftingInt,_ShiftingInt+len(_List)))

def getAnchoredShiftedListWithAnchoringVariable(_List,_AnchoringVariable):
	try:
		return getShiftedListWithShiftingInt(_List,_List.index(_AnchoringVariable))
	except ValueError:
		return _List

def getCuttedListWithCuttingVariable(_List,_CuttingVariable):

	'''
		<Test>
			print(getCuttedListWithCuttingVariable(["1","2","3","4"],"1"))
		</Test>
	'''
	try:
		CuttedInt=_List.index(_CuttingVariable)
		CuttedList=_List[:CuttedInt+1] if CuttedInt<len(_List) else _List
		CuttedList.reverse()
		return CuttedList
	except:
		return []

def getCuttedListsListWithCuttingVariable(_ListsList,_CuttingVariable):
	return filter_(lambda List:List!=[],map(lambda List:
		getCuttedListWithCuttingVariable(List,_CuttingVariable),
		_ListsList))

def getCuttedPathListsListWithDictAndCuttingVariable(_Dict,_CuttingVariable):
	return getCuttedListsListWithCuttingVariable(SYS.getPathListsListWithDict(_Dict),_CuttingVariable)

def getBuildingTuplesListWithDictAndBuildingString(_Dict,_BuildingString):
	return map(lambda CuttedPathList:
		(CuttedPathList,{}),
		getCuttedPathListsListWithDictAndCuttingVariable(_Dict,_BuildingString))

def getChildReBuildedDictWithBuildingString(_Dict,_BuildingString):
	'''
		<Test>
			#Rebuild with a Default Dict
			Dict={"3":[{"2a":[{"1a":["0a","0b"]},"1b"],"2b":[{"1b":["0a"],"1c":["0b"]}]}],"0a":[]}
			print(SYS.getChildReBuildedDictWithBuildingString(Dict,"0a"))
			#
			#Rebuild with the BaseTypes Structure
			Dict=getChildReBuildedDictWithBuildingString(SYS.TypeStringToBaseTypeStringsSetDictLoadedDict,"Structure")
		</Test>
	'''
	return SYS.getBuildedDictWithBuildingTuplesList(getBuildingTuplesListWithDictAndBuildingString(_Dict,_BuildingString))

def getParentReBuildedDict(_Dict):
	return SYS.getBuildedDictWithBuildingTuplesList(SYS.getBuildingTuplesListWithDict(_Dict))

def getKeyStringsListWithVariable(_Variable,**_KwargsDict):

	#Init the KeyStringsList if not already
	if "KeyStringsList" not in _KwargsDict:
		_KwargsDict['KeyStringsList']=[]

	#Get the Dict
	Dict=SYS.getDictWithVariable(_Variable)

	#Get the KeyStrings
	map(lambda KeyString:_KwargsDict['KeyStringsList'].append(KeyString),Dict.keys())
	
	#Call in deeper layers
	map(lambda ChildVariable:_KwargsDict['KeyStringsList'].extend(getKeyStringsListWithVariable(ChildVariable)) 
		if SYS.getDictWithVariable(ChildVariable)!={}
		else None,Dict.values())

	#Return _KwargsDict['KeyStringsList']
	return _KwargsDict['KeyStringsList']

def getDerivedTypeStringsListWithTypeString(_TypeString):
	return getKeyStringsListWithVariable(
		getChildReBuildedDictWithBuildingString(
			SYS.TypeStringToBaseTypeStringsSetDictLoadedDict,_TypeString
			)[_TypeString])

def getDerivedTypeStringToSpecificTypeStringsSetListTuplesListWithTypeString(_TypeString):

	#Get the BaseTypeStringsSetList
	BaseTypeStringsSetList=set([_TypeString]+SYS.TypeStringToBaseTypeStringsSetListLoadedDict[_TypeString])

	#Get the FirstDerivedTypeStringToTypeStringsSetListTuplesList
	DerivedTypeStringToTypeStringsSetListTuplesList=map(
			lambda DerivedTypeString:
				(
					DerivedTypeString,
					set(SYS.TypeStringToBaseTypeStringsSetListLoadedDict[DerivedTypeString])
				),
		getDerivedTypeStringsListWithTypeString(_TypeString)
			)

	#Keep the ones with their SpecificBaseTypes
	map(lambda ItemTuple:ItemTuple[1].difference_update(BaseTypeStringsSetList),DerivedTypeStringToTypeStringsSetListTuplesList)

	#Add
	map(lambda ItemTuple:ItemTuple[1].add(ItemTuple[0]),DerivedTypeStringToTypeStringsSetListTuplesList)

	#Return
	return DerivedTypeStringToTypeStringsSetListTuplesList

def getTypedStringToBaseTypeStringsListTuplesListWithTypeStringAndBaseTypeStringsList(_TypeString,_BaseTypeStringsList):
	'''
		<Test>
			print(SYS.getDerivedTypeStringToSpecificTypeStringsSetListTuplesListWithTypeString("Dictionnary"));
		</Test>
	'''

	#Get the TypeStringBaseTypeStringsSetList of the _TypeString
	TypeStringBaseTypeStringsSetList=SYS.TypeStringToBaseTypeStringsSetListLoadedDict[_TypeString]

	#Filter the BaseTypeStrings that are already in the _TypeString
	_BaseTypeStringsList=filter_(lambda BaseTypeString:BaseTypeString not in TypeStringBaseTypeStringsSetList,_BaseTypeStringsList)

	#Get the FirstDerivedTypeStringToBaseTypeStringsSetListDict
	DerivedTypeStringToSpecificTypeStringsSetListTuplesList=[(_TypeString,set([]))]+getDerivedTypeStringToSpecificTypeStringsSetListTuplesListWithTypeString(_TypeString)

	#Transform into a set
	if type(_BaseTypeStringsList)!=set:
		BaseTypeStringsSetList=set(_BaseTypeStringsList)

	#Look for the subset containing the _BaseTypeStringsList
	SubsetDerivedTypeStringToBaseTypeStringsSetListTuplesList=filter_(lambda ItemTuple:BaseTypeStringsSetList.issubset(set(ItemTuple[1])),
			DerivedTypeStringToSpecificTypeStringsSetListTuplesList)

	#Return the Subset
	return SubsetDerivedTypeStringToBaseTypeStringsSetListTuplesList

def getTypedStringWithTypeStringAndBaseTypeStringsList(_TypeString,_BaseTypeStringsList):
	'''
		<Test>
			print(SYS.getTypedStringWithTypeStringAndBaseTypeStringsList("Dictionnary",[]));
		</Test>
	'''
	try:
		return getTypedStringToBaseTypeStringsListTuplesListWithTypeStringAndBaseTypeStringsList(_TypeString,_BaseTypeStringsList)[0][0]
	except IndexError:
		return ""


def getFlattenedListWithVariablesList(_VariablesList):
	'''
		<Test>
			print(SYS.getFlattenedListWithVariablesList([(1,2),5]));
		</Test>
	'''

	return functools.reduce(
					lambda x,y:
						x+list(y) if type(y)==tuple 
						else list(x)+[y] if type(x) in [list,tuple] 
						else [x,y],_VariablesList
				)

def getFlattenedListWithVariablesList(_VariablesList):
	'''
		<Test>
			print(SYS.getFlattenedListWithVariablesList([(1,2),5]));
		</Test>
	'''

	return functools.reduce(
					lambda x,y:
						x+list(y) if type(y)==tuple 
						else list(x)+[y] if type(x) in [list,tuple] 
						else [x,y],_VariablesList
				)

def getPermutedIntsListWithCategoriesIntAndLengthInt(_CategoriesInt,_LengthInt):
	'''
		<Test>
				PermutedIntsList=SYS.getPermutedIntsListWithPermutedIntAndPermutingInt(2,12);
				print(PermutedIntsList);
				print(2**12,len(PermutedIntsList));
		</Test>
	'''
	return functools.reduce(
					lambda x,y:
						map(
								lambda __IntOrTuple:
										getFlattenedListWithVariablesList(list(__IntOrTuple)) 
										if type(__IntOrTuple)==tuple
										else __IntOrTuple,
										itertools.product(x,y)
							),
						map(lambda Int:range_(_LengthInt),range_(_CategoriesInt))
				)

def getFilteredPitchBoolsListsListWithNotesInt(_PitchBoolsListsList,_NotesInt):

	return filter_(
					lambda __PitchBoolsList:
					len(
						filter_(
								lambda Int:
								Int==1,
								__PitchBoolsList
							)
						)==_NotesInt,
					_PitchBoolsListsList
				)

def getFilteredPitchBoolsListsListWithPitchesIntAndNotesInt(_PitchesInt,_NotesInt):
	if type(_PitchesInt)==int and _PitchesInt>0:
		PitchBoolsListsList=getPermutedIntsListWithCategoriesIntAndLengthInt(_PitchesInt,2)
		if type(_NotesInt)==int and _NotesInt>0:
			return getFilteredPitchBoolsListsListWithNotesInt(PitchBoolsListsList,_NotesInt)
		return PitchBoolsListsList
	return []

def getOpenedProcessIdStringsListWithProcessNameString(_ProcessNameString):
	return map(
				lambda StringsList:
				StringsList[1]
				,
				map(
					lambda StringsList:
					filter_(lambda String:String!='',StringsList)
					,filter_(lambda StringsList:
							_ProcessNameString in StringsList and '-u' in StringsList,
							map(lambda CommandString:
										CommandString.split(' '),
										os.popen("ps -ef | grep "+_ProcessNameString).read().split('\n')
								)
							)
				)
			)

def getDictedDictWithVariable(_Variable):

	if hasattr(_Variable,'keys'):

		#Do the recursive call
		return dict(
					map(
							lambda KeyString:
							(KeyString,getDictedDictWithVariable(_Variable[KeyString])),
							_Variable.keys()
						)
					)
	else:

		#Return just the Variable either
		return _Variable

def setGroupWithIndexStringAndDatabaseFileAndDataItemTuple(_IndexString,_DatabaseFile,_DataItemTuple):

	#Set the group
	if _DataItemTuple[0] not in _DatabaseFile:
		_DatabaseFile.create_group(_DataItemTuple[0]) 

	#set deeper in this group
	setDataWithIndexStringAndDatabaseFileAndDataDict(_IndexString,_DatabaseFile[_DataItemTuple[0]],_DataItemTuple[1])

def setDataSetWithIndexStringAndDatabaseFileAndDataItemTuple(_IndexString,_DatabaseFile,_DataItemTuple):

	#Set the group
	if _DataItemTuple[0] not in _DatabaseFile:
		_DatabaseFile.create_group(_DataItemTuple[0]) 

	#set deeper in this group
	_DatabaseFile[_DataItemTuple[0]].create_dataset(_IndexString,data=_DataItemTuple[1])

def setDataWithIndexStringAndDatabaseFileAndDataDict(_IndexString,_DatabaseFile,_DataDict):

	map(
			lambda DataItemTuple:
			#Dict Case
			setGroupWithIndexStringAndDatabaseFileAndDataItemTuple(_IndexString,_DatabaseFile,DataItemTuple) 
			if type(DataItemTuple[1])==dict 
			else
				map(
					lambda IntAndListedVariable:
					setGroupWithIndexStringAndDatabaseFileAndDataItemTuple("_"+str(IntAndListedVariable[0]),_DatabaseFile,IntAndListedVariable[1]),
					enumerate(DataItemTuple[1])
					)
					if type(DataItemTuple[1])==list and all(map(lambda ListedVariable:type(ListedVariable)==dict,DataItemTuple[1]))
					else
						None
						if type(DataItemTuple[1])==list and any(map(lambda ListedVariable:type(ListedVariable)!=dict,DataItemTuple[1]))
						else
							_DatabaseFile.__setitem__(DataItemTuple[0],DataItemTuple[1])
							if DataItemTuple[0]=="_"
							else
							setDataSetWithIndexStringAndDatabaseFileAndDataItemTuple(_IndexString,_DatabaseFile,DataItemTuple),
			_DataDict.items()
		) 

def getLastIndexIntWithDatabaseFileAndDataDict(_DatabaseFile,_DataDict):

	return max(
				map(
						lambda DataString:
						max(
								map(
									lambda KeyString:
									(int)(KeyString),
									_DatabaseFile[DataString].keys()
								) 
							) if all(map(lambda String:String.isdigit(),
									_DatabaseFile[DataString].keys()))
							else
							getLastIndexIntWithDatabaseFileAndDataDict(_DatabaseFile[DataString],_DataDict[DataString]),
						_DataDict.keys()
					)
			)

def getArgumentFloatWithComplex(_Complex):

	#Define the RealFloat and the ImagFloat
	RealFloat=numpy.real(_Complex)
	ImagFloat=numpy.imag(_Complex)

	#Case where it is not a pure real
	if ImagFloat!=0.:

		#Compute the DenominatorFloat
		DenominatorFloat=numpy.sqrt(ImagFloat**2+RealFloat**2)+RealFloat

		if DenominatorFloat!=0.:
			
			#Return the division
			return 2.*numpy.arctan(ImagFloat/(DenominatorFloat))

	#Return 0
	return 0.

def getSplitListsListWithSplittedListAndFunctionPointer(_SplittedList,_FunctionPointer):

	#Init the SplitListsList
	SplitListsList=[[],[]]

	#do the map with side effect
	map(
			lambda ListedVariable:
			SplitListsList[0].append(ListedVariable) 
			if _FunctionPointer(ListedVariable) 
			else SplitListsList[1].append(ListedVariable),
			_SplittedList
		)

	#Return
	return SplitListsList

def groupby(_FunctionPointer,_List):
	return getSplitListsListWithSplittedListAndFunctionPointer(_List,_FunctionPointer)

def getIndexListsListWithSplittedListAndFunctionPointer(_SplittedList,_FunctionPointer):

	#Init the SplitListsList
	SplitListsList=[[],[]]

	#do the map with side effect
	map(
			lambda __IntAndListedVariable:
			SplitListsList[0].append(__IntAndListedVariable[0]) 
			if _FunctionPointer(__IntAndListedVariable[1]) 
			else SplitListsList[1].append(__IntAndListedVariable[0]),
			enumerate(_SplittedList)
		)

	#Return
	return SplitListsList

"""
def getSplittedIndexListsTupleWithSplittingFunctionAndSplittingList(_SplittingFunction,_SplittingList):
						return tuple(
									map(
										lambda __TuplesList:
										list(__TuplesList[0]) if len(__TuplesList)>0 else [],
										map(
											lambda __TuplesList:
												zip(*__TuplesList),
												SYS.groupby(
														lambda __IntAndVariableTuple:_SplittingFunction(__IntAndVariableTuple[1]),
														enumerate(_SplittingList)
													)
											)
									)
								)
"""
def whereby(_SplittingFunction,_SplittingList):
	return getIndexListsListWithSplittedListAndFunctionPointer(_SplittingList,_SplittingFunction)

def unzip(_TuplesList,_IndexesList):
	PickedList=map(
				lambda __Tuple:
				map(__Tuple.__getitem__,_IndexesList),
				_TuplesList
			)
	if len(_IndexesList)==1 and len(PickedList)>0:
		return list(zip(*PickedList)[0])
	return zip(*PickedList)

def collect(_Dict,_KeyStringsList,_IndexesList):
	return map(
				lambda __IndexInt:
				functools.reduce(
					operator.__add__,
					map(
							lambda __KeyString:
							SYS.unzip(_Dict[__KeyString],[__IndexInt]),
							_KeyStringsList
						)
					),
				_IndexesList
			)

def getUpdatedDictWithPrefixStringAndIndexIntsList(_Dict,_KeyStringsList,_PrefixString,_IndexIntsList):

	map(
			lambda __KeyString:
			_Dict.__setitem__(
					_PrefixString+__KeyString,
					map(
						_Dict[__KeyString].__getitem__,
						_IndexIntsList
					)
			),
			_KeyStringsList	
		)

def tag(_Dict,_KeyStringsList,_PrefixString,_IndexIntsList):
	getUpdatedDictWithPrefixStringAndIndexIntsList(_Dict,_KeyStringsList,_PrefixString,_IndexIntsList)

def getTaggedTuplesListWithDictAndTagString(_Dict,_TagString):

	#Get the TaggedTuplesList
	return filter_(
		lambda ItemTuple:
		SYS.getCommonPrefixStringWithStringsList([ItemTuple[0],_TagString])==_TagString,
		_Dict.items()
		)

def getRepresentedNumpyArray(_NumpyArray):

	#Define the ShapeList
	ShapeList=list(numpy.shape(_NumpyArray))

	#Debug
	'''
	print('getRepresentedNumpyArray')
	print('ShapeList is',ShapeList)
	print('')
	'''

	#Return the array directly if it is small or either a short represented version of it
	if (len(ShapeList)==1 and ShapeList[0]<3) or (len(ShapeList)>1 and ShapeList[1]<3):
		return str(_NumpyArray)
	return "<numpy.ndarray shape "+str(ShapeList)+">" 
											
def getRepresentedPointerStringWithRepresentedPointerAndRepresentingDict(_RepresentedPointer,_RepresentingDict):
	'''
		<Help>
			Get a simplified print of a Pointer to avoid circular prints
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Print the Pointer version of a Node Object
			print(SYS.getRepresentedPointerString(SYS.NodeClass()));
		</Test>
	'''

	if _RepresentingDict['IsIdBool']:
		return "<"+(
			_RepresentedPointer.__name__ if hasattr(_RepresentedPointer,__name__) else ""
			)+" ("+_RepresentedPointer.__class__.__name__+"), "+str(id(_RepresentedPointer))+">"
	else:
		return "<"+(
			_RepresentedPointer.__name__ if hasattr(_RepresentedPointer,__name__) else ""
			)+" ("+_RepresentedPointer.__class__.__name__+")"+" >"

def getRepresentedDictStringWithRepresentedDictAndRepresentingDict(
	_RepresentedDict,
	_RepresentingDict
	):

	'''
		<Help>
			Get a Stringified version of a dict with alphabetical order
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Define a Dict
			Dict={'ThisDict':{'ThingsDict':{'Thing':0},'MyObjectsList':[{'ID':0}]},'Stuff':"Sad"};
			#
			#Print the Dict
			print("The RepresentedDictString is :");
			print(SYS.getRepresentedDictStringWithRepresenter(SYS.ConsoleRepresenter,Dict));
		</Test>
	'''
	
	#Define Locals
	AlineaString="" if 'AlineaString' not in _RepresentingDict else _RepresentingDict['AlineaString']
	LineJumpString="\n" if 'LineJumpString' not in _RepresentingDict else _RepresentingDict['LineJumpString']
	ElementString="   /" if 'ElementString' not in _RepresentingDict else _RepresentingDict['ElementString']
	IsRepresentedKeysBool=True if 'IsRepresentedKeysBool' not in _RepresentingDict else _RepresentingDict['IsRepresentedKeysBool']
	IsRepresentedValuesBool=True if 'IsRepresentedValuesBool' not in _RepresentingDict else _RepresentingDict['IsRepresentedValuesBool']

	#Init the RepresentedDictString
	RepresentedDictString='{ '
	
	#Update the AlineaString and check that the binding has worked
	AlineaString+=ElementString

	#Do the first Jump
	RepresentedDictString+=LineJumpString
	
	#Scan the Items (integrativ loop)
	StringLengthInt=len(_RepresentedDict)
	ItemInt=0
	if type(_RepresentedDict)==collections.OrderedDict:
		TuplesList=_RepresentedDict.items()
	else:
		TuplesList=sorted(_RepresentedDict.iteritems(), key=lambda key_value: key_value[0])

	for KeyString,ValueVariable in TuplesList:
	
		RepresentedDictString+=AlineaString

		#Force the cast into string
		if type(KeyString) not in [unicode,str]:
			KeyString=str(KeyString)

		#Only Key Displayed Case
		if IsRepresentedKeysBool and IsRepresentedValuesBool==False:
		
			if SYS.getWordStringsListWithNameString(KeyString)[-1]=="Pointer":
				
				#Pointer Case
				RepresentedDictString+="'"+KeyString+"'"
			
			else:
				
				#OtherCases
				if hasattr(ValueVariable,'__dict__'):
					RepresentedDictString+=SYS.getRepresentedPointerStringWithRepresentedPointerAndRepresentingDict(ValueVariable,_RepresentingDict)
				else:
					RepresentedDictString+=str(type(ValueVariable))
		else:
			
			#Get the WordStringsList
			WordStringsList=SYS.getWordStringsListWithString(KeyString)

			#Init the DisplayedValueVariableString
			DisplayedValueVariableString="None"

			if len(WordStringsList)>0:

				#Value is displayed
				if SYS.getWordStringsListWithString(KeyString)[-1]=="Pointer":
				
					#Pointer Case
					DisplayedValueVariableString=SYS.getRepresentedPointerStringWithRepresentedPointerAndRepresentingDict(
						ValueVariable,_RepresentingDict)

				elif ''.join(SYS.getWordStringsListWithString(KeyString)[-2:])=="PointersList":
				
					#Pointer Case
					DisplayedValueVariableString=str(
							map(
									lambda ListedVariable:
						SYS.getRepresentedPointerStringWithRepresentedPointerAndRepresentingDict(ListedVariable,_RepresentingDict),
									ValueVariable
								)
							)
					
			#Special Suffix Cases
			Type=type(ValueVariable)
			TypeString=Type.__name__
			if TypeString=="instancemethod":
				DisplayedValueVariableString="<"+ValueVariable.__name__+" "+TypeString+">"
				
			elif Type==numpy.ndarray:
				DisplayedValueVariableString=getRepresentedNumpyArray(ValueVariable)

			elif DisplayedValueVariableString=="None":
					
				#Other Cases
				DisplayedValueVariableString=getRepresentedVariableStringWithRepresentedVariableAndRepresentingDict(
					ValueVariable,dict(_RepresentingDict,**{'AlineaString':AlineaString,'LineJumpString':LineJumpString}))
			
			if IsRepresentedKeysBool==False and IsRepresentedValuesBool:
			
				#Only Value Case
				RepresentedDictString+=DisplayedValueVariableString
		
			elif IsRepresentedKeysBool and IsRepresentedValuesBool:
				
				#Key and Value Case
				RepresentedDictString+="'"+KeyString+"' : "+DisplayedValueVariableString

		#Separate items
		ItemInt+=1;
		if ItemInt<StringLengthInt:
			RepresentedDictString+=LineJumpString

	#return the DictString
	return RepresentedDictString+LineJumpString+AlineaString[:-len(ElementString)]+' }'

def getRepresentedListOrTupleStringWithRepresentedListOrTupleAndRepresentingDict(_RepresentedListOrTuple,_RepresentingDict):
	'''
		<Help>
			Get a Stringified version of a list
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Define a Dict
			List=[{'ThisDict':{'ThingsDict':{'Thing':0},'MyObjectsList':[{'ID':0}]},'Stuff':"Sad"},3,{},[{'String':"Haha"}]];
			#
			#Print the Dict
			print("The RepresentedListString is :");
			print(SYS.getRepresentedListStringWithRepresenter(SYS.ConsoleRepresenter,List));
		</Test>
	'''

	#Define Locals
	AlineaString="" if 'AlineaString' not in _RepresentingDict else _RepresentingDict['AlineaString']
	LineJumpString="\n" if 'LineJumpString' not in _RepresentingDict else _RepresentingDict['LineJumpString']
	ElementString="   /" if 'ElementString' not in _RepresentingDict else _RepresentingDict['ElementString']
	IsRepresentedKeysBool=True if 'IsRepresentedKeysBool' not in _RepresentingDict else _RepresentingDict['IsRepresentedKeysBool']
	IsRepresentedValuesBool=True if 'IsRepresentedValuesBool' not in _RepresentingDict else _RepresentingDict['IsRepresentedValuesBool']

	#Init the RepresentedDictString
	if type(_RepresentedListOrTuple)==list:
		BeginBracketString='[ '
		EndBracketString=' ]'
	else:
		BeginBracketString='( '
		EndBracketString=' )'
	RepresentedListString=BeginBracketString

	#Update the AlineaString and check that the binding has worked
	AlineaString+=ElementString

	#Do the first Jump
	RepresentedListString+=LineJumpString
	
	#Scan the Items (integrativ loop)
	StringLengthInt=len(_RepresentedListOrTuple)
	ItemInt=0;
	for ListedVariableInt,ListedVariable in enumerate(_RepresentedListOrTuple):
	
		#Only Key Displayed Case
		if IsRepresentedKeysBool and IsRepresentedValuesBool==False:

			RepresentedListString+=str(ListedVariableInt)

		else:
			
			if type(ListedVariable).__name__=="instancemethod":
				DisplayedValueVariableString="instancemethod"
					
			else:
			
				#Other Cases
				DisplayedValueVariableString=getRepresentedVariableStringWithRepresentedVariableAndRepresentingDict(ListedVariable,
					dict(_RepresentingDict,**{'AlineaString':AlineaString,'LineJumpString':LineJumpString}))
			
			if IsRepresentedKeysBool==False and IsRepresentedValuesBool:
			
				#Only Value Case
				RepresentedListString+=DisplayedValueVariableString
		
			elif IsRepresentedKeysBool and IsRepresentedValuesBool:
				
				#Key and Value Case
				RepresentedListString+=AlineaString+"'"+str(ListedVariableInt)+"' : "+DisplayedValueVariableString

		#Separate items
		ItemInt+=1;
		if ItemInt<StringLengthInt:
			RepresentedListString+=LineJumpString

	#return the DictString
	return RepresentedListString+LineJumpString+AlineaString[:-len(ElementString)]+EndBracketString

def getRepresentedVariableStringWithRepresentedVariableAndRepresentingDict(_RepresentedVariable,_RepresentingDict):
	'''
		<Help>
			Return a special recursive __repr__ of the Variable if it is a Sys Object with an Indent given by the Representer
		</Help>

		<Test>
			#Load ShareYourSystem as SYS
			import ShareYourSystem as SYS
			#
			#Define a Representer
			Representer=SYS.getDefaultRepresenterDict;
			#
			#Get the RepresentedVariable
			print(SYS.getVariableStringWithRepresenter(Representer,{'ThisDict':{'ThingsDict':{'Thing':0},'MyObjectsList':[{'ID':0}]},'Stuff':"Sad"}))
		</Test>
	'''
	
	#Define Locals
	AlineaString="" if 'AlineaString' not in _RepresentingDict else _RepresentingDict['AlineaString']
	LineJumpString="\n" if 'LineJumpString' not in _RepresentingDict else _RepresentingDict['LineJumpString']
	ElementString="   /" if 'ElementString' not in _RepresentingDict else _RepresentingDict['ElementString']
	IsRepresentedKeysBool=True if 'IsRepresentedKeysBool' not in _RepresentingDict else _RepresentingDict['IsRepresentedKeysBool']
	IsRepresentedValuesBool=True if 'IsRepresentedValuesBool' not in _RepresentingDict else _RepresentingDict['IsRepresentedValuesBool']

	if type(_RepresentedVariable) in [dict,collections.OrderedDict]:
		return "\n"+AlineaString+getRepresentedDictStringWithRepresentedDictAndRepresentingDict(_RepresentedVariable,_RepresentingDict)
	elif type(_RepresentedVariable) in [list,tuple]:
		#Check if it is a List of Object or Python Types
		if all(
				map(
					lambda ListedVariable:
					type(ListedVariable) in [float,int,str,unicode,SYS.sys.modules['numpy'].float64],
					_RepresentedVariable
					)
			)==False:
			return getRepresentedListOrTupleStringWithRepresentedListOrTupleAndRepresentingDict(_RepresentedVariable,_RepresentingDict)
		else:
			return str(_RepresentedVariable)
	elif type(_RepresentedVariable).__name__=="instancemethod":
		return AlineaString+"instancemethod"
	elif type(_RepresentedVariable) in StringTypesList:
		return _RepresentedVariable.replace("\n","\n"+AlineaString)
	else:
		return repr(_RepresentedVariable).replace("\n","\n"+AlineaString)

def _print(_RepresentedVariable):
	print(represent(_RepresentedVariable))

def represent(_RepresentedVariable):

	#Define the RepresentingDict
	'''
	RepresentingDict={
			'AlineaString':"",
			'LineJumpString':"\n",
			'ElementString':"   /",
			'IsRepresentedKeysBool':True,
			'IsRepresentedValuesBool':True
		}
	'''

	RepresentingDict=SYS.RepresentingDict

	#Debug
	'''
	print('represent method')
	print('RepresentingDict is ')
	print(RepresentingDict)
	'''

	#Return 
	return getRepresentedVariableStringWithRepresentedVariableAndRepresentingDict(_RepresentedVariable,
																				RepresentingDict
																				)

def getInspectedListWithFrame(_Frame):

	#Define the FilePathString
	FilePathString='/'.join(_Frame.f_code.co_filename.split('/')[-2:])

	#Return 
	return [FilePathString,_Frame.f_code.co_name]

def getUppedString(_String):
	return _String[0].upper()+_String[1:]

def getLowedString(_String):
	return _String[0].lower()+_String[1:]

def getTestMethodStringsListWithClass(_Class):
	return filter_(
					lambda AttributeVariable:
					SYS.getCommonPrefixStringWithStringsList([AttributeVariable,'test_'])=='test_',
					dir(_Class)
				)

def getMethodsListWithClass(_Class):
	return filter_(
			lambda __AttributeVariable:
			type(__AttributeVariable).__name__=="function",
			_Class.__dict__.values()
			)

def getMethodsDictWithClass(_Class):
	return filter_(
			lambda __AttributeTuple:
			type(__AttributeTuple[1]).__name__=="function",
			_Class.__dict__.items()
			)

def setArgsDictWithClass(_Class):

	#Define the MethodsList
	MethodsList=getMethodsListWithClass(_Class)

	#Get the Args
	_Class.ArgsDict=dict(
							map(	
								lambda __Method:
								(
									__Method.__name__,
									dict(
										zip(
											[
												'ArgsList',
												'VarArgsList',
												'VarKwargsDict',
												'DefaultsList'
											],
											list(inspect.getargspec(__Method))
										)
									)
								),
								MethodsList
							)
						)

def getHookingMethodStringsListWithClass(_Class):

	return filter_(
					lambda AttributeVariable:
					any(
							map(
								lambda OrderString:
								SYS.getCommonSuffixStringWithStringsList(
									[AttributeVariable,OrderString])==OrderString,
								['Before','After']
							)
						),
					dir(_Class)
				)

def getHookingMethodStringToMethodsListWithClass(_Class):
	return dict(
				functools.reduce(list.__add__,
						map_(
							lambda __OrderString:
							map_(
								lambda HookingMethodString:
								(HookingMethodString,SYS.sys.modules['collections'].OrderedDict.fromkeys(
													map(
														lambda __MappedClass:
														getattr(__MappedClass,HookingMethodString),
														filter_(
																lambda __FilteredClass:
																hasattr(__FilteredClass,HookingMethodString),
																getattr(_Class,__OrderString+"ClassesList")
															)
														)
												).keys()),
								getattr(_Class,__OrderString+"HookingMethodStringsList")
							),
							['Before','After']
						)
					)
				)

def getBindingMethodStringToMethodsListDictWithClass(_Class):	
	return dict(
					filter_(
						lambda HookingMethodString:
						SYS.getCommonPrefixStringWithStringsList([HookingMethodString[0],"bind"])=="bind",
						_Class.HookingMethodStringToMethodsListDict.items()
					)
				)

def insertBindingMethodsWithBindingMethodStringAndClass(_BindingMethodString,_Class):

	#Get the OrderString
	OrderString=SYS.getWordStringsListWithString(_BindingMethodString)[-1]

	#Define the corresponding HookingMethodString
	HookingMethodString='set'+OrderString

	#Fill with tuples of Hooking and Binding methods
	_Class.BindingMethodStringToMethodTuplesListDict[_BindingMethodString]=filter_(
				lambda __MethodsTuple:
				__MethodsTuple!=(None,None),
				map(
					lambda __SettingClass:
					(
						getattr(__SettingClass,HookingMethodString) 
						if hasattr(__SettingClass,HookingMethodString) else None,
						getattr(__SettingClass,_BindingMethodString) 
						if hasattr(__SettingClass,_BindingMethodString) 
						and getHeritingClassWithMethod(getattr(__SettingClass,_BindingMethodString))==__SettingClass
						else None
					),
					getattr(_Class,OrderString+'ClassesList')
					)
			)

def getIsTuplesListBool(_TuplesList):

	#Check for list of tuples
	if type(_TuplesList)==list:
		return all(
					map(
							lambda _Tuple:
							type(_Tuple)==tuple,
							_TuplesList
						)
					)

	#Return False either
	return False

def setOrAppendWithSettedListAndSettingIntAndSettedVariable(_SettedList,_SettingInt,_SettedVariable):
	if _SettingInt<len(_SettedList):
		_SettedList[_SettingInt]=_SettedVariable
	else:
		_SettedList.append(_SettedVariable)

def getColTypeStringWithKeyString(_KeyString):

	#Define the ColTypeString
	WordStringsList=SYS.getWordStringsListWithString(_KeyString)
	TypeString=WordStringsList[-1]
	TabledTypeString=TypeString

	#Check if it is a Container
	if TypeString=='List':
		TabledTypeString='String'
		_KeyString+='PathString'

	#Init the ColTypeString
	ColTypeString=TabledTypeString
	if ColTypeString=='Int':
		ColTypeString+='64'
	
	#End by the Col type
	ColTypeString+='Col'

	#Define the Col variable
	Col=None
	if TabledTypeString=='Int':
		Col=getattr(tables,ColTypeString)()
	elif TabledTypeString=='String':
		Col=getattr(tables,ColTypeString)(16)

	#Return Col
	return Col

def getHeritingClassWithMethod(_Method):

	#Check for the classes possessing this method
	IsMethodBoolsList=map(lambda __MroClass:hasattr(__MroClass,_Method.__name__),_Method.im_class.__mro__)

	#Return in order to get the one the deepest
	IsMethodBoolsList.reverse()
	ReversedMroClassesList=list(copy.copy(_Method.im_class.__mro__))
	ReversedMroClassesList.reverse()

	#Return the deepest class
	try:
		return ReversedMroClassesList[IsMethodBoolsList.index(True)]
	except:
	 	return None

def getWithMethodAndArgsList(_Method,_ArgsList,**_KwargsDict):
	"""Call a method by paying attention to the shape of the args"""

	#Call the Method given a good shape of the args
	Class=getHeritingClassWithMethod(_Method)
	if Class!=None:

		#Get the Dict
		Dict=getHeritingClassWithMethod(_Method).ArgsDict[_Method.__name__]

		#Shape the args
		if len(Dict['ArgsList'])>2:
			if Dict['VarKwargsDict']!=None:
				return _Method(*_ArgsList,**_KwargsDict)
			else:
				return _Method(*_ArgsList)
		elif len(Dict['ArgsList'])==2:
			if Dict['VarKwargsDict']!=None:
				return _Method(_ArgsList,**_KwargsDict)
			else:
				return _Method(_ArgsList)
		else:
			if Dict['VarKwargsDict']!=None:
				return _Method(**_KwargsDict)
			else:
				return _Method()

def getJoinedStringWithJoiningTuplesList(_JoiningTuplesList):
	return '_'.join(
						map(
								lambda __JoiningTuple:
								'('+str(__JoiningTuple[0])+','+str(__JoiningTuple[1])+')',
								_JoiningTuplesList
							)
					)

def filterNone(__List):
	return filter(lambda __ListedVariable:__ListedVariable!=None,__List)

def getScannedTuplesListWithScanningListsList(_ScanningListsList):
	return list(
				itertools.product(
					*_ScanningListsList
					)
				)

def getIsEqualBool(_VariableA,_VariableB):
		
	#Debug
	'''
	print('getIsEqualBool Avant')
	print('_VariableA is',_VariableA)
	print('_VariableB is ',_VariableB)
	print('')
	'''

	#Cast maybe into numpy array before
	if type(_VariableA)==numpy.ndarray and type(_VariableB)==list:
		_VariableB=numpy.array(_VariableB)
	elif type(_VariableB)==numpy.ndarray and type(_VariableA)==list:
		_VariableA=numpy.array(_VariableA)

	#Debug
	'''
	print('getIsEqualBool Apres')
	print('_VariableA is',_VariableA)
	print('_VariableB is ',_VariableB)
	print('')
	'''

	#Then do the equal process
	if numpy.ndarray not in map(type,[_VariableA,_VariableB]):
		return _VariableA==_VariableB
	elif len(_VariableA)!=len(_VariableB):
			return False
	else:
		return all(
					map(
						lambda __ArrayingInt:
						getIsEqualBool(_VariableA[__ArrayingInt],_VariableB[__ArrayingInt]),
						range_(len(_VariableA))
					))

def getTabularedIntKeyStringWithTabularingTypeString(_TabularingTypeString):
	return getHookedStringWithHookString(_TabularingTypeString.split('Table')[0])+'Int'

def setWithSettingDictAndSettedDictAndGettingStringAndDefaultVariableTuplesList(
	_SettingVariable,
	_SettedVariable,
	_GettingStringAndDefaultVariableTuplesList
	):
	
	#Get the Variables list to be setted in locals
	VariablesList=map(
			lambda __GettingStringAndDefaultVariableTuple:
			_SettingVariable[__GettingStringAndDefaultVariableTuple[0]] 
			if __GettingStringAndDefaultVariableTuple[0] in _SettingVariable
			else __GettingStringAndDefaultVariableTuple[1],
			_GettingStringAndDefaultVariableTuplesList
		)

	#Set in the _SettedVariable
	map(
			lambda __SettingTuple:
			_SettedVariable.__setitem__(*__SettingTuple),
			zip(
				pick(_GettingStringAndDefaultVariableTuplesList,[0]),
				VariablesList
				)+_SettingVariable.items()
		)

def default(_SettingVariable,_SettedVariable,_DefaultingTuplesList):
	"""
		SettingDict={'MyString':"hello"}
		SettedDict={}
		DefaultingTuplesList=[('MyInt',0)]
		SYS.default(SettingDict,SettedDict,DefaultingTuplesList)
		print(SettedDict)
		SYS.default(SettingDict,locals(),DefaultingTuplesList)
		print(MyInt)
	"""
	setWithSettingDictAndSettedDictAndGettingStringAndDefaultVariableTuplesList(
		_SettingVariable,
		_SettedVariable,
		_DefaultingTuplesList
	)

def getDoingStringWithDoneString(_DoneString):
	if len(_DoneString)>0:
		return _DoneString[:-2]+"ing" if _DoneString[-3]!='i' else _DoneString[1][:-3]+'ying'
	return ""

def getDoingStringWithDoString(_DoString):
	if _DoString in ['Parameter','parameter']:
		return _DoString[0]+'arameterizing'
	elif len(_DoString)>0:
		return _DoString+"ing" if _DoString[-1] not in ['e'] else _DoString[:-1]+'ing'
	return ""

def getDoneStringWithDoString(_DoString):
	DoneString=""
	if _DoString in ['Init','init']:
		return _DoString[0]+'nitiated'
	elif _DoString in ['Do','do']:
		return _DoString[0]+'one'
	elif _DoString in ['Parameter','parameter']:
		return _DoString[0]+'arameterized'
	elif _DoString in ['Find','find']:
		return _DoString[0]+'ound'
	elif len(_DoString)>0:
		if _DoString[-1] in ['n']:
			DoneString=_DoString+_DoString[-1]+"ed"
		elif _DoString[-1] not in ['e','y']:
			DoneString=_DoString+"ed" 
		elif _DoString[-1]=='y':
			DoneString=_DoString[:-1]+'ied'  
		else:
			DoneString=_DoString[:-1]+'ed'
	return DoneString

def getDoStringWithDoneString(_DoneString):
	DoString=""
	if _DoneString=='Parameterized':
		return 'Parameter'
	elif _DoneString=='Found':
		return 'Find'
	elif len(_DoneString)>0:
		if _DoneString[-3] in ['y']:
			DoString=_DoneString[:-3]+'y'
		elif _DoneString in ['Structured']:
			DoString=_DoneString[:-1]
	return DoString

def getModeledIntColumnStringWithTable(_Table):
	#return '_'.join(Table._v_pathname[1:].replace('/','_').split('_')[:-1])+'ModeledInt'
	return _Table.name.split('Featured')[0]+'FeaturedModeledInt'

def getModeledIntColumnStringWithKeyString(_GroupedKeyString):
	#return '_'.join(Table._v_pathname[1:].replace('/','_').split('_')[:-1])+'ModeledInt'
	return _GroupedKeyString+'FeaturedModeledInt'

def getPastedPathStringWithPathStringsList(_PathStringsList):

	#Reduce
	PathString=functools.reduce(
			lambda _TotalPathString,_PathString:
			_TotalPathString+_PathString 
			if (len(_TotalPathString)>0 and _TotalPathString[-1]=='/') and (len(_PathString)>0 and _PathString[0]!='/'
				) or (len(_TotalPathString)>0 and _TotalPathString[-1]!='/') and (len(_PathString)>0 and _PathString[0]=='/')
			else 
			_TotalPathString[:-1]+_PathString 
			if (len(_TotalPathString)>0 and _TotalPathString[-1]=='/') and (len(_PathString)>0 and _PathString[0]=='/'
				)
			else _TotalPathString+'/'+_PathString 
			if '/' not in [_PathString,_TotalPathString]
			else "",
			_PathStringsList
			)

	#Maybe add / at the beginning
	if (len(PathString)>0 and PathString[0]!='/') or PathString=="":
		PathString='/'+PathString

	#Return
	return PathString

def getSerializedStringWithTuplesListAndJoinStringAndLinkString(
	_TuplesList,
	_JoinString,
	_LinkString):

	return _JoinString.join(
							map(
									lambda __Tuple:
									str(__Tuple[0])+_LinkString+str(__Tuple[1]),
									_TuplesList
								)
							)

def reinitRepresentingDict():
	#Reinit the RepresentingDict
	SYS.RepresentingDict=copy.copy(SYS.InitRepresentingDict)

######################################################################
#Define the SYSClass that gives to the SYS module Class useful __getattr__,__setattr__ methods
class SYSClass(object):
	def __init__(self,_Module):
		'''
			<Help>
				Init an Object that Wrap an Instance (useful for Module) for setting it like a Class Instance
			</Help>
		
			<Test>
				#Print a SYS __dict__
				SYS=SYS.SYSClass();
				print(SYS.__dict__);
			</Test>
		'''
		
		#Wrap the Module
		self.__dict__['Module']=_Module;
		
		#Set a Pointer to itself
		self.__dict__['SYS']=self

	def __getattr__(self,_KeyString):
		'''
			<Help>
				Get an Attribute safely in the SYS Wrapper
				and build shortcut get function names for accessing
				the loaded Variables
			</Help>
		
			<Test>
				#Print a SYS __dict__
				SYS=SYS.SYSClass();
				print(SYS.__dict__);
			</Test>
		'''
		#Get safely existing Values
		if hasattr(self.Module,_KeyString):
			return getattr(self.Module,_KeyString)
		
		else:

			#Check if it is a FunctionString From a not yet setted Module
			if _KeyString in self.Module.AttributeStringToTypeStringInstalledDict:
				if SYS.Module.AttributeStringToTypeStringInstalledDict[_KeyString] in SYS.Module.TypeStringToFolderPathStringInstalledDict:
					self.Module.importModulesWithFolderPathStringAndIsParsedBool(
						SYS.Module.TypeStringToFolderPathStringInstalledDict[
						SYS.Module.AttributeStringToTypeStringInstalledDict[_KeyString]],
						False)
					return getattr(self.Module,_KeyString)

			#Print no found
			#print("SYS didn't find ValueVariable associated with _KeyString : "+str(_KeyString)) 

	def __repr__(self):
		'''
			<Help>
				Represent the SYS Wrapper Object by displaying
				all its Loaded Variables
			</Help>
		
			<Test>
				#Print a SYS __dict__
				SYS=SYS.SYSClass();
				print(SYS.__dict__);
			</Test>
		'''

		RepresentingString=""
		for LoadedVariableString in SYS.LoadedVariableStringsList:
			RepresentingString+="TypeStringTo"+LoadedVariableString+"LoadedDict is :"
			RepresentingString+=str(getattr(SYS,"TypeStringTo"+LoadedVariableString+"LoadedDict"))
			RepresentingString+="\n"

		return RepresentingString

	def __setattr__(self,_KeyString,_ValueVariable):
		
		#Add to the _ValueVariable the _KeyString as a VariableString if it has __setitem__
		if hasattr(_ValueVariable,'__setitem__'):
			_ValueVariable.__setitem__("VariableString",_KeyString)

		#Set to the Module
		setattr(self.Module,_KeyString,_ValueVariable)

######################################################################
#Define Locals

if sys.version[0]==2:
	StringTypesList=[str,unicode]
else:
	StringTypesList=[str]

#Define the TypeStringTo<> Dicts
LoadedVariableStringsList=[
							"BaseTypeStringsSetList",
							"BaseTypeStringsSetDict",
							"Class",
							"FirstBaseTypeStringsList",
							"FolderPathString",
							"SpecificKeyStringsList"
						]
for LoadedVariableString in LoadedVariableStringsList:
	setattr(sys.modules[__name__],"TypeStringTo"+LoadedVariableString+"LoadedDict",{})

#Define the FlagsDict
FlagsDict={
				'ExecFlag':{'BeginString':"<Exec>",'EndString':"</Exec>"},
				'FunctionHeaderFlag':{'BeginString':"\ndef ",'EndString':":\n"},
				'FunctionHelpFlag':{'BeginString':"<Help>",'EndString':"</Help>"},
				'FunctionNameFlag':{'BeginString':"def ",'EndString':"("},
				'FunctionTestFlag':{'BeginString':"<Test>",'EndString':"</Test>"},
				'FunctionFlag':{'BeginString':"def ",'EndString':"\ndef "},
				'PrintFlag':{'BeginString':"print(",'EndString':");\n",'RegBeginString':"print\(",'RegEndString':");\n"},
				'TestFlag':{'BeginString':"<Test>\n",'EndString':"</Test>"}
		 	}

#Define the PathString
PathString='/'

#Define the SYSType Dicts
PythonTypeStringsList=["Class","Dict","Float","Function","Int","List","Module","Object"]
TypeStringToPythonType=dict(map(lambda TypeString:(TypeString,TypeString[0].lower()+TypeString[1:]),PythonTypeStringsList))
ParentTypeStringToChildTypeStringsListDict={
											'Dicter':["Dict","Instance"],
											'Instance':["Module","Class","Object"],
											'Object':["Node"],
											'Variable':["Dicter","Float","Int","List","String"]
										}
		
#Init the HookStringToHookedStringDict
HookingTuplesList=[
					('add',"Added","Adding"),
					('apply',"Applied","Applying"),
					('call',"Called","Calling"),
					('copy',"Copied","Copying"),
					('delete',"Deleted","Deleting"),
					('featured',"Featured","Featuring"),
					('init',"Initiated","Initiating"),
					('iter',"Iterated","Iterating"),
					('get',"Getted","Getting"),
					('group',"Group","Grouping"),
					('measure',"Measured","Measuring"),
					('rename',"Renamed","Renaming"),
					('repr',"Represented","Representing"),
					('set',"Setted","Setting"),
					('structure',"Structured","Structuring")
				]

#Get the LocalFolderPathString
SitePackageSYSFolderPathString=getLocalFolderPathString()

#The SYS Module becomes a Wrapped Class Instance and other Modules refers to this Wrapped Instance.
sys.modules[__name__]=SYSClass(sys.modules[__name__])

#Define a Shortcut for ShareYourSystem
SYS=sys.modules[__name__]

#Init a Multiprocessing Pool to None
SYS.Module.Pool=None
SYS.Module.IsMultiprocessingBool=False
SYS.Module.ProcessesMaxInt=10

#The Personal 'SYS' Folder PathString
SYSFolderPathString=getSYSFolderPathString()
InstallFolderPathString=SYSFolderPathString+"Install/"+__name__+"/"
ModulesFolderPathString=SYSFolderPathString+"Modules/"

#Init the Singular to Plural ExceptionNames Dict
SYS.Module.writePreVariableStringToPostVariableStringDict("SingularString",[__name__],"PluralStringInstalled",__name__+"s")

#Init the Singular to Plural ExceptionNames Dict
SYS.Module.writePreVariableStringToPostVariableStringDict("PluralString",[__name__+"s"],"SingularStringInstalled",__name__)

#Write/Update the AttributeStringToVariableStringDicts
SYS.Module.writePreVariableStringToPostVariableStringDict("AttributeString",
	getAttributeStringsListWithModule(sys.modules[__name__]),"TypeStringInstalled",__name__)

#Write/Update the TypeStringToFolderPathStringInstalledDict
SYS.Module.writePreVariableStringToPostVariableStringDict(
	"TypeString",["ShareYourSystem"],"FolderPathStringInstalled",SitePackageSYSFolderPathString)

#Prepare a unittest Test object
class TestClass(	
					unittest.TestCase
				):
	pass
SYS.Module.TestClass=TestClass

#Load already some modules in the Init Folder
SYS.Module.importModulesWithFolderPathStringAndIsParsedBool(SYS.SYSFolderPathString+"Modules/Init/",True)

#Define the RepresentingDict
SYS.InitRepresentingDict={
				'AlineaString':"",
				'LineJumpString':"\n",
				'ElementString':"   /",
				'IsRepresentedKeysBool':True,
				'IsRepresentedValuesBool':True,
				'IsIdBool':True
			}

SYS.RepresentingDict={
				'AlineaString':"",
				'LineJumpString':"\n",
				'ElementString':"   /",
				'IsRepresentedKeysBool':True,
				'IsRepresentedValuesBool':True,
				'IsIdBool':True
			}

def test():
	unittest.main(module=SYS)
