
#Readme

Welcome to the ShareYourSystem Tutorial. Here is the first Notebook helping for understanding the main goal of this application :
defining programmed Objects in a independent manner of the chosen language. It joins the concept of JSON Package (found in many ontologic sharing applications like Node, Bower, Grunt) for defining the essential properties characterizing one Type of Object.

##Load an Object With a Package : Example with a quick NeuralNetwork Setting

Let's assume that we want to build an easy multi-parameterized and manipulable Neural Network Object in this framework. We imagine roughly a Network with an excitatory population "E" and an inhibtory one "I", taking in account all the four possible connections between these Objects. We just here for the sake of simplicity stop the parameterized description to this level, letting for further explanations how we can smartly set the populations and connections subdicts for also dissecting and manipulating their properties.


    #Import SYS
    import ShareYourSystem as SYS
    
    #Define the NeuralNetworkPackage
    NeuralNetworkPackageDict={
                   "DefaultSpecificDict":{
                                              "NeuralPopulationsDict":{"E":{},"I":{}},
                                              "NeuralConnectionsDict":{"E-E":{},"E-I":{},"I-E":{},"I-I":{}}
                                          },
                   "TypeString":"NeuralNetwork"
               }
    
    #Load the Package
    SYS.loadModulesWithPackageDict(NeuralNetworkPackageDict)

Line 2 is always necessary if your Python Kernel hasn't yet imported the SYS module.

Line 5 to 11 is setting a dict that will be the Package helping for setting a new Module in the SYS framework. Pay already attention to that important point : in the SYS framework, only ShareYourSystem is strictly a "Pythonic" Module. Then other "Modules" corresponding each to one Loaded Package corresponds to a set of new defined Functions and One Class that are binded to the SYS Scope. Note also that the unavoidable item in the Package definition is the ("TypeString",<"TypeString">) one for making a successful Loading. The "DefaultSpecificDict" is used for defining the specific Attributes belonging to this Object.

In other words, once the loading is done by calling the command "SYS.loadModulesWithPackageDict(NeuralNetworkPackageDict)" l.14, now everything related to this NeuralNetwork "Module/Package" is available in the Context of the Python shortcut Module SYS (like thet underscore _ in the underscore Javascript package). A typical Default NeuralNetwork "Parameters" dict can notably be getted by using the following command :


    #Display the NeuralNetworkDefaultDict
    SYS.NeuralNetworkDefaultDict
    
    #Output is




    {'NeuralConnectionsDict': {'E-E': {}, 'E-I': {}, 'I-E': {}, 'I-I': {}},
     'NeuralPopulationsDict': {'E': {}, 'I': {}}}



##Deep Setting of a Default Dict

We wish we could add deeper description on what each population and each connection is. Therefore, we need in a first step to define their own Package.


    #Import Python Modules
    import ShareYourSystem as SYS
    import itertools
    
    #Define Packages
    NeuralPopulationPackageDict={
                                 "DefaultSpecificDict":{"NeuronsInt":10,"VoltageFloat":-70.},
                                 "TypeString":"NeuralPopulation"
                            }
    
    NeuralConnectionPackageDict={
                                 "DefaultSpecificDict":{"IsExcitatoryBool":True},
                                 "TypeString":"NeuralConnection"
                            }
    
    NeuralPopulationStringsList=["E","I"]               
    NeuralNetworkPackageDict={
                   "DefaultSpecificDict":{
                                              "NeuralPopulationsDict":dict(
                                                                           map(
                                                                               lambda NeuralPopulationString:
                                                                               (NeuralPopulationString,"<Exec>SYS.NeuralPopulationDefaultDict</Exec>"),
                                                                               NeuralPopulationStringsList
                                                                               )
                                                                           ),
                                              "NeuralConnectionsDict":dict(map(lambda PermutationString:(PermutationString,"<Exec>SYS.NeuralConnectionDefaultDict</Exec>"),itertools.product(NeuralPopulationStringsList,repeat=2)))
                                          },
                   "TypeString":"NeuralNetwork"
               }
    
    #Load the Packages
    for PackageDict in [NeuralPopulationPackageDict,NeuralConnectionPackageDict,NeuralNetworkPackageDict]:
        SYS.loadModulesWithPackageDict(PackageDict)

In this last CodeCell, note first that we used the usual multi languages one line built-in command function (map) and the itertools.product for defining in an automatic manner all the population and connection subdicts. 

Next, thanks to the Executed Strings delimited by the "<Exec>" and "</Exex>" Flags, we defined now a deep NeuralNetworkDict including also the individual Default Structures ot the subdicts NeuralPopulation and NeuralConnnection. The SYS.NeuralNetworkDefaultDict becomes now :


    SYS.NeuralNetworkDefaultDict
    #Display the NeuralNetworkDefaultDict
    SYS.NeuralNetworkDefaultDict
    
    #Output is




    {'NeuralConnectionsDict': {('E', 'E'): {'IsExcitatoryBool': True},
      ('E', 'I'): {'IsExcitatoryBool': True},
      ('I', 'E'): {'IsExcitatoryBool': True},
      ('I', 'I'): {'IsExcitatoryBool': True}},
     'NeuralPopulationsDict': {'E': {'NeuronsInt': 10, 'VoltageFloat': -70.0},
      'I': {'NeuronsInt': 10, 'VoltageFloat': -70.0}}}



So... You probably gonna ask for why this weird "<Exec></Exec>" BackBones, Ember, Angular JS Brackets like.... Here is the same kind of goal hopefully to be reached : A Package in this Framework has to be a universal data sharable structure like HTML scripts...That's why it is avoided in its corresponding dict struture to present items with "Python" Instances, and as the Big Conclusion of this first Notebook : Package in SYS has to only contain JSON-formated items. 
