##########################
##########################
###SPARSY DYNAMIC#########
##########################
##########################

################
#MODULE IMPORT
################

import h5py
import os
import numpy as np
import scipy as sc
import scipy.stats
import scipy.optimize
import matplotlib.pyplot
import time
import ShareYourSystem as SYS

#Kill other python processes
try :
	map(lambda IdString:os.popen("kill "+IdString),SYS.getOpenedProcessIdStringsListWithProcessNameString("python")[:-1])
except:
	pass

################
#DEFINITIONS 
################

################
#VARIABLES 

#Ids Definition
AuthorString="Erwan Ledoux"
ModelString="SpecklesScan"

#Params Definition (and Declaration)


#Variables Definition


#Storing Variables
SimulationString=AuthorString+" "+time.ctime(time.time())
SimulationH5pyFile=SYS.H5pyFileClass().update({
													'FileString':ModelString,
													'DirString':"/".join(__file__.split("/")[:-1])+"/"
												})

################
#FUNCTIONS

def initSimulation():
	pass

def runSimulation():
	pass

def plotSimulation():
	pass

def recordSimulation():

	global DataDict
	DataDict={
			}

def writeSimulation():

	#print Data
	#SYS._print(DataDict)

	#Commit
	SimulationH5pyFile.commit(SimulationString,DataDict)

	#Print the Storer
	#print(SimulationH5pyWrapper)
	
	#Push
	SimulationH5pyFile.push()

################
#PROTOCOLS EXECUTION
################

#Define a ScanDict
ScanDict={
		}

#Get the corresponding ScannedTuplesListsList
ScannedTuplesListsList=SYS.getScannedTuplesListsListWithScanDict(ScanDict)
#SYS._print(ScannedTuplesListsList)

#Define a Protocol function : set the locals then do the simulation protocol
def doProtocolWithParamTuplesList(_ParamTuplesList):

	#Set in locals()
	map(
			lambda ParamTuple:
			globals().__setitem__(ParamTuple[0],ParamTuple[1]),
			_ParamTuplesList
		)

	#initSimulation;runSimulation();plotSimulation();recordSimulation();writeSimulation()
	initSimulation;runSimulation();recordSimulation();writeSimulation();

#Do all the scan
map(
		lambda TuplesList:
		doProtocolWithParamTuplesList(TuplesList)
		#None
		,	
		ScannedTuplesListsList
	)

################
#STORING CHECKING
################

#Look the Database
print("Stored h5df Database is");SYS._print(SYS.getDictedDictWithVariable(SimulationH5pyFile.get("/")))

#Plot show
#matplotlib.pyplot.show()

#Get one Particular DataDict
#SYS._print(SimulationH5pyFile.getDataDictWithIndexInt(0))
#SYS._print(SYS.getListingDict(SimulationH5pyFile.getDataDictWithIndexInt(0)))

