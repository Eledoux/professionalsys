################
################
###DRAFT###
################
################

################
#MODULE IMPORT
################

import h5py
import os
import numpy as np
import scipy as sc
import scipy.stats
import matplotlib.pyplot
import time
import ShareYourSystem as SYS

#Kill other python processes
try :
	map(lambda IdString:os.popen("kill "+IdString),SYS.getOpenedProcessIdStringsListWithProcessNameString("python")[:-1])
except:
	pass

H5pyFile=h5py.File("Draft.hdf5","w")

H5pyFile.create_group("ParentGroup")
H5pyFile["ParentGroup"].create_dataset("MyArray",data=np.array([[0,1],[1,1]]))
H5pyFile["ParentGroup"].create_group("FirstChildGroup")
H5pyFile["ParentGroup/FirstChildGroup"].create_dataset("MyChildArray",data=np.array([["coucou","haha"],["hello",1]]))
H5pyFile["ParentGroup"].create_group("SecondChildGroup")
H5pyFile["ParentGroup/SecondChildGroup"].create_dataset("FrequencyFloat",data=0.1)

print(H5pyFile)
print(SYS.getDictedDictWithVariable(H5pyFile))
SYS._print(SYS.getDictedDictWithVariable(H5pyFile))

matplotlib.pyplot.hist(sc.stats.norm.rvs(0.,size=(100.)))
matplotlib.pyplot.show()