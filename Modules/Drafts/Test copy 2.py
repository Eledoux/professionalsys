if 'SYS' not in locals():
	import ShareYourSystem as SYS

#####################
#OBJECTS
#####################

#Object
"""
Object=SYS.ObjectClass()
print(Object)
"""

"""
Object=SYS.ObjectClass().update("MyDict":{"AString":"hello"})
print(Object)
"""

#####################
#DERIVERS
#####################

#Deriver
"""
Deriver=SYS.DeriverClass()
print(Deriver)
"""

#TypeStringDeriver
"""
TypeStringDeriver=SYS.TypeStringDeriverClass()
print(TypeStringDeriver)
"""

"""
TypeStringDeriver=SYS.TypeStringDeriverClass().update({'DerivingTypeString':"Dicter"})
print(TypeStringDeriver)
"""

#####################
#INSTANCERS
#####################

#Instancer
"""
Instancer=SYS.InstancerClass()
print(Instancer)
"""

#VariableInstancer

"""
VariableInstancer=SYS.VariableInstancerClass()
print(VariableInstancer)
"""

"""
VariableInstancer=SYS.VariableInstancerClass().update({'InstancingTypeString':"Functor"})
print(VariableInstancer)
"""

#####################
#USES
#####################

#Use

"""
Use=SYS.UseClass()
print(Use)
"""

"""
Use=SYS.UseClass().update({'DerivingTypeString':"Dicter"})
print(Use)
"""

"""
Object=SYS.ObjectClass();
Object.update({"SpecialDicterUse":SYS.UseClass().update(
	{
		'DerivingTypeString':"Dicter"
	}
	).update(
	{
		'UserPointer':Object
	}
	)})
print(Object)
"""

#####################
#USERS
#####################

#User
"""
User=SYS.UserClass()
print(User)
"""

"""
User=SYS.UserClass();
User(**{'TypeDictString':"Use",'UsedTypeString':"Dicter","PrefixString":"Special"})
print(User['<Used>OfUser_SpecialDicter'])
"""

#####################
#FUNCTORS
#####################

#Functor
"""
Functor=SYS.FunctorClass()
print(Functor)
"""

"""
Functor=SYS.FunctorClass().update({'FunctedFunctionPointer':SYS._print});
Functor(SYS.ElementClass())
"""

#ModuleFunctor
"""
ModuleFunctor=SYS.ModuleFunctorClass()
print(ModuleFunctor)
"""

"""
ModuleFunctor=SYS.ModuleFunctorClass().update({'ModuleString':"scipy",'FunctionString':"exp"})
print(ModuleFunctor)
print(ModuleFunctor(0.9))
"""

#####################
#FILTERS
#####################

#Filter
"""
Filter=SYS.FilterClass()
print(Filter)
"""

"""
Filter=SYS.FilterClass()
Filter['<Used>OfFilter_Functor']['FunctedFunctionPointer']=lambda _FilteredVariable:_FilteredVariable>5
print(Filter(**{'TypeDictString':"Filter","FilteredList":[6,4]}))
"""

#BaseTypesFilter
"""
BaseTypesFilter=SYS.BaseTypesFilterClass()
print(BaseTypesFilter)
"""

"""
print(SYS.BaseTypesFilterClass().update(
	{'FilteringBaseTypeStringsList':["Node","Lister"]}
	)(**{'TypeDictString':"Filter",'FilteredList':[SYS.ObjectClass(),SYS.NodeClass(),SYS.AddingListerClass()]}))
"""



#####################
#ITEMIZER
#####################

#Itemizer
"""
Itemizer=SYS.ItemizerClass()
print(Itemizer)
"""

"""
Itemizer=SYS.ItemizerClass()
Itemizer(**{	
			'ItemizedVariable':{"MyString":"Hello"},
			'MethodString':"append",
			'PrefixString':"Special",
			'TypeDictString':"Item"
		}
	)
print(Itemizer)
"""

"""
Itemizer=SYS.ItemizerClass()
Itemizer(**{	
			'ItemizedVariables':[{"MyString":"Hello"},{"MyOtherString":"Coucou"}],
			'MethodString':"append",
			'PrefixString':"Special",
			'TypeDictString':"Item"
		}
	)
print(Itemizer)
"""

#AddingItemizer
"""
AddingItemizer=SYS.AddingItemizerClass()
print(AddingItemizer)
"""

"""
AddingItemizer=SYS.AddingItemizerClass()+[{},{}]
print(AddingItemizer)
"""

"""
AddingItemizer=SYS.AddingItemizerClass()+[{},0]
print(AddingItemizer)
"""


#####################
#LISTER
#####################

#Lister
"""
Lister=SYS.ListerClass()
print(Lister)
"""

"""
Lister=SYS.ListerClass()
Lister(**{	
			'ListedVariable':{"MyString":"Hello"},
			'MethodString':"append",
			'PrefixString':"Special",
			'TypeDictString':"List"
		}
	)
print(Lister)
"""

"""
Lister=SYS.ListerClass()
Lister(**{	
			'ListedVariables':[{"MyString":"Hello"},{"MyOtherString":"Coucou"}],
			'MethodString':"append",
			'PrefixString':"Special",
			'TypeDictString':"List"
		}
	)
print(Lister)
"""

#AddingLister
"""
AddingLister=SYS.AddingListerClass()
print(AddingLister)
"""

"""
AddingLister=SYS.AddingListerClass()+[{},{}]
print(AddingLister)
"""

"""
AddingLister=SYS.AddingListerClass()+[{},0]
print(AddingLister)
"""

#UsingLister

"""
UsingLister=SYS.UsingListerClass()
print(UsingLister)
"""

"""
UsesLister=SYS.UsesListerClass()
UsesLister(**{	
			'ListedVariables':[
								{
									"UsedTypeString":"Object",
									"InstancedVariable":{"MyString":"Hello"}
								},
								{
									"UsedTypeString":"User",
									"InstancedVariable":{"MyOtherString":"Coucou"}
								}
							],
			'MethodString':"append",
			'PrefixString':"Special",
			'TypeDictString':"List"
		}
	)
print(UsesLister)
"""

#####################
#STRUCTURES
#####################

#Structure

"""
Structure=SYS.StructureClass()
print(Structure)
"""

"""
Structure=SYS.StructureClass()+[{},0]
print(Structure)
"""

"""
Structure=SYS.StructureClass()+[ {}, [ [0,{}],[{}] ] ,[{}] ]
print(Structure)
"""



