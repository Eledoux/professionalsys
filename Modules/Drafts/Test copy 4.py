if 'SYS' not in locals():
	import ShareYourSystem as SYS


#####################
#Indent Classes
#####################

#####################
#Printer Classes
#####################

#####################
#Object Classes
#####################

#Object
"""
Object=SYS.ObjectClass()
print(Object)
"""

"""
Object=SYS.ObjectClass().update({"MyDict":{"AString":"hello"}})
print(Object)
"""

#####################
#Pather Classes
#####################

#Pather
"""
Pather=SYS.PatherClass()
print(Pather)
"""

"""
Pather=SYS.PatherClass().update(
									[
										("MyChildSetter",SYS.SetterClass()),
										("<Pathed>MyChildSetter/MyChildString","coucou")
									]
								)
print(Pather)
"""

#####################
#Doer Classes
#####################

"""
Doer=SYS.DoerClass()
print(Doer)
"""

"""
Doer=SYS.DoerClass()
print(Doer)
"""


#####################
#Setter Classes
#####################

#Setter
"""
Setter=SYS.SetterClass()
print(Setter)
"""

"""
Setter=SYS.SetterClass().do("Set",*[("MyString","hello")],**{"MyInt":2})
print(Setter)
"""

#DeepSetter
"""
DeepSetter=SYS.DeepSetterClass()
DeepSetter.do("Set",*[
						("MyChildSetter",SYS.SetterClass()),
						("MyChildSetter/MyChildString","coucou")
					])
print(DeepSetter)
"""

#####################
#Getter Classes
#####################

#Getter
"""
Getter=SYS.GetterClass()
print(Getter)
"""

"""
Getter=SYS.GetterClass()
Getter.MyString="hello"
Getter.MyInt=2
print(Getter("Get",*["MyString","MyInt"]))
"""




#####################
#Typer Classes
#####################

#Typer
"""
Typer=SYS.TyperClass()
print(Typer)
"""

"""
Typer=SYS.TyperClass().update({'TypingTypeString':"Dicter"})
print(Typer)
"""

"""
Typer=SYS.TyperClass().update({'TypingTypeString':"Dicter",'TypingBaseTypeStringsList':[]})
print(Typer)
"""

#####################
#Instancer Classes
#####################

#Instancer
"""
Instancer=SYS.InstancerClass()
print(Instancer)
"""

"""
Instancer=SYS.InstancerClass().update({'InstancingTypeString':"Functor"})("Instance")
print(Instancer)
"""

#####################
#Users Classes
#####################

#User
"""
User=SYS.UserClass()
print(User)
"""

"""
User=SYS.UserClass();
User("Use",**{
				'<Pathed>UsingInstancer':{
									'InstancingDict':{'MyString':"hello"},
									'InstancingTypeString':"Object"
								}
			})
print(User)
"""

"""
User=SYS.UserClass()+{
						'UsedTypeString':"Dicter",
						'InstancingDict':{'MyString':"hello"}
					};
print(User)
"""

"""
Object=SYS.ObjectClass()
User=SYS.UserClass()+{
						'TypeDictString':"Use",
						'UsedVariable':Object,
						'InstancingDict':{'MyString':"hello"}
					};
print(User)
"""

#####################
#Functor Classes
#####################

#Functor
"""
Functor=SYS.FunctorClass()
print(Functor)
"""

"""
Functor=SYS.FunctorClass().update({'FunctedFunctionPointer':SYS._print});
Functor(SYS.ElementClass())
"""

#ModuleFunctor
"""
ModuleFunctor=SYS.ModuleFunctorClass()
print(ModuleFunctor)
"""

"""
ModuleFunctor=SYS.ModuleFunctorClass().update({'ModuleString':"scipy",'FunctionString':"exp"})
print(ModuleFunctor)
print(ModuleFunctor(0.9))
"""

#####################
#Filter Classes
#####################

#Filter
"""
Filter=SYS.FilterClass()
print(Filter)
"""

"""
Filter=SYS.FilterClass()
Filter['<Used>OfFilter_Functor']['FunctedFunctionPointer']=lambda _FilteredVariable:_FilteredVariable>5
print(Filter(**{'TypeDictString':"Filter","FilteredVariablesList":[6,4]}))
"""

#BaseTypesFilter
"""
BaseTypesFilter=SYS.BaseTypesFilterClass()
print(BaseTypesFilter)
"""

"""
BaseTypesFilter=SYS.BaseTypesFilterClass().update(
	{'FilteringBaseTypeStringsList':["Lister"]})
print(BaseTypesFilter(**{
							'TypeDictString':"Filter",
							'FilteredVariablesList':[SYS.ObjectClass(),SYS.ListerClass()]
							}
						))
"""

#####################
#Identifier Classes
#####################

#Identifier
"""
Identifier=SYS.IdentifierClass()
print(Identifier)
"""

#####################
#Hdfiler Classes
#####################

#Hdfiler
"""
Hdfiler=SYS.HdfilerClass()
print(Hdfiler)
"""

"""
import scipy
import time
Hdfiler=SYS.HdfilerClass()
Hdfiler['<Used>File'].update({
								'FileString':"test",
								'DirString':"/".join(__file__.split("/")[:-1])+"/"
							})
Hdfiler(**{
			'TypeDictString':"Hdfile",
			'HdfiledDict':{'<Hdfiling>ParamFloat':0.1,'<Hdfiled>VariableFloat':scipy.rand(1)},
			'HdfilingString':"Erwan at "+str(time.ctime(time.time())),
			'MethodString':"push"
		}
	)
print(Hdfiler)
"""

"""
import scipy
import time
Hdfiler=SYS.HdfilerClass()
Hdfiler['<Used>File'].update({
								'FileString':"test",
								'DirString':"/".join(__file__.split("/")[:-1])+"/"
							})
print(Hdfiler["<Hdfiled>/"])
"""

"""
import scipy
import time
Hdfiler=SYS.HdfilerClass()
Hdfiler['<Used>File'].update({
								'FileString':"test",
								'DirString':"/".join(__file__.split("/")[:-1])+"/"
							})
SYS._print([Hdfiler["<Hdfiled>0"],Hdfiler["<Hdfiled>2"]])
"""


#####################
#Committer Classes
#####################

#Committer
"""
Committer=SYS.CommitterClass()
print(Committer)
"""

"""
import scipy
Committer=SYS.CommitterClass()
Committer['<Used>Hdfiler']['<Used>File'].update({
								'FileString':"test",
								'DirString':"/".join(__file__.split("/")[:-1])+"/"
							})
Committer(**{
				'TypeDictString':"Commit",
				'HdfiledDict':{'<Hdfiling>ParamFloat':0.1,'<Hdfiled>VariableFloat':scipy.rand(1)},
				'IdentifiedString':"Erwan"
			})
print(Committer)
"""

#####################
#Scanner Classes
#####################

#Scanner
"""
Scanner=SYS.ScannerClass()
print(Scanner)
"""

"""
Scanner=SYS.ScannerClass().update(
						{
							'ScanningTuplesList':[
											('ParamFloatsList',[0.,0.1]),
											('ParamIntsList',[0,1,2])
										]
						}
		)
print(Scanner)
"""

#####################
#Recorder Classes
#####################

#Recorder
"""
Recorder=SYS.RecorderClass()
print(Recorder)
"""

"""
Recorder=SYS.RecorderClass()(**{
	'TypeDictString':"Use",
	'UsedTypeString':"Stimulater"
	})
Recorder['<Used>Stimulater'](**{'TypeDictString':"Stimulate"})
print(Recorder)
"""

#####################
#Procedurer Classes
#####################

#Procedurer
"""
Procedurer=SYS.ProcedurerClass()
print(Procedurer)
"""

"""
Procedurer=SYS.ProcedurerClass().update(
											{
												'ProceduringTypeString':"Stimulater",
												'<Pathed><Used>Scanner':{
																			'ScanningTuplesList':[
																						('TargetStringsList',["Spot","Uniform"]),
																						('PhotonStringsList',["Mono","Bi"])
																					]
																		}
											}
										)
#print(Procedurer)
Procedurer(**{'TypeDictString':"Procedure"})
"""

#####################
#Itemizer Classes
#####################

#Itemizer
"""
Itemizer=SYS.ItemizerClass()
print(Itemizer)
"""

"""
Itemizer=SYS.ItemizerClass()
Itemizer(**{	
			'ItemizedVariable':{"MyString":"Hello"},
			'MethodString':"append",
			'PrefixString':"Special",
			'TypeDictString':"Item"
		}
	)
print(Itemizer)
"""

"""
Itemizer=SYS.ItemizerClass()
Itemizer(**{	
			'ItemizedVariablesList':[{"MyString":"Hello"},{"MyOtherString":"Coucou"}],
			'MethodString':"append",
			'PrefixString':"Special",
			'TypeDictString':"Item"
		}
	)
print(Itemizer)
"""

"""
Itemizer=SYS.ItemizerClass()+[{},{}]
print(Itemizer)
"""

"""
Itemizer=SYS.ItemizerClass()+[{},0]
print(Itemizer)
"""

"""
Itemizer=SYS.ItemizerClass()+[{},0]
print(Itemizer)
"""

#####################
#Sorter
#####################

#Sorter

"""
Sorter=SYS.SorterClass()
print(Sorter)
"""

"""
Sorter=SYS.SorterClass()
Sorter['<Used>Filter']['<Used>Functor']['FunctedFunctionPointer']=lambda _Variable:SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Functor",_Variable)
Sorter(**{
			'TypeDictString':"Sort",
			'SortedVariablesList':[SYS.ObjectClass(),SYS.FunctorClass()]	
	})
print(Sorter)
"""

"""
Sorter=SYS.SorterClass()
Sorter.OfSorter_FilterUse['DerivingTypeString']="BaseTypesFilter"
Sorter['<Used>Filter']['FilteringBaseTypeStringsList']=["Functor"]
Sorter(**{
			'TypeDictString':"Sort",
			'SortedVariablesList':[SYS.ObjectClass(),SYS.FunctorClass()]	
	})
print(Sorter)
"""

#####################
#Structurer Classes
#####################

#Structurer
"""
Structurer=SYS.StructurerClass()
print(Structurer)
"""

"""
Structurer=SYS.StructurerClass()
Structurer(**{	
			'StructuredVariablesList':[
										{"MyString":"Hello"},
										[
											{"MyOtherString":"Coucou"}
										],
										0
									],
			'MethodString':"append",
			'PrefixString':"Special",
			'TypeDictString':"Structure"
		}
	)
print(Structurer)
"""

"""
Structurer=SYS.StructurerClass()+[
										{"MyString":"Hello"},
										[
											{"MyOtherString":"Coucou"}
										],
										0
									]
print(Structurer)
"""


#Structurer=SYS.StructurerClass()+[ 0 , [ 1,[SYS.StructuredObjectClass()],2 ] , 3 ]
#print(Structurer)


#####################
#Collector Classes
#####################

#Collector

"""
Collector=SYS.CollectorClass()
print(Collector)
"""

"""
Collector=SYS.CollectorClass()
Collector(**{	
			'CollectedVariablesList':[
										{"MyString":"Hello"},
										[
											SYS.StructuredObjectClass()
										],
										0
									],
			'MethodString':"append",
			'PrefixString':"Special",
			'TypeDictString':"Collection"
		}
	)
#print(Collector)
#print(Collector['<Used>Structurer'].keys())
"""

"""
import numpy as np
fi=SYS.sys.modules['h5py'].File("ici.hdf5",'w')
arr=np.array([[0.1+1j*6.]])
fi.create_dataset("rr",data=arr)
print(fi)
"""


