if 'SYS' not in locals():
	import ShareYourSystem as SYS

#Indent=SYS.IndentClass()
#print(Indent)

#Printer=SYS.PrinterClass()
#print(Printer)

#####################
#OBJECTS
#####################

#Object
"""
Object=SYS.ObjectClass()
print(Object)
"""

#DictatedObject
"""
DictatedObject=SYS.DictatedObjectClass()
print(DictatedObject)
"""

#ListedObject
"""
ListedObject=SYS.ListedObjectClass()
print(ListedObject)
"""

#IdentifiedObject
"""
IdentifiedObject=SYS.IdentifiedObjectClass()
print(IdentifiedObject)
"""

#####################
#SCALE
#####################

"""
ScalesLister=SYS.ScalesListerClass()
print(ScalesLister)
"""

"""
ScalesLister=SYS.ScalesListerClass().update({'PitchesInt':5})
print(ScalesLister)
"""

"""
ConstantScalesLister=SYS.ConstantScalesListerClass().update({'NotesInt':3}).update({'PitchesInt':5})
print(ConstantScalesLister)
"""

"""
RootedScalesLister=SYS.RootedScalesListerClass().update({'PitchesInt':5})
print(RootedScalesLister)
"""

#SuperScalesLister=SYS.SuperScalesListerClass().update({'NotesInt':3}).update({'PitchesInt':5})
#print(SuperScalesLister)

#TextFile=SYS.TextFileClass().update({'FilePathString':SYS.sys.modules['os'].getcwd()+"/Text.txt"})
#TextFile['TextString']="blabla"
#print(TextFile)

#Lilypond=SYS.LilypondClass().update({'FilePathString':SYS.sys.modules['os'].getcwd()+"/Lilypond.txt"})
#Lilypond["LilypondString"]="{c g e g}"


#H5pyFile=SYS.H5pyFileClass()
#print(H5pyFile)


"""
IntegrateAndFireTransferFunctionSwig=SYS.IntegrateAndFireTransferFunctionSwigClass()
print(IntegrateAndFireTransferFunctionSwig)
"""

#####################
#SWIGS
#####################

"""
Swig=SYS.SwigClass()
print(Swig)
"""

#####################
#USE
#####################

#Use
"""
Use=SYS.UseClass()
print(Use)
"""
"""
Use=SYS.UseClass().update({'UsingTypeString':"Lister",'UsedTypeString':"Object"})
print(Use)
"""

#InstancingUse
"""
InstancingUse=SYS.InstancingUseClass()
print(InstancingUse)
"""

"""
InstancingUse=SYS.InstancingUseClass().update({'UsingTypeString':"Lister",'UsedTypeString':"Object"})
print(InstancingUse)
"""

#StrictInstancingUse
"""
StrictInstancingUse=SYS.StrictInstancingUseClass()
print(StrictInstancingUse)
"""

"""
StrictInstancingUse=SYS.StrictInstancingUseClass().update({'UsingTypeString':"Lister",'UsedTypeString':"Object"})
print(StrictInstancingUse)
"""

#PointeringUse

"""
PointeringUse=SYS.PointeringUseClass()
print(PointeringUse)
"""

"""
PointeringUse=SYS.PointeringUseClass().update({'UsingPointer':SYS.ListerClass(),'UsedTypeString':"Object"})
print(PointeringUse)
"""

#TypeUse

"""
TypeUse=SYS.TypeUseClass()
print(TypeUse)
"""

"""
TypeUse=SYS.TypeUseClass().update({'UsingTypeString':"Lister",
	'UsedTypeString':"Object","TypingBaseTypeStringsList":["AddingLister"]})
print(TypeUse)
"""

#####################
#INSTANCERS
#####################

#Instancer
"""
Instancer=SYS.InstancerClass()
print(Instancer)
"""

"""
Instancer=SYS.InstancerClass().update({'InstancingTypeString':"Object",'InstancedTypeString':"Node"})
print(Instancer)
"""

"""
Instancer=SYS.InstancerClass().update(
	{'InstancingPointer':SYS.ObjectClass(),'InstancedTypeString':"ListedObject"}
	).update({'FromObjectListedObject_AString':"A"})
print(Instancer)
"""

"""
Instancer=SYS.InstancerClass().update(
	{'InstancingPointer':SYS.ObjectClass(),'InstancedTypeString':"ListedObject"}
	).update({'FromObjectListedObject_AString':"A"}).update({'InstancedTypeString':"DictatedObject"})
print(Instancer)
"""

#VariableInstancer

"""
VariableInstancer=SYS.VariableInstancerClass()
print(VariableInstancer)
"""

"""
VariableInstancer=SYS.VariableInstancerClass().update({'InstancingTypeString':"Functor"})
print(VariableInstancer)
"""


#####################
#DERIVERS
#####################

#Deriver
"""
Deriver=SYS.DeriverClass()
print(Deriver)
"""

#TypeStringDeriver
"""
TypeStringDeriver=SYS.TypeStringDeriverClass()
print(TypeStringDeriver)
"""

"""
TypeStringDeriver=SYS.TypeStringDeriverClass().update({'DerivingTypeString':"Dicter"})
print(TypeStringDeriver)
"""

#####################
#USES
#####################

#Use

"""
Use=SYS.UseClass()
print(Use)
"""

"""
Use=SYS.UseClass().update({'DerivingTypeString':"Dicter"})
print(Use)
"""

"""
Object=SYS.ObjectClass();
Object.update({"SpecialDicterUse":SYS.UseClass().update(
	{
		'DerivingTypeString':"Dicter"
	}
	).update(
	{
		'UserPointer':Object
	}
	)})
print(Object)
"""


#####################
#USERS
#####################

"""
User=SYS.UserClass()
print(User)
"""

User=SYS.UserClass();
#User.use("SpecialDicter")
User['SpecialDicter<Use>']="Dicter"
#print(User)
print(User['UsedSpecialDicter'])


#####################
#TYPERS
#####################

#Typer
"""
Typer=SYS.TyperClass()
print(Typer)
"""

"""
Typer=SYS.TyperClass().update({'DerivingTypeString':"Dictionnary"})
print(Typer)
"""

"""
Typer=SYS.TyperClass().update({'DerivingTypeString':"StructuratingLister"}).update({'DerivingBaseTypeStringsList':["AddingLister"]})
print(Typer)
"""

"""
Typer=SYS.TyperClass().update({'DerivingTypeString':"StructuratingLister"}).update({'DerivingBaseTypeStringsList':["SuperLister"]})
print(Typer)
"""

#####################
#BINDER
#####################

#Binder
"""
Binder=SYS.BinderClass()
print(Binder)
"""

"""
Binder=SYS.BinderClass().update(
	{'InstancingPointer':SYS.ObjectClass(),'DerivingTypeString':"Lister"}
	)
print(Binder)
"""

#ListedBinder
"""
ListedBinder=SYS.ListedBinderClass()
print(ListedBinder)
"""

#####################
#LISTERS
#####################

#Lister
"""
Lister=SYS.ListerClass()
print(Lister)
"""

"""
Lister=SYS.ListerClass().update({'VariablesList':[SYS.ElementClass(),SYS.ListedObjectClass()]})
print(Lister)
"""

"""
Lister=SYS.ListerClass().update({'ListedVariablesInt':2})
print(Lister)
"""

"""
Lister=SYS.ListerClass().update({'ListedVariablesInt':2})
print(list(Lister.__iter__()))
"""

#AddingLister
"""
AddingLister=SYS.AddingListerClass()+{}
print(AddingLister)
"""
"""
AddingLister=SYS.AddingListerClass()+[{},{}]
print(AddingLister)
"""
"""
AddingLister=SYS.AddingListerClass()+[{},0]
print(AddingLister)
"""

#InstancingLister
"""
InstancingLister=SYS.InstancingListerClass()
print(InstancingLister)
"""

"""
InstancingLister=SYS.InstancingListerClass().update(
	{'VariablesList':[{}]})
print(InstancingLister)
"""

"""
InstancingLister=SYS.InstancingListerClass().update(
	{'InstancedListedBaseTypeStringsList':["Element","Node"]}).update({'VariablesList':[{},{}]})
print(InstancingLister)
"""

"""
InstancingLister=SYS.InstancingListerClass().update(
	{'InstancedListedBaseTypeStringsList':["Element","Node"]}).update({'VariablesList':[{},{},{}]})
#print(InstancingLister)
"""

"""
InstancingLister=SYS.InstancingListerClass().update(
	{'InstancedListedBaseTypeStringsList':["Element","Node"]}).update({'VariablesList':[{},{},SYS.NodeClass()]})
print(InstancingLister)
"""

"""
InstancingLister=SYS.InstancingListerClass().update({'InstancedListedBaseTypeStringsList':["None","Node","Element"]}).update({'VariablesList':[0,{},SYS.ObjectClass()]})
print(InstancingLister)
"""

"""
InstancingLister=SYS.InstancingListerClass().update({'VariablesList':[{},0,{},SYS.NodeClass()]});
InstancingLister.update({'InstancedListedBaseTypeStringsList':["Element","None","None","Node"]})
print(InstancingLister)
"""

#SpecificInstancingLister
"""
SpecificInstancingLister=SYS.SpecificInstancingListerClass()
print(SpecificInstancingLister)
"""

"""
SpecificInstancingLister=SYS.SpecificInstancingListerClass().update(
	{'SpecificInstancedListedBaseTypeString':"Typer"}
	).update({'VariablesList':[{},{"MyString":"Test"}]})
print(SpecificInstancingLister)
"""

#ListedBindersListedLister

"""
ListedBindersListedLister=SYS.ListedBindersListedListerClass()
print(ListedBindersListedLister)
"""

"""
ListedBindersListedLister=SYS.ListedBindersListedListerClass().update({'VariablesList':[{'DerivingTypeString':"Object"}]})
print(ListedBindersListedLister)
"""

#####################
#TOOLBOXES
#####################

#Toolbox
"""
Toolbox=SYS.ToolboxClass()
print(Toolbox)
"""

#####################
#AGENTS
#####################

#Agent
"""
Agent=SYS.AgentClass()
print(Agent)
"""

"""
Agent=SYS.AgentClass().update({'Toolbox_VariablesList':[{'DerivingTypeString':"Object"}]})
print(Agent)
"""

#####################
#DICTERS
#####################

#Dicter
"""
Dicter=SYS.DicterClass()
print(Dicter)
"""

"""
Dicter=SYS.DicterClass().update({'0':{}})
print(len(Dicter))
print(Dicter)
"""

"""
Dicter=SYS.DicterClass().update({'_0':{}})
print(len(Dicter))
print(Dicter)
"""

"""
Dicter=SYS.DicterClass().update({'_0':{},'_1':{}})
print(list(Dicter.__iter__()))
"""

#PointersDicter
"""
PointersDicter=SYS.PointersDicterClass()
print(PointersDicter)
"""

"""
PointersDicter=SYS.PointersDicterClass().update({'_0':{},'_1':SYS.ObjectClass()})
print(PointersDicter)
"""

#InstancingDicter
"""
InstancingDicter=SYS.InstancingDicterClass()
print(InstancingDicter)
"""

"""
InstancingDicter=SYS.InstancingDicterClass().update({'_0':{}})
print(InstancingDicter)
"""

#StrictDicter
"""
StrictDicter=SYS.StrictDicterClass()
print(StrictDicter)
"""

"""
StrictDicter=SYS.StrictDicterClass()
Object=SYS.ObjectClass();StrictDicter=SYS.StrictDicterClass().update({'_0':Object})
StrictDicter.update({'_1':Object,'_2':{}})
"""

#FilteringDicter
"""
FilteringDicter=SYS.FilteringDicterClass()
print(FilteringDicter)
"""

"""
FilteringDicter=SYS.FilteringDicterClass().update(
	{"FromDicterFilterTypeString":"BaseTypesFilter"}
	).update({"FromDicterFilter_FilteringBaseTypeStringsList":["Lister"]}).update({"_0":SYS.NodeClass(),"_1":SYS.ListerClass()})
print(FilteringDicter)
"""






#####################
#IDENTITY
#####################

#Identity
"""
Identity=SYS.IdentityClass()
print(Identity)
"""


#####################
#PARENTS
#####################

#Parent=SYS.ParentClass()
#Parent=SYS.ParentClass().update({"MyChild":SYS.ObjectClass()})
#print(Parent)

#Parent=SYS.ParentClass().update({"MyChild":SYS.ChildClass()})
#print(Parent)

#####################
#CHILDS
#####################

#Child=SYS.ChildClass()
#print(Child)

#GrandChild=SYS.GrandChildClass()
#print(GrandChild)

#DictatedObjectGrandChild=SYS.DictatedObjectGrandChildClass()
#print(DictatedObjectGrandChild)

#TypedGrandChild=SYS.TypedGrandChildClass()
#print(TypedGrandChild)

#Familiarizer=SYS.FamiliarizerClass()
#print(Familiarizer)

#IdentifierParent=SYS.IdentifierClass()
#print(IdentifierParent)

#Element=SYS.ElementClass()
#print(Element)

#Coordinate=SYS.CoordinateClass()
#print(Coordinate)


#####################
#FUNCTORS
#####################

#Functor
"""
Functor=SYS.FunctorClass()
print(Functor)
"""

"""
Functor=SYS.FunctorClass().update({'FunctionPointer':SYS._print});
Functor(SYS.ElementClass())
print(Functor)
"""

#ModuleFunctor
"""
ModuleFunctor=SYS.ModuleFunctorClass()
print(ModuleFunctor)
"""

"""
ModuleFunctor=SYS.ModuleFunctorClass().update({'ModuleString':"scipy",'FunctionString':"exp"})
print(ModuleFunctor)
print(ModuleFunctor(0.9))
"""

#FilterPointeringFunctor
"""
FilterPointering=SYS.FilterPointeringFunctorClass()
print(FilterPointering)
"""

#####################
#FILTERS
#####################

#Filter
"""
Filter=SYS.FilterClass()
print(Filter)
"""

"""
Filter=SYS.FilterClass()
Filter.FilteringFunctor['FunctionPointer']=lambda _FilteredVariable:_FilteredVariable>5
print(Filter(*[6,4]))
"""

#BaseTypesFilter
"""
BaseTypesFilter=SYS.BaseTypesFilterClass()
print(BaseTypesFilter)
"""

"""
print(SYS.BaseTypesFilterClass().update(
	{'FilteringBaseTypeStringsList':["Node","Lister"]}
	)(*[SYS.ObjectClass(),SYS.NodeClass(),SYS.AddingListerClass()]))
"""

#FromDicterFilter
"""
FromDicterFilter=SYS.FromDicterFilterClass()
print(FromDicterFilter)
"""

#####################
#IDENTIFIERS
#####################

"""
Identifier=SYS.IdentifierClass()
print(Identifier)
"""


#####################
#TREE
#####################

#DictersDicter=SYS.DictersDicterClass()
#DictersDicter=SYS.DictersDicterClass().update({'_0_0':{},_0:{}})
#print(DictersDicter)

#InstancingDictersDicter=SYS.InstancingDictersDicterClass()
#InstancingDictersDicter=SYS.InstancingDictersDicterClass().update({'_0':{'DemandedDictatedBaseTypeString':"Node"}})
#print(InstancingDictersDicter)

#StrictPointersDicter=SYS.StrictPointersDicterClass()
#Object=SYS.ObjectClass();StrictPointersDicter=SYS.StrictPointersDicterClass().update({'_0':Object})
#StrictPointersDicter.update({'_1':Object,'_2':{}})
#print(StrictPointersDicter)

#####################
#SORTER
#####################

#Sorter
"""
Sorter=SYS.SorterClass()+{'_0':{},'_1':{}}
print(Sorter)
"""

#BaseTypesSorter
"""
BaseTypesSorter=SYS.BaseTypesSorterClass()
print(BaseTypesSorter)
"""

"""
BaseTypesSorter=SYS.BaseTypesSorterClass().update({'SortedBaseTypeStringsList':["Node","Object"]});
print(BaseTypesSorter)
"""

"""
BaseTypesSorter=SYS.BaseTypesSorterClass().update(
	{'SortedBaseTypeStringsList':["Node","Object"]}
	).update({'*_0':SYS.ObjectClass(),'*_1':SYS.NodeClass(),'*_2':SYS.ChildClass()})
print(BaseTypesSorter)
"""

#TypesSorter=SYS.TypesSorterClass()
#TypesSorter=SYS.TypesSorterClass().update({'SortedTypeStringsList':["Node","Object"]});
#print(TypesSorter)


#TypesSorter=SYS.TypesSorterClass()
#TypesSorter=SYS.TypesSorterClass().update({'SortedTypeStringsList':["Node","Object"]});
#TypesSorter=SYS.TypesSorterClass().update({'SortedTypeStringsList':["Node","Object"]}).update({'*_0':SYS.ObjectClass(),'*_1':SYS.NodeClass(),'*_2':SYS.ChildClass()})
#print(TypesSorter)



#InstancingStructuratingLister
"""
InstancingStructuratingLister=SYS.InstancingStructuratingListerClass()
print(InstancingStructuratingLister)
"""

#SortingLister
#SortingLister=SYS.SortingListerClass()
#print(SortingLister)

#SpecificInstancingStructuratingLister
#SpecificInstancingStructuratingLister=SYS.SpecificInstancingStructuratingListerClass()
#print(SpecificInstancingStructuratingLister)


#InstancingTypingLister=SYS.InstancingTypingListerClass()
#InstancingTypingLister=SYS.InstancingTypingListerClass().update({'InstancedListedBaseTypeStringsList':["Node"]})
#InstancingTypingLister=SYS.InstancingTypingListerClass().update({'InstancedListedBaseTypeStringsList':["Node"]}).update({'VariablesList':[{}]})
#print(InstancingTypingLister)

#ParentLister=SYS.ParentListerClass()
#ParentLister=SYS.ParentListerClass().update({'VariablesList':[SYS.ElementClass(),{},SYS.NodeClass()]})
#print(ParentLister)

#FamiliarizingStructuratingLister=SYS.FamiliarizingStructuratingListerClass()
#print(FamiliarizingStructuratingLister)

#SuperLister=SYS.SuperListerClass()
#print(SuperLister)

#SpecificLister=SYS.SpecificListerClass()
#SpecificLister=SYS.SpecificListerClass().update({'ListedVariablesInt':3})
#SpecificLister=SYS.SpecificListerClass().update({'DemandedBaseTypeString':"Node"}).update({'ListedVariablesInt':3})
#print(SpecificLister)

#ListersLister=SYS.ListersListerClass()
#ListersLister=SYS.ListersListerClass().update({'ListedVariablesInt':3})
#print(ListersLister)

#FamiliarizerAddingLister=SYS.FamiliarizerAddingListerClass()
#print(FamiliarizerAddingLister)

#DictatedLister=SYS.DictatedListerClass()
#print(DictatedLister)

#####################
#STRUCTURES
#####################

#Structure
"""
Structure=SYS.StructureClass()
print(Structure)
"""
"""
Structure=SYS.StructureClass()+[{},0]
print(Structure)
"""
"""
Structure=SYS.StructureClass()+[[{},0]]
print(Structure)
"""
"""
Structure=SYS.StructureClass()+[ {}, [ [0,{}],[{}] ] ,[{}] ]
print(Structure)
"""
"""
Structure=SYS.StructureClass()+[ {}, [ [0,SYS.SortedStructuredObjectClass(),SYS.ListedObjectClass()],[{}] ] ,[{}] ]
print(Structure)
"""

#StructuratingListersTypingStructure

"""
StructuratingListersTypingStructure=SYS.StructuratingListersTypingStructureClass()
print(StructuratingListersTypingStructure)
"""
"""
StructuratingListersTypingStructure=SYS.StructuratingListersTypingStructureClass()+[ {}, [ [0,{}],[{}] ] ,[{}] ]
print(StructuratingListersTypingStructure)
"""
"""
StructuratingListersTypingStructure=SYS.StructuratingListersTypingStructureClass();
print(StructuratingListersTypingStructure)
"""
"""
StructuratingListersTypingStructure.StructuratingListersTypingStructurePointeringTyper['DerivingBaseTypeStringsList']+=["SuperLister"]
print(StructuratingListersTypingStructure)
"""
"""
StructuratingListersTypingStructure+=[ {}, [ [0,{}],[{}] ] ,[{}] ]
print(StructuratingListersTypingStructure)
"""

#StructuratingStructuresTypingStructure

"""
StructuratingStructuresTypingStructure=SYS.StructuratingStructuresTypingStructureClass()
print(StructuratingStructuresTypingStructure)
"""
"""
StructuratingStructuresTypingStructure=SYS.StructuratingStructuresTypingStructureClass()+[ {}, [ [0,{}],[{}] ] ,[{}] ]
print(StructuratingStructuresTypingStructure)
"""
"""
StructuratingStructuresTypingStructure=SYS.StructuratingStructuresTypingStructureClass();
print(StructuratingStructuresTypingStructure)
"""
"""
StructuratingStructuresTypingStructure.StructuratingStructuresTypingStructurePointeringTyper['DerivingBaseTypeStringsList']+=["SortingStructure"]
StructuratingStructuresTypingStructure+=[ {}, [ [0,{}],[{}] ] ,[{}] ]
print(StructuratingStructuresTypingStructure)
"""

#SortingStructure
"""
SortingStructure=SYS.SortingStructureClass()
print(SortingStructure)
"""
"""
SortingStructure=SYS.SortingStructureClass()+[{},[0]]
print(SortingStructure)
"""
"""
SortingStructure=SYS.SortingStructureClass()+[ {}, [ [0,{}],[{}] ] ,[{}] ]
print(SortingStructure)
"""
"""
SortingStructure=SYS.SortingStructureClass()+[ {}, [ [0,{}],[SYS.SortedStructuredObjectClass()] ] ,[{}] ]
print(SortingStructure)
"""

#InstancingStructure
"""
InstancingStructure=SYS.InstancingStructureClass()
print(InstancingStructure)
"""
"""
InstancingStructure=SYS.InstancingStructureClass()+[ {}, [ [0,{}],[{}] ] ,[{}] ]
print(InstancingStructure)
"""
"""
InstancingStructure=SYS.InstancingStructureClass().update(
	{'InstancedStructuredBaseTypeStringsList':["Node"]}
	)+[ {}, [ [0,{}],[{}] ] ,[{}] ]
print(InstancingStructure)
"""

#SpecificStructure
"""
SpecificStructure=SYS.SpecificStructureClass()
print(SpecificStructure)
"""
"""
SpecificStructure=SYS.SpecificStructureClass().update(
	{'SpecificInstancedStructuredBaseTypeStringsList':["Node"]}
	)+[ {}, [ [0,{}],[{}] ] ,[{}] ]
print(SpecificStructure)
"""
"""
print(SYS.getStructuredDictsListWithIntsListsList([1,[1,2],2]))
"""
"""
SpecificStructure=SYS.SpecificStructureClass().update(
	{'StructuredIntsListsList':[1,[1,2],2]})
print(SpecificStructure)
"""
"""
SpecificStructure=SYS.SpecificStructureClass().update({'SpecificInstancedStructuredBaseTypeString':'StructuredObject'}).update(
	{'StructuredIntsListsList':[1,[1,2],2]})
print(SpecificStructure)
"""

#SuperStructure
"""
SuperStructure=SYS.SuperStructureClass()
print(SuperStructure)
"""

"""
SuperStructure=SYS.SuperStructureClass().update(
	{'SpecificInstancedStructuredBaseTypeString':'SortedStructuredObject'}
	).update(
	{'StructuredIntsListsList':[1,[1,2],2]}
	)
print(SuperStructure)
"""


#####################
#CONTAINERS
#####################

#Container
"""
Container=SYS.ContainerClass().update({'ContainedTypeStringsList':["Node","Object"]});
print(Container)
"""

"""
Container=SYS.ContainerClass().update(
	{'InstancedDictatedBaseTypeString':"SuperStructure"}
	).update(
	{'ContainedTypeStringsList':["Node","Object"]}
	);
print(Container)
"""

"""
Container=SYS.ContainerClass().update(
	{'InstancedDictatedBaseTypeString':"SuperStructure"}
	).update(
	{'ContainedTypeStringsList':["Node","Object"]}
	);
Container["_NodesSuperStructure"].update({'StructuredIntsListsList':[1,[1,2],2]})
print(Container)
"""

#####################
#PERMUTATERS
#####################

#Permutation
"""
Permutation=SYS.PermutationClass()
print(Permutation)
"""
#Permutater

"""
Permutater=SYS.PermutaterClass().update(
	{
		"PermutationDegreeInt":2,
		"PermutatedVariablesInt":3
		})
print(Permutater)
"""

"""
Permutater=SYS.PermutaterClass().update(
	{"PermutationDegreeInt":2,
	"PermutatedVariablesInt":3
	})
print(Permutater["0-1"])
print(Permutater.getPermutationIntWithPermutationString("0-1"))
print(Permutater[SYS.getPermutationIntWithCoordinateIntsListAndVariablesInt([0,1],Permutater.PermutatedVariablesInt)])
"""


#Table=SYS.TableClass()
#Table=SYS.TableClass().update({'DemandedBaseTypeString':"Dicter"})+{}
#Table=SYS.TableClass().update({'DemandedBaseTypeString':"Agent"})+[{},{}]
#Table=(SYS.TableClass().update({'DemandedBaseTypeString':"Agent"})+[{},{}]).update({'ListedVariablesInt':1})
#print(Table)
#print(list(Table.__iter__()))

#Database=SYS.DatabaseClass()
#Database=SYS.DatabaseClass()+{}
#Database=SYS.DatabaseClass().update({'DatabasedTablesTypeStringsList':["Table"]})
#Database=SYS.DatabaseClass().update({'DatabasedTablesTypeStringsList':["Table"]}).update({'0_ListedVariablesInt':2});
#print(Database)

#AddingDatabase=SYS.AddingDatabaseClass()
#AddingDatabase=SYS.AddingDatabaseClass()+[ [{},[{},{}],[{}]] ]
#print(AddingDatabase)

#SpecificDatabase=SYS.SpecificDatabaseClass()
#SpecificDatabase=SYS.SpecificDatabaseClass().update({'DatabasedTablesListedVariablesIntsList':[0,0,0]})
#SpecificDatabase=SYS.SpecificDatabaseClass().update({'DatabasedTypeString':"Coordinate"}).update({'DatabasedTablesListedVariablesIntsList':[1,0,2]})
#print(SpecificDatabase)

#SortedLister=SYS.SortedListerClass()
#print(SortedLister)

#Sorter=SYS.SorterClass()
#Sorter=SYS.SorterClass()+[ [{},[{},{}],[{}]] ]
#Sorter=SYS.SorterClass()+[ [{},[ [ [{},{}],[{}] ] ],[{}]] ]
#Sorter.setAllPreviousSortedVariablesInt()
#SYS._print(list(Sorter.__iter__()))
#print(Sorter)
#print(Sorter[2])
#print(Sorter["#4"])

#SpecificSorter=SYS.SpecificSorterClass()
#SpecificSorter=SYS.SpecificSorterClass().update({'DatabasedTablesListedVariablesIntsList':[1,0,2]})
#SpecificSorter=SYS.SpecificSorterClass().update({'DatabasedTablesListedVariablesIntsList':[1,[1,2],2]})
#print(SpecificSorter)


#Agent=SYS.AgentClass()
#print(Agent)

#Interaction=SYS.InteractionClass()
#print(Interaction)

#Tupler=SYS.TuplerClass()
#Tupler=SYS.TuplerClass().update({'DatabasedTypeString':'Interaction'}).update({'TupledDegreeIntsList':[1,2]}).update({'TupledAgentsInt':3})
#Tupler=SYS.TuplerClass().update({'TupledDegreeIntsList':[1,2]}).update({'TupledAgentsInt':3})
#print(Tupler)
#print(Tupler["0-1"])
#print(Tupler["#2"])

#Coupler=SYS.CouplerClass()
#Coupler=SYS.CouplerClass().update({'InteractionsCoupledTupler_TupledDegreeIntsList':[2]});Coupler.AgentsCoupledSorter+=[ [{},[ [ [{},{}],[{}] ] ],[{}]] ]
#print(Coupler)
#print(Coupler.InteractionsCoupledTupler["0-1"])
#print(Coupler["#1"])
#print(Coupler["#0-#1"])





#Mapper=SYS.MapperClass()
#Mapper=SYS.MapperClass().update({'MappedFunctor':SYS.FunctorClass().update({'FunctionPointer':lambda x:x+1})}).update({'VariablesList':[1,4,2]})
#print(Mapper)

#Scaler=SYS.ScalerClass()
#Scaler=SYS.ScalerClass().update({'VariablesList':[1,4,2]}).update({'ScalingFloat':2.})
#print(Scaler)

#MappedCoordinate=SYS.MappedCoordinateClass()
#print(MappedCoordinate)

#Space=SYS.SpaceClass()
#Space=SYS.SpaceClass()+[ [ [{},{},{}],[{},{}],[{}] ] ] 
#print(Space)

#ScalingSpace=SYS.ScalingSpaceClass()
#ScalingSpace=SYS.ScalingSpaceClass()+[ [ [{},{},{}],[{},{}],[{}] ] ] 
#print(ScalingSpace)

#Array=SYS.ArrayClass().update({}).update({'ArrayedLengthIntsList':[2]})
#print(Array[[0]])
#Array=SYS.ArrayClass().update({}).update({'ArrayedLengthIntsList':[2,2]})
#Array=SYS.ArrayClass().update({}).update({'ArrayedLengthIntsList':[1,2,1,1]});print(Array[[0,1,0,0]])
#Array=SYS.ArrayClass().update({}).update({'ArrayedLengthIntsList':[2,3,2]})
#print(Array[[0,1,0]])
#AddedNumpyArray=numpy.zeros((2,3,2),dtype=dict)
#AddedNumpyArray[0,2,1]={}
#AddedNumpyArray[1,0,0]={}
#print(numpy.where(AddedNumpyArray!=0))
#Array=SYS.ArrayClass().update({}).update({'ArrayedLengthIntsList':[2,3,2]})+AddedNumpyArray
#Array=SYS.ArrayClass().update({'ArrayedTypeString':'Decorator'}).update({'ArrayedLengthIntsList':[1,2]})
#print(Array[[0,1]])
#Array=SYS.ArrayClass().update({'ArrayedTypeString':'Decorator'}).update({'ArrayedLengthIntsList':[1,2,1]});print(Array[[0,1,0]])
#Array=SYS.ArrayClass().update({'ArrayedTypeString':'Decorator'}).update({'ArrayedLengthIntsList':[1,2,1,2]});print(Array[[0,1,0,1]])
#print(Array)
#SYS._print(list(Array.__iter__()))
#print(Array.getListersList())

#Matrix=SYS.MatrixClass()
#for Int in xrange(Matrix.ArrayedLengthIntsList)
#print(Matrix)

#Space=SYS.SpaceClass()
#Space=SYS.SpaceClass().update({}).update({'ArrayedLengthIntsList':[2,3,2]});
#SYS._print(list(Space.__iter__()))
#print(Space)

#RankedCoordinate=SYS.RankedCoordinateClass()
#print(RankedCoordinate)

#RankedSpace=SYS.RankedSpaceClass()
#RankedSpace=SYS.RankedSpaceClass().update({}).update({'ArrayedLengthIntsList':[2,3,2]});
#SYS._print(list(RankedSpace.__iter__()))
#print(RankedSpace)
#print(RankedSpace["#0"])
#print(RankedSpace["#11"])

#Structure=SYS.StructureClass()
#Structure=SYS.StructureClass().update({}).update({'ArrayedLengthIntsList':[2,3,2]});
#SYS._print(list(Structure.__iter__()))
#print(Structure["#1"])



#MutableCoupler=SYS.MutableCouplerClass()
#MutableCoupler=SYS.MutableCouplerClass().update({'CoupledAgentTypeString':'Node'}).update({'InteractionsTupler_TupledDegreeIntsList':[2]}).update({"NodesStructure_ArrayedLengthIntsList":[2,2]})
#MutableCoupler=SYS.MutableCouplerClass().update({'CoupledAgentTypeString':'Node','CoupledInteractionTypeString':'Connection'}).update({'ConnectionsTupler_TupledDegreeIntsList':[3]}).update({"NodesStructure_ArrayedLengthIntsList":[2,2]})
#print(MutableCoupler)

#Network=SYS.NetworkClass()
#Network=SYS.NetworkClass().update({"NodesStructure_ArrayedLengthIntsList":[2,2]})
#print(Network)
#print(Network["#1"])
#print(Network["#0-#1"])

#Grid=SYS.GridClass()
#Grid=SYS.GridClass().update({"NodesMatrix_ArrayedLengthIntsList":[2,2]})
#print(Grid)

#Population=SYS.PopulationClass()
#print(Population)

#DynamicPopulation=SYS.DynamicPopulationClass()
#print(DynamicPopulation)

#BrianedPopulation=SYS.BrianedPopulationClass()
#BrianedPopulation=SYS.BrianedPopulationClass().update({"UnitsInt":2})
#print(BrianedPopulation)

#Connection=SYS.ConnectionClass()
#print(Connection)

#BrianedConnection=SYS.BrianedConnectionClass()
#print(BrianedConnection)

#PopulationNetwork=SYS.PopulationNetworkClass()
#PopulationNetwork=SYS.PopulationNetworkClass().update({"AgentsArray_ArrayedLengthIntsList":[2,2]})
#print(PopulationNetwork)

#Clock=SYS.ClockClass()
#print(Clock)

#BrianClock=SYS.BrianClockClass()
#print(BrianClock)

"""
#BrianedNetwork=SYS.BrianedNetworkClass()
BrianedNetwork=SYS.BrianedNetworkClass().update({"AgentsArray_ArrayedLengthIntsList":[2,2]})
for Population in BrianedNetwork.PopulationsArray.__iter__():
	Population['UnitsInt']=10
BrianedNetwork.run()
print(BrianedNetwork)
"""

#Shape=SYS.ShapeClass()
#print(Shape)

#Shaper=SYS.ShaperClass()

#Shaper=SYS.ShaperClass().update({'ShapingCursor_CursorLengthIntsList':[10,10]}).update({'ShapesStructure_ArrayedLengthIntsList':[1]})
#Shaper=SYS.ShaperClass().update({'ShapingCursor_CursorLengthIntsList':[50,50]}).update({'ShapesStructure_ArrayedLengthIntsList':[2]})
#Shaper=SYS.ShaperClass().update({'ShapingLengthIntsList':[10,10]}).update({'ShapesStructure_ArrayedLengthIntsList':[5]})

#SYS._print(list(Shaper.ShapesStructure.__iter__()))
#print(Shaper.ShapesStructure["#1"])
#print(Shaper)

"""
for i in [1,2,3]:
	for j in ["a","b","c"]:
		print(j)
		if j=="b":
			break
"""



#def betatest(_Int):
#	return str(_Int)
#SYS.IsMultiprocessingBool=True
#SYS._map(SYS.getInstanceWithTypeString,["Array" for Int in range(1000)])

#Patch=SYS.PatchClass()
#print(Patch)

#Plot=SYS.PlotClass()
#print(Plot)

#Patcher=SYS.PatcherClass()
#Patcher=SYS.PatcherClass().update({'PatchedRowsInt':10,'PatchedColsInt':10}).update({'PatchesMatrix_ArrayLengthIntsList':[2,2]})
#print(Patcher)

#Panel=SYS.PanelClass()
#print(Panel)

"""
Figure=SYS.FigureClass().update({'PanelsMatrix_ArrayLengthIntsList':[2,2]})
for Panel in Figure.PanelsMatrix.__iter__():
	Panel.update({'PlotsMatrix_ArrayLengthIntsList':[2,2]})
print(Figure)
"""

#print(SYS.getPluralStringWithSingularString("Patch"))


"""
from pylab import *

Data={

		"TimeData":{
						"DatasList":
						[
							{
								"Label":"Ann\xe9es",
								"FloatsList":[2000,2006,2007,2008,2009,2010,2011,2012,2013]
							}
						]
					},
		"PerceptionData":
		{
			"DatasList":
			[
				{
					"Label":r'$La\ soci\acute{e}t\acute{e}\ est-elle\ injuste\ ?$',
					"FloatsList":[68,75,69,71,73,78,75,72,76]
				},
				{
					"Label":r'$La\ soci\acute{e}t\acute{e}\ est-elle\ juste\ ?$',
					"FloatsList":[27,21,28,26,26,21,23,26,22]
				},
				{
					"Label":r'$Les\ in\acute{e}galit\acute{e}es\ ont-elles\ augment\acute{e}e\ ?$',
					"FloatsList":[69,78,77,80,85,87,89,86,87]
				},
				{
					"Label":r'$Les in\acute{e}galit\acute{e}s\ vont\ plutot\ augment\acute{e}e\ ?$',
					"FloatsList":[65,75,72,78,81,84,83,82,83]
				}
			]
		}
	}

fig=figure()
ax=subplot2grid((20,40),(0,0),colspan=20,rowspan=20)
for Item in Data['PerceptionData']['DatasList']:
	#ax.plot(Item['FloatsList'],'o-',lw=3,label=Item['Label'])
	ax.plot(Data['TimeData']['DatasList'][0]['FloatsList'],Item['FloatsList'],'o-',lw=3,label=Item['Label'])
ax.legend(loc='upper center', bbox_to_anchor=(1.5, 1.05),
          ncol=1, fancybox=True, shadow=True)
#ax.set_xticklabels(Data['TimeData']['DatasList'][0]['FloatsList'])
show()
"""

#MyExampleA=SYS.ExampleAClass()
#print(SYS.ExampleA)

#print(MyExampleA.__dict__)
#MyExampleB=SYS.ExampleBClass()
#SYS._print(MyExampleB)


















