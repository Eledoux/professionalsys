#<ImportModules>
import collections
import inspect
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DebuggingElementString='    '
DebuggingIsString=' is '
DebuggingHeadPrefixString='////////////////////////////////\n'
DebuggingHeadSuffixString='\n////////////////////////////////\n\n'
BasingLocalTypeString="Representer"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineFunctions>
def getDebuggedStringWithDebuggingVariable(_DebuggingVariable):

	#Set the DebuggedString with the _DebuggingVariable
	if type(_DebuggingVariable)==str:
		return _DebuggingVariable
	elif type(_DebuggingVariable)==tuple:
		return '\n'.join(
						map(
								lambda __KeyString:
								_DebuggingVariable[0]+__KeyString+' is '+SYS.represent(
									_DebuggingVariable[1][__KeyString] if type(_DebuggingVariable[1]) in [collections.OrderedDict,dict]
									else getattr(_DebuggingVariable[1],__KeyString)
								),
								_DebuggingVariable[2]
							)
						)

	elif type(_DebuggingVariable)==list:
		return '\n'.join(map(getDebuggedStringWithDebuggingVariable,_DebuggingVariable))
#</DefineFunctions>

#<DefineClass>
class DebuggerClass(BasedLocalClass):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.DebuggingNotFramedFunctionStringsList=["HookedFunction","<lambda>"]
		self.DebuggingIsBool=True
		self.DebuggedFramesList=[]
		#</DefineSpecificDict>

	def debug(self,_DebuggingVariable):

		#Check
		if self.DebuggingIsBool:

			#Debug
			'''
			print('Debugger debug method')
			print('DebuggedCurrentFrame is ',DebuggedCurrentFrame)
			print('self.DebuggedFramesList is ',self.DebuggedFramesList)
			print('')
			'''

			#Define the DebuggedCurrentFrame
			DebuggedCurrentFrame=inspect.currentframe().f_back

			#Init the DebuggedString
			DebuggedString='\n'

			#Append maybe for the first time
			if len(self.DebuggedFramesList)==0 or DebuggedCurrentFrame!=self.DebuggedFramesList[0]:

				#Debug
				'''
				print('This is the first frame ')
				print('')
				'''

				#Build the DebuggedBackFramesList
				DebuggedBackFramesList=[]
				DebuggedBackFrame=DebuggedCurrentFrame
				while DebuggedBackFrame.f_back!=None:
					#if hasattr(self,DebuggedBackFrame.f_back.f_code.co_name):
					if DebuggedBackFrame.f_back.f_code.co_name not in self.DebuggingNotFramedFunctionStringsList:
						DebuggedBackFramesList.append(DebuggedBackFrame.f_back)
					DebuggedBackFrame=DebuggedBackFrame.f_back

				#Reduce the DebuggedBackFramesList with the DebuggedCurrentFrame
				self.DebuggedFramesList=[DebuggedCurrentFrame]+DebuggedBackFramesList

				#Set the DebuggedBackFrameString
				DebuggedBackFrameString='\nFrom '+' | '.join(
						map(
							lambda __InspectedList:
							' '.join(__InspectedList),
							map(SYS.getInspectedListWithFrame,DebuggedBackFramesList)
						)
					)

				#Add the name of the function or method
				DebuggedString+=DebuggingHeadPrefixString+" ".join(
							[
								'/'.join(DebuggedCurrentFrame.f_code.co_filename.split('/')[-2:]),
								DebuggedCurrentFrame.f_code.co_name
							]
							)+DebuggedBackFrameString+DebuggingHeadSuffixString

			#Update the RepresentingDict
			SYS.RepresentingDict['AlineaString']=''.join([DebuggingElementString]*(len(self.DebuggedFramesList)-1))
			SYS.RepresentingDict['LineJumpString']=SYS.RepresentingDict['AlineaString']+'\n'

			#Debug
			'''
			print('self.DebuggedAlineaString is ',self.DebuggedAlineaString)
			print('SYS.RepresentingDict is ',SYS.RepresentingDict)
			print('')
			'''

			#Add the lineno
			DebuggedString+='l.'+str(DebuggedCurrentFrame.f_lineno)+' : '

			#Add the DebuggedString from the debugging variable
			DebuggedString+=getDebuggedStringWithDebuggingVariable(_DebuggingVariable)

			#Print
			SYS._print(DebuggedString+'\n')

			#Reinit
			SYS.reinitRepresentingDict()

#</DefineClass>

