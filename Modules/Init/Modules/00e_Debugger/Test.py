#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

class DebuggerTestClass(SYS.DebuggerClass):

	def test1(self):

		#Debug
		self.debug('I am in the test1 method')

		#Call the test2 method
		self.test2()

		#Debug
		self.debug('I am back in the test1 method')

	def test2(self):

		#Debug
		self.debug('I am in the test2 method')

		#Call the test3 method
		self.test3()

		#Debug
		self.debug('I am back in the test2 method')

	def test3(self):

		#Debug
		self.debug('I am in the test3 method')

		#Call the test4 method
		self.test4()

	def test4(self):

		#Debug
		self.debug('I am in the test4 method')

		#Debug
		self.debug('I am still in the test4 method')

#Call the test1
DebuggerTestClass().test1()


