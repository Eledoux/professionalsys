#<ImportModules>
import inspect
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DebuggerElementString='    '
DebuggerIsString=' is '
DebuggerHeadPrefixString='////////////////////////////////\n'
DebuggerHeadSuffixString='\n////////////////////////////////\n\n'
#</DefineLocals>

#<DefineClass>
class DebuggerClass(SYS.RepresenterClass):
	
	def initAfter(self):

		#<DefineSpecificDict>
		self.DebuggedAlineaString=""
		self.DebuggedIndentBool=False
		self.DebuggedFramesList=[]
		#</DefineSpecificDict>

	def debug(self,_DebuggingVariable):

		#Debug
		'''
		print('Debugger debug method')
		print('')
		'''

		#Define the DebuggedCurrentFrame
		DebuggedCurrentFrame=inspect.currentframe().f_back

		#Debug
		'''
		print('DebuggedCurrentFrame is ',DebuggedCurrentFrame)
		print('self.DebuggedFramesList is ',self.DebuggedFramesList)
		print('')
		'''

		#Init the DebuggedString
		DebuggedString=self.DebuggedAlineaString
		
		#Append maybe for the first time
		if len(self.DebuggedFramesList)==0:

			#Debug
			'''
			print('This is the first frame ')
			print('')
			'''

			#Add the name of the function or method
			DebuggedString=DebuggerHeadPrefixString+" ".join(
						SYS.getInspectedListWithFrame(DebuggedCurrentFrame)
						)+DebuggerHeadSuffixString

			#Append
			self.DebuggedFramesList.append(DebuggedCurrentFrame)

		#Check for the previous frame
		elif DebuggedCurrentFrame!=self.DebuggedFramesList[-1]:

			#Debug
			'''
			print('This is a new frame ')
			print('')
			'''

			#Get the DebuggedBackFrame
			#DebuggedBackFrameString="\n(From "+" ".join(
			#	SYS.getInspectedListWithFrame(self.DebuggedFramesList[-1])
			#	)+")"
			
			#DebuggedBackFrameString=""

			DebuggedBackFramesList=[DebuggedCurrentFrame.f_back]
			while DebuggedBackFramesList[-1].f_back!=None:
				DebuggedBackFramesList.append(DebuggedBackFramesList[-1].f_back)
			DebuggedBackFramesList.reverse()
			DebuggedBackFrameString='\n From '+' | '.join(
					map(
						lambda __InspectedList:
						' '.join(__InspectedList),
						map(SYS.getInspectedListWithFrame,DebuggedBackFramesList)
					)
				)
				
			#print(map(SYS.getInspectedListWithFrame,self.DebuggedFramesList))

			#Look for previous frames
			if len(self.DebuggedFramesList)>1:

				#If it was the in previous frames then indent back
				if DebuggedCurrentFrame in self.DebuggedFramesList[:-1]:

					#Get the IndexInt of the DebuggedBackFrame
					DebuggedCurrentFrameIndexInt=self.DebuggedFramesList.index(DebuggedCurrentFrame)

					#Debug
					'''
					print('Indent back ')
					print('DebuggedBackFrameIndexInt is ',DebuggedBackFrameIndexInt)
					print('')
					'''

					#Pop the last element
					self.DebuggedFramesList=self.DebuggedFramesList[:(DebuggedCurrentFrameIndexInt+1)]

					#Indent back
					self.DebuggedAlineaString=DebuggerElementString.join(
						self.DebuggedAlineaString.split(DebuggerElementString
							)[:(DebuggedCurrentFrameIndexInt+1)])

					#Add the name of the function or method and the corresponding file pathstring
					DebuggedString=self.DebuggedAlineaString+DebuggerHeadPrefixString+" ".join(
						SYS.getInspectedListWithFrame(self.DebuggedFramesList[
							DebuggedCurrentFrameIndexInt])
						)+DebuggedBackFrameString+DebuggerHeadSuffixString

				else:

					#Debug
					'''
					print('Indent forward ')
					print('')
					'''

					#Find the last debugged frame
					DebuggedBackFramesList=[DebuggedCurrentFrame.f_back]
					IsFramedBool=DebuggedBackFramesList[-1] in self.DebuggedFramesList[:-1]
					while IsFramedBool==False:
						DebuggedBackFramesList.append(DebuggedBackFramesList[-1].f_back)
						IsFramedBool=DebuggedBackFramesList[-1] in self.DebuggedFramesList[:-1]
						
					#Define the DebuggedBackFrameIndexInt
					DebuggedBackFrameIndexInt=self.DebuggedFramesList[:-1].index(DebuggedBackFramesList[-1])

					#Append
					self.DebuggedFramesList=self.DebuggedFramesList[
					:(DebuggedBackFrameIndexInt+1)]+DebuggedBackFramesList+[DebuggedCurrentFrame]

					#Indent not or a bit forward 
					self.DebuggedAlineaString=''.join([DebuggerElementString]*(len(self.DebuggedFramesList)-2))

					#Add the name of the function or method
					DebuggedString=self.DebuggedAlineaString+DebuggerHeadPrefixString+" ".join(
						SYS.getInspectedListWithFrame(DebuggedCurrentFrame)
						)+DebuggedBackFrameString+DebuggerHeadSuffixString

			else:

				#Debug
				'''
				print('Indent forward ')
				print('')
				'''

				#Append
				self.DebuggedFramesList.append(DebuggedCurrentFrame)

				#Indent forward
				self.DebuggedAlineaString+=DebuggerElementString

				#Add the name of the function or method
				DebuggedString=self.DebuggedAlineaString+DebuggerHeadPrefixString+" ".join(
						SYS.getInspectedListWithFrame(DebuggedCurrentFrame)
						)+DebuggedBackFrameString+DebuggerHeadSuffixString

		#Update the RepresentingDict
		SYS.RepresentingDict['AlineaString']=self.DebuggedAlineaString
		SYS.RepresentingDict['LineJumpString']=self.DebuggedAlineaString+'\n'

		#Debug
		'''
		print('self.DebuggedAlineaString is ',self.DebuggedAlineaString)
		print('SYS.RepresentingDict is ',SYS.RepresentingDict)
		print('')
		'''

		#Add the lineno
		DebuggedString+='l.'+str(DebuggedCurrentFrame.f_lineno)+' : '

		#Add the DebuggedString from the debugging variable
		DebuggedString+=SYS.getDebuggedStringWithDebuggingVariable(_DebuggingVariable)

		#Replace the line jumps with ones with alineas
		DebuggedString=DebuggedString

		#Print
		SYS._print(DebuggedString+'\n')

#</DefineClass>

