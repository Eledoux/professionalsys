#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
#</ImportModules>

#<DefineClass>
class UseClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.SeedInt=1
		self.AuthorString="Erwan Ledoux"
		self.TimeString=""
		self.SpaceString="Home"
		#</DefineSpecificDict>

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindSeedIntAfter(self):
		
		#Bind with Seed in the numpy
		np.random.seed(self.SeedInt)

	def hdfile(self):
		pass
	#</DefineBindingHookMethods>	
#</DefineClass>

#<DefineModelClass>

#Get the just defined Class
ModeledClass=filter(lambda Variable:type(Variable[1])==type,globals().items())[0][1]

#Define the OutputingKeyStringsList
ModeledClass.OutputingKeyStringsList=['SeedInt','AuthorString']

"""
class UseTableClass(tables.IsDescription):

	#Get the OutputingKeyStringsList for making them like a IsDescription Type
	for KeyString in ModeledClass.OutputingKeyStringsList:

		#Get the Col 
		Col=SYS.getColTypeStringWithKeyString(KeyString)
		if Col!=None:
			locals().__setitem__(KeyString,Col)

	#Delete the local variables 
	del Col
"""
from tables import *
class UseTableClass(IsDescription):
	SeedInt=IntCol64()
	AuthorString=StringCol(16)
#</DefineModelClass>