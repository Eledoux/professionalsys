#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
from tables import *
import os
#</ImportModules>

#<DefineLocals>
HookString="Config"
SeedInt=0
SeedingConfigPointer=None
IsSeedSettedBool=False
#</DefineLocals>

#<DefineFunctions>
def seed(_SeedInt):
	global SeedInt,SeedingConfigPointer
	SeedInt=_SeedInt
	np.random.seed(_SeedInt)
	SeedingConfigPointer=None
#</DefineFunctions>

#<DefineClass>
class ConfigClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.SeedInt=0
		self.UseInt=1
		self.AuthorString="Erwan Ledoux"
		self.TimeString=""
		self.SpaceString="Home"
		#</DefineSpecificDict>

		#Don't repr the SeedInt because it is going to change for each test...
		self.__setitem__('+NotRepresentingGettingVariablesList',['SeedInt'])
		
		#Define the features
		self.FeaturingTuplesList=[
									#ColumnString 		#GettingVariable 	#Col 			#ScanningList
									('UseInt',			'UseInt',			Int64Col(),			[1,2]),
									('AuthorString',	'AuthorString',		StringCol(16),	["Erwan Ledoux"])
								]

		#Define the outputs
		self.OutputingTuplesList=[
									#ColumnString 	#GettingVariable 	#Col 
									('SeedInt',		'SeedInt',		Int64Col())
								]

	def reinitAfter(self,**_ReInitiatingVariablesDict):
	
		#Reset the Seed of the Config Module
		self.SeedInt=0
		SYS.Config.seed(0)

	def bindSeedIntAfter(self):

		#Bind with Seed in the numpy
		np.random.seed(self.SeedInt)

	def outputAfter(self,**_LocalOutputingVariablesDict):

		if SYS.Config.SeedInt==0:

			if SYS.Config.SeedingConfigPointer==None:
	
				#Pick a random to make the Seed
				self['SeedInt']=(int)(1000.*np.random.rand())

				#Then set it to the level of the module and therefore freeze this Seed for the others config
				SYS.Config.SeedInt=self.SeedInt

		else:

			#Give the Seed of the module Config
			self.SeedInt=np.random.mtrand.get_state()[1][0]

		#Say which Config has Seeded
		SYS.Config.SeedingConfigPointer=self

	
	#</DefineHookMethods>
	
#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.ConfigClass()

def attest_output():

	#Append a default output
	SYS.Config.seed(5)

	#Return the object __repr__
	TestedVariable=SYS.ConfigClass().output()
	print(TestedVariable)
	return TestedVariable

def attest_store():

	#Flush the default output
	SYS.Config.seed(5)
	Config=SYS.ConfigClass().organize().output().store().output().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Config.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

def attest_scan():

	#Flush with all the scanning featuring values
	Config=SYS.ConfigClass().organize().scan().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Config.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable
#</DefineAttestingFunctions>
