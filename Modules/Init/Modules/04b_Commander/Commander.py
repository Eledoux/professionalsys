#<ImportModules>
import os
import ShareYourSystem as SYS
import six
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Executer"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class CommanderClass(BasedLocalClass):

	def commandAllSetsForEach(self,_SettingVariableTuplesList,_GatheringVariablesList):
		"""Collect with _GatheringVariablesList and do a all sets for each with _SettingVariableTuplesList"""

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Get the GatheredVariablesList
		GatheredVariablesList=self.gather(_GatheringVariablesList)

		#Debug
		'''
		self.debug(('',vars(),['_GatheringVariablesList','GatheredVariablesList']))
		'''

		if GatheredVariablesList!=None:

			#Unzip
			GatheredVariablesList=zip(*GatheredVariablesList)
			if len(GatheredVariablesList)>0:

				#Just keep the values
				GatheredVariablesList=GatheredVariablesList[1]

				#For each __GatheredVariable it is updating with _SettingVariableTuplesList
				map(
						lambda __GatheredVariable:
						__GatheredVariable.update(_SettingVariableTuplesList),
						GatheredVariablesList
					)

		#Return self
		return self 

	def commandEachSetForAll(self,_SettingVariableTuplesList,_GatheringVariablesList):
		"""Collect with _GatheringVariablesList and do a each set for all with _SettingVariableTuplesList"""

		#Get the GatheredVariablesList
		GatheredVariablesList=zip(*self.gather(_GatheringVariablesList))
		if len(GatheredVariablesList)>0:

			#Just keep the values
			GatheredVariablesList=GatheredVariablesList[1]

			#For each SettingTuple it is setted in _GatheredVariablesList
			map(
					lambda __SettingVariableTuple:
					map(
						lambda __GatheredVariable:
						__GatheredVariable.__setitem__(*__SettingVariableTuple),
						GatheredVariablesList
						),
					_SettingVariableTuplesList
				)

		#Return self
		return self 
#</DefineClass>

#<DefineAttestingFunctions>
def attest_commandAllSetsForEach():

	return SYS.CommanderClass().__add__(
			[
				SYS.CommanderClass().update(
					[
						('AppendingNodeString',"Structure"),
						('StructuredKeyString',str(Int1))
					]) for Int1 in xrange(2)
			]
		).execute('self.__class__.CountingInt=0').commandAllSetsForEach(
				[
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.SettingValueVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					),
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.SettingValueVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					)
				],
				[
					['/'],
					'<Structure>'
				])

def attest_commandEachSetForAll():
	
	return SYS.CommanderClass().__add__(
			[
				SYS.CommanderClass().update(
					[
						('AppendingNodeString',"Structure"),
						('StructuredKeyString',str(Int1))
					]) for Int1 in xrange(2)
			]
		).execute('self.__class__.CountingInt=0').commandEachSetForAll(
				[
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.SettingValueVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					),
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.SettingValueVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					)
				],
				[
					['/'],
					'<Structure>'
				])
#</DefineAttestingFunctions>
