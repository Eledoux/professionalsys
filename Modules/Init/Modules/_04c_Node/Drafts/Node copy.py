#<ImportModules>
import collections
import copy
import os
import ShareYourSystem as SYS
import six
import sys
#</ImportModules>

#<DefineLocals>
AddShortString="+"
AttestString='attest'
AttributeShortString="."
DeepShortString="/"
AppendShortString="App_"
PointerShortString="Pointer"
CopyShortString='Cop_'
CollectShortString=":"
ExecShortString="Exec_"
NodeString="_"
#</DefineLocals>

#<DefineClass>
class NodeClass(SYS.TesterClass):
		
	def initAfter(self):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""		

		#<DefineSpecificDict>
		self.IsGettingBool=True   				#<NotRepresented>
		self.GettingVariable=None 				#<NotRepresented>
		self.GettedVariable=None  				#<NotRepresented>
		self.IsSettingBool=True   				#<NotRepresented>
		self.SettingVariable=None 				#<NotRepresented>
		self.SettedVariable=None  				#<NotRepresented>
		self.IsDeletingBool=True  				#<NotRepresented>
		self.DeletingVariable=None  			#<NotRepresented>
		self.NodifiedOrderedDict=None 			#<NotRepresented>
		self.NodifiedNodeString=""				#<NotRepresented>
		self.NodifiedNodedString=""				#<NotRepresented>
		self.NodifiedNodingString=""			#<NotRepresented>
		self.NodifiedKeyStringKeyString="" 		#<NotRepresented>
		self.NodifiedKeyString="" 				#<NotRepresented>
		self.NodifiedParentPointer=None 		#<NotRepresented>
		self.NodifiedInt=-1 					#<NotRepresented>
		self.NodifiedPathString="" 				#<NotRepresented>
		self.NodifiedGrandParentPointersList=[] #<NotRepresented>
		#</DefineSpecificDict>

	def __getitem__(self,_GettingVariable):
		"""Call the get<HookString> methods and return self.GettedVariable (None by default)"""
		
		#Refresh the attributes
		self.GettingVariable=_GettingVariable
		self.GettedVariable=None
		self.IsGettingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="get"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Debug
					#debug("HookingMethodString is : "+HookingMethodString)
					#debug("self.GettedVariable is : "+repr(self.GettedVariable))

					#Check Bool
					if self.IsGettingBool==False:
						return self.GettedVariable

		#Return GettedVariable
		return self.GettedVariable

	def __setitem__(self,_SettingVariable,_SettedVariable):
		"""Call the set<HookString> methods"""

		#Refresh the attributes
		self.SettingVariable=_SettingVariable
		self.SettedVariable=_SettedVariable
		self.IsSettingBool=True

		#Sort the HookMethodStrings
		for OrderString in ["Before","After"]:

			#Define a Local IsSettedBool
			IsSettedBool=False

			if type(_SettingVariable) in [str,unicode]:
			
				#Define the BindingMethodString
				BindingMethodString="bind"+_SettingVariable+OrderString
				
				#Check that there is HookingMethods for it
				if BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict:
					
					#Say ok
					IsSettedBool=True

					#Call the specific Appended methods 
					for HookingMethod,BindingMethod in self.__class__.BindingMethodStringToMethodTuplesListDict[BindingMethodString]:

						#Call the HookMethod
						if callable(HookingMethod):
							HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

						#Call the Binding
						if callable(BindingMethod):
							OutputVariable=BindingMethod(self)

							#End the Setting
							if self.IsSettingBool==False:
								return self

			#If a binding method was not yet found then look for a global set method
			if IsSettedBool==False:

				#_SettingVariable is not a string
				HookingMethodString="set"+OrderString

				#Check that there is HookingMethods for it
				if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

					#Call the specific Appended methods 
					for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

						#Call the HookingMethod
						HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

		#Return self
		return self

	def __delitem__(self,_DeletingVariable):
		"""Call the del<HookString> methods and delete in the __dict__ by default"""
		
		#Refresh the attributes
		self.DeletingVariable=_DeletingVariable
		self.IsDeletingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="delete"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsDeletingBool==False:
						return self

		#Return self
		return self

	def nodify(self,_NodeString,**_LocalNodifyingVariablesDict):

		#Debug
		'''
		print('nodify ',_NodeString)
		print('_LocalNodifyingVariablesDict is ',_LocalNodifyingVariablesDict)
		print('')
		'''

		#Get the NodifiedString
		self.NodifiedNodeString=_NodeString
		self.NodifiedNodedString=SYS.getDoneStringWithDoString(_NodeString)
		self.NodifiedNodingString=SYS.getDoingStringWithDoString(_NodeString)
	
		#Set the Nodified OrderedDict and KeyString
		NodifiedOrderedDictKeyString=self.NodifiedNodedString+'OrderedDict'
		self.NodifiedKeyStringKeyString=self.NodifiedNodedString+'KeyString'

		try:
			self.NodifiedOrderedDict=getattr(self,NodifiedOrderedDictKeyString)
		except AttributeError:
			self.__setattr__(NodifiedOrderedDictKeyString,collections.OrderedDict())
			self.NodifiedOrderedDict=getattr(self,NodifiedOrderedDictKeyString)

		try:
			self.NodifiedKeyString=getattr(self,self.NodifiedKeyStringKeyString)
		except AttributeError:
			self.__setattr__(self.NodifiedKeyStringKeyString,"")
			self.NodifiedKeyString=getattr(self,self.NodifiedKeyStringKeyString)

		#Debug
		'''
		print("_NodeString is : ",_NodeString)
		print("_LocalNodifyingVariablesDict is : ",_LocalNodifyingVariablesDict)
		'''

		#If this is a set aof a tree of nodes then also init the nodifying attribyues	
		if 'IsNodeBool' not in _LocalNodifyingVariablesDict or _LocalNodifyingVariablesDict['IsNodeBool']:

			NodifiedParentPointerKeyString=self.NodifiedNodedString+'ParentPointer'
			NodifiedIntKeyString=self.NodifiedNodedString+'Int'
			NodifiedPathStringKeyString=self.NodifiedNodedString+'PathString'
			NodifiedGrandParentPointersListKeyString=self.NodifiedNodedString+'GrandParentPointersList'

			try:
				self.NodifiedInt=getattr(self,NodifiedIntKeyString)
			except AttributeError:
				self.__setattr__(NodifiedIntKeyString,-1)
				self.NodifiedInt=getattr(self,NodifiedIntKeyString)

			try:
				self.NodifiedParentPointer=getattr(self,NodifiedParentPointerKeyString)
			except AttributeError:
				self.__setattr__(NodifiedParentPointerKeyString,None)
				self.NodifiedParentPointer=getattr(self,NodifiedParentPointerKeyString)

			try:
				self.NodifiedPathString=getattr(self,NodifiedPathStringKeyString)
			except AttributeError:
				self.__setattr__(NodifiedPathStringKeyString,"/")
				self.NodifiedPathString=getattr(self,NodifiedPathStringKeyString)

			try:
				self.NodifiedGrandParentPointersList=getattr(self,NodifiedGrandParentPointersListKeyString)
			except AttributeError:
				self.__setattr__(NodifiedGrandParentPointersListKeyString,[])
				self.NodifiedGrandParentPointersList=getattr(self,NodifiedGrandParentPointersListKeyString)


	def append(self,_AppendingVariable):
		"""Call __setitem__ with a defined SettingVariable contained in the _AppendingVariable"""
		
		#Init the SettingVariable and Add the NodifyingString
		SettingVariable=AppendShortString

		#TuplesList Case
		if SYS.getIsTuplesListBool(
			_AppendingVariable) and SYS.getCommonSuffixStringWithStringsList(
			[_AppendingVariable[0][0],'KeyString'])=='KeyString':
				
				#Add the hook version
				SettingVariable+=SYS.getDoStringWithDoneString(
					_AppendingVariable[0][0].split('KeyString')[0])+NodeString+_AppendingVariable[0][1]

		else:

			#Object Case
			if SYS.NodeClass in type(_AppendingVariable).__mro__ :

				Dict=_AppendingVariable.__dict__

			#dict Case
			elif type(_AppendingVariable)==dict:

				Dict=_AppendingVariable

			#Get the KeyStringsList
			GoodKeyTuplesList=filter(
						lambda __ItemTuple:
						SYS.getCommonSuffixStringWithStringsList(
							[__ItemTuple[0],'KeyString']
						)=='KeyString' and __ItemTuple[1]!="",
						Dict.items()
					)

			#If there is a good KeyString
			if len(GoodKeyTuplesList)==1:
				SettingVariable+=SYS.getDoStringWithDoneString(
					GoodKeyTuplesList[0][0].split('KeyString')[0])+NodeString+GoodKeyTuplesList[0][1]

		#Then set
		if SettingVariable==AppendShortString:
			SettingVariable+='Sort'+NodeString
		return self.__setitem__(SettingVariable,_AppendingVariable)

	def parentize(self,_ParentingString):

		#Debug
		'''
		print('parentize method')
		if hasattr(self,"StructuredKeyString"):
			print('self.StructuredKeyString is ',self.StructuredKeyString)
		print('_ParentingString is : ',_ParentingString)
		print('')
		'''

		#Check that this is a good string
		if _ParentingString!='':

			#Nodify
			self.nodify(_ParentingString)

			#Check ofr a parent pointer
			if self.NodifiedParentPointer!=None:

				#Nodify the parent
				self.NodifiedParentPointer.nodify(_ParentingString)

				#Set the GrandParentPointersList
				GrandParentPointersListKeyString=self.NodifiedNodedString+"GrandParentPointersList"
				setattr(
							self,
							GrandParentPointersListKeyString,
							[self.NodifiedParentPointer]+self.NodifiedParentPointer.NodifiedGrandParentPointersList
						)
				self.NodifiedGrandParentPointersList=getattr(self,GrandParentPointersListKeyString)

				#Set the GrandParentKeyStringsList
				KeyStringsListKeyString=self.NodifiedNodedString+"GrandParentKeyStringsList"
				self.__setattr__(
									KeyStringsListKeyString,
									map(
											lambda __GrandParentPointer:
											getattr(__GrandParentPointer,self.NodifiedKeyStringKeyString),
											self.NodifiedGrandParentPointersList
										)
							)

				#Set the PathStringsList
				PathStringsListKeyString=self.NodifiedNodedString+"PathStringsList"
				PathStringsList=[getattr(self,self.NodifiedKeyStringKeyString)]+copy.copy(
										getattr(self,KeyStringsListKeyString)
									)
				self.__setattr__(
									PathStringsListKeyString,
									PathStringsList
								)
				getattr(self,PathStringsListKeyString).reverse()

				#Set the PathString
				PathStringKeyString=self.NodifiedNodedString+"PathString"
				PathString=DeepShortString.join(getattr(self,PathStringsListKeyString))
				self.__setattr__(
									PathStringKeyString,
									PathString
								)
				self.NodifiedPathString=PathString

			else:

				#Debug
				'''
				print('parentize ',self.NodifiedKeyString)
				print('self.NodifiedParentPointer is None')
				print('')
				'''
				pass

		#Return self
		return self

	def getBefore(self):
		"""
			Hook in the __getitem__ of the Object 
			with different possible ShortStrings

			If _GettingVariable is an int, it returns the Value in the corresponding Index
			in self.AppendedValueVariablesList

			If _GettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			If _GettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			Else do a __getitem__ in the __dict__
		"""

		#Debug
		#debug("self.GettingVariable is : "+repr(self.GettingVariable))
		#debug("self.GettedVariable is : "+repr(self.GettedVariable))

		#Init a local IsGettedBool
		IsGettedBool=False

		#Case of a GettingString
		if type(self.GettingVariable) in [str,unicode]: 

			#Get with ExecShortString (it can be a return of a method call)
			if SYS.getCommonPrefixStringWithStringsList(
				[self.GettingVariable,ExecShortString])==ExecShortString:

				#Define the ExecString
				ExecString=self.GettingVariable.split(ExecShortString)[1]

				#Put the output in a local Local Variable
				six.exec_(ExecString,locals())

				#Give it to self.GettedVariable
				self.IsGettingBool=False
				return 

			#Get an Attribute (it can be a return of a method call)
			elif AttributeShortString in self.GettingVariable:

				#Define the GettingStringsList
				GettingStringsList=self.GettingVariable.split(AttributeShortString)

				#Look if the deeper getter is Ok
				GetterVariable=self[GettingStringsList[0]]

				#Define the next getting String
				NextGettingString=GettingStringsList[1]

				#If the GettingStringsList has brackets in the end it means a call of a method
				if "()"==NextGettingString[-2:]:
					NextGettingString=NextGettingString[:-2]

					#Case where it is an object
					if hasattr(GetterVariable,NextGettingString):
						self.GettedVariable=getattr(GetterVariable,NextGettingString)()
						self.IsGettingBool=False
						return 

					elif SYS.getIsTuplesListBool(GetterVariable):

						if NextGettingString=='keys':
							self.GettedVariable=map(
														lambda __ListedTuple:
														__ListedTuple[0],
														GetterVariable
													)
							self.IsGettingBool=False
							return 
						if NextGettingString=='values':
							self.GettedVariable=map(	
														lambda __ListedTuple:
														__ListedTuple[1],
														GetterVariable
													)
							self.IsGettingBool=False
							return 

				elif hasattr(GetterVariable,NextGettingString):

					#Else it is a get of an item in the __dict__
					GettingVariable=getattr(GetterVariable,NextGettingString)

					#Case where it has to continue to be getted
					if len(GettingStringsList)>2:
						GettingVariable[AttributeShortString.join(GettingStringsList[2:])]
						self.IsGettingBool=False
						return 
					else:

						#Else return the GettingVariable
						self.GettedVariable=GettingVariable
						self.IsGettingBool=False
						return 

			#Get with DeepShortString
			elif self.GettingVariable==DeepShortString:
				
				#If it is directly DeepShortString
				self.GettedVariable=self

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return 

			#Get in a ordered dict
			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,AppendShortString])==AppendShortString:

				#Define the appended NodifyingKeyString
				AppendingString=self.GettingVariable.split(AppendShortString)[1]

				#Define the appended NodeString and KeyString
				SplittedStringsList=AppendingString.split(NodeString)
				NodifyingString=SplittedStringsList[0]
				
				#Nodify
				self.nodify(NodifyingString,**{'IsNodeBool':False})

				#Get the KeyString
				KeyString=NodeString.join(SplittedStringsList[1:])

				#Get with a digited KeyString case
				if KeyString.isdigit():

					#Define the GettingInt
					GettingInt=(int)(KeyString)

					#Check if the size is ok
					if GettingInt<len(self.NodifiedOrderedDict):

						#Get the GettedVariable 
						self.GettedVariable=SYS.get(self.NodifiedOrderedDict,'values',GettingInt)
						#Stop the getting
						self.IsGettingBool=False

						#Return
						return

				#Get in the ValueVariablesList
				elif KeyString in self.NodifiedOrderedDict:
					
					#Get the GettedVariable
					self.GettedVariable=self.NodifiedOrderedDict[KeyString]

					#Stop the getting
					self.IsGettingBool=False

					#Return 
					return
			
			#Get with '/'
			elif self.GettingVariable==DeepShortString:
				
				#If it is directly '/'
				self.GettedVariable=self

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return 

			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,DeepShortString])==DeepShortString:

				#Define the GettingVariableStringsList
				GettingVariableStringsList=DeepShortString.join(self.GettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
				GettedVariable=self[GettingVariableStringsList[0]]
				if GettedVariable!=None:

					if len(GettingVariableStringsList)==1:

						#Direct update in the Child or go deeper with the ChildPathString
						self.GettedVariable=GettedVariable

						#Stop the getting
						self.IsGettingBool=False
					
						#Return 
						return 

					else:

						#Define the ChildPathString
						ChildPathString=DeepShortString.join(GettingVariableStringsList[1:])	

						#Get in the first deeper Element with the ChildPathString
						self.GettedVariable=GettedVariable[ChildPathString]

						#Stop the getting
						self.IsGettingBool=False

						#Return
						return 

			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,CopyShortString])==CopyShortString:

				#deepcopy
				self.GettedVariable=copy.deepcopy(
						self[CopyShortString.join(self.GettingVariable.split(CopyShortString)[1:])]				
					)

				#Stop the getting
				self.IsGettingBool=False

				#Return 
				return 

			elif self.GettingVariable=='UtilitiesDict':
				self.GettedVariable=self.__dict__
				self.IsGettingBool=False
				return 
			elif self.GettingVariable=='MethodsDict':
				self.GettedVariable=SYS.getMethodsDictWithClass(self.__class__)
				self.IsGettingBool=False
				return  
			elif self.GettingVariable=="__class__":
				self.GettedVariable=self.__class__.__dict__
				self.IsGettingBool=False
				return

		#Do the minimal get in the Utilities
		if IsGettedBool==False:
		
			if type(self.GettingVariable) in [str,unicode]:

				#Get safely the Value
				if self.GettingVariable in self.__dict__:

					#__getitem__ in the __dict__
					self.GettedVariable=self.__dict__[self.GettingVariable]

					#Stop the getting
					self.IsGettingBool=False

	def setBefore(self):
		"""
			Hook in the __setitem__ of the Object 
			with different possible ShortStrings

			If _SettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the setting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is <camelCase> then it is a hook query...
			so it calls with the form self(<camelCase>,SettedVariable)

			If _SettingVariable is of the form <String1>Pointer and _SettedVariable of the form /<String2>
			it gets the self[<String2>] and do self[<String1>Pointer]=self[<String2>]

			Else do a __setitem__ in the __dict__ 
		"""

		#Debug
		#print('set',self.SettingVariable)

		#Case of a SettingString
		if type(self.SettingVariable) in [str,unicode]:

			#Adding set
			if SYS.getCommonPrefixStringWithStringsList(
				[self.SettingVariable,AddShortString])==AddShortString:

				#Define the added GettingString
				GettingString=self.SettingVariable.split(AddShortString)[1]

				#Get the added Variable
				Variable=self[GettingString]

				#Add
				if hasattr(Variable,'__add__'):
					Variable+=self.SettedVariable

				#Return
				return

			#Appending set
			elif SYS.getCommonPrefixStringWithStringsList(
				[self.SettingVariable,AppendShortString])==AppendShortString:

				#Define the appended NodifyingKeyString
				NodifyingKeyString=self.SettingVariable.split(AppendShortString)[1]

				#Define the appended NodeString,KeyString,HookedNodeString,OrderedDictKeyString
				SplittedStringsList=NodifyingKeyString.split(NodeString)
				NodifyingString=SplittedStringsList[0]

				#Check if it is an append of Nodes
				IsNodeBool=SYS.NodeClass in type(self.SettedVariable).__mro__

				#Nodify
				self.nodify(NodifyingString,**{'IsNodeBool':IsNodeBool})

				#Get the KeyString
				KeyString=NodeString.join(SplittedStringsList[1:])

				#Append (or set if it is already in)
				self.NodifiedOrderedDict[KeyString]=self.SettedVariable

				#If it is an object
				if IsNodeBool:

					#Int and Set Child attributes
					self.SettedVariable.__setattr__(self.NodifiedNodedString+'Int',len(self.NodifiedOrderedDict)-1)
					NodifiedStringKeyString=self.NodifiedNodedString+'KeyString'
					self.SettedVariable.__setitem__(NodifiedStringKeyString,KeyString)
					self.SettedVariable.__setattr__(self.NodifiedNodedString+'ParentPointer',self)

					#Init GrandChild attributes
					self.SettedVariable.__setattr__(self.NodifiedNodedString+'PathString',"")
					self.SettedVariable.__setattr__(self.NodifiedNodedString+'GrandParentPointersList',[])

				#Return 
				return

			#Deep set
			elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,DeepShortString])==DeepShortString:

				#Get the SettingVariableStringsList
				SettingVariableStringsList=DeepShortString.join(
					self.SettingVariable.split(DeepShortString)[1:]
					).split(DeepShortString)
				GettedVariable=self[SettingVariableStringsList[0]]
				if GettedVariable!=None:
					
					#Debug
					#print('GettedVariable is',GettedVariable)

					#Define the ChildPathString
					ChildPathString='/'+DeepShortString.join(SettingVariableStringsList[1:])	

					#Debug
					#print('ChildPathString is',ChildPathString)

					#Direct update in the Child or go deeper with the ChildPathString
					if ChildPathString=="/": 

						#Case where it is an object to set inside
						if self.__class__ in type(GettedVariable).__mro__:

							#Modify directly the GettedVariable with self.SettedVariable
							GettedVariable.__setitem__(*self.SettedVariable)

						#Case where it is an object to append inside
						elif SYS.getCommonPrefixStringWithStringsList(
							[SettingVariableStringsList[0],AppendShortString]
							)==AppendShortString:
							
							#Define the appended NodifyingKeyString
							NodifyingKeyString=self.SettingVariable.split(AppendShortString)[1]

							#Define the appended NodeString and KeyString
							SplittedStringsList=NodifyingKeyString.split(NodeString)
							NodifyingString=SplittedStringsList[0]

							#Nodify
							self.nodify(NodifyingString)

							#Get the KeyString
							KeyString=NodeString.join(SplittedStringsList[1:])

							#Append without binding
							if KeyString in self.NodifiedOrderedDict:
								self.NodifiedOrderedDict[KeyString]=self.SettedVariable

						#Case where it is a set at the level of self of an already setted thing
						else:

							self[SettingVariableStringsList[0]]=self.SettedVariable
					else:

						#Debug
						'''
						print('GettedVariable is ',GettedVariable)
						print('ChildPathString is ',ChildPathString)
						print('')
						'''

						if type(GettedVariable) in [dict,collections.OrderedDict]:

							#Debug
							'''
							print('GettedVariable is ',GettedVariable)
							print('ChildPathString is ',ChildPathString)
							print('')
							'''

							#Call the setWithDictatedVariableAndKeyVariable
							SYS.setWithDictatedVariableAndKeyVariable(
								GettedVariable,
								ChildPathString,
								self.SettedVariable,
								**{'DeepShortString':DeepShortString}
							)

						else:

							#Set in the first deeper Element with the ChildPathString
							GettedVariable[ChildPathString]=self.SettedVariable


				#Case where it is a set at the level of self of new setted thing
				else:

					self[SettingVariableStringsList[0]]=self.SettedVariable

				#Return 
				return 

			#Call for a hook
			elif (self.SettingVariable[0].isalpha() or self.SettingVariable[:2]=="__") and  self.SettingVariable[0].lower()==self.SettingVariable[0]:

				#Get the Method
				if hasattr(self,self.SettingVariable):

					#Get the method
					SettingMethod=getattr(self,self.SettingVariable)

					#Adapt the shape of the args
					SYS.getWithMethodAndArgsList(
													SettingMethod,
													self.SettedVariable['ArgsVariable'] if 'ArgsVariable' in self.SettedVariable else [],
													**self.SettedVariable['KwargsDict'] if 'KwargsDict' in self.SettedVariable else {}
												)
				
				elif len(self.SettedVariable)==1:

					#Adapt format of the ArgsList
					self(self.SettingVariable,self.SettedVariable[0])
				else:
					self(self.SettingVariable,*self.SettedVariable)

				#Return
				self.IsSettingBool=False
				return

			elif type(self.SettedVariable) in [str,unicode]:

				if SYS.getCommonSuffixStringWithStringsList(
					[self.SettingVariable,PointerShortString])==PointerShortString:
					
					#The Value has to be a deep short string for getting the pointed variable
					if SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,DeepShortString])==DeepShortString:
						GettedVariable=self[self.SettedVariable]
						if GettedVariable!=None:
							#Link the both with a __setitem__
							self[self.SettingVariable]=GettedVariable

					#Return in the case of the Pointer and a string setted variable
					return

				#Get with ExecShortString (it can be a return of a method call)
				elif SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,ExecShortString])==ExecShortString:

					self[self.SettingVariable]=self[self.SettedVariable]
					self.IsSettingBool=False
					return

			#__setitem__ in the __dict__, this is an utility set
			self.__dict__[self.SettingVariable]=self.SettedVariable

	def deleteBefore(self):
		"""
			Hook in the __delitem__ of the Object
		"""

		#Do the minimal delitem
		if type(self.DeletingVariable) in [str,unicode]:

			#Del Safely the Value
			if self.DeletingVariable in self.__dict__:
				del self.__dict__[self.DeletingVariable]
#</DefineClass>

#<DefineAttestingFunctions>
def attest_setInNotOrderedDict():

	#Explicit expression
	Node=SYS.NodeClass().__setitem__('MyInt',0)
	
	#Set with a deep short string
	Node.__setitem__('MyNode',SYS.NodeClass()).__setitem__('/MyNode/MyString','hello')

	#Set with a deep deep short string
	Node.__setitem__('/MyNode/MyGrandOtherNode',SYS.NodeClass())

	#Set with a pointer short string
	return Node.__setitem__('MyNodePointer','/MyNode')

def attest_setInOrderedDict():

	#Short expression and set in the appended manner
	Node=SYS.NodeClass().__setitem__('App_Sort_MySecondInt',1)

	#Short expression for setting in the append without binding
	Node['/App_Sort_MySecondInt']+=1

	#Short expression for setting in the appended manner a structured object
	Node['App_Group_MyAppendedNode']=SYS.NodeClass()
	
	#Set with a deep short string and append string
	Node['/App_Group_MyAppendedNode/MyString']="bonjour"

	#Set with a deep deep short string and append string
	Node['/App_Group_MyAppendedNode/App_Group_MyGrandChildNode']=SYS.NodeClass()

	#Return Node
	return Node

def attest_setWithMethodString():

	#Set for being a call of method
	return SYS.NodeClass().__setitem__('__setitem__',{'ArgsVariable':('MySecondInt',1)})

def attest_append():

	#Append with a TuplesList
	Node=SYS.NodeClass().append([
										('StructuredKeyString',"MyTuplesList"),
										('MyString',"Hello")
									]
									)

	#Append with a dict
	Node.append({
						'StructuredKeyString':"MyDict",
						'MyOtherString':"Bonjour"
						}
					)

	#Append with an Node
	return Node.append(SYS.NodeClass())

#</DefineAttestingFunctions>

