#<ImportModules>
import collections
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
JoiningString='_'
#</DefineLocals>

#<DefineClass>
class JoinerClass(SYS.TablorClass):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.JoinedVariablesList=[]
		#</DefineSpecificDict>

	def tabularAfter(self,**_TabularingVariablesDict):

		#Maybe it is joined
		if 'JoinedOrderedDict' in self.ModeledDict:
			
			self.join(self.ModeledDict['JoiningString'])

			'''
			self.TabularedRowedTuplesList+=zip( 
												map(
														lambda __JoiningChildObject:
														'ModeledJoinList'+getattr(
																__JoiningChildObject,
																DoneJoiningKeyStringKeyString
															),
														self.ModeledDict['JoinedOrderedDict'].values()
													),
												self.JoinedModeledFeaturedIntsTuplesList
											)+[('ModeledJoinList',self.ModeledJoinList)]
			'''
			
			pass
	#</DefineHookMethods>

	def join(self,_JoiningString,_GatheringVariablesList,**_LocalJoiningVariablesDict):

		#Debug
		print('join method')
		print('_JoiningString is ',_JoiningString)

		if _LocalJoiningVariablesDict=={}:
			_LocalJoiningVariablesDict['IsJoiningBool']=False

		#Nodify
		self.nodify(_JoiningString)
				
		#Debug
		print('join',self.NodifiedKeyString)
		print('For the deeper child by the way it is already joined')
		print('self.JoinedVariablesList',self.JoinedVariablesList)
		print('')
		
		if len(self.NodifiedOrderedDict)==0 or _LocalJoiningVariablesDict['IsJoiningBool']:

			'''
			print('join',self.NodifiedKeyString)
			print('We have the right to join !')
			print('len(JoiningOrderedDict)',len(JoiningOrderedDict))
			print("_LocalJoiningVariablesDict['IsJoiningBool']",
				_LocalJoiningVariablesDict['IsJoiningBool'])
			print('')
			'''

			ParentPointer=getattr(self,self.NodifiedString+'ParentPointer')
			if ParentPointer!=None:

				'''
				print('join',self.NodifiedKeyString)
				print('ParentPointer exists')
				print('')
				'''

				Int=getattr(self,self.NodifiedString+"Int")
				if Int==0:

					'''
					print('join',self.NodifiedKeyString)
					print('This is the first child so init 
						the ParentPointer.JoinedVariablesList')
					print('')
					'''

					ParentPointer.JoinedVariablesList=[""]*len(
						getattr(ParentPointer,self.NodifiedString+"OrderedDict"))	

				
				'''
				#Get the GatheredVariablesList
				GatheredVariablesList=self.gather(_GatheringVariablesList)

				print('join',self.NodifiedKeyString)
				print('append in the ParentPointer.JoinedVariablesList[Int]')
				print('Int is ',str(Int))
				print('GatheredVariablesList is ',GatheredVariablesList)
				
				#Set in the parent pointer
				ParentPointer.JoinedVariablesList[Int]=JoiningString.join(
					map(
							lambda __JoiningTuple:
							'('+str(__JoiningTuple[0])+','+str(__JoiningTuple[1])+')',
							GatheredVariablesList
						)
				)
				'''

				

			else:

				print('join',self.NodifiedKeyString)
				print('ParentPointer is None')	
				
		else:

			print('join',self.NodifiedKeyString)
			print('This either a not last level of child or it is not yet authorized to join')
			print('len(self.NodifiedOrderedDict.values()) is ',len(self.NodifiedOrderedDict.values()))
			print("_LocalJoiningVariablesDict['IsJoiningBool']",_LocalJoiningVariablesDict['IsJoiningBool'])
			print('so join the deeper children groups first')
			print('')

			map(
					lambda __JoiningObject:
					__JoiningObject.join(
											_JoiningString,
											_GatheringVariablesList,
											**_LocalJoiningVariablesDict
										),
					self.NodifiedOrderedDict.values()
				)

			'''
			print('join',self.NodifiedKeyString)
			print('The deeper children groups are joined now')
			print('So join here !')
			print('')
			'''

			self.join(
						_JoiningString,
						_GatheringVariablesList,
						**dict(
									_LocalJoiningVariablesDict,**{'IsJoiningBool':True}
								)
						)

		'''
		print("join END for ",self.NodifiedKeyString)
		print('self.JoinedModeledIntsTuplesList',self.JoinedModeledIntsTuplesList)
		print('')
		'''

#</DefineClass>

#<DefineAttestingFunctions>
def attest_join():

	'''
	#Build a grouped structure
	Joiner=SYS.JoinerClass().update(
								[
									(
										'App_Structure_ChildJoiner1',
										SYS.JoinerClass().update(
										[
											(
												'App_Structure_GrandChildJoiner1',
												SYS.JoinerClass()
											)
										])
									),
									(
										'App_Structure_ChildJoiner2',
										SYS.JoinerClass().update(
											[]
										)
									)
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('join',{'ArgsVariable':[
																				"Structure",
																				[
																					["StructuredKeyString"]
																				]
																			]
															}
													)
												]
											}
							).close()
	'''

	#Build Hdf groups
	Tablor=SYS.TablorClass().hdformat().update(
								[
									(
										'App_Structure_ChildGrouper1',
										SYS.GrouperClass().update(
										[
											(
												'App_Structure_GrandChildTablor1',
												SYS.TablorClass()
											)
										])
									)
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':"Structure"})
												]
											}
									).update(
								map(
										lambda __Tuple:
										(
											'/App_Structure_ChildGrouper1/App_Structure_GrandChildTablor1/'+__Tuple[0],
											__Tuple[1]
										),
										[
											('MyInt',0),
											('MyString',"hello"),
											('UnitsInt',3),
											('MyIntsList',[2,4,1]),
											('MyFloat',0.1),
											('MyFloatsList',[2.3,4.5,1.1]),
											('App_Scan_ChildModeler',SYS.ModelerClass()),
											('App_Model_FeaturingDict',
														{
															'ColumningTuplesList':
															[
																('MyInt',tables.Int64Col()),
																('MyString',tables.StringCol(10)),
																('MyIntsList',(tables.Int64Col,'UnitsInt'))
															]
														}
											),
											('App_Model_OutputingDict',
												{
													'ColumningTuplesList':
													[
														('MyFloat',tables.Float32Col()),
														('MyFloatsList',(tables.Float32Col,'UnitsInt'))
													]
												}
											),
											('model',{'ArgsVariable':"Feature"}),
											('tabular',{'ArgsVariable':"Feature"}),
											('flush',{}),
											('UnitsInt',2),
											('MyIntsList',[2,4]),
											('MyFloatsList',[2.3,4.5]),
											('tabular',{'ArgsVariable':"Feature"}),
											('flush',{}),
											('model',{
														'ArgsVariable':"Output",
														'KwargsDict':{'JoiningString':"Scan"}
													}
											),
											('tabular',{
														'ArgsVariable':"Output",
													}
											),
											('flush',{})
										]
									)
							).close()
																
	#Return the object itself
	print(Joiner)
	return Joiner
#</DefineAttestingFunctions>
