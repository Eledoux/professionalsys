#<ImportModules>
import collections
import copy
import numpy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
JoinString='__'
JoinDeepString='/'
#</DefineLocals>

#<DefineClass>
class JoinerClass(SYS.FeaturerClass):
	
	#<DefineHookMethods>
	def modelAfter(self,**_ModelingVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						"self.ModeledDict['ModelString'] is "+str(self.ModeledDict['ModelString']),
						"We are going to join..."
					]
				)

		#Define the JoinedOrderedDict
		JoinedOrderedDict=collections.OrderedDict()

		#Set in ModeledDict
		self.ModeledDict['JoinedOrderedDict']=JoinedOrderedDict

		#Call the join method
		self.join()

		#Debug
		self.debug(
					[
						"Ok joined was done",
						"self.ModeledDict['ModelString'] is "+str(self.ModeledDict['ModelString'])
					]
				)

		#Model if there is a JoinedModeledDict
		if self.ModeledDict['JoinedModeledDict']!={}:

			#Set an alias for the ModeledClass
			ModeledClass=self.ModeledDict['ModeledClass']

			#Alias
			JoinedRetrievingTupleKeyString=self.ModeledDict['JoinedRetrievingTupleKeyString']

			#Set the columns
			ModeledClass.columns[JoinedRetrievingTupleKeyString]=tables.Int64Col(shape=2)
			
			#Alias
			JoinedNodifiedNodeString=self.ModeledDict['JoinedNodifiedNodeString']

			#Nodify with the JoinedNodifiedNodeString
			if JoinedNodifiedNodeString!="":

				#Debug
				self.debug('In the ModeledClass we add the joined columns')

				#Alias
				JoinedNodifiedNodedString=self.ModeledDict['JoinedNodifiedNodedString']
				JoinedModeledString=self.ModeledDict['JoinedModeledString']

				#Set the JoinedNodifiedRetrievingTupleKeyStringsList
				JoinedNodifiedRetrievingTupleKeyStringsList=map(
					lambda __NodifiedKeyStringKeyString:
					JoinedNodifiedNodedString+__NodifiedKeyStringKeyString+JoinedRetrievingTupleKeyString,
					self.ModeledDict['JoinedNodifiedKeyStringsList']
				)

				#Set the Cols for the joined children 
				map(
					lambda __JoinedNodifiedRetrievingTupleKeyString:
					ModeledClass.columns.__setitem__(
						JoinedNodifiedRetrievingTupleKeyStringsList,
						tables.Int64Col(shape=2)
					),
					JoinedNodifiedRetrievingTupleKeyStringsList
				)

		#Put them in the ModeledDict
		LocalVars=vars()
		map(
				lambda __GettingString:
				self.ModeledDict.__setitem__(__GettingString,LocalVars[__GettingString]),
				[
					'JoinedOrderedDict'
				]
			)

		#Debug
		self.debug('End of the method')

	def rowAfter(self,**_RowingVariablesList):

		#Debug
		self.debug('Start of the method')

		#Debug
		DebuggingString="self.ModeledDict['ModelString'] is "+str(self.ModeledDict['ModelString'])
		DebuggingString+='\nWe are going to check if this model is already flushed...'
		self.debug(DebuggingString)

		#Alias
		JoinedModeledDict=self.ModeledDict['JoinedModeledDict']

		#Check
		if JoinedModeledDict!={}:

			#Set JoinedRetrievingTupleKeyString
			JoinedRetrievingTupleKeyString=self.ModeledDict['JoinedRetrievingTupleKeyString']

			#Define the GettingStringsList and the GettedVariablesList
			if 'ColumningTuplesList' in JoinedModeledDict:

				#Debug
				self.debug('Check that this row is a new row in the table or not')

				#Get the GettingStringsList and GettedVariablesList
				GettingStringsList=SYS.unzip(JoinedModeledDict['ColumningTuplesList'],[0])
				GettedVariablesList=self.pick(GettingStringsList)

				#Check if it was already rowed
				IsRowedBoolsList=map(
						lambda __Row:
						all(
							map(
									lambda __GettingString,__GettedVariable:
									SYS.getIsEqualBool(__Row[__GettingString],__GettedVariable),
									GettingStringsList,
									GettedVariablesList
								)
						),
						JoinedModeledDict['TabularedTable'].iterrows()
					)					

				#Debug
				if hasattr(self,'StructuredKeyString'):
					self.debug('self.StructuredKeyString is '+str(self.StructuredKeyString))
				self.debug('IsRowedBoolsList is '+str(IsRowedBoolsList))

				#If it is rowed then set the JoinedRetrievingTuple
				try:
					ModeledInt=IsRowedBoolsList.index(True)
				except ValueError:
					ModeledInt=-1

				#Debug
				self.debug('So the corresponding ModeledInt is '+str(ModeledInt))

				#Set the ModeledInt
				self.ModeledDict['JoinedOrderedDict'][JoinedRetrievingTupleKeyString][1]=ModeledInt


			#Alias
			JoinedOrderedDict=self.ModeledDict['JoinedOrderedDict']
			JoinedRetrievingTuple=self.ModeledDict['JoinedOrderedDict'][JoinedRetrievingTupleKeyString]
			JoinedNodifiedNodeString=self.ModeledDict['JoinedNodifiedNodeString']

			#Give the JoinedRetrievingTuple to itself
			JoinedOrderedDict.__setitem__(
				JoinedRetrievingTupleKeyString,
				JoinedRetrievingTuple
			)

			#Debug
			self.debug(
						[
							'JoinedOrderedDict is now',
							SYS.represent(JoinedOrderedDict)
						]
					)

			#Give to the parent
			if JoinedNodifiedNodeString!="":
				ParentPointer=getattr(
										self,
										JoinedNodifiedNodedString+'ParentPointer'
							)
				if ParentPointer!=None:
					ParentPointer['App_Model_'+ModelingString+'Dict']['JoinedOrderedDict'][
							getattr(
									self,
									JoinedNodifiedNodedString+'KeyString'
									)+JoinedRetrievingTupleKeyString
							]=JoinedRetrievingTuple

			#Update the self.RowedIdentifiedOrderedDic
			self.ModeledDict['RowedIdentifiedOrderedDict'].update(JoinedOrderedDict)

		#Debug
		self.debug('End of the method')

	def flushBefore(self,**_FlushingVariablesList):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(	
					[
						"self.ModeledDict['ModelString'] is "+str(self.ModeledDict['ModelString']),
						'self.StructuredKeyString is '+str(self.StructuredKeyString) if hasattr(self,"StructuredKeyString") else ''
					]
				)

		#Define the NotRowedTuplesList
		NotRowedTuplesList=filter(
				lambda __JoiningTuple:
				__JoiningTuple[1][0]<0 or __JoiningTuple[1][1]<0,
				self.ModeledDict['JoinedOrderedDict'].items()
			)

		#Debug
		self.debug('NotRowedTuplesList is '+str(NotRowedTuplesList))

		#Alias
		ModelString=self.ModeledDict['ModelString']

		#IsNodingFlushingBool
		if 'IsNodingFlushingBool' not in _FlushingVariablesList or _FlushingVariablesList['IsNodingFlushingBool']:

			#Debug
			self.debug(
						[
							'We are going to make flush all the noded children with the Model',
							'ModelString is '+str(ModelString)
						]
					)

			#Flush each noded children
			map(
					lambda __Variable:
					__Variable.flush(ModelString),
					self.ModeledDict['JoinedNodifiedOrderedDict'].values()
				)

			#Debug
			self.debug(
						[
							'The noded children have flushed',
							'Now look at the joined model',
							('self.ModeledDict',self.ModeledDict,['JoinedModelString'])
						]
					)

		#flush the joined model
		if self.ModeledDict['JoinedModelString']!="":

			#Debug
			self.debug(
						[
							'Flush self with the joined model',
							'But without making the noded children flushing'
						]
					)

			#Copy the ModeledDict
			CopiedModeledDict=copy.copy(self.ModeledDict)

			#Flush
			self.flush(self.ModeledDict['JoinedModelString'],**{'IsNodingFlushingBool':False})

			#Debug
			self.debug('Flush self with the joined model was done')
		
			#Reset the self.ModeledDict
			self.ModeledDict=CopiedModeledDict

			#Set the JoinedRetrievingTupleKeyString
			JoinedRetrievingTupleKeyString=self.ModeledDict['JoinedModeledDict']['ModeledString']+'RetrievingTuple'

			#Alias
			JoinedOrderedDict=self.ModeledDict['JoinedOrderedDict']

			#It is going to be flushed so update the self.JoinedRetrievingTuple to the last row index
			if JoinedOrderedDict[JoinedRetrievingTupleKeyString][1]==-1:

				#Debug
				self.debug(
							[
								'This is a new row so we just set the ModeledInt of the <JoinedModeledString>RetrievingTuple',
								'To the size of the table',
								'JoinedOrderedDict[JoinedRetrievingTupleKeyString] is '+str(
									JoinedOrderedDict[JoinedRetrievingTupleKeyString])
							]
						)

				#Update the corresponding RetrievingTuple
				JoinedOrderedDict[JoinedRetrievingTupleKeyString][1]=self.ModeledDict['JoinedModeledDict']['TabularedTable'].nrows-1

				#Debug
				self.debug('Now JoinedOrderedDict[JoinedRetrievingTupleKeyString] is '+str(
					JoinedOrderedDict[JoinedRetrievingTupleKeyString]))

			#Get the JoinedRetrievingTuple
			JoinedRetrievingTuplesList=map(
								lambda __FlushingVariable:
								__FlushingVariable.ModeledDict['JoinedRetrievingTuple'],
								self.ModeledDict['JoinedNodifiedOrderedDict'].values()
							)

		else:

			#Set by default an empty list
			JoinedRetrievingTuplesList=[]

		#Debug
		self.debug(
					[
						'self.StructuredKeyString is '+str(self.StructuredKeyString) if hasattr(self,'StructuredKeyString') else '',
						"self.ModeledDict['ModelString'] is "+str(self.ModeledDict['ModelString']),
						'We add in the RowedIdentifiedOrderedDict the Joined JoinedRetrievingTuples',
						"self.ModeledDict['RowedIdentifiedOrderedDict'] is "+str(
								self.ModeledDict['RowedIdentifiedOrderedDict']),
						"NotRowedTuplesList is "+str(NotRowedTuplesList),
						"JoinedRetrievingTuplesList is "+str(JoinedRetrievingTuplesList)
					]
		)

		#Alias
		RowedIdentifiedOrderedDict=self.ModeledDict['RowedIdentifiedOrderedDict']

		#Just change the __ModeledInt
		map(
				lambda __NotRowedTuple:
				RowedIdentifiedOrderedDict.__setitem__(__NotRowedTuple[0],__NotRowedTuple[1]),
				NotRowedTuplesList
			)

		#Debug
		self.debug('Now the RowedIdentifiedOrderedDict is '+str(RowedIdentifiedOrderedDict))

		#Debug
		self.debug('End of the method')

	def retrieveAfter(self,**_RetrievingVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug("self.ModeledDict['RetrievingTuple'] is "+str(self.ModeledDict['RetrievingTuple']))

		#Alias
		RetrievedTuplesList=self.ModeledDict['RetrievedTuplesList']

		#Debug	
		DebuggedString='RetrievedTuplesList is '+str(RetrievedTuplesList)
		self.debug(DebuggedString)

		#Define the RetrievingTupleIndexInt
		if len(RetrievedTuplesList)>0:

			#Get the Index 
			RetrievingTupleIndexInt=SYS.unzip(RetrievedTuplesList,[0]).index(
				self.ModeledDict['JoinedRetrievingTupleKeyString'])

			#Set and retrieve for the joined model
			self.ModeledDict['RetrievingTuple']=RetrievedTuplesList[RetrievingTupleIndexInt][1]
			self.retrieve(self.ModeledDict['JoinedModeledDict']['ModelString'])

		#Debug
		self.debug('End of the method')

	def findBefore(self,**_FindingVariablesDict):

		#Debug
		self.debug('Find the joined model')

		#Alias
		JoinedModelString=self.ModeledDict['JoinedModelString']

		#Check that JoinedModelString is good
		if JoinedModelString!="":

			#Copy the ModeledDict
			CopiedModeledDict=copy.copy(self.ModeledDict)

			#Debug
			self.debug(
						[
							'First we are going to find in the joined model',
							'JoinedModelString is '+str(JoinedModelString)
						]
					)

			#Find
			self.ModeledDict=self.ModeledDict['JoinedModeledDict']
			self.find()
			JoinedModeledDict=self.ModeledDict
			self.ModeledDict=CopiedModeledDict

			#Debug
			self.debug(
						[
							'Find in the joined model is done',
							'JoinedModelString is '+str(JoinedModelString)
						]
					)

			#Debug
			self.debug('Ok we have found the joined model')

			#Copy the FoundFilteredRowedDictsList
			JoinedFoundFilteredRowedDictsList=JoinedModeledDict['FoundFilteredRowedDictsList']

			#Debug
			self.debug(
						[
							'JoinedFoundFilteredRowedDictsList is ',
							SYS.represent(JoinedFoundFilteredRowedDictsList)
						]
			)

			#Alias
			JoinedRetrievingTupleKeyString=self.ModeledDict['JoinedRetrievingTupleKeyString']

			#Make the TabularedInt and the ModeledInt as a <JoinedModelString>RetrievingTuple
			map(
					lambda __JoinedFoundFilteredRowedDict:
					__JoinedFoundFilteredRowedDict.__setitem__(
						JoinedRetrievingTupleKeyString,
						[
							__JoinedFoundFilteredRowedDict['TabularedInt'],
							__JoinedFoundFilteredRowedDict['ModeledInt'],
						]
					),
					JoinedFoundFilteredRowedDictsList
				)

			#Set the JoinedFindingTuplesList
			JoinedFindingTuplesList=[
						(
							JoinedRetrievingTupleKeyString,
							(
								SYS.getIsInListBool,
								map(
										lambda __JoinedFoundFilteredRowedDict:
										[
											__JoinedFoundFilteredRowedDict['TabularedInt'],
											__JoinedFoundFilteredRowedDict['ModeledInt'],
										],
										JoinedFoundFilteredRowedDictsList
									)
							)
					)
				]

			#Debug
			self.debug(
						[
							'JoinedFindingTuplesList is',
							SYS.represent(JoinedFindingTuplesList)
						]
					)

		else:

			#Set an default empty
			JoinedFindingTuplesList=[]

		#Debug
		self.debug(
					[
						'Joiner findbefore is over',
						('self.ModeledDict',self.ModeledDict,['ModelString','FoundRowedDictsList'])
					]
				)

		#Put them in the ModeledDict
		LocalVars=vars()
		map(
				lambda __GettingString:
				self.ModeledDict.__setitem__(__GettingString,LocalVars[__GettingString]),
				[
					'JoinedFindingTuplesList',
				]
			)

		#Debug
		self.debug('End of the method')

	def findAfter(self,**_FindingVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						('Are we going to do a where with the FoundFilteredRowedDictsList and the '),
						('filtering JoinedFindingTuplesList?'),
						('self.ModeledDict ',self.ModeledDict,[
																'ModelString',
																'FoundFilteredRowedDictsList'
																]),
						("'JoinedFindingTuplesList' in self.ModeledDict is "+str(
							'JoinedFindingTuplesList' in self.ModeledDict))
					]
			)

		if 'JoinedFindingTuplesList' in self.ModeledDict:

			#Debug
			self.debug(
						[
							'Ok we are going to do the where',
							"self.ModeledDict['JoinedFindingTuplesList'] is "+str(
								self.ModeledDict['JoinedFindingTuplesList'])
						]
					)

			#Where
			self.ModeledDict['FoundFilteredRowedDictsList']=SYS.filterNone(SYS.where(
							self.ModeledDict['FoundFilteredRowedDictsList'],
							self.ModeledDict['JoinedFindingTuplesList']
							)
			)

			#Debug
			self.debug('Ok the where is done.')

		#Debug
		self.debug(
					[
						'After intersection',
						('self.ModeledDict ',self.ModeledDict,[
															'ModelString',
															'FoundFilteredRowedDictsList'
															]
														)
					]
				)

		#Debug
		self.debug('End of the method')

	def recoverBefore(self,**_LocalRecoveringVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						('self.ModeledDict ',self.ModeledDict,[
															'ModelString',
															'FoundFilteredRowedDictsList'
															]
														)
					]
				)

		#Alias
		FoundFilteredRowedDictsList=self.ModeledDict['FoundFilteredRowedDictsList']

		#Debug
		self.debug(
					[
						'Look if we have found only one FilteredRowedDict',
						'len(FoundFilteredRowedDictsList) is '+str(len(FoundFilteredRowedDictsList))
					]
				)

		if len(FoundFilteredRowedDictsList)==1:
				
			#Debug
			self.debug('It is good, there is one solution !')

			#Define a JoinedRecoveredDict
			JoinedRecoveredDict=FoundFilteredRowedDictsList[0]

			#Alias
			JoinedNodifiedOrderedDict=self.ModeledDict['JoinedNodifiedOrderedDict']
			JoinedRetrievingTupleKeyString=self.ModeledDict['JoinedRetrievingTupleKeyString']
			
			#Debug
			self.debug(
						[
							'But first look if we have to recover the children first',
							"len(JoinedNodifiedOrderedDict) is "+str(
								len(JoinedNodifiedOrderedDict))
						]
					)

			#Maybe we have to recover the children before
			if len(JoinedNodifiedOrderedDict)>0:

				self.debug(
							[
								'Yes, we are going to make recover each children before'
							]
						)

				#Set each Children and recover each
				JoinedNodifiedKeyString=self.JoinedNodifiedNodedString+'KeyString'

				#Alias
				ModelString=self.ModeledDict['ModelString']
				ModelingString=self.ModeledDict['ModelingString']
				JoinedNodifiedNodedString=self.ModeledDict['JoinedNodifiedNodedString']

				#Map a Recover
				map(
						lambda __JoinedJoiner:
						__JoinedJoiner.__setitem__(
								'/App_Model_'+ModelingString+'Dict/FindingTuplesList',
								[
									(
										JoinString+JoinedRetrievingTupleKeyString,
										(
											SYS.getIsEqualBool,
											JoinedRecoveredDict[
											JoinedNodifiedNodedString+getattr(
													__JoinedJoiner,
													JoinedNodifiedNodedString
													)+JoinedRetrievingTupleKeyString
											]
										)
									)
								]
						).recover(ModelString),
						JoinedNodifiedOrderedDict.values()
					)

				#Debug
				self.debug('Ok the children are recovered')

			#Debug
			self.debug(
						[
							'Maybe update first the joined model by retrieving',
							"JoinedRecoveredDict[JoinedRetrievingTupleKeyString] is "+str(
								JoinedRecoveredDict[JoinedRetrievingTupleKeyString]
								)
						]
					)

			#Set RetrievingTuple in the model and retrieve
			self['App_Model_'+self.ModeledDict['JoinedModelingString']+'Dict']['RetrievingTuple']=JoinedRecoveredDict[
						JoinedRetrievingTupleKeyString
						]
			self.retrieve(JoinedModelString)

			#Debug
			self.debug('Ok the joined model was retrieved')


			"""
			print('self.JoinedFilteredMergedRowedDictsListTuplesList is')
			SYS._print(self.JoinedFilteredMergedRowedDictsListTuplesList)
			print('AppendingGettingStringsList is ',AppendingGettingStringsList)

			#Next we have maybe to update with the joined model	
			if '/' in AppendingGettingStringsList:	

				#Define the IndexInt of the joined model
				IndexInt=AppendingGettingStringsList.index('/')

				#Define the JoinedFilteredMergedRowedDictsList
				JoinedFilteredMergedRowedDictsList=self.JoinedFilteredMergedRowedDictsListTuplesList[
						IndexInt][1] 

				#Define the JoinedRetrievingTuple
				JoinedRetrievingTuple=JoinedRecoveredDict[self.JoinedRetrievingTupleKeyString]

				#Debug
				print('JoinedFilteredMergedRowedDictsList is ')
				SYS._print(JoinedFilteredMergedRowedDictsList)
				print('JoinedRetrievingTuple is ',JoinedRetrievingTuple)
				if hasattr(self,'StructuredKeyString'):
					print("self['StructuredKeyString'] is ",self.StructuredKeyString)
				print('')

				#Take the first element of self.JoinedFilteredMergedRowedDictsTuplesList which corresponds to the Joined Model at this level	
				JoinedRowedDict=next(
						RowedDict for RowedDict in JoinedFilteredMergedRowedDictsList
						if SYS.getIsEqualBool(
							RowedDict[self.JoinedRetrievingTupleKeyString],
							JoinedRetrievingTuple
						)
					)

				#Debug
				'''
				print('JoinedRowedDict is ',JoinedRowedDict)
				print('')
				'''

				#Update
				self.update(JoinedRowedDict.items())
			"""
			
		else:

			#Debug
			'''
			print('Joiner There are multiple retrieved states')
			if hasattr(self,'StructuredKeyString'):
				print("self['StructuredKeyString'] is ",self.StructuredKeyString)
			print("self.ModeledDict['FoundFilteredRowedDictsList'] is ")
			SYS._print(self.ModeledDict['FoundFilteredRowedDictsList'])
			print('')
			'''

			#Stop the recover
			self.IsRecoveringBool=False

		#Debug
		self.debug('End of the method')

	#</DefineHookMethods>
	def join(	
				self,
				_ModelString="",
				**_JoiningVariablesDict
			):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						'_ModelString is '+str(_ModelString),
						'self.StructuredKeyString is '+str(self.StructuredKeyString) if hasattr(self,"StructuredKeyString") else ''
					]
				)

		#Check
		if 'JoiningTuple' not in self.ModeledDict:
			self.ModeledDict['JoiningTuple']=("","")

		#Set JoinedNodifiedNodeString and JoinedModelString
		JoinedNodifiedNodeString=self.ModeledDict['JoiningTuple'][0]
		JoinedModelString=self.ModeledDict['JoiningTuple'][1]

		#Put them in the ModeledDict
		LocalVars=vars()
		map(
				lambda __GettingString:
				self.ModeledDict.__setitem__(__GettingString,LocalVars[__GettingString]),
				[
					'JoinedNodifiedNodeString',
					'JoinedModelString'
				]
			)

		#Set the JoiningTuple
		if 'JoiningTuple' not in _JoiningVariablesDict:

			#Set to the _JoiningVariablesDict
			_JoiningVariablesDict['JoiningTuple']=self.ModeledDict['JoiningTuple']

			#Maybe we have to structure
			if self.IsGroupedBool==False:

				#Debug
				self.debug(
							[
								'Join We have to structure first',
								'self.StructuredKeyString is '+str(self.StructuredKeyString) if hasattr(self,"StructuredKeyString") else ''
							]
					)
				
				#Structure
				self.structure(JoinedNodifiedNodeString)

		#Nodify if there is the nodified objects
		if JoinedNodifiedNodeString!="":

			#Debug
			self.debug('Joiner we are going to nodify the '+str(JoinedNodifiedNodeString))

			#Nodify
			self.nodify(JoinedNodifiedNodeString)

			#Set 
			JoinedNodifiedOrderedDict=copy.copy(self.NodifiedOrderedDict)
			JoinedNodifiedNodedString=self.NodifiedNodedString
			JoinedNodifiedNodingString=self.NodifiedNodingString

			#Debug
			self.debug('self.JoinedNodifiedOrderedDict is '+str(JoinedNodifiedOrderedDict))

		else:

			#Set an empty dict
			JoinedNodifiedOrderedDict=collections.OrderedDict()
			JoinedNodifiedNodedString=""
			JoinedNodifiedNodingString=""

		#Put them in the ModeledDict
		LocalVars=vars()
		map(
				lambda __GettingString:
				self.ModeledDict.__setitem__(__GettingString,LocalVars[__GettingString]),
				[
					'JoinedNodifiedOrderedDict',
					'JoinedNodifiedNodedString',
					'JoinedNodifiedNodingString'
				]
			)

		#Debug
		self.debug('We are maybe tabling the joined model ?')

		#Table maybe the joined model
		if JoinedModelString!="":

			#Set a copy of it
			LastModeledDict=copy.copy(self.ModeledDict)

			#Debug
			self.debug(
						[
							'We are going to table the joinedModel',
							'JoinedModelString is '+str(JoinedModelString)
						]
					)

			#table to configure the joined model
			self.table(JoinedModelString)

			#Debug
			self.debug(
						[
							'The joined model is tabled ok',
							'JoinedModelString is '+str(JoinedModelString)
						]
					)

			#Copy the ModeledDict
			JoinedModeledDict=copy.copy(self.ModeledDict)

			#Reset the ModeledDict to the original
			self.ModeledDict=LastModeledDict

		else:

			#Debug
			self.debug('We dont need to table because this model has not a joined model')

			#Init an empty dict
			JoinedModeledDict={}

		#Set a link
		self.ModeledDict['JoinedModeledDict']=JoinedModeledDict

		#Check
		if JoinedModeledDict!={}:

			#Debug
			self.debug('We are building the JoinedRetrievingTuple and give him the TabularedInt')

			#Set the JoinedRetrievingTupleKeyString
			JoinedRetrievingTupleKeyString=JoinedModeledDict['ModeledString']+'RetrievingTuple'
			self.ModeledDict['JoinedRetrievingTupleKeyString']=JoinedRetrievingTupleKeyString

			#Set in the ModeledDict
			self.ModeledDict['JoinedOrderedDict'][JoinedRetrievingTupleKeyString]=[JoinedModeledDict['TabularedInt'],-1]

			#Debug
			self.debug("OK self.ModeledDict['JoinedOrderedDict' is so "+str(
				self.ModeledDict['JoinedOrderedDict']))
			
		#Debug
		self.debug('End of the method')

		#Return self
		return self

#</DefineClass>

#<DefineAttestingFunctions>
def attest_join():

	#Build Hdf groups
	Joiner=SYS.JoinerClass().hdformat().update(
								[
									(
										'App_Structure_ChildJoiner1',
										SYS.JoinerClass().update(
										[
											('App_Model_ParameterizingDict',
														{
															'ColumningTuplesList':
															[
																('MyIntsList',tables.Int64Col(shape=2))
															]
														}
											),
											('MyIntsList',[2,4]),
											('App_Model_ResultingDict',
														{
															'ColumningTuplesList':
															[
																('MyInt',tables.Int64Col())
															]
														}
											),
											('MyInt',1)
										])
									),
									('App_Model_ParameterizingDict',
														{
															'ColumningTuplesList':
															[
																('MyString',tables.StringCol(10)),
															]
														}
									),
									('MyString',"hello")
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':""}),
													('model',{'ArgsVariable':"Parameter"}),
													('table',{'ArgsVariable':""}),
													('row',{'ArgsVariable':""}),
													('flush',{'ArgsVariable':""})
												]
											}
									).update(
										[
											
											('App_Model_ResultingDict',
												{
													'ColumningTuplesList':
													[
														('MyFloat',tables.Float32Col()),
														('MyFloatsList',tables.Float32Col(shape=3))
													],
													'JoiningTuple':("Structure","Parameter")
												}
											),
											('MyFloat',0.1),
											('MyFloatsList',[2.3,4.5,1.1])
										]
									).model("Result"		
									).table(
									).row(
									).flush("Result"
									).close()

	#Return the object itself
	return "Object is : \n"+SYS.represent(
		Joiner
		)+'\n\n\n'+SYS.represent(os.popen('/usr/local/bin/h5ls -dlr '+Joiner.HdformatingPathString
		).read())
#</DefineAttestingFunctions>
