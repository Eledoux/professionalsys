#<ImportModules>
import collections
import copy
import numpy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
JoinString='__'
JoinDeepString='/'
BasingLocalTypeString="Shaper"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class JoinerClass(BasedLocalClass):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self):

		#<DefineSpecificDict>
		self.JoinedOrderedDict=collections.OrderedDict()		#<NotRepresented>
		self.JoinedDatabasePointer=None							#<NotRepresented>
		#</DefineSpecificDict>

	@SYS.HookerClass(**{'BeforeTuplesList':[("<TypeString>","join")],'AfterTuplesList':[("Featurer","model")]})
	def model(self,**_VariablesDict):

		#Debug
		self.debug('Start of the method')

		#Model if there is a JoinedDatabasePointer
		if self.JoinedDatabasePointer!=None:

			#Set the columns
			self.ModeledClass.columns[self.JoinedRetrievingTupleKeyString]=tables.Int64Col(shape=2)
			
		#Debug
		self.debug('End of the method')

	@SYS.HookerClass(**{'AfterTuplesList':[("Featurer","row")]})
	def row(self,**_VariablesList):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						"self.ModelingModelString is "+str(self.ModelingModelString),
						"We are going to check if the joined model is already flushed...",
						"First look if self.JoinedModeledDatabase!={} is "+str(len(self.JoinedModeledDatabase)>0)
					]
				)

		#Check
		if self.JoinedDatabasePointer!=None:

			#Debug
			self.debug("Ok there is a self.JoinedDatabasePointer")

			#Alias
			JoinedRetrievingTuple=self.JoinedRetrievingTuple
			JoinedRetrievingTupleKeyString=self.JoinedRetrievingTupleKeyString

			#Debug
			self.debug('Check that this joined row is a new row in the table or not')

			#Get the GettingStringsList and GettedVariablesList
			GettingStringsList=SYS.unzip(self.JoinedModeledDatabase.ModelingColumningTuplesList,[0])
			GettedVariablesList=self.pick(GettingStringsList)

			#Check if it was already rowed
			IsRowedBoolsList=map(
					lambda __Row:
					all(
						map(
								lambda __GettingString,__GettedVariable:
								SYS.getIsEqualBool(__Row[__GettingString],__GettedVariable),
								GettingStringsList,
								GettedVariablesList
							)
					),
					self.JoinedDatabasePointer.TabledTable.iterrows()
				)					

			#Debug
			self.debug(
						[
							'self.StructuredKeyString is '+str(
								self.StructuredKeyString) if hasattr(self,'StructuredKeyString') else "",
							'IsRowedBoolsList is '+str(IsRowedBoolsList)
						]
					)

			#If it is rowed then set the JoinedRetrievingTuple
			try:
				ModeledInt=IsRowedBoolsList.index(True)
			except ValueError:
				ModeledInt=-1

			#Debug
			self.debug('So the corresponding ModeledInt is '+str(ModeledInt))

			#Set the ModeledInt
			JoinedRetrievingTuple[1]=ModeledInt
			#self.ModeledDict['JoinedOrderedDict'][JoinedRetrievingTupleKeyString][1]=ModeledInt


			#Alias
			'''
			JoinedOrderedDict=self.ModeledDict['JoinedOrderedDict']
			JoinedRetrievingTuple=self.ModeledDict['JoinedOrderedDict'][JoinedRetrievingTupleKeyString]

			#Give the JoinedRetrievingTuple to itself
			JoinedOrderedDict.__setitem__(
				JoinedRetrievingTupleKeyString,
				JoinedRetrievingTuple
			)
			'''

			'''
			#Debug
			self.debug(
						[
							'JoinedOrderedDict is now',
							SYS.represent(JoinedOrderedDict)
						]
					)
			'''

			#Debug
			#self.debug('So we set the JoinedRetrievingTuple in the RowedIdentifiedOrderedDict')

			#Update the self.RowedIdentifiedOrderedDic
			#self.ModeledDict['RowedIdentifiedOrderedDict'].update(JoinedOrderedDict)
			#self.ModeledDict['RowedIdentifiedOrderedDict'].__setitem__(
			#			JoinedRetrievingTupleKeyString,
			#			JoinedRetrievingTuple
			#		)

		#Debug
		self.debug('End of the method')

	def flushBefore(self,**_FlushingVariablesList):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(	
					[
						"self.ModeledDict['ModelString'] is "+str(self.ModeledDict['ModelString']),
						'self.StructuredKeyString is '+str(self.StructuredKeyString) if hasattr(self,"StructuredKeyString") else ''
					]
				)

		#Define the NotRowedTuplesList
		#NotRowedTuplesList=filter(
		#		lambda __JoiningTuple:
		#		__JoiningTuple[1][0]<0 or __JoiningTuple[1][1]<0,
		#		self.ModeledDict['JoinedOrderedDict'].items()
		#	)

		#Alias
		ModelString=self.ModeledDict['ModelString']

		#flush the joined model
		if self.ModeledDict['JoiningModelString']!="":

			#Debug
			self.debug(
						[
							'Flush self with the joined model'
						]
					)

			#Copy the ModeledDict
			CopiedModeledDict=copy.copy(self.ModeledDict)

			#Flush
			self.flush(self.ModeledDict['JoiningModelString'])

			#Debug
			self.debug('Flush self with the joined model was done')
		
			#Reset the self.ModeledDict
			self.ModeledDict=CopiedModeledDict

			#Set the JoinedRetrievingTupleKeyString
			JoinedRetrievingTupleKeyString=self.ModeledDict['JoinedModeledDict']['ModeledString']+'RetrievingTuple'

			#Alias
			JoinedRetrievingTuple=self.ModeledDict['JoinedRetrievingTuple']

			#It is going to be flushed so update the JoinedRetrievingTuple to the last row index
			if JoinedRetrievingTuple==-1:

				#Debug
				self.debug(
							[
								'This is a new row so we just set the ModeledInt of the <JoinedModeledString>RetrievingTuple',
								'To the size of the table',
								'JoinedRetrievingTuple is '+str(
									JoinedRetrievingTuple)
							]
						)

				#Update the corresponding RetrievingTuple
				JoinedRetrievingTuple[1]=self.ModeledDict['JoinedModeledDict']['TabledTable'].nrows-1

				#Debug
				self.debug('Now JoinedRetrievingTuple is '+str(
					JoinedRetrievingTuple))

		#Debug
		self.debug(
					[
						'self.StructuredKeyString is '+str(self.StructuredKeyString) if hasattr(self,'StructuredKeyString') else '',
						"self.ModeledDict['ModelString'] is "+str(self.ModeledDict['ModelString']),
						'We add in the RowedIdentifiedOrderedDict the Joined JoinedRetrievingTuples',
						"self.ModeledDict['RowedIdentifiedOrderedDict'] is "+str(
								self.ModeledDict['RowedIdentifiedOrderedDict']),
						"NotRowedTuplesList is "+str(NotRowedTuplesList)
					]
		)

		#Debug
		self.debug('We set the JoinedRetrievingTuple in the RowedIdentifiedOrderedDict')

		#Update the self.RowedIdentifiedOrderedDic
		#self.ModeledDict['RowedIdentifiedOrderedDict'].update(JoinedOrderedDict)
		self.ModeledDict['RowedIdentifiedOrderedDict'].__setitem__(
					JoinedRetrievingTupleKeyString,
					JoinedRetrievingTuple
				)

		#Alias
		#RowedIdentifiedOrderedDict=self.ModeledDict['RowedIdentifiedOrderedDict']

		#Just change the __ModeledInt
		#map(
		#		lambda __NotRowedTuple:
		#		RowedIdentifiedOrderedDict.__setitem__(__NotRowedTuple[0],__NotRowedTuple[1]),
		#		NotRowedTuplesList
		#	)

		#Debug
		#self.debug('Now the RowedIdentifiedOrderedDict is '+str(RowedIdentifiedOrderedDict))

		#Debug
		self.debug('End of the method')

	def retrieveAfter(self,**_RetrievingVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug("self.ModeledDict['RetrievingTuple'] is "+str(self.ModeledDict['RetrievingTuple']))

		#Alias
		RetrievedTuplesList=self.ModeledDict['RetrievedTuplesList']

		#Debug	
		DebuggedString='RetrievedTuplesList is '+str(RetrievedTuplesList)
		self.debug(DebuggedString)

		#Define the RetrievingTupleIndexInt
		if len(RetrievedTuplesList)>0:

			#Get the Index 
			RetrievingTupleIndexInt=SYS.unzip(RetrievedTuplesList,[0]).index(
				self.ModeledDict['JoinedRetrievingTupleKeyString'])

			#Set and retrieve for the joined model
			self.ModeledDict['RetrievingTuple']=RetrievedTuplesList[RetrievingTupleIndexInt][1]
			self.retrieve(self.ModeledDict['JoinedModeledDict']['ModelString'])

		#Debug
		self.debug('End of the method')

	def findBefore(self,**_FindingVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Alias
		JoiningModelString=self.ModeledDict['JoiningModelString']

		#Debug
		self.debug(
					[
						'Find maybe first in the joined model',
						'JoiningModelString is '+JoiningModelString
					]
				)

		#Check that JoiningModelString is good
		if JoiningModelString!="":

			#Copy the ModeledDict
			CopiedModeledDict=copy.copy(self.ModeledDict)

			#Debug
			self.debug(
						[
							'First we are going to find in the joined model',
							'JoiningModelString is '+str(JoiningModelString)
						]
					)

			#Find
			self.ModeledDict=self.ModeledDict['JoinedModeledDict']
			self.find()
			JoinedModeledDict=self.ModeledDict
			self.ModeledDict=CopiedModeledDict

			#Debug
			self.debug(
						[
							'Find in the joined model is done',
							'JoiningModelString is '+str(JoiningModelString)
						]
					)

			#Debug
			self.debug('Ok we have found the joined model')

			#Copy the FoundFilteredRowedDictsList
			JoinedFoundFilteredRowedDictsList=JoinedModeledDict['FoundFilteredRowedDictsList']

			#Debug
			self.debug(
						[
							'JoinedFoundFilteredRowedDictsList is ',
							SYS.represent(JoinedFoundFilteredRowedDictsList)
						]
			)

			#Alias
			JoinedRetrievingTupleKeyString=self.ModeledDict['JoinedRetrievingTupleKeyString']

			#Make the TabledInt and the ModeledInt as a <JoiningModelString>RetrievingTuple
			map(
					lambda __JoinedFoundFilteredRowedDict:
					__JoinedFoundFilteredRowedDict.__setitem__(
						JoinedRetrievingTupleKeyString,
						[
							__JoinedFoundFilteredRowedDict['TabledInt'],
							__JoinedFoundFilteredRowedDict['ModeledInt'],
						]
					),
					JoinedFoundFilteredRowedDictsList
				)

			#Set the JoinedFindingTuplesList
			JoinedFindingTuplesList=[
						(
							JoinedRetrievingTupleKeyString,
							(
								SYS.getIsInListBool,
								map(
										lambda __JoinedFoundFilteredRowedDict:
										[
											__JoinedFoundFilteredRowedDict['TabledInt'],
											__JoinedFoundFilteredRowedDict['ModeledInt'],
										],
										JoinedFoundFilteredRowedDictsList
									)
							)
					)
				]

			#Debug
			self.debug(
						[
							'JoinedFindingTuplesList is',
							SYS.represent(JoinedFindingTuplesList)
						]
					)

		else:

			#Set a default empty
			JoinedFindingTuplesList=[]

		#Debug
		self.debug(
					[
						('self.ModeledDict',self.ModeledDict,['ModelString','FoundRowedDictsList'])
					]
				)

		#Put them in the ModeledDict
		LocalVars=vars()
		map(
				lambda __GettingString:
				self.ModeledDict.__setitem__(__GettingString,LocalVars[__GettingString]),
				[
					'JoinedFindingTuplesList',
				]
			)

		#Debug
		self.debug('End of the method')

	def findAfter(self,**_FindingVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						('Are we going to do a where with the FoundFilteredRowedDictsList and the '),
						('filtering JoinedFindingTuplesList?'),
						('self.ModeledDict ',self.ModeledDict,[
																'ModelString',
																'FoundFilteredRowedDictsList'
																]),
						("'JoinedFindingTuplesList' in self.ModeledDict is "+str(
							'JoinedFindingTuplesList' in self.ModeledDict))
					]
			)

		if 'JoinedFindingTuplesList' in self.ModeledDict:

			#Debug
			self.debug(
						[
							'Ok we are going to do the where',
							"self.ModeledDict['JoinedFindingTuplesList'] is "+str(
								self.ModeledDict['JoinedFindingTuplesList'])
						]
					)

			#Where
			self.ModeledDict['FoundFilteredRowedDictsList']=SYS.filterNone(SYS.where(
							self.ModeledDict['FoundFilteredRowedDictsList'],
							self.ModeledDict['JoinedFindingTuplesList']
							)
			)

			#Debug
			self.debug('Ok the where is done.')

		#Debug
		self.debug(
					[
						'After intersection',
						('self.ModeledDict ',self.ModeledDict,[
															'ModelString',
															'FoundFilteredRowedDictsList'
															]
														)
					]
				)

		#Debug
		self.debug('End of the method')

	def recoverBefore(self,**_LocalRecoveringVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						('self.ModeledDict ',self.ModeledDict,[
															'ModelString',
															'FoundFilteredRowedDictsList'
															]
														)
					]
				)

		#Alias
		FoundFilteredRowedDictsList=self.ModeledDict['FoundFilteredRowedDictsList']

		#Debug
		self.debug(
					[
						'Look if we have found only one FilteredRowedDict',
						'len(FoundFilteredRowedDictsList) is '+str(len(FoundFilteredRowedDictsList))
					]
				)

		if len(FoundFilteredRowedDictsList)==1:
				
			#Debug
			self.debug('It is good, there is one solution !')

			#Define a JoinedRecoveredDict
			JoinedRecoveredDict=FoundFilteredRowedDictsList[0]

			#Alias
			JoinedRetrievingTupleKeyString=self.ModeledDict['JoinedRetrievingTupleKeyString']
				
		else:

			#Debug
			self.debug('There are multiple found states')

			#Stop the recover
			self.IsRecoveringBool=False

		#Debug
		self.debug('End of the method')

	def joinBefore(	
				self,
				**_JoiningVariablesDict
			):

		#Debug
		self.debug('Start of the method')

		#Alias
		JoiningModelString=self.ModeledDict['JoiningModelString']

		#Table maybe the joined model
		if JoiningModelString!="":

			#Debug
			self.debug('We are tabling the joined model ?')

			#Set a copy of it
			LastModeledDict=copy.copy(self.ModeledDict)

			#Get the joined ModeledDict
			self.ModeledDict=self['App_Model_'+SYS.getDoingStringWithDoString(JoiningModelString)+'Dict']

			#Debug
			self.debug(
						[
							'Check if we have to table',
							"self.ModeledDict!=None and ('IsTabledBool' not in self.ModeledDict or self.ModeledDict['IsTabledBool']==False) is "+str(
								self.ModeledDict!=None and (
									'IsTabledBool' not in self.ModeledDict or self.ModeledDict['IsTabledBool']==False)),
						]
					)
			
			#Check that we have to table
			if self.ModeledDict!=None and (
				'IsTabledBool' not in self.ModeledDict or self.ModeledDict['IsTabledBool']==False):
				
				#Debug
				self.debug(
							[
								'We are going to table',
								"'IsTabledBool' not in self.ModeledDict' is "+str(
								'IsTabledBool' not in self.ModeledDict)
							]
						)

				#Table
				self.table(JoiningModelString)

				#Debug
				self.debug('Ok this is tabled')

			#Debug
			self.debug(
						[
							'The joined model is tabled ok',
							'JoiningModelString is '+str(JoiningModelString)
						]
					)

			#Copy the ModeledDict
			JoinedModeledDict=copy.copy(self.ModeledDict)

			#Reset the ModeledDict to the original
			self.ModeledDict=LastModeledDict

			#Set a link
			self.ModeledDict['JoinedModeledDict']=JoinedModeledDict

			#Check
			if JoinedModeledDict!={}:

				#Debug
				self.debug('We are building the JoinedRetrievingTuple and give him the TabledInt')

				#Set the JoinedRetrievingTupleKeyString
				JoinedRetrievingTupleKeyString=JoinedModeledDict['ModeledString']+'RetrievingTuple'
				self.ModeledDict['JoinedRetrievingTupleKeyString']=JoinedRetrievingTupleKeyString

				#Set in the ModeledDict
				self.ModeledDict['JoinedOrderedDict'][JoinedRetrievingTupleKeyString]=[JoinedModeledDict['TabledInt'],-1]

				#Debug
				self.debug("OK self.ModeledDict['JoinedOrderedDict' is so "+str(
					self.ModeledDict['JoinedOrderedDict']))
				
		#Debug
		self.debug('End of the method')

		#Return self
		return self
	#</DefineHookMethods>

	#<DefineMethod>
	def join(self,**_JoiningVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Debug
		self.debug('Start of the method')

		#Check
		if self.IsTabledBool==False:

			#Debug
			self.debug('We have to table first')

			#Table
			self.table()

			#Debug
			self.debug('Ok this is tabled')

		#Refresh some attributes
		LocalOutputedPointer=self
		LocalJoiningVariablesDict=_JoiningVariablesDict
		self.IsJoiningBool=True

		#Hook
		if self.IsJoinedBool==False:

			#Hook methods
			for OrderString in ["Before","After"]:
			
				#Define the HookMethodString
				HookingMethodString='join'+OrderString

				#Check that there is HookingMethods for it
				if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

					#Call the specific Appended methods 
					for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

						#Call the HookMethod
						OutputVariable=HookingMethod(self,**LocalJoiningVariablesDict)

						if type(OutputVariable)==dict:
							if 'LocalJoiningVariablesDict' in OutputVariable:
								LocalJoiningVariablesDict=OutputVariable['LocalJoiningVariablesDict']
							if 'LocalOutputedPointer' in OutputVariable:
								LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

						#Check Bool
						if self.IsJoiningBool==False:
							self.IsJoinedBool=True
							return LocalOutputedPointer

			#Set
			self.IsJoinedBool=True

		#Debug
		self.debug('End of the method')

		#Return the OutputVariable
		return LocalOutputedPointer
#</DefineClass>

#<DefineAttestingFunctions>
def attest_join():

	#Build Hdf groups
	Joiner=SYS.JoinerClass().hdformat().update(
								[
									(
										'App_Structure_ChildJoiner1',
										SYS.JoinerClass().update(
										[
											('App_Model_ParameterizingDict',
														{
															'ColumningTuplesList':
															[
																('MyIntsList',tables.Int64Col(shape=2))
															]
														}
											),
											('MyIntsList',[2,4]),
											('App_Model_ResultingDict',
														{
															'ColumningTuplesList':
															[
																('MyInt',tables.Int64Col())
															]
														}
											),
											('MyInt',1)
										])
									),
									('App_Model_ParameterizingDict',
														{
															'ColumningTuplesList':
															[
																('MyString',tables.StringCol(10)),
															]
														}
									),
									('MyString',"hello")
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':""}),
													('model',{'ArgsVariable':"Parameter"}),
													('table',{'ArgsVariable':""}),
													('row',{'ArgsVariable':""}),
													('flush',{'ArgsVariable':""})
												]
											}
									).update(
										[
											
											('App_Model_ResultingDict',
												{
													'ColumningTuplesList':
													[
														('MyFloat',tables.Float32Col()),
														('MyFloatsList',tables.Float32Col(shape=3))
													],
													'JoiningTuple':("Structure","Parameter")
												}
											),
											('MyFloat',0.1),
											('MyFloatsList',[2.3,4.5,1.1])
										]
									).model("Result"		
									).table(
									).row(
									).flush("Result"
									).close()

	#Return the object itself
	return "Object is : \n"+SYS.represent(
		Joiner
		)+'\n\n\n'+SYS.represent(os.popen('/usr/local/bin/h5ls -dlr '+Joiner.HdformatingPathString
		).read())
#</DefineAttestingFunctions>
