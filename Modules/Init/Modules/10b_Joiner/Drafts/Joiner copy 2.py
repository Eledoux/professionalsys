#<ImportModules>
import collections
import copy
import numpy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
JoinString='__'
JoinDeepString='/'
#</DefineLocals>

#<DefineClass>
class JoinerClass(SYS.FeaturerClass):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.JoinedNodifiedOrderedDict=collections.OrderedDict()	#<NotRepresented>
		self.JoinedOrderedDict=collections.OrderedDict() 			#<NotRepresented>
		self.JoinedJoinedListKeyString=""							#<NotRepresented>
		self.JoinedModeledKeyString=""								#<NotRepresented>
		self.JoinedModeledDict={}									#<NotRepresented>
		self.JoinedTabularedTable=None      						#<NotRepresented>
		self.JoinedJoinedList=[-1,-1]								#<NotRepresented>
		self.JoinedRetrievingTuplesList=[]							#<NotRepresented>
		self.JoinedNodifiedNodeString=""							#<NotRepresented>
		self.JoinedNodifiedNodedString=""							#<NotRepresented>
		self.JoinedNodifiedNodingString=""							#<NotRepresented>
		self.JoinedModelString=""									#<NotRepresented>
		self.JoinedModeledString=""									#<NotRepresented>
		self.JoinedRowedDictsList=[] 								#<NotRepresented>
		self.JoinedFilteredMergedRowedDictsListTuplesList=[]		#<NotRepresented>
		#</DefineSpecificDict>

		#self.RepresentingKeyVariablesList+=['RowedIdentifiedOrderedDict','RowedNotIdentifiedOrderedDict']

	def modelAfter(self,**_ModelingVariablesDict):

		#Debug
		'''
		print('Joiner modelAfter method')
		print('')
		'''

		#Join maybe
		if 'JoiningTuple' in self.ModeledDict:

			#Debug
			'''
			print("self.ModeledDict['JoiningTuple'] is ",self.ModeledDict['JoiningTuple'])
			print()
			'''

			#Define the JoinedModeledString
			JoinedModeledString=SYS.getDoneStringWithDoString(self.ModeledDict['JoiningTuple'][1])

			#Add a JoinedList for the join with the joining table at this level
			self.JoinedJoinedListKeyString=JoinedModeledString+'JoinedList'
			self.ModeledClass.columns[self.JoinedJoinedListKeyString]=tables.Int64Col(shape=2)
			
			#Nodify with the JoiningNodifiedString
			if self.ModeledDict['JoiningTuple'][0]!="":

				#Debug
				'''
				print('Joiner we are going to nodify the ',self.ModeledDict['JoiningTuple'][0])
				print('')
				'''
				
				#Nodify
				self.nodify(self.ModeledDict['JoiningTuple'][0])

				#Set the JoinedNodifiedKeyStringKeyStringsList
				JoinedNodifiedKeyStringsList=map(
							lambda __JoinedObject:
							getattr(
									__JoinedObject,
									self.NodifiedKeyStringKeyString
									),
							self.NodifiedOrderedDict.values()
						)

				#Set the Cols for the joined children 
				map(
					lambda __NodifiedKeyStringKeyString:
					self.ModeledClass.columns.__setitem__(
						self.NodifiedNodedString+__NodifiedKeyStringKeyString+JoinedModeledString+'JoinedList',
						tables.Int64Col(shape=2)
					),
					JoinedNodifiedKeyStringsList
				)

				#Define the JoinedOrderedDict
				JoinedOrderedDictKeyString=self.NodifiedNodedString+'JoinedOrderedDict'
				self.__setattr__(JoinedOrderedDictKeyString,collections.OrderedDict())
				JoinedOrderedDict=getattr(self,JoinedOrderedDictKeyString)
				JoinedOrderedDict[self.JoinedJoinedListKeyString]=[-1,-1]

				#Init the JoinedList for each child
				map(
						lambda __JoinedNodifiedKeyString:
						JoinedOrderedDict.__setitem__(
							self.NodifiedNodedString+__JoinedNodifiedKeyString+self.JoinedJoinedListKeyString,
							[-1,-1]
							),
						JoinedNodifiedKeyStringsList
					)

	def rowAfter(self,**_LocalRowingVariablesList):

		#Join maybe
		if 'JoiningTuple' in self.ModeledDict:
			
			#Join
			if 'IsJoiningBool' not in _LocalRowingVariablesList or _LocalRowingVariablesList['IsJoiningBool']:
				self.join("")

			#Debug
			#print('self.JoinedOrderedDict is ',self.JoinedOrderedDict)

			#Update the self.RowedIdentifiedOrderedDic
			self.RowedIdentifiedOrderedDict.update(self.JoinedOrderedDict)

			#Debug
			'''
			print('self.JoinedOrderedDict is ',self.JoinedOrderedDict)
			print('self.RowedIdentifiedOrderedDict is ',self.RowedIdentifiedOrderedDict)
			print()
			'''

	def flushBefore(self,**_LocalFlushingVariablesList):

		#Debug
		'''
		print("Joiner flushBefore method")
		if hasattr(self,"StructuredKeyString"):
			print('self.StructuredKeyString is ',self.StructuredKeyString)
		print('_LocalFlushingVariablesList is',_LocalFlushingVariablesList)
		print('')
		'''
		
		#Flush the joined stuff if they are not yet
		if 'JoiningTuple' in self.ModeledDict:

			#We need maybe to join if it was not already rowed
			if 'IsRowedBool' in _LocalFlushingVariablesList and _LocalFlushingVariablesList['IsRowedBool']==False:
				self.join("")

			#Define the NodifyingString
			#NodifyingString=self.ModeledDict['JoiningTuple'][0]
			#NodifiedString=SYS.getDoneStringWithDoString(NodifyingString)
			
			#Debug
			'''
			print('Joiner flushAfter method')
			print('self.RowedIdentifiedOrderedDict is',self.RowedIdentifiedOrderedDict)
			print('self.JoinedOrderedDict is ',self.JoinedOrderedDict)
			print('')
			'''

			#Define the NotRowedTuplesList
			NotRowedTuplesList=filter(
					lambda __JoiningTuple:
					__JoiningTuple[1][0]<0 or __JoiningTuple[1][1]<0,
					self.JoinedOrderedDict.items()
				)

			#Debug
			'''
			print('NotRowedTuplesList is ',NotRowedTuplesList)
			print('')
			'''

			#Then get the corresponding NodifiedKeyStringsList
			NodifiedKeyStringsList=map(
										lambda __NodifiedKeyString:
										self.JoinedNodifiedNodedString.join(
											__NodifiedKeyString.split(self.JoinedNodifiedNodedString)[1:]
											)
										if self.JoinedNodifiedNodedString in __NodifiedKeyString and self.JoinedNodifiedNodedString!=""
										else __NodifiedKeyString,
										map(
											lambda __NotRowedTuple:
											self.JoinedJoinedListKeyString.join(__NotRowedTuple[0].split(
														self.JoinedJoinedListKeyString)[:-1]),
											NotRowedTuplesList
										)
									)

			#Debug
			'''
			print('NodifiedKeyStringsList is ',NodifiedKeyStringsList)
			print("self.JoinedNodifiedNodeString is ",self.JoinedNodifiedNodeString)
			print('')
			'''

			#Flush first all the children
			FlushingVariablesList=map(
					lambda __NodifiedKeyString:
					self['App_'+self.JoinedNodifiedNodeString+'_'+__NodifiedKeyString
					].flush(_LocalFlushingVariablesList['FlushingString'])
					if __NodifiedKeyString!=""
					else
					self.flush(self.JoinedModeledDict['ModelString']),
					NodifiedKeyStringsList
				)
			
			#It is going to be flushed so update the self.JoinedJoinedList to the last row index
			if self.JoinedJoinedList[1]==-1:
				self.JoinedJoinedList[1]=self.JoinedTabularedTable.nrows-1

			#Get the JoinedJoinedList
			JoinedListsList=map(lambda __FlushingVariable:__FlushingVariable.JoinedJoinedList,FlushingVariablesList)

			#Debug
			'''
			print('self.StructuredKeyString is ',self.StructuredKeyString)
			print('JoinedListsList is ',JoinedListsList)
			print('')
			'''

			#A row to reset the values plus the modeled and tabulared attributes is need but the join method doesn't need to be called one more time
			self.row(_LocalFlushingVariablesList['FlushingString'],**{'IsJoiningBool':False})

			#Just change the __ModeledInt
			map(
					lambda __NotRowedTuple,__JoinedList:
					self.RowedIdentifiedOrderedDict.__setitem__(__NotRowedTuple[0],__JoinedList),
					NotRowedTuplesList,
					JoinedListsList
				)

	def retrieveBefore(self,**_RetrievingVariablesDict):

		#Debug
		print('Joiner retrieveBefore method')
		print("self.ModeledDict['RetrievingTuplesList'] is ")
		SYS._print(self.ModeledDict['RetrievingTuplesList'])
		print('')

		#Check that there is the RetrievingTuplesList
		if 'RetrievingTuplesList' in self.ModeledDict:	

			#Join before
			self.join()

			'''

			#Set the self.JoinedRetrievingTuplesList and self.ModeledDict['RetrievingTuplesList']
			[
				self.JoinedRetrievingTuplesList,
				self.ModeledDict['RetrievingTuplesList']
			]=SYS.groupby(
							lambda __Tuple:
							SYS.getCommonPrefixStringWithStringsList(
									[__Tuple[0],JoinString]
								)==JoinString,
							self.ModeledDict['RetrievingTuplesList']
						)

			#Debug
			print('self.JoinedRetrievingTuplesList is ')
			SYS._print(self.JoinedRetrievingTuplesList)
			print("self.ModeledDict['RetrievingTuplesList'] is ")
			SYS._print(self.ModeledDict['RetrievingTuplesList'])
			print('')

			JoinedSortedDict=SYS.getSortedDictWithSortedTuplesList(
								map(
										lambda __JoinedTuple:
										(
											__JoinedTuple[0],
											(
												JoinString.join(__JoinedTuple[1][0].split(JoinString)[1:])
												if JoinString in __JoinedTuple[1][0]
												else __JoinedTuple[1][0],
												__JoinedTuple[1][1]
											)
										),
										map(
												lambda __JoinedSplittedRetrievingTuple:
												(
													#KeyString for the ModeledDict getting
													SYS.Node.DeepShortString.join(
														map(
																lambda __KeyString:
																SYS.Node.AppendShortString+self.JoinedNodifiedNodeString+SYS.Node.NodeString+JoinString.join(
																	__KeyString.split(
																		JoinString)[1:]
																),
																__JoinedSplittedRetrievingTuple[0][:-1]
															)
													)
													if len(__JoinedSplittedRetrievingTuple[0])>1
													else '/',
													#Tuple
													(
														#GettingString
														__JoinedSplittedRetrievingTuple[0][-1]
														if len(__JoinedSplittedRetrievingTuple[0])>1
														else
														__JoinedSplittedRetrievingTuple[0][0],
														#OperatorTuple
														__JoinedSplittedRetrievingTuple[1]
													)
												),
												#Split the PathString with the JoinDeepString
												map(
														lambda __JoinedRetrievingTuple:
														(
																__JoinedRetrievingTuple[0].split(
																	JoinDeepString
																),
																__JoinedRetrievingTuple[1]
														),
														self.JoinedRetrievingTuplesList
												)
											)
									)
							)

			#Debug
			print('JoinedSortedDict is ')
			SYS._print(JoinedSortedDict)
			print('')

			#Set the MergedRowedDictsListsList
			self.JoinedJoinedListKeyString=self.JoinedModeledString+'JoinedList'
			MergedRowedDictsListsTuplesList=map(
									lambda __AppendingGettingString,__Joiner:
									(
										__AppendingGettingString,
										SYS.getJoinedRowedDictsListWithJoinedListKeyString(
											__Joiner.merge(self.JoinedModelString).MergedRowedDictsList,
											self.JoinedJoinedListKeyString
										)
									)
									if __Joiner!=None else (__AppendingGettingString,[]),
									JoinedSortedDict.keys(),
									self.pick(JoinedSortedDict.keys())
								)
						

			#Debug
			print('MergedRowedDictsListsTuplesList is ')
			SYS._print(MergedRowedDictsListsTuplesList)
			print('JoinedSortedDict.values() is ')
			SYS._print(JoinedSortedDict.values())
			print('')

			#Set the JoinedFilteredMergedRowedDictsListsList
			self.JoinedFilteredMergedRowedDictsListTuplesList=map(
					lambda __MergedRowedDictsListsTuple,__JoinedRetrievingTuplesList:
					(
						__MergedRowedDictsListsTuple[0],
						map(
							lambda __MergedRowedDict:
							__MergedRowedDict,
							SYS.filterNone(
								SYS.where(
									__MergedRowedDictsListsTuple[1],
									__JoinedRetrievingTuplesList)
							)
						)
					),
					MergedRowedDictsListsTuplesList,
					JoinedSortedDict.values()
				)

			#Debug
			print('self.JoinedFilteredMergedRowedDictsListTuplesList is ')
			SYS._print(self.JoinedFilteredMergedRowedDictsListTuplesList)
			print('JoinedSortedDict.keys() is ',JoinedSortedDict.keys())
			if hasattr(self,'StructuredKeyString'):
				print('self.StructuredKeyString is ',self.StructuredKeyString)
			print('')

			#Set the JoinedRetrievingTuplesList
			self.JoinedRetrievingTuplesList=map(
					lambda __JoinedFilteredMergedRowedDictsListTuple,__JoinedRetrievingKeyString:
					(
						''.join(
							map(
								lambda __IntAndStringTuple:
								SYS.getDoneStringWithDoString(__IntAndStringTuple[1])
								if __IntAndStringTuple[0]==0
								else __IntAndStringTuple[1],
								enumerate(
											SYS.Node.AppendShortString.join(
											__JoinedRetrievingKeyString.split(
												SYS.Node.AppendShortString)[1:] 
											).split(SYS.Node.NodeString)
										)
								)
						)+self.JoinedJoinedListKeyString,
						(
						SYS.getIsInListBool,
						map(
								lambda __JoinedFilteredMergedRowedDict:
								__JoinedFilteredMergedRowedDict[self.JoinedJoinedListKeyString],
								__JoinedFilteredMergedRowedDictsListTuple[1]
							)
						)
					),
					self.JoinedFilteredMergedRowedDictsListTuplesList,
					JoinedSortedDict.keys()
				)

			#Debug
			print('self.JoinedRetrievingTuplesList is ',self.JoinedRetrievingTuplesList)
			print('')
			'''
			

		else:

			#Debug
			print('WARNING : Joiner, you have to mention a RetrievingTuplesList')
			print('')

	def retrieveAfter(self,**_RetrievingVariablesDict):

		#Debug
		print('Joiner retrieveAfter method')
		print('self.RetrievedFilteredRowedDictsList is ')
		SYS._print(self.RetrievedFilteredRowedDictsList)
		print('self.JoinedRetrievingTuplesList is ')
		SYS._print(self.JoinedRetrievingTuplesList)
		print('self.JoinedJoinedListKeyString is ')
		SYS._print(self.JoinedJoinedListKeyString)
		print('')

		#Rebound the RetrievedFilteredRowedDictsList
		self.RetrievedFilteredRowedDictsList=filter(
														lambda __Variable:
														__Variable!=None,
														SYS.where(
							self.RetrievedFilteredRowedDictsList,
							self.JoinedRetrievingTuplesList
							)
						)

		#Debug
		print('self.RetrievedFilteredRowedDictsList is ')
		SYS._print(self.RetrievedFilteredRowedDictsList)
		print('')

	def recoverBefore(self,**_LocalRecoveringVariablesDict):

		#Debug
		print('Joiner recoverBefore method')
		print('self.RetrievedFilteredRowedDictsList is ')
		SYS._print(self.RetrievedFilteredRowedDictsList)
		if hasattr(self,'StructuredKeyString'):
				print("self['StructuredKeyString'] is ",self.StructuredKeyString)
		print('')

		if len(self.RetrievedFilteredRowedDictsList)==1:
				
			#Debug
			'''
			print('Joiner It is good, there is one solution !')
			print('')
			'''

			#Define a JoinedRecoveredDict
			JoinedRecoveredDict=self.RetrievedFilteredRowedDictsList[0]

			#Debug
			print('Joiner JoinedRecoveredDict is ')
			SYS._print(JoinedRecoveredDict)
			print('self.JoinedNodifiedNodedString is ',self.JoinedNodifiedNodedString)
			if hasattr(self,'StructuredKeyString'):
				print("self['StructuredKeyString'] is ",self.StructuredKeyString)
			print('')

			#Maybe we have to recover the children before
			ChildJoinersList=self.JoinedNodifiedOrderedDict.values()
			if len(ChildJoinersList)>0:

				#Set each Children and recover each
				JoinedNodifiedKeyString=self.JoinedNodifiedNodedString+'KeyString'
					
				#Debug
				print('We are going to make recover each children before')
				if hasattr(self,'StructuredKeyString'):
					print('self.StructuredKeyString is ',self.StructuredKeyString)
				print('')
				print('self.JoinedNodifiedOrderedDict.values() is ',self.JoinedNodifiedOrderedDict.values())
				print('')

				#Map a Recover
				map(
						lambda __JoinedJoiner:
						__JoinedJoiner.__setitem__(
								'/App_Model_'+self.ModeledDict['ModelingString']+'Dict/RetrievingTuplesList',
								[
									(
										JoinString+self.JoinedJoinedListKeyString,
										(
											SYS.getIsEqualBool,
											JoinedRecoveredDict[
											self.JoinedNodifiedNodedString+getattr(
													__JoinedJoiner,
													JoinedNodifiedKeyString
													)+self.JoinedJoinedListKeyString
											]
										)
									)
								]
						).recover(self.ModeledModelString),
						ChildJoinersList
					)

			#Define the 
			AppendingGettingStringsList=SYS.unzip(self.JoinedFilteredMergedRowedDictsListTuplesList,[0])

			#Debug
			print('Joiner maybe update first the joined model')
			print('self.JoinedFilteredMergedRowedDictsListTuplesList is')
			SYS._print(self.JoinedFilteredMergedRowedDictsListTuplesList)
			print('AppendingGettingStringsList is ',AppendingGettingStringsList)

			#Next we have maybe to update with the joined model	
			if '/' in AppendingGettingStringsList:	

				#Define the IndexInt of the joined model
				IndexInt=AppendingGettingStringsList.index('/')

				#Define the JoinedFilteredMergedRowedDictsList
				JoinedFilteredMergedRowedDictsList=self.JoinedFilteredMergedRowedDictsListTuplesList[
						IndexInt][1] 

				#Define the JoinedJoinedList
				JoinedJoinedList=JoinedRecoveredDict[self.JoinedJoinedListKeyString]

				#Debug
				print('JoinedFilteredMergedRowedDictsList is ')
				SYS._print(JoinedFilteredMergedRowedDictsList)
				print('JoinedJoinedList is ',JoinedJoinedList)
				if hasattr(self,'StructuredKeyString'):
					print("self['StructuredKeyString'] is ",self.StructuredKeyString)
				print('')

				#Take the first element of self.JoinedFilteredMergedRowedDictsTuplesList which corresponds to the Joined Model at this level	
				JoinedRowedDict=next(
						RowedDict for RowedDict in JoinedFilteredMergedRowedDictsList
						if SYS.getIsEqualBool(
							RowedDict[self.JoinedJoinedListKeyString],
							JoinedJoinedList
						)
					)

				#Debug
				'''
				print('JoinedRowedDict is ',JoinedRowedDict)
				print('')
				'''

				#Update
				self.update(JoinedRowedDict.items())
			
		else:

			#Debug
			print('Joiner There are multiple retrieved states')
			if hasattr(self,'StructuredKeyString'):
				print("self['StructuredKeyString'] is ",self.StructuredKeyString)
			print('self.RetrievedFilteredRowedDictsList is ')
			SYS._print(self.RetrievedFilteredRowedDictsList)
			print('')

			#Stop the recover
			self.IsRecoveringBool=False


	#</DefineHookMethods>
	def join(	
				self,
				_ModelString="",
				**_LocalJoiningVariablesDict
			):

		#Debug
		print('Joiner join method')
		print('_ModelString is ',_ModelString)
		if hasattr(self,"StructuredKeyString"):
			print('self.StructuredKeyString is ',self.StructuredKeyString)
		print('')

		#Maybe we need to refresh some modeled and tabulared attributes
		if _ModelString!="":

			#Debug
			'''
			print('_ModelString is ',_ModelString)
			print('')
			'''

			#row
			self.tabular(_ModelString)

		else:

			#Set again the _ModelString
			_ModelString=self.ModeledDict['ModelString']

		#Init maybe the _LocalJoiningVariablesDict
		if 'IsJoiningBool' not in _LocalJoiningVariablesDict:

			#Set IsScanningBool to False
			_LocalJoiningVariablesDict['IsJoiningBool']=False

			#Refresh attributes
			self.JoinedJoinedList=[-1,-1]
			self.JoinedOrderedDict=collections.OrderedDict()
			self.JoinedNodifiedOrderedDict=collections.OrderedDict()

		#Init a default False IsCheckingJoiningBool
		if 'IsCheckingJoiningBool' not in _LocalJoiningVariablesDict:
			_LocalJoiningVariablesDict['IsCheckingJoiningBool']=True

		#Set the JoiningTuple
		if 'JoiningTuple' not in _LocalJoiningVariablesDict:

			#Case where there are joins
			if 'JoiningTuple' in self.ModeledDict:

				#Set to the _LocalJoiningVariablesDict
				_LocalJoiningVariablesDict['JoiningTuple']=self.ModeledDict['JoiningTuple']

				#Maybe we have to structure
				if self.IsGroupedBool==False:

					#Debug
					'''
					print('Join We have to structure first')
					if hasattr(self,"StructuredKeyString"):
						print('self.StructuredKeyString is ',self.StructuredKeyString)
					'''
					
					#Structure
					self.structure(self.ModeledDict['JoiningTuple'][0])

			else:

				#Set a default
				_LocalJoiningVariablesDict['JoiningTuple']=("","")

		#Nodify if there is the nodified objects
		if _LocalJoiningVariablesDict['IsJoiningBool']==False and _LocalJoiningVariablesDict['JoiningTuple'
		][0]!="":

			#Debug
			print('Joiner we are going to nodify the ',_LocalJoiningVariablesDict['JoiningTuple'][0])
			if hasattr(self,'StructuredKeyString'):
				print('self.StructuredKeyString is ',self.StructuredKeyString)
			print('')

			#Nodify
			self.nodify(_LocalJoiningVariablesDict['JoiningTuple'][0])
			self.JoinedNodifiedOrderedDict=copy.copy(self.NodifiedOrderedDict)
			self.JoinedNodifiedNodeString=self.NodifiedNodeString
			self.JoinedNodifiedNodedString=SYS.getDoneStringWithDoString(self.NodifiedNodeString)
			self.JoinedNodifiedNodingString=SYS.getDoneStringWithDoString(self.NodifiedNodeString)

			#Debug
			print('self.JoinedNodifiedOrderedDict is ',self.JoinedNodifiedOrderedDict)
			print('')

		#Debug
		'''
		print("_LocalJoiningVariablesDict['JoiningTuple'] is ",
			_LocalJoiningVariablesDict['JoiningTuple'])
		print('')
		'''

		#If there is a joined model
		if len(self.JoinedNodifiedOrderedDict)==0 or _LocalJoiningVariablesDict['IsJoiningBool']:

			#Debug
			'''
			print('We have the right to join !')
			print("_LocalJoiningVariablesDict['IsJoiningBool']",
				_LocalJoiningVariablesDict['IsJoiningBool'])
			print('self.ModeledDict is ',self.ModeledDict)
			print('')
			'''

			#Tabular for the _JoiningRowingString if JoiningTuple is not None
			if _LocalJoiningVariablesDict['JoiningTuple'][1]!=None:
				self.JoinedModelString=_LocalJoiningVariablesDict['JoiningTuple'][1]
				self.JoinedModeledString=SYS.getDoneStringWithDoString(self.JoinedModelString)
				self.tabular(self.JoinedModelString)
				self.JoinedModeledDict=copy.copy(self.ModeledDict)
				self.JoinedModeledKeyString=self.ModeledKeyString
				self.JoinedJoinedList[0]=self.ShapedTabularedInt
				self.JoinedTabularedTable=self.TabularedTable

			#Debug
			'''
			print('self.JoinedModeledDict is ',self.JoinedModeledDict)
			print('self.JoinedModeledKeyString is ',self.JoinedModeledKeyString)
			print('self.JoinedJoinedList is ',self.JoinedJoinedList)
			print('')
			'''

			#Check if the actual setup is already rowed
			if _LocalJoiningVariablesDict['IsCheckingJoiningBool'] and self.JoinedJoinedList[1]==-1:

				#Define the GettingStringsList and the GettedVariablesList
				if 'ColumningTuplesList' in self.JoinedModeledDict:

					#Get the GettingStringsList and GettedVariablesList
					GettingStringsList=SYS.unzip(self.JoinedModeledDict['ColumningTuplesList'],[0])
					GettedVariablesList=self.pick(GettingStringsList)

					#Debug
					'''
					print(map(
							lambda __Row:
							map(
									lambda __GettingString,__GettedVariable:
									all(__Row[__GettingString]==__GettedVariable),
									GettingStringsList,
									GettedVariablesList
								),
							JoinedTabularedTable.iterrows()
						)
					)
					'''

					#Check if it was already rowed
					IsRowedBoolsList=map(
							lambda __Row:
							all(
								map(
										lambda __GettingString,__GettedVariable:
										SYS.getIsEqualBool(__Row[__GettingString],__GettedVariable),
										GettingStringsList,
										GettedVariablesList
									)
							),
							self.JoinedTabularedTable.iterrows()
						)					

					#Debug
					'''
					print('self.StructuredKeyString is ',self.StructuredKeyString)
					print('IsRowedBoolsList is ',IsRowedBoolsList)
					print('')
					'''

					#If it is rowed then set the JoinedJoinedList
					try:
						IndexInt=IsRowedBoolsList.index(True)
					except ValueError:
						IndexInt=-1
					self.JoinedJoinedList[1]=IndexInt

			#Give to itself
			JoinedOrderedDictKeyString=self.JoinedNodifiedNodedString+'JoinedOrderedDict'
			if hasattr(self,JoinedOrderedDictKeyString):
				self.JoinedOrderedDict=getattr(self,JoinedOrderedDictKeyString)
			if self.JoinedOrderedDict==None:
				self.JoinedOrderedDict=collections.OrderedDict()
			self.JoinedOrderedDict.__setitem__(
				self.JoinedJoinedListKeyString,self.JoinedJoinedList
			)

			if self.JoinedNodifiedNodedString!="":
				ParentPointer=getattr(self,self.JoinedNodifiedNodedString+'ParentPointer')
				if ParentPointer!=None:
					#Give to the Parent
					JoinedOrderedDict=getattr(ParentPointer,JoinedOrderedDictKeyString)
					JoinedOrderedDict[getattr(self,self.JoinedNodifiedNodedString+'KeyString'
						)+self.JoinedJoinedListKeyString]=self.JoinedJoinedList

			#Debug
			'''
			print('JoinedModeledDict is ',JoinedModeledDict)
			print('JoinedList is ',JoinedList)
			print('')
			'''

			#Tabular again to set again the ModeledDict
			self.tabular(_ModelString)
				
		else:

			#Debug
			'''
			print('This either a not last level of child or it is not yet authorized to join')
			print('self.JoinedNodifiedOrderedDict.values() is ',self.JoinedNodifiedOrderedDict.values())
			print('len(self.NodifiedOrderedDict.values()) is ',len(self.NodifiedOrderedDict.values()))
			print("_LocalJoiningVariablesDict['IsJoiningBool']",_LocalJoiningVariablesDict['IsJoiningBool'])
			print('so join the deeper children groups first')
			print('')
			'''

			map(
					lambda __JoiningObject:
					__JoiningObject.join(
											self.ModeledDict['ModelString'],
											**_LocalJoiningVariablesDict
										),
					self.JoinedNodifiedOrderedDict.values()
				)

			'''
			print('The deeper children groups are joined now')
			print('So join here !')
			print('')
			'''

			self.join(
						self.ModeledDict['ModelString'],
						**dict(
								_LocalJoiningVariablesDict,**{'IsJoiningBool':True}
							)
					)


		#Return self
		return self

#</DefineClass>

#<DefineAttestingFunctions>
def attest_join():

	#Build Hdf groups
	Joiner=SYS.JoinerClass().hdformat().update(
								[
									(
										'App_Structure_ChildJoiner1',
										SYS.JoinerClass().update(
										[
											('App_Model_ParameterizingDict',
														{
															'ColumningTuplesList':
															[
																('MyIntsList',tables.Int64Col(shape=2))
															]
														}
											),
											('MyIntsList',[2,4]),
											('App_Model_ResultingDict',
														{
															'ColumningTuplesList':
															[
																('MyInt',tables.Int64Col())
															]
														}
											),
											('MyInt',1)
										])
									),
									('App_Model_ParameterizingDict',
														{
															'ColumningTuplesList':
															[
																('MyString',tables.StringCol(10)),
															]
														}
									),
									('MyString',"hello")
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':""}),
													('model',{'ArgsVariable':"Parameter"}),
													('tabular',{'ArgsVariable':""}),
													('row',{'ArgsVariable':""}),
													('flush',{'ArgsVariable':""})
												]
											}
									).update(
										[
											
											('App_Model_ResultingDict',
												{
													'ColumningTuplesList':
													[
														('MyFloat',tables.Float32Col()),
														('MyFloatsList',tables.Float32Col(shape=3))
													],
													'JoiningTuple':("Structure","Parameter")
												}
											),
											('MyFloat',0.1),
											('MyFloatsList',[2.3,4.5,1.1])
										]
									).model("Result"		
									).tabular(
									).row(
									).flush("Result"
									).close()

	#Return the object itself
	return "Object is : \n"+SYS.represent(
		Joiner
		)+'\n\n\n'+SYS.represent(os.popen('/usr/local/bin/h5ls -dlr '+Joiner.HdformatingPathString
		).read())
#</DefineAttestingFunctions>
