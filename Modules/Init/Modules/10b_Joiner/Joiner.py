#<ImportModules>
import collections
import copy
import numpy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
JoinString='__'
JoinDeepString='/'
BasingLocalTypeString="Shaper"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class JoinerClass(BasedLocalClass):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.JoinedOrderedDict=collections.OrderedDict()		#<NotRepresented>
		self.JoinedDatabasePointer=None							#<NotRepresented>
		#</DefineSpecificDict>

	@SYS.HookerClass(**{'BeforeTuplesList':[("<TypeString>","join")],'AfterTuplesList':[("Featurer","model")]})
	def model(self,**_VariablesDict):

		#Debug
		self.debug('Start of the method')

		#Model if there is a JoinedDatabasePointer
		if self.JoinedDatabasePointer!=None:

			#Set the columns
			self.ModeledClass.columns[self.JoinedRetrievingTupleKeyString]=tables.Int64Col(shape=2)
			
		#Debug
		self.debug('End of the method')

	@SYS.HookerClass(**{'AfterTuplesList':[("Featurer","row")]})
	def row(self,**_VariablesList):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						"self.ModelingModelString is "+str(self.ModelingModelString),
						"We are going to check if the joined model is already flushed...",
						"First look if self.JoinedModeledDatabase!={} is "+str(len(self.JoinedModeledDatabase)>0)
					]
				)

		#Check
		if self.JoinedDatabasePointer!=None:

			#Debug
			self.debug("Ok there is a self.JoinedDatabasePointer")

			#Alias
			JoinedRetrievingTuple=self.JoinedRetrievingTuple
			JoinedRetrievingTupleKeyString=self.JoinedRetrievingTupleKeyString

			#Debug
			self.debug('Check that this joined row is a new row in the table or not')

			#Get the GettingStringsList and GettedVariablesList
			GettingStringsList=SYS.unzip(self.JoinedModeledDatabase.ModelingColumningTuplesList,[0])
			GettedVariablesList=self.pick(GettingStringsList)

			#Check if it was already rowed
			IsRowedBoolsList=map(
					lambda __Row:
					all(
						map(
								lambda __GettingString,__GettedVariable:
								SYS.getIsEqualBool(__Row[__GettingString],__GettedVariable),
								GettingStringsList,
								GettedVariablesList
							)
					),
					self.JoinedDatabasePointer.TabledTable.iterrows()
				)					

			#Debug
			self.debug(
						[
							'self.StructuredKeyString is '+str(
								self.StructuredKeyString) if hasattr(self,'StructuredKeyString') else "",
							'IsRowedBoolsList is '+str(IsRowedBoolsList)
						]
					)

			#If it is rowed then set the JoinedRetrievingTuple
			try:
				ModeledInt=IsRowedBoolsList.index(True)
			except ValueError:
				ModeledInt=-1

			#Debug
			self.debug('So the corresponding ModeledInt is '+str(ModeledInt))

			#Set the ModeledInt
			JoinedRetrievingTuple[1]=ModeledInt


		#Debug
		self.debug('End of the method')

	@SYS.HookerClass(**{'AfterTuplesList':[("Featurer","flush")]})
	def flush(self,**_FlushingVariablesList):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(	
					[
						"self.ModelingModelString is "+str(self.ModelingModelString),
						'self.StructuredKeyString is '+str(self.StructuredKeyString) if hasattr(self,"StructuredKeyString") else ''
					]
				)

		#Define the NotRowedTuplesList
		#NotRowedTuplesList=filter(
		#		lambda __JoiningTuple:
		#		__JoiningTuple[1][0]<0 or __JoiningTuple[1][1]<0,
		#		self.ModeledDict['JoinedOrderedDict'].items()
		#	)

		#flush the joined model
		if self.JoinedDatabasePointer!=None:

			if self.JoinedDatabasePointer.ModelingModelString!="":

				#Debug
				self.debug(
							[
								'Flush self with the joined model'
							]
						)

				#Flush
				self.JoinedDatabasePointer.flush()

				#Debug
				self.debug('Flush self with the joined model was done')

				#Set the JoinedRetrievingTupleKeyString
				JoinedRetrievingTupleKeyString=self.JoinedDatabasePointer['ModelingModelString']+'RetrievingTuple'

				#Alias
				JoinedRetrievingTuple=self.JoinedDatabasePointer['RetrievingTuple']

				#It is going to be flushed so update the JoinedRetrievingTuple to the last row index
				if JoinedRetrievingTuple==-1:

					#Debug
					self.debug(
								[
									'This is a new row so we just set the ModeledInt of the <JoinedModeledString>RetrievingTuple',
									'To the size of the table',
									'JoinedRetrievingTuple is '+str(
										JoinedRetrievingTuple)
								]
							)

					#Update the corresponding RetrievingTuple
					JoinedRetrievingTuple[1]=self.JoinedDatabasePointer['TabledTable'].nrows-1

					#Debug
					self.debug('Now JoinedRetrievingTuple is '+str(
						JoinedRetrievingTuple))

		#Debug
		self.debug(
					[
						'self.StructuredKeyString is '+str(self.StructuredKeyString) if hasattr(
							self,'StructuredKeyString') else '',
						"self.ModeledDict['ModelString'] is "+str(self.ModelingModelString),
						'We add in the RowedIdentifiedOrderedDict the Joined JoinedRetrievingTuples',
						"self.RowedIdentifiedOrderedDict is "+str(
								self.RowedIdentifiedOrderedDict),
						"NotRowedTuplesList is "+str(NotRowedTuplesList)
					]
		)

		#Debug
		self.debug('We set the JoinedRetrievingTuple in the RowedIdentifiedOrderedDict')

		#Update the self.RowedIdentifiedOrderedDic
		self.RowedIdentifiedOrderedDict.__setitem__(
					JoinedRetrievingTupleKeyString,
					JoinedRetrievingTuple
				)

		#Debug
		self.debug('End of the method')

	@SYS.HookerClass(**{'AfterTuplesList':[("Featurer","retrieve")]})
	def retrieveAfter(self,**_VariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug("self.",self,['RetrievingTuple','RetrievedTuplesList'])

		#Define the RetrievingTupleIndexInt
		if len(self.RetrievedTuplesList)>0:

			#Get the Index 
			RetrievingTupleIndexInt=SYS.unzip(self.RetrievedTuplesList,[0]).index(
				self.JoinedRetrievingTupleKeyString)

			#Set and retrieve for the joined model
			self.RetrievingTuple=self.RetrievedTuplesList[RetrievingTupleIndexInt][1]
			self.JoinedDatabasePointer.retrieve()

		#Debug
		self.debug('End of the method')

	@SYS.HookerClass(**{'AfterTuplesList':[("Featurer","find")]})
	def find(self,**_VariablesDict):

		#Debug
		self.debug('Start of the method')

		#Check that JoiningModelString is good
		if JoiningModelString!="":

			#Find
			self.JoinedDatabasePointer.find()

			#Debug
			self.debug('Ok we have found the joined model')

			#Copy the FoundFilteredRowedDictsList
			JoinedFoundFilteredRowedDictsList=self.JoinedDatabasePointer['FoundFilteredRowedDictsList']

			#Debug
			self.debug(
						[
							'JoinedFoundFilteredRowedDictsList is ',
							SYS.represent(JoinedFoundFilteredRowedDictsList)
						]
			)

			#Alias
			JoinedRetrievingTupleKeyString=self.ModeledDict['JoinedRetrievingTupleKeyString']

			#Make the TabledInt and the ModeledInt as a <JoiningModelString>RetrievingTuple
			map(
					lambda __JoinedFoundFilteredRowedDict:
					__JoinedFoundFilteredRowedDict.__setitem__(
						JoinedRetrievingTupleKeyString,
						[
							__JoinedFoundFilteredRowedDict['TabledInt'],
							__JoinedFoundFilteredRowedDict['ModeledInt'],
						]
					),
					JoinedFoundFilteredRowedDictsList
				)

			#Set the JoinedFindingTuplesList
			JoinedFindingTuplesList=[
						(
							JoinedRetrievingTupleKeyString,
							(
								SYS.getIsInListBool,
								map(
										lambda __JoinedFoundFilteredRowedDict:
										[
											__JoinedFoundFilteredRowedDict['TabledInt'],
											__JoinedFoundFilteredRowedDict['ModeledInt'],
										],
										JoinedFoundFilteredRowedDictsList
									)
							)
					)
				]

			#Debug
			self.debug(
						[
							'JoinedFindingTuplesList is',
							SYS.represent(JoinedFindingTuplesList)
						]
					)

		else:

			#Set a default empty
			JoinedFindingTuplesList=[]

		#Debug
		self.debug(
					[
						('self.ModeledDict',self.ModeledDict,['ModelString','FoundRowedDictsList'])
					]
				)

		#Put them in the ModeledDict
		LocalVars=vars()
		map(
				lambda __GettingString:
				self.ModeledDict.__setitem__(__GettingString,LocalVars[__GettingString]),
				[
					'JoinedFindingTuplesList',
				]
			)

		#Debug
		self.debug('End of the method')

	def findAfter(self,**_FindingVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						('Are we going to do a where with the FoundFilteredRowedDictsList and the '),
						('filtering JoinedFindingTuplesList?'),
						('self.ModeledDict ',self.ModeledDict,[
																'ModelString',
																'FoundFilteredRowedDictsList'
																]),
						("'JoinedFindingTuplesList' in self.ModeledDict is "+str(
							'JoinedFindingTuplesList' in self.ModeledDict))
					]
			)

		if 'JoinedFindingTuplesList' in self.ModeledDict:

			#Debug
			self.debug(
						[
							'Ok we are going to do the where',
							"self.ModeledDict['JoinedFindingTuplesList'] is "+str(
								self.ModeledDict['JoinedFindingTuplesList'])
						]
					)

			#Where
			self.ModeledDict['FoundFilteredRowedDictsList']=SYS.filterNone(SYS.where(
							self.ModeledDict['FoundFilteredRowedDictsList'],
							self.ModeledDict['JoinedFindingTuplesList']
							)
			)

			#Debug
			self.debug('Ok the where is done.')

		#Debug
		self.debug(
					[
						'After intersection',
						('self.ModeledDict ',self.ModeledDict,[
															'ModelString',
															'FoundFilteredRowedDictsList'
															]
														)
					]
				)

		#Debug
		self.debug('End of the method')

	@SYS.HookerClass(**{'AfterTuplesList':[("Featurer","recover")]})
	def recover(self,**_LocalRecoveringVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						('self.',self,[
											'ModelingModelString',
											'FoundFilteredRowedDictsList'
											]
										)
					]
				)

		#Debug
		self.debug(
					[
						'Look if we have found only one FilteredRowedDict',
						'len(self.FoundFilteredRowedDictsList) is '+str(len(self.FoundFilteredRowedDictsList))
					]
				)

		if len(self.FoundFilteredRowedDictsList)==1:
				
			#Debug
			self.debug('It is good, there is one solution !')

			#Define a JoinedRecoveredDict
			JoinedRecoveredDict=self.FoundFilteredRowedDictsList[0]

			#Alias
			JoinedRetrievingTupleKeyString=self.ModeledDict['JoinedRetrievingTupleKeyString']
				
		else:

			#Debug
			self.debug('There are multiple found states')

			#Stop the recover
			self.RecoveringIsBool=False

		#Debug
		self.debug('End of the method')

	@SYS.HookerClass(**{'AfterTuplesList':[("<TypeString>","table")]})
	def join(	
				self,
				**_VariablesDict
			):

		#Debug
		self.debug('Start of the method')

		#Alias
		JoiningModelString=self.ModeledDict['JoiningModelString']

		#Table maybe the joined model
		if self.JoinedDatabasePointer.ModelingModelString!="":

			#Debug
			self.debug('Table maybe the joined model')

			#Table
			self.table(JoiningModelString)

			#Check
			if JoinedModeledDict!={}:

				#Debug
				self.debug('We are building the JoinedRetrievingTuple and give him the TabledInt')

				#Set the JoinedRetrievingTupleKeyString
				JoinedRetrievingTupleKeyString=JoinedModeledDict['ModeledString']+'RetrievingTuple'
				self.ModeledDict['JoinedRetrievingTupleKeyString']=JoinedRetrievingTupleKeyString

				#Set in the ModeledDict
				self.ModeledDict['JoinedOrderedDict'][JoinedRetrievingTupleKeyString]=[JoinedModeledDict['TabledInt'],-1]

				#Debug
				self.debug("OK self.ModeledDict['JoinedOrderedDict' is so "+str(
					self.ModeledDict['JoinedOrderedDict']))
				
		#Debug
		self.debug('End of the method')

		#Return self
		return self
#</DefineClass>

#<DefineAttestingFunctions>
def attest_flush():

	#Define a binding method
	SYS.JoinerClass.setSumInt=SYS.BinderClass(
						**{
							'ConditionTuplesList':[
										('SettingKeyVariable',(SYS.getIsInListBool,['FirstInt','SecondInt']))
									]
						}
					)(
						lambda _InstanceVariable:_InstanceVariable.__setattr__(
								'SumInt',
								_InstanceVariable.FirstInt+_InstanceVariable.SecondInt 
								if hasattr(_InstanceVariable,'FirstInt') and hasattr(_InstanceVariable,'SecondInt')
								else None
								)
					)

	Joiner=JoinerClass().update([
									('FirstInt',1),
									('SecondInt',1),
							]).setSumInt(
							).update([
											('FirstInt',3),
											('SecondInt',5),
							]).update(								
									[
										(
											'<Model>ParameterDatabase',
											SYS.DatabaseClass().update(
												[
													('ModelingColumnTuplesList',
														[
															('FirstInt',(tables.Int64Col())),
															('SecondInt',(tables.Int64Col()))
														]
													),
													('FeaturingAllBool',True)
												]
											)
										),
										(
											'<Model>ResultDatabase',
											SYS.DatabaseClass().update(
												[
													('ModelingColumnTuplesList',
														[
															('SumInt',tables.Int64Col()),
														]
													),
													('FeaturingAllBool',True)
												]
											)
										)
									]
									).update(
										[
											('FirstInt',1),
											('SecondInt',1),
										]
									).flush(
									).close()
												
	#Get the h5ls version of the stored hdf
	return SYS.represent(
						Joiner
						)+'\n\n\n\n'+Joiner.hdfview().HdformatedString
	'''
#</DefineAttestingFunctions>
