#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

Applyier=SYS.ApplyierClass().__setitem__(
                    'TestingAttestFunctionStringsList',
                    [
                    	'attest_apply',
                    	'attest_pick',
                      	'attest_collect',
                      	'attest_update',
                      	'attest_add'
                    ]
                  )
Applyier.attest()
Applyier.test()
