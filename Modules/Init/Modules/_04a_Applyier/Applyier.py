#<ImportModules>
import copy
import operator
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class ApplyierClass(SYS.NoderClass):
	
	#<DefineHookMethods>
	@SYS.HookerClass(**{'AfterTypeString':"Node"})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.IsApplyingBool=True #<NotRepresented>
		self.AppliedMappedVariablesList=[] #<NotRepresented>
		#</DefineSpecificDict>

	def map(self,_MappingFunction,*_MappedList):
		"""Just map"""
		map(_MappingFunction,*_MappedList)
		return self

	def apply(self,_MethodString,_AppliedVariablesList):
		"""Map a call with the _MethodString to the _AppliedVariablesList"""

		#Get the AppliedMethod
		if hasattr(self,_MethodString):
			self.AppliedMappedVariablesList=map(
					lambda __AppliedVariable:
					SYS.getWithMethodAndArgsList(getattr(self,_MethodString),__AppliedVariable)
					if hasattr(self,_MethodString)
					else None,
					_AppliedVariablesList
				)

		#Return self
		return self

	def pick(self,_GettingVariablesList):
		"""Apply the __getitem__ to the <_GettingVariablesList>"""

		#Apply __getitem__
		self.apply('__getitem__',_GettingVariablesList)

		#Return AppliedVariablesList
		return self.AppliedMappedVariablesList

	def gather(self,_GatheringVariablesList):
		"""Reduce a map of pick for GatheringVariable being a List or __getitem__ for GatheringVariable being a GettingVariable"""
			
		print(_GatheringVariablesList)
		print(self['StructuredOrderedDict.items()'])

		return reduce(
						operator.__add__,
						map(
								lambda __GatheringVariable:
								zip(__GatheringVariable,self.pick(__GatheringVariable))
								if type(__GatheringVariable)==list
								else self[__GatheringVariable],
								_GatheringVariablesList
						)
					) if len(_GatheringVariablesList)>0 else []

	def collect(self,_CollectingGatheringVariablesList,_CollectingGatheredVariablesList):
		"""Reduce a map of pick for GatheringVariable being a List or __getitem__ for GatheringVariable being a GettingVariable"""
		
		print(_CollectingGatheringVariablesList)

		#Get the GatheredVariablesList
		GatheredVariablesList=zip(*self.gather(_CollectingGatheringVariablesList))
		if len(GatheredVariablesList)>0:

			#Just keep the values
			GatheredVariablesList=GatheredVariablesList[1]

			#Reduce a gather for each
			return reduce(
							operator.__add__,
							map(
									lambda __CollectingApplyier:
									__CollectingApplyier.gather(_CollectingGatheredVariablesList)
									if hasattr(__CollectingApplyier,'gather') else None,
									GatheredVariablesList
							)
						)

	def update(self,_SettingVariableTuplesList):
		"""Apply the __setitem__ to the <_SettingVariableTuplesList>"""

		#Apply __setitem__
		return self.apply('__setitem__',_SettingVariableTuplesList)

	def __add__(self,_AddingVariablesList):
		"""Apply the append to the <_AddingVariablesList>"""

		#Apply
		self.apply('append',_AddingVariablesList)

		#Return 
		return self
#</DefineClass>

#<DefineAttestingFunctions>
def attest_apply():

	#Apply a __setitem__ to several things 
	Applyier=SYS.ApplyierClass().apply('__setitem__',[
													('MyString',"Hello"),
													('MySecondString',"Bonjour")
												]
									)

	#Apply an apply is possible 
	return Applyier.apply(
							'__setitem__',
							[
											('MyThirdString',"GutenTag"),
											('apply',{'ArgsVariable':(
															'__setitem__',[
																		('MyInt',0)
																		]
																)
													}
											),
											('MyNotLostString',"ben he"),
							]
				)

def attest_pick():

	#Define an Applyier
	Applyier=SYS.ApplyierClass().update([
										('MyInt',0),
										('MyApplyier',SYS.ApplyierClass().update(
											[
												('MyString',"hello"),
												('MyFloat',0.1)
											]
											)
										),
										('App_Group_MyAppendedApplyier',SYS.ApplyierClass().update(
											[
												('MyOtherString',"bonjour"),
												('MyOtherFloat',0.2)
											]
											)
										),
										('App_Sort_MySecondInt',3)
									])
	#Map some gets
	TestedVariable=Applyier.pick(
								[
									#Get directly in the __dict__
									'MyInt',
									'MyApplyier',

									#Get with copying
									'Cop_MyApplyier',

									#Get with a DeepShortString
									'/MyApplyier/MyString',

									#Get with a AppendShortString
									'App_Sort_MySecondInt',
									'App_Group_MyAppendedApplyier',
									'Cop_App_Group_MyAppendedApplyier',
									'/App_Group_MyAppendedApplyier/MyOtherString',
									#Or the AppendedInt
									'App_Group_0',

									#Get in the class object
									'/__class__/TypeString'
								]
							)

	#Change in the objects for looking at the copy difference
	Applyier['/MyApplyier/MyFloat']=0.4444
	Applyier['/App_Group_MyAppendedApplyier/MyOtherFloat']=0.5555

	#Return
	return TestedVariable

def attest_gather():

	return SYS.ApplyierClass().__add__(
			[
				SYS.ApplyierClass().update(
					[
						('StructuredKeyString',str(Int1))
					]).__add__(
						[
							SYS.ApplyierClass().update(
										[
											('StructuredKeyString',str(Int2))
										]
									)
							for Int2 in xrange(2)
						]
					) for Int1 in xrange(2)
			]).update(
			[
				('App_Group_'+str(Int1),SYS.ApplyierClass().update(
					[
						('GroupedKeyString',str(Int1))
					]).update(
						[
							('App_Group_'+str(Int2),SYS.ApplyierClass().update(
										[
											('GroupedKeyString',str(Int2))
										]
									)
							)
							for Int2 in xrange(2)
						]
					)
				) for Int1 in xrange(2)
			]).gather([
							['/'],
							'StructuredOrderedDict.items()',
							'GroupedOrderedDict.items()'
						])

def attest_collect():

	Applyier=SYS.ApplyierClass().__add__(
			[
				SYS.ApplyierClass().update(
					[
						('StructuredKeyString',str(Int1))
					]).__add__(
						[
							SYS.ApplyierClass().update(
										[
											('StructuredKeyString',str(Int2))
										]
									)
							for Int2 in xrange(2)
						]
					) for Int1 in xrange(2)
			])

	print(Applyier)
	
	return Applyier.collect([
							['/'],
							'StructuredOrderedDict.items()',
							['App_Structure_1'],
						],
						[
							['StructuredKeyString']
						]
					)

def attest_update():

	#Update several things
	Applyier=SYS.ApplyierClass().update([
										('MyInt',0),
										('App_Sort_MySecondInt',0),
										('MyFloat',0.2)
									]
								)

	#Update in a update is possible
	return Applyier.update([
										('MyString',"Hello"),
										('update',{'ArgsVariable':[
													('MySecondInt',2)
													]}
										),
										('MyNotLostString',"ben heee")
									]
								)

def attest_add():

	#Explicit expression
	return SYS.ApplyierClass().__add__([
												[
													('StructuredKeyString',"MyTuplesList"),
													('MyString',"Hello")
												],
												{
													'StructuredKeyString':"MyDict",
													'MyOtherString':"Bonjour"
												},
												SYS.ApplyierClass()
										])
#</DefineAttestingFunctions>
