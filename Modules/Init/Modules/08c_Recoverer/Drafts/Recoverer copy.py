#<ImportModules>
import collections
import copy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class RecovererClass(
						SYS.FindorClass
				):
	
	#<DefineHookMethods>
	def recoverBefore(self,**_RecoveringVariablesDict):

		#Debug
		self.debug('Start of the method')

		#If it was not yet setted
		if self.RecoveredDict=={}:

			#Debug
			print('self.RetrievedFilteredRowedDictsList is ')
			SYS._print(self.FoundFilteredRowedDictsList)
			print('')

			if len(self.FoundFilteredRowedDictsList)==1:
				
				#Debug
				print('It is good, there is one solution !')
				print('')

				#Set the RecoveredDict
				self.RecoveredDict=self.FoundFilteredRowedDictsList[0]

			else:

				#Debug
				print('Recoverer There are not multiple retrieved states')
				print('self.FoundFilteredRowedDictsList is ',self.FoundFilteredRowedDictsList)
				print('')

		#Update
		self.update(self.RecoveredDict.items())

	#</DefineHookMethods>

	#<DefineMethods>
	def recover(self,_ModelString,**_RecoveringVariablesDict):
		"""Call the retrieve<HookString> methods and return self by default"""

		#Debug
		'''
		print('Recoverer recover method')
		print('')
		'''

		#Maybe refresh some modeled attributes
		if _ModelString!="":
			self.find(_ModelString)
		else:
			_ModelString=self.ModeledModelingString
			
		#Refresh the attributes 
		LocalRecoveringVariablesDict=dict(
			{'RecoveringString':_ModelString},**_RecoveringVariablesDict
		)
		LocalOutputedPointer=self
		self.IsRecoveringBool=True
		self.RecoveredDict={}

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='recover'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalRecoveringVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalRecoveringVariablesDict' in OutputVariable:
							LocalRecoveringVariablesDict=OutputVariable['LocalRecoveringVariablesDict']
						if 'LocalOutputedPointer' in OutputVariable:
							LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

					#Check Bool
					if self.IsRecoveringBool==False:
						return LocalOutputedPointer

		#Return the LocalOutputedPointer
		return LocalOutputedPointer


#</DefineClass>

#<DefineAttestingFunctions>
def attest_recover():

	#Bound an output method
	def outputAfter(_Recoverer):
		_Recoverer.MulInt=_Retriever.FirstInt*_Retriever.SecondInt
	SYS.RecovererClass.HookingMethodStringToMethodsListDict['outputAfter']=[outputAfter]

	#Retrieve
	Recoverer=SYS.RecovererClass(
		).update(
					[
						('FirstInt',0),
						('SecondInt',0),
						('App_Model_ParameterizingDict',{
											'ColumningTuplesList':
											[
												#ColumnString 	#Col 	
												('FirstInt',	tables.Int64Col()),
												('SecondInt',	tables.Int64Col())
											],
											'IsFeaturingBool':True,
										}),
						('App_Model_ResultingDict',{
											'ColumningTuplesList':
											[
												#ColumnString 	#Col 	
												('MulInt',	tables.Int64Col())
											],
											'JoiningTuple':("","Parameter")
										})
					]
		).update(
					[
						('FirstInt',1),
						('SecondInt',2)
					]
		).flush('Result'
		).update(
					[
						('FirstInt',1),
						('SecondInt',3)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',2)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',3)
					]
		).flush(
		).update(
					[
						('FirstInt',3),
						('SecondInt',3)
					]
		).flush(
		).recover(
					'Result'	
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Retriever
		)+'\n\n\n'+SYS.represent(
				os.popen('h5ls -dlr '+Retriever.HdformatingPathString).read()
			)
#</DefineAttestingFunctions>
