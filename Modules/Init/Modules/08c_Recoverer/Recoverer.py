#<ImportModules>
import collections
import copy
import tables
import operator
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Findor"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class RecovererClass(
						BasedLocalClass
				):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.RecoveredDict={} 					#<NotRepresented>
		#</DefineSpecificDict>

	@SYS.HookerClass(**{'AfterTuplesList':[("<TypeString>","find")]})
	def recover(self,**_VariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						'Is the self.RecoveredDict already setted ?',
						'len(self.RecoveredDict) is '+str(len(self.RecoveredDict))
					]
			)

		#Check
		if len(self.RecoveredDict)==0:

			#Debug
			self.debug('The RecoveredDict is not yet setted')

			#Debug
			self.debug(
						[
							'Look if we have found only one FilteredRowedDict',
							'len(self.FoundFilteredRowedDictsList) is '+str(len(self.FoundFilteredRowedDictsList))
						]
					)

			#Check
			if len(self.FoundFilteredRowedDictsList)==1:

				#Debug
				self.debug('It is good, there is one solution !')

				#Set the RecoveredDict
				self.RecoveredDict.update(self.FoundFilteredRowedDictsList[0])

		#Debug
		self.debug(
					[
						'Now we update with the self.RecoveredDict',
						'self.RecoveredDict is '+str(self.RecoveredDict)
					]
				)

		#Set the RetrievingTuple and retrieve
		self.RetrievingTuple=(0,self.RetrievedModeledInt)
	
		#Now we can retrieve
		self.retrieve()

		#Debug
		self.debug('End of the method')
#</DefineClass>

#<DefineAttestingFunctions>
def attest_recover():
	Recoverer=SYS.RecovererClass(
		).update(
					[
						('ModelingColumnTuplesList',
											[
												#ColumnString 	#Col 	
												('FirstInt',	tables.Int64Col()),
												('SecondInt',	tables.Int64Col())
											]
						),
						('FindingTuplesList',
							[
								('SecondInt',(operator.gt,3))
							]
						),
						('RowedIdentifiedGettingStringsList',['FirstInt','SecondInt'])
					]
		).update(
					[
						('FirstInt',1),
						('SecondInt',2)
					]
		).flush(
		).update(
					[
						('FirstInt',1),
						('SecondInt',3)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',2)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',3)
					]
		).flush(
		).update(
					[
						('FirstInt',3),
						('SecondInt',4)
					]
		).flush(
		).recover(		
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Recoverer
		)+'\n\n\n'+Recoverer.hdfview().HdformatedString
#</DefineAttestingFunctions>
