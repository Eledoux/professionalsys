#<ImportModules>
import collections
import copy
import inspect
import matplotlib.pyplot
import numpy
import operator
import os
import ShareYourSystem as SYS
import sys
import tables
import unittest
#</ImportModules>

"""
 coucou erwan, ce message est d'une grande importance... 
 "mmmhhh!! noh oui oh oui!! oooohh!!!"" 
 c'est la reponse a ttes tes annees de recherches intensives en neuroscience!! 
 Tu peux me remercier!!!!
"""

#<DefineLocals>
AddShortString="+"
AttestString='attest'
AttributeShortString="."
DeepShortString="/"
AppendShortString="App_"
PointerShortString="Pointer"
CopyShortString='Cop_'
CollectShortString=":"
ExecShortString="Exec_"
TagString="_"
#</DefineLocals>

#Define for each a test method to be bounded
def setUp(_Test):
	_Test.TestedPointer.TestedInt+=1
	_Test.TestedPointer.TestedVariable=None
	_Test.TestedPointer.TestedString=""

def debug(_String):
	Frame=inspect.currentframe().f_back
	ScriptPathStringsList=SYS.getWordStringsListWithString(Frame.f_code.co_filename)
	ScriptPathString=''.join(ScriptPathStringsList)
	print("%s , %s, l. %s : "%(ScriptPathString,Frame.f_code.co_name,Frame.f_lineno));
	print(_String)
	print("")

#<DefineClass>
class ObjectClass(object):
	
	#<DefineMethods>

		#<HandlingMethods>
	
			#<InitMethod>
	def __init__(self):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""

		#<DefineSpecificDict>

			#<HandlingAttributes>

		#Attributes for the __init__ property
		self.IsInitiatingBool=True

		#Attributes for the __getitem__ property
		self.IsGettingBool=True
		self.GettingVariable=None
		self.GettedVariable=None

		#Attributes for the __setitem__ property
		self.IsSettingBool=True
		self.SettingVariable=None
		self.SettedVariable=None

		#Attributes for the call property
		self.IsCallingBool=False

		#Attributes for the __delitem__ property
		self.IsDeletingBool=True
		self.DeletingVariable=None
		
		#Attributes for the apply property
		self.IsApplyingBool=True
		self.MappedAppliedVariablesList=[]

		#Attributes for the walk property
		self.WalkingDict={}
		self.WalkedOrderedDict=None

		#Attributes for the grabb property
		self.GrabbedVariablesList=[]

		#Attributes for the order property
		self.MappedOrderedVariablesList=[]

			#</HandlingAttributes>

			#<TestingAttributes>
		self.TestingString=""
		self.TestingIsPrintedBool=True
		if hasattr(getattr(SYS,self.__module__),'AttestingFunctionStringsList'):
			self.TestingAttestFunctionStringsList=copy.copy(
				getattr(SYS,self.__module__).AttestingFunctionStringsList)
		else:
			self.TestingAttestFunctionStringsList=[]
		self.TestingFolderPathString=SYS.getCurrentFolderPathString()+'Tests/'
		self.TestedClass=None
		self.TestedOrderedDict=collections.OrderedDict()
		self.TestedInt=-1
		self.TestedVariable=None
		self.TestedString=""
			#</TestingAttributes>

			#<OrderingAttributes>

		#Attributes for the group property
		self.SortedOrderedDict=collections.OrderedDict()
		self.SortedKeyString=""
		self.SortedInt=-1

		#Attributes for the model property
		self.OldModeledInt=-1
		self.NewModeledInt=-1

		self.IsReInitiatingBool=True

		#Attributes for the store property
		self.StoringTuplesList=[]

		#Attributes for the plot property
		self.IsPlotingBool=True
		self.PlotingRowsInt=1
		self.PlotingColsInt=3
		self.PlotedFigure=None

			#</ParameterizingAttributes>

			#<PrintingAttributes>

		#Attributes for the __repr__ property
		self.IsRepresentingBool=True
		self.RepresentingDict={}
		self.RepresentedTuplesList=[]
		self.NotRepresentingGettingVariablesList=filter(
													lambda _KeyString:
													_KeyString not in [

																		#'WalkedOrderedDict',

																		#'SortedOrderedDict',

																		#'GroupedOrderedDict',
																		#'GroupedInt',
																		#'GroupedKeyString',
																		#'GroupedGrandParentPointersList',
																		#'GroupedKeyString',
																		#'GroupedGrandParentPointersList',
																		#'GroupedFilePointer',
																		#'GroupedPathString',

																		#'JoinedTuplesList',

																		#'GrabbedVariablesList',
																		#'FeaturedModelClass',
																		#'FeaturedTable'

																		#'StructuredOrderedDict',
																		#'StructuredInt',
																		#'StructuredKeyString',
																		#'StructuredGrandParentPointersList',
																		#'StructuredPathString'
										
																		
																	],
													self.__class__.SpecificKeyStringsList
										) if type(self)==SYS.ObjectClass else []

		self.RepresentingKeyVariablesList=[
											'WalkedOrderedDict',

											'SortedOrderedDict',

											'GroupedOrderedDict',
											'GroupedInt',
											'GroupedKeyString',
											'GroupedGrandParentPointersList',
											'GroupedKeyString',
											'GroupedGrandParentPointersList',
											'GroupedFilePointer',
											'GroupedPathString',

											'JoinedTuplesList',
											'GrabbedVariablesList',
											'FeaturedModelClass',
											#'FeaturedTable'

											'StructuredOrderedDict',
											'StructuredInt',
											'StructuredKeyString',
											'StructuredGrandParentPointersList',
											'StructuredPathString'

										] if type(self)!=SYS.ObjectClass else []
		self.RepresentingKeyVariablesList=[]

			#</PrintingAttributes>

		#</DefineSpecificDict>

		#Hook methods (integrativ)
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="init"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsInitiatingBool==False:
						return 

					#Call the HookingMethod
					HookingMethod(self)

			#</InitMethod>

			#<ExecuteMethod>

	def execute(self,_ExecString):

		exec _ExecString in locals()
		return self

			#</ExecuteMethod>

		#<TestingMethods>

			#<TestingAttestingMethods>

	def bindTestedVariableAfter(self):

		#Bind with TestedString setting
		SYS.PrintingDict['IsIdBool']=False
		self.TestedString=SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(self.TestedVariable,SYS.PrintingDict)
		SYS.PrintingDict['IsIdBool']=False

	def writeAttestWithTestingAttestFunction(self,_TestingAttestFunction):

		#Call the attest function to get the TestedVariable
		self['TestedVariable']=_TestingAttestFunction()

		#Write the TestedString
		File=open(self.TestingFolderPathString+_TestingAttestFunction.__name__+'.txt','w')
		File.write(self.TestedString)
		File.close()

	def attest(self):

		#Check that there is a Folder Tests
		if os.path.isdir(self.TestingFolderPathString)==False:
			os.popen('mkdir '+self.TestingFolderPathString)
		else:
			os.popen("cd "+self.TestingFolderPathString+";rm *")

		#Get the TestingAttestFunctionsList
		Module=getattr(SYS,self.__module__)
		TestingAttestFunctionsList=map(
										lambda TestingAttestFunctionString:
										getattr(Module,TestingAttestFunctionString),
										self.TestingAttestFunctionStringsList
									)

		#Write the TestedString made by each function and append an equivalent test method into the test ordered dict
		map(
				lambda __TestingAttestFunction:
				self.writeAttestWithTestingAttestFunction(__TestingAttestFunction),
				TestingAttestFunctionsList
			)

		#Return self
		return self

			#</TestingAttestingMethods>

			#<TestingMethods>

	def setTestFunctionWithTestingAttestFunction(self,_TestingAttestFunction):

		#Define the TestString
		TestString='at'.join(_TestingAttestFunction.__name__.split('at')[1:])

		def test(_Test):

			#Define the TestingAttestFunction
			TestingAttestFunction=getattr(
									getattr(SYS,_Test.TestedPointer.__module__),
									_Test.TestedPointer.TestingAttestFunctionStringsList[_Test.TestedPointer.TestedInt]
								)

			#Get the AssertedString
			File=open(_Test.TestedPointer.TestingFolderPathString+TestingAttestFunction.__name__+'.txt','r')
			AssertingString=File.read()
			File.close()

			#Call the attest function to get the TestedVariable
			_Test.TestedPointer['TestedVariable']=TestingAttestFunction()

			#Print maybe
			if _Test.TestedPointer.TestingIsPrintedBool:
				#print("\n###########################################")
				#print("")
				#print('AssertingString is :')
				#print(AssertingString)
				#print("")

				#Print maybe
				print('AssertedString is :')
				print(_Test.TestedPointer.TestedString)
				print("")

			#Assert
			#print("a",AssertingString)
			#print("b",_Test.TestedPointer.TestedString)

			_Test.assertEqual(
					#1,1
					AssertingString,
					_Test.TestedPointer.TestedString
			)

		#Copy a form of the test function and name it differently
		test.__name__=TestString

		#Append in the Test OrderedDict
		self['App_Test_'+test.__name__]=test

	def test(self):

		#Get the TestingAttestFunctionsList
		Module=getattr(SYS,self.__module__)
		TestingAttestFunctionsList=map(
										lambda TestingAttestFunctionString:
										getattr(Module,TestingAttestFunctionString),
										self.TestingAttestFunctionStringsList
									)

		#Set the tests for each asserting function
		map(
				lambda __TestingAttestFunction:
				self.setTestFunctionWithTestingAttestFunction(__TestingAttestFunction),
				TestingAttestFunctionsList
			)
			
		#Define the TestClass
		class TestClass(unittest.TestCase):				

			#Bind with the Tested object
			TestedPointer=self

			#Bound the setUp function
			locals().__setitem__(setUp.__name__,setUp)
			
			#Bound each testing function
			for TestedKeyString,TestedMethod in self.TestedOrderedDict.items():
				locals().__setitem__(TestedKeyString,TestedMethod)

			try:
				del TestedKeyString,TestedMethod
			except:
				pass

		#Give a name
		TestClass.__name__=SYS.getClassStringWithTypeString(self.__class__.TypeString+'Test')

		#Set to the TestedClass
		self.TestedClass=TestClass

		#Bound to the unittest runner
		TestLoader=unittest.TestLoader().loadTestsFromTestCase(self.TestedClass)
		unittest.TextTestRunner(verbosity=2).run(TestLoader)

		#</TestingMethods>

		#<CallMethod>

	def reinit(self,**_ReInitiatingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Refresh the attributes and set maybe default values in the _OutputingVariablesDict
		LocalReInitiatingVariablesDict=_ReInitiatingVariablesDict
		LocalReInitiatedPointer=self
		self.IsReInitiatingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='reinit'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalReInitiatingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalReInitiatingVariablesDict' in OutputVariable:
							LocalReInitiatingVariablesDict=OutputVariable['LocalReInitiatingVariablesDict']
						if 'LocalReInitiatedPointer' in OutputVariable:
							LocalReInitiatedPointer=OutputVariable['LocalReInitiatedPointer']

					#Check Bool
					if self.IsReInitiatingBool==False:
						return LocalReInitiatedPointer

		#Return the OutputVariable
		return LocalReInitiatedPointer

	def output(self,**_OutputingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Refresh the attributes and set maybe default values in the _OutputingVariablesDict
		map(
			lambda __DefaultTuple:
			_OutputingVariablesDict.__setitem__(__DefaultTuple[0],__DefaultTuple[1])
			if __DefaultTuple[0] not in _OutputingVariablesDict
			else None,
			[
				('IsRowingBool',False)
			]
		)
		LocalOutputingVariablesDict=_OutputingVariablesDict
		LocalOutputedPointer=self
		self.IsOutputingBool=True
		self.OutputedJoinedRowedIntsList=[]
		self.OldFeaturedModeledInt=-1
		self.NewFeaturedModeledInt=-1
		self.OldOutputedModeledInt=-1
		self.NewOutputedModeledInt=-1
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='output'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalOutputingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalOutputingVariablesDict' in OutputVariable:
							LocalOutputingVariablesDict=OutputVariable['LocalOutputingVariablesDict']
						if 'LocalOutputedPointer' in OutputVariable:
							LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

					#Check Bool
					if self.IsOutputingBool==False:
						return LocalOutputedPointer

		#Return the OutputVariable
		return LocalOutputedPointer

	def plot(self,**_PlotingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Refresh the attributes and set maybe default values in the _PlotingVariablesDict
		LocalPlotingVariablesDict=_PlotingVariablesDict
		LocalPlotedPointer=self
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='plot'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalPlotingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalPlotingVariablesDict' in OutputVariable:
							LocalPlotingVariablesDict=OutputVariable['LocalPlotingVariablesDict']
						if 'LocalPlotedPointer' in OutputVariable:
							LocalPlotedPointer=OutputVariable['LocalPlotedPointer']

					#Check Bool
					if self.IsPlotingBool==False:
						return LocalPlotedPointer

		#Return the OutputVariable
		return LocalPlotedPointer

	def plotBefore(self,**_LocalPlotingVariablesDict):


		#Kill other python processes
		try :
			map(
					lambda IdString:
					os.popen("kill "+IdString),
					SYS.getOpenedProcessIdStringsListWithProcessNameString("python")[:-1]
				)
		except:
			pass


		#Init a Figure
		self.PlotedFigure=matplotlib.pyplot.figure(figsize=(17,5))
		self.PlotedFigure
		self.PlotedInt=1

	def show(self):
		matplotlib.pyplot.show()

	def __call__(self,_CallVariable,*_CallingVariablesList,**_CallingVariablesDict):
		"""Call the <_CallString><HookString> methods and return self.CalledVariable (self by default)"""

		#Refresh the attributes
		LocalCallVariable=_CallVariable
		LocalCallingVariablesList=_CallingVariablesList
		LocalCallingVariablesDict=_CallingVariablesDict
		LocalCalledPointer=self

		#self.CalledPointer=self
		self.IsCallingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString=LocalCallVariable+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,LocalCallVariable,LocalCallingVariablesList,**LocalCallingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalCallVariable' in OutputVariable:
							LocalCallVariable=OutputVariable['LocalCallVariable']
						if 'LocalCallingVariablesList' in OutputVariable:
							LocalCallingVariable=OutputVariable['LocalCallingVariablesList']
						if 'LocalCallingVariablesDict' in OutputVariable:
							LocalCallingVariablesDict=OutputVariable['LocalCallingVariablesDict']
						if 'LocalCalledPointer' in OutputVariable:
							LocalCalledPointer=OutputVariable['LocalCalledPointer']

					#Check Bool
					if self.IsCallingBool==False:
						return LocalCalledPointer

		#Return the OutputVariable
		return LocalCalledPointer

			#</CallMethod>

			#<GetMethod>

	def __getitem__(self,_GettingVariable):
		"""Call the get<HookString> methods and return self.GettedVariable (None by default)"""
		
		#Refresh the attributes
		self.GettingVariable=_GettingVariable
		self.GettedVariable=None
		self.IsGettingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="get"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Debug
					#debug("HookingMethodString is : "+HookingMethodString)
					#debug("self.GettedVariable is : "+repr(self.GettedVariable))

					#Check Bool
					if self.IsGettingBool==False:
						return self.GettedVariable

		#Return GettedVariable
		return self.GettedVariable

		#</GetMethod>

		#<SetMethods>

			#<GlobalMethod>

	def __setitem__(self,_SettingVariable,_SettedVariable):
		"""Call the set<HookString> methods"""

		#Refresh the attributes
		self.SettingVariable=_SettingVariable
		self.SettedVariable=_SettedVariable
		self.IsSettingBool=True

		#Sort the HookMethodStrings
		for OrderString in ["Before","After"]:

			#Define a Local IsSettedBool
			IsSettedBool=False

			if type(_SettingVariable) in [str,unicode]:
			
				#Define the BindingMethodString
				BindingMethodString="bind"+_SettingVariable+OrderString
				
				#print('setitem',BindingMethodString,BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict)

				#Check that there is HookingMethods for it
				if BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict:
					
					#Say ok
					IsSettedBool=True

					#Call the specific Appended methods 
					for HookingMethod,BindingMethod in self.__class__.BindingMethodStringToMethodTuplesListDict[BindingMethodString]:

						#Call the HookMethod
						if callable(HookingMethod):
							HookingMethod(self)

						#print('hassetted',HookingMethod,BindingMethod)

						#Check Bool
						if self.IsSettingBool==False:
							return self

						#Call the Binding
						if callable(BindingMethod):
							OutputVariable=BindingMethod(self)

							#print('hasbinded')

							#End the Setting
							if self.IsSettingBool==False:
								return self

			#If a binding method was not yet found then look for a global set method
			if IsSettedBool==False:

				#_SettingVariable is not a string
				HookingMethodString="set"+OrderString

				#Check that there is HookingMethods for it
				if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

					#Call the specific Appended methods 
					for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

						#Call the HookingMethod
						HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

		#Return self
		return self

			#</GlobalMethod>

			#<AppendMethod>

	def append(self,_AppendingVariable):
		"""Call __setitem__ with a defined SettingVariable contained in the _AppendingVariable"""
		
		#Init the SettingVariable and Add the TaggingString
		SettingVariable=AppendShortString

		#TuplesList Case
		if SYS.getIsTuplesListBool(
			_AppendingVariable) and SYS.getCommonSuffixStringWithStringsList([_AppendingVariable[0][0],'KeyString'])=='KeyString':
				
				#print("j",
				#	SYS.getHookStringWithHookedString(_AppendingVariable[0][0].split('KeyString')[0]
				#	)+TagString+_AppendingVariable[0][1])
				#Add the hook version
				SettingVariable+=SYS.getHookStringWithHookedString(_AppendingVariable[0][0].split('KeyString')[0])+TagString+_AppendingVariable[0][1]

		else:

			#Object Case
			if SYS.ObjectClass in type(_AppendingVariable).__mro__ :

				Dict=_AppendingVariable.__dict__

			#dict Case
			elif type(_AppendingVariable)==dict:

				Dict=_AppendingVariable

			#Get the KeyStringsList
			GoodKeyTuplesList=filter(
						lambda __ItemTuple:
						SYS.getCommonSuffixStringWithStringsList([__ItemTuple[0],'KeyString'])=='KeyString' and __ItemTuple[1]!="",
						Dict.items()
					)

			#If there is a good KeyString
			if len(GoodKeyTuplesList)==1:

				#print("i",
				#	SYS.getHookStringWithHookedString(GoodKeyTuplesList[0][0].split('KeyString')[0]
				#		)+TagString+GoodKeyTuplesList[0][1])
				SettingVariable+=SYS.getHookStringWithHookedString(GoodKeyTuplesList[0][0].split('KeyString')[0])+TagString+GoodKeyTuplesList[0][1]

		#Then set
		if SettingVariable==AppendShortString:
			SettingVariable+='Sort'+TagString
		return self.__setitem__(SettingVariable,_AppendingVariable)

			#</AppendMethod>

		#</SetMethods>

		#<DelMethod>

	def __delitem__(self,_DeletingVariable):
		"""Call the del<HookString> methods and delete in the __dict__ by default"""
		
		#Refresh the attributes
		self.DeletingVariable=_DeletingVariable
		self.IsDeletingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="delete"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsDeletingBool==False:
						return self

		#Return self
		return self

		#</DelMethod>

	#</HandlingMethods>

	#<MappingMethods>

		#<GlobalMethod>

	def map(self,_MappingFunction,*_MappedList):
		"""Just map"""
		map(_MappingFunction,*_MappedList)
		return self
		#</GlobalMethod>

		#<ApplyingMethods>

			#<GenericMethod>

	def apply(self,_MethodString,_AppliedVariablesList):
		"""Map a call with the _MethodString to the _AppliedVariablesList"""

		#Get the AppliedMethod
		if hasattr(self,_MethodString):
			self.MappedAppliedVariablesList=map(
					lambda __AppliedVariable:
					SYS.getWithMethodAndArgsList(getattr(self,_MethodString),__AppliedVariable)
					if hasattr(self,_MethodString)
					else None,
					_AppliedVariablesList
				)

		#Return self
		return self

			#</GenericMethod>

			#<GetMethods>

				#<PickMethod>

	def pick(self,_GettingVariablesList):
		"""Apply the __getitem__ to the <_GettingVariablesList>"""

		#Apply __getitem__
		self.apply('__getitem__',_GettingVariablesList)

		#Return AppliedVariablesList
		return self.MappedAppliedVariablesList

				#</PickMethod>

				#<GatherMethod>

	def gather(self,_GatheringVariablesList):
		"""Reduce a map of pick for GatheringVariable being a List or __getitem__ for GatheringVariable being a GettingVariable"""
		return reduce(
						operator.__add__,
						map(
								lambda __GatheringVariable:
								zip(__GatheringVariable,self.pick(__GatheringVariable))
								if type(__GatheringVariable)==list
								else self[__GatheringVariable],
								_GatheringVariablesList
						)
					) if len(_GatheringVariablesList)>0 else []

				#<GatherMethod>

				#<CollectMethod>

	def collect(self,_CollectingGatheringVariablesList,_CollectingGatheredVariablesList):
		"""Reduce a map of pick for GatheringVariable being a List or __getitem__ for GatheringVariable being a GettingVariable"""
		
		#Get the GatheredVariablesList
		GatheredVariablesList=zip(*self.gather(_CollectingGatheringVariablesList))
		if len(GatheredVariablesList)>0:

			#Just keep the values
			GatheredVariablesList=GatheredVariablesList[1]

			#Reduce a gather for each
			return reduce(
							operator.__add__,
							map(
									lambda __CollectingObject:
									__CollectingObject.gather(_CollectingGatheredVariablesList),
									GatheredVariablesList
							)
						)

				#</CollectMethod>

			#</GetMethods>

			#<SetMethods>

				#<GlobalMethod>

	def update(self,_SettingVariableTuplesList):
		"""Apply the __setitem__ to the <_SettingVariableTuplesList>"""

		#Apply __setitem__
		return self.apply('__setitem__',_SettingVariableTuplesList)

				#</GlobalMethod>

				#<AppendMethod>

	def __add__(self,_AddingVariablesList):
		"""Apply the append to the <_AddingVariablesList>"""

		#Apply
		self.apply('append',_AddingVariablesList)

		#Return 
		return self

				#</AppendMethod>

			#</SetMethods>

		#</ApplyingMethods>

		#<CommandingMethods>

			#<AllSetsForEachMethod>

	def commandAllSetsForEach(self,_SettingVariableTuplesList,_GatheringVariablesList):
		"""Collect with _GatheringVariablesList and do a all sets for each with _SettingVariableTuplesList"""

		#Get the GatheredVariablesList
		GatheredVariablesList=self.gather(_GatheringVariablesList)
		if GatheredVariablesList!=None:

			#Unzip
			GatheredVariablesList=zip(*GatheredVariablesList)
			if len(GatheredVariablesList)>0:

				#Just keep the values
				GatheredVariablesList=GatheredVariablesList[1]

				#For each __GatheredVariable it is updating with _SettingVariableTuplesList
				map(
						lambda __GatheredVariable:
						__GatheredVariable.update(_SettingVariableTuplesList),
						GatheredVariablesList
					)

		#Return self
		return self 

			#</AllSetsForEachMethod>

			#<EachSetForAllMethod>

	def commandEachSetForAll(self,_SettingVariableTuplesList,_GatheringVariablesList):
		"""Collect with _GatheringVariablesList and do a each set for all with _SettingVariableTuplesList"""

		#Get the GatheredVariablesList
		GatheredVariablesList=zip(*self.gather(_GatheringVariablesList))
		if len(GatheredVariablesList)>0:

			#Just keep the values
			GatheredVariablesList=GatheredVariablesList[1]

			#For each SettingTuple it is setted in _GatheredVariablesList
			map(
					lambda __SettingVariableTuple:
					map(
						lambda __GatheredVariable:
						__GatheredVariable.__setitem__(*__SettingVariableTuple),
						GatheredVariablesList
						),
					_SettingVariableTuplesList
				)

		#Return self
		return self 

			#</EachSetForAllMethod>

		#</CommandingMethods>

		#<ParsingMethods>

			#<WalkMethod>

	def walk(self,_WalkingGatheringVariablesList,**_WalkingDict):
		
		#Init the TopWalkedOrderedDict
		TopWalkedOrderedDict=None
		if 'IdString' not in _WalkingDict:

			#Define the IdString of this walk
			IdString=str(id(_WalkingDict))

			#Set the _WalkingDict
			_WalkingDict.update(
									{
										'IdString':IdString,
										'TopPointer':self,
									}
								)

			#Define TopWalkedOrderedDictKeyString
			TopWalkedOrderedDictKeyString=IdString+'WalkedOrderedDict'

			#Set the corresponding WalkedOrderedDict
			self.__setattr__(
								TopWalkedOrderedDictKeyString,
								collections.OrderedDict(**
								{
									'WalkedInt':-1,
									'TopWalkedIntsList':['/'],
									'GrabbedVariablesOrderedDict':collections.OrderedDict(),
									'RoutedVariablesList':[],
									'TopPointersList':[self]
								})
							)

			#Alias this Dict
			TopWalkedOrderedDict=getattr(self,TopWalkedOrderedDictKeyString)

		else:

			#Get the information at the top
			TopWalkedOrderedDictKeyString=_WalkingDict['IdString']+'WalkedOrderedDict'
			TopWalkedOrderedDict=getattr(_WalkingDict['TopPointer'],TopWalkedOrderedDictKeyString)
			TopWalkedOrderedDict['WalkedInt']+=1
			TopWalkedOrderedDict['TopWalkedIntsList']+=[str(TopWalkedOrderedDict['WalkedInt'])]
			TopWalkedOrderedDict['TopPointersList']+=[self]

		#Refresh the attributes
		self.WalkingDict=_WalkingDict

		#Maybe Grab things...It is important to put his kind of hook before the recursive walk if we want to set from top to down
		if 'GrabbingVariablesList' in _WalkingDict:
			self.grab(_WalkingDict['GrabbingVariablesList'],TopWalkedOrderedDict)

		#Maybe Route
		if 'RoutingVariablesList' in _WalkingDict:
			self.route(_WalkingDict['RoutingVariablesList'],TopWalkedOrderedDict)

		#An Update just before is possible
		if 'BeforeUpdatingTuplesList' in _WalkingDict:
			self.update(_WalkingDict['BeforeUpdatingTuplesList'])

		#Command an recursive order in other gathered variables
		self.commandAllSetsForEach(
									[
										('walk',{
													'ArgsVariable':_WalkingGatheringVariablesList,
													'KwargsDict':_WalkingDict
													})
									],
									_WalkingGatheringVariablesList
								)

		#An Update just after is possible
		if 'AfterUpdatingTuplesList' in _WalkingDict:
			self.update(_WalkingDict['AfterUpdatingTuplesList'])

		#Retrieve the previous Path
		if len(TopWalkedOrderedDict['TopWalkedIntsList'])>0:
			TopWalkedOrderedDict['TopWalkedIntsList']=TopWalkedOrderedDict['TopWalkedIntsList'][:-1] 
			TopWalkedOrderedDict['TopPointersList']=TopWalkedOrderedDict['TopPointersList'][:-1]

		#Return self
		if _WalkingDict['TopPointer']==self:
			self.WalkedOrderedDict=TopWalkedOrderedDict
			del self[TopWalkedOrderedDictKeyString]
			return self

			#</WalkMethod>

			#<GrabMethod>

	def grab(self,_GrabbingVariablesList,_TopWalkedOrderedDict):

		SYS.setWithDictatedVariableAndKeyVariable(
									_TopWalkedOrderedDict['GrabbedVariablesOrderedDict'],
									_TopWalkedOrderedDict['TopWalkedIntsList'],
									collections.OrderedDict(
										self.gather(
											_GrabbingVariablesList
										) if len(_GrabbingVariablesList)>0 else {}
									)
								)
			#</GrabMethod>

			#<RouteMethod>


	def route(self,_RoutingVariablesList,_TopWalkedOrderedDict):

		RoutedVariablesList=None
		RoutedVariablesList=reduce(
				operator.__add__,
				map(
					lambda __TopPointer:
					zip(*__TopPointer.gather(_RoutingVariablesList))[1],
					_TopWalkedOrderedDict['TopPointersList']
					)
			)
		_TopWalkedOrderedDict['RoutedVariablesList'].append(RoutedVariablesList)

			#</RouteMethod>

		#</ParsingMethod>
		
	#</WalkingMethods>

	#</MappingMethods>
	
	#<OrganizingMethods>

		#<ParentizingMethod>

	def parentize(self,_ParentingString):

		#Define the HookedString
		HookedParentingString=SYS.getDoneStringWithDoString(_ParentingString)

		#Define the GroupedFilePointerKeyString
		GroupedFilePointerKeyString=HookedParentingString+"ParentPointer"
		if getattr(self,GroupedFilePointerKeyString)!=None:

			#Set the GrandParentPointersList
			GrandParentPointersListKeyString=HookedParentingString+"GrandParentPointersList"
			setattr(
						self,
						GrandParentPointersListKeyString,
						[getattr(self,GroupedFilePointerKeyString)]+getattr(
															getattr(self,GroupedFilePointerKeyString),
															GrandParentPointersListKeyString
															)
					)

			#Set the KeyStringsList
			KeyStringKeyString=HookedParentingString+"KeyString"
			KeyStringsListKeyString=HookedParentingString+"GrandParentKeyStringsList"
			self.__setattr__(
								KeyStringsListKeyString,
								map(
										lambda __GrandParentPointer:
										getattr(__GrandParentPointer,KeyStringKeyString),
										getattr(self,GrandParentPointersListKeyString)
									)
						)

			#Set the PathStringsList
			PathStringsListKeyString=HookedParentingString+"PathStringsList"
			self.__setattr__(
								PathStringsListKeyString,
								[getattr(self,KeyStringKeyString)]+copy.copy(
									getattr(self,KeyStringsListKeyString)
								)
							)
			getattr(self,PathStringsListKeyString).reverse()

			#Set the PathString
			PathStringKeyString=HookedParentingString+"PathString"
			self.__setattr__(
								PathStringKeyString,
								DeepShortString.join(getattr(self,PathStringsListKeyString))
							)

		#</ParentizingMethod>

		#<RowingMethods>

			

		#</RowingMethods>

		#<ScanningMethods>

	


	def scan(self,**_ScanningVariablesDict):

		'''
		print('')
		print('SCAN begin for ',self.GroupedKeyString)
		'''

		#Init maybe the _ScanningVariablesDict
		if _ScanningVariablesDict=={}:
			_ScanningVariablesDict['IsScanningBool']=False

		#Get the GroupedObjectsList
		GroupedObjectsList=self.GroupedOrderedDict.values()

		#If it is the last layer of group then flush directly all the featuring scanning values
		if len(GroupedObjectsList)==0:
			_ScanningVariablesDict['IsScanningBool']=True

		#Scan the children groups first 
		if _ScanningVariablesDict['IsScanningBool']==False:

			'''
			print('Scan',self.GroupedKeyString)
			print('We dont have the right to scan already, scan first the children...')
			print('')
			'''

			map(
					lambda __GroupedObject:
					__GroupedObject.scan(),
					GroupedObjectsList
				)

			'''
			print('Scan',self.GroupedKeyString)
			print('Children scanned ok')
			'''

		else:

			'''
			print('Scan',self.GroupedKeyString)
			print('We have the right to scan !')
			print('')
			'''

			#Get FeaturingColumnStringsList,FeaturingGettingStringsList
			[FeaturingColumnStringsList,FeaturingGettingStringsList]=SYS.unzip(self.FeaturingTuplesList,[0,1])

			'''
			print('Scan',self.GroupedKeyString)
			print('Make sure that we have joined ...?')
			print('')
			'''

			#Get the JoinedModeledIntsTuplesList
			self.join()
			JoinedStringsList=map(
									lambda __GroupedObject:
									SYS.getModeledIntColumnStringWithGroupedKeyString(
												__GroupedObject.GroupedKeyString
											),
									GroupedObjectsList
								)

			ScanningGettingStringsList=list(FeaturingGettingStringsList)+JoinedStringsList
			ScanningListsList=SYS.unzip(self.FeaturingTuplesList,[3])+self.JoinedModeledIntsTuplesList
			
			'''
			print('Scan',self.GroupedKeyString)
			print('and join is done fore here...')
			print('scan','self.JoinedModeledIntsTuplesList',self.JoinedModeledIntsTuplesList)
			print('scan','JoinedStringsList',JoinedStringsList)
			print('scan','FeaturingGettingStringsList',FeaturingGettingStringsList)
			print('scan','ScanningGettingStringsList',ScanningGettingStringsList)
			print('scan','ScanningListsList',ScanningListsList)
			print('')
			'''

			
			#For each cartesian product of the values update, then output, then store
			"""
			map(
					lambda __ScannedTuple:
					self.update(
									zip(
											ScanningGettingStringsList,
											__ScannedTuple
										)
								).output().store().reinit(),
					SYS.getScannedTuplesListWithScanningListsList(
						ScanningListsList
						)
				)
			"""

			#print('Scan begin loop for ',self.GroupedKeyString)
			#print('')

			for ScannedTuple in SYS.getScannedTuplesListWithScanningListsList(
									ScanningListsList
							):

				'''
				print('Scan, ScannedTuple for',self.GroupedKeyString)
				print("is",zip(
											ScanningGettingStringsList,
											ScannedTuple
										))
				print("")
				'''

				self.update(
								zip(
										ScanningGettingStringsList,
										ScannedTuple
									)
							).output().store().reinit()

		

		#Climb again the branchs to scan the parents
		if self.GroupedParentPointer!=None:

			#If it is the last child group, then order to the parent to scan
			if self.GroupedInt==len(self.GroupedParentPointer.GroupedOrderedDict)-1:

				#print('Scan climb again in the Parent (that was the last child)',
				#	self.GroupedKeyString)
				#print('')

				#Call the parent with the authorisation to scan
				self.GroupedParentPointer.scan(**{'IsScanningBool':True})

		#print('SCAN end for ',self.GroupedKeyString)
		#print('')

		#Return self
		return self
		#<ModelingMethods>
		
	def organize(self,**_OrganizingVariablesDict):
		"""Call the Organize<HookString> methods and return self.OrganizedPointer (self by default)"""

		#Refresh the attributes and set maybe default values in the _OutputingVariablesDict
		LocalOrganizingVariablesDict=_OrganizingVariablesDict
		LocalOrganizedPointer=self
		self.IsOrganizingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='organize'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalOrganizingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalOrganizingVariablesDict' in OutputVariable:
							LocalOrganizingVariablesDict=OutputVariable['LocalOrganizingVariablesDict']
						if 'LocalOrganizedPointer' in OutputVariable:
							LocalOrganizedPointer=OutputVariable['LocalOrganizedPointer']

					#Check Bool
					if self.IsOrganizingBool==False:
						return LocalOrganizedPointer

		#Return the OutputVariable
		return LocalOrganizedPointer

	def bindFeaturedModeledIntAfter(self):

		#Bind with OutputedModeledInt Setting
		if self.OutputedTable!=None:

			IntsList=self.OutputedTable.read_where(
				'FeaturedModeledInt == '+str(self.FeaturedModeledInt),field='ModeledInt'
			)

			'''
			print('bind for',self.GroupedKeyString)
			print('table is')
			for r in self.OutputedTable.iterrows():
				print "%-16s | %11.1f |" % \
				(r['ModeledInt'], r['FeaturedModeledInt'])
			print('self.FeaturedModeledInt is ',self.FeaturedModeledInt)
			print('IntsList is ',IntsList)
			print("")
			'''

			if len(IntsList)>0:

				#Get the first
				self.OutputedModeledInt=IntsList[0]

			else:

				self.OutputedModeledInt=-1
				self.FeaturedModeledInt=-1

			'''
			print('bind for',self.GroupedKeyString)
			print('self.OutputedModeledInt is ',self.OutputedModeledInt)
			print("")
			'''

			if self.OutputedModeledInt>-1:

				'''
				print('bind',self.GroupedKeyString)
				print('So this a query of changing the object state')
				print('self.OutputedModeledInt',self.OutputedModeledInt)
				print('first get the JoinedTuplesList from the OutputedTable to set the children groups')
				'''

				#Bind with the setting of all the Featured<GroupedKeyString>ModeledInt
				JoinedTuplesList=filter(
									lambda __ColumStringAndVariable:
									SYS.getCommonSuffixStringWithStringsList(
										[__ColumStringAndVariable[0],'ModeledInt']
										)=='FeaturedModeledInt' and __ColumStringAndVariable[0]!="FeaturedModeledInt",
									zip(
										self.OutputedTable.colnames,
										self.OutputedTable[self.OutputedModeledInt]
										)
									)

				'''
				print('bind',self.GroupedKeyString)
				print('So the queried JoinedTuplesList are ')
				print('JoinedTuplesList',JoinedTuplesList)
				print('Set the <GroupedKeyString>FeaturedModeledInt and inside of the child object for each')
				print('')
				'''

				map(
						lambda __JoinedTuple:
						self.__setitem__(*__JoinedTuple)['App_Group_'+__JoinedTuple[0].split('ModeledInt')[0]].__setitem__(
								'FeaturedModeledInt',
								__JoinedTuple[1]
							),
						JoinedTuplesList,
					)


				'''
				print('bind',self.GroupedKeyString)
				print('bind','also update the features and the output at this level') 
				print('New Features are ',
					zip(
										SYS.unzip(self.FeaturingTuplesList,[1]),
										map(
												lambda __ColumnString:
												self.FeaturedTable[self.FeaturedModeledInt][__ColumnString],
												SYS.unzip(self.FeaturingTuplesList,[0])
											)
									)
					)
				print('New Outputs are ',
					zip(
										SYS.unzip(self.OutputingTuplesList,[1]),
										map(
												lambda __ColumnString:
												self.OutputedTable[self.OutputedModeledInt][__ColumnString],
												SYS.unzip(self.OutputingTuplesList,[0])
											)
									)
				)
				print('')
				'''

				self.update(
								zip(
										SYS.unzip(self.FeaturingTuplesList,[1]),
										map(
												lambda __ColumnString:
												self.FeaturedTable[self.FeaturedModeledInt][__ColumnString],
												SYS.unzip(self.FeaturingTuplesList,[0])
											)
									)+
								zip(
										SYS.unzip(self.OutputingTuplesList,[1]),
										map(
												lambda __ColumnString:
												self.OutputedTable[self.OutputedModeledInt][__ColumnString],
												SYS.unzip(self.OutputingTuplesList,[0])
											)
									)
							)

				'''
				print('bind',self.GroupedKeyString)
				print('Refreshing with FeaturedModeledInt is done !')
				'''

		else:

			self.OutputedModeledInt=-1
			self.FeaturedModeledInt=-1

		#<GlobalMethod>


		#</GlobalMethod>

	#</OrganizingMethods>

	#</ParentizingMethod>

	#<PrintingMethods>

		#<ReprMethod>

	def __repr__(self):
		"""Call the repr<HookString> methods and return a SYS printed version of the self.PrintedDict"""

		#Refresh the attributes
		self.RepresentingDict=copy.deepcopy(SYS.PrintingDict)
		self.RepresentedTuplesList=self.__dict__.items()
		self.IsRepresentingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="repr"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsRepresentingBool==False:
						return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
								self,
								self.RepresentedDict
								)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
								dict(self.RepresentedTuplesList),
								self.RepresentingDict
								)

		#return the Print
		return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
				self,
				self.RepresentingDict
				)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
				dict(self.RepresentedTuplesList),
				self.RepresentingDict
				)

		#</ReprMethod>

	#</PrintingMethods>

	#</DefineMethods>

	#<DefineHookMethods>

	def getBefore(self):
		"""
			Hook in the __getitem__ of the Object 
			with different possible ShortStrings

			If _GettingVariable is an int, it returns the Value in the corresponding Index
			in self.AppendedValueVariablesList

			If _GettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			If _GettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			Else do a __getitem__ in the __dict__
		"""

		#Debug
		#debug("self.GettingVariable is : "+repr(self.GettingVariable))
		#debug("self.GettedVariable is : "+repr(self.GettedVariable))

		#Init a local IsGettedBool
		IsGettedBool=False

		#Case of a GettingString
		if type(self.GettingVariable) in [str,unicode]: 

			#Get with ExecShortString (it can be a return of a method call)
			if SYS.getCommonPrefixStringWithStringsList(
				[self.GettingVariable,ExecShortString])==ExecShortString:

				#Define the ExecString
				ExecString=self.GettingVariable.split(ExecShortString)[1]

				#Put the output in a local Local Variable
				exec ExecString in locals()

				#Give it to self.GettedVariable
				self.IsGettingBool=False
				return 

			#Get an Attribute (it can be a return of a method call)
			elif AttributeShortString in self.GettingVariable:

				#Define the GettingStringsList
				GettingStringsList=self.GettingVariable.split(AttributeShortString)

				#Look if the deeper getter is Ok
				GetterVariable=self[GettingStringsList[0]]

				#Define the next getting String
				NextGettingString=GettingStringsList[1]

				#If the GettingStringsList has brackets in the end it means a call of a method
				if "()"==NextGettingString[-2:]:
					NextGettingString=NextGettingString[:-2]

					#Case where it is an object
					if hasattr(GetterVariable,NextGettingString):
						self.GettedVariable=getattr(GetterVariable,NextGettingString)()
						self.IsGettingBool=False
						return 

					elif SYS.getIsTuplesListBool(GetterVariable):

						if NextGettingString=='keys':
							self.GettedVariable=map(
														lambda __ListedTuple:
														__ListedTuple[0],
														GetterVariable
													)
							self.IsGettingBool=False
							return 
						if NextGettingString=='values':
							self.GettedVariable=map(	
														lambda __ListedTuple:
														__ListedTuple[1],
														GetterVariable
													)
							self.IsGettingBool=False
							return 

				elif hasattr(GetterVariable,NextGettingString):

					#Else it is a get of an item in the __dict__
					GettingVariable=getattr(GetterVariable,NextGettingString)

					#Case where it has to continue to be getted
					if len(GettingStringsList)>2:
						GettingVariable[AttributeShortString.join(GettingStringsList[2:])]
						self.IsGettingBool=False
						return 
					else:

						#Else return the GettingVariable
						self.GettedVariable=GettingVariable
						self.IsGettingBool=False
						return 

			#Get with DeepShortString
			elif self.GettingVariable==DeepShortString:
				
				#If it is directly DeepShortString
				self.GettedVariable=self

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return 

			#Get in a ordered dict
			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,AppendShortString])==AppendShortString:

				#Define the appended TaggingKeyString
				AppendingString=self.GettingVariable.split(AppendShortString)[1]

				#Define the appended TagString and KeyString
				SplittedStringsList=AppendingString.split(TagString)
				TaggingString=SplittedStringsList[0]
				KeyString=TagString.join(SplittedStringsList[1:])

				#Get the corresponding ordered lists and dict
				HookedTagString=SYS.getDoneStringWithDoString(TaggingString)
				OrderedDict=getattr(self,HookedTagString+'OrderedDict')

				#Get with a digited KeyString case
				if KeyString.isdigit():

					#Define the GettingInt
					GettingInt=(int)(KeyString)

					#Check if the size is ok
					if GettingInt<len(OrderedDict):

						#Get the GettedVariable 
						self.GettedVariable=SYS.get(OrderedDict,'values',GettingInt)
						#Stop the getting
						self.IsGettingBool=False

						#Return
						return

				#Get in the ValueVariablesList
				elif KeyString in OrderedDict:
					
					#Get the GettedVariable
					self.GettedVariable=OrderedDict[KeyString]

					#Stop the getting
					self.IsGettingBool=False

					#Return 
					return
			
			#Get with '/'
			elif self.GettingVariable==DeepShortString:
				
				#If it is directly '/'
				self.GettedVariable=self

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return 

			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,DeepShortString])==DeepShortString:

				#Define the GettingVariableStringsList
				GettingVariableStringsList=DeepShortString.join(self.GettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
				GettedVariable=self[GettingVariableStringsList[0]]
				if GettedVariable!=None:

					if len(GettingVariableStringsList)==1:

						#Direct update in the Child or go deeper with the ChildPathString
						self.GettedVariable=GettedVariable

						#Stop the getting
						self.IsGettingBool=False
					
						#Return 
						return 

					else:

						#Define the ChildPathString
						ChildPathString=DeepShortString.join(GettingVariableStringsList[1:])	

						#Get in the first deeper Element with the ChildPathString
						self.GettedVariable=GettedVariable[ChildPathString]

						#Stop the getting
						self.IsGettingBool=False

						#Return
						return 

			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,CopyShortString])==CopyShortString:

				#deepcopy
				self.GettedVariable=copy.deepcopy(
						self[CopyShortString.join(self.GettingVariable.split(CopyShortString)[1:])]				
					)

				#Stop the getting
				self.IsGettingBool=False

				#Return 
				return 

			elif self.GettingVariable=='UtilitiesDict':
				self.GettedVariable=self.__dict__
				self.IsGettingBool=False
				return 
			elif self.GettingVariable=='MethodsDict':
				self.GettedVariable=SYS.getMethodsDictWithClass(self.__class__)
				self.IsGettingBool=False
				return  
			elif self.GettingVariable=="__class__":
				self.GettedVariable=self.__class__.__dict__
				self.IsGettingBool=False
				return

		'''
		#Case of an Object
		elif SYS.ObjectClass in type(self.GettingVariable).__mro__:

			#If the GettingVariable is an object already then return this
			self.GettedVariable=self.GettingVariable
			self.IsGettingBool=False
			return 
		'''

		#Do the minimal get in the Utilities
		if IsGettedBool==False:
		
			if type(self.GettingVariable) in [str,unicode]:

				#Get safely the Value
				if self.GettingVariable in self.__dict__:

					#__getitem__ in the __dict__
					self.GettedVariable=self.__dict__[self.GettingVariable]

					#Stop the getting
					self.IsGettingBool=False


	def setBefore(self):
		"""
			Hook in the __setitem__ of the Object 
			with different possible ShortStrings

			If _SettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the setting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is <camelCase> then it is a hook query...
			so it calls with the form self(<camelCase>,SettedVariable)

			If _SettingVariable is of the form <String1>Pointer and _SettedVariable of the form /<String2>
			it gets the self[<String2>] and do self[<String1>Pointer]=self[<String2>]

			Else do a __setitem__ in the __dict__ 
		"""

		#Debug
		#print('set',self.SettingVariable)

		#Case of a SettingString
		if type(self.SettingVariable) in [str,unicode]:

			#Adding set
			if SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,AddShortString])==AddShortString:

				#Define the added GettingString
				GettingString=self.SettingVariable.split(AddShortString)[1]

				#Get the added Variable
				Variable=self[GettingString]

				#Add
				if hasattr(Variable,'__add__'):
					Variable+=self.SettedVariable

				#Return
				return

			#Appending set
			elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,AppendShortString])==AppendShortString:

				#Define the appended TaggingKeyString
				TaggingKeyString=self.SettingVariable.split(AppendShortString)[1]

				#Define the appended TagString,KeyString,HookedTagString,OrderedDictKeyString
				SplittedStringsList=TaggingKeyString.split(TagString)
				TaggingString=SplittedStringsList[0]
				KeyString=TagString.join(SplittedStringsList[1:])
				DoneTagString=SYS.getDoneStringWithDoString(TaggingString)
				OrderedDictKeyString=DoneTagString+'OrderedDict'
				try:

					#Get the corresponding OrderedDict
					OrderedDict=getattr(self,OrderedDictKeyString)

				except AttributeError:

					#Set it for the first time this ordering container
					self.__setattr__(OrderedDictKeyString,collections.OrderedDict())
					OrderedDict=getattr(self,OrderedDictKeyString)

					#Int and Set Child attributes
					self.__setattr__(DoneTagString+'Int',-1)
					self.__setattr__(DoneTagString+'KeyString',"")
					self.__setattr__(DoneTagString+'ParentPointer',None)

					#Init GrandChild attributes
					self.__setattr__(DoneTagString+'PathString',"/")
					self.__setattr__(DoneTagString+'GrandParentPointersList',[])

				#Append (or set if it is already in)
				OrderedDict[KeyString]=self.SettedVariable

				#If it is an object
				if SYS.ObjectClass in type(self.SettedVariable).__mro__:

					#Int and Set Child attributes
					self.SettedVariable.__setattr__(DoneTagString+'Int',len(OrderedDict)-1)
					self.SettedVariable.__setattr__(DoneTagString+'KeyString',KeyString)
					self.SettedVariable.__setattr__(DoneTagString+'ParentPointer',self)

					#Init GrandChild attributes
					self.SettedVariable.__setattr__(DoneTagString+'PathString',"")
					self.SettedVariable.__setattr__(DoneTagString+'GrandParentPointersList',[])

				#Return 
				return

			#Deep set
			elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,DeepShortString])==DeepShortString:

				#Get the SettingVariableStringsList
				SettingVariableStringsList=DeepShortString.join(self.SettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
				GettedVariable=self[SettingVariableStringsList[0]]
				if GettedVariable!=None:
					
					#Debug
					#print('GettedVariable is',GettedVariable)

					#Define the ChildPathString
					ChildPathString='/'+DeepShortString.join(SettingVariableStringsList[1:])	

					#Debug
					#print('ChildPathString is',ChildPathString)

					#Direct update in the Child or go deeper with the ChildPathString
					if ChildPathString=="/": 

						#Case where it is an object to set inside
						if self.__class__ in type(GettedVariable).__mro__:

							#Modify directly the GettedVariable with self.SettedVariable
							GettedVariable.__setitem__(*self.SettedVariable)

						#Case where it is an object to append inside
						elif SYS.getCommonPrefixStringWithStringsList(
							[SettingVariableStringsList[0],AppendShortString]
							)==AppendShortString:
							
							#Define the appended TaggingKeyString
							TaggingKeyString=self.SettingVariable.split(AppendShortString)[1]

							#Define the appended TagString and KeyString
							SplittedStringsList=TaggingKeyString.split(TagString)
							TaggingString=SplittedStringsList[0]
							KeyString=TagString.join(SplittedStringsList[1:])

							#Get the corresponding ordered lists and dict
							HookedTagString=SYS.getDoneStringWithDoString(TaggingString)
							OrderedDict=getattr(self,HookedTagString+'OrderedDict')

							#Append without binding
							if KeyString in OrderedDict:
								OrderedDict[KeyString]=self.SettedVariable

						#Case where it is a set at the level of self of an already setted thing
						else:

							self[SettingVariableStringsList[0]]=self.SettedVariable
					else:

						#Set in the first deeper Element with the ChildPathString
						GettedVariable[ChildPathString]=self.SettedVariable

				#Case where it is a set at the level of self of new setted thing
				else:

					self[SettingVariableStringsList[0]]=self.SettedVariable

				#Return 
				return 

			#Call for a hook
			elif (self.SettingVariable[0].isalpha() or self.SettingVariable[:2]=="__") and  self.SettingVariable[0].lower()==self.SettingVariable[0]:

				#Get the Method
				if hasattr(self,self.SettingVariable):

					#Get the method
					SettingMethod=getattr(self,self.SettingVariable)

					#Adapt the shape of the args
					SYS.getWithMethodAndArgsList(
													SettingMethod,
													self.SettedVariable['ArgsVariable'] if 'ArgsVariable' in self.SettedVariable else [],
													**self.SettedVariable['KwargsDict'] if 'KwargsDict' in self.SettedVariable else {}
												)
				
				elif len(self.SettedVariable)==1:

					#Adapt format of the ArgsList
					self(self.SettingVariable,self.SettedVariable[0])
				else:
					self(self.SettingVariable,*self.SettedVariable)

				#Return
				self.IsSettingBool=False
				return

			elif type(self.SettedVariable) in [str,unicode]:

				if SYS.getCommonSuffixStringWithStringsList(
					[self.SettingVariable,PointerShortString])==PointerShortString:
					
					#The Value has to be a deep short string for getting the pointed variable
					if SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,DeepShortString])==DeepShortString:
						GettedVariable=self[self.SettedVariable]
						if GettedVariable!=None:
							#Link the both with a __setitem__
							self[self.SettingVariable]=GettedVariable

					#Return in the case of the Pointer and a string setted variable
					return

				#Get with ExecShortString (it can be a return of a method call)
				elif SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,ExecShortString])==ExecShortString:

					self[self.SettingVariable]=self[self.SettedVariable]
					self.IsSettingBool=False
					return

			#__setitem__ in the __dict__, this is an utility set
			self.__dict__[self.SettingVariable]=self.SettedVariable

	def deleteBefore(self):
		"""
			Hook in the __delitem__ of the Object
		"""

		#Do the minimal delitem
		if type(self.DeletingVariable) in [str,unicode]:

			#Del Safely the Value
			if self.DeletingVariable in self.__dict__:
				del self.__dict__[self.DeletingVariable]

	def reprAfter(self):
		"""
			Hook in the repr of the Object
			for printing in the Console attributes in a more 
			user-friendly manner
		"""

		#Get the MroAppendedStringsList
		MroAppendedStringsList=SYS.getMroKeyStringsListWithClass(self.__class__)

		#Remove the NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.NotRepresentingGettingVariablesList,
										self.RepresentedTuplesList
								)

		#First keeps only the Specific and New attributes
		self.RepresentedTuplesList=map(
									lambda _RepresentedTuple:
									("<Spe>"+_RepresentedTuple[0],_RepresentedTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] in self.__class__.SpecificKeyStringsList,
											self.RepresentedTuplesList
										)
								)+map(
									lambda _NewTuple:
									("<New>"+_NewTuple[0],_NewTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] not in MroAppendedStringsList,
											self.RepresentedTuplesList
										)
								)

		#Add some forced Values with RepresentingKeyVariables
		self.RepresentedTuplesList+=map(
											lambda _KeyVariable:
											("<NotSpe>"+str(_KeyVariable),self[_KeyVariable]),
											self.RepresentingKeyVariablesList
										)

		#Simplify the numpy variables repr
		self.RepresentedTuplesList=map(
										lambda _RepresentedTuple:
										_RepresentedTuple
										if type(_RepresentedTuple[1]) not in [numpy.ndarray] 
										else (
												_RepresentedTuple[0],
												"<numpy.ndarray shape "+str(numpy.shape(
													_RepresentedTuple[1]))+">"
												),
										self.RepresentedTuplesList
									)

	
	#</DefineHookMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.ObjectClass()

def attest_getInNotOrderedDict():

	#Define an Object
	Object=SYS.ObjectClass().update([
						('MyInt',0),
						('MyObject',SYS.ObjectClass().update(
							[
								('MyString',"hello"),
								('MyFloat',0.1)
							]
							)
						)
					]
				)
	#Map some gets
	TestedVariable=map(
							lambda _KeyVariable:
							Object[_KeyVariable],
							[
								#Get directly in the __dict__
								'MyInt',
								'MyObject',

								#Get with copying
								'Cop_MyObject',

								#Get with a DeepShortString
								'/MyObject/MyString',

								#Get by the Object directly
								#Object
							]
					)

	#Change in the objects for looking at the copy difference
	Object['/MyObject/MyFloat']=0.4444

	#Bind
	return TestedVariable

def attest_getInOrderedDict():

	#Define an Object
	Object=SYS.ObjectClass().update([
										('App_Group_MyAppendedObject',SYS.ObjectClass().update(
											[
												('MyOtherString',"bonjour"),
												('MyOtherFloat',0.2)
											]
											)
										),
										('App_Group_MySecondInt',3)
									])
	#Map some gets
	TestedVariable=map(
								lambda _KeyVariable:
								Object[_KeyVariable],
								[
									#Get with a AppendShortString
									'App_Group_MySecondInt',
									'App_Group_MyAppendedObject',
									'Cop_App_Group_MyAppendedObject',
									'/App_Group_MyAppendedObject/MyOtherString',
									#Or the AppendedInt
									'App_Group_0',

									#Get in the class object
									'/__class__/TypeString'

								]
							)

	#Change in the objects for looking at the copy difference
	Object['/App_Group_MyAppendedObject/MyOtherFloat']=0.5555

	#Return 
	return TestedVariable

def attest_getWithAttributeString():

	#Define an ObjectGetterVariable
	Object=SYS.ObjectClass().update([
										('MyDict',{'MyInt':0}),
										('MyObject',SYS.ObjectClass().__setitem__('MySecondInt',2))
									]
								)

	#Get with AttributeShortString
	return [
							Object['MyDict.values()'],
							Object['MyObject.MySecondInt']
						]

def attest_setInNotOrderedDict():

	#Explicit expression
	Object=SYS.ObjectClass().__setitem__('MyInt',0)
	
	#Set with a deep short string
	Object.__setitem__('MyObject',SYS.ObjectClass()).__setitem__('/MyObject/MyString','hello')

	#Set with a deep deep short string
	Object.__setitem__('/MyObject/MyGrandOtherObject',SYS.ObjectClass())

	#Set with a pointer short string
	return Object.__setitem__('MyObjectPointer','/MyObject')

def attest_setInOrderedDict():

	#Short expression and set in the appended manner
	Object=SYS.ObjectClass().__setitem__('App_Sort_MySecondInt',1)

	#Short expression for setting in the append without binding
	Object['/App_Sort_MySecondInt']+=1

	#Short expression for setting in the appended manner a structured object
	Object['App_Group_MyAppendedObject']=SYS.ObjectClass()
	
	#Set with a deep short string and append string
	Object['/App_Group_MyAppendedObject/MyString']="bonjour"

	#Set with a deep deep short string and append string
	Object['/App_Group_MyAppendedObject/App_Group_MyGrandChildObject']=SYS.ObjectClass()

	#Return Object
	return Object

def attest_setWithMethodString():

	#Set for being a call of method
	return SYS.ObjectClass().__setitem__('__setitem__',{'ArgsVariable':('MySecondInt',1)})

def attest_hook():

	#Define a derived class and set it with SYS
	class BindingObjectClass(SYS.ObjectClass):

		#<DefineHookMethods>
		def setBefore(self):

			#Convert like a deserialization
			if SYS.getCommonSuffixStringWithStringsList([
				self.SettingVariable,'String'
				])=='String':
				self.SettingVariable=self.SettingVariable.split('String')[0]+'Int'
				self.SettedVariable=(int)(self.SettedVariable)
				self[self.SettingVariable]=self.SettedVariable
				self.IsSettingBool=False

		#</DefineHookgMethods>

		#<DefineBindingMethods>
		def bindMyIncrementedIntAfter(self):
			self.MyIncrementedInt+=1
		def bindApp_Sort_MyIncrementedIntAfter(self):
			self['/App_Sort_MyIncrementedInt']+=1
		#</DefineBindingMethods>

	SYS.setClassWithClass(BindingObjectClass)

	#Set to call the bind
	return BindingObjectClass().__setitem__(
									'MyIncrementedString',
									"0"
								).__setitem__(
									'App_Sort_MyIncrementedString',
									"2"
								)

def attest_apply():

	#Apply a __setitem__ to several things 
	Object=SYS.ObjectClass().apply('__setitem__',[
													('MyString',"Hello"),
													('MySecondString',"Bonjour")
												]
									)

	#Apply an apply is possible 
	return Object.apply(
							'__setitem__',
							[
											('MyThirdString',"GutenTag"),
											('apply',{'ArgsVariable':(
															'__setitem__',[
																		('MyInt',0)
																		]
																)
													}
											),
											('MyNotLostString',"ben he"),
							]
				)

def attest_pick():

	#Define an Object
	Object=SYS.ObjectClass().update([
										('MyInt',0),
										('MyObject',SYS.ObjectClass().update(
											[
												('MyString',"hello"),
												('MyFloat',0.1)
											]
											)
										),
										('App_Group_MyAppendedObject',SYS.ObjectClass().update(
											[
												('MyOtherString',"bonjour"),
												('MyOtherFloat',0.2)
											]
											)
										),
										('App_Sort_MySecondInt',3)
									])
	#Map some gets
	TestedVariable=Object.pick(
								[
									#Get directly in the __dict__
									'MyInt',
									'MyObject',

									#Get with copying
									'Cop_MyObject',

									#Get with a DeepShortString
									'/MyObject/MyString',

									#Get with a AppendShortString
									'App_Sort_MySecondInt',
									'App_Group_MyAppendedObject',
									'Cop_App_Group_MyAppendedObject',
									'/App_Group_MyAppendedObject/MyOtherString',
									#Or the AppendedInt
									'App_Group_0',

									#Get in the class object
									'/__class__/TypeString'
								]
							)

	#Change in the objects for looking at the copy difference
	Object['/MyObject/MyFloat']=0.4444
	Object['/App_Group_MyAppendedObject/MyOtherFloat']=0.5555

	#Return
	return TestedVariable

def attest_gather():

	return SYS.ObjectClass().__add__(
			[
				SYS.ObjectClass().update(
					[
						('StructuredKeyString',str(Int1))
					]).__add__(
						[
							SYS.ObjectClass().update(
										[
											('StructuredKeyString',str(Int2))
										]
									)
							for Int2 in xrange(2)
						]
					) for Int1 in xrange(2)
			]).update(
			[
				('App_Group_'+str(Int1),SYS.ObjectClass().update(
					[
						('GroupedKeyString',str(Int1))
					]).update(
						[
							('App_Group_'+str(Int2),SYS.ObjectClass().update(
										[
											('GroupedKeyString',str(Int2))
										]
									)
							)
							for Int2 in xrange(2)
						]
					)
				) for Int1 in xrange(2)
			]).gather([
							['/'],
							'StructuredOrderedDict.items()',
							'GroupedOrderedDict.items()'
						])

def attest_collect():

	Object=SYS.ObjectClass().__add__(
			[
				SYS.ObjectClass().update(
					[
						('StructuredKeyString',str(Int1))
					]).__add__(
						[
							SYS.ObjectClass().update(
										[
											('StructuredKeyString',str(Int2))
										]
									)
							for Int2 in xrange(2)
						]
					) for Int1 in xrange(2)
			])

	return Object.collect([
							['/'],
							'StructuredOrderedDict.items()',
							['App_Structure_1'],
							#[Object['App_Structure_0']]
						],
						[
							['StructuredKeyString']
						]
					)

def attest_update():

	#Update several things
	Object=SYS.ObjectClass().update([
										('MyInt',0),
										('App_Sort_MySecondInt',0),
										('MyFloat',0.2)
									]
								)

	#Update in a update is possible
	return Object.update([
										('MyString',"Hello"),
										('update',{'ArgsVariable':[
													('MySecondInt',2)
													]}
										),
										('MyNotLostString',"ben heee")
									]
								)

def attest_append():

	#Append with a TuplesList
	Object=SYS.ObjectClass().append([
										('StructuredKeyString',"MyTuplesList"),
										('MyString',"Hello")
									]
									)

	#Append with a dict
	Object.append({
						'StructuredKeyString':"MyDict",
						'MyOtherString':"Bonjour"
						}
					)

	#Append with an Object
	return Object.append(SYS.ObjectClass())

def attest_add():

	#Explicit expression
	return SYS.ObjectClass().__add__([
												[
													('StructuredKeyString',"MyTuplesList"),
													('MyString',"Hello")
												],
												{
													'StructuredKeyString':"MyDict",
													'MyOtherString':"Bonjour"
												},
												SYS.ObjectClass()
											])

def attest_commandAllSetsForEach():

	return SYS.ObjectClass().__add__(
			[
				SYS.ObjectClass().update(
					[
						('StructuredKeyString',str(Int1))
					]) for Int1 in xrange(2)
			]
		).execute('self.__class__.CountingInt=0').commandAllSetsForEach(
				[
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.GettedVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					),
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.GettedVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					)
				],
				[
					['/'],
					'StructuredOrderedDict.items()'
				])

def attest_commandEachSetForAll():
	
	return SYS.ObjectClass().__add__(
			[
				SYS.ObjectClass().update(
					[
						('StructuredKeyString',str(Int1))
					]) for Int1 in xrange(2)
			]
		).execute('self.__class__.CountingInt=0').commandEachSetForAll(
				[
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.GettedVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					),
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.GettedVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					)
				],
				[
					['/'],
					'StructuredOrderedDict.items()'
				])

def attest_walk():
	
	return SYS.ObjectClass().__add__(
			[
				SYS.ObjectClass().update(
					[
						('StructuredKeyString',str(Int1))
					]).__add__(
						[
							SYS.ObjectClass().update(
										[
											('StructuredKeyString',str(Int2))
										]
									)
							for Int2 in xrange(2)
						]
					) for Int1 in xrange(2)
			]
		).walk(
					[
						'StructuredOrderedDict.items()'
					],
					**{
							'BeforeUpdatingTuplesList':[
									('MyCoOrderedInt',2)
								],
							'GrabbingVariablesList':
							[
								['StructuredKeyString']
							],
							'RoutingVariablesList':
							[
								['StructuredKeyString']
							]
						}
				)
	
def attest_parentize():

	#Build a parentizing groups architecture
	return SYS.ObjectClass().update(
								[
									('GroupingPathString',SYS.getCurrentFolderPathString()+'MyObject.hdf5'),
									(
										'App_Group_ChildObject1',
										SYS.ObjectClass().update(
										[
											('App_Group_GrandChildObject1',
											SYS.ObjectClass())
										])
									),
									(
										'App_Group_ChildObject2',
										SYS.ObjectClass().update(
											[]
										)
									)
								]	
							).walk(
										[
											'GroupedOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Group"})
												]
											}
									)

def attest_group():

	#Build Hdf groups
	Object=SYS.ObjectClass().update(
								[
									('GroupingPathString',SYS.getCurrentFolderPathString()+'MyObject.hdf5'),
									(
										'App_Group_ChildObject1',
										SYS.ObjectClass().update(
										[
											('App_Group_GrandChildObject1',
											SYS.ObjectClass())
										])
									),
									(
										'App_Group_ChildObject2',
										SYS.ObjectClass().update(
											[]
										)
									)
								]	
							).walk(
										[
											'GroupedOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Group"}),
													('group',{})
												]
											}
									).close()

	#Get the h5ls version of the stored hdf
	return os.popen('h5ls -dlr '+Object.GroupingPathString).read()
#</DefineAttestingFunctions>
