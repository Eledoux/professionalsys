#<ImportModules>
import collections
import copy
import inspect
import numpy
import operator
import os
import ShareYourSystem as SYS
import sys
from tables import *
#</ImportModules>

"""
 coucou erwan, ce message est d'une grande importance... 
 "mmmhhh!! noh oui oh oui!! oooohh!!!"" 
 c'est la reponse a ttes tes annees de recherches intensives en neuroscience!! 
 Tu peux me remercier!!!!
"""

#<DefineLocals>
AddShortString="+"
AttributeShortString="."
DeepShortString="/"
AppendShortString="App_"
PointerShortString="Pointer"
CopyShortString='Cop_'
CollectShortString=":"
ExecShortString="Exec_"
TagString="_"
#</DefineLocals>

def debug(_String):
	Frame=inspect.currentframe().f_back
	ScriptPathStringsList=SYS.getWordStringsListWithString(Frame.f_code.co_filename)
	ScriptPathString=''.join(ScriptPathStringsList)
	print("%s , %s, l. %s : "%(ScriptPathString,Frame.f_code.co_name,Frame.f_lineno));
	print(_String)
	print("")

#<DefineClass>
class ObjectClass(object):
	
	#<DefineMethods>

		#<HandlingMethods>
	
			#<InitMethod>
	def __init__(self):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""

		#<DefineSpecificDict>

			#<HandlingAttributes>

		#Attributes for the __init__ property
		self.IsInitiatingBool=True

		#Attributes for the __getitem__ property
		self.IsGettingBool=True
		self.GettingVariable=None
		self.GettedVariable=None

		#Attributes for the __setitem__ property
		self.IsSettingBool=True
		self.SettingVariable=None
		self.SettedVariable=None

		#Attributes for the call property
		self.IsCallingBool=False

		#Attributes for the __delitem__ property
		self.IsDeletingBool=True
		self.DeletingVariable=None
		
		#Attributes for the apply property
		self.IsApplyingBool=True
		self.MappedAppliedVariablesList=[]

		#Attributes for the walk property
		self.WalkingDict={}
		self.WalkedOrderedDict=None

		#Attributes for the grabb property
		self.GrabbedVariablesList=[]

		#Attributes for the order property
		self.MappedOrderedVariablesList=[]

			#</HandlingAttributes>

			#<OrderingAttributes>

		#Attributes for the group property
		self.SortedOrderedDict=collections.OrderedDict()
		self.SortedKeyString=""
		self.SortedInt=-1

		#Attributes for the group property
		self.GroupingFilePathString=""
		self.GroupingModuleString="tables"
		self.GroupingFilePathString=""
		self.GroupedFilePointer=None
		self.GroupedOrderedDict=collections.OrderedDict()
		self.GroupedKeyString=""
		self.GroupedInt=-1
		self.GroupedParentPointer=None
		self.GroupedGrandParentPointersList=[]
		self.GroupedPathStringsList=[]
		self.GroupedPathString="/"

		#Attributes for the feature property
		self.FeaturingTuplesList=[]
		self.FeaturedModelClass=None
		self.FeaturedTable=None

		#Attributes for the join property
		self.JoinedTablePointersList=[]
		self.JoinedTuplesList=[]

		#Attributes for the output property
		self.OutputingTuplesList=[]
		self.OutputedModelTableClass=None
		self.OutputedTable=None

		#Attributes for the store property
		self.StoringTuplesList=[]

			#</ParameterizingAttributes>

			#<NetworkingAttributes>

		#Attributes for the structure property
		self.StructuredOrderedDict=collections.OrderedDict()
		self.StructuredKeyString=""
		self.StructuredInt=-1
		self.StructuredParentPointer=None
		self.StructuredGrandParentPointersList=[]

			#</NetworkingAttributes>

			#<PrintingAttributes>

		#Attributes for the __repr__ property
		self.IsRepresentingBool=True
		self.RepresentingDict={}
		self.RepresentedTuplesList=[]
		self.NotRepresentingGettingVariablesList=filter(
													lambda _KeyString:
													_KeyString not in [

																		'WalkedOrderedDict',

																		'SortedOrderedDict',

																		'GroupedOrderedDict',
																		'GroupedInt',
																		'GroupedKeyString',
																		'GroupedGrandParentPointersList',
																		'GroupedKeyString',
																		'GroupedGrandParentPointersList',
																		'GroupedFilePointer',
																		'GroupedPathString',

																		'JoinedTuplesList',

																		'GrabbedVariablesList',
																		'FeaturedModelClass',
																		#'FeaturedTable'

																		'StructuredOrderedDict',
																		'StructuredInt',
																		'StructuredKeyString',
																		'StructuredGrandParentPointersList',
																		'StructuredPathString'

																		
																	],
													self.__class__.SpecificKeyStringsList
										) if type(self)==SYS.ObjectClass else []

		self.RepresentingKeyVariablesList=[
											'WalkedOrderedDict',

											'SortedOrderedDict',

											'GroupedOrderedDict',
											'GroupedInt',
											'GroupedKeyString',
											'GroupedGrandParentPointersList',
											'GroupedKeyString',
											'GroupedGrandParentPointersList',
											'GroupedFilePointer',
											'GroupedPathString',

											'JoinedTuplesList',
											'GrabbedVariablesList',
											'FeaturedModelClass',
											#'FeaturedTable'

											'StructuredOrderedDict',
											'StructuredInt',
											'StructuredKeyString',
											'StructuredGrandParentPointersList',
											'StructuredPathString'

										] if type(self)!=SYS.ObjectClass else []

			#</PrintingAttributes>

		#</DefineSpecificDict>

		#Hook methods (integrativ)
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="init"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsInitiatingBool==False:
						return 

					#Call the HookingMethod
					HookingMethod(self)

			#</InitMethod>

			#<ExecuteMethod>

	def execute(self,_ExecString):

		exec _ExecString in locals()
		return self

			#</ExecuteMethod>

			#<CallMethod>

	def __call__(self,_CallVariable,*_CallingVariablesList,**_CallingVariablesDict):
		"""Call the <_CallString><HookString> methods and return self.CalledVariable (self by default)"""

		#Refresh the attributes
		LocalCallVariable=_CallVariable
		LocalCallingVariablesList=_CallingVariablesList
		LocalCallingVariablesDict=_CallingVariablesDict
		LocalCalledPointer=self

		#self.CalledPointer=self
		self.IsCallingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString=LocalCallVariable+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,LocalCallVariable,LocalCallingVariablesList,**LocalCallingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalCallVariable' in OutputVariable:
							LocalCallVariable=OutputVariable['LocalCallVariable']
						if 'LocalCallingVariablesList' in OutputVariable:
							LocalCallingVariable=OutputVariable['LocalCallingVariablesList']
						if 'LocalCallingVariablesDict' in OutputVariable:
							LocalCallingVariablesDict=OutputVariable['LocalCallingVariablesDict']
						if 'LocalCalledPointer' in OutputVariable:
							LocalCalledPointer=OutputVariable['LocalCalledPointer']

					#Check Bool
					if self.IsCallingBool==False:
						return LocalCalledPointer

		#Return the OutputVariable
		return LocalCalledPointer

			#</CallMethod>

			#<GetMethod>

	def __getitem__(self,_GettingVariable):
		"""Call the get<HookString> methods and return self.GettedVariable (None by default)"""
		
		#Refresh the attributes
		self.GettingVariable=_GettingVariable
		self.GettedVariable=None
		self.IsGettingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="get"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Debug
					#debug("HookingMethodString is : "+HookingMethodString)
					#debug("self.GettedVariable is : "+repr(self.GettedVariable))

					#Check Bool
					if self.IsGettingBool==False:
						return self.GettedVariable

		#Return GettedVariable
		return self.GettedVariable

		#</GetMethod>

		#<SetMethods>

			#<GlobalMethod>

	def __setitem__(self,_SettingVariable,_SettedVariable):
		"""Call the set<HookString> methods"""

		#Refresh the attributes
		self.SettingVariable=_SettingVariable
		self.SettedVariable=_SettedVariable
		self.IsSettingBool=True

		#Sort the HookMethodStrings
		for OrderString in ["Before","After"]:

			#Define a Local IsSettedBool
			IsSettedBool=False

			if type(_SettingVariable) in [str,unicode]:
			
				#Define the BindingMethodString
				BindingMethodString="bind"+_SettingVariable+OrderString
				
				#Check that there is HookingMethods for it
				if BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict:
					
					#Say ok
					IsSettedBool=True

					#Call the specific Appended methods 
					for HookingMethod,BindingMethod in self.__class__.BindingMethodStringToMethodTuplesListDict[BindingMethodString]:

						#Call the HookMethod
						if callable(HookingMethod):
							HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

						#Call the Binding
						if callable(BindingMethod):
							OutputVariable=BindingMethod(self)

							#End the Setting
							if self.IsSettingBool==False:
								return self

			#If a binding method was not yet found then look for a global set method
			if IsSettedBool==False:

				#_SettingVariable is not a string
				HookingMethodString="set"+OrderString

				#Check that there is HookingMethods for it
				if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

					#Call the specific Appended methods 
					for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

						#Call the HookingMethod
						HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

		#Return self
		return self

			#</GlobalMethod>

			#<AppendMethod>

	def append(self,_AppendingVariable):
		"""Call __setitem__ with a defined SettingVariable contained in the _AppendingVariable"""
		
		#Init the SettingVariable and Add the TaggingString
		SettingVariable=AppendShortString

		#TuplesList Case
		if SYS.getIsTuplesListBool(
			_AppendingVariable) and SYS.getCommonSuffixStringWithStringsList([_AppendingVariable[0][0],'KeyString'])=='KeyString':
				
				#print("j",
				#	SYS.getHookStringWithHookedString(_AppendingVariable[0][0].split('KeyString')[0]
				#	)+TagString+_AppendingVariable[0][1])
				#Add the hook version
				SettingVariable+=SYS.getHookStringWithHookedString(_AppendingVariable[0][0].split('KeyString')[0])+TagString+_AppendingVariable[0][1]

		else:

			#Object Case
			if SYS.ObjectClass in type(_AppendingVariable).__mro__ :

				Dict=_AppendingVariable.__dict__

			#dict Case
			elif type(_AppendingVariable)==dict:

				Dict=_AppendingVariable

			#Get the KeyStringsList
			GoodKeyTuplesList=filter(
						lambda __ItemTuple:
						SYS.getCommonSuffixStringWithStringsList([__ItemTuple[0],'KeyString'])=='KeyString' and __ItemTuple[1]!="",
						Dict.items()
					)

			#If there is a good KeyString
			if len(GoodKeyTuplesList)==1:

				#print("i",
				#	SYS.getHookStringWithHookedString(GoodKeyTuplesList[0][0].split('KeyString')[0]
				#		)+TagString+GoodKeyTuplesList[0][1])
				SettingVariable+=SYS.getHookStringWithHookedString(GoodKeyTuplesList[0][0].split('KeyString')[0])+TagString+GoodKeyTuplesList[0][1]

		#Then set
		if SettingVariable==AppendShortString:
			SettingVariable+='Sort'+TagString
		return self.__setitem__(SettingVariable,_AppendingVariable)

			#</AppendMethod>

		#</SetMethods>

		#<DelMethod>

	def __delitem__(self,_DeletingVariable):
		"""Call the del<HookString> methods and delete in the __dict__ by default"""
		
		#Refresh the attributes
		self.DeletingVariable=_DeletingVariable
		self.IsDeletingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="delete"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsDeletingBool==False:
						return self

		#Return self
		return self

		#</DelMethod>

	#</HandlingMethods>

	#<MappingMethods>

		#<GlobalMethod>

	def map(self,_MappingFunction,*_MappedList):
		"""Just map"""
		map(_MappingFunction,*_MappedList)
		return self
		#</GlobalMethod>

		#<ApplyingMethods>

			#<GenericMethod>

	def apply(self,_MethodString,_AppliedVariablesList):
		"""Map a call with the _MethodString to the _AppliedVariablesList"""

		#Get the AppliedMethod
		if hasattr(self,_MethodString):
			self.MappedAppliedVariablesList=map(
					lambda __AppliedVariable:
					SYS.getWithMethodAndArgsList(getattr(self,_MethodString),__AppliedVariable)
					if hasattr(self,_MethodString)
					else None,
					_AppliedVariablesList
				)

		#Return self
		return self

			#</GenericMethod>

			#<GetMethods>

				#<PickMethod>

	def pick(self,_GettingVariablesList):
		"""Apply the __getitem__ to the <_GettingVariablesList>"""

		#Apply __getitem__
		self.apply('__getitem__',_GettingVariablesList)

		#Return AppliedVariablesList
		return self.MappedAppliedVariablesList

				#</PickMethod>

				#<GatherMethod>

	def gather(self,_GatheringVariablesList):
		"""Reduce a map of pick for GatheringVariable being a List or __getitem__ for GatheringVariable being a GettingVariable"""
		return reduce(
						operator.__add__,
						map(
								lambda __GatheringVariable:
								zip(__GatheringVariable,self.pick(__GatheringVariable))
								if type(__GatheringVariable)==list
								else self[__GatheringVariable],
								_GatheringVariablesList
						)
					) if len(_GatheringVariablesList)>0 else []

				#<GatherMethod>

				#<CollectMethod>

	def collect(self,_CollectingGatheringVariablesList,_CollectingGatheredVariablesList):
		"""Reduce a map of pick for GatheringVariable being a List or __getitem__ for GatheringVariable being a GettingVariable"""
		
		#Get the GatheredVariablesList
		GatheredVariablesList=zip(*self.gather(_CollectingGatheringVariablesList))
		if len(GatheredVariablesList)>0:

			#Just keep the values
			GatheredVariablesList=GatheredVariablesList[1]

			#Reduce a gather for each
			return reduce(
							operator.__add__,
							map(
									lambda __CollectingObject:
									__CollectingObject.gather(_CollectingGatheredVariablesList),
									GatheredVariablesList
							)
						)

				#</CollectMethod>

			#</GetMethods>

			#<SetMethods>

				#<GlobalMethod>

	def update(self,_SettingVariableTuplesList):
		"""Apply the __setitem__ to the <_SettingVariableTuplesList>"""

		#Apply __setitem__
		return self.apply('__setitem__',_SettingVariableTuplesList)

				#</GlobalMethod>

				#<AppendMethod>

	def __add__(self,_AddingVariablesList):
		"""Apply the append to the <_AddingVariablesList>"""

		#Apply
		self.apply('append',_AddingVariablesList)

		#Return 
		return self

				#</AppendMethod>

			#</SetMethods>

		#</ApplyingMethods>

		#<CommandingMethods>

			#<AllSetsForEachMethod>

	def commandAllSetsForEach(self,_SettingVariableTuplesList,_GatheringVariablesList):
		"""Collect with _GatheringVariablesList and do a all sets for each with _SettingVariableTuplesList"""

		#Get the GatheredVariablesList
		GatheredVariablesList=zip(*self.gather(_GatheringVariablesList))
		if len(GatheredVariablesList)>0:

			#Just keep the values
			GatheredVariablesList=GatheredVariablesList[1]

			#For each __GatheredVariable it is updating with _SettingVariableTuplesList
			map(
					lambda __GatheredVariable:
					__GatheredVariable.update(_SettingVariableTuplesList),
					GatheredVariablesList
				)

		#Return self
		return self 

			#</AllSetsForEachMethod>

			#<EachSetForAllMethod>

	def commandEachSetForAll(self,_SettingVariableTuplesList,_GatheringVariablesList):
		"""Collect with _GatheringVariablesList and do a each set for all with _SettingVariableTuplesList"""

		#Get the GatheredVariablesList
		GatheredVariablesList=zip(*self.gather(_GatheringVariablesList))
		if len(GatheredVariablesList)>0:

			#Just keep the values
			GatheredVariablesList=GatheredVariablesList[1]

			#For each SettingTuple it is setted in _GatheredVariablesList
			map(
					lambda __SettingVariableTuple:
					map(
						lambda __GatheredVariable:
						__GatheredVariable.__setitem__(*__SettingVariableTuple),
						GatheredVariablesList
						),
					_SettingVariableTuplesList
				)

		#Return self
		return self 

			#</EachSetForAllMethod>

		#</CommandingMethods>

		#<ParsingMethods>

			#<WalkMethod>

	def walk(self,_WalkingGatheringVariablesList,**_WalkingDict):
		
		#Init the TopWalkedOrderedDict
		TopWalkedOrderedDict=None
		if 'IdString' not in _WalkingDict:

			#Define the IdString of this walk
			IdString=str(id(_WalkingDict))

			#Set the _WalkingDict
			_WalkingDict.update(
									{
										'IdString':IdString,
										'TopPointer':self,
									}
								)

			#Define TopWalkedOrderedDictKeyString
			TopWalkedOrderedDictKeyString=IdString+'WalkedOrderedDict'

			#Set the corresponding WalkedOrderedDict
			self.__setattr__(
								TopWalkedOrderedDictKeyString,
								collections.OrderedDict(**
								{
									'WalkedInt':-1,
									'TopWalkedIntsList':['/'],
									'GrabbedVariablesOrderedDict':collections.OrderedDict(),
									'RoutedVariablesList':[],
									'TopPointersList':[self]
								})
							)

			#Alias this Dict
			TopWalkedOrderedDict=getattr(self,TopWalkedOrderedDictKeyString)

		else:

			#Get the information at the top
			TopWalkedOrderedDictKeyString=_WalkingDict['IdString']+'WalkedOrderedDict'
			TopWalkedOrderedDict=getattr(_WalkingDict['TopPointer'],TopWalkedOrderedDictKeyString)
			TopWalkedOrderedDict['WalkedInt']+=1
			TopWalkedOrderedDict['TopWalkedIntsList']+=[str(TopWalkedOrderedDict['WalkedInt'])]
			TopWalkedOrderedDict['TopPointersList']+=[self]

		#Refresh the attributes
		self.WalkingDict=_WalkingDict

		#Maybe Grab things...It is important to put his kind of hook before the recursive walk if we want to set from top to down
		if 'GrabbingVariablesList' in _WalkingDict:
			self.grab(_WalkingDict['GrabbingVariablesList'],TopWalkedOrderedDict)

		#Maybe Route
		if 'RoutingVariablesList' in _WalkingDict:
			self.route(_WalkingDict['RoutingVariablesList'],TopWalkedOrderedDict)

		#An Update just before is possible
		if 'BeforeUpdatingTuplesList' in _WalkingDict:
			self.update(_WalkingDict['BeforeUpdatingTuplesList'])

		#Command an recursive order in other gathered variables
		self.commandAllSetsForEach(
									[
										('walk',{
													'ArgsVariable':_WalkingGatheringVariablesList,
													'KwargsDict':_WalkingDict
													})
									],
									_WalkingGatheringVariablesList
								)

		#An Update just after is possible
		if 'AfterUpdatingTuplesList' in _WalkingDict:
			self.update(_WalkingDict['AfterUpdatingTuplesList'])

		#Retrieve the previous Path
		if len(TopWalkedOrderedDict['TopWalkedIntsList'])>0:
			TopWalkedOrderedDict['TopWalkedIntsList']=TopWalkedOrderedDict['TopWalkedIntsList'][:-1] 
			TopWalkedOrderedDict['TopPointersList']=TopWalkedOrderedDict['TopPointersList'][:-1]

		#Return self
		if _WalkingDict['TopPointer']==self:
			self.WalkedOrderedDict=TopWalkedOrderedDict
			del self[TopWalkedOrderedDictKeyString]
			return self

			#</WalkMethod>

			#<GrabMethod>

	def grab(self,_GrabbingVariablesList,_TopWalkedOrderedDict):

		SYS.setWithDictatedVariableAndKeyVariable(
									_TopWalkedOrderedDict['GrabbedVariablesOrderedDict'],
									_TopWalkedOrderedDict['TopWalkedIntsList'],
									collections.OrderedDict(
										self.gather(
											_GrabbingVariablesList
										) if len(_GrabbingVariablesList)>0 else {}
									)
								)
			#</GrabMethod>

			#<RouteMethod>


	def route(self,_RoutingVariablesList,_TopWalkedOrderedDict):

		RoutedVariablesList=None
		RoutedVariablesList=reduce(
				operator.__add__,
				map(
					lambda __TopPointer:
					zip(*__TopPointer.gather(_RoutingVariablesList))[1],
					_TopWalkedOrderedDict['TopPointersList']
					)
			)
		_TopWalkedOrderedDict['RoutedVariablesList'].append(RoutedVariablesList)

			#</RouteMethod>

		#</ParsingMethod>
		
	#</WalkingMethods>

	#</MappingMethods>
	
	#<OrganizingMethods>

		#<ParentizingMethod>

	def parentize(self,_ParentingString):

		#Define the HookedString
		HookedParentingString=SYS.getHookedStringWithHookString(_ParentingString)

		#Define the ParentPointerKeyString
		ParentPointerKeyString=HookedParentingString+"ParentPointer"
		if getattr(self,ParentPointerKeyString)!=None:

			#Set the GrandParentPointersList
			GrandParentPointersListKeyString=HookedParentingString+"GrandParentPointersList"
			setattr(
						self,
						GrandParentPointersListKeyString,
						[getattr(self,ParentPointerKeyString)]+getattr(
															getattr(self,ParentPointerKeyString),
															GrandParentPointersListKeyString
															)
					)

			#Set the KeyStringsList
			KeyStringKeyString=HookedParentingString+"KeyString"
			KeyStringsListKeyString=HookedParentingString+"GrandParentKeyStringsList"
			self.__setattr__(
								KeyStringsListKeyString,
								map(
										lambda __GrandParentPointer:
										getattr(__GrandParentPointer,KeyStringKeyString),
										getattr(self,GrandParentPointersListKeyString)
									)
						)

			#Set the PathStringsList
			PathStringsListKeyString=HookedParentingString+"PathStringsList"
			self.__setattr__(
								PathStringsListKeyString,
								[getattr(self,KeyStringKeyString)]+copy.copy(
									getattr(self,KeyStringsListKeyString)
								)
							)
			getattr(self,PathStringsListKeyString).reverse()

			#Set the PathString
			PathStringKeyString=HookedParentingString+"PathString"
			self.__setattr__(
								PathStringKeyString,
								DeepShortString.join(getattr(self,PathStringsListKeyString))
							)

		#</ParentizingMethod>

		#<GroupingMethods>

	def group(self):

		#For the uppest Parent maybe init a hdf5 file
		if len(self.GroupedGrandParentPointersList)>0:
			UppestGroup=self.GroupedGrandParentPointersList[-1]
		else:
			UppestGroup=self

		#Check for a good FilePathString
		if UppestGroup.GroupingFilePathString!="":

			#Check for first write
			if os.path.isfile(UppestGroup.GroupingFilePathString)==False:
				UppestGroup.GroupedFilePointer=sys.modules[UppestGroup.GroupingModuleString].File(
					UppestGroup.GroupingFilePathString,'w')
				UppestGroup.GroupedFilePointer.close()

		if UppestGroup.GroupedFilePointer==None or ( 
			(UppestGroup.GroupingModuleString=='tables' and UppestGroup.GroupedFilePointer.isopen==0
				) or (UppestGroup.GroupingModuleString=='h5py' and UppestGroup.GroupedFilePointer.mode=='c') ):

			#Open the GroupedFilePointer
			UppestGroup.GroupedFilePointer=sys.modules[UppestGroup.GroupingModuleString].File(
				UppestGroup.GroupingFilePathString,'r+')

		#Point on the GroupingFilePointer of the uppest Parent
		self.GroupedFilePointer=UppestGroup.GroupedFilePointer

		#Create a group in the hdf5 file
		if self.GroupedFilePointer!=None:

			#Make sure that the first char is /
			if self.GroupedPathString[0]!="/":
				self.GroupedPathString="/"+self.GroupedPathString

			#Check if the Path exists
			if self.GroupedPathString not in self.GroupedFilePointer:

				#Set all the intermediate Paths before
				GroupPathStringsList=self.GroupedPathString.split('/')[1:]
				ParsingGroupPathString="/"

				#Set the PathString from the top to the down (integrativ loop)
				for GroupPathString in GroupPathStringsList:

					#Go deeper
					NewParsingGroupPathString=ParsingGroupPathString+GroupPathString

					#Create the group if not already
					if NewParsingGroupPathString not in self.GroupedFilePointer:
						if self.GroupingModuleString=="tables":
							self.GroupedFilePointer.create_group(ParsingGroupPathString,GroupPathString)
						elif self.GroupingModuleString=="h5py":
							Group=self.GroupedFilePointer[ParsingGroupPathString]
							Group.create_group(GroupPathString)
					
					#Prepare the next group	
					ParsingGroupPathString=NewParsingGroupPathString+'/'

		#</GroupingMethod>

		#<TabularingMethods>
			
	def tabular(self,_TabularingString):

		#Define HookedTabularString
		HookedTabularString=SYS.getHookedStringWithHookString(_TabularingString)

		#Define a short alias for the class
		TabularedClass=getattr(self,SYS.getClassStringWithTypeString(HookedTabularString+'Model'))

		#Init the table if it is not already
		TabularedPathString=self.GroupedPathString
		if self.GroupedPathString!='/':
			TabularedPathString+='/'
		TableNameString=self.GroupedKeyString+HookedTabularString+'Table'
		TabularedPathString+=TableNameString

		if TabularedPathString not in self.GroupedFilePointer:

			#Build the Table in the self.TabularedDict
			setattr(self,
						HookedTabularString+'Table',
						self.GroupedFilePointer.create_table(
									self.GroupedFilePointer.getNode(self.GroupedPathString),
									TableNameString,
									TabularedClass,
									TabularedClass.__doc__ 
									if TabularedClass.__doc__!=None 
									else "This is the "+TabularedClass.__name__
						)
					)
		else:

			#Get the Node and set it to the TabularedDict
			setattr(
						self,
						HookedTabularString+'Table',
						self.GroupedFilePointer.getNode(TabularedPathString)
					)

		#</TabularingMethods>

		#<RowingMethods>

	def row(self,_RowingTable,_RowingFeaturingTuplesList=[],_RowingOutputingTuplesList=[]):

		'''
		print(map(	
				lambda __Row:
						map(
						lambda __RowingTuple:
						(type(__Row[__RowingTuple[0]]),__RowingTuple[1]),					
						_RowingTuplesList
						),
				Table.iterrows()
				)
			)
		'''		

		#print(self.__class__.TypeString,_RowingTable,_RowingFeaturingTuplesList,_RowingOutputingTuplesList)
		
		if _RowingTable!=None:

			#Check if it is already appended
			IsAlreadyAppendedBool=any(
				map(	
					lambda __Row:
					all(
							map(
								lambda __RowingFeaturingTuple:
								SYS.getIsEqualBool(
									__Row[__RowingFeaturingTuple[0]],
									__RowingFeaturingTuple[1]
								),					
								_RowingFeaturingTuplesList
							)
						),
					_RowingTable.iterrows()
					)
				)

			if IsAlreadyAppendedBool==False:

				#Short alias for the row
				Row=_RowingTable.row

				#Set the TabularedInt
				Row.__setitem__('ModeledInt',_RowingTable.nrows)

				#Set special items
				map(
						lambda __FeaturingTuplesList:
						map(
								lambda __RowingTuple:
								Row.__setitem__(*__RowingTuple),
								__FeaturingTuplesList
							),
						[_RowingFeaturingTuplesList,_RowingOutputingTuplesList]
					)

				#Look for dataset
				'''
				.map(	
					lambda __GrindedArrayingColumningString,__GrindedArrayingPickedVariable:
					self.GroupedFilePointer.createArray(
						self.GroupedFilePointer.getNode(self.GroupedPathString),
						str(self[GrindedIntKeyString])+__GrindedArrayingColumningString,
						numpy.array(__GrindedArrayingPickedVariable)
						) if self.GroupedPathString+str(self[GrindedIntKeyString])+__GrindedArrayingColumningString not in self.GroupedFilePointer
					else None,
					*(GrindedArrayingColumningStringsList,
						self.pick(GrindedArrayingGettingStringsList))
				'''

				#Append
				Row.append()

				#Flush
				_RowingTable.flush()

		#</RowingMethods>

		#<ScanningMethods>

	def getOutputedValueVariablesListWithScanningModeledIntsTuple(self,_ScanningModeledIntsTuple):

		#Cast into list
		ScanningModeledIntsList=list(_ScanningModeledIntsTuple)

		#Map an update of the child objects and trigger their output, then update and trigger also the output of the self
		map(
				lambda __IntAndScanningModeledInt:
				self.update(
								#Get the FeaturingUpdatingTuplesList
								zip(
									SYS.unzip(self.FeaturingTuplesList,[1]),
									map(
											lambda __ColumnStringsList:
											self.FeaturedTable[__IntAndScanningModeledInt[1]][__ColumnStringsList],
											SYS.unzip(self.FeaturingTuplesList,[0])
										)
								)
				)('output',{})
				if __IntAndScanningModeledInt[0]==len(ScanningModeledIntsList)-1
				else
				None,
				enumerate(ScanningModeledIntsList)
			)

		#Return the OutputedValues
		return self.pick(SYS.unzip(self.OutputingTuplesList,[1]))

	def scan(self,_ModelString):

		#Init locals to be sure
		ScanningKeyStringsList=[]
		ScanningListsList=[]

		#For the Feature case it is only a scan on the Featuring values
		if _ModelString=='Feature':

			#Define the ScanningTuplesList,ScanningKeyStringsList,ScanningListsList
			ScanningTuplesList=getattr(self,SYS.getHookingStringWithHookString(_ModelString)+'TuplesList')
			ScanningListsList=SYS.unzip(ScanningTuplesList,[3])
			FeaturingRowingTuplesListsList=SYS.getScannedTuplesListWithScanningListsList(ScanningListsList)
			FeaturingColumnStringsList=SYS.unzip(ScanningTuplesList,[1])
			OutputingRowingTuplesListsList=[]
			OutputingColumnStringsList=[]

		#For the Output case it is a scan on the ModeledInt and the child ones for Featuring, and also record the outputs
		elif _ModelString=='Output':

			#Get the child grouped objects
			GroupedObjectsList=self.GroupedOrderedDict.values()

			#Define the JoinedModeledIntsList
			JoinedModeledIntsListsList=map(
					lambda __GroupedObject:
					(
						map(
								lambda __Row:
								__Row['ModeledInt'],
								__GroupedObject.FeaturedTable.iterrows()
							)
					),
					GroupedObjectsList
				)

			#Define the ModeledIntsList
			ModeledIntsList=map(
									lambda __Row:
									__Row['ModeledInt'],
									self.FeaturedTable.iterrows()
								)

			#Define the FeaturingRowingTuplesListsList
			FeaturingRowingTuplesListsList=SYS.getScannedTuplesListWithScanningListsList(
										JoinedModeledIntsListsList+[ModeledIntsList]
									)

			#Define the FeaturingColumnStringsList
			FeaturingColumnStringsList=['ModeledInt']+map(
													lambda __GroupedObject:
													SYS.getModeledIntColumnStringWithTable(__GroupedObject.FeaturedTable),
													GroupedObjectsList
												)
			#Define the OutputingRowingTuplesListsList
			OutputingRowingTuplesListsList=map(
										lambda __ScanningModeledIntsTuple:
										self.getOutputedValueVariablesListWithScanningModeledIntsTuple(__ScanningModeledIntsTuple),
										FeaturingRowingTuplesListsList
									)

			#Get the OutputingColumnStringsList
			OutputingColumnStringsList=SYS.unzip(self.OutputingTuplesList,[0])

		#Append the rows general method
		#if len(OutputingColumnStringsList)>0:
			#print("a",FeaturingColumnStringsList)
			#print("b",FeaturingRowingTuplesListsList)
			#print("c",OutputingColumnStringsList)
			#print("d",OutputingRowingTuplesListsList)
		map(
				lambda __FeaturingRowingTuplesList,__OutputingRowingTuplesList:
				self.row(
							getattr(self,SYS.getHookedStringWithHookString(_ModelString)+'Table'),
							zip(FeaturingColumnStringsList,__FeaturingRowingTuplesList) 
							if len(FeaturingColumnStringsList)>0 and __FeaturingRowingTuplesList!=None else [],
							zip(OutputingColumnStringsList,__OutputingRowingTuplesList) 
							if len(OutputingColumnStringsList)>0 and __OutputingRowingTuplesList!=None else []
					),
				FeaturingRowingTuplesListsList,
				OutputingRowingTuplesListsList
			)



		#</ScanningMethods>

		#<ModelingMethods>

	def model(self,_ModelString):

		#Define the class
		class ModelClass(IsDescription):

			#Add a tabulared Int (just like a unique KEY in mysql...) 
			ModeledInt=Int64Col()

			#Set the Col objects in this class... for is necessary for using the locals()...
			for ColumningString,Col in SYS.unzip(
											getattr(
														self,
														SYS.getHookingStringWithHookString(_ModelString)+'TuplesList'
													),
													[0,2]
												):

				#Set the Col 
				locals().__setitem__(ColumningString,Col)

			#If it is the Output table then it has to be the join with all the featuring ModeledInt of the submodels...
			if _ModelString=='Output':	

				#Get the child grouped objects
				GroupedObjectsList=self.GroupedOrderedDict.values()
				if len(GroupedObjectsList)>0:

					#Set the JoinedModeledInt
					for ColumningString in map(
												lambda __GroupedObject:
												SYS.getModeledIntColumnStringWithTable(__GroupedObject.FeaturedTable),
												GroupedObjectsList
											):

						#Set the Col 
						locals().__setitem__(ColumningString,Int64Col())

				#Del GroupedObjectsList
				del GroupedObjectsList

			#Del ColumningString,ColClass
			try:
				del ColumningString,Col
			except:
				pass

		#Define the HookedString
		HookedModelString=SYS.getHookedStringWithHookString(_ModelString)+'Model'

		#Give a name of this local defined class
		ModelClass.__name__=SYS.getClassStringWithTypeString(self.__class__.TypeString+HookedModelString)

		#Set the ModelClass to a node
		setattr(
					self,
					SYS.getClassStringWithTypeString(HookedModelString),
					ModelClass
				)

	def feature(self):

		self.update(
						[
							('GroupingFilePathString',SYS.getCurrentFolderPathString()+self.__class__.TypeString+'.hdf5')
						]
					).walk(
								[
									'GroupedOrderedDict.items()'
								],
								**{
									'BeforeUpdatingTuplesList':
									[
										('parentize',{'ArgsVariable':"Group"}),
										('group',{}),
										('model',{'ArgsVariable':"Feature"}),
										('tabular',{'ArgsVariable':"Feature"}),
										('scan',{'ArgsVariable':"Feature"}),
									]
								}
							)
		return self

		#</ModelingMethods>

		#<JoiningMethod>

	def output(self):

		self.walk(
					[
						'GroupedOrderedDict.items()'
					],
					**{
						'BeforeUpdatingTuplesList':
									[
										('model',{'ArgsVariable':"Output"}),
										('tabular',{'ArgsVariable':"Output"}),
									]
					}
				).walk(
						[
							'GroupedOrderedDict.items()'
						],
						**{
							'BeforeUpdatingTuplesList':
										[
											('grind',{})
										]
						}
					)

		return self

	def outputBefore(self,_LocalCallVariable,_LocalCallingVariablesList,**_LocalCallingVariablesDict):

		#Firt output the grouped children objects
		map(
				lambda __GroupedObject:
				__GroupedObject('output',[],**_LocalCallingVariablesDict),
				self.GroupedOrderedDict.values()
			)


		#</JoiningMethod>


		#<GrindingMethods>
	
	def grind(self,**_GrindingDict):

		#Init maybe the _GrindingDict
		if _GrindingDict=={}:
			_GrindingDict={
							'IsScanningBool':False
							}

		#If it is one of the deepest leaf...Then begin the grind of the Output!
		if len(self.GroupedOrderedDict)==0 or ('IsScanningBool' in _GrindingDict and _GrindingDict['IsScanningBool']):

			#Scan for here
			self.scan("Output")

		#If it is the last of the child then autorize the GroupedParentPointer to scan also
		if self.GroupedParentPointer!=None:
			if len(self.GroupedParentPointer.GroupedOrderedDict)==self.GroupedInt+1:

				#Grind for the grouped parent
				self.GroupedParentPointer.grind(**{'IsScanningBool':True})

		#</GrindingMethods>

		#<GlobalMethod>

	def close(self):

		#Close the GroupedFilePointer
		self.GroupedFilePointer.close()

		#Return self
		return self

		#</GlobalMethod>

	#</OrganizingMethods>

		#<JoinMethod>

	'''
	def join(self):

		#Define the Module
		Module=getattr(SYS,self.__module__)

		#Zip
		self.JoinedTuplesList=zip(
									Module.ModeledColumningStringsList,
									self.gather(
										[
											Module.ModeledGettingStringsList
										]
									)
								)

		#Set the JoinedString
		self.JoinedString='_'.join(
									map(
											lambda __JoinedTuple:
											'('+str(__JoinedTuple[0])+','+str(__JoinedTuple[1])+')',
											self.JoinedTuplesList
										)
								)

		#Return self
		return self
	'''
		#</JoinMethod>

		#<StructureMethod>
		#</StructureMethod>

	#</ParentizingMethod>

	#<PrintingMethods>

		#<ReprMethod>

	def __repr__(self):
		"""Call the repr<HookString> methods and return a SYS printed version of the self.PrintedDict"""

		#Refresh the attributes
		self.RepresentingDict=copy.deepcopy(SYS.PrintingDict)
		self.RepresentedTuplesList=self.__dict__.items()
		self.IsRepresentingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="repr"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsRepresentingBool==False:
						return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
								self,
								self.RepresentedDict
								)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
								dict(self.RepresentedTuplesList),
								self.RepresentingDict
								)

		#return the Print
		return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
				self,
				self.RepresentingDict
				)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
				dict(self.RepresentedTuplesList),
				self.RepresentingDict
				)

		#</ReprMethod>

	#</PrintingMethods>

	#</DefineMethods>

	#<DefineHookMethods>

	def getBefore(self):
		"""
			Hook in the __getitem__ of the Object 
			with different possible ShortStrings

			If _GettingVariable is an int, it returns the Value in the corresponding Index
			in self.AppendedValueVariablesList

			If _GettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			If _GettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			Else do a __getitem__ in the __dict__
		"""

		#Debug
		#debug("self.GettingVariable is : "+repr(self.GettingVariable))
		#debug("self.GettedVariable is : "+repr(self.GettedVariable))

		#Init a local IsGettedBool
		IsGettedBool=False

		#Case of a GettingString
		if type(self.GettingVariable) in [str,unicode]: 

			#Get with ExecShortString (it can be a return of a method call)
			if SYS.getCommonPrefixStringWithStringsList(
				[self.GettingVariable,ExecShortString])==ExecShortString:

				#Define the ExecString
				ExecString=self.GettingVariable.split(ExecShortString)[1]

				#Put the output in a local Local Variable
				exec ExecString in locals()

				#Give it to self.GettedVariable
				self.IsGettingBool=False
				return 

			#Get an Attribute (it can be a return of a method call)
			elif AttributeShortString in self.GettingVariable:

				#Define the GettingStringsList
				GettingStringsList=self.GettingVariable.split(AttributeShortString)

				#Look if the deeper getter is Ok
				GetterVariable=self[GettingStringsList[0]]

				#Define the next getting String
				NextGettingString=GettingStringsList[1]

				#If the GettingStringsList has brackets in the end it means a call of a method
				if "()"==NextGettingString[-2:]:
					NextGettingString=NextGettingString[:-2]

					#Case where it is an object
					if hasattr(GetterVariable,NextGettingString):
						self.GettedVariable=getattr(GetterVariable,NextGettingString)()
						self.IsGettingBool=False
						return 

					elif SYS.getIsTuplesListBool(GetterVariable):

						if NextGettingString=='keys':
							self.GettedVariable=map(
														lambda __ListedTuple:
														__ListedTuple[0],
														GetterVariable
													)
							self.IsGettingBool=False
							return 
						if NextGettingString=='values':
							self.GettedVariable=map(	
														lambda __ListedTuple:
														__ListedTuple[1],
														GetterVariable
													)
							self.IsGettingBool=False
							return 

				elif hasattr(GetterVariable,NextGettingString):

					#Else it is a get of an item in the __dict__
					GettingVariable=getattr(GetterVariable,NextGettingString)

					#Case where it has to continue to be getted
					if len(GettingStringsList)>2:
						GettingVariable[AttributeShortString.join(GettingStringsList[2:])]
						self.IsGettingBool=False
						return 
					else:

						#Else return the GettingVariable
						self.GettedVariable=GettingVariable
						self.IsGettingBool=False
						return 

			#Get with DeepShortString
			elif self.GettingVariable==DeepShortString:
				
				#If it is directly DeepShortString
				self.GettedVariable=self

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return 

			#Get in a ordered dict
			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,AppendShortString])==AppendShortString:

				#Define the appended TaggingKeyString
				AppendingString=self.GettingVariable.split(AppendShortString)[1]

				#Define the appended TagString and KeyString
				SplittedStringsList=AppendingString.split(TagString)
				TaggingString=SplittedStringsList[0]
				KeyString=TagString.join(SplittedStringsList[1:])

				#Get the corresponding ordered lists and dict
				HookedTagString=SYS.getHookedStringWithHookString(TaggingString)
				OrderedDict=getattr(self,HookedTagString+'OrderedDict')

				#Get with a digited KeyString case
				if KeyString.isdigit():

					#Define the GettingInt
					GettingInt=(int)(KeyString)

					#Check if the size is ok
					if GettingInt<len(OrderedDict):

						#Get the GettedVariable 
						self.GettedVariable=SYS.get(OrderedDict,'values',GettingInt)
						#Stop the getting
						self.IsGettingBool=False

						#Return
						return

				#Get in the ValueVariablesList
				elif KeyString in OrderedDict:
					
					#Get the GettedVariable
					self.GettedVariable=OrderedDict[KeyString]

					#Stop the getting
					self.IsGettingBool=False

					#Return 
					return
			
			#Get with '/'
			elif self.GettingVariable==DeepShortString:
				
				#If it is directly '/'
				self.GettedVariable=self

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return 

			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,DeepShortString])==DeepShortString:

				#Define the GettingVariableStringsList
				GettingVariableStringsList=DeepShortString.join(self.GettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
				GettedVariable=self[GettingVariableStringsList[0]]
				if GettedVariable!=None:

					if len(GettingVariableStringsList)==1:

						#Direct update in the Child or go deeper with the ChildPathString
						self.GettedVariable=GettedVariable

						#Stop the getting
						self.IsGettingBool=False
					
						#Return 
						return 

					else:

						#Define the ChildPathString
						ChildPathString=DeepShortString.join(GettingVariableStringsList[1:])	

						#Get in the first deeper Element with the ChildPathString
						self.GettedVariable=GettedVariable[ChildPathString]

						#Stop the getting
						self.IsGettingBool=False

						#Return
						return 

			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,CopyShortString])==CopyShortString:

				#deepcopy
				self.GettedVariable=copy.deepcopy(
						self[CopyShortString.join(self.GettingVariable.split(CopyShortString)[1:])]				
					)

				#Stop the getting
				self.IsGettingBool=False

				#Return 
				return 

			elif self.GettingVariable=='UtilitiesDict':
				self.GettedVariable=self.__dict__
				self.IsGettingBool=False
				return 
			elif self.GettingVariable=='MethodsDict':
				self.GettedVariable=SYS.getMethodsDictWithClass(self.__class__)
				self.IsGettingBool=False
				return  
			elif self.GettingVariable=="__class__":
				self.GettedVariable=self.__class__.__dict__
				self.IsGettingBool=False
				return

		'''
		#Case of an Object
		elif SYS.ObjectClass in type(self.GettingVariable).__mro__:

			#If the GettingVariable is an object already then return this
			self.GettedVariable=self.GettingVariable
			self.IsGettingBool=False
			return 
		'''

		#Do the minimal get in the Utilities
		if IsGettedBool==False:
		
			if type(self.GettingVariable) in [str,unicode]:

				#Get safely the Value
				if self.GettingVariable in self.__dict__:

					#__getitem__ in the __dict__
					self.GettedVariable=self.__dict__[self.GettingVariable]

					#Stop the getting
					self.IsGettingBool=False


	def setBefore(self):
		"""
			Hook in the __setitem__ of the Object 
			with different possible ShortStrings

			If _SettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the setting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is <camelCase> then it is a hook query...
			so it calls with the form self(<camelCase>,SettedVariable)

			If _SettingVariable is of the form <String1>Pointer and _SettedVariable of the form /<String2>
			it gets the self[<String2>] and do self[<String1>Pointer]=self[<String2>]

			Else do a __setitem__ in the __dict__ 
		"""

		#Case of a SettingString
		if type(self.SettingVariable) in [str,unicode]:

			#Adding set
			if SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,AddShortString])==AddShortString:

				#Define the added GettingString
				GettingString=self.SettingVariable.split(AddShortString)[1]

				#Get the added Variable
				Variable=self[GettingString]

				#Add
				if hasattr(Variable,'__add__'):
					Variable+=self.SettedVariable

				#Return
				return

			#Appending set
			elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,AppendShortString])==AppendShortString:

				#Define the appended TaggingKeyString
				TaggingKeyString=self.SettingVariable.split(AppendShortString)[1]

				#Define the appended TagString,KeyString,HookedTagString,OrderedDictKeyString
				SplittedStringsList=TaggingKeyString.split(TagString)
				TaggingString=SplittedStringsList[0]
				KeyString=TagString.join(SplittedStringsList[1:])
				HookedTagString=SYS.getHookedStringWithHookString(TaggingString)
				OrderedDictKeyString=HookedTagString+'OrderedDict'
				try:

					#Get the corresponding OrderedDict
					OrderedDict=getattr(self,OrderedDictKeyString)
				except AttributeError:

					#Set it for the first time else
					self.__setattr__(OrderedDictKeyString,collections.OrderedDict)

				#Append (or set if it is already in)
				OrderedDict[KeyString]=self.SettedVariable

				#If it is an object
				if SYS.ObjectClass in type(self.SettedVariable).__mro__:

					#Childify
					self.SettedVariable.__setattr__(HookedTagString+'Int',len(OrderedDict)-1)
					self.SettedVariable.__setattr__(HookedTagString+'KeyString',KeyString)
					self.SettedVariable.__setattr__(HookedTagString+'ParentPointer',self)

				#Return 
				return

			#Deep set
			elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,DeepShortString])==DeepShortString:

				#Get the SettingVariableStringsList
				SettingVariableStringsList=DeepShortString.join(self.SettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
				GettedVariable=self[SettingVariableStringsList[0]]
				if GettedVariable!=None:
					
					#Define the ChildPathString
					ChildPathString=DeepShortString.join(SettingVariableStringsList[1:])	

					#Direct update in the Child or go deeper with the ChildPathString
					if ChildPathString=="": 
						if self.__class__ in type(GettedVariable).__mro__:

							#Modify directly the GettedVariable with self.SettedVariable
							GettedVariable.__setitem__(*self.SettedVariable)

						elif SYS.getCommonPrefixStringWithStringsList([SettingVariableStringsList[0],AppendShortString])==AppendShortString:
							
							#Define the appended TaggingKeyString
							TaggingKeyString=self.SettingVariable.split(AppendShortString)[1]

							#Define the appended TagString and KeyString
							SplittedStringsList=TaggingKeyString.split(TagString)
							TaggingString=SplittedStringsList[0]
							KeyString=TagString.join(SplittedStringsList[1:])

							#Get the corresponding ordered lists and dict
							HookedTagString=SYS.getHookedStringWithHookString(TaggingString)
							OrderedDict=getattr(self,HookedTagString+'OrderedDict')

							#Append without binding
							if KeyString in OrderedDict:
								OrderedDict[KeyString]=self.SettedVariable
					else:

						#Set in the first deeper Element with the ChildPathString
						GettedVariable[ChildPathString]=self.SettedVariable

				#Return 
				return 

			#Call for a hook
			elif (self.SettingVariable[0].isalpha() or self.SettingVariable[:2]=="__") and  self.SettingVariable[0].lower()==self.SettingVariable[0]:

				#Get the Method
				if hasattr(self,self.SettingVariable):

					#Get the method
					SettingMethod=getattr(self,self.SettingVariable)

					#Adapt the shape of the args
					SYS.getWithMethodAndArgsList(
													SettingMethod,
													self.SettedVariable['ArgsVariable'] if 'ArgsVariable' in self.SettedVariable else [],
													**self.SettedVariable['KwargsDict'] if 'KwargsDict' in self.SettedVariable else {}
												)
				
				elif len(self.SettedVariable)==1:

					#Adapt format of the ArgsList
					self(self.SettingVariable,self.SettedVariable[0])
				else:
					self(self.SettingVariable,*self.SettedVariable)

				#Return
				self.IsSettingBool=False
				return

			elif type(self.SettedVariable) in [str,unicode]:

				if SYS.getCommonSuffixStringWithStringsList(
					[self.SettingVariable,PointerShortString])==PointerShortString:
					
					#The Value has to be a deep short string for getting the pointed variable
					if SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,DeepShortString])==DeepShortString:
						GettedVariable=self[self.SettedVariable]
						if GettedVariable!=None:
							#Link the both with a __setitem__
							self[self.SettingVariable]=GettedVariable

					#Return in the case of the Pointer and a string setted variable
					return

				#Get with ExecShortString (it can be a return of a method call)
				elif SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,ExecShortString])==ExecShortString:

					self[self.SettingVariable]=self[self.SettedVariable]
					self.IsSettingBool=False
					return

			#__setitem__ in the __dict__, this is an utility set
			self.__dict__[self.SettingVariable]=self.SettedVariable

	def deleteBefore(self):
		"""
			Hook in the __delitem__ of the Object
		"""

		#Do the minimal delitem
		if type(self.DeletingVariable) in [str,unicode]:

			#Del Safely the Value
			if self.DeletingVariable in self.__dict__:
				del self.__dict__[self.DeletingVariable]

	def reprAfter(self):
		"""
			Hook in the repr of the Object
			for printing in the Console attributes in a more 
			user-friendly manner
		"""

		#Get the MroAppendedStringsList
		MroAppendedStringsList=SYS.getMroKeyStringsListWithClass(self.__class__)

		#Remove the NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.NotRepresentingGettingVariablesList,
										self.RepresentedTuplesList
								)

		#First keeps only the Specific and New attributes
		self.RepresentedTuplesList=map(
									lambda _RepresentedTuple:
									("<Spe>"+_RepresentedTuple[0],_RepresentedTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] in self.__class__.SpecificKeyStringsList,
											self.RepresentedTuplesList
										)
								)+map(
									lambda _NewTuple:
									("<New>"+_NewTuple[0],_NewTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] not in MroAppendedStringsList,
											self.RepresentedTuplesList
										)
								)

		#Add some forced Values with RepresentingKeyVariables
		self.RepresentedTuplesList+=map(
											lambda _KeyVariable:
											("<NotSpe>"+str(_KeyVariable),self[_KeyVariable]),
											self.RepresentingKeyVariablesList
										)

		#Simplify the numpy variables repr
		self.RepresentedTuplesList=map(
										lambda _RepresentedTuple:
										_RepresentedTuple
										if type(_RepresentedTuple[1]) not in [numpy.ndarray] 
										else (
												_RepresentedTuple[0],
												"<numpy.ndarray shape "+str(numpy.shape(
													_RepresentedTuple[1]))+">"
												),
										self.RepresentedTuplesList
									)

	
	#</DefineHookMethods>

	


#</DefineClass>



