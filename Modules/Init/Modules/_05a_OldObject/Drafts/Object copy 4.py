#<ImportModules>
import copy
import inspect
import ShareYourSystem as SYS
import numpy
#</ImportModules>

#<DefineLocals>
DeepShortString="/"
OrderShortString="*"
PointerShortString="Pointer"
CopyShortString='c'
#</DefineLocals>

def debug(_String):
	Frame=inspect.currentframe().f_back
	ScriptPathStringsList=SYS.getWordStringsListWithString(Frame.f_code.co_filename)
	ScriptPathString=''.join(ScriptPathStringsList)
	print("%s , %s, l. %s : "%(ScriptPathString,Frame.f_code.co_name,Frame.f_lineno));
	print(_String)
	print("")

#<DefineClass>
class ObjectClass(object):
	
	#<DefineMethods>

		#<InitMethod>
	def __init__(self):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""

			#<DefineSpecificDict>

				#<InitAttributes>

		#Attributes for the __init__ property
		self.IsInitiatingBool=True

				#</InitAttributes>

				#<ApplyAttributes>

		self.IsApplyingBool=True
		#self.ApplyVariable=None
		#self.ApplyingVariablesList=[]
		#self.AppliedVariablesList=[]

				#</ApplyAttributes>

				#<SetAttributes>

					#<UnOrderedSetAttributes>

						#<UnitUnOrderedSetAttributes>

		#Attributes for the __setitem__ property
		self.IsSettingBool=True
		self.SettedVariable=None
		self.SettingVariable=None

						#</UnitUnOrderedSetAttributes>

						#<MappingUnOrderedSetAttributes>

		#Attributes for the update property
		self.IsUpdatingBool=True
		self.UpdatingVariable=None
		self.UpdatedVariable=None
				
						#</MappingUnOrderedSetAttributes>

					#</UnOrderedSetAttributes>

					#<OrderedSetAttributes>

						#<UnitOrderedSetAttributes>

		#Attributes for the append property
		self.IsAppendingBool=True
		self.AppendingVariable=None
		self.AppendedInt=-1
		self.AppendedKeyString=""
		self.AppendedValueVariable=None
		self.AppendedKeyVariablesList=[]
		self.AppendedKeyStringsList=[]
		self.AppendedValueVariablesList=[]
		self.AppendedKeyStringToAppendedIntDict={}
				
						#</UnitOrderedSetAttributes>

						#<MappingOrderedSetAttributes>

		#Attributes for the __add__ property
		self.IsAddingBool=True
		self.AddedVariable=None
				
						#</MappingOrderedSetAttributes>

					#</OrderedSetAttributes>

				#</SetAttributes>

				#<UnSetAttributes>

		#Attributes for the __delitem__ property
		self.IsDeletingBool=True

		#Attributes for the pop property
		self.IsPopingBool=True

				#</UnSetAttributes>

				#<GetAttributes>

					#<NotDeepGetAttributes>

						#<UnitNotDeepGetAttributes>

		#Attributes for the __getitem__ property
		self.IsGettingBool=True
		self.GettingVariable=None
		self.GettedVariable=None

						#</UnitNotDeepGetAttributes>

						#<MappingNotDeepGetAttributes>

		#Attributes for the pick property
		self.StructuringKeyVariablesList=['AppendedInt','AppendedKeyString']
		self.StructuredAppendedInt=""
		self.StructuredAppendedKeyString=""

		#Attributes for the feature property
		self.FeaturingKeyVariablesList=[]

		#Attributes for the output property
		self.OutputingKeyVariablesList=[]

						#</MappingNotDeepGetAttributes>

					#<NotDeepGetAttributes>

					#<DeepGetAttributes>

						#</UnitNotDeepGetAttributes>

		#Attributes for the copy property
		self.IsCopyingBool=False
		self.CopiedTuplesList=[]
		self.CopyingKeyVariablesList=[]

						#</UnitNotDeepGetAttributes>

						#<MappingDeepGetAttributes>

		#Attributes for the scan property
		self.IsScanningBool=False
		self.ScannedTuplesList=[]
		self.ScanningKeyVariablesList=[]

						#</MappingDeepGetAttributes>

					#<DeepGetAttributes>

				#</GetAttributes>

				#<ControlAttributes>

					#<NotDeepControlAttributes>

						#<UnitNotDeepControlAttributes>

		#Attributes for the share property (get and set/append in SharedPointer)
		self.IsSharingBool=True
		self.SharedPointer=None

						#</UnitNotDeepControlAttributes>

						#<MappingNotDeepControlAttributes>

		#Attributes for the link property (pick and update in LinkedPointer)
		self.IsLinkingBool=True


						#</MappingNotDeepControlAttributes>

					#</NotDeepControlAttributes>

					#<DeepControlAttributes>

						#<UnitDeepControlAttributes>

		#Attributes for the write property (copy and set/append in WrittenPointer)
		self.IsWritingBool=True
		self.WrittenPointer=None

						#<UnitDeepControlAttributes>

						#<MappingDeepControlAttributes>
				
		#Attributes for the deliver property (copy and set in DeliveredPointer)
		self.IsDeliveringBool=True
		self.DeliveredTuplesList=[]
		self.DeliveredPointer=None

						#</MappingDeepControlAttributes>

					#</DeepControlAttributes>

				#</ControlAttributes>

				#<RelayAttributes>

		#Attributes for the relay property (copy and set in DeliveredPointer)
		self.IsRelayingBool=True
		self.RelayedKeyVariablesList=[]
		self.RelayingKeyVariablesList=[]
		self.RelayingVariable=None

				#</RelayAttributes>

				#<PrintAttributes>

		#Attributes for the __repr__ property
		self.IsRepresentingBool=True
		self.RepresentingDict={}
		self.RepresentedTuplesList=[]
		self.NotRepresentingAppendedKeyStringsList=filter(
													lambda _KeyString:
													_KeyString not in [
																		'AppendedKeyStringsList',
																		'AppendedValueVariablesList',
																		'StructuredAppendedInt',
																		'StructuredAppendedKeyString'
																	],
													self.__class__.SpecificKeyStringsList
										) if type(self)==SYS.ObjectClass else []
		self.RepresentingKeyVariablesList=[
											'AppendedKeyStringsList',
											'AppendedValueVariablesList',
											'StructuredAppendedInt',
											'StructuredAppendedKeyString'
											] if type(self)!=SYS.ObjectClass else []

				#</PrintAttributes>

				#<CallAttributes>

		#Attributes for the __call__ property
		self.IsCallingBool=True
		self.CallString=""
		self.CallingList=[]
		self.CallingDict={}
		self.CalledPointer=None

				#</CallAttributes>

		#Attributes for the use property
		self.UsedPointer=None

			#</DefineSpecificDict>

		#Hook methods (integrativ)
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="init"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsInitiatingBool==False:
						return 

					#Call the HookingMethod
					HookingMethod(self)

		#Return None

		#</InitMethod>

		#<ApplyMethod>

	def apply(self,_ApplyVariable,_ApplyingVariablesList):
		"""Call the apply<HookString> methods"""
		
		#Refresh the attributes
		#self.ApplyVariable=_ApplyVariable
		#self.ApplyingVariablesList=_ApplyingVariablesList
		LocalApplyVariable=_ApplyVariable
		LocalApplyingVariablesList=_ApplyingVariablesList
		self.IsApplyingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="apply"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,LocalApplyVariable,LocalApplyingVariablesList)

					if type(OutputVariable)==dict:
						if 'LocalApplyVariable' in OutputVariable:
							LocalApplyVariable=OutputVariable['LocalApplyVariable']
						if 'LocalApplyingVariablesList' in OutputVariable:
							LocalApplyVariable=OutputVariable['LocalApplyingVariablesList']

					#Check Bool
					if self.IsApplyingBool==False:
						return self

		#Return self
		return self

		#</ApplyMethod>

		#<SetMethods>

			#<UnOrderedSetMethods>

				#<UnitUnOrderedSetMethod>

	def __setitem__(self,_SettingVariable,_SettedVariable):
		"""Call the set<HookString> methods"""

		#Refresh the attributes
		self.SettingVariable=_SettingVariable
		self.SettedVariable=_SettedVariable
		self.IsSettingBool=True

		#Sort the HookMethodStrings
		for OrderString in ["Before","After"]:

			#Define a Local IsSettedBool
			IsSettedBool=False

			if type(_SettingVariable) in [str,unicode]:
			
				#Define the BindingMethodString
				BindingMethodString="bind"+_SettingVariable+OrderString
				
				#Check that there is HookingMethods for it
				if BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict:
					
					#Say ok
					IsSettedBool=True

					#Call the specific Appended methods 
					for HookingMethod,BindingMethod in self.__class__.BindingMethodStringToMethodTuplesListDict[BindingMethodString]:

						#Call the HookMethod
						if callable(HookingMethod):
							HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

						#Call the Binding
						if callable(BindingMethod):
							OutputVariable=BindingMethod(self)

							#End the Setting
							if self.IsSettingBool==False:
								return self

			#If a binding method was not yet found then look for a global set method
			if IsSettedBool==False:

				#_SettingVariable is not a string
				HookingMethodString="set"+OrderString

				#Check that there is HookingMethods for it
				if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

					#Call the specific Appended methods 
					for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

						#Call the HookingMethod
						HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

		#Return self
		return self

			#</UnitUnOrderedSetMethod>
			
			#<MappingUnOrderedSetMethod>

	def update(self,_UpdatingVariablesList):
		"""Apply the __setitem__ to the <_UpdatingVariablesList>"""

		#Refresh the attributes
		#self.UpdatingVariablesList=_UpdatingVariablesList

		#Apply __setitem__
		self.apply('__setitem__',_UpdatingVariablesList)
					
		#Return 
		return self

			#</MappingUnOrderedSetMethod>

		#</UnOrderedSetMethods>

		#<OrderedSetMethods>

			#<UnitOrderedSetMethod>

	def append(self,_AppendingVariable):
		"""Call the append<HookString> methods for append in the OrderedKeyStringsList and OrderedVariablesList"""

		#Refresh the attributes
		self.AppendingVariable=_AppendingVariable

		#Set the corresponding AppendedInt
		self.AppendedInt=len(self.AppendedKeyStringsList)

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="append"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsAppendingBool==False:
						return self

		#return self
		return self

			#</UnitOrderedSetMethod>

			#<MappingOrderedSetMethod>

	def __add__(self,_AddingVariablesList):
		"""Apply the append to the <_AddingVariablesList>"""

		#Refresh the attributes
		#self.AddingVariablesList=_AddingVariablesList

		#Apply
		self.apply('append',_AddingVariablesList)

		#Return 
		return self

			#</MappingOrderedSetMethod>

		#<OrderedSetMethods>

	#<SetMethods>

	#<UnSetMethods>

		#<UnOrderedUnSetMethods>

			#<UnitsUnOrderedUnSetMethod>

	def __delitem__(self,_DeletingVariable):
		"""Call the del<HookString> methods and delete in the __dict__ by default"""
		
		#Refresh the attributes
		self.DeletingVariable=_DeletingVariable
		self.IsDeletingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="delete"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsDeletingBool==False:
						return self

		#Return self
		return self

			#</UnitUnOrderedUnSetMethod>

		#</UnOrderedUnSetMethods>

	#<UnSetMethods>

	#<JoinMethod>
	def join(self):
		"""Call the join<HookString> methods and return "_".join(<JoinedTuplesList>) with self.AppliedVariablesList"""

		#Apply __getitem__ with self.FeaturingKeyVariablesList
		self.apply('__getitem__',self.FeaturingKeyVariablesList)

		#Return The joined String
		return "_".join(
							map(
									lambda _FeaturingKeyVariable,_AppliedVariable:
									"("+str(_FeaturingKeyVariable)+","+str(_AppliedVariable)+")",
									self.FeaturingKeyVariablesList,
									self.AppliedVariablesList
								)
						)
	#</JoinMethod>

	#<GetMethods>

		#<NotDeepGetMethods>

			#<UnitNotDeepGetMethod>

	def __getitem__(self,_GettingVariable):
		"""Call the get<HookString> methods and return self.GettedVariable (None by default)"""
		
		#Refresh the attributes
		self.GettingVariable=_GettingVariable
		self.GettedVariable=None
		self.IsGettingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="get"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Debug
					#debug("HookingMethodString is : "+HookingMethodString)
					#debug("self.GettedVariable is : "+repr(self.GettedVariable))

					#Check Bool
					if self.IsGettingBool==False:
						return self.GettedVariable

		#Return GettedVariable
		return self.GettedVariable

			#</UnitNotDeepGetMethod>

			#<MappingNotDeepGetMethods>

	def feature(self):
		"""Call the feature<HookString> methods and return <FeaturedTuplesList> with self.AppliedVariablesList"""

		#Apply __getitem__ with self.FeaturingKeyVariablesList
		self.apply('__getitem__',self.FeaturingKeyVariablesList)

		#Return <FeaturedTuplesList>
		return zip(self.FeaturingKeyVariablesList,self.AppliedVariablesList)

	def output(self):
		"""Call the output<HookString> methods and return <OutputedTuplesList> with self.AppliedVariablesList"""

		#Apply __getitem__ with self.OutputingKeyVariablesList
		self.apply('__getitem__',self.OutputingKeyVariablesList)

		#Return <OutputedTuplesList>
		return zip(self.OutputingKeyVariablesList,self.AppliedVariablesList)

			#</MappingNotDeepGetMethods>

		#</NotDeepGetMethods>

		#</DeepGetMethods>

			#<UnitDeepGetMethod>

	def copy(self,_CopyingVariable):
		"""Call the copy<HookString> methods and return self.CopiedVariable"""

		#Refresh the attributes
		self.CopyingVariable=_CopyingVariable
		self.CopiedVariable=None
		self.IsCopyingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="copy"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsCopyingBool==False:
						return self.CopiedVariable

		#return the CopiedVariable
		return self.CopiedVariable

			#</UnitDeepGetMethod>

			#<MappingDeepGetMethod>

	def scan(self):
		"""Call the scan<HookString> methods and return a <ScannedTuplesList> with self.AppliedVariablesList"""

		#Apply __getitem__ with self.ScanningKeyVariablesList
		self.apply('copy',self.ScanningKeyVariablesList)

		#Return PickedTuplesList
		return zip(self.ScanningKeyVariablesList,self.AppliedVariablesList)

			#</MappingDeepGetMethod>

		#</DeepGetMethods>

	#</GetMethods>

	#<ControlMethods>

		#<NotDeepControlMethods>

			#<UnitNotDeepControlMethod>

	def share(self,_SharingPointer,_SharingVariable):
		"""__getitem__ in the self with _SharingVariable and __setitem__ to the _SharingPointer with the getted tuple"""

		#Get and set
		_SharingPointer.__setitem__(_SharingVariable,self[_SharingVariable])

		#return self
		return self

			#</UnitNotDeepControlMethod>

			#<MappingNotDeepControlMethod>

	def structure(self,_LinkingPointer):
		"""Pick in the self and update to the _LinkingPointer"""

		#Apply __getitem__ with self.StructuringKeyVariablesList
		self.apply('__getitem__',self.StructuringKeyVariablesList)

		#Return <StructuredTuplesList>
		StructuringTuplesList=zip(
					map(
							lambda __KeyVariable:
							'Structured'+str(__KeyVariable),
							self.StructuringKeyVariablesList
						),
					self.AppliedVariablesList
				)

		#Pick and Update
		_LinkingPointer.update(StructuringTuplesList)

		#return self
		return self

	def parameterize(self,_ParameterizingPointer):
		"""Feature in the self with _SharingVariable and update to the _ParameterizingPointer"""

		#Feature and Update
		_ParameterizingPointer.update(self.feature())

		#return self
		return self

	def store(self,_StoringPointer):
		"""Output in the self with _SharingVariable and update to the _StoringPointer"""

		#Output and Update
		_StoringPointer.update(self.output())

		#return self
		return self

			#</MappingNotDeepControlMethod>

		#</NotDeepControlMethod>

		#<DeepControlMethod>

			#<UnitDeepControlMethod>

	def write(self,_WritingPointer,_WritingVariable):
		"""Copy in the self with _WritingVariable and update in the _WritingPointer"""

		#Get and copy
		_WritingPointer.__setitem__(_WritingVariable,self.copy(_WritingVariable))

		#return self
		return self

			#<UnitDeepControlMethod>

			#<MappingDeepControlMethod>

	def deliver(self,_DeliveringPointer):
		"""Scan in the self and update the _DeliveringPointer"""

		#Scan and Update
		_DeliveringPointer.update(self.scan())

		#return self
		return self

			#</MappingDeepControlMethod>

		#</DeepControlMethod>

	#</ControlMethods>

	#<CommunicationMethods>

	def relay(self,_RelayingVariable):
		"""Call the relay<HookString> methods and return self"""

		#Refresh the attributes
		self.RelayingVariable=_RelayingVariable
		self.RelayedPointer=self
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="relay"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsRelayingBool==False:
						return self.RelayedPointer

		#Return self.RelayedVariable
		return self.RelayedPointer

	def control(self,_ControllingVariablesList):
		"""Apply relay for each element of the _ControllingVariablesList"""

		#Apply relay
		self.apply('relay',_ControllingVariablesList)

		#return self
		return self
	#</CommunicationMethods>

	#<CallMethod>
	def __call__(self,_CallVariable,*_CallingVariablesList):
		"""Call the <_CallString><HookString> methods and return self.CalledVariable (self by default)"""

		#Refresh the attributes
		#self.CallString=_CallString
		#self.CallingList=_CallingList
		#self.CallingDict=_CallingDict
		LocalCallVariable=_CallVariable
		LocalCallingVariablesList=_CallingVariablesList
		LocalCalledPointer=self

		#self.CalledPointer=self
		self.IsCallingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString=LocalCallVariable+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,LocalCallVariable,LocalCallingVariablesList)

					if type(OutputVariable)==dict:
						if 'LocalCallVariable' in OutputVariable:
							LocalCallVariable=OutputVariable['LocalCallVariable']
						if 'LocalCallingVariablesList' in OutputVariable:
							LocalCallingVariable=OutputVariable['LocalCallingVariablesList']
						if 'LocalCallPointer' in OutputVariable:
							LocalCallPointer=OutputVariable['LocalCallPointer']

					#Check Bool
					if self.IsCallingBool==False:
						return LocalCalledPointer

		#Return the OutputVariable
		return LocalCalledPointer
	#</CallMethod>

	#<PrintMethod>
	def __repr__(self):
		"""Call the repr<HookString> methods and return a SYS printed version of the self.PrintedDict"""

		#Refresh the attributes
		self.RepresentingDict=copy.deepcopy(SYS.PrintingDict)
		self.RepresentedTuplesList=self.__dict__.items()
		self.IsRepresentingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="repr"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsRepresentingBool==False:
						return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
								self,
								self.RepresentedDict
								)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
								dict(self.RepresentedTuplesList),
								self.RepresentingDict
								)

		#return the Print
		return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
				self,
				self.RepresentingDict
				)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
				dict(self.RepresentedTuplesList),
				self.RepresentingDict
				)
	#</PrintMethod>
	
	#</DefineMethods>

	#<DefineHookMethods>

		#<ApplyHookMethod>
	def applyBefore(self,_LocalApplyVariable,_LocalApplyingVariablesList):
		"""
			Hook in the apply of the Object
		"""

		#Get the AppliedMethod
		ApplyString=str(_LocalApplyVariable)
		if hasattr(self,ApplyString):

			#Define the ApplyingMethod
			ApplyingMethod=getattr(self,ApplyString)

			#Look for the shape of the inputs of this method
			ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(ApplyingMethod)

			#Apply for each with a map and the ApplyingMethod
			if len(ArgsList)>2:
				self.AppliedVariablesList=map(
						lambda __ApplyingVariable:
						ApplyingMethod(*__ApplyingVariable),
						_LocalApplyingVariablesList
					)
			else:
				self.AppliedVariablesList=map(
							lambda __ApplyingVariable:
							ApplyingMethod(__ApplyingVariable),
							_LocalApplyingVariablesList
						)

		else:

			#Apply for each with a map and a Call
			self.AppliedVariablesList=map(
					lambda __ApplyingVariable:
					#ApplyVariable is a CallString and _ApplyingVariable is a CallingList
					self(_LocalApplyVariable,*[__ApplyingVariable])
					if type(__ApplyingVariable)!=list
					else self(_LocalApplyVariable,__ApplyingVariable),
					_LocalApplyingVariablesList
				)
		#</ApplyHookMethod>
	
		#<SetHookMethods>

			#<UnOrderedSetHookMethod>

				#<UnitUnOrderedSetHookMethod>

	def setBefore(self):
		"""
			Hook in the __setitem__ of the Object 
			with different possible ShortStrings

			If _SettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the setting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is <camelCase> then it is a hook query...
			so it calls with the form self(<camelCase>,SettedVariable)

			If _SettingVariable is of the form <String1>Pointer and _SettedVariable of the form /<String2>
			it gets the self[<String2>] and do self[<String1>Pointer]=self[<String2>]

			Else do a __setitem__ in the __dict__ 
		"""

		#Init a local IsSettedBool
		IsSettedBool=False

		#Deep set
		if SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,DeepShortString])==DeepShortString:

			#Get the SettingVariableStringsList
			SettingVariableStringsList=DeepShortString.join(self.SettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
			GettedVariable=self[SettingVariableStringsList[0]]
			if GettedVariable!=None:
				
				#Define the ChildPathString
				ChildPathString=DeepShortString.join(SettingVariableStringsList[1:])	

				#Direct update in the Child or go deeper with the ChildPathString
				if ChildPathString=="":
					if type(self.SettedVariable)==tuple:
						GettedVariable.__setitem__(*self.SettedVariable)
					if type(self.SettedVariable)==list:
						GettedVariable.update(*self.SettedVariable)
					elif hasattr(self.SettedVariable,"items"):
						GettedVariable.update(**self.SettedVariable)
				else:
					#Set in the first deeper Element with the ChildPathString
					GettedVariable[ChildPathString]=self.SettedVariable

				#Say that it is setted
				IsSettedBool=True

				#Stop the setting at this level
				self.IsSettingBool=False

		#Set with the OrderShortString
		elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,OrderShortString])==OrderShortString:
			
			#Remove the OrderShortString
			self.SettingVariable=self.SettingVariable.split(OrderShortString)[1]

			#Append
			self.append((self.SettingVariable,self.SettedVariable))
				
			#Say that it is setted
			IsSettedBool=True

			#Stop the setting at this level
			self.IsSettingBool=False

		#Call for a hook
		elif self.SettingVariable[0].lower()==self.SettingVariable[0]:

				"""
				#Look for the shape of the inputs of this method
				ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(ApplyingMethod)
				"""

				#Get the Method
				if hasattr(self,self.SettingVariable):

					#Get the method
					SettingMethod=getattr(self,self.SettingVariable)

					#Look for the shape of the inputs of this method
					ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(SettingMethod)

					#Adapt the shape of the args
					if len(ArgsList)>2:
						getattr(self,self.SettingVariable)(*self.SettedVariable)
					else:
						getattr(self,self.SettingVariable)(self.SettedVariable)
				
				elif len(self.SettedVariable)==1:

					#Adapt format of the ArgsList
					self(self.SettingVariable,self.SettedVariable[0])
				else:
					self(self.SettingVariable,*self.SettedVariable)

				#Say that it is setted
				IsSettedBool=True

				#Stop the setting at this level
				self.IsSettingBool=False

		elif SYS.getCommonSuffixStringWithStringsList([self.SettingVariable,PointerShortString])==PointerShortString:
			
			#Special set for Pointer with a / getted Value
			if type(self.SettedVariable) in [str,unicode]:

				#The Value has to be a deep short string for getting the pointed variable
				if SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,DeepShortString])==DeepShortString:
					GettedVariable=self[self.SettedVariable]
					if GettedVariable!=None:

						#Link the both with a __setitem__
						self[self.SettingVariable]=GettedVariable

						#Say that it is setted
						IsSettedBool=True

						#Stop the setting at this level
						self.IsSettingBool=False

		#Do the minimal set if nothing was done yet
		if IsSettedBool==False:
		
			if type(self.SettingVariable) in [str,unicode]:

				#__setitem__ in the __dict__
				self.__dict__[self.SettingVariable]=self.SettedVariable

				#<UnitUnOrderedSetHookMethod>
			
			#</UnOrderedSetHookMethods>

			#<OrderedSetHookMethods>

				#<UnitOrderedSetHookMethod>

	def appendBefore(self):
		"""
			Hook in the append of the Object
		"""

		if type(self.AppendingVariable) in [list,tuple] and len(self.AppendingVariable)==2:
			
			#Special case of '*' append
			if '*' in self.AppendingVariable[0]:

				#Define AppendingStringsList
				AppendingStringsList=self.AppendingVariable[0].split('*')
				
				#Append in the child
				self['*'+AppendingStringsList[0]].append(('*'.join(AppendingStringsList[1:]),self.AppendingVariable[1]))

				#Stop the append
				self.IsAppendingBool=False

				#Return
				return 

			else:

				#Set the AppendedKeyString and AppendedValueVariable
				self.AppendedKeyString=self.AppendingVariable[0]
				self.AppendedValueVariable=self.AppendingVariable[1]

		else:

			#Check that it is an Object
			if SYS.ObjectClass in type(self.AppendingVariable).__mro__:

				#Set the AppendedKeyString and AppendedValueVariable
				self.AppendedKeyString=self.AppendingVariable.join()
				self.AppendedValueVariable=self.AppendingVariable

		#Append in the AppendedKeyStringsList and the AppendingVariable with the join signature
		if self.AppendedInt<len(self.AppendedKeyStringsList):
			self.AppendedKeyStringsList[self.AppendedInt]=self.AppendedKeyString
			self.AppendedValueVariablesList[self.AppendedInt]=self.AppendedValueVariable
		else:
			self.AppendedKeyStringsList.append(self.AppendedKeyString)
			self.AppendedValueVariablesList.append(self.AppendedValueVariable)

		#Update the self.AppendedKeyStringToAppendedIntDict
		self.AppendedKeyStringToAppendedIntDict[self.AppendedKeyString]=self.AppendedInt	
	
		#Link and deliver to the AppendedValueVariable
		if hasattr(self.AppendedValueVariable,'__setitem__'):
			
			#Structure
			self.structure(self.AppendedValueVariable)

			'''
			ChildAppendedKeyStringsList=['AppendedInt','AppendedKeyString']

			#TuplesList Case
			if SYS.getIsTuplesListBool(self.AppendedValueVariable):

				#Map
				map(
						lambda __KeyString:
						self.AppendedVariable.append((__KeyString,getattr(self,__KeyString))),
						ChildAppendedKeyStringsList
					)
			else:
				
				#Dictable case
				map(
						lambda __KeyString:
						self.AppendedVariable.__setitem__(__KeyString,getattr(self,__KeyString)),
						ChildAppendedKeyStringsList
					)
			'''

				#</UnitOrderedSetHookMethod>
				
				#<MappingOrderedSetHookMethod>

				#</MappingOrderedSetHookMethod>

			#</OrderedSetHookMethod>

		#</SetHookMethods>

		#<UnSetHookMethods>

			#<UnOrderedUnSetHookMethods>

				#<UnitUnOrderedUnSetHookMethods>
		
	def delBefore(self):
		"""
			Hook in the __delitem__ of the Object
		"""

		#Do the minimal getitem
		if type(self.DeletingVariable) in [str,unicode]:

			#Get Safely the Value
			if self.DeletingVariable in self.__dict__:
				del self.__dict__[self.DeletingVariable]

				#</UnitUnOrderedUnSetHookMethods>

			#</UnOrderedUnSetHookMethods>

		#</UnSetHookMethods>

		#<GetHookMethods>

			#<NotDeepGetHookMethods>

				#<UnitNotDeepGetHookMethods>

	def getBefore(self):
		"""
			Hook in the __getitem__ of the Object 
			with different possible ShortStrings

			If _GettingVariable is an int, it returns the Value in the corresponding Index
			in self.AppendedValueVariablesList

			If _GettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			If _GettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			Else do a __getitem__ in the __dict__
		"""

		#Debug
		#debug("self.GettingVariable is : "+repr(self.GettingVariable))
		#debug("self.GettedVariable is : "+repr(self.GettedVariable))

		#Init a local IsGettedBool
		IsGettedBool=False

		#Get with the AppendedInt in the ordered "dict"
		if type(self.GettingVariable)==int:
			if self.GettingVariable<len(self.AppendedValueVariablesList):

				#Get the GettedVariable from the AppendedValueVariablesList and the Index
				self.GettedVariable=self.AppendedValueVariablesList[self.GettingVariable]

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,OrderShortString])==OrderShortString:

			#Get with the * 
			self.GettingVariable=self.GettingVariable.split(OrderShortString)[1]
			if self.GettingVariable in self.AppendedKeyStringToAppendedIntDict:
				
				#Get the GettedVariable from the AppendedValueVariablesList and the KeyStringToAppendedIntDict
				self.GettedVariable=self.AppendedValueVariablesList[self.AppendedKeyStringToAppendedIntDict[self.GettingVariable]]

				#Stop the getting
				self.IsGettingBool=False

				#Return 
				return
		
		#Get with '/'
		elif self.GettingVariable==DeepShortString:
			
			#If it is directly '/'
			self.GettedVariable=self

			#Stop the getting
			self.IsGettingBool=False

			#Return
			return 

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,DeepShortString])==DeepShortString:

			#Define the GettingVariableStringsList
			GettingVariableStringsList=DeepShortString.join(self.GettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
			GettedVariable=self[GettingVariableStringsList[0]]
			if GettedVariable!=None:

				if len(GettingVariableStringsList)==1:

					#Direct update in the Child or go deeper with the ChildPathString
					self.GettedVariable=GettedVariable

					#Stop the getting
					self.IsGettingBool=False
				
					#Return 
					return 

				else:

					#Define the ChildPathString
					ChildPathString=DeepShortString.join(GettingVariableStringsList[1:])	

					#Get in the first deeper Element with the ChildPathString
					self.GettedVariable=GettedVariable[ChildPathString]

					#Stop the getting
					self.IsGettingBool=False

					#Return
					return 

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,CopyShortString])==CopyShortString:

			#deepcopy
			self.GettedVariable=copy.deepcopy(
					self[CopyShortString.join(self.GettingVariable.split(CopyShortString)[1:])]				
				)

			#Stop the getting
			self.IsGettingBool=False

			#Return 
			return 

		#Do the minimal get
		if IsGettedBool==False:
		
			if type(self.GettingVariable) in [str,unicode]:

				#Get safely the Value
				if self.GettingVariable in self.__dict__:

					#__getitem__ in the __dict__
					self.GettedVariable=self.__dict__[self.GettingVariable]

					#Stop the getting
					self.IsGettingBool=False

				elif self.GettingVariable=="__class__":

					#Return the __dict__ of the __class__
					self.GettedVariable=self.__class__.__dict__

					#Stop the getting
					self.IsGettingBool=False

		#Debug
		#debug("self.GettingVariable is : "+repr(self.GettingVariable))
		#debug("self.GettedVariable is : "+repr(self.GettedVariable))

				#</UnitNotDeepGetHookMethods>
			
				#<MappingNotDeepGetHookMethods>

				#</MappingNotDeepGetHookMethods>

			#</NotDeepGetHookMethods>

			#<DeepGetHookMethods>

				#<UnitDeepGetHookMethods>

	def copyAfter(self):

		#Deepcopy the getting of the self.CopyingVariable
		self.CopiedVariable=copy.deepcopy(self[self.CopyingVariable])

				#<UnitNotDeepGetHookMethods>

				#<MappingDeepGetHookMethods>

				#</MappingDeepGetHookMethods>

			#</DeepGetHookMethods>

		#</GetHookMethods>

	def relayBefore(self):
	
		#Update for the RelayedValueVariables
		map(
			lambda _RelayedValueVariable:
			_RelayedValueVariable.update(self.RelayingVariable) 
			if hasattr(_RelayedValueVariable,'update')
			else None,
			map(
					lambda _RelayedKeyVariable:
					self[_RelayedKeyVariable],
					self.RelayedKeyVariablesList
				)
			)

		#relay for the RelayingValueVariables
		map(
			lambda _RelayingValueVariable:
			_RelayingValueVariable.relay(self.RelayingVariable) 
			if hasattr(_RelayingValueVariable,'relay')
			else None,
			map(
					lambda _RelayingKeyVariable:
					self[_RelayingKeyVariable],
					self.RelayingKeyVariablesList
				)
			)

	def reprAfter(self):
		"""
			Hook in the repr of the Object
			for printing in the Console attributes in a more 
			user-friendly manner
		"""

		#Get the MroAppendedStringsList
		MroAppendedStringsList=SYS.getMroKeyStringsListWithClass(self.__class__)

		#Remove the NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.NotRepresentingAppendedKeyStringsList,
										self.RepresentedTuplesList
								)

		#First keeps only the Specific and New attributes
		self.RepresentedTuplesList=map(
									lambda _RepresentedTuple:
									("<Spe>"+_RepresentedTuple[0],_RepresentedTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] in self.__class__.SpecificKeyStringsList,
											self.RepresentedTuplesList
										)
								)+map(
									lambda _NewTuple:
									("<New>"+_NewTuple[0],_NewTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] not in MroAppendedStringsList,
											self.RepresentedTuplesList
										)
								)

		#Add some forced Values with RepresentingKeyVariables
		self.RepresentedTuplesList+=map(
											lambda _KeyVariable:
											("<NotSpe>"+str(_KeyVariable),self[_KeyVariable]),
											self.RepresentingKeyVariablesList
										)

		#Simplify the numpy variables repr
		self.RepresentedTuplesList=map(
										lambda _RepresentedTuple:
										_RepresentedTuple
										if type(_RepresentedTuple[1]) not in [numpy.ndarray] 
										else (
												_RepresentedTuple[0],
												"<numpy.ndarray shape "+str(numpy.shape(
													_RepresentedTuple[1]))+">"
												),
										self.RepresentedTuplesList
									)

	def itemsAfter(self):
		"""
			Deepest Hook in the items of the Object
		"""

		#Return the zip
		self.CalledPointer=zip(self.AppendedStringsList,self.AppendedValueVariablesList)


	#</DefineHookMethods>
#</DefineClass>



