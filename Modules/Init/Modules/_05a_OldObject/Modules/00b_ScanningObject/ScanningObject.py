#<ImportModules>
import collections
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Scan"
DoneString="Scanned"
DoingString="Scanning"
#</DefineLocals>

#<DefineClass>
class ScanningObjectClass(SYS.ObjectClass):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.ScannedKeyString=""
		self.ScannedInt=-1
		self.ScannedParentPointer=None
		self.ScannedGrandParentPointersList=[]
		self.ScannedPathStringsList=[]
		self.ScannedPathString="/"
		#</DefineSpecificDict>

	def organizeAfter(self,**_OrganizingVariablesDict):

		#Do a walk for parentizing and foldering the grouped children objects
		self.walk(
					[
						'ScannedOrderedDict.items()'
					],
					**{
						'BeforeUpdatingTuplesList':
						[
							('parentize',{'ArgsVariable':"Scan"}),
							('group',{'ArgsVariable':"Scan"})
						]
					}
				)
		return self
	#</DefineHookMethods>

#</DefineClass>

#<DefineAttestingFunctions>
#</DefineAttestingFunctions>
