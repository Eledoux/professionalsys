#<ImportModules>
import collections
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class GrouperClass(SYS.ObjectClass):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.GroupingModuleString="tables"
		self.GroupingPathString=""
		self.GroupedFilePointer=None
		self.GroupedParentPointer=None
		self.GroupedInt=-1
		self.GroupedKeyString=""
		self.GroupedGrandParentPointersList=[]
		self.GroupedPathStringsList=[]
		self.GroupedPathString=""
		#</DefineSpecificDict>

	def group(self,_GroupingString):

		#Debug
		print('group method')

		#Define the DoneString
		DoneString=SYS.getDoneStringWithDoString(_GroupingString)

		#For the uppest Parent maybe init a hdf5 file
		GrandParentPointersList=getattr(self,DoneString+"GrandParentPointersList")
		if len(GrandParentPointersList)>0:
			UppestParentPointer=self.GrandParentPointersList[-1]
		else:
			UppestParentPointer=self

		#Define the GroupedFilePointerKeyString
		GroupedFilePointerKeyString="GroupedFilePointer"

		#Debug
		print('UppestParentPointer.GroupingPathString is ',UppestParentPointer.GroupingPathString)

		#Check for a good GroupingPathString
		if UppestParentPointer.GroupingPathString!="":

			#Check for first write
			if os.path.isfile(UppestParentPointer.GroupingPathString)==False:
				UppestParentPointer.__setattr__(
						GroupedFilePointerKeyString,
						sys.modules[UppestParentPointer.GroupingModuleString].File(
									UppestParentPointer.GroupingPathString,'w')
						).close()

		if getattr(UppestParentPointer,GroupedFilePointerKeyString)==None or ( 
			(UppestParentPointer.GroupingModuleString=='tables' and getattr(UppestParentPointer,GroupedFilePointerKeyString).isopen==0
				) or (UppestParentPointer.GroupingModuleString=='h5py' and getattr(UppestParentPointer,GroupedFilePointerKeyString).mode=='c') ):

			#Open the GroupedFilePointer
			UppestParentPointer.__setattr__(
				GroupedFilePointerKeyString,
				sys.modules[UppestParentPointer.GroupingModuleString].File(
				UppestParentPointer.GroupingPathString,'r+')
				)

		#Point on the FilePointer of the uppest Parent
		self.__setattr__(
							GroupedFilePointerKeyString,
							getattr(UppestParentPointer,GroupedFilePointerKeyString)
						)

		#Get it definitely !
		FilePointer=getattr(GroupedFilePointerKeyString)

		#Create a group in the hdf5 file
		if FilePointer!=None:

			#Define the PathStringKeyString
			PathStringKeyString=DoneString+"PathString"

			#Make sure that the first char is /
			if getattr(self,PathStringKeyString)[0]!="/":
				self.__setattr__(
								PathStringKeyString,
								getattr(self,PathStringKeyString)
								)

			#Get the PathString
			PathString=getattr(self,PathStringKeyString)

			#Check if the Path exists
			if PathString not in FilePointer:

				#Set all the intermediate Paths before
				PathStringsList=PathString.split('/')[1:]
				ParsingChildPathString="/"

				#Set the PathString from the top to the down (integrativ loop)
				for ChildPathString in PathStringsList:

					#Go deeper
					NewParsingChildPathString=ParsingChildPathString+ChildPathString

					#Create the group if not already
					if NewParsingChildPathString not in FilePointer:
						if self.GroupingModuleString=="tables":
							FilePointer.create_group(ParsingChildPathString,ChildPathString)
						elif self.GroupingModuleString=="h5py":
							Group=FilePointer[ParsingChildPathString]
							Group.create_group(ChildPathString)
					
					#Prepare the next group	
					ParsingChildPathString=NewParsingChildPathString+'/'
	#</DefineHookMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_group():

	#Build Hdf groups
	Grouper=SYS.GrouperClass().organize().update(
								[
									(
										'App_Structure_ChildGrouper1',
										SYS.GrouperClass().update(
										[
											('App_Structure_GrandChildGrouper1',
											SYS.GrouperClass())
										])
									),
									(
										'App_Structure_ChildGrouper2',
										SYS.GrouperClass().update(
											[]
										)
									)
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':"Structure"})
												]
											}
									).close()

	#Get the h5ls version of the stored hdf
	return os.popen('h5ls -dlr '+Grouper.GroupingPathString).read()
#</DefineAttestingFunctions>
