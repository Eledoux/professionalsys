#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
from tables import *
import time
import operator
import os
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class GriderClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.GridingTypeString="Gridion"
		self.GridingTuplesList=[]
		self.IsGridingBool=True
		#</DefineSpecificDict>
						
	def gridAfter(self):
		
		'''
		print('')
		print('Grid begin for ',self.GroupedKeyString)
		'''

		ScannedTuplesList=SYS.getScannedTuplesListWithScanningListsList(
										SYS.unzip(
													self.GridingTuplesList,
													[2]
												)
											)

		ScannedUpdatingTuplesList=map(
								lambda __ScannedTuple:
								zip(
										SYS.unzip(
													self.GridingTuplesList,
													[1]
												),
										__ScannedTuple
									),
								ScannedTuplesList
								)

		'''
		print('scan',self.GroupedKeyString)
		print('ScannedUpdatingTuplesList is :',ScannedUpdatingTuplesList)
		print('For creating appended scanned objects')
		print('')
		'''

		ScannedKeyStringsList=map(
									lambda __ScannedUpdatingTuple:
									'_'.join(
												map(
														lambda __ScannedUpdatingItemTuple:
														'('+str(__ScannedUpdatingItemTuple[0]
															)+','+str(__ScannedUpdatingItemTuple[1])+')',
														__ScannedUpdatingTuple
													)
											),
									ScannedUpdatingTuplesList
								)

		'''
		print('scan',self.GroupedKeyString)
		print('ScannedKeyStringsList are :',ScannedKeyStringsList)
		print('add them now')
		print('')
		'''

		#Define the classs to be grinded
		Class=getattr(SYS,SYS.getClassStringWithTypeString(self.GridedTypeString))

		if Class!=None:
			self.__add__(
							map(
								lambda __ScannedKeyString,__ScannedUpdatingTuple:
								Class().update(
									[
										('GridedKeyString',__ScannedKeyString)
									]+
									__ScannedUpdatingTuple
								),
								ScannedKeyStringsList,
								ScannedUpdatingTuplesList	
							)
						)

			'''
			print('scan',self.GroupedKeyString)
			print('Ok they are added now')
			#print(self)
			'''

		else:

			'''
			print('Grid, no such Class...with ',self.TypeString)
			print('')
			'''
			pass

		#Return self
		return self

	#</DefineHookMethods>
	
	#<DefineMethods>
	def grid(self,**_GridingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Debug
		'''
		print('Outputer output method')
		print('')
		'''

		#Refresh the attributes and set maybe default values in the _GridingVariablesDict
		LocalGridingVariablesDict=_GridingVariablesDict
		LocalOutputedPointer=self
		self.IsGridingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='output'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalGridingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalGridingVariablesDict' in OutputVariable:
							LocalGridingVariablesDict=OutputVariable['LocalGridingVariablesDict']
						if 'LocalOutputedPointer' in OutputVariable:
							LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

					#Check Bool
					if self.IsGridingBool==False:
						return LocalOutputedPointer

		#Return the OutputVariable
		return LocalOutputedPointer
	#</DefineMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.GriderClass()

def attest_grid():
	Grider=SYS.GriderClass().update([
									('GridingTypeString','Vector'),
									('GridingTuplesList',[
																('UnitsInt','UnitsInt',[1,2])
															])
								]).grid()

#</DefineAttestingFunctions>
