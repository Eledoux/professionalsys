#<ImportModules>
import collections
import copy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Tablor"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class RowerClass(
					BasedLocalClass,
				):
	
	#<DefineHookMethods>
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.RowedVariablePointer=self 									#<NotRepresented>
		self.RowedNotIdentifiedGettingStringsList=[]					#<NotRepresented>
		self.RowedIdentifiedGettingStringsList=[]						#<NotRepresented>
		self.RowedIdentifiedOrderedDict=collections.OrderedDict() 		#<NotRepresented>
		self.RowedNotIdentifiedOrderedDict=collections.OrderedDict() 	#<NotRepresented>
		#</DefineSpecificDict>
		

	@SYS.HookerClass(**{'AfterTuplesList':[("<TypeString>","table")]})
	def row(self,**_VariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Debug
		self.debug('Start of the method')
			
		#Split the GettingStrings between the ones that identify and the other that not
		self.RowedNotIdentifiedGettingStringsList=filter(
													lambda __GettingString:
													__GettingString not in self.RowedIdentifiedGettingStringsList,
													SYS.unzip(
														self.ModelingColumnTuplesList,[0]
														)
												)

		self.RowedIdentifiedOrderedDict.update(
					zip(
						self.RowedIdentifiedGettingStringsList,
						self.RowedVariablePointer.pick(self.RowedIdentifiedGettingStringsList)
						)
				)

		self.RowedNotIdentifiedOrderedDict.update(
					zip(
						self.RowedNotIdentifiedGettingStringsList,
						self.RowedVariablePointer.pick(self.RowedNotIdentifiedGettingStringsList)
						)
				)

		#Debug
		self.debug('End of the method')
		
#</DefineClass>

#<DefineAttestingFunctions>
def attest_row():
	Rower=SYS.RowerClass().update(								
									[
										('MyInt',0),
										('MyString',"hello"),
										('MyIntsList',[2,4,1]),
										('ModelingColumnTuplesList',
											[
												('MyInt',tables.Int64Col()),
												('MyString',tables.StringCol(10)),
												('MyIntsList',(tables.Int64Col(shape=3)))
											]
										),
										('RowedIdentifiedGettingStringsList',['MyInt','MyString'])
									]).row().close()
																
	#Get the h5ls version of the stored hdf
	return SYS.represent(
						Rower
						)+'\n\n\n\n'+Rower.hdfview().HdformatedString	
#</DefineAttestingFunctions>
