#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>


Object=SYS.ObjectClass().__setitem__(
										'TestingAttestFunctionStringsList',
										[
											'attest_default', 
											'attest_getInNotOrderedDict',
											'attest_getInOrderedDict', 
											'attest_getWithAttributeString',
											'attest_setInNotOrderedDict', 
											'attest_setInOrderedDict', 
											'attest_setWithMethodString',
											'attest_hook',
											'attest_apply',
											'attest_pick',
											'attest_gather',
											'attest_collect',
											'attest_update',
											'attest_append',
											'attest_add',
											'attest_commandAllSetsForEach',
											'attest_commandEachSetForAll',
											'attest_walk',
											'attest_parentize',
											'attest_group'
										]
									)
Object.attest()
Object.test()


Module=SYS.ModuleClass().__setitem__(
										'TestingAttestFunctionStringsList',
										[
											'attest_default', 
											'attest_feature',
											'attest_output'
										]
									)
Module.attest()
Module.test()
