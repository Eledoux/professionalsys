#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>


#<DefineClass>
class UsedObjectClass(
						SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.UsingPointer=None
		#</DefineSpecificDict>

	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>


#</DefineClass>

