#<ImportModules>
import copy
import inspect
import ShareYourSystem as SYS
import numpy
import operator
import os
import sys
from tables import *
#</ImportModules>

"""
 coucou erwan, ce message est d'une grande importance... 
 "mmmhhh!! noh oui oh oui!! oooohh!!!"" 
 c'est la reponse a ttes tes annees de recherches intensives en neuroscience!! 
 Tu peux me remercier!!!!
"""

#<DefineLocals>
AddShortString="+"
AttributeShortString="."
DeepShortString="/"
AppendShortString="App_"
PointerShortString="Pointer"
CopyShortString='*'
CollectShortString=":"
ExecShortString="Exec_"
TabularShortString="Tab_"
#</DefineLocals>

def debug(_String):
	Frame=inspect.currentframe().f_back
	ScriptPathStringsList=SYS.getWordStringsListWithString(Frame.f_code.co_filename)
	ScriptPathString=''.join(ScriptPathStringsList)
	print("%s , %s, l. %s : "%(ScriptPathString,Frame.f_code.co_name,Frame.f_lineno));
	print(_String)
	print("")

#<DefineClass>
class ObjectClass(object):
	
	#<DefineMethods>

		#<HandlingMethods>
	
			#<InitMethod>
	def __init__(self):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""

		#<DefineSpecificDict>

		#Attributes for the __init__ property
		self.IsInitiatingBool=True

		#Attributes for the __getitem__ property
		self.IsGettingBool=True
		self.GettingVariable=None
		self.GettedVariable=None

		#Attributes for the __setitem__ property
		self.IsSettingBool=True
		self.SettingVariable=None
		self.SettedVariable=None
		
		#Attributes for the apply property
		self.IsApplyingBool=True
		self.MappedAppliedVariablesList=[]

		#Attributes for the order property
		self.MappedOrderedVariablesList=[]

		#Attributes for the append property
		self.AppendedKeyStringsList=[]
		self.AppendedValueVariablesList=[]
		self.AppendedKeyStringToAppendedIntDict={}

		#Attributes for the group property
		self.GroupingFilePathString=""
		self.GroupingModuleString="tables"
		self.GroupingFilePathString=""
		self.GroupedFilePointer=None
		self.GroupedObjectsDict={}
		self.GroupedKeyString=""
		self.GroupedParentPointer=None
		self.GroupedGrandParentPointersList=[]
		self.GroupedKeyStringsList=[]
		self.GroupedPathStringsList=[]
		self.GroupedPathString="/"

		#Attributes for the join property
		self.JoinedTuplesList=[]

		#Attributes for the table property
		self.TabularingDictsList=[]
		self.TabularedTypeStringToTabularedIntDict={}
		self.TabularedTypeStringsList=[]
		self.TabularedDictsList=[]

		#Attributes for the row property
		self.RowingDict={}

		#Attributes for the model property
		self.ModeledJoiningTuplesList=[]
		self.ModeledInt=-1

		#Attributes for the use property
		self.UsedInt=-1

		#Attributes for the structure property
		self.StructuredInt=-1
		self.StructuredKeyString=""
		self.StructuredParentPointer=None
		self.StructuredGrandParentPointersList=[]
		self.StructuredObjectsTuplesList=[]

		#Attributes for the __repr__ property
		self.IsRepresentingBool=True
		self.RepresentingDict={}
		self.RepresentedTuplesList=[]
		self.NotRepresentingGettingVariablesList=filter(
													lambda _KeyString:
													_KeyString not in [
																		'AppendedKeyStringsList',
																		'AppendedValueVariablesList',
																		'StructuredInt',
																		'StructuredKeyString',
																		'StructuredGrandParentPointersList',
																		'GroupingHdfFile',
																		'GroupingHdfFilePointer',
																		'GroupedKeyString',
																		'GroupedGrandParentPointersList',
																		'GroupedPathString'
																	],
													self.__class__.SpecificKeyStringsList
										) if type(self)==SYS.ObjectClass else []

		self.RepresentingKeyVariablesList=[
											'AppendedKeyStringsList',
											'AppendedValueVariablesList',
											'StructuredInt',
											'StructuredKeyString',
											'StructuredGrandParentPointersList',
											'GroupingHdfFile',
											'GroupingHdfFilePointer',
											'GroupedKeyString',
											'GroupedGrandParentPointersList',
											'GroupedPathString'
											] if type(self)!=SYS.ObjectClass else []

		#</DefineSpecificDict>

		#Hook methods (integrativ)
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="init"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsInitiatingBool==False:
						return 

					#Call the HookingMethod
					HookingMethod(self)

			#</InitMethod>

			#<ExecuteMethod>

	def execute(self,_ExecString):

		exec _ExecString in locals()
		return self

			#</ExecuteMethod>

			#<CallMethod>

	def __call__(self,_CallVariable,*_CallingVariablesList):
		"""Call the <_CallString><HookString> methods and return self.CalledVariable (self by default)"""

		#Refresh the attributes
		LocalCallVariable=_CallVariable
		LocalCallingVariablesList=_CallingVariablesList
		LocalCalledPointer=self

		#self.CalledPointer=self
		self.IsCallingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString=LocalCallVariable+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,LocalCallVariable,LocalCallingVariablesList)

					if type(OutputVariable)==dict:
						if 'LocalCallVariable' in OutputVariable:
							LocalCallVariable=OutputVariable['LocalCallVariable']
						if 'LocalCallingVariablesList' in OutputVariable:
							LocalCallingVariable=OutputVariable['LocalCallingVariablesList']
						if 'LocalCallPointer' in OutputVariable:
							LocalCallPointer=OutputVariable['LocalCallPointer']

					#Check Bool
					if self.IsCallingBool==False:
						return LocalCalledPointer

		#Return the OutputVariable
		return LocalCalledPointer

			#</CallMethod>

			#<GetMethod>

	def __getitem__(self,_GettingVariable):
		"""Call the get<HookString> methods and return self.GettedVariable (None by default)"""
		
		#Refresh the attributes
		self.GettingVariable=_GettingVariable
		self.GettedVariable=None
		self.IsGettingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="get"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Debug
					#debug("HookingMethodString is : "+HookingMethodString)
					#debug("self.GettedVariable is : "+repr(self.GettedVariable))

					#Check Bool
					if self.IsGettingBool==False:
						return self.GettedVariable

		#Return GettedVariable
		return self.GettedVariable

		#</GetMethod>

		#<SetMethods>

			#<GlobalMethod>

	def __setitem__(self,_SettingVariable,_SettedVariable):
		"""Call the set<HookString> methods"""

		#Refresh the attributes
		self.SettingVariable=_SettingVariable
		self.SettedVariable=_SettedVariable
		self.IsSettingBool=True

		#Sort the HookMethodStrings
		for OrderString in ["Before","After"]:

			#Define a Local IsSettedBool
			IsSettedBool=False

			if type(_SettingVariable) in [str,unicode]:
			
				#Define the BindingMethodString
				BindingMethodString="bind"+_SettingVariable+OrderString
				
				#Check that there is HookingMethods for it
				if BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict:
					
					#Say ok
					IsSettedBool=True

					#Call the specific Appended methods 
					for HookingMethod,BindingMethod in self.__class__.BindingMethodStringToMethodTuplesListDict[BindingMethodString]:

						#Call the HookMethod
						if callable(HookingMethod):
							HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

						#Call the Binding
						if callable(BindingMethod):
							OutputVariable=BindingMethod(self)

							#End the Setting
							if self.IsSettingBool==False:
								return self

			#If a binding method was not yet found then look for a global set method
			if IsSettedBool==False:

				#_SettingVariable is not a string
				HookingMethodString="set"+OrderString

				#Check that there is HookingMethods for it
				if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

					#Call the specific Appended methods 
					for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

						#Call the HookingMethod
						HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

		#Return self
		return self

			#</GlobalMethod>

			#<AppendMethod>

	def append(self,_AppendingVariable):
		"""Call __setitem__ with a defined SettingVariable contained in the _AppendingVariable"""
		
		#Define the SettingVariable
		SettingVariable="App_"
		if SYS.getIsTuplesListBool(_AppendingVariable) and _AppendingVariable[0][0]=='StructuredKeyString':
				SettingVariable+=_AppendingVariable[0][1]
		elif self.__class__ in type(_AppendingVariable).__mro__:
			SettingVariable+=_AppendingVariable.StructuredKeyString
		elif type(_AppendingVariable)==dict and 'StructuredKeyString' in _AppendingVariable:
			SettingVariable+=_AppendingVariable['StructuredKeyString']

		#Then set
		return self.__setitem__(SettingVariable,_AppendingVariable)

			#</AppendMethod>

		#</SetMethods>

		#<DelMethod>

	def __delitem__(self,_DeletingVariable):
		"""Call the del<HookString> methods and delete in the __dict__ by default"""
		
		#Refresh the attributes
		self.DeletingVariable=_DeletingVariable
		self.IsDeletingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="delete"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsDeletingBool==False:
						return self

		#Return self
		return self

		#</DelMethod>

	#</HandlingMethods>

	#<MappingMethods>

		#<GlobalMethod>

	def map(self,_MappingFunction,*_MappedList):
		"""Just map"""
		map(_MappingFunction,*_MappedList)
		return self
		#</GlobalMethod>

		#<ApplyingMethods>

			#<GenericMethod>

	def apply(self,_MethodString,_AppliedVariablesList):
		"""Map a call with the _MethodString to the _AppliedVariablesList"""

		#Get the AppliedMethod
		if hasattr(self,_MethodString):
			self.MappedAppliedVariablesList=map(
					lambda __AppliedVariable:
					SYS.getWithMethodAndArgsVariable(getattr(self,_MethodString),__AppliedVariable)
					if hasattr(self,_MethodString)
					else None,
					_AppliedVariablesList
				)

		#Return self
		return self

			#</GenericMethod>

			#<GetMethods>

				#<PickMethod>

	def pick(self,_GettingVariablesList):
		"""Apply the __getitem__ to the <_GettingVariablesList>"""

		#Apply __getitem__
		self.apply('__getitem__',_GettingVariablesList)

		#Return AppliedVariablesList
		return self.MappedAppliedVariablesList

				#</PickMethod>

				#<CollectMethod>

	def collect(self,_CollectingVariablesList):
		"""Reduce a map of pick for CollectingVariable being a List or __getitem__ for CollectingVariable being a GettingVariable"""
		return reduce(operator.__add__,map(
												lambda __CollectingVariable:
												self.pick(__CollectingVariable) 
												if type(__CollectingVariable)==list
												else self[__CollectingVariable],
												_CollectingVariablesList
										)
					)


				#<CollectMethod>

			#</GetMethods>

			#<SetMethods>

				#<GlobalMethod>

	def update(self,_SettingVariableTuplesList):
		"""Apply the __setitem__ to the <_SettingVariableTuplesList>"""

		#Apply __setitem__
		return self.apply('__setitem__',_SettingVariableTuplesList)

				#</GlobalMethod>

				#<AppendMethod>

	def __add__(self,_AddingVariablesList):
		"""Apply the append to the <_AddingVariablesList>"""

		#Apply
		self.apply('append',_AddingVariablesList)

		#Return 
		return self

				#</AppendMethod>

			#</SetMethods>

		#</ApplyingMethods>

		#<CommandingMethods>

			#<AllSetsForEachMethod>

	def commandAllSetsForEach(self,_SettingVariableTuplesList,_CollectingVariablesList):
		"""In each _OrderedVariable of _OrderedVariablesList there is an update with _SettingVariableTuplesList"""

		#Get the CollectedVariablesList
		CollectedVariablesList=self.collect(_CollectingVariablesList)

		#For each __CollectedVariable it is updating with _SettingVariableTuplesList
		self.MappedOrderedVariablesList=map(
				lambda __CollectedVariable:
				__CollectedVariable.update(_SettingVariableTuplesList),
				CollectedVariablesList
			)

		#Return self
		return self 

			#</AllSetsForEachMethod>

			#<EachSetForAllMethod>

	def commandEachSetForAll(self,_SettingVariableTuplesList,_CollectingVariablesList):
		"""With each _SettingTuple of _SettingVariableTuplesList there is a set in _OrderedVariablesList"""

		#Get the CollectedVariablesList
		CollectedVariablesList=self.collect(_CollectingVariablesList)

		#For each SettingTuple it is setted in _OrderedVariablesList
		self.MappedOrderedVariablesList=map(
				lambda __SettingVariableTuple:
				map(
					lambda __CollectedVariable:
					__CollectedVariable.__setitem__(*__SettingVariableTuple),
					CollectedVariablesList
					),
				_SettingVariableTuplesList
			)

		#Return self
		return self 

			#</EachSetForAllMethod>

		#</CommandingMethods>

		#<OrderingMethods>

			#<CoOrderMethod>

	def coOrder(self,_OrderingStringsTuple,_OrderingCollectingVariablesList,_CommandingTuple):
		"""From top to down it calls a Command at the level of self with a _CommandingTuple and a recursive order call to _OrderingCollectingVariablesList"""

		#Get the OrderingCollectedVariablesList
		OrderingCollectedVariablesList=self.collect(_OrderingCollectingVariablesList)

		#Map an Order/Command given the _OrderingStringsTuple
		map(
				lambda __OrderingString:
				#This is the coOrder
				map(
						lambda __OrderingCollectedVariable:
						getattr(
									__OrderingCollectedVariable,
									__OrderingString
								)(_OrderingStringsTuple,_OrderingCollectingVariablesList,_CommandingTuple),
						OrderingCollectedVariablesList
					) if 'Order' in __OrderingString
				else
				#This is the command
				getattr(self,__OrderingString)(*_CommandingTuple),	
				_OrderingStringsTuple
			)

		#Return self
		return self 

			#</CoOrderMethod>

			#<TransOrderMethod>

	def transOrder(self,_OrderingStringsTuple,_OrderingCollectingVariablesList,_CommandingTuple):
		"""From top to down it calls a Command at the level of self with a _CommandingTuple and a recursive order call to _OrderingCollectingVariablesList"""

		#Map an Order/Command given the _OrderingStringsTuple
		for OrderingString in _OrderingStringsTuple:

			if 'Order' in OrderingString:

				#Get the OrderingCollectedVariablesList
				OrderingCollectedVariablesList=self.collect(_OrderingCollectingVariablesList)

				#Map a recursive call
				map(
						lambda __OrderingCollectedVariable:
						map(	
								lambda __OrderingString:
								#This is the coOrder
								getattr(
											__OrderingCollectedVariable,
											__OrderingString
										)(_OrderingStringsTuple,_OrderingCollectingVariablesList,_CommandingTuple)
							),
						OrderingCollectedVariablesList
					)

			else:

				#This is the command
				getattr(self,__OrderingString)(*_CommandingTuple)

		#Return self
		return self 

			#</TransOrderMethod>

		#</OrderingMethods>

	#</MappingMethods>
	
	#<OrganizingMethods>

		#<ParentizingMethods>

	def parentize(self,_ParentingString):

		#Define the ParentPointerKeyString
		ParentPointerKeyString=_ParentingString+"ParentPointer"
		if getattr(self,ParentPointerKeyString)!=None:

			#Set the GrandParentPointersList
			GrandParentPointersListKeyString=_ParentingString+"GrandParentPointersList"
			setattr(
						self,
						GrandParentPointersListKeyString,
						[getattr(self,ParentPointerKeyString)]+getattr(
															getattr(self,ParentPointerKeyString),
															GrandParentPointersListKeyString
															)
					)

			#Set the KeyStringsList
			KeyStringKeyString=_ParentingString+"KeyString"
			KeyStringsListKeyString=_ParentingString+"KeyStringsList"
			self.__setattr__(
								KeyStringsListKeyString,
								map(
										lambda __GrandParentPointer:
										getattr(__GrandParentPointer,KeyStringKeyString),
										getattr(self,GrandParentPointersListKeyString)
									)
						)

			#Set the PathStringsList
			PathStringsListKeyString=_ParentingString+"PathStringsList"
			self.__setattr__(
								PathStringsListKeyString,
								[getattr(self,KeyStringKeyString)]+copy.copy(
									getattr(self,KeyStringsListKeyString)
								)
							)
			getattr(self,PathStringsListKeyString).reverse()

			#Set the PathString
			PathStringKeyString=_ParentingString+"PathString"
			self.__setattr__(
								PathStringKeyString,
								DeepShortString.join(getattr(self,PathStringsListKeyString))
							)

		#</ParentizingMethods>

		#<GroupingMethods>

	def group(self):

		#For the uppest Parent maybe init a hdf5 file
		if len(self.GroupedGrandParentPointersList)>0:
			UppestGroup=self.GroupedGrandParentPointersList[-1]
		else:
			UppestGroup=self

		#Check for a good FilePathString
		if UppestGroup.GroupingFilePathString!="":

			#Check for first write
			if os.path.isfile(UppestGroup.GroupingFilePathString)==False:
				UppestGroup.GroupedFilePointer=sys.modules[UppestGroup.GroupingModuleString].File(
					UppestGroup.GroupingFilePathString,'w')
				UppestGroup.GroupedFilePointer.close()

		if UppestGroup.GroupedFilePointer==None or ( (UppestGroup.GroupingModuleString=='tables' and UppestGroup.GroupedFilePointer.isopen==0) or (UppestGroup.GroupingModuleString=='h5py' and UppestGroup.GroupedFilePointer.mode=='c') ):

			#Open the GroupedFilePointer
			UppestGroup.GroupedFilePointer=sys.modules[UppestGroup.GroupingModuleString].File(
				UppestGroup.GroupingFilePathString,'r+')

		#Point on the GroupingFilePointer of the uppest Parent
		self.GroupedFilePointer=UppestGroup.GroupedFilePointer

		#Create a group in the hdf5 file
		if self.GroupedFilePointer!=None:

			#Make sure that the first char is /
			if self.GroupedPathString[0]!="/":
				self.GroupedPathString="/"+self.GroupedPathString

			#Check if the Path exists
			if self.GroupedPathString not in self.GroupedFilePointer:

				#Set all the intermediate Paths before
				GroupPathStringsList=self.GroupedPathString.split('/')[1:]
				ParsingGroupPathString="/"

				#Set the PathString from the top to the down (integrativ loop)
				for GroupPathString in GroupPathStringsList:

					#Go deeper
					NewParsingGroupPathString=ParsingGroupPathString+GroupPathString

					#Create the group if not already
					if NewParsingGroupPathString not in self.GroupedFilePointer:
						if self.GroupingModuleString=="tables":
							self.GroupedFilePointer.create_group(ParsingGroupPathString,GroupPathString)
						elif self.GroupingModuleString=="h5py":
							Group=self.GroupedFilePointer[ParsingGroupPathString]
							Group.create_group(GroupPathString)
					
					#Prepare the next group	
					ParsingGroupPathString=NewParsingGroupPathString+'/'

		#</GroupingMethod>

		#<TabularingMethods>

	def model(self):

		#Define the class
		class ModelClass(IsDescription):

			#Add an tagged Int (just like a unique KEY in mysql...) 
			locals().__setitem__(
							SYS.getTabularedIntKeyStringWithTabularingTypeString(
								_TabularingDict['TabularingTypeString']
							),
							Int64Col()
						)

			#Set the Col objects in this class... for is necessary for using the locals()...
			for ColumningString,Col in zip(*self.ModelingTuplesList):
								*SYS.collect(
									_TabularingDict,
									map(
										lambda __ParamString:
										__ParamString+'TuplesList',
										[
											'Featuring',
											'Outputing',
											'Storing'
										]
									),
									[0,2]
								)
							):

				#Set the Col 
				locals().__setitem__(ColumningString,Col)

			#Del ColumningString,ColClass
			try:
				del ColumningString,Col
			except:
				pass

		#Give a name of this local defined class
		LocalClass.__name__=SYS.getClassStringWithTypeString(_TabularingDict['TabularingTypeString'])

		#Append  in a new copying TabularedDict
		TabularedDict=copy.copy(_TabularingDict)

		#Change the ing in ed 
		map(
			lambda __KeyStringTuple:
			SYS.renameWithDict(TabularedDict,*map(lambda __KeyString:__KeyString+'TuplesList',__KeyStringTuple)),
			zip(
				*map(
						lambda __SuffixString:
						map(getattr(SYS,'getHook'+__SuffixString+'StringWithHookString'),['Feature','Output','Store']),
						['ing','ed']
					)
				)
			)
		self.TabularedTypeStringsList.append(_TabularingDict['TabularingTypeString'])
		self.TabularedDictsList.append(dict(TabularedDict,**{'TabularedClass':LocalClass}))

		#Set in the TabularedDict
		self.TabularedTypeStringToTabularedIntDict[_TabularingDict['TabularingTypeString']]=len(self.TabularedDictsList)-1
			
	def setTableWithTabularedDict(self,_TabularedDict):

		#Define a short alias for the class
		TabularedClass=_TabularedDict['TabularedClass']

		#Init the table if it is not already
		TableTypeString=SYS.getTypeStringWithClassString(TabularedClass.__name__)
		TabularedPathString='/'+TableTypeString
		TableTypeString=SYS.getTypeStringWithClassString(TabularedClass.__name__)
		if TabularedPathString not in self.GroupedFilePointer:

			#Build the Table in the self.TabularedDict
			_TabularedDict['Table']=self.GroupedFilePointer.create_table(
									self.GroupedFilePointer.getNode(self.GroupedPathString),
									TableTypeString,
									TabularedClass,
									TabularedClass.__doc__ if TabularedClass.__doc__!=None else "This is the "+TabularedClass.__name__
									)
		else:

			#Get the Node and set it to the TabularedDict
			_TabularedDict['Table']=self.GroupedFilePointer.getNode(TabularedPathString)

	
	def table(self):

		#Set all the Tables
		map(
				lambda __TabularedDict:
				self.setTableWithTabularedDict(__TabularedDict),
				self.TabularedDictsList
			)
		
		#</TabularingMethods>

		#<RowingMethods>
	def row(self,_RowingTable,_RowingFeaturingTuplesList=[],_RowingOutputingTuplesList=[]):

		'''
		print(map(	
				lambda __Row:
						map(
						lambda __RowingTuple:
						(type(__Row[__RowingTuple[0]]),__RowingTuple[1]),					
						_RowingTuplesList
						),
				Table.iterrows()
				)
			)
		'''		

		print("kkk",_RowingFeaturingTuplesList,_RowingOutputingTuplesList)
		if _RowingTable!=None:

			#Check if it is already appended
			IsAlreadyAppendedBool=any(
				map(	
					lambda __Row:
					all(
							map(
								lambda __RowingFeaturingTuple:
								SYS.getIsEqualBool(
									__Row[__RowingFeaturingTuple[0]],
									__RowingFeaturingTuple[1]
								),					
								_RowingFeaturingTuplesList
							)
						),
					_RowingTable.iterrows()
					)
				)

			if IsAlreadyAppendedBool==False:

				#Short alias for the row
				Row=_RowingTable.row

				#Set the key int

				Row.__setitem__(
						SYS.getTabularedIntKeyStringWithTabularingTypeString(
									_RowingTable.name
								),
						_RowingTable.nrows
					)

				#Set special items
				map(
						lambda __FeaturingTuplesList:
						map(
								lambda __RowingTuple:
								Row.__setitem__(*__RowingTuple),
								__FeaturingTuplesList
							),
						[_RowingFeaturingTuplesList,_RowingOutputingTuplesList]
					)

				#Look for dataset
				'''
				.map(	
					lambda __GrindedArrayingColumningString,__GrindedArrayingPickedVariable:
					self.GroupedFilePointer.createArray(
						self.GroupedFilePointer.getNode(self.GroupedPathString),
						str(self[GrindedIntKeyString])+__GrindedArrayingColumningString,
						numpy.array(__GrindedArrayingPickedVariable)
						) if self.GroupedPathString+str(self[GrindedIntKeyString])+__GrindedArrayingColumningString not in self.GroupedFilePointer
					else None,
					*(GrindedArrayingColumningStringsList,
						self.pick(GrindedArrayingGettingStringsList))
				'''

				#Append
				Row.append()

				#Flush
				_RowingTable.flush()

		#</RowingMethods>

		#<ScanningMethods>

	def scan(self,_ScanningTable,_FeaturingTuplesList):

		map(
				lambda __RowingTuplesList:
				self.row(
							_ScanningTable,
							zip(SYS.pick(_FeaturingTuplesList,[1]),__RowingTuplesList)
					),
				SYS.getScannedTuplesListWithScanningListsList(SYS.pick(_FeaturingTuplesList,[3]))
			)

		#</ScanningMethods>

		#<ModelingMethods>

	def model(self):

		#Scan for all the modeling tables
		self.apply('scan',map(
								lambda __TabularedDict:
								map(__TabularedDict.__getitem__,['Table','FeaturedTuplesList']),
								self.pick(
										map(
												lambda __ModelingTypeString:
												'Tab_'+__ModelingTypeString,
												filter(
														lambda __TabularedTypeString:
														SYS.getCommonSuffixStringWithStringsList(['ModelTable',__TabularedTypeString])=='ModelTable',
														self.TabularedTypeStringsList
												)
											)
										)
								)
					)

		#</ModelingMethods>

		#<JoiningMethod>

	def join(self,_JoinString):
		
		#Define the JoiningTableTypeString
		JoinedTableSuffixString=_JoinString+'Table'
		JoinedHookedString=SYS.getHookedStringWithHookString(_JoinString)
		JoinedIntSuffixString=JoinedHookedString+'Int'

		#Pick all the joined tabulared Dict
		TabularedDictsListKeyString=JoinedHookedString+'JoiningTabularedDictsList'
		self.__setattr__(TabularedDictsListKeyString,
							self.pick(
								map(
										lambda __ModelingTypeString:
										'Tab_'+__ModelingTypeString,
										filter(
												lambda __TabularedTypeString:
												SYS.getCommonSuffixStringWithStringsList(
													[JoinedTableSuffixString,__TabularedTypeString])==JoinedTableSuffixString,
												self.TabularedTypeStringsList
										)
									)
							)
					)



		#Pick the Columning and Getting Strings
		#ColumningAndGettingTuplesListsListKeyString=JoinedHookedString+'JoiningListsList'
		#self.__setattr__(
		#				ColumningAndGettingTuplesListsListKeyString,
		ColumningStringsListAndGettingStringsListAndTableTuplesList=map(
								lambda __ModelingTabularedDict:
								map(
									lambda __ParamString:
									SYS.collect(
										__ModelingTabularedDict,
										map(
												lambda __ParamString:
												__ParamString+'TuplesList',
												[__ParamString]
											),
										[0,1]
									),
									['Featured']
								)+[__ModelingTabularedDict['Table']],
								getattr(self,TabularedDictsListKeyString)
						)

		
		#Set the corresponding JoiningTuplesList
		IntStringAndIntsListTuplesList=map(
					lambda __JoinedIntKeyString,__JoinedTabularedDictTuple:
					(
						__JoinedIntKeyString,
						map(
							lambda __Row:
							__Row[__JoinedIntKeyString],
							__JoinedTabularedDictTuple['Table'].iterrows()
							)
					),
					map(
						lambda __JoinedTabularedDict:
						filter(
								lambda __ColumnString:
								SYS.getCommonSuffixStringWithStringsList(
									[__ColumnString,JoinedIntSuffixString])==JoinedIntSuffixString,
								 __JoinedTabularedDict['Table'].colnames
							)[0],
						getattr(self,TabularedDictsListKeyString)
					),
					getattr(self,TabularedDictsListKeyString)
				)


		self.__setattr__(
							JoinedHookedString+'JoiningTabularedIntColumnStringsList',
							SYS.pick(IntStringAndIntsListTuplesList,[0])
						)
		self.__setattr__(
							JoinedHookedString+'JoiningTabularedIntsTuplesList',
							SYS.getScannedTuplesListWithScanningListsList(
									SYS.pick(IntStringAndIntsListTuplesList,[1])
								)
						)

		

		self[JoinedHookedString+'JoiningTuplesList']=zip(
															IntStringAndIntsListTuplesList,
															ColumningStringsListAndGettingStringsListAndTableTuplesList
														)
		#</JoiningMethod>


		#<GrindingMethods>
		
	def grind(self):

		#Check that the GrindingTable exists
		GrindingTableTypeString=self.__class__.TypeString+'GrindTable'
		GrindedTabularedDict=self['Tab_'+GrindingTableTypeString]
		GrindedIntKeyString=SYS.getTabularedIntKeyStringWithTabularingTypeString(GrindingTableTypeString)
		if GrindedTabularedDict!=None:
			
			#Map the tables and FeaturedGettingStringsList...
			ModeledTablesList=map(
									lambda __TabularedDict:
									__TabularedDict['Table'],
									self.ModeledJoiningTabularedDictsList
								)
			ModeledFeaturedGettingStringsListsList=map(
									lambda __TabularedDict:
									SYS.pick(__TabularedDict['FeaturedTuplesList'],[1]),
									self.ModeledJoiningTabularedDictsList
								)

			#...In order to get the UpdatingTuplesListsList to be grinded !
			print("aaa",self.ModeledJoiningTabularedIntColumnStringsList)
			ModeledJoiningFeaturingTuplesListsList=map(
					lambda __TabularedIntsTuple:
					map(
							lambda __ColumnString,__TabularedInt:
							(__ColumnString,__TabularedInt),
							self.ModeledJoiningTabularedIntColumnStringsList,
							__TabularedIntsTuple
						),
					self.ModeledJoiningTabularedIntsTuplesList,
				)
			print("iii",ModeledJoiningFeaturingTuplesListsList)

			#...In order to get the UpdatingTuplesListsList to be grinded !
			UpdatingTuplesListsList=map(
					lambda __TabularedIntsTuple:
					reduce(
							operator.__add__,
							map(
								lambda __TabularedInt,__Table,__GettingStringsList:
								zip(__GettingStringsList,__Table[__TabularedInt]),
								__TabularedIntsTuple,
								ModeledTablesList,
								ModeledFeaturedGettingStringsListsList
								)
							),
					self.ModeledJoiningTabularedIntsTuplesList,
				)

			print(UpdatingTuplesListsList)

			#Set the ScannedTuplesList for the GrindedTable and Define the Grinded things
			ScannedTuplesList=SYS.getScannedTuplesListWithScanningListsList(
				SYS.pick(GrindedTabularedDict['FeaturedTuplesList'],[3])
				)
			[
				(GrindedFeaturingColumningStringsList,GrindedFeaturingGettingStringsList),
				(GrindedOutputingColumningStringsList,GrindedOutputingGettingStringsList),
				(GrindedArrayingColumningStringsList,GrindedArrayingGettingStringsList)
			]=map(
						lambda __ParamString:
						SYS.collect(
							GrindedTabularedDict,
							map(
									lambda __ParamString:
									__ParamString+'TuplesList',
									[__ParamString]
								),
							[0,1]
						),
						['Featured','Outputed','Stored']
					)



			print("AAA",
				map(lambda __ModeledJoiningFeaturingTuplesList:
				zip(
					GrindedFeaturingColumningStringsList,
					self.pick(GrindedFeaturingGettingStringsList)
					)+__ModeledJoiningFeaturingTuplesList,
				ModeledJoiningFeaturingTuplesListsList
			))


			#For each ScannedTuple update, then look for the ModeledInt
			map(
				lambda __ScannedTuple:
				self.update(
								zip(
										GrindedFeaturingGettingStringsList,
										__ScannedTuple
									)
						).map(
							lambda __UpdatingTuplesList,__ModeledJoiningFeaturingTuplesList:
							self.update(
										__UpdatingTuplesList
										)(
										'output',[]
										).row(
											GrindedTabularedDict['Table'],
											zip(
												GrindedFeaturingColumningStringsList,
												self.pick(GrindedFeaturingGettingStringsList)
												)+__ModeledJoiningFeaturingTuplesList,
											zip(
												GrindedOutputingColumningStringsList,
												self.pick(GrindedOutputingGettingStringsList)
												)
										),
							UpdatingTuplesListsList,
							ModeledJoiningFeaturingTuplesListsList
							),
					ScannedTuplesList
				)
		#</GrindingMethods>

		#<GlobalMethod>

	def organize(self,_CommandingTuplesList):

		#CoOrder
		self.coOrder(
						[
							'commandEachSetForAll',
							'coOrder'
						],
						[
							'GroupedObjectsDict.values()'
						],
						(
							_CommandingTuplesList,
							[
								['/']
							]
						)
					)

		#Close the GroupedFilePointer
		self.GroupedFilePointer.close()

		#Return self
		return self

		#</GlobalMethod>

	#</OrganizingMethods>

		#<JoinMethod>

	'''
	def join(self):

		#Define the Module
		Module=getattr(SYS,self.__module__)

		#Zip
		self.JoinedTuplesList=zip(
									Module.ModeledColumningStringsList,
									self.collect(
										[
											Module.ModeledGettingStringsList
										]
									)
								)

		#Set the JoinedString
		self.JoinedString='_'.join(
									map(
											lambda __JoinedTuple:
											'('+str(__JoinedTuple[0])+','+str(__JoinedTuple[1])+')',
											self.JoinedTuplesList
										)
								)

		#Return self
		return self
	'''
		#</JoinMethod>

		#<StructureMethod>

	def structure(self):

		self.coOrder(
						[
							'commandEachSetForAll',
							'coOrder'
						],
						[
							'StructureObjectsTuplesList.values()'
						],
						(
							[
								('parentize','Structured')
							],
							[
								['/']
							]
						)
					)

		return self
		#</StructureMethod>

	#</ParentizingMethod>

	#<PrintingMethods>

		#<ReprMethod>

	def __repr__(self):
		"""Call the repr<HookString> methods and return a SYS printed version of the self.PrintedDict"""

		#Refresh the attributes
		self.RepresentingDict=copy.deepcopy(SYS.PrintingDict)
		self.RepresentedTuplesList=self.__dict__.items()
		self.IsRepresentingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="repr"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsRepresentingBool==False:
						return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
								self,
								self.RepresentedDict
								)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
								dict(self.RepresentedTuplesList),
								self.RepresentingDict
								)

		#return the Print
		return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
				self,
				self.RepresentingDict
				)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
				dict(self.RepresentedTuplesList),
				self.RepresentingDict
				)

		#</ReprMethod>

	#</PrintingMethods>

	#</DefineMethods>

	#<DefineHookMethods>

	def getBefore(self):
		"""
			Hook in the __getitem__ of the Object 
			with different possible ShortStrings

			If _GettingVariable is an int, it returns the Value in the corresponding Index
			in self.AppendedValueVariablesList

			If _GettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			If _GettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			Else do a __getitem__ in the __dict__
		"""

		#Debug
		#debug("self.GettingVariable is : "+repr(self.GettingVariable))
		#debug("self.GettedVariable is : "+repr(self.GettedVariable))

		#Init a local IsGettedBool
		IsGettedBool=False

		#Get with the AppendedInt in the ordered "dict"
		if type(self.GettingVariable)==int:
			if self.GettingVariable<len(self.AppendedValueVariablesList):

				#Get the GettedVariable from the AppendedValueVariablesList and the Index
				self.GettedVariable=self.AppendedValueVariablesList[self.GettingVariable]

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return

		#Get with ExecShortString (it can be a return of a method call)
		elif type(self.GettingVariable) in [str,unicode] and SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,ExecShortString])==ExecShortString:

			#Define the ExecString
			ExecString=self.GettingVariable.split(ExecShortString)[1]

			#Put the output in a local Local Variable
			exec ExecString in locals()

			#Give it to self.GettedVariable
			self.IsGettingBool=False
			return 

		#Get an Attribute (it can be a return of a method call)
		elif AttributeShortString in self.GettingVariable:

			#Define the GettingStringsList
			GettingStringsList=self.GettingVariable.split(AttributeShortString)

			#Look if the deeper getter is Ok
			GetterVariable=self[GettingStringsList[0]]

			#Define the next getting String
			NextGettingString=GettingStringsList[1]

			#If the GettingStringsList has brackets in the end it means a call of a method
			if "()"==NextGettingString[-2:]:
				NextGettingString=NextGettingString[:-2]

				#Case where it is an object
				if hasattr(GetterVariable,NextGettingString):
					self.GettedVariable=getattr(GetterVariable,NextGettingString)()
					self.IsGettingBool=False
					return 

				elif SYS.getIsTuplesListBool(GetterVariable):

					if NextGettingString=='keys':
						self.GettedVariable=map(
													lambda __ListedTuple:
													__ListedTuple[0],
													GetterVariable
												)
						self.IsGettingBool=False
						return 
					if NextGettingString=='values':
						self.GettedVariable=map(	
													lambda __ListedTuple:
													__ListedTuple[1],
													GetterVariable
												)
						self.IsGettingBool=False
						return 

			elif hasattr(GetterVariable,NextGettingString):

				#Else it is a get of an item in the __dict__
				GettingVariable=getattr(GetterVariable,NextGettingString)

				#Case where it has to continue to be getted
				if len(GettingStringsList)>2:
					GettingVariable[AttributeShortString.join(GettingStringsList[2:])]
					self.IsGettingBool=False
					return 
				else:

					#Else return the GettingVariable
					self.GettedVariable=GettingVariable
					self.IsGettingBool=False
					return 

		#Get with DeepShortString
		elif self.GettingVariable==DeepShortString:
			
			#If it is directly DeepShortString
			self.GettedVariable=self

			#Stop the getting
			self.IsGettingBool=False

			#Return
			return 

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,AppendShortString])==AppendShortString:

			#Get with the * 
			self.GettingVariable=self.GettingVariable.split(AppendShortString)[1]
			if self.GettingVariable in self.AppendedKeyStringToAppendedIntDict:
				
				#Get the GettedVariable from the AppendedValueVariablesList and the AppendedKeyStringToAppendedIntDict
				self.GettedVariable=self.AppendedValueVariablesList[self.AppendedKeyStringToAppendedIntDict[self.GettingVariable]]

				#Stop the getting
				self.IsGettingBool=False

				#Return 
				return

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,TabularShortString])==TabularShortString:

			#Get with the * 
			self.GettingVariable=self.GettingVariable.split(TabularShortString)[1]

			if self.GettingVariable in self.TabularedTypeStringToTabularedIntDict:
				
				#Get the GettedVariable from the TabularedDictsList and the TabularTypeStringToTabularedIntDict
				self.GettedVariable=self.TabularedDictsList[self.TabularedTypeStringToTabularedIntDict[self.GettingVariable]]

				#Stop the getting
				self.IsGettingBool=False

				#Return 
				return
		
		#Get with '/'
		elif self.GettingVariable==DeepShortString:
			
			#If it is directly '/'
			self.GettedVariable=self

			#Stop the getting
			self.IsGettingBool=False

			#Return
			return 

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,DeepShortString])==DeepShortString:

			#Define the GettingVariableStringsList
			GettingVariableStringsList=DeepShortString.join(self.GettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
			GettedVariable=self[GettingVariableStringsList[0]]
			if GettedVariable!=None:

				if len(GettingVariableStringsList)==1:

					#Direct update in the Child or go deeper with the ChildPathString
					self.GettedVariable=GettedVariable

					#Stop the getting
					self.IsGettingBool=False
				
					#Return 
					return 

				else:

					#Define the ChildPathString
					ChildPathString=DeepShortString.join(GettingVariableStringsList[1:])	

					#Get in the first deeper Element with the ChildPathString
					self.GettedVariable=GettedVariable[ChildPathString]

					#Stop the getting
					self.IsGettingBool=False

					#Return
					return 

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,CopyShortString])==CopyShortString:

			#deepcopy
			self.GettedVariable=copy.deepcopy(
					self[CopyShortString.join(self.GettingVariable.split(CopyShortString)[1:])]				
				)

			#Stop the getting
			self.IsGettingBool=False

			#Return 
			return 

		elif self.GettingVariable=="DictatedVariablesList":

			#Return values of the __dict__
			self.GettedVariable=self.__dict__.values()

			#Stop the getting
			self.IsGettingBool=False

			#Return 
			return  

		elif self.GettingVariable=="AppendedVariablesList":

			#Return AppendedValueVariablesList
			self.GettedVariable=self.AppendedValueVariablesList

			#Stop the getting
			self.IsGettingBool=False

			#Return 
			return  

		elif self.GettingVariable=='Dict':
			self.GettedVariable=self.__dict__
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='GroupedObjectsDict':
			self.GettedVariable=self.GroupedObjectsDict
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='MethodsDict':
			self.GettedVariable=SYS.getMethodsDictWithClass(self.__class__)
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='CommoditedVariablesDict':
			self.GettedVariable=filter(
							lambda __DictatedVariable:
							self.__class__ not in type(__DictatedVariable).__mro__,
							self.__dict__.items()
						)
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='OutputedVariablesDict':
			self.GettedVariable=[]
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='FeaturedVariablesDict':
			self.GettedVariable=[]
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='TuplesList':
			self.GettedVariable=zip(self.AppendedKeyStringsList,self.AppendedValueVariablesList)
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='StructuredObjectsTuplesList':
			self.GettedVariable=self.StructuredObjectsTuplesList
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='ElementedVariablesTuplesList':
			self.GettedVariable=filter(
							lambda __AppendedVariable:
							self.__class__ not in type(__AppendedVariable[1]).__mro__,
							zip(self.AppendedKeyStringsList,self.AppendedValueVariablesList)
						)
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=="__class__":
			self.GettedVariable=self.__class__.__dict__
			self.IsGettingBool=False
			return

		#Do the minimal get
		if IsGettedBool==False:
		
			if type(self.GettingVariable) in [str,unicode]:

				#Get safely the Value
				if self.GettingVariable in self.__dict__:

					#__getitem__ in the __dict__
					self.GettedVariable=self.__dict__[self.GettingVariable]

					#Stop the getting
					self.IsGettingBool=False


	def setBefore(self):
		"""
			Hook in the __setitem__ of the Object 
			with different possible ShortStrings

			If _SettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the setting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is <camelCase> then it is a hook query...
			so it calls with the form self(<camelCase>,SettedVariable)

			If _SettingVariable is of the form <String1>Pointer and _SettedVariable of the form /<String2>
			it gets the self[<String2>] and do self[<String1>Pointer]=self[<String2>]

			Else do a __setitem__ in the __dict__ 
		"""

		#Adding set
		if SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,AddShortString])==AddShortString:

			#Define the AddedKeyString
			AddedKeyString=self.SettingVariable.split(AddShortString)[1]

			#Get the AddedVariable
			AddedVariable=self[AddedKeyString]

			#Add
			if hasattr(AddedVariable,'__add__'):
				AddedVariable+=self.SettedVariable

			#Return
			return

		#Appending set
		if SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,AppendShortString])==AppendShortString:

			#Define the AppendedKeyString
			AppendedKeyString=self.SettingVariable.split(AppendShortString)[1]

			#Update an already
			if AppendedKeyString in self.AppendedKeyStringToAppendedIntDict:
				self.AppendedValueVariablesList[self.AppendedKeyStringToAppendedIntDict[AppendedKeyString]]=self.SettedVariable
			else:
				#Append
				self.AppendedKeyStringsList+=[AppendedKeyString]
				self.AppendedValueVariablesList+=[self.SettedVariable]
				self.AppendedKeyStringToAppendedIntDict[AppendedKeyString]=len(self.AppendedValueVariablesList)-1

			#If it is an object
			if self.__class__ in type(self.SettedVariable).__mro__:

				#Structure in the Child
				self.SettedVariable.StructuredInt=len(self.AppendedValueVariablesList)-1
				self.SettedVariable.StructuredKeyString=AppendedKeyString
				self.SettedVariable.StructuredParentPointer=self

				#Update the StructuringObjectsList
				self.StructuredObjectsTuplesList.append((AppendedKeyString,self.SettedVariable))

			#Return 
			return

		#Deep set
		elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,DeepShortString])==DeepShortString:

			#Get the SettingVariableStringsList
			SettingVariableStringsList=DeepShortString.join(self.SettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
			GettedVariable=self[SettingVariableStringsList[0]]
			if GettedVariable!=None:
				
				#Define the ChildPathString
				ChildPathString=DeepShortString.join(SettingVariableStringsList[1:])	

				#Direct update in the Child or go deeper with the ChildPathString
				if ChildPathString=="": 
					if self.__class__ in type(GettedVariable).__mro__:

						#Modify directly the GettedVariable with self.SettedVariable
						GettedVariable.__setitem__(*self.SettedVariable)

					elif SYS.getCommonPrefixStringWithStringsList([SettingVariableStringsList[0],AppendShortString])==AppendShortString:
						
						#This is in that case a set in the append but not binded
						AppendedKeyString=SettingVariableStringsList[0].split(AppendShortString)[1]
						if AppendedKeyString in self.AppendedKeyStringToAppendedIntDict:
							self.AppendedValueVariablesList[self.AppendedKeyStringToAppendedIntDict[AppendedKeyString]]=self.SettedVariable
				else:

					#Set in the first deeper Element with the ChildPathString
					GettedVariable[ChildPathString]=self.SettedVariable

			#Return 
			return 

		#Call for a hook
		elif (self.SettingVariable[0].isalpha() or self.SettingVariable[1:2]==["_","_"]) and  self.SettingVariable[0].lower()==self.SettingVariable[0]:

			#Get the Method
			if hasattr(self,self.SettingVariable):

				#Get the method
				SettingMethod=getattr(self,self.SettingVariable)

				#Look for the shape of the inputs of this method
				ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(SettingMethod)

				#Adapt the shape of the args
				if len(ArgsList)>2:
					getattr(self,self.SettingVariable)(*self.SettedVariable)
				elif len(ArgsList)==1:
					getattr(self,self.SettingVariable)()
				else:
					getattr(self,self.SettingVariable)(self.SettedVariable)
			
			elif len(self.SettedVariable)==1:

				#Adapt format of the ArgsList
				self(self.SettingVariable,self.SettedVariable[0])
			else:
				self(self.SettingVariable,*self.SettedVariable)

			#Return
			return

		elif SYS.getCommonSuffixStringWithStringsList([self.SettingVariable,PointerShortString])==PointerShortString:
				
			#Special set for Pointer with a / getted Value
			if type(self.SettedVariable) in [str,unicode]:

				#The Value has to be a deep short string for getting the pointed variable
				if SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,DeepShortString])==DeepShortString:
					GettedVariable=self[self.SettedVariable]
					if GettedVariable!=None:
						#Link the both with a __setitem__
						self[self.SettingVariable]=GettedVariable

				#Return in the case of the Pointer and a string setted variable
				return

		#Get with ExecShortString (it can be a return of a method call)
		elif type(self.SettedVariable) in [str,unicode] and SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,ExecShortString])==ExecShortString:

			self[self.SettingVariable]=self[self.SettedVariable]
			self.IsSettingBool=False
			return

		#Do the minimal set if nothing was done yet
		if type(self.SettingVariable) in [str,unicode]:

			#__setitem__ in the __dict__
			self.__dict__[self.SettingVariable]=self.SettedVariable

			#Group in the Child
			if self.__class__ in type(self.SettedVariable).__mro__ and SYS.getCommonSuffixStringWithStringsList([self.SettingVariable,PointerShortString])!=PointerShortString:
				
				#Group the Child
				self.SettedVariable.GroupedKeyString=self.SettingVariable
				self.SettedVariable.GroupedParentPointer=self
				
				#Update the GroupedObjectsDict
				self.GroupedObjectsDict[self.SettingVariable]=self.SettedVariable

	def delBefore(self):
		"""
			Hook in the __delitem__ of the Object
		"""

		#Do the minimal delitem
		if type(self.DeletingVariable) in [str,unicode]:

			#Del Safely the Value
			if self.DeletingVariable in self.__dict__:
				del self.__dict__[self.DeletingVariable]

	def reprAfter(self):
		"""
			Hook in the repr of the Object
			for printing in the Console attributes in a more 
			user-friendly manner
		"""

		#Get the MroAppendedStringsList
		MroAppendedStringsList=SYS.getMroKeyStringsListWithClass(self.__class__)

		#Remove the NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.NotRepresentingGettingVariablesList,
										self.RepresentedTuplesList
								)

		#First keeps only the Specific and New attributes
		self.RepresentedTuplesList=map(
									lambda _RepresentedTuple:
									("<Spe>"+_RepresentedTuple[0],_RepresentedTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] in self.__class__.SpecificKeyStringsList,
											self.RepresentedTuplesList
										)
								)+map(
									lambda _NewTuple:
									("<New>"+_NewTuple[0],_NewTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] not in MroAppendedStringsList,
											self.RepresentedTuplesList
										)
								)

		#Add some forced Values with RepresentingKeyVariables
		self.RepresentedTuplesList+=map(
											lambda _KeyVariable:
											("<NotSpe>"+str(_KeyVariable),self[_KeyVariable]),
											self.RepresentingKeyVariablesList
										)

		#Simplify the numpy variables repr
		self.RepresentedTuplesList=map(
										lambda _RepresentedTuple:
										_RepresentedTuple
										if type(_RepresentedTuple[1]) not in [numpy.ndarray] 
										else (
												_RepresentedTuple[0],
												"<numpy.ndarray shape "+str(numpy.shape(
													_RepresentedTuple[1]))+">"
												),
										self.RepresentedTuplesList
									)

	
	#</DefineHookMethods>

	


#</DefineClass>



