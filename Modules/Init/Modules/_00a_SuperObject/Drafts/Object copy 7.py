#<ImportModules>
import copy
import inspect
import ShareYourSystem as SYS
import numpy
import os
import sys
from tables import *
#</ImportModules>

"""
 coucou erwan, ce message est d'une grande importance... 
 "mmmhhh!! noh oui oh oui!! oooohh!!!"" 
 c'est la reponse a ttes tes annees de recherches intensives en neuroscience!! 
 Tu peux me remercier!!!!
"""

#<DefineLocals>
AttributeShortString="."
DeepShortString="/"
AppendShortString="App_"
PointerShortString="Pointer"
CopyShortString='*'
CollectShortString=":"
ExecShortString="Exec_"
#</DefineLocals>

def debug(_String):
	Frame=inspect.currentframe().f_back
	ScriptPathStringsList=SYS.getWordStringsListWithString(Frame.f_code.co_filename)
	ScriptPathString=''.join(ScriptPathStringsList)
	print("%s , %s, l. %s : "%(ScriptPathString,Frame.f_code.co_name,Frame.f_lineno));
	print(_String)
	print("")

#<DefineClass>
class ObjectClass(object):
	
	#<DefineMethods>

		#<HandlingMethods>
	
			#<InitMethod>
	def __init__(self):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""

		#<DefineSpecificDict>

		#Attributes for the __init__ property
		self.IsInitiatingBool=True

		#Attributes for the __getitem__ property
		self.IsGettingBool=True
		self.GettingVariable=None
		self.GettedVariable=None

		#Attributes for the __setitem__ property
		self.IsSettingBool=True
		self.SettingVariable=None
		self.SettedVariable=None
		
		#Attributes for the apply property
		self.IsApplyingBool=True
		self.MappedAppliedVariablesList=[]

		#Attributes for the order property
		self.MappedOrderedVariablesList=[]

		#Attributes for the append property
		self.AppendedKeyStringsList=[]
		self.AppendedValueVariablesList=[]
		self.AppendedKeyStringToAppendedIntDict={}

		#Attributes for the group property
		self.GroupingFilePathString=""
		self.GroupingModuleString="tables"
		self.GroupingFilePathString=""
		self.GroupedFilePointer=None
		self.GroupedObjectsDict={}
		self.GroupedKeyString=""
		self.GroupedParentPointer=None
		self.GroupedGrandParentPointersList=[]
		self.GroupedKeyStringsList=[]
		self.GroupedPathStringsList=[]
		self.GroupedPathString="/"

		#Attributes for the join property
		self.JoinedTuplesList=[]
		self.JoinedString=""

		#Attributes for the table property
		self.TabularingTuplesList=[]
		self.TabularedDict={}
		self.TabularedClassesDict={}
		self.TabularedPointersDict={}

		#Attributes for the model property
		self.ModeledInt=-1

		#Attributes for the use property
		self.UsedInt=-1

		#Attributes for the structure property
		self.StructuredInt=-1
		self.StructuredKeyString=""
		self.StructuredParentPointer=None
		self.StructuredGrandParentPointersList=[]
		self.StructuredObjectsTuplesList=[]

		#Attributes for the relay property (copy and set in DeliveredPointer)
		self.IsRelayingBool=True
		self.RelayedKeyVariablesList=[]
		self.RelayingKeyVariablesList=[]
		self.RelayingVariable=None

		#Attributes for the __repr__ property
		self.IsRepresentingBool=True
		self.RepresentingDict={}
		self.RepresentedTuplesList=[]
		self.NotRepresentingKeyStringsList=filter(
													lambda _KeyString:
													_KeyString not in [
																		'AppendedKeyStringsList',
																		'AppendedValueVariablesList',
																		'StructuredInt',
																		'StructuredKeyString',
																		'StructuredGrandParentPointersList',
																		'GroupingHdfFile',
																		'GroupingHdfFilePointer',
																		'GroupedKeyString',
																		'GroupedGrandParentPointersList',
																		'GroupedPathString'
																	],
													self.__class__.SpecificKeyStringsList
										) if type(self)==SYS.ObjectClass else []
		self.RepresentingKeyVariablesList=[
											'AppendedKeyStringsList',
											'AppendedValueVariablesList',
											'StructuredInt',
											'StructuredKeyString',
											'StructuredGrandParentPointersList',
											'GroupingHdfFile',
											'GroupingHdfFilePointer',
											'GroupedKeyString',
											'GroupedGrandParentPointersList',
											'GroupedPathString'
											] if type(self)!=SYS.ObjectClass else []

		#</DefineSpecificDict>

		#Hook methods (integrativ)
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="init"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsInitiatingBool==False:
						return 

					#Call the HookingMethod
					HookingMethod(self)

			#</InitMethod>

			#<ExecuteMethod>

	def execute(self,_ExecString):

		exec _ExecString in locals()
		return self

			#</ExecuteMethod>

			#<CallMethod>

	def __call__(self,_CallVariable,*_CallingVariablesList):
		"""Call the <_CallString><HookString> methods and return self.CalledVariable (self by default)"""

		#Refresh the attributes
		LocalCallVariable=_CallVariable
		LocalCallingVariablesList=_CallingVariablesList
		LocalCalledPointer=self

		#self.CalledPointer=self
		self.IsCallingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString=LocalCallVariable+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,LocalCallVariable,LocalCallingVariablesList)

					if type(OutputVariable)==dict:
						if 'LocalCallVariable' in OutputVariable:
							LocalCallVariable=OutputVariable['LocalCallVariable']
						if 'LocalCallingVariablesList' in OutputVariable:
							LocalCallingVariable=OutputVariable['LocalCallingVariablesList']
						if 'LocalCallPointer' in OutputVariable:
							LocalCallPointer=OutputVariable['LocalCallPointer']

					#Check Bool
					if self.IsCallingBool==False:
						return LocalCalledPointer

		#Return the OutputVariable
		return LocalCalledPointer

			#</CallMethod>

			#<GetMethod>

	def __getitem__(self,_GettingVariable):
		"""Call the get<HookString> methods and return self.GettedVariable (None by default)"""
		
		#Refresh the attributes
		self.GettingVariable=_GettingVariable
		self.GettedVariable=None
		self.IsGettingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="get"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Debug
					#debug("HookingMethodString is : "+HookingMethodString)
					#debug("self.GettedVariable is : "+repr(self.GettedVariable))

					#Check Bool
					if self.IsGettingBool==False:
						return self.GettedVariable

		#Return GettedVariable
		return self.GettedVariable

		#</GetMethod>

		#<SetMethods>

			#<GlobalMethod>

	def __setitem__(self,_SettingVariable,_SettedVariable):
		"""Call the set<HookString> methods"""

		#Refresh the attributes
		self.SettingVariable=_SettingVariable
		self.SettedVariable=_SettedVariable
		self.IsSettingBool=True

		#Sort the HookMethodStrings
		for OrderString in ["Before","After"]:

			#Define a Local IsSettedBool
			IsSettedBool=False

			if type(_SettingVariable) in [str,unicode]:
			
				#Define the BindingMethodString
				BindingMethodString="bind"+_SettingVariable+OrderString
				
				#Check that there is HookingMethods for it
				if BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict:
					
					#Say ok
					IsSettedBool=True

					#Call the specific Appended methods 
					for HookingMethod,BindingMethod in self.__class__.BindingMethodStringToMethodTuplesListDict[BindingMethodString]:

						#Call the HookMethod
						if callable(HookingMethod):
							HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

						#Call the Binding
						if callable(BindingMethod):
							OutputVariable=BindingMethod(self)

							#End the Setting
							if self.IsSettingBool==False:
								return self

			#If a binding method was not yet found then look for a global set method
			if IsSettedBool==False:

				#_SettingVariable is not a string
				HookingMethodString="set"+OrderString

				#Check that there is HookingMethods for it
				if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

					#Call the specific Appended methods 
					for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

						#Call the HookingMethod
						HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

		#Return self
		return self

			#</GlobalMethod>

			#<AppendMethod>

	def append(self,_AppendingVariable):
		"""Call __setitem__ with a defined SettingVariable contained in the _AppendingVariable"""
		
		#Define the SettingVariable
		SettingVariable="App_"
		if SYS.getIsTuplesListBool(_AppendingVariable) and _AppendingVariable[0][0]=='StructuredKeyString':
				SettingVariable+=_AppendingVariable[0][1]
		elif self.__class__ in type(_AppendingVariable).__mro__:
			SettingVariable+=_AppendingVariable.StructuredKeyString
		elif type(_AppendingVariable)==dict and 'StructuredKeyString' in _AppendingVariable:
			SettingVariable+=_AppendingVariable['StructuredKeyString']

		#Then set
		return self.__setitem__(SettingVariable,_AppendingVariable)

			#</AppendMethod>

		#</SetMethods>

		#<DelMethod>

	def __delitem__(self,_DeletingVariable):
		"""Call the del<HookString> methods and delete in the __dict__ by default"""
		
		#Refresh the attributes
		self.DeletingVariable=_DeletingVariable
		self.IsDeletingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="delete"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsDeletingBool==False:
						return self

		#Return self
		return self

		#</DelMethod>

	#</HandlingMethods>

	#<MappingMethods>

		#<GlobalMethod>

	def map(self,_MappingFunction,_MappedList):
		"""Just map"""
		return map(_MappingFunction,_MappedList)

		#</GlobalMethod>

		#<ApplyingMethods>

			#<GenericMethod>

	def apply(self,_MethodString,_AppliedVariablesList):
		"""Map a call with the _MethodString to the _AppliedVariablesList"""

		#Get the AppliedMethod
		if hasattr(self,_MethodString):
			self.MappedAppliedVariablesList=map(
					lambda __AppliedVariable:
					SYS.getWithMethodAndArgsVariable(getattr(self,_MethodString),__AppliedVariable)
					if hasattr(self,_MethodString)
					else None,
					_AppliedVariablesList
				)

		#Return self
		return self

			#</GenericMethod>

			#<GetMethods>

				#<PickMethod>

	def pick(self,_GettingVariablesList):
		"""Apply the __getitem__ to the <_GettingVariablesList>"""

		#Apply __getitem__
		self.apply('__getitem__',_GettingVariablesList)

		#Return AppliedVariablesList
		return self.MappedAppliedVariablesList

				#</PickMethod>

				#<CollectMethod>

	def collect(self,_CollectingVariablesList):
		"""Reduce a map of pick for CollectingVariable being a List or __getitem__ for CollectingVariable being a GettingVariable"""

		import operator
		return reduce(operator.__add__,map(
												lambda __CollectingVariable:
												self.pick(__CollectingVariable) 
												if type(__CollectingVariable)==list
												else self[__CollectingVariable],
												_CollectingVariablesList
										)
					)


				#<CollectMethod>

			#</GetMethods>

			#<SetMethods>

				#<GlobalMethod>

	def update(self,_SettingVariableTuplesList):
		"""Apply the __setitem__ to the <_SettingVariableTuplesList>"""

		#Apply __setitem__
		return self.apply('__setitem__',_SettingVariableTuplesList)

				#</GlobalMethod>

				#<AppendMethod>

	def __add__(self,_AddingVariablesList):
		"""Apply the append to the <_AddingVariablesList>"""

		#Apply
		self.apply('append',_AddingVariablesList)

		#Return 
		return self

				#</AppendMethod>

			#</SetMethods>

		#</ApplyingMethods>

		#<CommandingMethods>

			#<AllSetsForEachMethod>

	def commandAllSetsForEach(self,_SettingVariableTuplesList,_CollectingVariablesList):
		"""In each _OrderedVariable of _OrderedVariablesList there is an update with _SettingVariableTuplesList"""

		#Get the CollectedVariablesList
		CollectedVariablesList=self.collect(_CollectingVariablesList)

		#For each __CollectedVariable it is updating with _SettingVariableTuplesList
		self.MappedOrderedVariablesList=map(
				lambda __CollectedVariable:
				__CollectedVariable.update(_SettingVariableTuplesList),
				CollectedVariablesList
			)

		#Return self
		return self 

			#</AllSetsForEachMethod>

			#<EachSetForAllMethod>

	def commandEachSetForAll(self,_SettingVariableTuplesList,_CollectingVariablesList):
		"""With each _SettingTuple of _SettingVariableTuplesList there is a set in _OrderedVariablesList"""

		#Get the CollectedVariablesList
		CollectedVariablesList=self.collect(_CollectingVariablesList)

		#For each SettingTuple it is setted in _OrderedVariablesList
		self.MappedOrderedVariablesList=map(
				lambda __SettingVariableTuple:
				map(
					lambda __CollectedVariable:
					__CollectedVariable.__setitem__(*__SettingVariableTuple),
					CollectedVariablesList
					),
				_SettingVariableTuplesList
			)

		#Return self
		return self 

			#</EachSetForAllMethod>

		#</CommandingMethods>

		#<OrderingMethods>

			#<CoOrderMethod>

	def coOrder(self,_OrderingStringsTuple,_OrderingCollectingVariablesList,_CommandingTuple):
		"""From top to down it calls a Command at the level of self with a _CommandingTuple and a recursive order call to _OrderingCollectingVariablesList"""

		#Get the OrderingCollectedVariablesList
		OrderingCollectedVariablesList=self.collect(_OrderingCollectingVariablesList)

		#Map an Order/Command given the _OrderingStringsTuple
		map(
				lambda __OrderingString:
				#This is the coOrder
				map(
						lambda __OrderingCollectedVariable:
						getattr(
									__OrderingCollectedVariable,
									__OrderingString
								)(_OrderingStringsTuple,_OrderingCollectingVariablesList,_CommandingTuple),
						OrderingCollectedVariablesList
					) if 'Order' in __OrderingString
				else
				#This is the command
				getattr(self,__OrderingString)(*_CommandingTuple),	
				_OrderingStringsTuple
			)

		#Return self
		return self 

			#</CoOrderMethod>

			#<TransOrderMethod>

	def transOrder(self,_OrderingStringsTuple,_OrderingCollectingVariablesList,_CommandingTuple):
		"""From top to down it calls a Command at the level of self with a _CommandingTuple and a recursive order call to _OrderingCollectingVariablesList"""

		#Map an Order/Command given the _OrderingStringsTuple
		for OrderingString in _OrderingStringsTuple:

			if 'Order' in OrderingString:

				#Get the OrderingCollectedVariablesList
				OrderingCollectedVariablesList=self.collect(_OrderingCollectingVariablesList)

				#Map a recursive call
				map(
						lambda __OrderingCollectedVariable:
						map(	
								lambda __OrderingString:
								#This is the coOrder
								getattr(
											__OrderingCollectedVariable,
											__OrderingString
										)(_OrderingStringsTuple,_OrderingCollectingVariablesList,_CommandingTuple)
							),
						OrderingCollectedVariablesList
					)

			else:

				#This is the command
				getattr(self,__OrderingString)(*_CommandingTuple)

		#Return self
		return self 

			#</TransOrderMethod>

		#</OrderingMethods>

	#</MappingMethods>
	
	#<OrganizingMethods>

		#<ParentizingMethods>

	def parentize(self,_ParentingString):

		#Define the ParentPointerKeyString
		ParentPointerKeyString=_ParentingString+"ParentPointer"
		if getattr(self,ParentPointerKeyString)!=None:

			#Set the GrandParentPointersList
			GrandParentPointersListKeyString=_ParentingString+"GrandParentPointersList"
			setattr(
						self,
						GrandParentPointersListKeyString,
						[getattr(self,ParentPointerKeyString)]+getattr(
															getattr(self,ParentPointerKeyString),
															GrandParentPointersListKeyString
															)
					)

			#Set the KeyStringsList
			KeyStringKeyString=_ParentingString+"KeyString"
			KeyStringsListKeyString=_ParentingString+"KeyStringsList"
			self.__setattr__(
								KeyStringsListKeyString,
								map(
										lambda __GrandParentPointer:
										getattr(__GrandParentPointer,KeyStringKeyString),
										getattr(self,GrandParentPointersListKeyString)
									)
						)

			#Set the PathStringsList
			PathStringsListKeyString=_ParentingString+"PathStringsList"
			self.__setattr__(
								PathStringsListKeyString,
								[getattr(self,KeyStringKeyString)]+copy.copy(
									getattr(self,KeyStringsListKeyString)
								)
							)
			getattr(self,PathStringsListKeyString).reverse()

			#Set the PathString
			PathStringKeyString=_ParentingString+"PathString"
			self.__setattr__(
								PathStringKeyString,
								DeepShortString.join(getattr(self,PathStringsListKeyString))
							)

		#</ParentizingMethods>

		#<GroupingMethods>

	def setGroupedFilePointer(self):

		#For the uppest Parent maybe init a hdf5 file
		if len(self.GroupedGrandParentPointersList)>0:
			UppestGroup=self.GroupedGrandParentPointersList[-1]
		else:
			UppestGroup=self

		#Check for a good FilePathString
		if UppestGroup.GroupingFilePathString!="":

			#Check for first write
			if os.path.isfile(UppestGroup.GroupingFilePathString)==False:
				UppestGroup.GroupedFilePointer=sys.modules[UppestGroup.GroupingModuleString].File(
					UppestGroup.GroupingFilePathString,'w')
				UppestGroup.GroupedFilePointer.close()

		if UppestGroup.GroupedFilePointer==None or ( (UppestGroup.GroupingModuleString=='tables' and UppestGroup.GroupedFilePointer.isopen==0) or (UppestGroup.GroupingModuleString=='h5py' and UppestGroup.GroupedFilePointer.mode=='c') ):

			#Open the GroupedFilePointer
			UppestGroup.GroupedFilePointer=sys.modules[UppestGroup.GroupingModuleString].File(
				UppestGroup.GroupingFilePathString,'r+')

		#Point on the GroupingFilePointer of the uppest Parent
		self.GroupedFilePointer=UppestGroup.GroupedFilePointer

	def group(self):

		#Create a group in the hdf5 file
		if self.GroupedFilePointer!=None:

			#Make sure that the first char is /
			if self.GroupedPathString[0]!="/":
				self.GroupedPathString="/"+self.GroupedPathString

			#Check if the Path exists
			if self.GroupedPathString not in self.GroupedFilePointer:

				#Set all the intermediate Paths before
				GroupPathStringsList=self.GroupedPathString.split('/')[1:]
				ParsingGroupPathString="/"

				#Set the PathString from the top to the down (integrativ loop)
				for GroupPathString in GroupPathStringsList:

					#Go deeper
					NewParsingGroupPathString=ParsingGroupPathString+GroupPathString

					#Create the group if not already
					if NewParsingGroupPathString not in self.GroupedFilePointer:
						if self.GroupingModuleString=="tables":
							self.GroupedFilePointer.create_group(ParsingGroupPathString,GroupPathString)
						elif self.GroupingModuleString=="h5py":
							Group=self.GroupedFilePointer[ParsingGroupPathString]
							Group.create_group(GroupPathString)
					
					#Prepare the next group	
					ParsingGroupPathString=NewParsingGroupPathString+'/'

		#</GroupingMethod>

		#<TabularingMethods>

	def bindTabularingTuplesListAfter(self):

		self.TabularedDict={}
		self.TabularedClassesDict={}
		for TabularingTuple in self.TabularingTuplesList:

			#Init at the [TabularingTuple[0]
			self.TabularedDict[TabularingTuple[0]]={}

			#Define a short alias
			Dict=self.TabularedDict[TabularingTuple[0]]

			#Listify the TabularingTuplesList
			Dict.update(
				zip(
					[
						'ColumningStringsList',
						'GettingStringsList',
						'ColsList',
						'ParameterizingVariablesList'
					],
					zip(*TabularingTuple[1])
					)
				)

			#Remove the Scanning with None or []
			Dict['ScanningIndexIntsList'],Dict['NotScanningIndexIntsList']=SYS.whereby(
				lambda __ScanningList:
				type(__ScanningList)==list and len(__ScanningList)>0,
				Dict['ParameterizingVariablesList']
				)

			#Tag the real scanning things
			SYS.tag(Dict,[
							'ColumningStringsList',
							'GettingStringsList',
							'ParameterizingVariablesList'
						],'Real',Dict['ScanningIndexIntsList'])

			#Get the Arrayed data
			Dict['ArrayingIndexIntsList'],Dict['NotArrayingIndexIntsList']=SYS.whereby(
				lambda __ColumningString:
				SYS.getCommonSuffixStringWithStringsList(
				['ArrayString',__ColumningString])=='ArrayString',
				Dict['ColumningStringsList']
				)

			#Tag the arraying things
			SYS.tag(Dict,[
							'ColumningStringsList',
							'GettingStringsList',
						],'Arraying',Dict['ArrayingIndexIntsList'])

			#Define the class
			class LocalTableClass(IsDescription):

				#Add an tagged Int (just like a unique KEY in mysql...) 
				locals().__setitem__(TabularingTuple[0].split('Table')[0]+'Int',Int64Col())

				#Get the OutputingKeyStringsList for making them like a IsDescription Type
				for ColumningString,Col in zip(
									Dict['ColumningStringsList'],
									Dict['ColsList']
								):

					#Set the Col 
					locals().__setitem__(ColumningString,Col)

					
				#Del ColumningString,ColClass
				try:
					del ColumningString,Col
				except:
					pass

			#Give a name of this local defined class
			LocalTableClass.__name__=SYS.getClassStringWithTypeString(TabularingTuple[0])

			#Set in the TabularedClassesDict
			self.TabularedClassesDict[LocalTableClass.__name__]=LocalTableClass
			setattr(SYS,LocalTableClass.__name__,LocalTableClass)

	def appendRowWithTableAndRowingTuplesList(self,_Table,_RowingTuplesList):
		
		'''
		print(map(	
				lambda __Row:
						map(
						lambda __RowingTuple:
						(type(__Row[__RowingTuple[0]]),__RowingTuple[1]),					
						_RowingTuplesList
						),
				_Table.iterrows()
				)
			)
		'''

		#Check if it is already appended
		IsAlreadyAppendedBool=any(
			map(	
				lambda __Row:
				all(
						map(
						lambda __RowingTuple:
						SYS.getIsEqualBool(__Row[__RowingTuple[0]],__RowingTuple[1]),					
						_RowingTuplesList
						)
					),
				_Table.iterrows()
				)
			)

		if IsAlreadyAppendedBool==False:

			#Short alias for the row
			Row=_Table.row

			#Set the key int
			Row.__setitem__(_Table.name.split('Table')[0]+'Int',_Table.nrows)

			#Set special items
			map(
					lambda __RowingTuple:
					Row.__setitem__(__RowingTuple[0],__RowingTuple[1]),
					_RowingTuplesList
				)

			#Append
			Row.append()

			#Flush
			_Table.flush()

	def setTableWithTableClass(self,_TableClass):

		#Init the table if it is not already
		TableTypeString=SYS.getTypeStringWithClassString(_TableClass.__name__)
		TabularedPathString='/'+TableTypeString
		TableTypeString=SYS.getTypeStringWithClassString(_TableClass.__name__)
		if TabularedPathString not in self.GroupedFilePointer:

			#Build the Table
			self.TabularedPointersDict[TableTypeString]=self.GroupedFilePointer.create_table(
									self.GroupedFilePointer.getNode(self.GroupedPathString),
									TableTypeString,
									_TableClass,
									_TableClass.__doc__ if _TableClass.__doc__!=None else "This is the "+_TableClass.__name__
									)
		else:

			#Get the Node
			self.TabularedPointersDict[TableTypeString]=self.GroupedFilePointer.getNode(TabularedPathString)

	def table(self):

		#Set all the Tables
		map(
				lambda __TableTuple:
				self.setTableWithTableClass(__TableTuple[1]),
				self.TabularedClassesDict.items()
			)
		
		#</TabularingMethods>

		#<ModelingMethods>

	def model(self):

		#Define the TableString
		TableTypeString='ModeledTable'

		#Get the Dict
		Dict=self.TabularedDict[TableTypeString]
			
		#Define the ModeledTableClass
		ModeledTableClass=getattr(SYS,SYS.getClassStringWithTypeString(TableTypeString))

		#Set the Table
		self.setTableWithTableClass(ModeledTableClass)

		#Set the ScannedTuplesList
		ScannedTuplesList=SYS.getScannedTuplesListWithScanningListsList(Dict['ScanningParameterizingVariablesList'])

		#Get the Table
		Table=self.TabularedPointersDict[TableTypeString]

		#Set And Append for each RowingTuple
		map(
				lambda __RowingTuple:
				self.appendRowWithTableAndRowingTuplesList(
						Table,
						zip(Dict['ScanningColumningStringsList'],__RowingTuple)
					),
				ScannedTuplesList
			)
		#</ModelingMethods>

		#<GrindingMethods>

	def grindWithTable(self,_Table):

		#Get the ModeledTable
		ModeledTable=self.TabularedPointersDict['ModeledTable']
		ModeledTableTuplesList=self.TabularedDict['ModeledTable']

		#Get the corresponding ModelingDict
		TableTuplesList=self.TabularedDict[_Table.name]

		#Set the ScannedTuplesList
		ScannedTuplesList=SYS.getScannedTuplesListWithScanningListsList(TableTuplesList['RealScanningListsList'])


		print("kkk",TableTuplesList['ScanningGettingStringsList'])



		#Set the ModelingVariables, output and appendRow by picking the GettingStringsList of the Table
		map(
			lambda __ScannedTuple:
			self.update(
							zip(
									TableTuplesList['ScanningGettingStringsList'],
									__ScannedTuple
								)
					).map(
						lambda __Row:
						self.update(
									zip(
										['ModeledInt']+list(ModeledTableTuplesList['GettingStringsList']),
										[__Row['ModeledInt']]+map(
											lambda __ColumnString:
											__Row[__ColumnString],
											ModeledTable.colnames
											)
										)
									)('output',[]).appendRowWithTableAndRowingTuplesList(
										_Table,
										zip(
											TableTuplesList['ColumningStringsList'],

											self.pick(TableTuplesList['GettingStringsList'])
											)
										),
						ModeledTable.iterrows()
						),
			ScannedTuplesList
			)
		
	def grind(self):

		#Grind
		OtherTablesTuplesList=map(
						lambda __TabularingTuple:
						__TabularingTuple[1],
						filter(
							lambda __TabularingPointerTuple: 
							'Modeled' not in __TabularingPointerTuple[0],
							self.TabularedPointersDict.items()
						)
					)

		#Set for each row setups the object, output it and them store in the corresponding table
		map(
				lambda __OtherTable:
				self.grindWithTable(__OtherTable),
				OtherTablesTuplesList
			)

		#</GrindingMethods>

		#<GlobalMethod>

	def organize(self,_CommandingTuplesList):

		#CoOrder
		self.coOrder(
						[
							'commandEachSetForAll',
							'coOrder'
						],
						[
							'GroupedObjectsDict.values()'
						],
						(
							_CommandingTuplesList,
							[
								['/']
							]
						)
					)

		#Close the GroupedFilePointer
		self.GroupedFilePointer.close()

		#Return self
		return self

		#</GlobalMethod>

	#</OrganizingMethods>

		#<JoinMethod>

	def join(self):

		#Define the Module
		Module=getattr(SYS,self.__module__)

		#Zip
		self.JoinedTuplesList=zip(
									Module.ModeledColumningStringsList,
									self.collect(
										[
											Module.ModeledGettingStringsList
										]
									)
								)

		#Set the JoinedString
		self.JoinedString='_'.join(
									map(
											lambda __JoinedTuple:
											'('+str(__JoinedTuple[0])+','+str(__JoinedTuple[1])+')',
											self.JoinedTuplesList
										)
								)

		#Return self
		return self

		#</JoinMethod>

		#<StructureMethod>

	def structure(self):

		self.coOrder(
						[
							'commandEachSetForAll',
							'coOrder'
						],
						[
							'StructureObjectsTuplesList.values()'
						],
						(
							[
								('parentize','Structured')
							],
							[
								['/']
							]
						)
					)

		return self
		#</StructureMethod>

	#</ParentizingMethod>

	#<PrintingMethods>

		#<ReprMethod>

	def __repr__(self):
		"""Call the repr<HookString> methods and return a SYS printed version of the self.PrintedDict"""

		#Refresh the attributes
		self.RepresentingDict=copy.deepcopy(SYS.PrintingDict)
		self.RepresentedTuplesList=self.__dict__.items()
		self.IsRepresentingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="repr"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsRepresentingBool==False:
						return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
								self,
								self.RepresentedDict
								)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
								dict(self.RepresentedTuplesList),
								self.RepresentingDict
								)

		#return the Print
		return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
				self,
				self.RepresentingDict
				)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
				dict(self.RepresentedTuplesList),
				self.RepresentingDict
				)

		#</ReprMethod>

	#</PrintingMethods>

	#</DefineMethods>

	#<DefineHookMethods>

	def getBefore(self):
		"""
			Hook in the __getitem__ of the Object 
			with different possible ShortStrings

			If _GettingVariable is an int, it returns the Value in the corresponding Index
			in self.AppendedValueVariablesList

			If _GettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			If _GettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			Else do a __getitem__ in the __dict__
		"""

		#Debug
		#debug("self.GettingVariable is : "+repr(self.GettingVariable))
		#debug("self.GettedVariable is : "+repr(self.GettedVariable))

		#Init a local IsGettedBool
		IsGettedBool=False

		#Get with the AppendedInt in the ordered "dict"
		if type(self.GettingVariable)==int:
			if self.GettingVariable<len(self.AppendedValueVariablesList):

				#Get the GettedVariable from the AppendedValueVariablesList and the Index
				self.GettedVariable=self.AppendedValueVariablesList[self.GettingVariable]

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return

		#Get with ExecShortString (it can be a return of a method call)
		elif type(self.GettingVariable) in [str,unicode] and SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,ExecShortString])==ExecShortString:

			#Define the ExecString
			ExecString=self.GettingVariable.split(ExecShortString)[1]

			#Put the output in a local Local Variable
			exec ExecString in locals()

			#Give it to self.GettedVariable
			self.IsGettingBool=False
			return 

		#Get an Attribute (it can be a return of a method call)
		elif AttributeShortString in self.GettingVariable:

			#Define the GettingStringsList
			GettingStringsList=self.GettingVariable.split(AttributeShortString)

			#Look if the deeper getter is Ok
			GetterVariable=self[GettingStringsList[0]]

			#Define the next getting String
			NextGettingString=GettingStringsList[1]

			#If the GettingStringsList has brackets in the end it means a call of a method
			if "()"==NextGettingString[-2:]:
				NextGettingString=NextGettingString[:-2]

				#Case where it is an object
				if hasattr(GetterVariable,NextGettingString):
					self.GettedVariable=getattr(GetterVariable,NextGettingString)()
					self.IsGettingBool=False
					return 

				elif SYS.getIsTuplesListBool(GetterVariable):

					if NextGettingString=='keys':
						self.GettedVariable=map(
													lambda __ListedTuple:
													__ListedTuple[0],
													GetterVariable
												)
						self.IsGettingBool=False
						return 
					if NextGettingString=='values':
						self.GettedVariable=map(	
													lambda __ListedTuple:
													__ListedTuple[1],
													GetterVariable
												)
						self.IsGettingBool=False
						return 

			elif hasattr(GetterVariable,NextGettingString):

				#Else it is a get of an item in the __dict__
				GettingVariable=getattr(GetterVariable,NextGettingString)

				#Case where it has to continue to be getted
				if len(GettingStringsList)>2:
					GettingVariable[AttributeShortString.join(GettingStringsList[2:])]
					self.IsGettingBool=False
					return 
				else:

					#Else return the GettingVariable
					self.GettedVariable=GettingVariable
					self.IsGettingBool=False
					return 

		#Get with DeepShortString
		elif self.GettingVariable==DeepShortString:
			
			#If it is directly DeepShortString
			self.GettedVariable=self

			#Stop the getting
			self.IsGettingBool=False

			#Return
			return 

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,AppendShortString])==AppendShortString:

			#Get with the * 
			self.GettingVariable=self.GettingVariable.split(AppendShortString)[1]
			if self.GettingVariable in self.AppendedKeyStringToAppendedIntDict:
				
				#Get the GettedVariable from the AppendedValueVariablesList and the KeyStringToAppendedIntDict
				self.GettedVariable=self.AppendedValueVariablesList[self.AppendedKeyStringToAppendedIntDict[self.GettingVariable]]

				#Stop the getting
				self.IsGettingBool=False

				#Return 
				return
		
		#Get with '/'
		elif self.GettingVariable==DeepShortString:
			
			#If it is directly '/'
			self.GettedVariable=self

			#Stop the getting
			self.IsGettingBool=False

			#Return
			return 

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,DeepShortString])==DeepShortString:

			#Define the GettingVariableStringsList
			GettingVariableStringsList=DeepShortString.join(self.GettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
			GettedVariable=self[GettingVariableStringsList[0]]
			if GettedVariable!=None:

				if len(GettingVariableStringsList)==1:

					#Direct update in the Child or go deeper with the ChildPathString
					self.GettedVariable=GettedVariable

					#Stop the getting
					self.IsGettingBool=False
				
					#Return 
					return 

				else:

					#Define the ChildPathString
					ChildPathString=DeepShortString.join(GettingVariableStringsList[1:])	

					#Get in the first deeper Element with the ChildPathString
					self.GettedVariable=GettedVariable[ChildPathString]

					#Stop the getting
					self.IsGettingBool=False

					#Return
					return 

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,CopyShortString])==CopyShortString:

			#deepcopy
			self.GettedVariable=copy.deepcopy(
					self[CopyShortString.join(self.GettingVariable.split(CopyShortString)[1:])]				
				)

			#Stop the getting
			self.IsGettingBool=False

			#Return 
			return 

		elif self.GettingVariable=="DictatedVariablesList":

			#Return values of the __dict__
			self.GettedVariable=self.__dict__.values()

			#Stop the getting
			self.IsGettingBool=False

			#Return 
			return  

		elif self.GettingVariable=="AppendedVariablesList":

			#Return AppendedValueVariablesList
			self.GettedVariable=self.AppendedValueVariablesList

			#Stop the getting
			self.IsGettingBool=False

			#Return 
			return  

		elif self.GettingVariable=='Dict':
			self.GettedVariable=self.__dict__
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='GroupedObjectsDict':
			self.GettedVariable=self.GroupedObjectsDict
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='MethodsDict':
			self.GettedVariable=SYS.getMethodsDictWithClass(self.__class__)
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='CommoditedVariablesDict':
			self.GettedVariable=filter(
							lambda __DictatedVariable:
							self.__class__ not in type(__DictatedVariable).__mro__,
							self.__dict__.items()
						)
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='OutputedVariablesDict':
			self.GettedVariable=[]
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='FeaturedVariablesDict':
			self.GettedVariable=[]
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='TuplesList':
			self.GettedVariable=zip(self.AppendedKeyStringsList,self.AppendedValueVariablesList)
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='StructuredObjectsTuplesList':
			self.GettedVariable=self.StructuredObjectsTuplesList
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=='ElementedVariablesTuplesList':
			self.GettedVariable=filter(
							lambda __AppendedVariable:
							self.__class__ not in type(__AppendedVariable[1]).__mro__,
							zip(self.AppendedKeyStringsList,self.AppendedValueVariablesList)
						)
			self.IsGettingBool=False
			return 
		elif self.GettingVariable=="__class__":
			self.GettedVariable=self.__class__.__dict__
			self.IsGettingBool=False
			return

		#Do the minimal get
		if IsGettedBool==False:
		
			if type(self.GettingVariable) in [str,unicode]:

				#Get safely the Value
				if self.GettingVariable in self.__dict__:

					#__getitem__ in the __dict__
					self.GettedVariable=self.__dict__[self.GettingVariable]

					#Stop the getting
					self.IsGettingBool=False


	def setBefore(self):
		"""
			Hook in the __setitem__ of the Object 
			with different possible ShortStrings

			If _SettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the setting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is <camelCase> then it is a hook query...
			so it calls with the form self(<camelCase>,SettedVariable)

			If _SettingVariable is of the form <String1>Pointer and _SettedVariable of the form /<String2>
			it gets the self[<String2>] and do self[<String1>Pointer]=self[<String2>]

			Else do a __setitem__ in the __dict__ 
		"""

		#Appending set
		if SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,AppendShortString])==AppendShortString:

			#Define the AppendedKeyString
			AppendedKeyString=self.SettingVariable.split(AppendShortString)[1]

			#Update an already
			if AppendedKeyString in self.AppendedKeyStringToAppendedIntDict:
				self.AppendedValueVariablesList[self.AppendedKeyStringToAppendedIntDict[AppendedKeyString]]=self.SettedVariable
			else:
				#Append
				self.AppendedKeyStringsList+=[AppendedKeyString]
				self.AppendedValueVariablesList+=[self.SettedVariable]
				self.AppendedKeyStringToAppendedIntDict[AppendedKeyString]=len(self.AppendedValueVariablesList)-1

			#If it is an object
			if self.__class__ in type(self.SettedVariable).__mro__:

				#Structure in the Child
				self.SettedVariable.StructuredInt=len(self.AppendedValueVariablesList)-1
				self.SettedVariable.StructuredKeyString=AppendedKeyString
				self.SettedVariable.StructuredParentPointer=self

				#Update the StructuringObjectsList
				self.StructuredObjectsTuplesList.append((AppendedKeyString,self.SettedVariable))

			#Return 
			return

		#Deep set
		elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,DeepShortString])==DeepShortString:

			#Get the SettingVariableStringsList
			SettingVariableStringsList=DeepShortString.join(self.SettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
			GettedVariable=self[SettingVariableStringsList[0]]
			if GettedVariable!=None:
				
				#Define the ChildPathString
				ChildPathString=DeepShortString.join(SettingVariableStringsList[1:])	

				#Direct update in the Child or go deeper with the ChildPathString
				if ChildPathString=="": 
					if self.__class__ in type(GettedVariable).__mro__:

						#Modify directly the GettedVariable with self.SettedVariable
						GettedVariable.__setitem__(*self.SettedVariable)

					elif SYS.getCommonPrefixStringWithStringsList([SettingVariableStringsList[0],AppendShortString])==AppendShortString:
						
						#This is in that case a set in the append but not binded
						AppendedKeyString=SettingVariableStringsList[0].split(AppendShortString)[1]
						if AppendedKeyString in self.AppendedKeyStringToAppendedIntDict:
							self.AppendedValueVariablesList[self.AppendedKeyStringToAppendedIntDict[AppendedKeyString]]=self.SettedVariable
				else:

					#Set in the first deeper Element with the ChildPathString
					GettedVariable[ChildPathString]=self.SettedVariable

			#Return 
			return 

		#Call for a hook
		elif (self.SettingVariable[0].isalpha() or self.SettingVariable[1:2]==["_","_"]) and  self.SettingVariable[0].lower()==self.SettingVariable[0]:

			#Get the Method
			if hasattr(self,self.SettingVariable):

				#Get the method
				SettingMethod=getattr(self,self.SettingVariable)

				#Look for the shape of the inputs of this method
				ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(SettingMethod)

				#Adapt the shape of the args
				if len(ArgsList)>2:
					getattr(self,self.SettingVariable)(*self.SettedVariable)
				elif len(ArgsList)==1:
					getattr(self,self.SettingVariable)()
				else:
					getattr(self,self.SettingVariable)(self.SettedVariable)
			
			elif len(self.SettedVariable)==1:

				#Adapt format of the ArgsList
				self(self.SettingVariable,self.SettedVariable[0])
			else:
				self(self.SettingVariable,*self.SettedVariable)

			#Return
			return

		elif SYS.getCommonSuffixStringWithStringsList([self.SettingVariable,PointerShortString])==PointerShortString:
				
			#Special set for Pointer with a / getted Value
			if type(self.SettedVariable) in [str,unicode]:

				#The Value has to be a deep short string for getting the pointed variable
				if SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,DeepShortString])==DeepShortString:
					GettedVariable=self[self.SettedVariable]
					if GettedVariable!=None:
						#Link the both with a __setitem__
						self[self.SettingVariable]=GettedVariable

				#Return in the case of the Pointer and a string setted variable
				return

		#Get with ExecShortString (it can be a return of a method call)
		elif type(self.SettedVariable) in [str,unicode] and SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,ExecShortString])==ExecShortString:

			self[self.SettingVariable]=self[self.SettedVariable]
			self.IsSettingBool=False
			return

		#Do the minimal set if nothing was done yet
		if type(self.SettingVariable) in [str,unicode]:

			#__setitem__ in the __dict__
			self.__dict__[self.SettingVariable]=self.SettedVariable

			#Group in the Child
			if self.__class__ in type(self.SettedVariable).__mro__ and SYS.getCommonSuffixStringWithStringsList([self.SettingVariable,PointerShortString])!=PointerShortString:
				
				#Group the Child
				self.SettedVariable.GroupedKeyString=self.SettingVariable
				self.SettedVariable.GroupedParentPointer=self
				
				#Update the GroupedObjectsDict
				self.GroupedObjectsDict[self.SettingVariable]=self.SettedVariable

	def delBefore(self):
		"""
			Hook in the __delitem__ of the Object
		"""

		#Do the minimal delitem
		if type(self.DeletingVariable) in [str,unicode]:

			#Del Safely the Value
			if self.DeletingVariable in self.__dict__:
				del self.__dict__[self.DeletingVariable]

	def reprAfter(self):
		"""
			Hook in the repr of the Object
			for printing in the Console attributes in a more 
			user-friendly manner
		"""

		#Get the MroAppendedStringsList
		MroAppendedStringsList=SYS.getMroKeyStringsListWithClass(self.__class__)

		#Remove the NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.NotRepresentingKeyStringsList,
										self.RepresentedTuplesList
								)

		#First keeps only the Specific and New attributes
		self.RepresentedTuplesList=map(
									lambda _RepresentedTuple:
									("<Spe>"+_RepresentedTuple[0],_RepresentedTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] in self.__class__.SpecificKeyStringsList,
											self.RepresentedTuplesList
										)
								)+map(
									lambda _NewTuple:
									("<New>"+_NewTuple[0],_NewTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] not in MroAppendedStringsList,
											self.RepresentedTuplesList
										)
								)

		#Add some forced Values with RepresentingKeyVariables
		self.RepresentedTuplesList+=map(
											lambda _KeyVariable:
											("<NotSpe>"+str(_KeyVariable),self[_KeyVariable]),
											self.RepresentingKeyVariablesList
										)

		#Simplify the numpy variables repr
		self.RepresentedTuplesList=map(
										lambda _RepresentedTuple:
										_RepresentedTuple
										if type(_RepresentedTuple[1]) not in [numpy.ndarray] 
										else (
												_RepresentedTuple[0],
												"<numpy.ndarray shape "+str(numpy.shape(
													_RepresentedTuple[1]))+">"
												),
										self.RepresentedTuplesList
									)

	
	#</DefineHookMethods>

	


#</DefineClass>



