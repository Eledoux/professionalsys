#<ImportModules>
import collections
import copy
import inspect
import numpy
import operator
import os
import ShareYourSystem as SYS
import sys
import tables
import unittest
#</ImportModules>

"""
 coucou erwan, ce message est d'une grande importance... 
 "mmmhhh!! noh oui oh oui!! oooohh!!!"" 
 c'est la reponse a ttes tes annees de recherches intensives en neuroscience!! 
 Tu peux me remercier!!!!
"""

#<DefineLocals>
AddShortString="+"
AttestString='attest'
AttributeShortString="."
DeepShortString="/"
AppendShortString="App_"
PointerShortString="Pointer"
CopyShortString='Cop_'
CollectShortString=":"
ExecShortString="Exec_"
TagString="_"
#</DefineLocals>

#Define for each a test method to be bounded
def setUp(_Test):
	_Test.TestedPointer.TestedInt+=1
	_Test.TestedPointer.TestedVariable=None
	_Test.TestedPointer.TestedString=""

def debug(_String):
	Frame=inspect.currentframe().f_back
	ScriptPathStringsList=SYS.getWordStringsListWithString(Frame.f_code.co_filename)
	ScriptPathString=''.join(ScriptPathStringsList)
	print("%s , %s, l. %s : "%(ScriptPathString,Frame.f_code.co_name,Frame.f_lineno));
	print(_String)
	print("")

#<DefineClass>
class ObjectClass(object):
	
	#<DefineMethods>

		#<HandlingMethods>
	
			#<InitMethod>
	def __init__(self):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""

		#<DefineSpecificDict>

			#<HandlingAttributes>

		#Attributes for the __init__ property
		self.IsInitiatingBool=True

		#Attributes for the __getitem__ property
		self.IsGettingBool=True
		self.GettingVariable=None
		self.GettedVariable=None

		#Attributes for the __setitem__ property
		self.IsSettingBool=True
		self.SettingVariable=None
		self.SettedVariable=None

		#Attributes for the call property
		self.IsCallingBool=False

		#Attributes for the __delitem__ property
		self.IsDeletingBool=True
		self.DeletingVariable=None
		
		#Attributes for the apply property
		self.IsApplyingBool=True
		self.MappedAppliedVariablesList=[]

		#Attributes for the walk property
		self.WalkingDict={}
		self.WalkedOrderedDict=None

		#Attributes for the grabb property
		self.GrabbedVariablesList=[]

		#Attributes for the order property
		self.MappedOrderedVariablesList=[]

			#</HandlingAttributes>

			#<TestingAttributes>
		self.TestingString=""
		self.TestingIsPrintedBool=True
		if hasattr(getattr(SYS,self.__module__),'AttestingFunctionStringsList'):
			self.TestingAttestFunctionStringsList=copy.copy(
				getattr(SYS,self.__module__).AttestingFunctionStringsList)
		else:
			self.TestingAttestFunctionStringsList=[]
		self.TestingFolderPathString=SYS.getCurrentFolderPathString()+'Tests/'
		self.TestedClass=None
		self.TestedOrderedDict=collections.OrderedDict()
		self.TestedInt=-1
		self.TestedVariable=None
		self.TestedString=""
			#</TestingAttributes>

			#<OrderingAttributes>

		#Attributes for the group property
		self.SortedOrderedDict=collections.OrderedDict()
		self.SortedKeyString=""
		self.SortedInt=-1

		#Attributes for the group property
		self.GroupingFilePathString=""
		self.GroupingModuleString="tables"
		self.GroupingFilePathString=""
		self.GroupedFilePointer=None
		self.GroupedOrderedDict=collections.OrderedDict()
		self.GroupedKeyString=""
		self.GroupedInt=-1
		self.GroupedParentPointer=None
		self.GroupedGrandParentPointersList=[]
		self.GroupedPathStringsList=[]
		self.GroupedPathString="/"

		#Attributes for the feature property
		self.FeaturingTuplesList=[]
		self.FeaturedModelClass=None
		self.FeaturedTable=None
		self.FeaturedModeledInt=-1

		#Attributes for the join property
		self.JoinedModeledIntsTuplesList=[]
		self.JoinedModeledIntsList=[]
		self.JoinedTablePointersList=[]
		self.JoinedTuplesList=[]
		self.IsJoinedBool=False


		#Attributes for the output property
		self.OutputingTuplesList=[]
		self.OutputedModelTableClass=None
		self.OutputedTable=None
		self.OutputedJoinedRowedIntsList=[]
		self.OutputedModeledInt=-1

		#Attributes for the store property
		self.StoringTuplesList=[]

			#</ParameterizingAttributes>

			#<NetworkingAttributes>

		#Attributes for the structure property
		self.StructuredOrderedDict=collections.OrderedDict()
		self.StructuredKeyString=""
		self.StructuredInt=-1
		self.StructuredParentPointer=None
		self.StructuredGrandParentPointersList=[]

			#</NetworkingAttributes>

			#<PrintingAttributes>

		#Attributes for the __repr__ property
		self.IsRepresentingBool=True
		self.RepresentingDict={}
		self.RepresentedTuplesList=[]
		self.NotRepresentingGettingVariablesList=filter(
													lambda _KeyString:
													_KeyString not in [

																		'WalkedOrderedDict',

																		'SortedOrderedDict',

																		'GroupedOrderedDict',
																		'GroupedInt',
																		'GroupedKeyString',
																		'GroupedGrandParentPointersList',
																		'GroupedKeyString',
																		'GroupedGrandParentPointersList',
																		'GroupedFilePointer',
																		'GroupedPathString',

																		'JoinedTuplesList',

																		'GrabbedVariablesList',
																		'FeaturedModelClass',
																		#'FeaturedTable'

																		'StructuredOrderedDict',
																		'StructuredInt',
																		'StructuredKeyString',
																		'StructuredGrandParentPointersList',
																		'StructuredPathString'

																		
																	],
													self.__class__.SpecificKeyStringsList
										) if type(self)==SYS.ObjectClass else []

		self.RepresentingKeyVariablesList=[
											'WalkedOrderedDict',

											'SortedOrderedDict',

											'GroupedOrderedDict',
											'GroupedInt',
											'GroupedKeyString',
											'GroupedGrandParentPointersList',
											'GroupedKeyString',
											'GroupedGrandParentPointersList',
											'GroupedFilePointer',
											'GroupedPathString',

											'JoinedTuplesList',
											'GrabbedVariablesList',
											'FeaturedModelClass',
											#'FeaturedTable'

											'StructuredOrderedDict',
											'StructuredInt',
											'StructuredKeyString',
											'StructuredGrandParentPointersList',
											'StructuredPathString'

										] if type(self)!=SYS.ObjectClass else []

			#</PrintingAttributes>

		#</DefineSpecificDict>

		#Hook methods (integrativ)
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="init"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsInitiatingBool==False:
						return 

					#Call the HookingMethod
					HookingMethod(self)

			#</InitMethod>

			#<ExecuteMethod>

	def execute(self,_ExecString):

		exec _ExecString in locals()
		return self

			#</ExecuteMethod>

		#<TestingMethods>

			#<TestingAttestingMethods>

	def bindTestedVariableAfter(self):

		#Bind with TestedString setting
		SYS.PrintingDict['IsIdBool']=False
		self.TestedString=SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(self.TestedVariable,SYS.PrintingDict)
		SYS.PrintingDict['IsIdBool']=False

	def writeAttestWithTestingAttestFunction(self,_TestingAttestFunction):

		#Call the attest function to get the TestedVariable
		self['TestedVariable']=_TestingAttestFunction()

		#Write the TestedString
		File=open(self.TestingFolderPathString+_TestingAttestFunction.__name__+'.txt','w')
		File.write(self.TestedString)
		File.close()

	def attest(self):

		#Check that there is a Folder Tests
		if os.path.isdir(self.TestingFolderPathString)==False:
			os.popen('mkdir '+self.TestingFolderPathString)
		else:
			os.popen("cd "+self.TestingFolderPathString+";rm *")

		#Get the TestingAttestFunctionsList
		Module=getattr(SYS,self.__module__)
		TestingAttestFunctionsList=map(
										lambda TestingAttestFunctionString:
										getattr(Module,TestingAttestFunctionString),
										self.TestingAttestFunctionStringsList
									)

		#Write the TestedString made by each function and append an equivalent test method into the test ordered dict
		map(
				lambda __TestingAttestFunction:
				self.writeAttestWithTestingAttestFunction(__TestingAttestFunction),
				TestingAttestFunctionsList
			)

		#Return self
		return self

			#</TestingAttestingMethods>

			#<TestingMethods>

	def setTestFunctionWithTestingAttestFunction(self,_TestingAttestFunction):

		#Define the TestString
		TestString='at'.join(_TestingAttestFunction.__name__.split('at')[1:])

		def test(_Test):

			#Define the TestingAttestFunction
			TestingAttestFunction=getattr(
									getattr(SYS,_Test.TestedPointer.__module__),
									_Test.TestedPointer.TestingAttestFunctionStringsList[_Test.TestedPointer.TestedInt]
								)

			#Get the AssertedString
			File=open(_Test.TestedPointer.TestingFolderPathString+TestingAttestFunction.__name__+'.txt','r')
			AssertingString=File.read()
			File.close()

			#Call the attest function to get the TestedVariable
			_Test.TestedPointer['TestedVariable']=TestingAttestFunction()

			#Print maybe
			if _Test.TestedPointer.TestingIsPrintedBool:
				#print("\n###########################################")
				#print("")
				#print('AssertingString is :')
				#print(AssertingString)
				#print("")

				#Print maybe
				print('AssertedString is :')
				print(_Test.TestedPointer.TestedString)
				print("")

			#Assert
			#print("a",AssertingString)
			#print("b",_Test.TestedPointer.TestedString)

			_Test.assertEqual(
					#1,1
					AssertingString,
					_Test.TestedPointer.TestedString
			)

		#Copy a form of the test function and name it differently
		test.__name__=TestString

		#Append in the Test OrderedDict
		self['App_Test_'+test.__name__]=test

	def test(self):

		#Get the TestingAttestFunctionsList
		Module=getattr(SYS,self.__module__)
		TestingAttestFunctionsList=map(
										lambda TestingAttestFunctionString:
										getattr(Module,TestingAttestFunctionString),
										self.TestingAttestFunctionStringsList
									)

		#Set the tests for each asserting function
		map(
				lambda __TestingAttestFunction:
				self.setTestFunctionWithTestingAttestFunction(__TestingAttestFunction),
				TestingAttestFunctionsList
			)
			
		#Define the TestClass
		class TestClass(unittest.TestCase):				

			#Bind with the Tested object
			TestedPointer=self

			#Bound the setUp function
			locals().__setitem__(setUp.__name__,setUp)
			
			#Bound each testing function
			for TestedKeyString,TestedMethod in self.TestedOrderedDict.items():
				locals().__setitem__(TestedKeyString,TestedMethod)

			try:
				del TestedKeyString,TestedMethod
			except:
				pass

		#Give a name
		TestClass.__name__=SYS.getClassStringWithTypeString(self.__class__.TypeString+'Test')

		#Set to the TestedClass
		self.TestedClass=TestClass

		#Bound to the unittest runner
		TestLoader=unittest.TestLoader().loadTestsFromTestCase(self.TestedClass)
		unittest.TextTestRunner(verbosity=2).run(TestLoader)

		#</TestingMethods>

		#<CallMethod>

	def reinit(self,**_ReInitiatingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Refresh the attributes and set maybe default values in the _OutputingVariablesDict
		LocalReInitiatingVariablesDict=_ReInitiatingVariablesDict
		LocalReInitiatedPointer=self
		self.IsReInitiatingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='reinit'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalReInitiatingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalReInitiatingVariablesDict' in OutputVariable:
							LocalReInitiatingVariablesDict=OutputVariable['LocalReInitiatingVariablesDict']
						if 'LocalReInitiatedPointer' in OutputVariable:
							LocalReInitiatedPointer=OutputVariable['LocalReInitiatedPointer']

					#Check Bool
					if self.IsReInitiatingBool==False:
						return LocalReInitiatedPointer

		#Return the OutputVariable
		return LocalReInitiatedPointer

	def output(self,**_OutputingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Refresh the attributes and set maybe default values in the _OutputingVariablesDict
		map(
			lambda __DefaultTuple:
			_OutputingVariablesDict.__setitem__(__DefaultTuple[0],__DefaultTuple[1])
			if __DefaultTuple[0] not in _OutputingVariablesDict
			else None,
			[
				('IsRowingBool',False)
			]
		)
		LocalOutputingVariablesDict=_OutputingVariablesDict
		LocalOutputedPointer=self
		self.IsOutputingBool=True
		self.OutputedJoinedRowedIntsList=[]
		self.OldFeaturedModeledInt=-1
		self.NewFeaturedModeledInt=-1
		self.OutputedOldModeledInt=-1
		self.NewOutputedModeledInt=-1
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='output'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalOutputingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalOutputingVariablesDict' in OutputVariable:
							LocalOutputingVariablesDict=OutputVariable['LocalOutputingVariablesDict']
						if 'LocalOutputedPointer' in OutputVariable:
							LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

					#Check Bool
					if self.IsOutputingBool==False:
						return LocalOutputedPointer

		#Return the OutputVariable
		return LocalOutputedPointer

	def __call__(self,_CallVariable,*_CallingVariablesList,**_CallingVariablesDict):
		"""Call the <_CallString><HookString> methods and return self.CalledVariable (self by default)"""

		#Refresh the attributes
		LocalCallVariable=_CallVariable
		LocalCallingVariablesList=_CallingVariablesList
		LocalCallingVariablesDict=_CallingVariablesDict
		LocalCalledPointer=self

		#self.CalledPointer=self
		self.IsCallingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString=LocalCallVariable+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,LocalCallVariable,LocalCallingVariablesList,**LocalCallingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalCallVariable' in OutputVariable:
							LocalCallVariable=OutputVariable['LocalCallVariable']
						if 'LocalCallingVariablesList' in OutputVariable:
							LocalCallingVariable=OutputVariable['LocalCallingVariablesList']
						if 'LocalCallingVariablesDict' in OutputVariable:
							LocalCallingVariablesDict=OutputVariable['LocalCallingVariablesDict']
						if 'LocalCalledPointer' in OutputVariable:
							LocalCalledPointer=OutputVariable['LocalCalledPointer']

					#Check Bool
					if self.IsCallingBool==False:
						return LocalCalledPointer

		#Return the OutputVariable
		return LocalCalledPointer

			#</CallMethod>

			#<GetMethod>

	def __getitem__(self,_GettingVariable):
		"""Call the get<HookString> methods and return self.GettedVariable (None by default)"""
		
		#Refresh the attributes
		self.GettingVariable=_GettingVariable
		self.GettedVariable=None
		self.IsGettingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="get"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Debug
					#debug("HookingMethodString is : "+HookingMethodString)
					#debug("self.GettedVariable is : "+repr(self.GettedVariable))

					#Check Bool
					if self.IsGettingBool==False:
						return self.GettedVariable

		#Return GettedVariable
		return self.GettedVariable

		#</GetMethod>

		#<SetMethods>

			#<GlobalMethod>

	def __setitem__(self,_SettingVariable,_SettedVariable):
		"""Call the set<HookString> methods"""

		#Refresh the attributes
		self.SettingVariable=_SettingVariable
		self.SettedVariable=_SettedVariable
		self.IsSettingBool=True

		#Sort the HookMethodStrings
		for OrderString in ["Before","After"]:

			#Define a Local IsSettedBool
			IsSettedBool=False

			if type(_SettingVariable) in [str,unicode]:
			
				#Define the BindingMethodString
				BindingMethodString="bind"+_SettingVariable+OrderString
				
				#print('setitem',BindingMethodString,BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict)

				#Check that there is HookingMethods for it
				if BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict:
					
					#Say ok
					IsSettedBool=True

					#Call the specific Appended methods 
					for HookingMethod,BindingMethod in self.__class__.BindingMethodStringToMethodTuplesListDict[BindingMethodString]:

						#Call the HookMethod
						if callable(HookingMethod):
							HookingMethod(self)

						#print('hassetted',HookingMethod,BindingMethod)

						#Check Bool
						if self.IsSettingBool==False:
							return self

						#Call the Binding
						if callable(BindingMethod):
							OutputVariable=BindingMethod(self)

							#print('hasbinded')

							#End the Setting
							if self.IsSettingBool==False:
								return self

			#If a binding method was not yet found then look for a global set method
			if IsSettedBool==False:

				#_SettingVariable is not a string
				HookingMethodString="set"+OrderString

				#Check that there is HookingMethods for it
				if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

					#Call the specific Appended methods 
					for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

						#Call the HookingMethod
						HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

		#Return self
		return self

			#</GlobalMethod>

			#<AppendMethod>

	def append(self,_AppendingVariable):
		"""Call __setitem__ with a defined SettingVariable contained in the _AppendingVariable"""
		
		#Init the SettingVariable and Add the TaggingString
		SettingVariable=AppendShortString

		#TuplesList Case
		if SYS.getIsTuplesListBool(
			_AppendingVariable) and SYS.getCommonSuffixStringWithStringsList([_AppendingVariable[0][0],'KeyString'])=='KeyString':
				
				#print("j",
				#	SYS.getHookStringWithHookedString(_AppendingVariable[0][0].split('KeyString')[0]
				#	)+TagString+_AppendingVariable[0][1])
				#Add the hook version
				SettingVariable+=SYS.getHookStringWithHookedString(_AppendingVariable[0][0].split('KeyString')[0])+TagString+_AppendingVariable[0][1]

		else:

			#Object Case
			if SYS.ObjectClass in type(_AppendingVariable).__mro__ :

				Dict=_AppendingVariable.__dict__

			#dict Case
			elif type(_AppendingVariable)==dict:

				Dict=_AppendingVariable

			#Get the KeyStringsList
			GoodKeyTuplesList=filter(
						lambda __ItemTuple:
						SYS.getCommonSuffixStringWithStringsList([__ItemTuple[0],'KeyString'])=='KeyString' and __ItemTuple[1]!="",
						Dict.items()
					)

			#If there is a good KeyString
			if len(GoodKeyTuplesList)==1:

				#print("i",
				#	SYS.getHookStringWithHookedString(GoodKeyTuplesList[0][0].split('KeyString')[0]
				#		)+TagString+GoodKeyTuplesList[0][1])
				SettingVariable+=SYS.getHookStringWithHookedString(GoodKeyTuplesList[0][0].split('KeyString')[0])+TagString+GoodKeyTuplesList[0][1]

		#Then set
		if SettingVariable==AppendShortString:
			SettingVariable+='Sort'+TagString
		return self.__setitem__(SettingVariable,_AppendingVariable)

			#</AppendMethod>

		#</SetMethods>

		#<DelMethod>

	def __delitem__(self,_DeletingVariable):
		"""Call the del<HookString> methods and delete in the __dict__ by default"""
		
		#Refresh the attributes
		self.DeletingVariable=_DeletingVariable
		self.IsDeletingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="delete"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsDeletingBool==False:
						return self

		#Return self
		return self

		#</DelMethod>

	#</HandlingMethods>

	#<MappingMethods>

		#<GlobalMethod>

	def map(self,_MappingFunction,*_MappedList):
		"""Just map"""
		map(_MappingFunction,*_MappedList)
		return self
		#</GlobalMethod>

		#<ApplyingMethods>

			#<GenericMethod>

	def apply(self,_MethodString,_AppliedVariablesList):
		"""Map a call with the _MethodString to the _AppliedVariablesList"""

		#Get the AppliedMethod
		if hasattr(self,_MethodString):
			self.MappedAppliedVariablesList=map(
					lambda __AppliedVariable:
					SYS.getWithMethodAndArgsList(getattr(self,_MethodString),__AppliedVariable)
					if hasattr(self,_MethodString)
					else None,
					_AppliedVariablesList
				)

		#Return self
		return self

			#</GenericMethod>

			#<GetMethods>

				#<PickMethod>

	def pick(self,_GettingVariablesList):
		"""Apply the __getitem__ to the <_GettingVariablesList>"""

		#Apply __getitem__
		self.apply('__getitem__',_GettingVariablesList)

		#Return AppliedVariablesList
		return self.MappedAppliedVariablesList

				#</PickMethod>

				#<GatherMethod>

	def gather(self,_GatheringVariablesList):
		"""Reduce a map of pick for GatheringVariable being a List or __getitem__ for GatheringVariable being a GettingVariable"""
		return reduce(
						operator.__add__,
						map(
								lambda __GatheringVariable:
								zip(__GatheringVariable,self.pick(__GatheringVariable))
								if type(__GatheringVariable)==list
								else self[__GatheringVariable],
								_GatheringVariablesList
						)
					) if len(_GatheringVariablesList)>0 else []

				#<GatherMethod>

				#<CollectMethod>

	def collect(self,_CollectingGatheringVariablesList,_CollectingGatheredVariablesList):
		"""Reduce a map of pick for GatheringVariable being a List or __getitem__ for GatheringVariable being a GettingVariable"""
		
		#Get the GatheredVariablesList
		GatheredVariablesList=zip(*self.gather(_CollectingGatheringVariablesList))
		if len(GatheredVariablesList)>0:

			#Just keep the values
			GatheredVariablesList=GatheredVariablesList[1]

			#Reduce a gather for each
			return reduce(
							operator.__add__,
							map(
									lambda __CollectingObject:
									__CollectingObject.gather(_CollectingGatheredVariablesList),
									GatheredVariablesList
							)
						)

				#</CollectMethod>

			#</GetMethods>

			#<SetMethods>

				#<GlobalMethod>

	def update(self,_SettingVariableTuplesList):
		"""Apply the __setitem__ to the <_SettingVariableTuplesList>"""

		#Apply __setitem__
		return self.apply('__setitem__',_SettingVariableTuplesList)

				#</GlobalMethod>

				#<AppendMethod>

	def __add__(self,_AddingVariablesList):
		"""Apply the append to the <_AddingVariablesList>"""

		#Apply
		self.apply('append',_AddingVariablesList)

		#Return 
		return self

				#</AppendMethod>

			#</SetMethods>

		#</ApplyingMethods>

		#<CommandingMethods>

			#<AllSetsForEachMethod>

	def commandAllSetsForEach(self,_SettingVariableTuplesList,_GatheringVariablesList):
		"""Collect with _GatheringVariablesList and do a all sets for each with _SettingVariableTuplesList"""

		#Get the GatheredVariablesList
		GatheredVariablesList=zip(*self.gather(_GatheringVariablesList))
		if len(GatheredVariablesList)>0:

			#Just keep the values
			GatheredVariablesList=GatheredVariablesList[1]

			#For each __GatheredVariable it is updating with _SettingVariableTuplesList
			map(
					lambda __GatheredVariable:
					__GatheredVariable.update(_SettingVariableTuplesList),
					GatheredVariablesList
				)

		#Return self
		return self 

			#</AllSetsForEachMethod>

			#<EachSetForAllMethod>

	def commandEachSetForAll(self,_SettingVariableTuplesList,_GatheringVariablesList):
		"""Collect with _GatheringVariablesList and do a each set for all with _SettingVariableTuplesList"""

		#Get the GatheredVariablesList
		GatheredVariablesList=zip(*self.gather(_GatheringVariablesList))
		if len(GatheredVariablesList)>0:

			#Just keep the values
			GatheredVariablesList=GatheredVariablesList[1]

			#For each SettingTuple it is setted in _GatheredVariablesList
			map(
					lambda __SettingVariableTuple:
					map(
						lambda __GatheredVariable:
						__GatheredVariable.__setitem__(*__SettingVariableTuple),
						GatheredVariablesList
						),
					_SettingVariableTuplesList
				)

		#Return self
		return self 

			#</EachSetForAllMethod>

		#</CommandingMethods>

		#<ParsingMethods>

			#<WalkMethod>

	def walk(self,_WalkingGatheringVariablesList,**_WalkingDict):
		
		#Init the TopWalkedOrderedDict
		TopWalkedOrderedDict=None
		if 'IdString' not in _WalkingDict:

			#Define the IdString of this walk
			IdString=str(id(_WalkingDict))

			#Set the _WalkingDict
			_WalkingDict.update(
									{
										'IdString':IdString,
										'TopPointer':self,
									}
								)

			#Define TopWalkedOrderedDictKeyString
			TopWalkedOrderedDictKeyString=IdString+'WalkedOrderedDict'

			#Set the corresponding WalkedOrderedDict
			self.__setattr__(
								TopWalkedOrderedDictKeyString,
								collections.OrderedDict(**
								{
									'WalkedInt':-1,
									'TopWalkedIntsList':['/'],
									'GrabbedVariablesOrderedDict':collections.OrderedDict(),
									'RoutedVariablesList':[],
									'TopPointersList':[self]
								})
							)

			#Alias this Dict
			TopWalkedOrderedDict=getattr(self,TopWalkedOrderedDictKeyString)

		else:

			#Get the information at the top
			TopWalkedOrderedDictKeyString=_WalkingDict['IdString']+'WalkedOrderedDict'
			TopWalkedOrderedDict=getattr(_WalkingDict['TopPointer'],TopWalkedOrderedDictKeyString)
			TopWalkedOrderedDict['WalkedInt']+=1
			TopWalkedOrderedDict['TopWalkedIntsList']+=[str(TopWalkedOrderedDict['WalkedInt'])]
			TopWalkedOrderedDict['TopPointersList']+=[self]

		#Refresh the attributes
		self.WalkingDict=_WalkingDict

		#Maybe Grab things...It is important to put his kind of hook before the recursive walk if we want to set from top to down
		if 'GrabbingVariablesList' in _WalkingDict:
			self.grab(_WalkingDict['GrabbingVariablesList'],TopWalkedOrderedDict)

		#Maybe Route
		if 'RoutingVariablesList' in _WalkingDict:
			self.route(_WalkingDict['RoutingVariablesList'],TopWalkedOrderedDict)

		#An Update just before is possible
		if 'BeforeUpdatingTuplesList' in _WalkingDict:
			self.update(_WalkingDict['BeforeUpdatingTuplesList'])

		#Command an recursive order in other gathered variables
		self.commandAllSetsForEach(
									[
										('walk',{
													'ArgsVariable':_WalkingGatheringVariablesList,
													'KwargsDict':_WalkingDict
													})
									],
									_WalkingGatheringVariablesList
								)

		#An Update just after is possible
		if 'AfterUpdatingTuplesList' in _WalkingDict:
			self.update(_WalkingDict['AfterUpdatingTuplesList'])

		#Retrieve the previous Path
		if len(TopWalkedOrderedDict['TopWalkedIntsList'])>0:
			TopWalkedOrderedDict['TopWalkedIntsList']=TopWalkedOrderedDict['TopWalkedIntsList'][:-1] 
			TopWalkedOrderedDict['TopPointersList']=TopWalkedOrderedDict['TopPointersList'][:-1]

		#Return self
		if _WalkingDict['TopPointer']==self:
			self.WalkedOrderedDict=TopWalkedOrderedDict
			del self[TopWalkedOrderedDictKeyString]
			return self

			#</WalkMethod>

			#<GrabMethod>

	def grab(self,_GrabbingVariablesList,_TopWalkedOrderedDict):

		SYS.setWithDictatedVariableAndKeyVariable(
									_TopWalkedOrderedDict['GrabbedVariablesOrderedDict'],
									_TopWalkedOrderedDict['TopWalkedIntsList'],
									collections.OrderedDict(
										self.gather(
											_GrabbingVariablesList
										) if len(_GrabbingVariablesList)>0 else {}
									)
								)
			#</GrabMethod>

			#<RouteMethod>


	def route(self,_RoutingVariablesList,_TopWalkedOrderedDict):

		RoutedVariablesList=None
		RoutedVariablesList=reduce(
				operator.__add__,
				map(
					lambda __TopPointer:
					zip(*__TopPointer.gather(_RoutingVariablesList))[1],
					_TopWalkedOrderedDict['TopPointersList']
					)
			)
		_TopWalkedOrderedDict['RoutedVariablesList'].append(RoutedVariablesList)

			#</RouteMethod>

		#</ParsingMethod>
		
	#</WalkingMethods>

	#</MappingMethods>
	
	#<OrganizingMethods>

		#<ParentizingMethod>

	def parentize(self,_ParentingString):

		#Define the HookedString
		HookedParentingString=SYS.getHookedStringWithHookString(_ParentingString)

		#Define the ParentPointerKeyString
		ParentPointerKeyString=HookedParentingString+"ParentPointer"
		if getattr(self,ParentPointerKeyString)!=None:

			#Set the GrandParentPointersList
			GrandParentPointersListKeyString=HookedParentingString+"GrandParentPointersList"
			setattr(
						self,
						GrandParentPointersListKeyString,
						[getattr(self,ParentPointerKeyString)]+getattr(
															getattr(self,ParentPointerKeyString),
															GrandParentPointersListKeyString
															)
					)

			#Set the KeyStringsList
			KeyStringKeyString=HookedParentingString+"KeyString"
			KeyStringsListKeyString=HookedParentingString+"GrandParentKeyStringsList"
			self.__setattr__(
								KeyStringsListKeyString,
								map(
										lambda __GrandParentPointer:
										getattr(__GrandParentPointer,KeyStringKeyString),
										getattr(self,GrandParentPointersListKeyString)
									)
						)

			#Set the PathStringsList
			PathStringsListKeyString=HookedParentingString+"PathStringsList"
			self.__setattr__(
								PathStringsListKeyString,
								[getattr(self,KeyStringKeyString)]+copy.copy(
									getattr(self,KeyStringsListKeyString)
								)
							)
			getattr(self,PathStringsListKeyString).reverse()

			#Set the PathString
			PathStringKeyString=HookedParentingString+"PathString"
			self.__setattr__(
								PathStringKeyString,
								DeepShortString.join(getattr(self,PathStringsListKeyString))
							)

		#</ParentizingMethod>

		#<GroupingMethods>

	def group(self):

		#For the uppest Parent maybe init a hdf5 file
		if len(self.GroupedGrandParentPointersList)>0:
			UppestGroup=self.GroupedGrandParentPointersList[-1]
		else:
			UppestGroup=self

		#Check for a good FilePathString
		if UppestGroup.GroupingFilePathString!="":

			#Check for first write
			if os.path.isfile(UppestGroup.GroupingFilePathString)==False:
				UppestGroup.GroupedFilePointer=sys.modules[UppestGroup.GroupingModuleString].File(
					UppestGroup.GroupingFilePathString,'w')
				UppestGroup.GroupedFilePointer.close()

		if UppestGroup.GroupedFilePointer==None or ( 
			(UppestGroup.GroupingModuleString=='tables' and UppestGroup.GroupedFilePointer.isopen==0
				) or (UppestGroup.GroupingModuleString=='h5py' and UppestGroup.GroupedFilePointer.mode=='c') ):

			#Open the GroupedFilePointer
			UppestGroup.GroupedFilePointer=sys.modules[UppestGroup.GroupingModuleString].File(
				UppestGroup.GroupingFilePathString,'r+')

		#Point on the GroupingFilePointer of the uppest Parent
		self.GroupedFilePointer=UppestGroup.GroupedFilePointer

		#Create a group in the hdf5 file
		if self.GroupedFilePointer!=None:

			#Make sure that the first char is /
			if self.GroupedPathString[0]!="/":
				self.GroupedPathString="/"+self.GroupedPathString

			#Check if the Path exists
			if self.GroupedPathString not in self.GroupedFilePointer:

				#Set all the intermediate Paths before
				GroupPathStringsList=self.GroupedPathString.split('/')[1:]
				ParsingGroupPathString="/"

				#Set the PathString from the top to the down (integrativ loop)
				for GroupPathString in GroupPathStringsList:

					#Go deeper
					NewParsingGroupPathString=ParsingGroupPathString+GroupPathString

					#Create the group if not already
					if NewParsingGroupPathString not in self.GroupedFilePointer:
						if self.GroupingModuleString=="tables":
							self.GroupedFilePointer.create_group(ParsingGroupPathString,GroupPathString)
						elif self.GroupingModuleString=="h5py":
							Group=self.GroupedFilePointer[ParsingGroupPathString]
							Group.create_group(GroupPathString)
					
					#Prepare the next group	
					ParsingGroupPathString=NewParsingGroupPathString+'/'

		#</GroupingMethod>

		#<TabularingMethods>
			
	def tabular(self,_TabularingString):

		#Define HookedTabularString
		HookedTabularString=SYS.getHookedStringWithHookString(_TabularingString)

		#Define a short alias for the class
		TabularedClass=getattr(self,SYS.getClassStringWithTypeString(HookedTabularString+'Model'))

		#Init the table if it is not already
		TabularedPathString=self.GroupedPathString
		if self.GroupedPathString!='/':
			TabularedPathString+='/'
		TableNameString=self.GroupedKeyString+HookedTabularString+'Table'
		TabularedPathString+=TableNameString

		if TabularedPathString not in self.GroupedFilePointer:

			#Build the Table in the self.TabularedDict
			setattr(self,
						HookedTabularString+'Table',
						self.GroupedFilePointer.create_table(
									self.GroupedFilePointer.getNode(self.GroupedPathString),
									TableNameString,
									TabularedClass,
									TabularedClass.__doc__ 
									if TabularedClass.__doc__!=None 
									else "This is the "+TabularedClass.__name__
						)
					)
		else:

			#Get the Node and set it to the TabularedDict
			setattr(
						self,
						HookedTabularString+'Table',
						self.GroupedFilePointer.getNode(TabularedPathString)
					)

		#</TabularingMethods>

		#<RowingMethods>

	def row(self,_RowingTable,_IdentifyingTuplesList=[],_NotIdentifyingTuplesList=[]):

		'''
		print(map(	
				lambda __Row:
						map(
						lambda __RowingTuple:
						(type(__Row[__RowingTuple[0]]),__RowingTuple[1]),					
						_RowingTuplesList
						),
				Table.iterrows()
				)
			)
		'''		

		#print('row for ',self.GroupedKeyString)
		#print(self.GroupedKeyString,_IdentifyingTuplesList,_NotIdentifyingTuplesList)
		#print("")

		#Refresh the attributes
		self.NewModeledInt=-1
		self.OldModeledInt=-1

		if _RowingTable!=None:

			#Check if it is already appended
			IsAlreadyAppendedBoolsList=map(	
											lambda __Row:
											all(
													map(
														lambda __IdentifyingTuple:
														SYS.getIsEqualBool(
															__Row[__IdentifyingTuple[0]],
															__IdentifyingTuple[1]
														),					
														_IdentifyingTuplesList
													)
												),
											_RowingTable.iterrows()
										)
				
			#print("row is IsAlreadyAppendedBoolsList",IsAlreadyAppendedBoolsList)

			if any(IsAlreadyAppendedBoolsList)==False or _RowingTable.nrows==0:

				#print('row this is a new one')

				#Short alias for the row
				Row=_RowingTable.row

				#Set the RowedInt
				self.NewModeledInt=_RowingTable.nrows

			else:

				#Set the RowedInt where it is already appended
				self.OldModeledInt=IsAlreadyAppendedBoolsList.index(True)

				#print('row this is not a new one')

		#print(self.GroupedKeyString,self.NewModeledInt,self.OldModeledInt)

	def flush(self,_FlushingTable,_FlushingTuplesList):

		#Get the row
		Row=_FlushingTable.row

		#Set the TabularedInt
		Row.__setitem__('ModeledInt',_FlushingTable.nrows)

		#Set the FlushingTuples in the Row
		map(
				lambda __FlushingTuple:
				Row.__setitem__(*__FlushingTuple),
				_FlushingTuplesList
			)

		#Look for dataset
		'''
		.map(	
			lambda __GrindedArrayingColumningString,__GrindedArrayingPickedVariable:
			self.GroupedFilePointer.createArray(
				self.GroupedFilePointer.getNode(self.GroupedPathString),
				str(self[GrindedIntKeyString])+__GrindedArrayingColumningString,
				numpy.array(__GrindedArrayingPickedVariable)
				) if self.GroupedPathString+str(self[GrindedIntKeyString])+__GrindedArrayingColumningString not in self.GroupedFilePointer
			else None,
			*(GrindedArrayingColumningStringsList,
				self.pick(GrindedArrayingGettingStringsList))
		'''

		#Append
		Row.append()
		_FlushingTable.flush()			

		#</RowingMethods>

		#<ScanningMethods>

	def outputWithIsOutputedBoolAndFeaturedModeledInt(self,_IsOutputedBool,_FeaturedModeledInt):

		self.update(
					#Get the FeaturingUpdatingTuplesList
					zip(
						SYS.unzip(self.FeaturingTuplesList,[1]),
						map(
								lambda __ColumnStringsList:
								self.FeaturedTable[_FeaturedModeledInt][__ColumnStringsList],
								SYS.unzip(self.FeaturingTuplesList,[0])
							)
					)
				).output(**{
								'IsFlushedBool':_IsOutputedBool,
								'FeaturedTuplesList':[('ModeledInt',_FeaturedModeledInt)]
							}
						)

	def getOutputedValueVariablesListWithScanningModeledIntsTuple(self,_ScanningModeledIntsTuple):

		#Cast into list
		ScanningModeledIntsList=list(_ScanningModeledIntsTuple)

		#Map an update of the child objects and trigger their output, then update and trigger also the output of the self
		map(
				lambda __IntAndScanningModeledInt:
				self.outputWithIsOutputedBoolAndFeaturedModeledInt(False,__IntAndScanningModeledInt[1])
				if __IntAndScanningModeledInt[0]==len(ScanningModeledIntsList)-1
				else
				SYS.get(
							self.GroupedOrderedDict,
							"values",
							__IntAndScanningModeledInt[0]
						).outputWithIsOutputedBoolAndFeaturedModeledInt(True,__IntAndScanningModeledInt[1]),
				enumerate(ScanningModeledIntsList)
			)

		#Return the OutputedValues of the self
		return self.pick(SYS.unzip(self.OutputingTuplesList,[1]))

	def join(self,**_LocalJoiningVariablesDict):

		if _LocalJoiningVariablesDict=={}:
			_LocalJoiningVariablesDict['IsJoiningBool']=False

		'''
		print('JOIN',self.GroupedKeyString)
		print('for the depper by the way is is already')
		print('self.JoinedModeledIntsTuplesList',self.JoinedModeledIntsTuplesList)
		print('')
		'''

		#Get the GroupedObjectsList
		GroupedObjectsList=self.GroupedOrderedDict.values()
		
		#Init the JoinedModeledIntsTuplesList
		#self.JoinedModeledIntsTuplesList=[0]*len(GroupedObjectsList)

		if len(GroupedObjectsList)==0 or _LocalJoiningVariablesDict['IsJoiningBool']:

			'''
			print('join',self.GroupedKeyString)
			print('We have the right to join !')
			print('len(GroupedObjectsList)',len(GroupedObjectsList))
			print("_LocalJoiningVariablesDict['IsJoiningBool']",_LocalJoiningVariablesDict['IsJoiningBool'])
			print('')
			'''
		
			if self.GroupedParentPointer!=None:

				'''
				print('join',self.GroupedKeyString)
				print('GroupedParentPointer exists')
				print('')
				'''

				if self.GroupedInt==0:

					'''
					print('join',self.GroupedKeyString,self.GroupedInt)
					print('this is the first child so init the self.GroupedParentPointer.JoinedModeledIntsTuplesList')
					print('')
					'''

					self.GroupedParentPointer.JoinedModeledIntsTuplesList=[0]*len(
						self.GroupedParentPointer.GroupedOrderedDict)	

				'''
				print('join',self.GroupedKeyString,self.GroupedInt)
				print('append in the self.GroupedParentPointer.JoinedModeledIntsTuplesList[self.GroupedInt]')
				print('')
				'''
				self.GroupedParentPointer.JoinedModeledIntsTuplesList[self.GroupedInt]=self.FeaturedTable.read(field='ModeledInt')

		else:

			'''
			print('join',self.GroupedKeyString)
			print('This either a not last level of child or it is not yet authorized to join')
			print('len(GroupedObjectsList)',len(GroupedObjectsList))
			print("_LocalJoiningVariablesDict['IsJoiningBool']",_LocalJoiningVariablesDict['IsJoiningBool'])
			print('so join the deeper children groups first')
			print('')
			'''

			map(
					lambda __GroupedObject:
					__GroupedObject.join(),
					GroupedObjectsList
				)

			'''
			print('join',self.GroupedKeyString)
			print('The deeper children groups are joined now')
			print('So join here !')
			print('')
			'''

			self.join(**{'IsJoiningBool':True})

		'''
		print("join END for ",self.GroupedKeyString)
		print('self.JoinedModeledIntsTuplesList',self.JoinedModeledIntsTuplesList)
		print('')
		'''


	def scan(self,**_ScanningVariablesDict):

		'''
		print('')
		print('SCAN begin for ',self.GroupedKeyString)
		'''

		#Init maybe the _ScanningVariablesDict
		if _ScanningVariablesDict=={}:
			_ScanningVariablesDict['IsScanningBool']=False

		#Get the GroupedObjectsList
		GroupedObjectsList=self.GroupedOrderedDict.values()

		#If it is the last layer of group then flush directly all the featuring scanning values
		if len(GroupedObjectsList)==0:
			_ScanningVariablesDict['IsScanningBool']=True

		#Scan the children groups first 
		if _ScanningVariablesDict['IsScanningBool']==False:

			'''
			print('Scan',self.GroupedKeyString)
			print('We dont have the right to scan already, scan first the children...')
			print('')
			'''

			map(
					lambda __GroupedObject:
					__GroupedObject.scan(),
					GroupedObjectsList
				)

			'''
			print('Scan',self.GroupedKeyString)
			print('Children scanned ok')
			'''

		else:

			'''
			print('Scan',self.GroupedKeyString)
			print('We have the right to scan !')
			print('')
			'''

			#Get FeaturingColumnStringsList,FeaturingGettingStringsList
			[FeaturingColumnStringsList,FeaturingGettingStringsList]=SYS.unzip(self.FeaturingTuplesList,[0,1])

			'''
			print('Scan',self.GroupedKeyString)
			print('Make sure that we have joined ...?')
			print('')
			'''

			#Get the JoinedModeledIntsTuplesList
			self.join()
			JoinedStringsList=map(
									lambda __GroupedObject:
									SYS.getModeledIntColumnStringWithGroupedKeyString(
												__GroupedObject.GroupedKeyString
											),
									GroupedObjectsList
								)

			ScanningGettingStringsList=list(FeaturingGettingStringsList)+JoinedStringsList
			ScanningListsList=SYS.unzip(self.FeaturingTuplesList,[3])+self.JoinedModeledIntsTuplesList
			
			'''
			print('Scan',self.GroupedKeyString)
			print('and join is done fore here...')
			print('scan','self.JoinedModeledIntsTuplesList',self.JoinedModeledIntsTuplesList)
			print('scan','JoinedStringsList',JoinedStringsList)
			print('scan','FeaturingGettingStringsList',FeaturingGettingStringsList)
			print('scan','ScanningGettingStringsList',ScanningGettingStringsList)
			print('scan','ScanningListsList',ScanningListsList)
			print('')
			'''

			
			#For each cartesian product of the values update, then output, then store
			"""
			map(
					lambda __ScannedTuple:
					self.update(
									zip(
											ScanningGettingStringsList,
											__ScannedTuple
										)
								).output().store().reinit(),
					SYS.getScannedTuplesListWithScanningListsList(
						ScanningListsList
						)
				)
			"""

			#print('Scan begin loop for ',self.GroupedKeyString)
			#print('')

			for ScannedTuple in SYS.getScannedTuplesListWithScanningListsList(
									ScanningListsList
							):

				'''
				print('Scan, ScannedTuple for',self.GroupedKeyString)
				print("is",zip(
											ScanningGettingStringsList,
											ScannedTuple
										))
				print("")
				'''

				self.update(
								zip(
										ScanningGettingStringsList,
										ScannedTuple
									)
							).output().store().reinit()

		

		#Climb again the branchs to scan the parents
		if self.GroupedParentPointer!=None:

			#If it is the last child group, then order to the parent to scan
			if self.GroupedInt==len(self.GroupedParentPointer.GroupedOrderedDict)-1:

				#print('Scan climb again in the Parent (that was the last child)',
				#	self.GroupedKeyString)
				#print('')

				#Call the parent with the authorisation to scan
				self.GroupedParentPointer.scan(**{'IsScanningBool':True})

		#print('SCAN end for ',self.GroupedKeyString)
		#print('')

		#Return self
		return self
		#<ModelingMethods>

	def model(self,_ModelString):

		#Define the class
		class ModelClass(tables.IsDescription):

			#Add a tabulared Int (just like a unique KEY in mysql...) 
			ModeledInt=tables.Int64Col()

			#Set the Col objects in this class... for is necessary for using the locals()...
			for ColumningString,Col in zip(*SYS.unzip(
													getattr(
																self,
																SYS.getHookingStringWithHookString(_ModelString)+'TuplesList'
															),
															[0,2]
													)):

				#Set the Col 
				locals().__setitem__(ColumningString,Col)

			#If it is the Output table then it has to be the join with all the featuring ModeledInt of the submodels...
			if _ModelString=='Output':	

				#Add a FeaturedModeledInt for the join with the FeaturedModelTable 
				FeaturedModeledInt=tables.Int64Col()

				#Get the child grouped objects
				GroupedObjectsList=self.GroupedOrderedDict.values()
				if len(GroupedObjectsList)>0:

					#Set the JoinedModeledInt
					for ColumningString in map(
												lambda __GroupedObject:
												SYS.getModeledIntColumnStringWithGroupedKeyString(
												__GroupedObject.GroupedKeyString),
												GroupedObjectsList
											):

						#Set the Col 
						locals().__setitem__(ColumningString,tables.Int64Col())

				#Del GroupedObjectsList
				del GroupedObjectsList

			#Del ColumningString,ColClass
			try:
				del ColumningString,Col
			except:
				pass

		#Define the HookedString
		HookedModelString=SYS.getHookedStringWithHookString(_ModelString)+'Model'

		#Give a name of this local defined class
		ModelClass.__name__=SYS.getClassStringWithTypeString(self.__class__.TypeString+HookedModelString)

		#Set the ModelClass to a node
		setattr(
					self,
					SYS.getClassStringWithTypeString(HookedModelString),
					ModelClass
				)

	def organize(self):

		self.update(
						[
							('GroupingFilePathString',SYS.getCurrentFolderPathString()+self.__class__.TypeString+'.hdf5')
						]
					).walk(
								[
									'GroupedOrderedDict.items()'
								],
								**{
									'BeforeUpdatingTuplesList':
									[
										('parentize',{'ArgsVariable':"Group"}),
										('group',{})
									]
								}
							).walk(
									[
										'GroupedOrderedDict.items()'
									],
									**{
										'BeforeUpdatingTuplesList':
										[
											('model',{'ArgsVariable':"Feature"}),
											('tabular',{'ArgsVariable':"Feature"}),
										]
									}
								).walk(
									[
										'GroupedOrderedDict.items()'
									],
									**{
										'BeforeUpdatingTuplesList':
										[
											('model',{'ArgsVariable':"Output"}),
											('tabular',{'ArgsVariable':"Output"}),
										]
									}
								)
		return self

		#</ModelingMethods>

		#<JoiningMethod>

	def grind(self,_ModelString):

		self.walk(
					[
						'GroupedOrderedDict.items()'
					],
					**{
						'BeforeUpdatingTuplesList':
									[
										('model',{'ArgsVariable':_ModelString}),
										('tabular',{'ArgsVariable':_ModelString}),
									]
					}
				).walk(
						[
							'GroupedOrderedDict.items()'
						],
						**{
							'BeforeUpdatingTuplesList':
									[
										('deploy',{'ArgsVariable':_ModelString,'KwargsDict':{}})
									]
						}
					)

		return self


	def bindFeaturedModeledIntAfter(self):

		#Bind with OutputedModeledInt Setting
		if self.OutputedTable!=None:

			IntsList=self.OutputedTable.read_where(
				'FeaturedModeledInt == '+str(self.FeaturedModeledInt),field='ModeledInt'
			)

			#print('bind for',self.GroupedKeyString)
			#print('table is')
			#for r in self.OutputedTable.iterrows():
			#	print "%-16s | %11.1f |" % \
			#	(r['ModeledInt'], r['FeaturedModeledInt'])
			#print('self.FeaturedModeledInt is ',self.FeaturedModeledInt)
			#print('IntsList is ',IntsList)
			#print("")

			if len(IntsList)>0:

				#Get the first
				self.OutputedModeledInt=IntsList[0]

			else:

				self.OutputedModeledInt=-1
				self.FeaturedModeledInt=-1

			#print('bind for',self.GroupedKeyString)
			#print('self.OutputedModeledInt is ',self.OutputedModeledInt)
			#print("")

			if self.OutputedModeledInt>-1:

				#print('bind',self.GroupedKeyString)
				#print('So this a query of changing the object state')
				#print('self.OutputedModeledInt',self.OutputedModeledInt)
				#print('first get the JoinedTuplesList from the OutputedTable to set the children groups')

				#Bind with the setting of all the Featured<GroupedKeyString>ModeledInt
				JoinedTuplesList=filter(
									lambda __ColumStringAndVariable:
									SYS.getCommonSuffixStringWithStringsList(
										[__ColumStringAndVariable[0],'ModeledInt']
										)=='FeaturedModeledInt' and __ColumStringAndVariable[0]!="FeaturedModeledInt",
									zip(
										self.OutputedTable.colnames,
										self.OutputedTable[self.OutputedModeledInt]
										)
									)

				#print('bind',self.GroupedKeyString)
				#print('So the queried JoinedTuplesList are ')
				#print('JoinedTuplesList',JoinedTuplesList)
				#print('Set the <GroupedKeyString>FeaturedModeledInt and inside of the child object for each')
				#print('')
				map(
						lambda __JoinedTuple:
						self.__setitem__(*__JoinedTuple)['App_Group_'+__JoinedTuple[0].split('ModeledInt')[0]].__setitem__(
								'FeaturedModeledInt',
								__JoinedTuple[1]
							),
						JoinedTuplesList,
					)


				#
				#print('bind',self.GroupedKeyString)
				#print('bind','also update the features and the output at this level') 
				'''
				print('New Features are ',
					zip(
										SYS.unzip(self.FeaturingTuplesList,[1]),
										map(
												lambda __ColumnString:
												self.FeaturedTable[self.FeaturedModeledInt][__ColumnString],
												SYS.unzip(self.FeaturingTuplesList,[0])
											)
									)
					)
				print('New Outputs are ',
					zip(
										SYS.unzip(self.OutputingTuplesList,[1]),
										map(
												lambda __ColumnString:
												self.OutputedTable[self.OutputedModeledInt][__ColumnString],
												SYS.unzip(self.OutputingTuplesList,[0])
											)
									)
				)
				print('')
				'''

				self.update(
								zip(
										SYS.unzip(self.FeaturingTuplesList,[1]),
										map(
												lambda __ColumnString:
												self.FeaturedTable[self.FeaturedModeledInt][__ColumnString],
												SYS.unzip(self.FeaturingTuplesList,[0])
											)
									)+
								zip(
										SYS.unzip(self.OutputingTuplesList,[1]),
										map(
												lambda __ColumnString:
												self.OutputedTable[self.OutputedModeledInt][__ColumnString],
												SYS.unzip(self.OutputingTuplesList,[0])
											)
									)
							)
				#print('bind',self.GroupedKeyString)
				#print('Refreshing with FeaturedModeledInt is done !')

		else:

			self.OutputedModeledInt=-1
			self.FeaturedModeledInt=-1


	def reinitBefore(self,**_LocalReinitiatingVariablesDict):

		#Reinit the children groups first
		map(
				lambda __GroupedObject:
				__GroupedObject.reinit(),
				self.GroupedOrderedDict.values()
			)

		self.FeaturedModeledInt=-1
		self.OutputedModeledInt=-1
		self.OldFeaturedModeledInt=-1
		self.NewFeaturedModeledInt=-1
		self.OldOutputedModeledInt=-1
		self.NewOutputedModeledInt=-1

	def outputBefore(self,**_LocalOutputingVariablesDict):

		if self.GroupedKeyString in [
											'',
											'Config'
										]:
			'''
			print('output begin for ',self.GroupedKeyString)
			print('Featuring values are ',zip(	
												SYS.unzip(self.FeaturingTuplesList,[1]),
												self.pick(SYS.unzip(self.FeaturingTuplesList,[1]))
											)
				)
			print("")
			'''

		#Get the GroupedObjectsList
		GroupedObjectsList=self.GroupedOrderedDict.values()

		#Maybe it is already a modeled object so the output is almost already done...
		if self.FeaturedModeledInt>-1:

			'''
			if self.GroupedKeyString in [
											'',
											'Config'
										]:
				print('output',self.GroupedKeyString)
				print('actually we know already its FeaturedModeledInt')
				print('FeaturedModeledInt',self.FeaturedModeledInt)
				print('so just put self.FeaturedOldModeledInt=self.FeaturedModeledInt')
				print('so just put self.OutputedOldModeledInt=self.OutputedModeledInt')
				print('')
			'''

			self.FeaturedOldModeledInt=self.FeaturedModeledInt
			self.OutputedOldModeledInt=self.OutputedModeledInt

		else:

			'''
			if self.GroupedKeyString in [
											'',
											'Config'
										]:
				print('output',self.GroupedKeyString)
				print('FeaturedModeledInt is not known but maybe there are already queried <ChildGroupedKeyString>FeaturedModeledInts')
				print('')
			'''

			#look for <ChildGroupedKeyString>FeaturedModeledInts to see if they are already outputed
			JoiningModeledIntKeyStringsList=map(
													lambda __GroupedObject:
													SYS.getModeledIntColumnStringWithGroupedKeyString(
																__GroupedObject.GroupedKeyString
													),
													GroupedObjectsList
											)

			JoiningTuplesList=map(
									lambda __GroupedObject,__JoiningModeledIntKeyString:
									(
										__GroupedObject.GroupedKeyString,
										getattr(self,__JoiningModeledIntKeyString)
										if hasattr(self,__JoiningModeledIntKeyString)
										else -1	
									),
									GroupedObjectsList,
									JoiningModeledIntKeyStringsList
								)

			'''
			if self.GroupedKeyString in [
												'',
												'Config'
											]:
				print(self.GroupedKeyString,'output the joined grouped children')
				print('The JoiningTuplesList are ',JoiningTuplesList)
				print('So for each, set their FeaturedModeledInt and output them if >-1')
				print("")
			'''

			ChildFeaturedModeledIntsList=map(
					lambda __JoiningTuple,__GroupedObject:
					__GroupedObject.__setitem__(
						'FeaturedModeledInt',
						__JoiningTuple[1]
					).output().FeaturedModeledInt,
					JoiningTuplesList,
					GroupedObjectsList
				)

			'''
			print(self.GroupedKeyString,'the children were joined and setted and ouputed ok...')
			print('ChildFeaturedModeledIntsList is ',ChildFeaturedModeledIntsList)
			print('')
			'''

			#if all(
			#		map(
			#			lambda __ChildFeaturedModeledInt:
			#			__ChildFeaturedModeledInt>-1,
			#			ChildFeaturedModeledIntsList
			#		)
			#	):
		'''
		if self.GroupedKeyString in [
											'',
											'Config'
										]:
			print('Grouped children are outputed ok',self.GroupedKeyString)
			print("")
		'''

		if self.OldFeaturedModeledInt==-1 and self.NewFeaturedModeledInt==-1:

			'''
			if self.GroupedKeyString in [
											'',
											'Config'
										]:
				print(self.GroupedKeyString,'self.OldFeaturedModeledInt==-1 self.NewFeaturedModeledInt==-1')
				print('so we have to see if it is already rowed by matching featuring values')
				print("")
			'''

			#Row in the FeaturedTable
			self.row(
						self.FeaturedTable,
						zip(
								SYS.unzip(self.FeaturingTuplesList,[0]),
								self.pick(SYS.unzip(self.FeaturingTuplesList,[1]))
							),
						[]
					)
			self.OldFeaturedModeledInt=self.OldModeledInt
			self.NewFeaturedModeledInt=self.NewModeledInt

			#Update the FeaturedModeledInt
			if self.OldFeaturedModeledInt>-1:

				#Set FeaturedModeledInt with the binding 
				self['FeaturedModeledInt']=self.OldFeaturedModeledInt
			else:

				#Set FeaturedModeledInt without the binding 
				self.FeaturedModeledInt=self.NewModeledInt

			'''
			if self.GroupedKeyString in [
											'',
											'Config'
										]:
				print('output', self.GroupedKeyString)
				print('results of the row for Features gives :')
				print('self.OldFeaturedModeledInt',self.OldFeaturedModeledInt)
				print('self.NewFeaturedModeledInt',self.NewFeaturedModeledInt)
				print('self.FeaturedModeledInt',self.FeaturedModeledInt)
				print('')
			'''



		else:

			'''
			if self.GroupedKeyString in [
											'',
											'Config'
										]:
				print(self.GroupedKeyString,'self.OldFeaturedModeledInt',self.OldFeaturedModeledInt)
				print('self.NewFeaturedModeledInt',self.NewFeaturedModeledInt)
				print('so we know if it is knew or already rowed')
				print("")
			'''

		if len(GroupedObjectsList)==0:
			self.OutputedOldModeledInt=self.OldFeaturedModeledInt
		if 'OutputedOldModeledInt' in _LocalOutputingVariablesDict:
			self.OutputedOldModeledInt=_LocalOutputingVariablesDict['OutputedOldModeledInt']

		'''
		if self.GroupedKeyString in [
											'',
											'Config'
										]:
			print(self.GroupedKeyString,"check if it is already outputed")
			print('self.OutputedOldModeledInt',self.OutputedOldModeledInt)
			print("")
		'''

		#Look for an already corresponded flushed row
		if self.OldFeaturedModeledInt>-1 and self.OutputedOldModeledInt>-1:

			'''
			if self.GroupedKeyString in [
											'',
											'Config'
										]:
				print(self.GroupedKeyString,'self.OldFeaturedModeledInt==-1 self.OutputedOldModeledInt==-1')
				print("and self.OutputedModeledInt is ",self.OutputedModeledInt)
				print("and self.FeaturedModeledInt is ",self.FeaturedModeledInt)
				print("and self.OldFeaturedModeledInt is ",self.OldFeaturedModeledInt)
				print("")
			'''

			if self.OutputedModeledInt>-1:

				'''
				if self.GroupedKeyString in [
											'',
											'Config'
										]:
					print(self.GroupedKeyString,'self.OutputedModeledInt>-1')
					print('it is already outputed so just update the outputed values with the Table')
					print("")
				'''

				#Then pick the already outputed values in the table 
				self.update(
								zip(
									self.pick(SYS.unzip(self.OutputingTuplesList,[1])),
									map(
											lambda __OutputingColumnString:
											self.OutputedTable[self.OutputedModeledInt][
											__OutputingColumnString],
											SYS.unzip(self.OutputingTuplesList,[0])
										)
									)
							)

				#print(self.GroupedKeyString,"output This is already rowed")

				#Stop the Output process
				self.IsOutputingBool=False
				return

			else:

				if self.GroupedKeyString in [
											'',
											'Config'
										]:
					print(self.GroupedKeyString,"Not found in the Table a FeaturedModeledInt like",
						self.OldFeaturedModeledInt)

		else:

			'''
			if self.GroupedKeyString in [
											'',
											'Config'
										]:
				print(self.GroupedKeyString)
				print('It is not yet outputed')
				print('')
			'''

			if len(GroupedObjectsList)==0:

				'''
				if self.GroupedKeyString in [
											'',
											'Config'
										]:
					print(self.GroupedKeyString,'This is a new value !')
					print('And This is the last level of group so directly')
					print('self.NewOutputedModeledInt=self.NewFeaturedModeledInt')
					print('')
				'''

				self.NewOutputedModeledInt=self.NewFeaturedModeledInt
				

			else:

				#Define the self.JoinedModeledIntsList
				self.JoinedModeledIntsList=map(
						lambda __GroupedObject:
						__GroupedObject.FeaturedModeledInt,
						GroupedObjectsList
					)

				'''
				if self.GroupedKeyString in [
											'',
											'Config'
										]:
					print(self.GroupedKeyString,'Maybe check for the joins :')
					print('self.JoinedModeledIntsList',self.JoinedModeledIntsList)
					print('')
				'''

				'''
				#Output in the children groups first to also get the JoinedRowedIntsList
				self.OutputedJoinedRowedIntsList=map(
											lambda __GroupedObject:
											__GroupedObject.OldFeaturedModeledInt 
											if __GroupedObject.OldFeaturedModeledInt>-1
											else
											__GroupedObject.NewFeaturedModeledInt,
											GroupedObjectsList
										)

				#print(self.GroupedKeyString,"self.OutputedJoinedRowedIntsList",
				#	self.OutputedJoinedRowedIntsList)

				'''
				#Check that all the JoinedModeledInts are good 
				#(maybe a Table was not opened therefore it returns -1)
				if all(
						map(
								lambda __JoinedModeledInt:
								__JoinedModeledInt>-1,
								self.JoinedModeledIntsList
							)
					):

					'''
					if self.GroupedKeyString in [
											'',
											'Config'
										]:
						print(self.GroupedKeyString,'All the __JoinedModeledInt>-1')
						print('so row now in the OutputedTable')
						print('')
					'''

					#Row in the OutputedTable
					self.row(
								self.OutputedTable,
								zip( 
									map(
											lambda __GroupedObject:
											SYS.getModeledIntColumnStringWithGroupedKeyString(
												__GroupedObject.GroupedKeyString
											),
											GroupedObjectsList
										),
									self.JoinedModeledIntsList
								)+[('FeaturedModeledInt',self.FeaturedModeledInt)],
								[]
							)

					self.OldOutputedModeledInt=self.OldModeledInt
					self.NewOutputedModeledInt=self.NewModeledInt

					'''
					if self.GroupedKeyString in [
											'',
											'Config'
										]:
						print('output',self.GroupedKeyString)
						print('results of the row for Outputs gives :')
						print('self.OldOutputedModeledInt',self.OldOutputedModeledInt)
						print('self.NewOutputedModeledInt',self.NewOutputedModeledInt)
						print('')
					'''
		'''
		if self.GroupedKeyString in [
											'',
											'Config'
										]:
			print('END of output for ',self.GroupedKeyString)
			print('')
		'''
		#</JoiningMethod>

	def store(self):

		'''
		if self.GroupedKeyString in [
											'',
											'Config'
										]:
			print('STORE for',self.GroupedKeyString)
			print('self.NewFeaturedModeledInt',self.NewFeaturedModeledInt)
			print('self.NewOutputedModeledInt',self.NewOutputedModeledInt)
		'''

		#Get the GroupedObjectsList
		GroupedObjectsList=self.GroupedOrderedDict.values()

		#Store the children also
		map(
				lambda __GroupedObject:
				__GroupedObject.store(),
				GroupedObjectsList
			)

		if self.NewFeaturedModeledInt>-1:

			'''
			if self.GroupedKeyString in [
											'',
											'Config'
										]:
				print('store',self.GroupedKeyString)
				print('self.NewFeaturedModeledInt>-1')
				print('so we can flush in features !')
				print('')
			'''

			self.flush(
							self.FeaturedTable,
							zip(
									SYS.unzip(self.FeaturingTuplesList,[0]),
									self.pick(SYS.unzip(self.FeaturingTuplesList,[1]))
								)
						)

			'''
			if self.GroupedKeyString in [
											'',
											'Config'
										]:
				print('store',self.GroupedKeyString)
				print('flush in features ok!')
				print('')
			'''

		if self.NewOutputedModeledInt>-1:

			'''
			if self.GroupedKeyString in [
											'',
											'Config'
										]:
				print('store',self.GroupedKeyString)
				print('self.NewOutputedModeledInt>-1')
				print('so we can flush in outputs!')
				print('')
			'''

			self.flush(
						self.OutputedTable,
						zip(
								SYS.unzip(self.OutputingTuplesList,[0]),
								self.pick(SYS.unzip(self.OutputingTuplesList,[1]))
							)+zip( 
									map(
											lambda __GroupedObject:
											SYS.getModeledIntColumnStringWithGroupedKeyString(
												__GroupedObject.GroupedKeyString
											),
											GroupedObjectsList
										),
									self.JoinedModeledIntsList
								)+[('FeaturedModeledInt',self.FeaturedModeledInt)]
						)

			'''
			if self.GroupedKeyString in [
											'',
											'Config'
										]:
				print('store',self.GroupedKeyString)
				print('flush in outputs ok!')
				print('')
			'''

		'''
		if self.GroupedKeyString in [
											'',
											'Config'
										]:
			print('END STORE for',self.GroupedKeyString)
			print('')
		'''

		#Return self
		return self

		#<DeployingMethods>
	
	def deploy(self,_ModelString,**_DeployingDict):

		#Init maybe the _DeployingDict
		if _DeployingDict=={}:
			if _ModelString=="Feature":
				_DeployingDict={
								'IsScanningBool':True
							}
			elif _ModelString=="Output":
				_DeployingDict={
								'IsScanningBool':False
							}

		#If it is one of the deepest leaf...Then begin the grind of the Output!
		if len(self.GroupedOrderedDict)==0 or ('IsScanningBool' in _DeployingDict and _DeployingDict['IsScanningBool']):

			#Set to True
			_DeployingDict['IsScanningBool']=True

			#Scan for here
			self.scan(_ModelString)

		#If it is the last of the child then autorize the GroupedParentPointer to scan also for the Output
		if _ModelString=="Output" and _DeployingDict['IsScanningBool']:
			if self.GroupedParentPointer!=None:
				print("ii",self.GroupedKeyString,len(self.GroupedParentPointer.GroupedOrderedDict),self.GroupedInt+1)
				if len(self.GroupedParentPointer.GroupedOrderedDict)==self.GroupedInt+1:

					#Grind for the grouped parent
					self.GroupedParentPointer.deploy(_ModelString,**{'IsScanningBool':True})


		#</DeployingMethods>

		#<GlobalMethod>

	def close(self):

		#Close the GroupedFilePointer
		self.GroupedFilePointer.close()

		#Return self
		return self

		#</GlobalMethod>

	#</OrganizingMethods>

	#</ParentizingMethod>

	#<PrintingMethods>

		#<ReprMethod>

	def __repr__(self):
		"""Call the repr<HookString> methods and return a SYS printed version of the self.PrintedDict"""

		#Refresh the attributes
		self.RepresentingDict=copy.deepcopy(SYS.PrintingDict)
		self.RepresentedTuplesList=self.__dict__.items()
		self.IsRepresentingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="repr"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsRepresentingBool==False:
						return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
								self,
								self.RepresentedDict
								)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
								dict(self.RepresentedTuplesList),
								self.RepresentingDict
								)

		#return the Print
		return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
				self,
				self.RepresentingDict
				)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
				dict(self.RepresentedTuplesList),
				self.RepresentingDict
				)

		#</ReprMethod>

	#</PrintingMethods>

	#</DefineMethods>

	#<DefineHookMethods>

	def getBefore(self):
		"""
			Hook in the __getitem__ of the Object 
			with different possible ShortStrings

			If _GettingVariable is an int, it returns the Value in the corresponding Index
			in self.AppendedValueVariablesList

			If _GettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			If _GettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			Else do a __getitem__ in the __dict__
		"""

		#Debug
		#debug("self.GettingVariable is : "+repr(self.GettingVariable))
		#debug("self.GettedVariable is : "+repr(self.GettedVariable))

		#Init a local IsGettedBool
		IsGettedBool=False

		#Case of a GettingString
		if type(self.GettingVariable) in [str,unicode]: 

			#Get with ExecShortString (it can be a return of a method call)
			if SYS.getCommonPrefixStringWithStringsList(
				[self.GettingVariable,ExecShortString])==ExecShortString:

				#Define the ExecString
				ExecString=self.GettingVariable.split(ExecShortString)[1]

				#Put the output in a local Local Variable
				exec ExecString in locals()

				#Give it to self.GettedVariable
				self.IsGettingBool=False
				return 

			#Get an Attribute (it can be a return of a method call)
			elif AttributeShortString in self.GettingVariable:

				#Define the GettingStringsList
				GettingStringsList=self.GettingVariable.split(AttributeShortString)

				#Look if the deeper getter is Ok
				GetterVariable=self[GettingStringsList[0]]

				#Define the next getting String
				NextGettingString=GettingStringsList[1]

				#If the GettingStringsList has brackets in the end it means a call of a method
				if "()"==NextGettingString[-2:]:
					NextGettingString=NextGettingString[:-2]

					#Case where it is an object
					if hasattr(GetterVariable,NextGettingString):
						self.GettedVariable=getattr(GetterVariable,NextGettingString)()
						self.IsGettingBool=False
						return 

					elif SYS.getIsTuplesListBool(GetterVariable):

						if NextGettingString=='keys':
							self.GettedVariable=map(
														lambda __ListedTuple:
														__ListedTuple[0],
														GetterVariable
													)
							self.IsGettingBool=False
							return 
						if NextGettingString=='values':
							self.GettedVariable=map(	
														lambda __ListedTuple:
														__ListedTuple[1],
														GetterVariable
													)
							self.IsGettingBool=False
							return 

				elif hasattr(GetterVariable,NextGettingString):

					#Else it is a get of an item in the __dict__
					GettingVariable=getattr(GetterVariable,NextGettingString)

					#Case where it has to continue to be getted
					if len(GettingStringsList)>2:
						GettingVariable[AttributeShortString.join(GettingStringsList[2:])]
						self.IsGettingBool=False
						return 
					else:

						#Else return the GettingVariable
						self.GettedVariable=GettingVariable
						self.IsGettingBool=False
						return 

			#Get with DeepShortString
			elif self.GettingVariable==DeepShortString:
				
				#If it is directly DeepShortString
				self.GettedVariable=self

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return 

			#Get in a ordered dict
			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,AppendShortString])==AppendShortString:

				#Define the appended TaggingKeyString
				AppendingString=self.GettingVariable.split(AppendShortString)[1]

				#Define the appended TagString and KeyString
				SplittedStringsList=AppendingString.split(TagString)
				TaggingString=SplittedStringsList[0]
				KeyString=TagString.join(SplittedStringsList[1:])

				#Get the corresponding ordered lists and dict
				HookedTagString=SYS.getHookedStringWithHookString(TaggingString)
				OrderedDict=getattr(self,HookedTagString+'OrderedDict')

				#Get with a digited KeyString case
				if KeyString.isdigit():

					#Define the GettingInt
					GettingInt=(int)(KeyString)

					#Check if the size is ok
					if GettingInt<len(OrderedDict):

						#Get the GettedVariable 
						self.GettedVariable=SYS.get(OrderedDict,'values',GettingInt)
						#Stop the getting
						self.IsGettingBool=False

						#Return
						return

				#Get in the ValueVariablesList
				elif KeyString in OrderedDict:
					
					#Get the GettedVariable
					self.GettedVariable=OrderedDict[KeyString]

					#Stop the getting
					self.IsGettingBool=False

					#Return 
					return
			
			#Get with '/'
			elif self.GettingVariable==DeepShortString:
				
				#If it is directly '/'
				self.GettedVariable=self

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return 

			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,DeepShortString])==DeepShortString:

				#Define the GettingVariableStringsList
				GettingVariableStringsList=DeepShortString.join(self.GettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
				GettedVariable=self[GettingVariableStringsList[0]]
				if GettedVariable!=None:

					if len(GettingVariableStringsList)==1:

						#Direct update in the Child or go deeper with the ChildPathString
						self.GettedVariable=GettedVariable

						#Stop the getting
						self.IsGettingBool=False
					
						#Return 
						return 

					else:

						#Define the ChildPathString
						ChildPathString=DeepShortString.join(GettingVariableStringsList[1:])	

						#Get in the first deeper Element with the ChildPathString
						self.GettedVariable=GettedVariable[ChildPathString]

						#Stop the getting
						self.IsGettingBool=False

						#Return
						return 

			elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,CopyShortString])==CopyShortString:

				#deepcopy
				self.GettedVariable=copy.deepcopy(
						self[CopyShortString.join(self.GettingVariable.split(CopyShortString)[1:])]				
					)

				#Stop the getting
				self.IsGettingBool=False

				#Return 
				return 

			elif self.GettingVariable=='UtilitiesDict':
				self.GettedVariable=self.__dict__
				self.IsGettingBool=False
				return 
			elif self.GettingVariable=='MethodsDict':
				self.GettedVariable=SYS.getMethodsDictWithClass(self.__class__)
				self.IsGettingBool=False
				return  
			elif self.GettingVariable=="__class__":
				self.GettedVariable=self.__class__.__dict__
				self.IsGettingBool=False
				return

		'''
		#Case of an Object
		elif SYS.ObjectClass in type(self.GettingVariable).__mro__:

			#If the GettingVariable is an object already then return this
			self.GettedVariable=self.GettingVariable
			self.IsGettingBool=False
			return 
		'''

		#Do the minimal get in the Utilities
		if IsGettedBool==False:
		
			if type(self.GettingVariable) in [str,unicode]:

				#Get safely the Value
				if self.GettingVariable in self.__dict__:

					#__getitem__ in the __dict__
					self.GettedVariable=self.__dict__[self.GettingVariable]

					#Stop the getting
					self.IsGettingBool=False


	def setBefore(self):
		"""
			Hook in the __setitem__ of the Object 
			with different possible ShortStrings

			If _SettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the setting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is <camelCase> then it is a hook query...
			so it calls with the form self(<camelCase>,SettedVariable)

			If _SettingVariable is of the form <String1>Pointer and _SettedVariable of the form /<String2>
			it gets the self[<String2>] and do self[<String1>Pointer]=self[<String2>]

			Else do a __setitem__ in the __dict__ 
		"""

		#print('set',self.SettingVariable)

		#Case of a SettingString
		if type(self.SettingVariable) in [str,unicode]:

			#Adding set
			if SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,AddShortString])==AddShortString:

				#Define the added GettingString
				GettingString=self.SettingVariable.split(AddShortString)[1]

				#Get the added Variable
				Variable=self[GettingString]

				#Add
				if hasattr(Variable,'__add__'):
					Variable+=self.SettedVariable

				#Return
				return

			#Appending set
			elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,AppendShortString])==AppendShortString:

				#Define the appended TaggingKeyString
				TaggingKeyString=self.SettingVariable.split(AppendShortString)[1]

				#Define the appended TagString,KeyString,HookedTagString,OrderedDictKeyString
				SplittedStringsList=TaggingKeyString.split(TagString)
				TaggingString=SplittedStringsList[0]
				KeyString=TagString.join(SplittedStringsList[1:])
				HookedTagString=SYS.getHookedStringWithHookString(TaggingString)
				OrderedDictKeyString=HookedTagString+'OrderedDict'
				try:

					#Get the corresponding OrderedDict
					OrderedDict=getattr(self,OrderedDictKeyString)
				except AttributeError:

					#Set it for the first time else
					self.__setattr__(OrderedDictKeyString,collections.OrderedDict())
					OrderedDict=getattr(self,OrderedDictKeyString)

				#Append (or set if it is already in)
				OrderedDict[KeyString]=self.SettedVariable

				#If it is an object
				if SYS.ObjectClass in type(self.SettedVariable).__mro__:

					#Childify
					self.SettedVariable.__setattr__(HookedTagString+'Int',len(OrderedDict)-1)
					self.SettedVariable.__setattr__(HookedTagString+'KeyString',KeyString)
					self.SettedVariable.__setattr__(HookedTagString+'ParentPointer',self)

				#Return 
				return

			#Deep set
			elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,DeepShortString])==DeepShortString:

				#Get the SettingVariableStringsList
				SettingVariableStringsList=DeepShortString.join(self.SettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
				GettedVariable=self[SettingVariableStringsList[0]]
				if GettedVariable!=None:
					
					#Define the ChildPathString
					ChildPathString=DeepShortString.join(SettingVariableStringsList[1:])	

					#Direct update in the Child or go deeper with the ChildPathString
					if ChildPathString=="": 
						if self.__class__ in type(GettedVariable).__mro__:

							#Modify directly the GettedVariable with self.SettedVariable
							GettedVariable.__setitem__(*self.SettedVariable)

						elif SYS.getCommonPrefixStringWithStringsList([SettingVariableStringsList[0],AppendShortString])==AppendShortString:
							
							#Define the appended TaggingKeyString
							TaggingKeyString=self.SettingVariable.split(AppendShortString)[1]

							#Define the appended TagString and KeyString
							SplittedStringsList=TaggingKeyString.split(TagString)
							TaggingString=SplittedStringsList[0]
							KeyString=TagString.join(SplittedStringsList[1:])

							#Get the corresponding ordered lists and dict
							HookedTagString=SYS.getHookedStringWithHookString(TaggingString)
							OrderedDict=getattr(self,HookedTagString+'OrderedDict')

							#Append without binding
							if KeyString in OrderedDict:
								OrderedDict[KeyString]=self.SettedVariable
					else:

						#Set in the first deeper Element with the ChildPathString
						GettedVariable[ChildPathString]=self.SettedVariable

				#Return 
				return 

			#Call for a hook
			elif (self.SettingVariable[0].isalpha() or self.SettingVariable[:2]=="__") and  self.SettingVariable[0].lower()==self.SettingVariable[0]:

				#Get the Method
				if hasattr(self,self.SettingVariable):

					#Get the method
					SettingMethod=getattr(self,self.SettingVariable)

					#Adapt the shape of the args
					SYS.getWithMethodAndArgsList(
													SettingMethod,
													self.SettedVariable['ArgsVariable'] if 'ArgsVariable' in self.SettedVariable else [],
													**self.SettedVariable['KwargsDict'] if 'KwargsDict' in self.SettedVariable else {}
												)
				
				elif len(self.SettedVariable)==1:

					#Adapt format of the ArgsList
					self(self.SettingVariable,self.SettedVariable[0])
				else:
					self(self.SettingVariable,*self.SettedVariable)

				#Return
				self.IsSettingBool=False
				return

			elif type(self.SettedVariable) in [str,unicode]:

				if SYS.getCommonSuffixStringWithStringsList(
					[self.SettingVariable,PointerShortString])==PointerShortString:
					
					#The Value has to be a deep short string for getting the pointed variable
					if SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,DeepShortString])==DeepShortString:
						GettedVariable=self[self.SettedVariable]
						if GettedVariable!=None:
							#Link the both with a __setitem__
							self[self.SettingVariable]=GettedVariable

					#Return in the case of the Pointer and a string setted variable
					return

				#Get with ExecShortString (it can be a return of a method call)
				elif SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,ExecShortString])==ExecShortString:

					self[self.SettingVariable]=self[self.SettedVariable]
					self.IsSettingBool=False
					return

			#__setitem__ in the __dict__, this is an utility set
			self.__dict__[self.SettingVariable]=self.SettedVariable

	def deleteBefore(self):
		"""
			Hook in the __delitem__ of the Object
		"""

		#Do the minimal delitem
		if type(self.DeletingVariable) in [str,unicode]:

			#Del Safely the Value
			if self.DeletingVariable in self.__dict__:
				del self.__dict__[self.DeletingVariable]

	def reprAfter(self):
		"""
			Hook in the repr of the Object
			for printing in the Console attributes in a more 
			user-friendly manner
		"""

		#Get the MroAppendedStringsList
		MroAppendedStringsList=SYS.getMroKeyStringsListWithClass(self.__class__)

		#Remove the NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.NotRepresentingGettingVariablesList,
										self.RepresentedTuplesList
								)

		#First keeps only the Specific and New attributes
		self.RepresentedTuplesList=map(
									lambda _RepresentedTuple:
									("<Spe>"+_RepresentedTuple[0],_RepresentedTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] in self.__class__.SpecificKeyStringsList,
											self.RepresentedTuplesList
										)
								)+map(
									lambda _NewTuple:
									("<New>"+_NewTuple[0],_NewTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] not in MroAppendedStringsList,
											self.RepresentedTuplesList
										)
								)

		#Add some forced Values with RepresentingKeyVariables
		self.RepresentedTuplesList+=map(
											lambda _KeyVariable:
											("<NotSpe>"+str(_KeyVariable),self[_KeyVariable]),
											self.RepresentingKeyVariablesList
										)

		#Simplify the numpy variables repr
		self.RepresentedTuplesList=map(
										lambda _RepresentedTuple:
										_RepresentedTuple
										if type(_RepresentedTuple[1]) not in [numpy.ndarray] 
										else (
												_RepresentedTuple[0],
												"<numpy.ndarray shape "+str(numpy.shape(
													_RepresentedTuple[1]))+">"
												),
										self.RepresentedTuplesList
									)

	
	#</DefineHookMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.ObjectClass()

def attest_getInNotOrderedDict():

	#Define an Object
	Object=SYS.ObjectClass().update([
						('MyInt',0),
						('MyObject',SYS.ObjectClass().update(
							[
								('MyString',"hello"),
								('MyFloat',0.1)
							]
							)
						)
					]
				)
	#Map some gets
	TestedVariable=map(
							lambda _KeyVariable:
							Object[_KeyVariable],
							[
								#Get directly in the __dict__
								'MyInt',
								'MyObject',

								#Get with copying
								'Cop_MyObject',

								#Get with a DeepShortString
								'/MyObject/MyString',

								#Get by the Object directly
								#Object
							]
					)

	#Change in the objects for looking at the copy difference
	Object['/MyObject/MyFloat']=0.4444

	#Bind
	return TestedVariable

def attest_getInOrderedDict():

	#Define an Object
	Object=SYS.ObjectClass().update([
										('App_Group_MyAppendedObject',SYS.ObjectClass().update(
											[
												('MyOtherString',"bonjour"),
												('MyOtherFloat',0.2)
											]
											)
										),
										('App_Group_MySecondInt',3)
									])
	#Map some gets
	TestedVariable=map(
								lambda _KeyVariable:
								Object[_KeyVariable],
								[
									#Get with a AppendShortString
									'App_Group_MySecondInt',
									'App_Group_MyAppendedObject',
									'Cop_App_Group_MyAppendedObject',
									'/App_Group_MyAppendedObject/MyOtherString',
									#Or the AppendedInt
									'App_Group_0',

									#Get in the class object
									'/__class__/TypeString'

								]
							)

	#Change in the objects for looking at the copy difference
	Object['/App_Group_MyAppendedObject/MyOtherFloat']=0.5555

	#Return 
	return TestedVariable

def attest_getWithAttributeString():

	#Define an ObjectGetterVariable
	Object=SYS.ObjectClass().update([
										('MyDict',{'MyInt':0}),
										('MyObject',SYS.ObjectClass().__setitem__('MySecondInt',2))
									]
								)

	#Get with AttributeShortString
	return [
							Object['MyDict.values()'],
							Object['MyObject.MySecondInt']
						]

def attest_setInNotOrderedDict():

	#Explicit expression
	Object=SYS.ObjectClass().__setitem__('MyInt',0)
	
	#Set with a deep short string
	Object.__setitem__('MyObject',SYS.ObjectClass()).__setitem__('/MyObject/MyString','hello')

	#Set with a deep deep short string
	Object.__setitem__('/MyObject/MyGrandOtherObject',SYS.ObjectClass())

	#Set with a pointer short string
	return Object.__setitem__('MyObjectPointer','/MyObject')

def attest_setInOrderedDict():

	#Short expression and set in the appended manner
	Object=SYS.ObjectClass().__setitem__('App_Sort_MySecondInt',1)

	#Short expression for setting in the append without binding
	Object['/App_Sort_MySecondInt']+=1

	#Short expression for setting in the appended manner a structured object
	Object['App_Group_MyAppendedObject']=SYS.ObjectClass()
	
	#Set with a deep short string and append string
	Object['/App_Group_MyAppendedObject/MyString']="bonjour"

	#Set with a deep deep short string and append string
	Object['/App_Group_MyAppendedObject/App_Group_MyGrandChildObject']=SYS.ObjectClass()

	#Return Object
	return Object

def attest_setWithMethodString():

	#Set for being a call of method
	return SYS.ObjectClass().__setitem__('__setitem__',{'ArgsVariable':('MySecondInt',1)})

def attest_hook():

	#Define a derived class and set it with SYS
	class BindingObjectClass(SYS.ObjectClass):

		#<DefineHookMethods>
		def setBefore(self):

			#Convert like a deserialization
			if SYS.getCommonSuffixStringWithStringsList([
				self.SettingVariable,'String'
				])=='String':
				self.SettingVariable=self.SettingVariable.split('String')[0]+'Int'
				self.SettedVariable=(int)(self.SettedVariable)
				self[self.SettingVariable]=self.SettedVariable
				self.IsSettingBool=False

		#</DefineHookgMethods>

		#<DefineBindingMethods>
		def bindMyIncrementedIntAfter(self):
			self.MyIncrementedInt+=1
		def bindApp_Sort_MyIncrementedIntAfter(self):
			self['/App_Sort_MyIncrementedInt']+=1
		#</DefineBindingMethods>

	SYS.setClassWithClass(BindingObjectClass)

	#Set to call the bind
	return BindingObjectClass().__setitem__(
									'MyIncrementedString',
									"0"
								).__setitem__(
									'App_Sort_MyIncrementedString',
									"2"
								)

def attest_apply():

	#Apply a __setitem__ to several things 
	Object=SYS.ObjectClass().apply('__setitem__',[
													('MyString',"Hello"),
													('MySecondString',"Bonjour")
												]
									)

	#Apply an apply is possible 
	return Object.apply(
							'__setitem__',
							[
											('MyThirdString',"GutenTag"),
											('apply',{'ArgsVariable':(
															'__setitem__',[
																		('MyInt',0)
																		]
																)
													}
											),
											('MyNotLostString',"ben he"),
							]
				)

def attest_pick():

	#Define an Object
	Object=SYS.ObjectClass().update([
										('MyInt',0),
										('MyObject',SYS.ObjectClass().update(
											[
												('MyString',"hello"),
												('MyFloat',0.1)
											]
											)
										),
										('App_Group_MyAppendedObject',SYS.ObjectClass().update(
											[
												('MyOtherString',"bonjour"),
												('MyOtherFloat',0.2)
											]
											)
										),
										('App_Sort_MySecondInt',3)
									])
	#Map some gets
	TestedVariable=Object.pick(
								[
									#Get directly in the __dict__
									'MyInt',
									'MyObject',

									#Get with copying
									'Cop_MyObject',

									#Get with a DeepShortString
									'/MyObject/MyString',

									#Get with a AppendShortString
									'App_Sort_MySecondInt',
									'App_Group_MyAppendedObject',
									'Cop_App_Group_MyAppendedObject',
									'/App_Group_MyAppendedObject/MyOtherString',
									#Or the AppendedInt
									'App_Group_0',

									#Get in the class object
									'/__class__/TypeString'
								]
							)

	#Change in the objects for looking at the copy difference
	Object['/MyObject/MyFloat']=0.4444
	Object['/App_Group_MyAppendedObject/MyOtherFloat']=0.5555

	#Return
	return TestedVariable

def attest_gather():

	return SYS.ObjectClass().__add__(
			[
				SYS.ObjectClass().update(
					[
						('StructuredKeyString',str(Int1))
					]).__add__(
						[
							SYS.ObjectClass().update(
										[
											('StructuredKeyString',str(Int2))
										]
									)
							for Int2 in xrange(2)
						]
					) for Int1 in xrange(2)
			]).update(
			[
				('App_Group_'+str(Int1),SYS.ObjectClass().update(
					[
						('GroupedKeyString',str(Int1))
					]).update(
						[
							('App_Group_'+str(Int2),SYS.ObjectClass().update(
										[
											('GroupedKeyString',str(Int2))
										]
									)
							)
							for Int2 in xrange(2)
						]
					)
				) for Int1 in xrange(2)
			]).gather([
							['/'],
							'StructuredOrderedDict.items()',
							'GroupedOrderedDict.items()'
						])

def attest_collect():

	Object=SYS.ObjectClass().__add__(
			[
				SYS.ObjectClass().update(
					[
						('StructuredKeyString',str(Int1))
					]).__add__(
						[
							SYS.ObjectClass().update(
										[
											('StructuredKeyString',str(Int2))
										]
									)
							for Int2 in xrange(2)
						]
					) for Int1 in xrange(2)
			])

	return Object.collect([
							['/'],
							'StructuredOrderedDict.items()',
							['App_Structure_1'],
							#[Object['App_Structure_0']]
						],
						[
							['StructuredKeyString']
						]
					)

def attest_update():

	#Update several things
	Object=SYS.ObjectClass().update([
										('MyInt',0),
										('App_Sort_MySecondInt',0),
										('MyFloat',0.2)
									]
								)

	#Update in a update is possible
	return Object.update([
										('MyString',"Hello"),
										('update',{'ArgsVariable':[
													('MySecondInt',2)
													]}
										),
										('MyNotLostString',"ben heee")
									]
								)

def attest_append():

	#Append with a TuplesList
	Object=SYS.ObjectClass().append([
										('StructuredKeyString',"MyTuplesList"),
										('MyString',"Hello")
									]
									)

	#Append with a dict
	Object.append({
						'StructuredKeyString':"MyDict",
						'MyOtherString':"Bonjour"
						}
					)

	#Append with an Object
	return Object.append(SYS.ObjectClass())

def attest_add():

	#Explicit expression
	return SYS.ObjectClass().__add__([
												[
													('StructuredKeyString',"MyTuplesList"),
													('MyString',"Hello")
												],
												{
													'StructuredKeyString':"MyDict",
													'MyOtherString':"Bonjour"
												},
												SYS.ObjectClass()
											])

def attest_commandAllSetsForEach():

	return SYS.ObjectClass().__add__(
			[
				SYS.ObjectClass().update(
					[
						('StructuredKeyString',str(Int1))
					]) for Int1 in xrange(2)
			]
		).execute('self.__class__.CountingInt=0').commandAllSetsForEach(
				[
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.GettedVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					),
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.GettedVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					)
				],
				[
					['/'],
					'StructuredOrderedDict.items()'
				])

def attest_commandEachSetForAll():
	
	return SYS.ObjectClass().__add__(
			[
				SYS.ObjectClass().update(
					[
						('StructuredKeyString',str(Int1))
					]) for Int1 in xrange(2)
			]
		).execute('self.__class__.CountingInt=0').commandEachSetForAll(
				[
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.GettedVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					),
					(
						'MyCountingInt',
						';'.join([
									"Exec_self.GettedVariable=self.__class__.CountingInt",
									"self.__class__.CountingInt+=1"
								])
					)
				],
				[
					['/'],
					'StructuredOrderedDict.items()'
				])

def attest_walk():
	
	return SYS.ObjectClass().__add__(
			[
				SYS.ObjectClass().update(
					[
						('StructuredKeyString',str(Int1))
					]).__add__(
						[
							SYS.ObjectClass().update(
										[
											('StructuredKeyString',str(Int2))
										]
									)
							for Int2 in xrange(2)
						]
					) for Int1 in xrange(2)
			]
		).walk(
					[
						'StructuredOrderedDict.items()'
					],
					**{
							'BeforeUpdatingTuplesList':[
									('MyCoOrderedInt',2)
								],
							'GrabbingVariablesList':
							[
								['StructuredKeyString']
							],
							'RoutingVariablesList':
							[
								['StructuredKeyString']
							]
						}
				)
	
def attest_parentize():

	#Build a parentizing groups architecture
	return SYS.ObjectClass().update(
								[
									('GroupingFilePathString',SYS.getCurrentFolderPathString()+'MyObject.hdf5'),
									(
										'App_Group_ChildObject1',
										SYS.ObjectClass().update(
										[
											('App_Group_GrandChildObject1',
											SYS.ObjectClass())
										])
									),
									(
										'App_Group_ChildObject2',
										SYS.ObjectClass().update(
											[]
										)
									)
								]	
							).walk(
										[
											'GroupedOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Group"})
												]
											}
									)

def attest_group():

	#Build Hdf groups
	Object=SYS.ObjectClass().update(
								[
									('GroupingFilePathString',SYS.getCurrentFolderPathString()+'MyObject.hdf5'),
									(
										'App_Group_ChildObject1',
										SYS.ObjectClass().update(
										[
											('App_Group_GrandChildObject1',
											SYS.ObjectClass())
										])
									),
									(
										'App_Group_ChildObject2',
										SYS.ObjectClass().update(
											[]
										)
									)
								]	
							).walk(
										[
											'GroupedOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Group"}),
													('group',{})
												]
											}
									).close()

	#Get the h5ls version of the stored hdf
	return os.popen('h5ls -dlr '+Object.GroupingFilePathString).read()

#</DefineAttestingFunctions>
