#<ImportModules>
import collections
import copy
import itertools
import ShareYourSystem as SYS
import tables
import operator
import os
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Retriever"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class FindorClass(BasedLocalClass):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.FindingTuplesList=[]				#<NotRepresented>
		self.FoundRowedDictsList=[] 			#<NotRepresented>
		self.FoundFilteredRowedDictsList=[] 	#<NotRepresented>
		#</DefineSpecificDict>

	@SYS.HookerClass(**{'AfterTuplesList':[("<TypeString>","table")]})
	def find(self,**_FindingVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(("self.",self,['ModelingModelString','FindingTuplesList']))

		#If the FoundRowedTuplesList was not yet setted
		if self.FoundRowedDictsList==[]:

			#Debug
			self.debug('FoundRowedDictsList was not yet setted')

			#Take the first one in the list
			self.FoundRowedDictsList=SYS.getRowedDictsListWithTable(
												self.TabularedGroup._f_getChild(
													self.TabularedKeyStringsList[0]
												)
											)
		
		#Debug
		self.debug(
						[
							("self.",self,['FoundRowedDictsList'])
						]
				)

		#Now find really ! 
		self.FoundFilteredRowedDictsList=SYS.filterNone(SYS.where(
														self.FoundRowedDictsList,
														self.FindingTuplesList
											))

		#Debug
		self.debug(
					[
						'The where is over now',
						("self.",self,['FoundFilteredRowedDictsList'])
					]

				)

		#Debug
		self.debug('End of the method')
		
#</DefineClass>

#<DefineAttestingFunctions>
def attest_find():
	Findor=SYS.FindorClass(
		).update(
					[
						('ModelingColumnTuplesList',
											[
												#ColumnString 	#Col 	
												('FirstInt',	tables.Int64Col()),
												('SecondInt',	tables.Int64Col())
											]
						),
						('FindingTuplesList',
							[
								('SecondInt',(operator.gt,2))
							]
						),
						('RowedIdentifiedGettingStringsList',['FirstInt','SecondInt'])
					]
		).update(
					[
						('FirstInt',1),
						('SecondInt',2)
					]
		).flush(
		).update(
					[
						('FirstInt',1),
						('SecondInt',3)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',2)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',3)
					]
		).flush(
		).update(
					[
						('FirstInt',3),
						('SecondInt',4)
					]
		).flush(
		).find(		
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Findor
		)+'\n\n\n'+Findor.hdfview().HdformatedString
#</DefineAttestingFunctions>
