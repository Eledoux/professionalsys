#<ImportModules>
import collections
import copy
import itertools
import ShareYourSystem as SYS
import tables
import operator
import os
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class RetrieverClass(SYS.JoinerClass):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.RetrievedRowedDictsList=[] 		#<NotRepresented>
		self.IsRetrievingBool=True 				#<NotRepresented>
		#</DefineSpecificDict>

	def retrieveBefore(self,**_RetrievingVariablesDict):

		#Debug
		'''
		print('Joiner retrieveBefore method')
		print("self.ModeledDict['RetrievingTuplesList'] is ")
		SYS._print(self.ModeledDict['RetrievingTuplesList'])
		print('')
		'''

		#Check that there is the RetrievingTuplesList
		if 'RetrievingTuplesList' not in self.ModeledDict:
			self.ModeledDict['RetrievingTuplesList']={}

		#Join before maybe
		if 'IsJoiningBool' not in _RetrievingVariablesDict or _RetrievingVariablesDict['IsJoiningBool']:
			
			#Join
			self.join()

			#Debug
			'''
			print('self.JoinedModelString is ',self.JoinedModelString)
			print('Maybe we have to retrieve a joined model')
			print('')
			'''

			if self.JoinedModelString!="":

				#Debug
				'''
				print('self.JoinedModelString is ',self.JoinedModelString)
				print('self.JoinedModeledString is ',self.JoinedModeledString)
				print('')
				'''

				#Look already for JoinedList
				self.JoinedRetrievingTuplesList=filter(
							lambda __RetrievingTuple:
							SYS.getCommonSuffixStringWithStringsList(
									[
										__RetrievingTuple[0],
										'JoinedList'
									]
								)=='JoinedList',
							self.ModeledDict['RetrievingTuplesList']
					)

				#Debug
				print('Maybe we have already JoinedLists')
				print('self.JoinedRetrievingTuplesList is ')
				SYS._print(self.JoinedRetrievingTuplesList)
				print('')

				#Set the self.JoinedJoinedListKeyString
				self.JoinedJoinedListKeyString=self.JoinedModeledString+'JoinedList'

				#Define the JoinedListKeyStringsList
				JoinedListKeyStringsList=SYS.unzip(self.JoinedRetrievingTuplesList,[0])

				#Debug
				print('Look if we retrieve precisely if if there are a joined list')
				print('JoinedListKeyStringsList is ',JoinedListKeyStringsList)
				print('')

				#Check
				if self.JoinedJoinedListKeyString in JoinedListKeyStringsList:

					#Debug
					print('There is a defined <JoinedModelString>JoinedList')
					print('self.JoinedRetrievingTuplesList is')
					SYS._print(self.JoinedRetrievingTuplesList)
					print('')

					#Copy the RetrievedFilteredRowedDictsList
					self.JoinedRetrievedFilteredRowedDictsList=self.ModeledDict['TablesOrderedDict']

					copy.copy(self.FoundFilteredRowedDictsList)

		#Return self
		return self

	def retrieveAfter(self,**_RetrievingVariablesDict):

		#Debug
		print('Joiner retrieveAfter method')
		print("self.ModeledDict['ModelString'] is ",self.ModeledDict['ModelString'])
		print('self.FoundFilteredRowedDictsList is ')
		SYS._print(self.FoundFilteredRowedDictsList)
		#print('self.JoinedRetrievedFilteredRowedDictsList is ')
		#SYS._print(self.JoinedRetrievedFilteredRowedDictsList)
		print('self.JoinedRetrievingTuplesList is ')
		SYS._print(self.JoinedRetrievingTuplesList)
		print('')

		#Keep the intersection of the both
		self.FoundFilteredRowedDictsList=SYS.filterNone(SYS.where(
						self.FoundFilteredRowedDictsList,
						self.JoinedRetrievingTuplesList
						))

		#Debug
		print('After intersection')
		print('self.FoundFilteredRowedDictsList is ')
		SYS._print(self.FoundFilteredRowedDictsList)
		print('')
	#</DefineMethods>

	#<DefineMethods>
	def retrieve(self,_ModelString,**_RetrievingVariablesDict):
		"""Call the retrieve<HookString> methods and return self by default"""

		#Debug
		'''
		print('Retriever retrieve method')
		print('')
		'''

		#Maybe refresh some modeled attributes
		if _ModelString!="":
			self.tabular(_ModelString)
		else:
			_ModelString=self.ModeledModelString
			
		#Refresh the attributes 
		LocalRetrievingVariablesDict=dict(
			{'ModelString':_ModelString},**_RetrievingVariablesDict
		)
		LocalOutputedPointer=self
		self.IsRetrievingBool=True
		self.RetrievedRowedDictsList=[]

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='retrieve'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalRetrievingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalRetrievingVariablesDict' in OutputVariable:
							LocalRetrievingVariablesDict=OutputVariable['LocalRetrievingVariablesDict']
						if 'LocalOutputedPointer' in OutputVariable:
							LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

					#Check Bool
					if self.IsRetrievingBool==False:
						return LocalOutputedPointer

		#Return the LocalOutputedPointer
		return LocalOutputedPointer
	#</DefineMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_retrieve():

	#Bound an output method
	def outputAfter(_Retriever):
		_Retriever.MulInt=_Retriever.FirstInt*_Retriever.SecondInt
	SYS.RetrieverClass.HookingMethodStringToMethodsListDict['outputAfter']=[outputAfter]

	#Retrieve
	Retriever=SYS.RetrieverClass(
		).update(
					[
						('FirstInt',0),
						('SecondInt',0),
						('App_Model_ParameterizingDict',{
											'ColumningTuplesList':
											[
												#ColumnString 	#Col 	
												('FirstInt',	tables.Int64Col()),
												('SecondInt',	tables.Int64Col())
											],
											'IsFeaturingBool':True,
										}),
						('App_Model_ResultingDict',{
											'ColumningTuplesList':
											[
												#ColumnString 	#Col 	
												('MulInt',	tables.Int64Col())
											],
											'JoiningTuple':("","Parameter")
										})
					]
		).update(
					[
						('FirstInt',1),
						('SecondInt',2)
					]
		).flush('Result'
		).update(
					[
						('FirstInt',1),
						('SecondInt',3)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',2)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',3)
					]
		).flush(
		).update(
					[
						('FirstInt',3),
						('SecondInt',3)
					]
		).flush(
		).retrieve(
					'Result'		
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Retriever
		)+'\n\n\n'+SYS.represent(
				os.popen('h5ls -dlr '+Retriever.HdformatingPathString).read()
			)
#</DefineAttestingFunctions>
