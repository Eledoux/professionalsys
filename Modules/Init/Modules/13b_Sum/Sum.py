#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
from tables import *
import time
import operator
import os
#</ImportModules>

#<DefineLocals>
HookString="Mul"
#</DefineLocals>

#<DefineClass>
class SumClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.IntsList=[1,4,3]
		self.UnitsInt=3
		self.SumInt=8
		#</DefineSpecificDict>

		#Define the features
		self['App_Data_ParameterizingDatabase']=SYS.DatabaseClass().update(
									[
										('ColumningTuplesList',
											[
												#ColumnString 	#Col 	
												('UnitsInt',	Int64Col()),
												('IntsList',	(Int64Col,'UnitsInt'))
											]
										),
										('IsFeaturingBool',True),
										('ScanningTuplesList',
											[
												('IntsList',[[1,2],[4,5,6]])
											]
										)
									]
								)

		#Define the outputs
		self['App_Data_ResultingDatabase']={
										'ColumningTuplesList':
										[
											#ColumnString 	#Col 	
											('SumInt',		Int64Col())
										],
										'JoiningTuple':("","Parameter")
									}
						
	def outputAfter(self,**_LocalOutputingVariablesDict):
		
		#Set the SumInt
		self.SumInt=sum(self.IntsList)
	#</DefineHookMethods>
	
	#</DefineBindingHookMethods>
	def bindIntsListAfter(self):

		#Bind with UnitsInt setting
		self.UnitsInt=len(self.IntsList)

	#</DefineBindingHookMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_flush():

	#Flush
	Sum=SYS.SumClass(
		).__setitem__('IntsList',[4,5]
		).flush('Result'
		)

	print('\n\n\n ENSUITE \n\n\n')

	Sum.__setitem__('IntsList',[0,1,2]
		).flush(
		).__setitem__('IntsList',[0,2]
		).flush(
		).__setitem__('IntsList',[0,1]
		).flush(
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Sum
		)+'\n\n\n'+SYS.represent(Sum.hdfview().HdformatedString)

def attest_retrieve():
	Sum=SYS.SumClass(
		).__setitem__('/App_Model_ResultingDict/RetrievingTuple',(0,2)
		).retrieve('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Sum
		)

def attest_find():

	#Define a finding function
	def getIsSecondIntLtBool(_List,_Int):
		return _List[1]<_Int

	#Find in the sum
	Sum=SYS.SumClass(
		).update(
					[
						('/App_Model_ParameterizingDict/update',
								[	
									('MergingTuplesList',
											[
												('UnitsInt',(SYS.getIsEqualBool,2))
											]),
									('FindingTuplesList',
											[
												('IntsList',(getIsSecondIntLtBool,2))
											])
								]
						),
						('/App_Model_ResultingDict/FindingTuplesList',
											[
												('SumInt',(operator.lt,5))
											])
					]
		).find('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Sum
		)

def attest_recover():

	#Define a finding function
	def getIsSecondIntLtBool(_List,_Int):
		return _List[1]<_Int
		
	#Recover
	Sum=SYS.SumClass(
		).update(
					[
						('/App_Model_ParameterizingDict/update',
								[	
									('MergingTuplesList',
											[
												('UnitsInt',(SYS.getIsEqualBool,2))
											]),
									('FindingTuplesList',
											[
												('IntsList',(getIsSecondIntLtBool,2))
											])
								]
						),
						('/App_Model_ResultingDict/FindingTuplesList',
											[
												('SumInt',(operator.lt,5))
											])
					]
		).recover('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Sum
		)

def attest_scan():

	#Scan
	Sum=SYS.SumClass(
		).scan('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Sum
		)+'\n\n\n'+SYS.represent(
				os.popen('/usr/local/bin/h5ls -dlr '+Sum.HdformatingPathString).read()
			)
#</DefineAttestingFunctions>
