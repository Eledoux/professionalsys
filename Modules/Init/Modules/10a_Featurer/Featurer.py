#<ImportModules>
import collections
import copy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Merger"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class FeaturerClass(
					BasedLocalClass
				):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.FeaturingAllBool=False										#<NotRepresented>
		#</DefineSpecificDict>

	@SYS.HookerClass(**{'BeforeTuplesList':[("Rower","model")]})
	def model(self,**_VariablesDict):

		#Put all the GettingStringsList in the identifying container
		if self.FeaturingAllBool:
			self.RowedIdentifiedGettingStringsList=SYS.unzip(
														self.ModelingColumnTuplesList,[0]
													)
			self.RowedNotIdentifiedGettingStringsList=[]	

#</DefineClass>

#<DefineAttestingFunctions>
def attest_flush():
	Featurer=SYS.FeaturerClass().update(								
									[
										('MyInt',0),
										('MyString',"hello"),
										('MyIntsList',[2,4,1]),
										('ModelingColumnTuplesList',
											[
												('MyInt',tables.Int64Col()),
												('MyString',tables.StringCol(10)),
												('MyIntsList',(tables.Int64Col(shape=3)))
											]
										),
										('FeaturingAllBool',True)
									]).flush(
									).update(
										[
											('MyInt',1),
											('MyString',"bonjour"),
											('MyIntsList',[2,4,6])
										]
									).flush(
									).close()
																
	#Get the h5ls version of the stored hdf
	return SYS.represent(
						Featurer
						)+'\n\n\n\n'+Featurer.hdfview().HdformatedString	
#</DefineAttestingFunctions>
