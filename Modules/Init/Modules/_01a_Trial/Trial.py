#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
from tables import *
import time
import operator
import os
#</ImportModules>

class TrialClass(SYS.ObjectClass):

  def initAfter(self):
      self.TrialInt=1
      self.TrialFloat=1.

  def bindTrialIntAfter(self):
    self.TrialFloat=float(self.TrialInt)
