#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

class DoerClass(SYS.NoderClass):

	@SYS.HookerClass(**{'AfterTuplesList':[("Noder","init")]})
	def init(self,**_VariablesDict):

		#Define a default Int
		self.MyInt=1

	@SYS.SwitcherClass()
	def do(self):
		self.MyInt+=1

#Get an instance
MyDoer=DoerClass()

#Show the default value of the instance
print('At the beginning the object is...')
print(MyDoer)
print('')

#Change the int
print('do with the object...')
MyDoer.do()
print(MyDoer)
print('')

#Change the int
print('do again with the object, but...')
MyDoer.do()
print(MyDoer)
print('')

#Now set InititatedIsBool to False to bind with a reinit of the object
print('Now reset the object...')
MyDoer['DoneIsBool']=False
print(MyDoer)
print('')