#<ImportModules>
import collections
import copy
import inspect
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
SwitchingBeforeString='Before'
SwitchingAfterString='After'
SwitchingBindString='bind'
#</DefineLocals>

#<DefineClass>
class SwitcherClass(SYS.DebuggerClass):
	def __init__(self,**_VariablesDict):

		#Init the Debugger
		SYS.DebuggerClass.init(self)

		#<DefineSpecificDict>
		self.SwitchingFunctionPointer=None
		self.SwitchingBeforeMethodString=""
		self.SwitchedBeforeBoolKeyString=""
		self.SwitchingBeforeDoneMethodString=""
		self.SwitchedFunctionString=""
		self.SwitchedDoneFunctionString=""
		self.SwitchedFunctionPointer=None
		#<DefineSpecificDict>

		#Update with the Kwargs
		map(
				lambda __ItemTuple:
				self.__setattr__('Switching'+__ItemTuple[0],__ItemTuple[1]),
				_VariablesDict.iteritems()
			)

	def __call__(self,_SwitchingFunction):

		#Set the  SwitchedFunction
		self.SwitchingFunctionPointer=_SwitchingFunction

		#Set the SwitchedFunctionString
		self.SwitchedFunctionString=_SwitchingFunction.__name__
		self.SwitchedDoneFunctionString=SYS.getDoneStringWithDoString(self.SwitchedFunctionString)
		self.SwitchedBoolKeyString=self.SwitchedDoneFunctionString[0].upper()+self.SwitchedDoneFunctionString[1:]+'IsBool'

		#Set SwitchingBeforeBoolKeyString
		if self.SwitchingBeforeMethodString!="":
			self.SwitchingBeforeDoneMethodString=SYS.getDoneStringWithDoString(self.SwitchingBeforeMethodString)
			self.SwitchedBeforeBoolKeyString=self.SwitchingBeforeDoneMethodString[0].upper()+self.SwitchingBeforeDoneMethodString[1:]+'IsBool'

		#Hook
		self.switch()

		#Return the SwitchedFunction
		return self.SwitchedFunctionPointer

	def switch(self):

		#Define the WrappedFunction
		def SwitchedFunction(*VariablesList,**_VariablesDict):

			#Alias
			InstanceVariable=VariablesList[0]

			#Set the SwitchedBool if it was not already
			if hasattr(InstanceVariable,self.SwitchedBoolKeyString)==False:
				InstanceVariable.__setattr__(self.SwitchedBoolKeyString,False)
			elif getattr(InstanceVariable,self.SwitchedBoolKeyString):
				return InstanceVariable

			#Call maybe a before method
			if (hasattr(InstanceVariable,self.SwitchedBeforeBoolKeyString)==False or getattr(
				InstanceVariable,self.SwitchedBeforeBoolKeyString)==False) and self.SwitchingBeforeMethodString!="":

				#Debug
				self.debug('Call before the '+self.SwitchingBeforeMethodString+' method')

				#Call
				getattr(InstanceVariable,self.SwitchingBeforeMethodString)()

				#Debug
				self.debug('Ok the '+self.SwitchingBeforeMethodString+' method was called')

			#At the level of the class set the new binding set function
			if hasattr(InstanceVariable.__class__,self.SwitchedBoolKeyString)==False:

				#Set the OldSetFunction
				InstanceVariable.__class__.OldSetFunction=InstanceVariable.__class__.set

				#Define the new binding and setting function
				def bindAndSet(_Instance,**_VariablesDict):

					#Bind the reset 
					if _Instance.SettingKeyVariable==self.SwitchedBoolKeyString and getattr(
						_Instance,self.SwitchedBoolKeyString) and _Instance.SettingValueVariable==False:

						#Debug
						'''
						self.debug('Reinit with '+_Instance.SettingKeyVariable)
						'''

						#Call the init method just at the level of this class definition
						_Instance.init.im_func.HookerPointer.HookingFunctionPointer(_Instance)

					#Return the Instance
					return _Instance

					InstanceVariable.__class__.OldSetFunction(_Instance,**_VariablesDict)

				#Set this new decorated set function
				InstanceVariable.__class__.set=bindAndSet

				#Say that it is ok
				setattr(InstanceVariable.__class__,self.SwitchedBoolKeyString,True)

			#Debug
			'''
			self.debug('InstanceVariable is '+SYS.represent(InstanceVariable))
			'''

			#Call the SwitchedFunction
			self.SwitchingFunctionPointer(*VariablesList,**_VariablesDict)

			#Set True for the IsBool after the call
			InstanceVariable.__setattr__(self.SwitchedBoolKeyString,True)

			#Return self for the wrapped method call
			return InstanceVariable

		#Give a Pointer to the Hooker
		SwitchedFunction.SwitcherPointer=self

		#Set
		self.SwitchedFunctionPointer=SwitchedFunction

		#Return self
		return self

#</DefineClass>

