#<ImportModules>
import collections
import copy
import inspect
import os
import operator
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
SwitchingBeforeString='Before'
SwitchingAfterString='After'
SwitchingBindString='bind'
BasingLocalTypeString="Debugger"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class SwitcherClass(BasedLocalClass):

	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):
		
		#<DefineSpecificDict>
		self.SwitchingFunction=None
		self.SwitchedFunctionString=""
		self.SwitchedDoneFunctionString=""
		self.SwitchedFunction=None
		#<DefineSpecificDict>

		#Update with the Kwargs
		map(
				lambda __ItemTuple:
				self.__setattr__('Switching'+__ItemTuple[0],__ItemTuple[1]),
				_VariablesDict.iteritems()
			)

	def __call__(self,_SwitchingFunction):

		#Set the  SwitchedFunction
		self.SwitchingFunction=_SwitchingFunction

		#Set the SwitchedFunctionString
		self.SwitchedFunctionString=_SwitchingFunction.__name__
		self.SwitchedDoneFunctionString=SYS.getDoneStringWithDoString(self.SwitchedFunctionString)
		self.SwitchedBoolKeyString=self.SwitchedDoneFunctionString[0].upper()+self.SwitchedDoneFunctionString[1:]+'IsBool'

		#Hook
		self.switch()

		#Return the SwitchedFunction
		return self.SwitchedFunction

	def switch(self):

		#Define the WrappedFunction
		def SwitchedFunction(*_VariablesList,**_VariablesDict):

			#Alias
			InstanceVariable=_VariablesList[0]

			#Append for debbuging
			if hasattr(InstanceVariable,'DebuggingNotFramedFunctionStringsList'):
				if 'SwitchedFunction' not in InstanceVariable.DebuggingNotFramedFunctionStringsList:
					InstanceVariable.DebuggingNotFramedFunctionStringsList.append('SwitchedFunction')

			#Set the SwitchedBool if it was not already
			if hasattr(InstanceVariable,self.SwitchedBoolKeyString)==False:
				InstanceVariable.__setattr__(self.SwitchedBoolKeyString,False)
			elif getattr(InstanceVariable,self.SwitchedBoolKeyString):
				return InstanceVariable

			#At the level of the class set the new binding set function
			if hasattr(InstanceVariable.__class__,self.SwitchedBoolKeyString)==False:

				#Define the new binding and setting function
				def bind(*_BindingVariablesList,**_BindingVariablesDict):

					#Alias
					BindedInstanceVariable=_BindingVariablesList[0]

					#Debug
					"""
					self.debug('Reinit with '+BindedInstanceVariable.SettingKeyVariable)
					"""

					#Call the init method just at the level of this class definition
					BindedInstanceVariable.init.im_func.HookerPointer.HookingFunction(BindedInstanceVariable)

				#Bind
				BindedMethodString='bind'+self.SwitchedBoolKeyString
				setattr(
							InstanceVariable.__class__,
							BindedMethodString,
							SYS.BinderClass(**
								{
									'ConditionTuplesList':[
															('SettingKeyVariable',(operator.eq,self.SwitchedBoolKeyString)),
															(self.SwitchedBoolKeyString,(operator.eq,True)),
															('SettingValueVariable',(operator.eq,False))
														],
									'HookingString':"Before"
								}
							)(bind)
						)

				#Debug
				'''
				self.debug(
							[
								('Have to call a first time to set the binding method'),
								('BindedMethodString is '+BindedMethodString)
							]
						)
				'''

				#Call a first time the method with the InstanceVariable to set
				getattr(InstanceVariable,BindedMethodString)()

				#Say that it is ok
				setattr(InstanceVariable.__class__,self.SwitchedBoolKeyString,True)

			#Debug
			'''
			self.debug(
						[
							#('InstanceVariable is '+SYS.represent(InstanceVariable)),
							('_VariablesList is '+str(_VariablesList))
						]
					)
			'''

			#Call the SwitchedFunction
			self.SwitchingFunction(*_VariablesList,**_VariablesDict)

			#Set True for the IsBool after the call
			InstanceVariable.__setattr__(self.SwitchedBoolKeyString,True)

			#Return self for the wrapped method call
			return InstanceVariable

		#Give a Pointer to the Hooker
		SwitchedFunction.SwitcherPointer=self

		#Define a represent function for the hooked function
		def represent():
			RepresentedString=inspect.getmodule(self.SwitchingFunction
					).__name__+'.Switched_'+self.SwitchingFunction.__repr__()
			return RepresentedString
		SwitchedFunction.__repr__=represent

		#Set
		self.SwitchedFunction=SwitchedFunction

		#Return self
		return self

#</DefineClass>

