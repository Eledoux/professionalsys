#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
from tables import *
import time
import operator
import os
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class  ConnecterClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.IsConnectingBool=True
		#</DefineSpecificDict>
						
	def connectAfter(self):
		
		pass

		#Return self
		return self

	#</DefineHookMethods>
	
	#<DefineMethods>
	def connect(self,**_ConnectingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Debug
		'''
		print('Outputer output method')
		print('')
		'''

		#Refresh the attributes and set maybe default values in the _ConnectingVariablesDict
		LocalConnectingVariablesDict=_ConnectingVariablesDict
		LocalOutputedPointer=self
		self.IsConnectingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='connect'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalConnectingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalConnectingVariablesDict' in OutputVariable:
							LocalConnectingVariablesDict=OutputVariable['LocalConnectingVariablesDict']
						if 'LocalOutputedPointer' in OutputVariable:
							LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

					#Check Bool
					if self.IsConnectingBool==False:
						return LocalOutputedPointer

		#Return the OutputVariable
		return LocalOutputedPointer
	#</DefineMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.ConnecterClass()

def attest_connect():
	Connecter=SYS.ConnecterClass().update([
									('ConnectingTypeString','Vector'),
									('ConnectingTuplesList',[
																('UnitsInt','UnitsInt',[1,2])
															])
								]).connect()

#</DefineAttestingFunctions>
