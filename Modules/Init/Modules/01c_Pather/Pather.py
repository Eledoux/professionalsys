#<ImportModules>
import collections
import copy
import os
import ShareYourSystem as SYS
import six
import sys
#</ImportModules>

#<DefineLocals>
PathingString="/"
BasingLocalTypeString="Deleter"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineFunctions>
def getVariableWithDictatedVariableAndKeyList(_DictatedVariable,_KeyList):
	''''''
	if type(_KeyList)==list:

		#Empty list case : return the Object
		if len(_KeyList)==0:
			return _DictatedVariable;
		elif len(_KeyList)==1:

			#One Variable List case : return the associated Value at the Int
			if type(_DictatedVariable) in [list,tuple]:
				if type(_KeyList[0])==int:
					if _KeyList[0]<len(_DictatedVariable):
						return _DictatedVariable[_KeyList[0]]

			elif _KeyList[0] in _DictatedVariable:

				#One Variable Dict case : return the associated Value at the KeyString
				return _DictatedVariable[_KeyList[0]]
		else:
			
			#Multi Variables case : recursive call with the reduced list
			if _KeyList[0] in _DictatedVariable:
				return getVariableWithDictatedVariableAndKeyList(_DictatedVariable[_KeyList[0]],_KeyList[1:])

	#Return by default "NotFound"
	return "NotFound"

def getVariableWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable):
	if type(_KeyVariable)==list:
		return getVariableWithDictatedVariableAndKeyList(_DictatedVariable,_KeyVariable)
	elif type(_KeyVariable) in SYS.StringTypesList:
		return _DictatedVariable[_KeyVariable] if _KeyVariable in _DictatedVariable else None

def setWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable,_ValueVariable,**_KwargsDict):
	'''	'''

	#Special dict case for also handling SluggerNamesList Key
	Type=type(_DictatedVariable)
	if Type in [dict,collections.OrderedDict] or SYS.PatherClass in Type.__mro__:

		#Debug
		'''
		print('_DictatedVariable has items')
		print('')
		'''

		#Set with a list
		if type(_KeyVariable)==list:
			if len(_KeyVariable)>0:
				if _KeyVariable[0]==PathingString:
					if _KeyVariable==[PathingString]:
						GettedVariable=_DictatedVariable
						GettedVariable.update(_ValueVariable)
						return
					else:
						_KeyVariable=_KeyVariable[1:]

				#Debug
				'''
				print('_KeyVariable is a list')
				print('_KeyVariable is '+str(_KeyVariable))
				print('')
				'''

				#Get the next "path"
				GettedVariable=getVariableWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable[:-1])

				#Debug
				'''
				print('GettedVariable is '+str(GettedVariable))
				print('')
				'''

				#Set
				setWithDictatedVariableAndKeyVariable(GettedVariable,_KeyVariable[-1],_ValueVariable)

				#Return 
				return
		else:

			#Debug
			'''
			print('_KeyVariable is not a list')
			print('_KeyVariable is '+str(_KeyVariable))
			print('')
			'''

			#Call a method of the dict
			if (_KeyVariable[0].isalpha() or _KeyVariable[0:2]=="__") and  _KeyVariable[0].lower()==_KeyVariable[0]:
				getattr(_DictatedVariable,_KeyVariable)(_ValueVariable)
				
				#Return 
				return

			#Set deeply in the dict
			elif _KeyVariable.startswith(PathingString):

				#Cas of the dict or OrderedDict we have to convert in list to make the key been understood
				if Type in [dict,collections.OrderedDict]:

					#Split
					_KeyVariable=_KeyVariable.split(PathingString)[1:]

					#Debug
					'''
					print('_KeyVariable has PathingStrings so convert the _KeyVariable into a list')
					print(_KeyVariable)
					print('')
					'''

					#Set in the dict
					setWithDictatedVariableAndKeyVariable(
							_DictatedVariable,
							_KeyVariable,
							_ValueVariable
						)

					#Return 
					return

				else:

					#Debug
					'''
					print('_KeyVariable has PathingStrings bu the _DictatedVariable knows how to deal with that')
					'''

					#This is an object that understandsa already how to do
					_DictatedVariable[_KeyVariable]=_ValueVariable

					#Return 
					return

			else:

				#Debug
				'''
				print('_KeyVariable has no PathingString so set direclty')
				print(_KeyVariable)
				print('')
				'''

				#Set 
				_DictatedVariable[_KeyVariable]=_ValueVariable

				#Return 
				return

	#List Case
	if type(_DictatedVariable)==list:
		if type(_KeyVariable)==list():
			NextSlugger=getVariableWithDictatedVariableAndKeyVariable(_DictatedVariable,_KeyVariable[0])
			setWithDictatedVariableAndKeyVariable(NextSlugger,_KeyVariable[1:],_ValueVariable)
			return
		else:
			_DictatedVariable[_KeyVariable]=_ValueVariable
			return
#</DefineFunctions>

#<DefineClass>
class PatherClass(BasedLocalClass):

	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.PathingString="" 						#<NotRepresented>
		self.PathedStringsList=[] 					#<NotRepresented>
		self.PathedKeyString="" 					#<NotRepresented>
		self.PathedChildKeyString="" 				#<NotRepresented>
		self.PathedValueVariable=None  				#<NotRepresented>
		#</DefineSpecificDict>

	def path(self):

		#Split
		self.PathedStringsList=PathingString.join(
			self.PathingString.split(PathingString)[1:]).split(PathingString)

		#Set
		self.PathedKeyString=self.PathedStringsList[0]

		#Define the ChildPathString
		self.PathedChildKeyString=PathingString+PathingString.join(self.PathedStringsList[1:])

		#Set the PathedValueVariable
		if self.PathedKeyString=="":
			self.PathedValueVariable=self
		else:
			self.PathedValueVariable=self[self.PathedKeyString]

	@SYS.HookerClass(**{'BeforeTuplesList':[(BasedLocalClass,"get")]})
	def get(self,**_VariablesDict):

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Check
		if self.GettingKeyVariable.startswith(PathingString):

			#Path
			self.PathingString=self.GettingKeyVariable
			self.path()

			#Debug
			'''
			self.debug(('self.',self,[
										"PathedKeyString",
										"PathedChildKeyString",
										"PathedValueVariable"
									]
								))
			'''

			#Get
			if self.PathedValueVariable!=None:

				if len(self.PathedStringsList)==1:

					#Direct update in the Child or go deeper with the ChildPathString
					self.GettedValueVariable=self.PathedValueVariable

					#Stop the getting
					_VariablesDict['IsCallingBool']=False

				else:

					#Gett with the PathedChildKeyString
					self.GettedValueVariable=self.PathedValueVariable[self.PathedChildKeyString]

					#Stop the getting
					_VariablesDict['IsCallingBool']=False

	@SYS.HookerClass(**{'BeforeTuplesList':[(BasedLocalClass,"set")]})
	def set(self,**_VariablesDict):
		""" """

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Deep set
		if self.SettingKeyVariable.startswith(PathingString):

			#Debug
			'''
			self.debug('We are going to path')
			'''

			#Path
			self.PathingString=self.SettingKeyVariable
			self.path()

			#Debug
			'''
			self.debug(('self.',self,[
										"PathedKeyString",
										"PathedChildKeyString",
										"PathedValueVariable"
									]
								))
			'''

			#Set
			if self.PathedValueVariable!=None:

				#Direct update in the Child or go deeper with the ChildPathString
				if self.PathedChildKeyString==PathingString: 

					#Case where it is an object to set inside
					if SYS.PatherClass in type(self.PathedValueVariable).__mro__:

						#Modify directly the GettedValueVariable with self.SettingValueVariable
						self.PathedValueVariable.__setitem__(*self.SettingValueVariable)

					#Case where it is a set at the level of self of an already setted thing
					else:

						self[self.PathedKeyString]=self.SettingValueVariable
				else:

					#Debug
					'''
					self.debug(("self.",self,[
												'SettingValueVariable',
												'PathedChildKeyString'])
					)
					'''

					#Call the setWithDictatedVariableAndKeyVariable
					setWithDictatedVariableAndKeyVariable(
						self.PathedValueVariable,
						self.PathedChildKeyString,
						self.SettingValueVariable
					)

			#Case where it is a set at the level of self of new setted thing
			else:

				self[self.PathedKeyString]=self.SettingValueVariable

			#Set to False
			return {'IsCallingBool':False}

#</DefineClass>

#<DefineAttestingFunctions>
def attest_path():

	#Explicit expression
	Pather=SYS.PatherClass()
	
	#Set with a deep short string
	Pather.__setitem__('MyPather',SYS.PatherClass()).__setitem__('/MyPather/MyString','hello')

	#Set with a deep deep short string
	Pather.__setitem__('/MyPather/MyGrandChildPather',SYS.PatherClass())

	#Return the object
	return Pather
#</DefineAttestingFunctions>

