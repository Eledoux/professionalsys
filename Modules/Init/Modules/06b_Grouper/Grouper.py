#<ImportModules>
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Hdformater"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class GrouperClass(BasedLocalClass):
		
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.GroupedParentPointer=None
		self.GroupedInt=-1
		self.GroupedKeyString=""
		self.GroupedGrandParentPointersList=[]
		self.GroupedPathStringsList=[]
		self.GroupedPathString="/"
		#</DefineSpecificDict>

	def group(self,_GroupingString=""):

		#Debug
		self.debug('Start of the method')
		
		#Nodify
		if _GroupingString!="":
			self.node(_GroupingString)
		
		if len(self.NodedGrandParentPointersList)>0:
			UppestParentPointer=self.NodedGrandParentPointersList[-1]
		else:
			UppestParentPointer=self

		#Then get also from the UppestParentPointer its UppestGroupedParentPointer
		if hasattr(UppestParentPointer,'GroupedGrandParentPointersList'):
			if len(UppestParentPointer.GroupedGrandParentPointersList)>0:
				UppestGroupedParentPointer=UppestParentPointer.GroupedGrandParentPointersList.GroupedGrandParentPointersList[-1]
			else:
				UppestGroupedParentPointer=UppestParentPointer

		#Define the HdformatedFilePointerKeyString
		HdformatedFilePointerKeyString="HdformatedFilePointer"

		#Debug
		#self.debug('UppestParentPointer.GroupingPathString is '+UppestParentPointer.GroupingPathString)

		#Point on the FilePointer of the uppest grouped Parent
		self.__setattr__(
							HdformatedFilePointerKeyString,
							getattr(UppestGroupedParentPointer,"HdformatedFilePointer")
						)

		#Get it definitely !
		FilePointer=getattr(self,HdformatedFilePointerKeyString)

		#Debug
		#print('FilePointer is ',FilePointer)

		#Create a group in the hdf5 file
		if FilePointer!=None:

			#Debug
			#self.debug('self.NodedPathString is '+self.NodedPathString))

			#Set the GroupedPathString
			self.GroupedPathString=SYS.getPastedPathStringWithPathStringsList(
				[
					UppestGroupedParentPointer.GroupedPathString,
					self.NodedPathString
				]
			)

			#Debug
			#self.debug('self.GroupedPathString is '+str(self.GroupedPathString))

			#Check if the Path exists
			if self.GroupedPathString not in FilePointer:

				#Set all the intermediate Paths before
				PathStringsList=self.GroupedPathString.split('/')[1:]
				ParsingChildPathString="/"

				#Set the PathString from the top to the down (integrativ loop)
				for ChildPathString in PathStringsList:

					#Go deeper
					NewParsingChildPathString=ParsingChildPathString+ChildPathString

					#Create the group if not already
					if NewParsingChildPathString not in FilePointer:
						if self.HdformatingModuleString=="tables":
							FilePointer.create_group(ParsingChildPathString,ChildPathString)
						elif self.HdformatingModuleString=="h5py":
							Group=FilePointer[ParsingChildPathString]
							Group.create_group(ChildPathString)
					
					#Prepare the next group	
					ParsingChildPathString=NewParsingChildPathString+'/'

		#Debug
		self.debug('End of the method')

		#Return self
		return self
	#</DefineHookMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_group():

	#Build Hdf groups
	Grouper=SYS.GrouperClass().hdformat().update(
								[
									(
										'<Structure>ChildGrouper1',
										SYS.GrouperClass().update(
										[
											('<Structure>GrandChildGrouper1',
											SYS.GrouperClass())
										])
									),
									(
										'<Structure>ChildGrouper2',
										SYS.GrouperClass().update(
											[]
										)
									)
								]	
							).walk(
										[
											'<Structure>'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parent',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':""})
												]
											}
									).close()

	#Get the h5ls version of the stored hdf
	return Grouper.hdfview().HdformatedString
#</DefineAttestingFunctions>
