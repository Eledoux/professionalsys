#<ImportModules>
import collections
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
ModelingJoinString=SYS.Modeler.ModelingJoinString
#</DefineLocals>

#<DefineClass>
class TablorClass(
					SYS.GrouperClass,
					SYS.ModelerClass
				):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		pass
		#self.TabularedCalibratedModelClass=None
		#self.TabularedCalibratedTable=None
		#self.TabularedCalibratedKeyString=""
		#self.TabularedRowedPointer=None
		#self.TabularedRowedTuplesList=[]
		#self.IsRowingBool=True
		#</DefineSpecificDict>

		self.RepresentingKeyVariablesList+=['ModeledOrderedDict']

	#</DefineHookMethods>

	#<DefineMethod>
	def modelAfter(self,**_ModelingVariablesList):

		#Debug
		print('Tablor modelAfter method')
		print(self.ModeledDict)

		#Define the ModelClass
		ModelClass=self.ModeledDict['ModelClassesOrderedDict'][self.ModeledDict['ShapingString']]

		#Define the TableString
		TableString=ModelingJoinString.join(
						ModelClass.__name__.split(ModelingJoinString)[1:-1]
					)+ModelingJoinString+'Table'

		#Define the TablePathString
		TablePathString=self.GroupedPathString+'/'+TableString

		#Create the Table if not already
		if TablePathString not in self.HdformatedFilePointer:
			self.ModeledDict['Table']=self.HdformatedFilePointer.create_table(
												self.HdformatedFilePointer.getNode(self.GroupedPathString),
												TableString,
												ModelClass,
												ModelClass.__doc__ 
												if ModelClass.__doc__!=None 
												else "This is the "+ModelClass.__name__
									)
		else:
			self.ModeledDict['Table']=self.HdformatedFilePointer.getNode(
				TablePathString)
		
	"""					
	def tabular(self,_TabularingString):

		#Debug
		print('tabular method')
		print('self.GroupedPathString is ',self.GroupedPathString)
		
		#Define the DoneTabularingString
		DoneTabularingString=SYS.getDoneStringWithDoString(_TabularingString)
		
		#Check that the ModeledOrderedDict exists 
		ModeledOrderedDictKeyString="Modeled"+DoneTabularingString+"OrderedDict"
		if hasattr(self,ModeledOrderedDictKeyString):
			ModeledOrderedDict=getattr(self,ModeledOrderedDictKeyString)

			#Hdformat maybe if it was not done
			if hasattr(self,'HdformatedFilePointer')==None:
				self.hdformat()
			print("iiii",self)

			#Check that the TabularedOrderedDict exists 
			TabularedOrderedDictKeyString="Tabulared"+DoneTabularingString+"OrderedDict"
			if hasattr(self,TabularedOrderedDictKeyString):
				TabularedOrderedDict=getattr(self,TabularedOrderedDictKeyString)

				#Define the ModelingJoinString
				ModelingJoinString=SYS.Modeler.ModelingJoinString

				#Debug
				#print('ModelingJoinString is : ',ModelingJoinString)

				#Create the table or get it and set it again
				map(
						lambda __TableStringAndTableGroupedPathStringTuple,__ModeledClass:
						TabularedOrderedDict.__setitem__(
							ModelingJoinString.join(
								__TableStringAndTableGroupedPathStringTuple[0].split(ModelingJoinString)[:-1]
								),
							self.HdformatedFilePointer.create_table(
											self.HdformatedFilePointer.getNode(self.GroupedPathString),
											__TableStringAndTableGroupedPathStringTuple[0],
											__ModeledClass,
											__ModeledClass.__doc__ 
											if __ModeledClass.__doc__!=None 
											else "This is the "+__ModeledClass.__name__
								)
						) 
						if __TableStringAndTableGroupedPathStringTuple[1] not in self.HdformatedFilePointer
						else
						#Get the Node and set it to the TabularedDict
						TabularedOrderedDict.__setitem__(
								ModelingJoinString.join(
								__TableStringAndTableGroupedPathStringTuple[0].split(ModelingJoinString)[:-1]
								),
								self.HdformatedFilePointer.getNode(__TableStringAndTableGroupedPathStringTuple[1])
							),
						#Map on (__TableString,__TableGroupedPathString),__ModeledClass
						map(
							lambda __TableString:
							(
								__TableString,
								SYS.getPastedPathStringWithPathStringsList(
									[self.GroupedPathString,__TableString])
							),
							map(
								lambda __ClassKeyString:
								#'Class'.join(__ClassKeyString.split('Class')[:-1])+'Table'
								__ClassKeyString+ModelingJoinString+DoneTabularingString+'Table'
								,ModeledOrderedDict.keys()
								)	
						),
						ModeledOrderedDict.values()
					)

				#Do a smart joining link to the corresponding OrderedDict
				map(
						lambda __Table:
						#TabularedOrderedDict.__setitem__(
							#'__'.join(__Table.name.split('__')[1:-1])
						DoneTabularingString.join(__Table.name.split(DoneTabularingString)[1:])
							#__Table.name
						#	,__Table
						#)
						,filter(
								lambda __Node:
								type(__Node)==tables.table.Table,
								self.HdformatedFilePointer.getNode(self.GroupedPathString)._f_iterNodes()
						)
					)
				
			else:
				print('WARNING tabular method : not such TabularedClassString ',
						TabularedClassString)


		else:
			print('WARNING tabular method : not such ModeledOrderedDictKeyString ',
				ModeledOrderedDictKeyString)

		#Return self
		return self

	"""
	"""
		'''
			#Debug
			#print('ModeledGettingStringsList is ',ModeledGettingStringsList)

			#Build the Table of it doesn't exist yet
			if TableString not in TabularedOrderedDict:

				#Build maybe the model
				ModeledOrderedDictKeyString="Modeled"+DoneTabularingString+"OrderedDict"
				ModeledOrderedDict=getattr(self,ModeledOrderedDictKeyString)
				if TableString not in ModeledOrderedDict:
					self.model(_CalibratingString)

				#Tabular
				self.tabular(_CalibratingString)

			#Debug
			#print('TabularedOrderedDict is ',TabularedOrderedDict)

			#Define the Table
			self.TabularedCalibratedModelClass=ModeledOrderedDict[TableString]
			self.TabularedCalibratedTable=TabularedOrderedDict[TableString]
			self.TabularedCalibratedKeyString=TableString

		else:

			print('WARNING calibrate method : not such TabularedOrderedDictKeyString ',
						TabularedOrderedDictKeyString)
		'''
	"""

	def row(self,_RowingString,**_RowingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Refresh the attributes and set maybe default values in the _RowingVariablesDict
		LocalOutputedPointer=self
		LocalRowingVariablesDict=dict({'RowingString':_RowingString},**_RowingVariablesDict)
		self.TabularedRowedTuplesList=[]
		self.IsRowingBool=True

		#Calibrate the good table
		self.calibrate(_RowingString)

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='row'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalRowingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalRowingVariablesDict' in OutputVariable:
							LocalRowingVariablesDict=OutputVariable['LocalRowingVariablesDict']
						if 'LocalOutputedPointer' in OutputVariable:
							LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

					#Check Bool
					if self.IsRowingBool==False:
						return LocalOutputedPointer

		#Return the OutputVariable
		return LocalOutputedPointer

	def rowBefore(self,**_RowingVariablesDict):

		#Check that the TabularedCalibratedTable is ok
		if self.TabularedCalibratedTable!=None:

			#Set the TabularedRowedPointer
			self.TabularedRowedPointer=self.TabularedCalibratedTable.row

			#Get the ModelingTuplesList
			ModelingTuplesList=self.ModelingDict[
				SYS.getDoingStringWithDoString(_RowingVariablesDict['RowingString'])+'TuplesList'
				]

			#Get the FlushingTuplesList
			self.TabularedRowedTuplesList=zip(
									SYS.unzip(ModelingTuplesList,[0]),
									self.pick(SYS.unzip(ModelingTuplesList,[0]))
								)

	def flush(self):

		#Get the row
		Row=self.TabularedCalibratedTable.row

		#Set the FlushingTuples in the Row
		map(
				lambda __FlushingTuple:
				Row.__setitem__(*__FlushingTuple),
				self.TabularedRowedTuplesList
			)

		#Append
		Row.append()
		self.TabularedCalibratedTable.flush()

	'''
	def flush(self,_FlushingString,_JoiningString=""):

		#Check that the TabularedCalibratedTable is ok
		if self.TabularedCalibratedTable!=None:

			#Define the DoneJoiningString 
			if _JoiningString!="":
				DoneJoiningString=SYS.getDoneStringWithDoString(_JoiningString)
				JoiningOrderedDict=getattr(self,DoneJoiningString+'OrderedDict')
				DoneJoiningKeyStringKeyString=DoneJoiningString+'KeyString'
			else:
				JoiningOrderedDict={}

			#Get the ModelingTuplesList
			ModelingTuplesList=self.ModelingDict[DoingModelingString+'TuplesList']

			#Get the FlushingTuplesList
			FlushingTuplesList=zip(
									SYS.unzip(ModelingTuplesList,[0]),
									self.pick(SYS.unzip(ModelingTuplesList,[0]))
								)

			#If it is an output add the joins
			if _FlushingString=="Output":

				#Join before
				self.join()

				FlushingTuplesList+=zip( 
										map(
												lambda __JoiningChildObject:
												SYS.getModeledIntColumnStringWithKeyString(
													getattr(
														__JoiningChildObject,
														DoneJoiningKeyStringKeyString
													)
												),
												JoiningOrderedDict.values()
											),
										self.JoinedModeledFeaturedIntsTuplesList
									)+[('ModeledFeaturedIntsTuple',self.ModeledFeaturedIntsTuple)]
	'''


#</DefineClass>

#<DefineAttestingFunctions>
def attest_tabular():

	#Build Hdf groups
	Tablor=SYS.TablorClass().hdformat().update(
								[
									(
										'App_Structure_ChildGrouper1',
										SYS.GrouperClass().update(
										[
											(
												'App_Structure_GrandChildTablor1',
												SYS.TablorClass()
											)
										])
									)
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':"Structure"})
												]
											}
									).update(
								map(
										lambda __Tuple:
										(
											'/App_Structure_ChildGrouper1/App_Structure_GrandChildTablor1/'+__Tuple[0],
											__Tuple[1]
										),
										[
											('MyInt',0),
											('MyString',"hello"),
											('UnitsInt',3),
											('MyIntsList',[2,4,1]),
											('App_Model_FeaturingDict',
														{
															'ColumningTuplesList':
															[
																('MyInt',tables.Int64Col()),
																('MyString',tables.StringCol(10)),
																('MyIntsList',(tables.Int64Col,'UnitsInt'))
															]
														}
														),
											('model',{'ArgsVariable':"Feature"}),
											#('calibrate',{'ArgsVariable':"Feature"}),
											#('UnitsInt',2),
											#('MyIntsList',[2,4]),
											#('calibrate',{'ArgsVariable':"Feature"}),
											#('row',{'ArgsVariable':"Feature"}),
											#('flush',{})
										]
									)
							).close()
																
	#Return the object itself
	#Get the h5ls version of the stored hdf
	print(Tablor)
	print(os.popen('h5ls -dlr '+Tablor.HdformatingPathString).read())
	return os.popen('h5ls -dlr '+Tablor.HdformatingPathString).read()
	#</DefineMethods>

#</DefineAttestingFunctions>
