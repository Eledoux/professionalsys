#<ImportModules>
import collections
import copy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Modeler"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class TabularerClass(
					BasedLocalClass
				):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.TabularedGroup=None 								#<NotRepresented>
		self.TabularedSuffixString="" 							#<NotRepresented>
		self.TabularedKeyStringsList=[] 						#<NotRepresented>
		self.TabularedOrderedDict=collections.OrderedDict() 	#<NotRepresented>
		#</DefineSpecificDict>

	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"set")]})
	def set(self,**_VariablesDict):

		#Bind the reset 
		if self.SettingKeyVariable=='TabularedIsBool' and getattr(self,'TabularedIsBool') and self.SettingKeyVariable==False:
			self.init.DecoratedMethod(self)

	@SYS.SwitcherClass()
	@SYS.HookerClass(**{'AfterTuplesList':[("<TypeString>","model")]})
	def tabular(self,**_TabularingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Debug
		self.debug('Start of the method')
		
		#Maybe we have to hdformat first
		if self.HdformatedFilePointer==None and self.GroupedParentPointer==None:

			#Debug
			self.debug('We have to hdformat first')

			#Hdformat
			self.hdformat()

			#Debug
			self.debug('Ok this is hdformated')

		#Define Tabulared attributes
		self.TabularedGroup=self.HdformatedFilePointer.getNode(self.GroupedPathString)
		self.TabularedSuffixString=self.ModelingModelString+'Table'
		self.TabularedKeyStringsList=sorted(
											filter(
													lambda __KeyString:
													__KeyString.endswith(self.TabularedSuffixString),
													self.TabularedGroup._v_leaves.keys()
												)
										)
		
		self.TabularedOrderedDict.update(
									map(
											lambda __TabularedKeyString:
											(
												__TabularedKeyString,
												self.TabularedGroup._f_getChild(__TabularedKeyString)
											),
											self.TabularedKeyStringsList
										)
								)

		#Set the TabularedSuffixString
		if self.TabularedSuffixString=="":
			self.TabularedSuffixString='Model'.join(
				SYS.getTypeStringWithClassString(self.ModeledKeyString).split('Model')[:-1])+"Table"

		#Debug
		self.debug(("self.",self,['TabularedKeyStringsList']))
	
		#Debug
		self.debug('End of the method')
		
#</DefineClass>

#<DefineAttestingFunctions>
def attest_tabular():
	Tabularer=SYS.TabularerClass().update(								
									[
										('MyInt',0),
										('MyString',"hello"),
										('MyIntsList',[2,4,1]),
										('ModelingColumnTuplesList',
											[
												('MyInt',tables.Int64Col()),
												('MyString',tables.StringCol(10)),
												('MyIntsList',(tables.Int64Col(shape=3)))
											]
										)
									]).tabular().close()
																
	#Get the h5ls version of the stored hdf
	return SYS.represent(
						Tabularer
						)+'\n\n\n\n'+Tabularer.hdfview().HdformatedString	
#</DefineAttestingFunctions>
