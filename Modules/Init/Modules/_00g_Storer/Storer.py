#<ImportModules>
import collections
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class StorerClass(
					SYS.TablorClass
				):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.IsFeaturedStoredBool=True
		self.IsOutputedStoredBool=True
		self.FeaturedTabularedOrderedDict=collections.OrderedDict()
		self.OutputedTabularedOrderedDict=collections.OrderedDict()
		#</DefineSpecificDict>

	#</DefineHookMethods>

	#<DefineMethods>
	def store(self,_StoringString):

		#Get the DoneStoringString
		DoneStoringString=SYS.getDoneStringWithDoString(_StoringString)

		#Get the <Done>KeyString
		DoneStoringStringKeyStringKeyString=DoneStoringString+'KeyString'
		DoneKeyString=getattr(self,DoneStoringStringKeyStringKeyString)


		'''
		if DoneKeyString in [
											'',
											'RealMul','ComplexMul',
											'Config'
										]:
			print('STORE for',DoneKeyString)
			print('self.NewFeaturedModeledInt',self.NewFeaturedModeledInt)
			print('self.NewOutputedModeledInt',self.NewOutputedModeledInt)
		'''

		#Get the StoringOrderedDict
		StoringOrderedDictKeyString=DoneStoringString+"OrderedDict"
		if hasattr(self,StoringOrderedDictKeyString):
			StoringOrderedDict=getattr(self,StoringOrderedDictKeyString)

			#Get the StoringChildObjectsList
			StoringChildObjectsList=self.OrderedDict.values()

			#Store the children also
			map(
					lambda __StoringChildObject:
					__StoringChildObject.store(),
					StoringChildObjectsList
				)

			if self.IsFeaturedStoredBool:

				'''
				if DoneKeyString in [
												'',
												'Config'
											]:
					print('store',self.GroupedKeyString)
					print('self.NewFeaturedModeledInt>-1')
					print('so we can flush in features !')
					print('')
				'''

				self.flush(
								self.FeaturedTable,
								zip(
										SYS.unzip(self.FeaturingTuplesList,[0]),
										self.pick(SYS.unzip(self.FeaturingTuplesList,[1]))
									)
							)

				'''
				if DoneKeyString in [
												'',
												'Config'
											]:
					print('store',DoneKeyString)
					print('flush in features ok!')
					print('')
				'''

			if self.IsOutputedStoredBool:

				'''
				if DoneKeyString in [
												'',
												'Config'
											]:
					print('store'DoneKeyString)
					print('self.NewOutputedModeledInt>-1')
					print('so we can flush in outputs!')
					print('')
				'''

				self.flush(
							self.OutputedTable,
							zip(
									SYS.unzip(self.OutputingTuplesList,[0]),
									self.pick(SYS.unzip(self.OutputingTuplesList,[1]))
								)+zip( 
										map(
												lambda __StoringChildObject:
												SYS.getModeledIntColumnStringWithKeyString(
													__GroupedObject.GroupedKeyString
												),
												StoringChildObjectsList
											),
										self.JoinedModeledIntsList
									)+[('FeaturedModeledInt',self.FeaturedModeledInt)]
							)

				'''
				if DoneKeyString in [
												'',
												'Config'
											]:
					print('store',DoneKeyString)
					print('flush in outputs ok!')
					print('')
				'''

			'''
			if DoneKeyString in [
												'',
												'Config'
											]:
				print('END STORE for',DoneKeyString)
				print('')
			'''

			#Return self
			return self
	#</DefineMethods>		

#</DefineClass>

#<DefineAttestingFunctions>
def attest_store():

	#Build Hdf groups
	Storer=SYS.StorerClass().organize().update(
								[
									(
										'App_Structure_ChildGrouper1',
										SYS.GrouperClass().update(
										[
											(
												'App_Structure_GrandChildTablor1',
												SYS.StorerClass()
											)
										])
									)
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':"Structure"})
												]
											}
									).update(
								map(
										lambda __Tuple:
										(
											'/App_Structure_ChildGrouper1/App_Structure_GrandChildTablor1/'+__Tuple[0],
											__Tuple[1]
										),
										[
											('MyInt',0),
											('MyString',"hello"),
											('UnitsInt',3),
											('MyIntsList',[2,4,1]),
											('ModelingTuplesList',[
																	('MyInt',tables.Int64Col()),
																	('MyString',tables.StringCol(10)),
																	('MyIntsList',(tables.Int64Col,'UnitsInt'))
																]),
											('model',{'ArgsVariable':"Feature"}),
											('tabular',{'ArgsVariable':"Feature"}),
											('output',{}),
											('store',{})
										]
									)
							).close()
																
	#Get the h5ls version of the stored hdf
	print(Storer)
	print(os.popen('h5ls -dlr '+Storer.GroupingPathString).read())
	return os.popen('h5ls -dlr '+Storer.GroupingPathString).read()
	#</DefineMethods>

#</DefineAttestingFunctions>
