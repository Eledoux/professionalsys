#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class ObjectTesterClass(SYS.TesterClass):
	
	def test_0default(self):
		self.TestedVariable=self.TestedClass()

	def test_1update(self):
		self.TestedVariable=self.TestedClass().update(*[('MyInt',0)],**{'MyDict':{'MyString':"hello"}})

	def test_2set(self):
		self.TestedVariable=self.TestedClass().update(
									*[
										('MyFirstObject',self.TestedClass()),
										('/MyFirstObject/MyChildString',"coucou"),
										('MySecondObject',self.TestedClass()),
										('/MySecondObject',[
																('MyChildString',"hello"),
																('MyInt',0)
															])
									]
								)

	def test_3get(self):
		self.TestedVariable=self.TestedClass()['/__class__/TypeString']

	def test_4pick(self):
		self.TestedVariable=self.TestedClass().update(
											*[('MyInt',0)],
											**{'MyDict':{'MyString':"hello"}}
											)('pick',['MyInt','MyDict'])

#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply','testify',MethodStringsList)
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply','bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>
