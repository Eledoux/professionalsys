#<ImportModules>
import ShareYourSystem as SYS
import os
#</ImportModules>


#<DefineTestClass>
class ObjectTesterClass(SYS.TesterClass):
	'''
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	'''
	'''
	def test_01_getInNotOrderedDict(self):

		#Define an Object
		Object=SYS.ObjectClass().update([
											('MyInt',0),
											('MyObject',SYS.ObjectClass().update(
												[
													('MyString',"hello"),
													('MyFloat',0.1)
												]
												)
											)
										]
									)
		#Map some gets
		self.TestedVariable=map(
									lambda _KeyVariable:
									Object[_KeyVariable],
									[
										#Get directly in the __dict__
										'MyInt',
										'MyObject',

										#Get with copying
										'Cop_MyObject',

										#Get with a DeepShortString
										'/MyObject/MyString',

										#Get by the Object directly
										#Object
									]
								)

		#Change in the objects for looking at the copy difference
		Object['/MyObject/MyFloat']=0.4444
	'''
	'''
	def test_02_getInOrderedDict(self):

		#Define an Object
		Object=SYS.ObjectClass().update([
											('App_Group_MyAppendedObject',SYS.ObjectClass().update(
												[
													('MyOtherString',"bonjour"),
													('MyOtherFloat',0.2)
												]
												)
											),
											('App_Group_MySecondInt',3)
										])
		#Map some gets
		self.TestedVariable=map(
									lambda _KeyVariable:
									Object[_KeyVariable],
									[
										#Get with a AppendShortString
										'App_Group_MySecondInt',
										'App_Group_MyAppendedObject',
										'Cop_App_Group_MyAppendedObject',
										'/App_Group_MyAppendedObject/MyOtherString',
										#Or the AppendedInt
										'App_Group_0',

										#Get in the class object
										'/__class__/TypeString'

									]
								)

		#Change in the objects for looking at the copy difference
		Object['/App_Group_MyAppendedObject/MyOtherFloat']=0.5555

	def test_03_getWithAttributeString(self):

		#Define an ObjectGetterVariable
		Object=SYS.ObjectClass().update([
											('MyDict',{'MyInt':0}),
											('MyObject',SYS.ObjectClass().__setitem__('MySecondInt',2))
										]
									)

		#Get with AttributeShortString
		self.TestedVariable=[
								Object['MyDict.values()'],
								Object['MyObject.MySecondInt']
							]

	def test_04_setInNotOrderedDict(self):

		#Explicit expression
		self.TestedVariable=self.TestedClass().__setitem__('MyInt',0)
		
		#Set with a deep short string
		self.TestedVariable.__setitem__('MyObject',SYS.ObjectClass()).__setitem__('/MyObject/MyString','hello')

		#Set with a deep deep short string
		self.TestedVariable.__setitem__('/MyObject/MyGrandOtherObject',SYS.ObjectClass())

		#Set with a pointer short string
		self.TestedVariable.__setitem__('MyObjectPointer','/MyObject')

	def test_05_setInOrderedDict(self):

		#Short expression and set in the appended manner
		self.TestedVariable=self.TestedClass().__setitem__('App_Sort_MySecondInt',1)

		#Short expression for setting in the append without binding
		self.TestedVariable['/App_Sort_MySecondInt']+=1

		#Short expression for setting in the appended manner a structured object
		self.TestedVariable['App_Group_MyAppendedObject']=SYS.ObjectClass()
		
		#Set with a deep short string and append string
		self.TestedVariable['/App_Group_MyAppendedObject/MyString']="bonjour"

		#Set with a deep deep short string and append string
		self.TestedVariable['/App_Group_MyAppendedObject/App_Group_MyGrandChildObject']=SYS.ObjectClass()

	def test_06_setWithMethodString(self):

		#Set for being a call of method
		self.TestedVariable=self.TestedClass().__setitem__('__setitem__',{'ArgsVariable':('MySecondInt',1)})

	def test_07_hook(self):

		#Define a derived class and set it with SYS
		class BindingObjectClass(SYS.ObjectClass):

			#<DefineHookMethods>
			def setBefore(self):

				#Convert like a deserialization
				if SYS.getCommonSuffixStringWithStringsList([
					self.SettingVariable,'String'
					])=='String':
					self.SettingVariable=self.SettingVariable.split('String')[0]+'Int'
					self.SettedVariable=(int)(self.SettedVariable)
					self[self.SettingVariable]=self.SettedVariable
					self.IsSettingBool=False

			#</DefineHookgMethods>

			#<DefineBindingMethods>
			def bindMyIncrementedIntAfter(self):
				self.MyIncrementedInt+=1
			def bindApp_Sort_MyIncrementedIntAfter(self):
				self['/App_Sort_MyIncrementedInt']+=1
			#</DefineBindingMethods>

		SYS.setClassWithClass(BindingObjectClass)

		#Set to call the bind
		self.TestedVariable=BindingObjectClass().__setitem__(
										'MyIncrementedString',
										"0"
									).__setitem__(
										'App_Sort_MyIncrementedString',
										"2"
									)

	def test_08_apply(self):

		#Apply a __setitem__ to several things 
		Object=self.TestedClass().apply('__setitem__',[
														('MyString',"Hello"),
														('MySecondString',"Bonjour")
													]
										)

		#Apply an apply is possible 
		self.TestedVariable=Object.apply(
								'__setitem__',
								[
												('MyThirdString',"GutenTag"),
												('apply',{'ArgsVariable':(
																'__setitem__',[
																			('MyInt',0)
																			]
																	)
														}
												),
												('MyNotLostString',"ben he"),
								]
					)

	def test_09_pick(self):

		#Define an Object
		Object=SYS.ObjectClass().update([
											('MyInt',0),
											('MyObject',SYS.ObjectClass().update(
												[
													('MyString',"hello"),
													('MyFloat',0.1)
												]
												)
											),
											('App_Group_MyAppendedObject',SYS.ObjectClass().update(
												[
													('MyOtherString',"bonjour"),
													('MyOtherFloat',0.2)
												]
												)
											),
											('App_Sort_MySecondInt',3)
										])
		#Map some gets
		self.TestedVariable=Object.pick(
									[
										#Get directly in the __dict__
										'MyInt',
										'MyObject',

										#Get with copying
										'Cop_MyObject',

										#Get with a DeepShortString
										'/MyObject/MyString',

										#Get with a AppendShortString
										'App_Sort_MySecondInt',
										'App_Group_MyAppendedObject',
										'Cop_App_Group_MyAppendedObject',
										'/App_Group_MyAppendedObject/MyOtherString',
										#Or the AppendedInt
										'App_Group_0',

										#Get in the class object
										'/__class__/TypeString'
									]
								)

		#Change in the objects for looking at the copy difference
		Object['/MyObject/MyFloat']=0.4444
		Object['/App_Group_MyAppendedObject/MyOtherFloat']=0.5555
	'''

	'''
	def test_10_gather(self):

		self.TestedVariable=self.TestedClass().__add__(
				[
					SYS.ObjectClass().update(
						[
							('StructuredKeyString',str(Int1))
						]).__add__(
							[
								SYS.ObjectClass().update(
											[
												('StructuredKeyString',str(Int2))
											]
										)
								for Int2 in xrange(2)
							]
						) for Int1 in xrange(2)
				]).update(
				[
					('App_Group_'+str(Int1),SYS.ObjectClass().update(
						[
							('GroupedKeyString',str(Int1))
						]).update(
							[
								('App_Group_'+str(Int2),SYS.ObjectClass().update(
											[
												('GroupedKeyString',str(Int2))
											]
										)
								)
								for Int2 in xrange(2)
							]
						)
					) for Int1 in xrange(2)
				]).gather([
								['/'],
								'StructuredOrderedDict.items()',
								'GroupedOrderedDict.items()'
							])
	'''
	'''
	def test_11_collect(self):

		Object=self.TestedClass().__add__(
				[
					SYS.ObjectClass().update(
						[
							('StructuredKeyString',str(Int1))
						]).__add__(
							[
								SYS.ObjectClass().update(
											[
												('StructuredKeyString',str(Int2))
											]
										)
								for Int2 in xrange(2)
							]
						) for Int1 in xrange(2)
				])

		self.TestedVariable=Object.collect([
								['/'],
								'StructuredOrderedDict.items()',
								['App_Structure_1'],
								#[Object['App_Structure_0']]
							],
							[
								['StructuredKeyString']
							]
						)
	'''

	'''
	def test_11_update(self):

		#Update several things
		Object=self.TestedClass().update([
											('MyInt',0),
											('App_Sort_MySecondInt',0),
											('MyFloat',0.2)
										]
									)

		#Update in a update is possible
		self.TestedVariable=Object.update([
											('MyString',"Hello"),
											('update',{'ArgsVariable':[
														('MySecondInt',2)
														]}
											),
											('MyNotLostString',"ben heee")
										]
									)

	def test_12_append(self):

		#Append with a TuplesList
		Object=self.TestedClass().append([
											('StructuredKeyString',"MyTuplesList"),
											('MyString',"Hello")
										]
										)

		#Append with a dict
		self.TestedVariable=Object.append({
											'StructuredKeyString':"MyDict",
											'MyOtherString':"Bonjour"
											}
										)

		#Append with an Object
		self.TestedVariable=Object.append(SYS.ObjectClass())

	def test_13_add(self):

		#Explicit expression
		self.TestedVariable=self.TestedClass().__add__([
													[
														('StructuredKeyString',"MyTuplesList"),
														('MyString',"Hello")
													],
													{
														'StructuredKeyString':"MyDict",
														'MyOtherString':"Bonjour"
													},
													SYS.ObjectClass()
												])
	'''
	'''
	def test_14_commandAllSetsForEach(self):

		self.TestedVariable=self.TestedClass().__add__(
				[
					SYS.ObjectClass().update(
						[
							('StructuredKeyString',str(Int1))
						]) for Int1 in xrange(2)
				]
			).execute('self.__class__.CountingInt=0').commandAllSetsForEach(
					[
						(
							'MyCountingInt',
							';'.join([
										"Exec_self.GettedVariable=self.__class__.CountingInt",
										"self.__class__.CountingInt+=1"
									])
						),
						(
							'MyCountingInt',
							';'.join([
										"Exec_self.GettedVariable=self.__class__.CountingInt",
										"self.__class__.CountingInt+=1"
									])
						)
					],
					[
						['/'],
						'StructuredOrderedDict.items()'
					])
	'''
	'''
	def test_15_commandEachSetForAll(self):
		
		self.TestedVariable=self.TestedClass().__add__(
				[
					SYS.ObjectClass().update(
						[
							('StructuredKeyString',str(Int1))
						]) for Int1 in xrange(2)
				]
			).execute('self.__class__.CountingInt=0').commandEachSetForAll(
					[
						(
							'MyCountingInt',
							';'.join([
										"Exec_self.GettedVariable=self.__class__.CountingInt",
										"self.__class__.CountingInt+=1"
									])
						),
						(
							'MyCountingInt',
							';'.join([
										"Exec_self.GettedVariable=self.__class__.CountingInt",
										"self.__class__.CountingInt+=1"
									])
						)
					],
					[
						['/'],
						'StructuredOrderedDict.items()'
					])
	'''

	def test_16_walk(self):
		
		Object=self.TestedClass().__add__(
				[
					SYS.ObjectClass().update(
						[
							('StructuredKeyString',str(Int1))
						]).__add__(
							[
								SYS.ObjectClass().update(
											[
												('StructuredKeyString',str(Int2))
											]
										)
								for Int2 in xrange(2)
							]
						) for Int1 in xrange(2)
				]
			).walk(
						[
							'StructuredOrderedDict.items()'
						],
						**{
								'BeforeUpdatingTuplesList':[
										('MyCoOrderedInt',2)
									],
								'GrabbingVariablesList':
								[
									['StructuredKeyString']
								],
								'RoutingVariablesList':
								[
									['StructuredKeyString']
								]
							}
					)
		self.TestedVariable=Object
		
	def test_18_parentize(self):

		#Build a parentizing groups architecture
		self.TestedVariable=self.TestedClass().update(
									[
										('GroupingFilePathString',SYS.getCurrentFolderPathString()+'MyObject.hdf5'),
										(
											'App_Group_ChildObject1',
											SYS.ObjectClass().update(
											[
												('App_Group_GrandChildObject1',
												SYS.ObjectClass())
											])
										),
										(
											'App_Group_ChildObject2',
											SYS.ObjectClass().update(
												[]
											)
										)
									]	
								).walk(
											[
												'GroupedOrderedDict.items()'
											],
											**{
													'BeforeUpdatingTuplesList':
													[
														('parentize',{'ArgsVariable':"Group"})
													]
												}
										)

	def test_19_group(self):

		#Build Hdf groups
		Object=self.TestedClass().update(
									[
										('GroupingFilePathString',SYS.getCurrentFolderPathString()+'MyObject.hdf5'),
										(
											'App_Group_ChildObject1',
											SYS.ObjectClass().update(
											[
												('App_Group_GrandChildObject1',
												SYS.ObjectClass())
											])
										),
										(
											'App_Group_ChildObject2',
											SYS.ObjectClass().update(
												[]
											)
										)
									]	
								).walk(
											[
												'GroupedOrderedDict.items()'
											],
											**{
													'BeforeUpdatingTuplesList':
													[
														('parentize',{'ArgsVariable':"Group"}),
														('group',{})
													]
												}
										)

		#Get the h5ls version of the stored hdf
		self.TestedVariable=os.popen('h5ls -dlr '+Object.GroupingFilePathString).read()

#</DefineTestClass>

#<Test>

#Get the local test class and the tested method strings
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)

#Write the solutions to the tests
SYS.sys.modules['os'].popen("cd "+TestedClass().TestingFolderPathString+";rm *")
TestedClass().apply('testify',MethodStringsList)

#Test
TestedClass().update([
						('IsPrintedBool',True),
						('TestingClass',SYS.TestClass)
					]
					).apply('bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()

#</Test>
