#<ImportModules>
import copy
import inspect
import ShareYourSystem as SYS
import numpy
#</ImportModules>

#<DefineLocals>
DeepShortString="/"
AppendShortString="App_"
PointerShortString="Pointer"
CopyShortString='*'
CollectShortString=":"
#</DefineLocals>

def debug(_String):
	Frame=inspect.currentframe().f_back
	ScriptPathStringsList=SYS.getWordStringsListWithString(Frame.f_code.co_filename)
	ScriptPathString=''.join(ScriptPathStringsList)
	print("%s , %s, l. %s : "%(ScriptPathString,Frame.f_code.co_name,Frame.f_lineno));
	print(_String)
	print("")

#<DefineClass>
class ObjectClass(object):
	
	#<DefineMethods>

		#<InitMethod>
	def __init__(self):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""

		#<DefineSpecificDict>

		#Attributes for the __init__ property
		self.IsInitiatingBool=True

		#Attributes for the __getitem__ property
		self.IsGettingBool=True
		self.GettingVariable=None
		self.GettedVariable=None

		#Attributes for the __setitem__ property
		self.IsSettingBool=True
		self.SettedVariable=None
		self.SettingVariable=None

		#Attributes for the apply property
		self.IsApplyingBool=True
		self.AppliedVariablesList=[]

		#Attributes for the append property
		self.AppendedKeyStringsList=[]
		self.AppendedValueVariablesList=[]
		self.AppendedKeyStringToAppendedIntDict={}

		#Attributes for the group property
		self.GroupedKeyString=""
		self.GroupedPointer=None
		self.GroupedParentPointersList=[]
		self.GroupingHdfFile=None
		self.GroupingHdfFilePointer=None
		self.GroupedObjectsList=[]

		#Attributes for the structure property
		self.StructuredInt=-1
		self.StructuredKeyString=""
		self.StructuredPointer=None
		self.StructuredParentPointersList=[]
		self.StructuredObjectsList=[]

		#Attributes for the relay property (copy and set in DeliveredPointer)
		self.IsRelayingBool=True
		self.RelayedKeyVariablesList=[]
		self.RelayingKeyVariablesList=[]
		self.RelayingVariable=None

		#Attributes for the __repr__ property
		self.IsRepresentingBool=True
		self.RepresentingDict={}
		self.RepresentedTuplesList=[]
		self.NotRepresentingKeyStringsList=filter(
													lambda _KeyString:
													_KeyString not in [
																		'AppendedKeyStringsList',
																		'AppendedValueVariablesList',
																		'StructuredInt',
																		'StructuredKeyString',
																		'StructuredParentPointersList',
																		'GroupedKeyString',
																		'GroupedParentPointersList',
																		'GroupingHdfFile',
																		'GroupingHdfFilePointer'
																	],
													self.__class__.SpecificKeyStringsList
										) if type(self)==self.__class__ else []
		self.RepresentingKeyVariablesList=[
											'AppendedKeyStringsList',
											'AppendedValueVariablesList',
											'StructuredInt',
											'StructuredKeyString',
											'StructuredParentPointersList',
											'GroupedKeyString',
											'GroupedParentPointersList',
											'GroupingHdfFile',
											'GroupingHdfFilePointer'
											] if type(self)!=self.__class__ else []

		#</DefineSpecificDict>

		#Hook methods (integrativ)
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="init"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsInitiatingBool==False:
						return 

					#Call the HookingMethod
					HookingMethod(self)

	def __getitem__(self,_GettingVariable):
		"""Call the get<HookString> methods and return self.GettedVariable (None by default)"""
		
		#Refresh the attributes
		self.GettingVariable=_GettingVariable
		self.GettedVariable=None
		self.IsGettingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="get"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Debug
					#debug("HookingMethodString is : "+HookingMethodString)
					#debug("self.GettedVariable is : "+repr(self.GettedVariable))

					#Check Bool
					if self.IsGettingBool==False:
						return self.GettedVariable

		#Return GettedVariable
		return self.GettedVariable

	def __setitem__(self,_SettingVariable,_SettedVariable):
		"""Call the set<HookString> methods"""

		#Refresh the attributes
		self.SettingVariable=_SettingVariable
		self.SettedVariable=_SettedVariable
		self.IsSettingBool=True

		#Sort the HookMethodStrings
		for OrderString in ["Before","After"]:

			#Define a Local IsSettedBool
			IsSettedBool=False

			if type(_SettingVariable) in [str,unicode]:
			
				#Define the BindingMethodString
				BindingMethodString="bind"+_SettingVariable+OrderString
				
				#Check that there is HookingMethods for it
				if BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict:
					
					#Say ok
					IsSettedBool=True

					#Call the specific Appended methods 
					for HookingMethod,BindingMethod in self.__class__.BindingMethodStringToMethodTuplesListDict[BindingMethodString]:

						#Call the HookMethod
						if callable(HookingMethod):
							HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

						#Call the Binding
						if callable(BindingMethod):
							OutputVariable=BindingMethod(self)

							#End the Setting
							if self.IsSettingBool==False:
								return self

			#If a binding method was not yet found then look for a global set method
			if IsSettedBool==False:

				#_SettingVariable is not a string
				HookingMethodString="set"+OrderString

				#Check that there is HookingMethods for it
				if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

					#Call the specific Appended methods 
					for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

						#Call the HookingMethod
						HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

		#Return self
		return self

	"""
	def map(self,_MethodString,_MappingVariablesList):

		#Check that the method exists
		if callable(self,_MethodString):

			#Define the MappingMethod
			Method=getattr(self,_MethodString)

			#Look for the shape of the inputs of this method
			ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(Method)

			#Apply for each with a map and the ApplyingMethod
			if len(ArgsList)>2:
				return map(
						lambda __MappingVariable:
						Method(*__MappingVariable),
						_MappingVariablesList
					)
			else:
				return map(
							lambda __MappingVariable:
							Method(__MappingVariable),
							_MappingVariablesList
						)


	def call(_MethodString,_CallingVariable):

		#Check that the method exists
		if callable(self,_MethodString):

			#Define the MappingMethod
			Method=getattr(self,_MethodString)

			#Look for the shape of the inputs of this method
			ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(Method)

			#Apply for each with a map and the ApplyingMethod
			if len(ArgsList)>2:
				return Method(*_CallingVariable)
			else:
				return Method(_CallingVariable)

	def reduce(self,_OrderingStringsList,_OrderedVariable):

		map(
				lambda __OrderingString:
				getattr(__OrderingString,_OrderedVariablesList),
				_OrderingStringsList
			)

	def order(self,_OrderingStringsList,_OrderedVariablesList):

		map(
				lambda __OrderingString:
				self.map(__OrderingString,_OrderedVariablesList),
				_OrderingStringsList
			)
	"""

	def apply(self,_ApplyVariable,_ApplyingVariablesList):
		"""Call the apply<HookString> methods"""
		
		#Refresh the attributes
		LocalApplyVariable=_ApplyVariable
		LocalApplyingVariablesList=_ApplyingVariablesList
		self.IsApplyingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="apply"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,LocalApplyVariable,LocalApplyingVariablesList)

					if type(OutputVariable)==dict:
						if 'LocalApplyVariable' in OutputVariable:
							LocalApplyVariable=OutputVariable['LocalApplyVariable']
						if 'LocalApplyingVariablesList' in OutputVariable:
							LocalApplyingVariablesList=OutputVariable['LocalApplyingVariablesList']

					#Check Bool
					if self.IsApplyingBool==False:
						return self

		#Return self
		return self

	def pick(self,_PickingVariablesList):
		"""Apply the __getitem__ to the <_PickingVariablesList>"""

		#Apply __getitem__
		self.apply('__getitem__',_PickingVariablesList)

		#Return AppliedVariablesList
		return self.AppliedVariablesList

	def update(self,_UpdatingVariablesList):
		"""Apply the __setitem__ to the <_UpdatingVariablesList>"""

		#Apply __setitem__
		return self.apply('__setitem__',_UpdatingVariablesList)

	def collect(self,_CollectingVariable):

		#if SYS.getCommonPrefixStringWithStringsList([_CollectingVariable,CollectShortString])==CollectShortString:

			#CollectingString=CollectShortString.join(_CollectingVariable.split(CollectShortString)[1])

			#return getattr(self,self.getPluralStringWithSingularString(CollectingString))
			#if SYS.getCommonPrefixStringWithStringsList([CollectingString,'Structures'])=='Structures':
			#	return self.StructuresList
		return self.pick(_CollectingVariable)

	def modify(self,_ModifyingVariable):
		if type(_ModifyingVariable)==tuple:
			return self.__setitem__(*_ModifyingVariable)
		elif SYS.getIsTuplesListBool(_ModifyingVariable):
			return self.update(_ModifyingVariable)

	def append(self,_AppendingVariable):
		"""Call __setitem__ with a defined SettingVariable contained in the _AppendingVariable"""
		
		#Define the SettingVariable
		SettingVariable="App_"
		if SYS.getIsTuplesListBool(_AppendingVariable) and _AppendingVariable[0][0]=='StructuredKeyString':
				SettingVariable+=_AppendingVariable[0][1]
		elif self.__class__ in type(_AppendingVariable).__mro__:
			SettingVariable+=_AppendingVariable.StructuredKeyString
		elif type(_AppendingVariable)==dict and 'StructuredKeyString' in _AppendingVariable:
			SettingVariable+=_AppendingVariable['StructuredKeyString']

		#Then set
		return self.__setitem__(SettingVariable,_AppendingVariable)

	def __add__(self,_AddingVariablesList):
		"""Apply the append to the <_AddingVariablesList>"""

		#Apply
		self.apply('append',_AddingVariablesList)

		#Return 
		return self

	def __delitem__(self,_DeletingVariable):
		"""Call the del<HookString> methods and delete in the __dict__ by default"""
		
		#Refresh the attributes
		self.DeletingVariable=_DeletingVariable
		self.IsDeletingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="delete"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsDeletingBool==False:
						return self

		#Return self
		return self

	def relay(self,_RelayingVariable):
		"""Call the relay<HookString> methods and return self"""

		#Refresh the attributes
		self.RelayingVariable=_RelayingVariable
		self.RelayedPointer=self
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="relay"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsRelayingBool==False:
						return self.RelayedPointer

		#Return self.RelayedVariable
		return self.RelayedPointer

	def control(self,_ControllingVariablesList):
		"""Apply relay for each element of the _ControllingVariablesList"""

		#Apply relay
		self.apply('relay',_ControllingVariablesList)

		#return self
		return self
	#</CommunicationMethods>

	#<CallMethod>
	def __call__(self,_CallVariable,*_CallingVariablesList):
		"""Call the <_CallString><HookString> methods and return self.CalledVariable (self by default)"""

		#Refresh the attributes
		LocalCallVariable=_CallVariable
		LocalCallingVariablesList=_CallingVariablesList
		LocalCalledPointer=self

		#self.CalledPointer=self
		self.IsCallingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString=LocalCallVariable+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,LocalCallVariable,LocalCallingVariablesList)

					if type(OutputVariable)==dict:
						if 'LocalCallVariable' in OutputVariable:
							LocalCallVariable=OutputVariable['LocalCallVariable']
						if 'LocalCallingVariablesList' in OutputVariable:
							LocalCallingVariable=OutputVariable['LocalCallingVariablesList']
						if 'LocalCallPointer' in OutputVariable:
							LocalCallPointer=OutputVariable['LocalCallPointer']

					#Check Bool
					if self.IsCallingBool==False:
						return LocalCalledPointer

		#Return the OutputVariable
		return LocalCalledPointer
	#</CallMethod>

	#<PrintMethod>
	def __repr__(self):
		"""Call the repr<HookString> methods and return a SYS printed version of the self.PrintedDict"""

		#Refresh the attributes
		self.RepresentingDict=copy.deepcopy(SYS.PrintingDict)
		self.RepresentedTuplesList=self.__dict__.items()
		self.IsRepresentingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="repr"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsRepresentingBool==False:
						return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
								self,
								self.RepresentedDict
								)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
								dict(self.RepresentedTuplesList),
								self.RepresentingDict
								)

		#return the Print
		return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
				self,
				self.RepresentingDict
				)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
				dict(self.RepresentedTuplesList),
				self.RepresentingDict
				)
	#</PrintMethod>
	'''
	def structure(self):
		if self.StructuredPointer!=None:
			self['StructuredParentPointersList']=[self.StructuredPointer]+self.StructuredPointer.StructuredParentPointersList
		return self
	'''

	def transfer(self,_CollectingVariable,_ModifyingVariable,_TransferingStringsList):

		#Define the CollectedVariablesList
		CollectedVariablesList=self.collect(_CollectingVariable)

		#Map and Modify/Transfer
		if _TransferingStringsList[0]=='map':

			#Modify in each DictatedValue
			map(
					lambda __DictatedValueVariable:
					map(
							lambda __TransferingString:
							getattr(__DictatedValueVariable,__TransferingString)(_GroupingVariable)
							if __TransferingString=='modify'
							else
							getattr(__DictatedValueVariable,__TransferingString)(),
							_TransferingStringsList[1:]
						),
					CollectedVariablesList
				)

		else:
			pass

		#Map and Modify/Transfer

		#if self.GroupedPointer!=None:
		#	self['GroupedParentPointersList']=[self.GroupedPointer]+self.GroupedPointer.GroupedParentPointersList
		return self

	#</DefineMethods>

	#<DefineHookMethods>

	def getBefore(self):
		"""
			Hook in the __getitem__ of the Object 
			with different possible ShortStrings

			If _GettingVariable is an int, it returns the Value in the corresponding Index
			in self.AppendedValueVariablesList

			If _GettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			If _GettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			Else do a __getitem__ in the __dict__
		"""

		#Debug
		#debug("self.GettingVariable is : "+repr(self.GettingVariable))
		#debug("self.GettedVariable is : "+repr(self.GettedVariable))

		#Init a local IsGettedBool
		IsGettedBool=False

		#Get with the AppendedInt in the ordered "dict"
		if type(self.GettingVariable)==int:
			if self.GettingVariable<len(self.AppendedValueVariablesList):

				#Get the GettedVariable from the AppendedValueVariablesList and the Index
				self.GettedVariable=self.AppendedValueVariablesList[self.GettingVariable]

				#Stop the getting
				self.IsGettingBool=False

				#Return
				return

		#Get with DeepShortString
		elif self.GettingVariable==DeepShortString:
			
			#If it is directly DeepShortString
			self.GettedVariable=self

			#Stop the getting
			self.IsGettingBool=False

			#Return
			return 

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,AppendShortString])==AppendShortString:

			#Get with the * 
			self.GettingVariable=self.GettingVariable.split(AppendShortString)[1]
			if self.GettingVariable in self.AppendedKeyStringToAppendedIntDict:
				
				#Get the GettedVariable from the AppendedValueVariablesList and the KeyStringToAppendedIntDict
				self.GettedVariable=self.AppendedValueVariablesList[self.AppendedKeyStringToAppendedIntDict[self.GettingVariable]]

				#Stop the getting
				self.IsGettingBool=False

				#Return 
				return
		
		#Get with '/'
		elif self.GettingVariable==DeepShortString:
			
			#If it is directly '/'
			self.GettedVariable=self

			#Stop the getting
			self.IsGettingBool=False

			#Return
			return 

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,DeepShortString])==DeepShortString:

			#Define the GettingVariableStringsList
			GettingVariableStringsList=DeepShortString.join(self.GettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
			GettedVariable=self[GettingVariableStringsList[0]]
			if GettedVariable!=None:

				if len(GettingVariableStringsList)==1:

					#Direct update in the Child or go deeper with the ChildPathString
					self.GettedVariable=GettedVariable

					#Stop the getting
					self.IsGettingBool=False
				
					#Return 
					return 

				else:

					#Define the ChildPathString
					ChildPathString=DeepShortString.join(GettingVariableStringsList[1:])	

					#Get in the first deeper Element with the ChildPathString
					self.GettedVariable=GettedVariable[ChildPathString]

					#Stop the getting
					self.IsGettingBool=False

					#Return
					return 

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,CopyShortString])==CopyShortString:

			#deepcopy
			self.GettedVariable=copy.deepcopy(
					self[CopyShortString.join(self.GettingVariable.split(CopyShortString)[1:])]				
				)

			#Stop the getting
			self.IsGettingBool=False

			#Return 
			return 

		elif self.GettingVariable=="DictatedVariablesList":

			#Return values of the __dict__
			self.GettedVariable=self.__dict__.values()

			#Stop the getting
			self.IsGettingBool=False

			#Return 
			return  

		elif self.GettingVariable=="AppendedVariablesList":

			#Return AppendedValueVariablesList
			self.GettedVariable=self.AppendedValueVariablesList

			#Stop the getting
			self.IsGettingBool=False

			#Return 
			return  

		#Do the minimal get
		if IsGettedBool==False:
		
			if type(self.GettingVariable) in [str,unicode]:

				#Get safely the Value
				if self.GettingVariable in self.__dict__:

					#__getitem__ in the __dict__
					self.GettedVariable=self.__dict__[self.GettingVariable]

					#Stop the getting
					self.IsGettingBool=False

				elif self.GettingVariable=="__class__":

					#Return the __dict__ of the __class__
					self.GettedVariable=self.__class__.__dict__

					#Stop the getting
					self.IsGettingBool=False

	def setBefore(self):
		"""
			Hook in the __setitem__ of the Object 
			with different possible ShortStrings

			If _SettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the setting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedValueVariablesList[<String>]
			and keeps on the getting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is <camelCase> then it is a hook query...
			so it calls with the form self(<camelCase>,SettedVariable)

			If _SettingVariable is of the form <String1>Pointer and _SettedVariable of the form /<String2>
			it gets the self[<String2>] and do self[<String1>Pointer]=self[<String2>]

			Else do a __setitem__ in the __dict__ 
		"""

		#Appending set
		if SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,AppendShortString])==AppendShortString:

			#Define the AppendedKeyString
			AppendedKeyString=self.SettingVariable.split(AppendShortString)[1]

			#Update an already
			if AppendedKeyString in self.AppendedKeyStringToAppendedIntDict:
				self.AppendedValueVariablesList[self.AppendedKeyStringToAppendedIntDict[AppendedKeyString]]=self.SettedVariable
			else:
				#Append
				self.AppendedKeyStringsList+=[AppendedKeyString]
				self.AppendedValueVariablesList+=[self.SettedVariable]
				self.AppendedKeyStringToAppendedIntDict[AppendedKeyString]=len(self.AppendedValueVariablesList)-1

			#If it is an object
			if self.__class__ in type(self.SettedVariable).__mro__:

				#Structure in the Child
				self.SettedVariable.StructuredInt=len(self.AppendedValueVariablesList)-1
				self.SettedVariable.StructuredKeyString=AppendedKeyString
				self.SettedVariable.StructuredPointer=self

				#Update the StructuringObjectsList
				self.StructuredObjectsList.append(self.SettedVariable)

			#Return 
			return

		#Deep set
		elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,DeepShortString])==DeepShortString:

			#Get the SettingVariableStringsList
			SettingVariableStringsList=DeepShortString.join(self.SettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
			GettedVariable=self[SettingVariableStringsList[0]]
			if GettedVariable!=None:
				
				#Define the ChildPathString
				ChildPathString=DeepShortString.join(SettingVariableStringsList[1:])	

				#Direct update in the Child or go deeper with the ChildPathString
				if ChildPathString=="": 
					if self.__class__ in type(GettedVariable).__mro__:

						#Modify directly the GettedVariable with self.SettedVariable
						if GettedVariable.modify(self.SettedVariable)==None:

							#This is in that case a set in the append but not binded
							if SYS.getCommonPrefixStringWithStringsList([SettingVariableStringsList[0],AppendShortString])==AppendShortString:
								AppendedKeyString=SettingVariableStringsList[0].split(AppendShortString)[1]
								if AppendedKeyString in self.AppendedKeyStringToAppendedIntDict:
									self.AppendedValueVariablesList[self.AppendedKeyStringToAppendedIntDict[AppendedKeyString]]=self.SettedVariable
				else:

					#Set in the first deeper Element with the ChildPathString
					GettedVariable[ChildPathString]=self.SettedVariable

			#Return 
			return 

		#Call for a hook
		elif self.SettingVariable[0].lower()==self.SettingVariable[0]:

			#Get the Method
			if hasattr(self,self.SettingVariable):

				#Get the method
				SettingMethod=getattr(self,self.SettingVariable)

				#Look for the shape of the inputs of this method
				ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(SettingMethod)

				#Adapt the shape of the args
				if len(ArgsList)>2:
					getattr(self,self.SettingVariable)(*self.SettedVariable)
				else:
					getattr(self,self.SettingVariable)(self.SettedVariable)
			
			elif len(self.SettedVariable)==1:

				#Adapt format of the ArgsList
				self(self.SettingVariable,self.SettedVariable[0])
			else:
				self(self.SettingVariable,*self.SettedVariable)

			#Return
			return

		elif SYS.getCommonSuffixStringWithStringsList([self.SettingVariable,PointerShortString])==PointerShortString:
				
			#Special set for Pointer with a / getted Value
			if type(self.SettedVariable) in [str,unicode]:

				#The Value has to be a deep short string for getting the pointed variable
				if SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,DeepShortString])==DeepShortString:
					GettedVariable=self[self.SettedVariable]
					if GettedVariable!=None:
						#Link the both with a __setitem__
						self[self.SettingVariable]=GettedVariable

				#Return in the case of the Pointer and a string setted variable
				return

		#Do the minimal set if nothing was done yet
		if type(self.SettingVariable) in [str,unicode]:

			#__setitem__ in the __dict__
			self.__dict__[self.SettingVariable]=self.SettedVariable

			#Group in the Child
			if self.__class__ in type(self.SettedVariable).__mro__ and SYS.getCommonSuffixStringWithStringsList([self.SettingVariable,PointerShortString])!=PointerShortString:
				
				#Group the Child
				self.SettedVariable.GroupedKeyString=self.SettingVariable
				
				#Update the StructuringObjectsList
				self.GroupedObjectsList.append(self.SettedVariable)


	def applyBefore(self,_LocalApplyVariable,_LocalApplyingVariablesList):
		"""
			Hook in the apply of the Object
		"""

		#Get the AppliedMethod
		ApplyString=str(_LocalApplyVariable)
		if hasattr(self,ApplyString):

			#Define the ApplyingMethod
			ApplyingMethod=getattr(self,ApplyString)

			#Look for the shape of the inputs of this method
			ArgsList,VarArgsList,VarKwargsDict,DefaultsList=inspect.getargspec(ApplyingMethod)

			#Apply for each with a map and the ApplyingMethod
			if len(ArgsList)>2:
				self.AppliedVariablesList=map(
						lambda __ApplyingVariable:
						ApplyingMethod(*__ApplyingVariable),
						_LocalApplyingVariablesList
					)
			else:
				self.AppliedVariablesList=map(
							lambda __ApplyingVariable:
							ApplyingMethod(__ApplyingVariable),
							_LocalApplyingVariablesList
						)
		else:

			#Apply for each with a map and a Call
			self.AppliedVariablesList=map(
					lambda __ApplyingVariable:
					#ApplyVariable is a CallString and _ApplyingVariable is a CallingList
					self(_LocalApplyVariable,*[__ApplyingVariable])
					if type(__ApplyingVariable)!=list
					else self(_LocalApplyVariable,__ApplyingVariable),
					_LocalApplyingVariablesList
				)
		
	def delBefore(self):
		"""
			Hook in the __delitem__ of the Object
		"""

		#Do the minimal delitem
		if type(self.DeletingVariable) in [str,unicode]:

			#Del Safely the Value
			if self.DeletingVariable in self.__dict__:
				del self.__dict__[self.DeletingVariable]

	def relayBefore(self):
	
		#Update for the RelayedValueVariables
		map(
			lambda _RelayedValueVariable:
			_RelayedValueVariable.update(self.RelayingVariable) 
			if hasattr(_RelayedValueVariable,'update')
			else None,
			map(
					lambda _RelayedKeyVariable:
					self[_RelayedKeyVariable],
					self.RelayedKeyVariablesList
				)
			)

		#relay for the RelayingValueVariables
		map(
			lambda _RelayingValueVariable:
			_RelayingValueVariable.relay(self.RelayingVariable) 
			if hasattr(_RelayingValueVariable,'relay')
			else None,
			map(
					lambda _RelayingKeyVariable:
					self[_RelayingKeyVariable],
					self.RelayingKeyVariablesList
				)
			)

	def reprAfter(self):
		"""
			Hook in the repr of the Object
			for printing in the Console attributes in a more 
			user-friendly manner
		"""

		#Get the MroAppendedStringsList
		MroAppendedStringsList=SYS.getMroKeyStringsListWithClass(self.__class__)

		#Remove the NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.NotRepresentingKeyStringsList,
										self.RepresentedTuplesList
								)

		#First keeps only the Specific and New attributes
		self.RepresentedTuplesList=map(
									lambda _RepresentedTuple:
									("<Spe>"+_RepresentedTuple[0],_RepresentedTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] in self.__class__.SpecificKeyStringsList,
											self.RepresentedTuplesList
										)
								)+map(
									lambda _NewTuple:
									("<New>"+_NewTuple[0],_NewTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] not in MroAppendedStringsList,
											self.RepresentedTuplesList
										)
								)

		#Add some forced Values with RepresentingKeyVariables
		self.RepresentedTuplesList+=map(
											lambda _KeyVariable:
											("<NotSpe>"+str(_KeyVariable),self[_KeyVariable]),
											self.RepresentingKeyVariablesList
										)

		#Simplify the numpy variables repr
		self.RepresentedTuplesList=map(
										lambda _RepresentedTuple:
										_RepresentedTuple
										if type(_RepresentedTuple[1]) not in [numpy.ndarray] 
										else (
												_RepresentedTuple[0],
												"<numpy.ndarray shape "+str(numpy.shape(
													_RepresentedTuple[1]))+">"
												),
										self.RepresentedTuplesList
									)

	#</DefineHookMethods>
		
	#<DefineBindingMethods>
	"""
	def bindGroupedParentPointersListAfter(self):
		
		#Build a short alias to the HdfFile of the uppest Group Parent:
		if len(self.GroupedParentPointersList)>0:
			self['GroupingHdfFilePointer']=self.GroupedParentPointersList[-1].GroupingHdfFile

	def bindGroupingHdfFilePointerAfter(self):

		#Init the Group inside the file
		if self.GroupingHdfFilePointer!=None:

			#Define the GroupingPathString
			GroupingPathString='/'.join(
					map(
						lambda __GroupedParentPointer:
						__GroupedParentPointer.GroupedKeyString,
						self.GroupedParentPointersList
						)
					)

			#pass
			#self.GroupingHdfFilePointer.create_group(self.GroupingPathString)	
	"""
	#</DefineBindingMethods>

#</DefineClass>



