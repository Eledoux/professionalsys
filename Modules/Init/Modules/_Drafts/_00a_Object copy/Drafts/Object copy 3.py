#<ImportModules>
import copy
import inspect
import ShareYourSystem as SYS
import numpy
#</ImportModules>

#<DefineLocals>
DeepShortString="/"
OrderShortString="*"
PointerShortString="Pointer"
#</DefineLocals>

def debug(_String):
	Frame=inspect.currentframe().f_back
	ScriptPathStringsList=SYS.getWordStringsListWithString(Frame.f_code.co_filename)
	ScriptPathString=''.join(ScriptPathStringsList)
	print("%s , %s, l. %s : "%(ScriptPathString,Frame.f_code.co_name,Frame.f_lineno));
	print(_String)
	print("")

#<DefineClass>
class ObjectClass(object):
	
	#<DefineMethods>

		#<InitMethod>
	def __init__(self):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""

			#<DefineSpecificDict>

				#<InitAttributes>

		#Attributes for the __init__ property
		self.IsInitiatingBool=True

				#</InitAttributes>

				#<ApplyAttributes>

		self.IsApplyingBool=True
		self.ApplyingVariablesList=[]
		self.AppliedVariablesList=[]

				#</ApplyAttributes>

				#<SetAttributes>

					#<UnOrderedSetAttributes>

						#<UnitUnOrderedSetAttributes>

		#Attributes for the __setitem__ property
		self.IsSettingBool=True
		self.SettedVariable=None
		self.SettingVariable=None

						#</UnitUnOrderedSetAttributes>

						#<MappingUnOrderedSetAttributes>

		#Attributes for the update property
		self.IsUpdatingBool=True
		self.UpdatingVariable=None
		self.UpdatedVariable=None
				
						#</MappingUnOrderedSetAttributes>

					#</UnOrderedSetAttributes>

					#<OrderedSetAttributes>

						#<UnitOrderedSetAttributes>

		#Attributes for the append property
		self.IsAppendingBool=True
		self.AppendingKeyVariable=None
		self.AppendingValueVariable=None
		self.AppendedInt=-1
		self.AppendedStringsList=[]
		self.AppendedVariablesList=[]
		self.AppendedStringToAppendedIntDict={}
				
						#</UnitOrderedSetAttributes>

						#<MappingOrderedSetAttributes>

		#Attributes for the __add__ property
		self.IsAddingBool=True
		self.AddedVariable=None
				
						#</MappingOrderedSetAttributes>

					#</OrderedSetAttributes>

				#</SetAttributes>

				#<UnSetAttributes>

		#Attributes for the __delitem__ property
		self.IsDeletingBool=True

		#Attributes for the pop property
		self.IsPopingBool=True

				#</UnSetAttributes>

				#<GetAttributes>

					#<NotDeepGetAttributes>

						#<UnitNotDeepGetAttributes>

		#Attributes for the __getitem__ property
		self.IsGettingBool=True
		self.GettingVariable=None
		self.GettedVariable=None

						#</UnitNotDeepGetAttributes>

						#<MappingNotDeepGetAttributes>

		#Attributes for the pick property
		self.IsPickingBool=False
		self.PickedTuplesList=[]
		self.PickingKeyVariablesList=[]

						#</MappingNotDeepGetAttributes>

					#<NotDeepGetAttributes>

					#<DeepGetAttributes>

						#</UnitNotDeepGetAttributes>

		#Attributes for the copy property
		self.IsCopyingBool=False
		self.CopiedTuplesList=[]
		self.CopyingKeyVariablesList=[]

						#</UnitNotDeepGetAttributes>

						#<MappingDeepGetAttributes>

		#Attributes for the scan property
		self.IsScanningBool=False
		self.ScannedTuplesList=[]
		self.ScanningKeyVariablesList=[]

						#</MappingDeepGetAttributes>

					#<DeepGetAttributes>

				#</GetAttributes>

				#<ControlAttributes>

					#<NotDeepControlAttributes>

						#<UnitNotDeepControlAttributes>

		#Attributes for the share property (get and set/append in SharedPointer)
		self.IsSharingBool=True
		self.SharedPointer=None

						#</UnitNotDeepControlAttributes>

						#<MappingNotDeepControlAttributes>

		#Attributes for the link property (pick and update in LinkedPointer)
		self.IsLinkingBool=True
		self.LinkedDict=[]
		self.LinkedTuplesList=[]
		self.LinkedPointer=None

						#</MappingNotDeepControlAttributes>

					#</NotDeepControlAttributes>

					#<DeepControlAttributes>

						#<UnitDeepControlAttributes>

		#Attributes for the deliver property (copy and set/append in WrittenPointer)
		self.IsWritingBool=True
		self.WrittenPointer=None

						#<UnitDeepControlAttributes>

						#<MappingDeepControlAttributes>
				
		#Attributes for the deliver property (copy and set in DeliveredPointer)
		self.IsDeliveringBool=True
		self.DeliveredTuplesList=[]
		self.DeliveredPointer=None

						#</MappingDeepControlAttributes>

					#</DeepControlAttributes>

				#</ControlAttributes>

				#<PrintAttributes>

		#Attributes for the __repr__ property
		self.IsRepresentingBool=True
		self.RepresentingDict={}
		self.RepresentedTuplesList=[]
		self.NotRepresentingAppendedStringsList=filter(
													lambda _KeyString:
													_KeyString not in [
																		'AppendedStringsList',
																		'AppendedVariablesList'
																	],
													self.__class__.SpecificKeyStringsList
										) if type(self)==SYS.ObjectClass else []
		self.RepresentingKeyVariablesList=['AppendedStringsList','AppendedVariablesList'] if type(self)!=SYS.ObjectClass else []

				#</PrintAttributes>

				#<CallAttributes>

		#Attributes for the __call__ property
		self.IsCallingBool=True
		self.CallString=""
		self.CallingList=[]
		self.CallingDict={}
		self.CalledPointer=None

				#</CallAttributes>

		#Attributes for the use property
		self.UsedPointer=None

			#</DefineSpecificDict>

		#Hook methods (integrativ)
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="init"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsInitiatingBool==False:
						return 

					#Call the HookingMethod
					HookingMethod(self)

		#Return None

		#</InitMethod>

		#<ApplyMethod>

	def apply(_HookString,_ApplyingVariablesList):
		"""Call the apply<HookString> methods"""
		
		#Refresh the attributes
		self.ApplyingVariablesList=_ApplyingVariablesList
		self.IsApplyingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="apply"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsApplyingBool==False:
						return self

		#Return self
		return self

		#</ApplyMethod>

		#<SetMethods>

			#<UnOrderedSetMethods>

				#<UnitUnOrderedSetMethods>
	def __setitem__(self,_SettingVariable,_SettedVariable):
		"""Call the set<HookString> methods and set in the __dict__ in the default case"""

		#Refresh the attributes
		self.SettingVariable=_SettingVariable
		self.SettedVariable=_SettedVariable
		self.IsSettingBool=True

		#Sort the HookMethodStrings
		for OrderString in ["Before","After"]:

			#Define a Local IsSettedBool
			IsSettedBool=False

			if type(_SettingVariable) in [str,unicode]:
			
				#Define the BindingMethodString
				BindingMethodString="bind"+_SettingVariable+OrderString
				
				#Check that there is HookingMethods for it
				if BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict:
					
					#Say ok
					IsSettedBool=True

					#Call the specific Appended methods 
					for HookingMethod,BindingMethod in self.__class__.BindingMethodStringToMethodTuplesListDict[BindingMethodString]:

						#Call the HookMethod
						if callable(HookingMethod):
							HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

						#Call the Binding
						if callable(BindingMethod):
							OutputVariable=BindingMethod(self)

							#End the Setting
							if self.IsSettingBool==False:
								return self

			#If a binding method was not yet found then look for a global set method
			if IsSettedBool==False:

				#_SettingVariable is not a string
				HookingMethodString="set"+OrderString

				#Check that there is HookingMethods for it
				if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

					#Call the specific Appended methods 
					for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

						#Call the HookingMethod
						HookingMethod(self)

						#Check Bool
						if self.IsSettingBool==False:
							return self

		#Return self
		return self

			#</UnitUnOrderedSetMethods>
		


		#<MappingUnOrderedSetMethods>

	def update(self,*_UpdatingList,**_UpdatingDict):
		"""Call __setitem__ for each Tuple in UpdatingList and UpdatingDict"""

		#Set with the UpdatingList
		map(
				lambda _UpdatingTuple:
				self.__setitem__(_UpdatingTuple[0],_UpdatingTuple[1]),
				filter(lambda _Tuple:len(_Tuple)==2,_UpdatingList)
			)

		#Set with the UpdatingDict
		map(
				lambda Tuple:
				self.__setitem__(Tuple[0],Tuple[1]),
				_UpdatingDict.items()
			)
					
		#Return 
		return self

		#<MappingUnOrderedSetMethods>

	def __delitem__(self,_DeletingVariable):
		"""Call the del<HookString> methods and delete in the __dict__ by default"""
		
		#Refresh the attributes
		self.DeletingVariable=_DeletingVariable
		self.IsDeletingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="delete"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsDeletingBool==False:
						return self

		#Return self
		return self

	def __getitem__(self,_GettingVariable):
		"""Call the get<HookString> methods and return self.GettedVariable (None by default)"""
		
		#Refresh the attributes
		self.GettingVariable=_GettingVariable
		self.GettedVariable=None
		self.IsGettingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="get"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Debug
					#debug("HookingMethodString is : "+HookingMethodString)
					#debug("self.GettedVariable is : "+repr(self.GettedVariable))

					#Check Bool
					if self.IsGettingBool==False:
						return self.GettedVariable

		#Return GettedVariable
		return self.GettedVariable

	def __call__(self,_CallString,*_CallingList,**_CallingDict):
		"""Call the <_CallString><HookString> methods and return self.CalledVariable (self by default)"""

		#Refresh the attributes
		self.CallString=_CallString
		self.CallingList=_CallingList
		self.CallingDict=_CallingDict
		self.CalledPointer=self
		self.IsCallingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString=self.CallString+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsCallingBool==False:
						return self.CalledPointer

		#Return the OutputVariable
		return self.CalledPointer

	#Define a generic add Method
	def __add__(self,_AddedVariable):
		"""Call the add<HookString> methods and return self"""

		#Refresh the attributes
		self.AddedVariable=_AddedVariable
		self.IsAddingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="add"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					HookingMethod(self)

					#Check Bool
					if self.IsCallingBool==False:
						return self

		#Return self to use it directly in one command line
		return self

	def __repr__(self):
		"""Call the repr<HookString> methods and return a SYS printed version of the self.PrintedDict"""

		#Refresh the attributes
		self.RepresentingDict=copy.deepcopy(SYS.PrintingDict)
		self.RepresentedTuplesList=self.__dict__.items()
		self.IsRepresentingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="repr"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsRepresentingBool==False:
						return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
								self,
								self.RepresentedDict
								)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
								dict(self.RepresentedTuplesList),
								self.RepresentingDict
								)

		#return the Print
		return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(
				self,
				self.RepresentingDict
				)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(
				dict(self.RepresentedTuplesList),
				self.RepresentingDict
				)

	def pick(self):
		"""Call the pick<HookString> methods and return a PickedTuplesList"""

		#Refresh the attributes
		self.PickedTuplesList=[]
		self.IsPickingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="pick"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsPickingBool==False:
						return self.PickedTuplesList

		#return the PickedTuplesList
		return self.PickedTuplesList

	def copy(self):
		"""Call the copy<HookString> methods and return a CopiedTuplesList"""

		#Refresh the attributes
		self.CopiedTuplesList=[]
		self.IsCopyingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="pick"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsCopyingBool==False:
						return self.CopiedTuplesList

		#return the CopiedTuplesList
		return self.CopiedTuplesList

	def link(self,_LinkedPointer):
		"""Call the link<HookString> methods for picking in the self and setting in the LinkedPointer"""

		#Refresh the attributes
		self.LinkedTuplesList=self('pick')
		self.LinkedPointer=_LinkedPointer

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="pick"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsCopyingBool==False:
						return self

		#return self
		return self

	def deliver(self,_DeliveredPointer):
		"""Call the deliver<HookString> methods for copying in the self and setting in the DeliveredPointer"""

		#Refresh the attributes
		self.DeliveredTuplesList=self('copy')
		self.DeliveredPointer=_DeliveredPointer

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="deliver"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsDeliveringBool==False:
						return self

		#return self
		return self

	def append(self,_AppendingTuple):
		"""Call the append<HookString> methods for append in the OrderedKeyStringsList and OrderedVariablesList"""

		#Refresh the attributes
		self.AppendingKeyVariable=_AppendingTuple[0]
		self.AppendingValueVariable=_AppendingTuple[1]

		#Set the corresponding AppendedInt
		self.AppendedInt=len(self.AppendedStringsList)

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="append"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsAppendingBool==False:
						return self

		#return self
		return self
	#</DefineMethods>

	#<DefineHookMethods>
	def getBefore(self):
		"""
			Hook in the __getitem__ of the Object 
			with different possible ShortStrings

			If _GettingVariable is an int, it returns the Value in the corresponding Index
			in self.AppendedVariablesList

			If _GettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			If _GettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedVariablesList[<String>]
			and keeps on the getting with <String> at GettedVariable[<String>]

			Else do a __getitem__ in the __dict__
		"""

		#Debug
		#debug("self.GettingVariable is : "+repr(self.GettingVariable))
		#debug("self.GettedVariable is : "+repr(self.GettedVariable))

		#Get with the AppendedInt in the ordered "dict"
		if type(self.GettingVariable)==int:
			if self.GettingVariable<len(self.AppendedVariablesList):

				#Get the GettedVariable from the AppendedVariablesList and the Index
				self.GettedVariable=self.AppendedVariablesList[self.GettingVariable]

				#Stop the getting
				self.IsGettingBool=False

		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,OrderShortString])==OrderShortString:

			#Get with the * 
			self.GettingVariable=self.GettingVariable.split(OrderShortString)[1]
			if self.GettingVariable in self.AppendedStringToAppendedIntDict:
				
				#Get the GettedVariable from the AppendedVariablesList and the KeyStringToAppendedIntDict
				self.GettedVariable=self.AppendedVariablesList[self.AppendedStringToAppendedIntDict[self.GettingVariable]]
				
				#Stop the getting
				self.IsGettingBool=False
		
		#Get with '/'
		elif SYS.getCommonPrefixStringWithStringsList([self.GettingVariable,DeepShortString])==DeepShortString:

			#Define the GettingVariableStringsList
			GettingVariableStringsList=DeepShortString.join(self.GettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
			GettedVariable=self[GettingVariableStringsList[0]]
			if GettedVariable!=None:

				if len(GettingVariableStringsList)==1:

					#Direct update in the Child or go deeper with the ChildPathString
					self.GettedVariable=GettedVariable

					#Stop the getting
					self.IsGettingBool=False
				
				else:

					#Define the ChildPathString
					ChildPathString=DeepShortString.join(GettingVariableStringsList[1:])	

					#Get in the first deeper Element with the ChildPathString
					self.GettedVariable=GettedVariable[ChildPathString]

					#Stop the getting
					self.IsGettingBool=False

		#Do the minimal getitem in the __dict__
		elif type(self.GettingVariable) in [str,unicode]:

			#Get safely the Value
			if self.GettingVariable in self.__dict__:

				#Get in the __dict__
				self.GettedVariable=self.__dict__[self.GettingVariable]

				#Stop the getting
				self.IsGettingBool=False

			elif self.GettingVariable=="__class__":

				#Return the __dict__ of the __class__
				self.GettedVariable=self.__class__.__dict__

				#Stop the getting
				self.IsGettingBool=False

		#Debug
		#debug("self.GettingVariable is : "+repr(self.GettingVariable))
		#debug("self.GettedVariable is : "+repr(self.GettedVariable))

	def setBefore(self):
		"""
			Hook in the __setitem__ of the Object 
			with different possible ShortStrings

			If _SettingVariable is of the form /<String> it gets the 
			GettedVariable=__dict__[<String>]
			and keeps on the setting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is of the form *<String> it gets the GettedVariable at 
			self.AppendedVariablesList[<String>]
			and keeps on the getting with <String> such that GettedVariable[<String>]=self.SettedVariable

			If _SettingVariable is <camelCase> then it is a hook query...
			so it calls with the form self(<camelCase>,SettedVariable)

			If _SettingVariable is of the form <String1>Pointer and _SettedVariable of the form /<String2>
			it gets the self[<String2>] and do self[<String1>Pointer]=self[<String2>]

			Else do a __setitem__ in the __dict__ 
		"""

		#Deep set
		if SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,DeepShortString])==DeepShortString:

			#Get the SettingVariableStringsList
			SettingVariableStringsList=DeepShortString.join(self.SettingVariable.split(DeepShortString)[1:]).split(DeepShortString)
			GettedVariable=self[SettingVariableStringsList[0]]
			if GettedVariable!=None:
				
				#Define the ChildPathString
				ChildPathString=DeepShortString.join(SettingVariableStringsList[1:])	

				#Direct update in the Child or go deeper with the ChildPathString
				if ChildPathString=="":
					if type(self.SettedVariable)==tuple:
						GettedVariable.__setitem__(*self.SettedVariable)
					if type(self.SettedVariable)==list:
						GettedVariable.update(*self.SettedVariable)
					elif hasattr(self.SettedVariable,"items"):
						GettedVariable.update(**self.SettedVariable)
				else:
					#Set in the first deeper Element with the ChildPathString
					GettedVariable[ChildPathString]=self.SettedVariable

				#Stop the setting at this level
				self.IsSettingBool=False

		#Set with the OrderShortString
		elif SYS.getCommonPrefixStringWithStringsList([self.SettingVariable,OrderShortString])==OrderShortString:
			
			#Remove the OrderShortString
			self.SettingVariable=self.SettingVariable.split(OrderShortString)[1]

			#Append
			self('append',(self.SettingVariable,self.SettedVariable))
				
			#Stop the setting at this level
			self.IsSettingBool=False

		#Call for a hook
		elif self.SettingVariable[0].lower()==self.SettingVariable[0]:

				#Adapt format of the ArgsList
				if len(self.SettedVariable)==1:
					self(self.SettingVariable,self.SettedVariable[0])
				else:
					self(self.SettingVariable,*self.SettedVariable)

				#Stop the setting
				self.IsSettingBool=False

		elif SYS.getCommonSuffixStringWithStringsList([self.SettingVariable,PointerShortString])==PointerShortString:
			
			#Special set for Pointer with a / geted Value
			if type(self.SettedVariable) in [str,unicode]:
				if SYS.getCommonPrefixStringWithStringsList([self.SettedVariable,DeepShortString])==DeepShortString:
					GettedVariable=self[self.SettedVariable]
					if GettedVariable!=None:

						#Link the both
						self[self.SettingVariable]=GettedVariable

						#Stop the setting at this level
						self.IsSettingBool=False

		
		elif type(self.SettingVariable) in [str,unicode]:

			#Do the minimal __setitem__ in the __dict__
			self.__dict__[self.SettingVariable]=self.SettedVariable

	def delBefore(self):
		"""
			Hook in the __delitem__ of the Object
		"""

		#Do the minimal getitem
		if type(self.DeletingVariable) in [str,unicode]:

			#Get Safely the Value
			if self.DeletingVariable in self.__dict__:
				del self.__dict__[self.DeletingVariable]


	def reprAfter(self):
		"""
			Hook in the repr of the Object
			for printing in the Console attributes in a more 
			user-friendly manner
		"""

		#Get the MroAppendedStringsList
		MroAppendedStringsList=SYS.getMroKeyStringsListWithClass(self.__class__)

		#Remove the NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.NotRepresentingAppendedStringsList,
										self.RepresentedTuplesList
								)

		#First keeps only the Specific and New attributes
		self.RepresentedTuplesList=map(
									lambda _RepresentedTuple:
									("<Spe>"+_RepresentedTuple[0],_RepresentedTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] in self.__class__.SpecificKeyStringsList,
											self.RepresentedTuplesList
										)
								)+map(
									lambda _NewTuple:
									("<New>"+_NewTuple[0],_NewTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] not in MroAppendedStringsList,
											self.RepresentedTuplesList
										)
								)

		#Add some forced Values with RepresentingKeyVariables
		self.RepresentedTuplesList+=map(
											lambda _KeyVariable:
											("<NotSpe>"+str(_KeyVariable),self[_KeyVariable]),
											self.RepresentingKeyVariablesList
										)

		#Simplify the numpy variables repr
		self.RepresentedTuplesList=map(
										lambda _RepresentedTuple:
										_RepresentedTuple
										if type(_RepresentedTuple[1]) not in [numpy.ndarray] 
										else (
												_RepresentedTuple[0],
												"<numpy.ndarray shape "+str(numpy.shape(
													_RepresentedTuple[1]))+">"
												),
										self.RepresentedTuplesList
									)

	def appendBefore(self):
		"""
			Hook in the append of the Object
		"""

		#Append in the AppendedStringsList and the AppendedVariablesList
		self.AppendedStringsList.append(self.AppendingKeyVariable)
		self.AppendedVariablesList.append(self.AppendingValueVariable)

		#Give the AppendedInt as an AppendedInt for the ValueVariable
		if hasattr(self.AppendedVariablesList[self.AppendedInt],'__setitem__'):
			
			#TuplesList Case
			if SYS.getIsTuplesListBool(self.AppendedVariablesList[self.AppendedInt]):
				self.AppendedVariablesList[self.AppendedInt].append(('AppendedInt',self.AppendedInt))
			else:
				
				#Dictable case
				self.AppendedVariablesList[self.AppendedInt]['AppendedInt']=self.AppendedInt

		#Update the KeyStringToAppendedIntDict
		self.AppendedStringToAppendedIntDict[self.AppendingKeyVariable]=len(self.AppendedStringsList)-1
		
	def itemsAfter(self):
		"""
			Deepest Hook in the items of the Object
		"""

		#Return the zip
		self.CalledPointer=zip(self.AppendedStringsList,self.AppendedVariablesList)

	def applyBefore(self):
		"""
			Hook in the pick of the Object
		"""
		
		#Keep the self.CallingList in the ApplyingList
		self.ApplyingVariablesList=copy.deepcopy(self.ApplyingVariablesList)

		self.ApplyingString
		self.AppliedMethod=

		#Apply for each with a map
		self.AppliedVariablesList=map(
				lambda _AppliedVariable:
				self(ApplyingList[0],_AppliedVariable),
				ApplyingList[1]
			)

	def addAfter(self):
		"""
			Hook in the __add__ of the Object
		"""
		self('apply','append',self.AddedVariable)

	def pickAfter(self):
		"""
			Hook in the pick of the Object
		"""

		#Return
		self.PickedTuplesList=map(
								lambda _PickingVariable:
								(
									str(_PickingVariable),
									self[_PickingVariable]
								),
								self.PickingKeyVariablesList
							)

	def copyAfter(self):
		"""
			Hook in the pick of the Object
		"""

		#Return
		self.CopiedTuplesList=map(
								lambda _CopyingVariable:
								(
									str(_CopyingVariable),
									copy.deepcopy(self[_CopyingVariable])
								),
								self.CopyingKeyVariablesList
							)

	def linkAfter(self):
		"""
			Hook in the pick of the Object
		"""

		#Return
		self.LinkedPointer.update(*map(self.pick()))

	def deliverAfter(self):
		"""
			Hook in the pick of the Object
		"""

		#Return
		self.LinkedPointer.update(*map(self.copy()))

	def useAfter(self):
		"""
			Hook in the pick of the Object
		"""

		#Bind with UsedPointer setting
		self['UsedPointer']=self.CallingList[0]

	#</DefineHookMethods>
#</DefineClass>



