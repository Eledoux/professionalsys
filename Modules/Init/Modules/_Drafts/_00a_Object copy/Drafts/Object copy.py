#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DeepString="/"
#</DefineLocals>

#<DefineClass>
class ObjectClass(object):
	
	#<DefineMethods>
	def __init__(self):

		#<DefineSpecificDict>
		map(
				lambda HookingString:
				setattr(self,"Is"+HookingString+"Bool",True),
				SYS.HookStringToHookingStringDict.values()
			)
		#</DefineSpecificDict>

		#Hook methods (integrativ)
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="init"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Ordered methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsInitiatingBool==False:
						return 

					#Call the HookingMethod
					HookingMethod(self)

		#Return None

	def __getitem__(self,_GettingVariable):
		'''
			<Help>
				Generic GetItem Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''
		
		#Init the OutputVariable
		OutputVariable=None

		#Refresh the IsDeletingBool
		self.IsGettingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="get"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Ordered methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsGettingBool==False:
						return GettedVariable

					#Call the HookMethod
					OutputVariable=HookingMethod(self,_GettingVariable)
					
					#Look for updating _GettingVariable,_GettedVariable
					if type(OutputVariable)==dict:
						if 'GettingVariable' in OutputVariable:
							_GettingVariable=OutputVariable['GettingVariable']

						if 'GettedVariable' in OutputVariable:
							if OutputVariable['GettedVariable']!=None:
								return OutputVariable['GettedVariable']	

			#Do the minimal getitem
			if type(_GettingVariable) in [str,unicode]:

				#Get Safely the Value
				if _GettingVariable in self.__dict__:
					return self.__dict__[_GettingVariable]
				elif _GettingVariable=="__class__":
					return self.__class__.__dict__

	def __setitem__(self,_SettingVariable,_SettedVariable):
		'''
			<Help>
				Generic Setitem Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#Init the OutputVariable
		OutputVariable=None

		#Reset the IsSettingBool
		self.IsSettingBool=True

		#Sort the HookMethodStrings
		for OrderString in ["Before","After"]:

			#Define a Local IsSettedBool
			IsSettedBool=False

			if type(_SettingVariable) in [str,unicode]:
			
				#Define the BindingMethodString
				BindingMethodString="bind"+_SettingVariable+OrderString
				
				#Check that there is HookingMethods for it
				if BindingMethodString in self.__class__.BindingMethodStringToMethodTuplesListDict:
					
					#Say ok
					IsSettedBool=True

					#Call the specific Ordered methods 
					for HookingMethod,BindingMethod in self.__class__.BindingMethodStringToMethodTuplesListDict[BindingMethodString]:

						

						#Check Bool
						if self.IsSettingBool==False:
							return self
						
						#Call the HookMethod
						if callable(HookingMethod):
							OutputVariable=HookingMethod(self,_SettingVariable,_SettedVariable)

							#Look for updating _SettingVariable,_SettedVariable
							if type(OutputVariable)==dict:
								if 'SettingVariable' in OutputVariable:
									if OutputVariable['SettingVariable']!=None:
										_SettingVariable=OutputVariable['SettingVariable']

								if 'SettedVariable' in OutputVariable:
									if OutputVariable['SettedVariable']!=None:
										_SettedVariable=OutputDict['SettedVariable']

						#Call the Binding
						if callable(BindingMethod):
							OutputVariable=BindingMethod(self)

							#End the Setting
							if self.IsSettingBool==False:
								return self

							#Look for updating _SettingVariable,_SettedVariable
							if type(OutputVariable)==dict:
								if 'SettingVariable' in OutputVariable:
									if OutputVariable['SettingVariable']!=None:
										_SettingVariable=OutputVariable['SettingVariable']

								if 'SettedVariable' in OutputDict:
									if OutputVariable['SettedVariable']!=None:
										_SettedVariable=OutputVariable['SettedVariable']

			#If a binding method was not yet found then look for a global set method
			if IsSettedBool==False:

				#_SettingVariable is not a string
				HookingMethodString="set"+OrderString

				#Check that there is HookingMethods for it
				if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

					#Call the specific Ordered methods 
					for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

						#Check Bool
						if self.IsSettingBool==False:
							return self

						#Call the HookingMethod
						OutputVariable=HookingMethod(self,_SettingVariable,_SettedVariable)

						#Look for updating _SettingVariable,_SettedVariable
						if type(OutputVariable)==dict:
							if 'SettingVariable' in OutputVariable:
								if OutputVariable['SettingVariable']!=None:
									_SettingVariable=OutputVariable['SettingVariable']

							if 'SettedVariable' in OutputVariable:
								if OutputVariable['SettedVariable']!=None:
									_SettedVariable=OutputVariable['SettedVariable']

			#Do the minimal setitem
			if OrderString=="Before":
				if self.IsSettingBool:
					if type(_SettingVariable) in [str,unicode]:
						if len(_SettingVariable)>0:
							if _SettingVariable[0]!="x":
								self.__dict__[_SettingVariable]=_SettedVariable

		#Return self
		return self

	def update(self,*_ArgsList,**_KwargsDict):
		'''
			<Help>
				Each Item of the Dict are setted with 
				the specific __setitem__ of the Object
			</Help>

			<Test>
				#Get a Dicter Instance that is directly updated
				HappyDicter=SYS.DicterClass().update({'HumorString':"Happy"});
				#Print the HappyDicter
				print("The Happy Dicter __dict__ is :");
				print(HappyDicter.__dict__);
			</Test>
		'''

		#Set with the ArgsList
		map(
				lambda Tuple:
				self.__setitem__(Tuple[0],Tuple[1]),
				_ArgsList
			)

		#Set with the KwargsDict
		map(
				lambda Tuple:
				self.__setitem__(Tuple[0],Tuple[1]),
				_KwargsDict.items()
			)
					
		#Return 
		return self

	def __delitem__(self,_DeletingVariable):
		'''
			<Help>
				Generic GetItem Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''
		
		#Init the OutputVariable
		OutputVariable={}

		#Refresh the IsDeletingBool
		self.IsDeletingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:

			#Define the HookMethodString
			HookingMethodString="delete"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Ordered methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsDeletingBool==False:
						return self

					#Call the HookMethod
					OutputVariable=HookingMethod(self,_DeletingVariable)
					
					#Look for updating _GettingVariable,_GettedVariable
					if type(OutputVariable)==dict:
						if 'DeletingVariable' in OutputVariable:
							_DeletingVariable=OutputVariable['DeletingVariable']

			#Do the minimal getitem
			if type(_DeletingVariable) in [str,unicode]:

				#Get Safely the Value
				if _DeletingVariable in self.__dict__:
					del self.__dict__[_DeletingVariable]

		#Return self
		return self

	def __call__(self,_CallString,*_ArgsList,**_KwargsDict):

		#Init the OutputVariable
		OutputVariable=None

		#Reset the IsCallingBool
		self.IsCallingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString=_CallString+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Ordered methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsCallingBool==False:
						return OutputVariable

					#Call depending on the structure of the _ArgsList
					if len(_ArgsList)==1:
						OutputVariable=HookingMethod(self,_ArgsList[0],**_KwargsDict)
					else:
						OutputVariable=HookingMethod(self,*_ArgsList,**_KwargsDict)

					#Modify the arguments
					if type(OutputVariable)==dict:
						if 'ArgsList' in OutputVariable:
							_ArgsList=OutputVariable['ArgsList']
						if 'KwargsDict' in OutputVariable:
							_KwargsDict=OutputVariable['KwargsDict']

		#Return the OutputVariable
		return OutputVariable

	#Define a generic add Method
	def __add__(self,_AddedVariable):
		'''
			<Help>
				Generic AddItem Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#Init the OutputDict
		OutputDict={}

		#Reset the IsCallingBool
		self.IsAddingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="add"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Ordered methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsCallingBool==False:
						return self

					#Call the HookMethod
					OutputDict=HookingMethod(self,_AddedVariable)

					#Modify the arguments
					if type(OutputDict)==dict:
						if 'AddedVariable' in OutputDict:
							_AddedVariable=OutputDict['AddedVariable']

		#Return self to use it directly in one command line
		return self

	def __repr__(self):
		'''
			<Help>
				Generic Repr Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#Init the OutputDict
		OutputDict={}
		PrintedDict=self.__dict__

		#Reset the IsCallingBool
		self.IsRepresentingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="repr"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Ordered methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Check Bool
					if self.IsRepresentingBool==False:
						return OutputDict

					#Call the HookingMethod
					OutputDict=HookingMethod(self,PrintedDict)

					#Modify the arguments
					if type(OutputDict)==dict:
						if 'PrintedDict' in OutputDict:
							PrintedDict=OutputDict['PrintedDict']
						if 'PrintingDict' in OutputDict:
							PrintingDict=OutputDict['PrintingDict']
		#return the Print
		return SYS.getPrintedPointerStringWithPrintedPointerAndPrintingDict(self,SYS.PrintingDict)+SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(PrintedDict,SYS.PrintingDict)

	#</DefineMethods>

	#<DefineHookMethods>
	def getBefore(self,_GettingVariable):
		
		if SYS.getCommonPrefixStringWithStringsList([_GettingVariable,DeepString])==DeepString:

			#Define the GettingVariableStringsList
			GettingVariableStringsList=DeepString.join(_GettingVariable.split(DeepString)[1:]).split(DeepString)
			GettedVariable=self[GettingVariableStringsList[0]]
			if GettedVariable!=None:

				if len(GettingVariableStringsList)==1:

					#Direct update in the Child or go deeper with the ChildPathString
					return GettedVariable
				
				else:

					#Define the ChildPathString
					ChildPathString=DeepString.join(GettingVariableStringsList[1:])	

					#Get in the first deeper Element with the ChildPathString
					return {'GettedVariable':GettedVariable[ChildPathString]}

	def setBefore(self,_SettingVariable,_SettedVariable):

		#Deep set
		if SYS.getCommonPrefixStringWithStringsList([_SettingVariable,DeepString])==DeepString:

			#Get the SettingVariableStringsList
			SettingVariableStringsList=DeepString.join(_SettingVariable.split(DeepString)[1:]).split(DeepString)
			GettedVariable=self[SettingVariableStringsList[0]]
			if GettedVariable!=None:
				
				#Define the ChildPathString
				ChildPathString=DeepString.join(SettingVariableStringsList[1:])	

				#Direct update in the Child or go deeper with the ChildPathString
				if ChildPathString=="":
					if type(_SettedVariable)==tuple:
						GettedVariable.__setitem__(*_SettedVariable)
					if type(_SettedVariable)==list:
						GettedVariable.update(*_SettedVariable)
					elif hasattr(_SettedVariable,"items"):
						GettedVariable.update(**_SettedVariable)
				else:
					#Set in the first deeper Element with the ChildPathString
					GettedVariable[ChildPathString]=_SettedVariable

				#Stop the Setting at this level
				self.IsSettingBool=False

		#Call for a hook
		elif _SettingVariable[0].lower()==_SettingVariable[0]:

				#Adapt format of the ArgsList
				if len(_SettedVariable)==1:
					self(_SettingVariable,_SettedVariable[0])
				else:
					self(_SettingVariable,*_SettedVariable)

				#Stop the Setting at this level
				self.IsSettingBool=False

		#Return self
		return self

	def reprAfter(self,_PrintedDict):
		
		#Define the NotRepresentedKeyStringsList
		NotRepresentedKeyStringsList=map(
											lambda HookingString:
											'Is'+HookingString+'Bool',
											SYS.HookStringToHookingStringDict.values()
										)

		#Remove the Object Keystrings
		_NewPrintedDict=dict(
								map(
										lambda FilteredItemTuple:
										FilteredItemTuple 
										if type(FilteredItemTuple[1]) not in [SYS.sys.modules['numpy'].ndarray] 
										else (
												FilteredItemTuple[0],
												"<numpy.ndarray shape "+str(SYS.sys.modules['numpy'].shape(
													FilteredItemTuple[1]))+">"
												),
										filter(
												lambda ItemTuple:
												ItemTuple[0] not in NotRepresentedKeyStringsList,
												_PrintedDict.items()
											)
									)
							)

		#Return
		return {'PrintedDict':_NewPrintedDict}

	def applyAfter(self,_CallString,_AppliedList):

		#Apply for each with a map
		_MappedList=map(
				lambda AppliedVariable:
				self(_CallString,AppliedVariable),
				_AppliedList
			)

		#Return self
		return self

	def pickAfter(self,_PickingVariablesList):

		#Return
		return dict(
					map(
						lambda PickingVariable:
						(str(PickingVariable),self[PickingVariable]),
						_PickingVariablesList
					)
				)

	#</DefineHookMethods>
#</DefineClass>



