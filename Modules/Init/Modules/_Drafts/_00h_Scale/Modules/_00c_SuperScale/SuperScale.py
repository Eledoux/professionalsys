#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SuperScalesListerClass(
								SYS.ConstantScalesListerClass,
								SYS.RootedScalesListerClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithSuperScalesLister(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the SpecificListedBaseTypeString
		self['SpecificInstancedListedBaseTypeString']="SuperScale"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SuperScalesLister"]=[
												]

	def setAfterBasesWithSuperScalesLister(self,_KeyVariable,_ValueVariable):

		if _KeyVariable==self.ListKeyString:

			#Freeze the setAfterBasesWithRootedScalesLister Method
			self.FrozenMethodStringsList.append('setAfterBasesWithSuperScalesLister')

			#Bind with setListedScalesWithRootedScalesLister
			self.setListedScalesWithSuperScalesLister()

			#Reput the Hook
			self.FrozenMethodStringsList.pop(self.FrozenMethodStringsList.index('setAfterBasesWithSuperScalesLister'))
	
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setListedScalesWithSuperScalesLister(self):

		#Reset the listed Scales by keeping only the ones with the good number of NotesInt
		self[self.ListKeyString]=filter(lambda Scale:
													Scale.IsSuperScaleBool==1,
													getattr(self,self.ListKeyString)				
										)
	#<DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>
#</DefineClass>

