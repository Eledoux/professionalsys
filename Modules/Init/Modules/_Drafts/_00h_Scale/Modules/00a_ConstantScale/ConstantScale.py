#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ConstantScaleClass(
							SYS.ScaleClass
						):

	#<DefineHookMethods>
	def initAfter(self):
		
		#<DefineSpecificDict>
		self.NotesInt=0
		#</DefineSpecificDict>

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindNotesIntAfter(self):

		#Bind with the listed list setting
		self.bindTabularingTuplesListAfter()

	def bindTabularingTuplesListAfter(self):

		#Set the ScanningList for PitchBoolsList
		IndexInt=self.TabularedDict['ModeledTable']['GettingStringsList'].index('PitchBoolsListsList')
		self.TabularingTuplesList[0][1][
					IndexInt][3]=SYS.getFilteredPitchBoolsListsListWithNotesInt(
											self.TabularingTuplesList[0][1][IndexInt][3],
											self.NotesInt
										)
	#<DefineBindingHookMethods>

#</DefineClass>

