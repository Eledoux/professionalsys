#<ImportModules>
import ShareYourSystem as SYS
import os
#</ImportModules>

#<DefineTestClass>
class ConstantScaleTesterClass(SYS.TesterClass):
	
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	def test_01_output(self):
		self.TestedVariable=self.TestedClass().__setitem__('PitchesInt',4)

	def test_02_organize(self):

		self.TestedVariable=self.TestedClass().update(
									[
										('GroupingFilePathString',SYS.getCurrentFolderPathString()+'MyScale.hdf5'),
										('PitchesInt',7),
										('NotesInt',3)
									]
								).organize(
									[
												('parentize','Grouped'),
												('setGroupedFilePointer',[]),
												('group',[]),
												('table',[]),
												('model',[]),
												('grind',[])
									])

	def test_03_organizeHdf(self):

		Object=self.TestedClass().update(
									[
										('GroupingFilePathString',SYS.getCurrentFolderPathString()+'MyScale.hdf5'),
										('PitchesInt',7),
										('NotesInt',3)
									]
								).organize([
												('parentize','Grouped'),
												('setGroupedFilePointer',[]),
												('group',[]),
												('table',[]),
												('model',[]),
												('grind',[])
									])

		self.TestedVariable=os.popen('h5ls -rd '+Object.GroupingFilePathString).read()

#</DefineTestClass>

#<Test>

#Get the local test class and the tested method strings
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)

#Write the solutions to the tests
TestedObject=TestedClass()
TestingFolderPathString=TestedObject.TestingFolderPathString
if os.path.isdir(TestingFolderPathString)==True:
	os.popen("cd "+TestingFolderPathString+";rm *")
TestedObject.apply('testify',MethodStringsList)

#Test
TestedClass().update([
						('IsPrintedBool',True),
						('TestingClass',SYS.TestClass)
					]
					).apply('bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()

#</Test>