#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class RootedScalesListerClass(
							SYS.ScalesListerClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithRootedScalesLister(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["RootedScalesLister"]=[
												]

	def setAfterBasesWithRootedScalesLister(self,_KeyVariable,_ValueVariable):

		if _KeyVariable==self.ListKeyString:

			#Freeze the setAfterBasesWithRootedScalesLister Method
			self.FrozenMethodStringsList.append('setAfterBasesWithRootedScalesLister')

			#Bind with setListedScalesWithRootedScalesLister
			self.setListedScalesWithRootedScalesLister()

			#Reput the Hook
			self.FrozenMethodStringsList.pop(self.FrozenMethodStringsList.index('setAfterBasesWithRootedScalesLister'))
	
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>

	#<DefineMethods>
	def setListedScalesWithRootedScalesLister(self):

		#Reset the listed Scales by keeping only the ones with the good number of NotesInt
		self[self.ListKeyString]=filter(lambda Scale:
													Scale.PitchBoolsList[0]==1,
													getattr(self,self.ListKeyString)				
										)
	#</DefineMethods>
#</DefineClass>

