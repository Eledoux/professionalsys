#<ImportModules>
import ShareYourSystem as SYS
from tables import *
#</ImportModules>

#<DefineLocals>
HookString="Scale"
#</DefineLocals>

#<DefineClass>
class ScaleClass(
					SYS.ConfigClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.NotesInt=3
		self.PitchesInt=7
		self.PitchBoolsListsList=[]
		#</DefineSpecificDict>

		#Bind with TabularingTuplesList setting
		#Set the TabularingTuplesList
		self.apply('tabular',
				[
					{
						'TabularingTypeString':HookString+'ModeledTable',
						'FeaturingTuplesList':
									[
										#ColumnString 	#GettingVariable 	#Col 			#VariablesList
										('PitchesInt',	'PitchesInt',		Int64Col(),		[12]),
										('NotesInt',	'NotesInt',			Int64Col(),		[3,4,5,6,7]),	
									]
					},
					{	
						'TabularingTypeString':HookString+'UsedTable',
						'FeaturingTuplesList':self['Tab_UseModeledTable']['FeaturedTuplesList'],
						'OutputingTuplesList':
									[
										#ColumnString 	#GettingVariable 	#Col 
										(
											'PitchBoolsListsListArrayString',
											'PitchBoolsListsList',	
											StringCol(100)
										)
									]
					}
				]
			)
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def outputAfter(self,_LocalCallVariable,_LocalCallingVariablesList):

		self.PitchBoolsListsList=SYS.getFilteredPitchBoolsListsListWithPitchesIntAndNotesInt(
													self.PitchesInt,
													self.NotesInt
											)
		
	#<DefineBindingHookMethods>

#</DefineClass>

#<DefineModelClass>

#</DefineModelClass>
