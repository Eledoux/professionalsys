#<ImportModules>
import copy
import operator
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Noder"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineFunctions>
def getHeritingClassWithMethod(_Method):

	#Check for the classes possessing this method
	IsMethodBoolsList=map(lambda __MroClass:hasattr(__MroClass,_Method.__name__),_Method.im_class.__mro__)

	#Return in order to get the one the deepest
	IsMethodBoolsList.reverse()
	ReversedMroClassesList=list(copy.copy(_Method.im_class.__mro__))
	ReversedMroClassesList.reverse()

	#Return the deepest class
	try:
		return ReversedMroClassesList[IsMethodBoolsList.index(True)]
	except:
	 	return None

def getWithMethodAndArgsList(_Method,_ArgsList,**_KwargsDict):
	"""Call a method by paying attention to the shape of the args"""

	#Call the Method given a good shape of the args
	Class=getHeritingClassWithMethod(_Method)
	if Class!=None:

		#Get the Dict
		Dict=getHeritingClassWithMethod(_Method).ArgsDict[_Method.__name__]

		#Shape the args
		if len(Dict['ArgsList'])>2:
			if Dict['VarKwargsDict']!=None:
				return _Method(*_ArgsList,**_KwargsDict)
			else:
				return _Method(*_ArgsList)
		elif len(Dict['ArgsList'])==2:
			if Dict['VarKwargsDict']!=None:
				return _Method(_ArgsList,**_KwargsDict)
			else:
				return _Method(_ArgsList)
		else:
			if Dict['VarKwargsDict']!=None:
				return _Method(**_KwargsDict)
			else:
				return _Method()
#</DefineFunctions>

#<DefineClass>
class ApplyierClass(BasedLocalClass):
	
	#<DefineHookMethods>
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.AppliedMappedVariablesList=[] 	#<NotRepresented>
		#</DefineSpecificDict>

		#Update the DebuggingNotFramedFunctionStringsList
		self.DebuggingNotFramedFunctionStringsList.append("getWithMethodAndArgsList")

	def apply(self,_MethodString,_AppliedVariablesList):
		"""Map a call with the _MethodString to the _AppliedVariablesList"""

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Debug
		'''
		self.debug(
					'_AppliedVariablesList is '+SYS.represent(_AppliedVariablesList)
			)
		'''
		
		#Get the AppliedMethod
		if hasattr(self,_MethodString):
			self.AppliedMappedVariablesList=map(
					lambda __AppliedVariable:
					getWithMethodAndArgsList(getattr(self,_MethodString),__AppliedVariable)
					if hasattr(self,_MethodString)
					else None,
					_AppliedVariablesList
				)

		#Return self
		return self

	@SYS.HookerClass(**{'BeforeTuplesList':[(BasedLocalClass,"set")]})
	def set(self,**_VariablesDict):

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Call for a hook
		if (self.SettingKeyVariable[0].isalpha() or self.SettingKeyVariable[:2]=="__"
			) and self.SettingKeyVariable[0].lower()==self.SettingKeyVariable[0]:

			#Debug
			self.debug(
						[
							'This is a set that calls a method',
							'self.SettingKeyVariable is '+self.SettingKeyVariable
						]
					)

			#Get the Method
			if hasattr(self,self.SettingKeyVariable):

				#Get the method
				SettingMethod=getattr(self,self.SettingKeyVariable)

				#Debug
				'''
				self.debug('SettingMethod is '+str(SettingMethod))
				'''

				#Adapt the shape of the args
				getWithMethodAndArgsList(
											SettingMethod,
											self.SettingValueVariable['ArgsVariable'] if 'ArgsVariable' in self.SettingValueVariable else [],
											**self.SettingValueVariable['KwargsDict'] if 'KwargsDict' in self.SettingValueVariable else {}
										)
			#Return
			return {'IsCallingBool':False}

	def pick(self,_GettingVariablesList):
		"""Apply the __getitem__ to the <_GettingVariablesList>"""

		#Apply __getitem__
		self.apply('__getitem__',_GettingVariablesList)

		#Return AppliedVariablesList
		return self.AppliedMappedVariablesList

	def gather(self,_GatheringVariablesList):
		"""Reduce a map of pick for GatheringVariable being a List or __getitem__ for GatheringVariable being a GettingVariable"""
		
		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Debug
		'''
		self.debug("_GatheringVariablesList is "+str(_GatheringVariablesList))
		'''

		GatheredList=reduce(
						operator.__add__,
						map(
								lambda __GatheringVariable:
								zip(__GatheringVariable,self.pick(__GatheringVariable))
								if type(__GatheringVariable)==list
								else self[__GatheringVariable],
								_GatheringVariablesList
						)
					) if len(_GatheringVariablesList)>0 else []

		#Debug
		'''
		self.debug('End of the method')
		'''

		#Return the reduce
		return GatheredList

	def collect(self,_CollectingGatheringVariablesList,_CollectingGatheredVariablesList):
		"""Reduce a map of pick for GatheringVariable being a List or __getitem__ for GatheringVariable being a GettingVariable"""


		#Get the GatheredVariablesList
		GatheredVariablesList=zip(*self.gather(_CollectingGatheringVariablesList))
		if len(GatheredVariablesList)>0:

			#Just keep the values
			GatheredVariablesList=GatheredVariablesList[1]

			#Reduce a gather for each
			return reduce(
							operator.__add__,
							map(
									lambda __CollectingApplyier:
									__CollectingApplyier.gather(_CollectingGatheredVariablesList)
									if hasattr(__CollectingApplyier,'gather') else None,
									GatheredVariablesList
							)
						)
			
	def update(self,_SettingVariableTuplesList):
		"""Apply the __setitem__ to the <_SettingVariableTuplesList>"""

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Debug
		'''
		self.debug("_SettingVariableTuplesList is "+SYS.represent(_SettingVariableTuplesList))
		'''

		#Apply __setitem__
		return self.apply('__setitem__',_SettingVariableTuplesList)

#</DefineClass>

#<DefineAttestingFunctions>
def attest_apply():

	#Apply a __setitem__ to several things 
	Applyier=SYS.ApplyierClass().apply('__setitem__',[
													('MyString',"Hello"),
													('MySecondString',"Bonjour")
												]
									)

	#Apply an apply is possible 
	return Applyier.apply(
							'__setitem__',
							[
											('MyThirdString',"GutenTag"),
											('apply',{'ArgsVariable':(
															'__setitem__',[
																		('MyInt',0)
																		]
																)
													}
											),
											('MyNotLostString',"ben he"),
							]
				)

def attest_pick():

	#Define an Applyier
	Applyier=SYS.ApplyierClass().update([
										('MyInt',0),
										('MyApplyier',SYS.ApplyierClass().update(
											[
												('MyString',"hello"),
												('MyFloat',0.1)
											]
											)
										),
										('<Group>MyAppendedApplyier',SYS.ApplyierClass().update(
											[
												('MyOtherString',"bonjour"),
												('MyOtherFloat',0.2)
											]
											)
										),
										('<Sort>MySecondInt',3)
									])
	#Map some gets
	TestedVariable=Applyier.pick(
								[
									#Get directly in the __dict__
									'MyInt',
									'MyApplyier',

									#Get with copying
									'Cop_MyApplyier',

									#Get with a DeepShortString
									'/MyApplyier/MyString',

									#Get with a AppendShortString
									'<Sort>MySecondInt',
									'<Group>MyAppendedApplyier',
									'Cop_<Group>MyAppendedApplyier',
									'/<Group>MyAppendedApplyier/MyOtherString',
									#Or the AppendedInt
									'App<Group>0',

									#Get in the class object
									'/__class__/TypeString'
								]
							)

	#Change in the objects for looking at the copy difference
	Applyier['/MyApplyier/MyFloat']=0.4444
	Applyier['/<Group>MyAppendedApplyier/MyOtherFloat']=0.5555

	#Return
	return TestedVariable

def attest_update():

	#Update several things
	Applyier=SYS.ApplyierClass().update([
										('MyInt',0),
										('<Sort>MySecondInt',0),
										('MyFloat',0.2)
									]
								)

	#Update in a update is possible
	return Applyier.update([
										('MyString',"Hello"),
										('update',{'ArgsVariable':[
													('MySecondInt',2)
													]}
										),
										('MyNotLostString',"ben heee")
									]
								)
#</DefineAttestingFunctions>
