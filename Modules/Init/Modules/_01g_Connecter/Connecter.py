#<ImportModules>
import ShareYourSystem as SYS
import itertools
import numpy as np
import scipy.stats
from tables import *
import time
import operator
import os
#</ImportModules>

#<DefineLocals>
HookString="Mul"
#</DefineLocals>

#<DefineClass>
class ConnecterClass(SYS.GriderClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.GridedTypeString="Connection"
		self.GridingTuplesList=[]
		#</DefineSpecificDict>

						
	def organizeBefore(self):
		
		self.GridingTuplesList=[
									#('PreInt','PreInt',range(len(self.StructuredOrderedDict))),
									#('PostInt','PostInt',range(len(self.StructuredOrderedDict)))
									(
										'ConnectedIntsTuple',
										'ConnectedIntsTuple',
										itertools.product(*[range(len(self.StructuredOrderedDict)),range(len(self.StructuredOrderedDict))])
									)
		]

		#Return self
		return self

	#</DefineHookMethods>
	
#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.GriderClass()

def attest_output():

	#Append a default output
	SYS.Config.seed(5)

	#Return the object __repr__
	TestedVariable=SYS.GriderClass().output()
	print(TestedVariable)
	return TestedVariable

def attest_store():

	#Flush the default output
	SYS.Config.seed(5)
	Grider=SYS.GriderClass().organize().output().store().output().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Grider.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

def attest_scan():

	#Flush with all the scanning featuring values
	Grider=SYS.GriderClass().organize().scan().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Grider.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

#</DefineAttestingFunctions>
