#<ImportModules>
import collections
import matplotlib
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class PatcherClass(
						SYS.OutputerClass
				):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.PatchedFigure=None 	#<NotRepresented>
		#</DefineSpecificDict>


	def patchBefore(self,**_PatchingVariablesDict):

		#Kill other python processes
		try :
			map(
					lambda IdString:
					os.popen("kill "+IdString),
					SYS.getOpenedProcessIdStringsListWithProcessNameString("python")[:-1]
				)
		except:
			pass


		#Init a Figure
		self.PatchedFigure=matplotlib.pyplot.figure(figsize=(17,5))


	#</DefineHookMethods>

	#<DefineMethods>
	def patch(self,**_PatchingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Debug
		'''
		print('Outputer output method')
		print('')
		'''

		#Refresh the attributes and set maybe default values in the _PatchingVariablesDict
		LocalPatchingVariablesDict=_PatchingVariablesDict
		LocalOutputedPointer=self
		self.IsPatchingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='patch'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalPatchingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalPatchingVariablesDict' in OutputVariable:
							LocalPatchingVariablesDict=OutputVariable['LocalPatchingVariablesDict']
						if 'LocalOutputedPointer' in OutputVariable:
							LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

					#Check Bool
					if self.IsPatchingBool==False:
						return LocalOutputedPointer

		#Return the OutputVariable
		return LocalOutputedPointer
	#</DefineMethods>

#<DefineAttestingFunctions>
def attest_patch():

	#Build Hdf groups
	Patcher=SYS.PatcherClass().patch()

	#Show
	Patcher.PatchedFigure.show()

	#Return the object itself
	return None
#</DefineAttestingFunctions>
