#<ImportModules>
import collections
import copy
import os
import ShareYourSystem as SYS
import six
import sys
#</ImportModules>

#<DefineLocals>
NodingStartString='<'
NodingEndString='>'
BasingLocalTypeString="Commander"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class ParenterClass(BasedLocalClass):

	def parent(self,_ParentingString):

		#Debug
		self.debug('Start of the method')

		#Nodify
		self.node(_ParentingString)

		#Check of a parent pointer
		if self.NodedParentPointer!=None:

			#Nodify the parent
			self.NodedParentPointer.node(_ParentingString)

			#Set the GrandParentPointersList
			GrandParentPointersListKeyString=self.NodedNodedString+"GrandParentPointersList"
			setattr(
						self,
						GrandParentPointersListKeyString,
						[self.NodedParentPointer]+self.NodedParentPointer.NodedGrandParentPointersList
					)
			self.NodedGrandParentPointersList=getattr(self,GrandParentPointersListKeyString)

			#Set the GrandParentKeyStringsList
			KeyStringsListKeyString=self.NodedNodedString+"GrandParentKeyStringsList"
			self.__setattr__(
								KeyStringsListKeyString,
								map(
										lambda __GrandParentPointer:
										getattr(__GrandParentPointer,self.NodedKeyStringKeyString),
										self.NodedGrandParentPointersList
									)
						)

			#Set the PathStringsList
			PathStringsListKeyString=self.NodedNodedString+"PathStringsList"
			PathStringsList=[getattr(self,self.NodedKeyStringKeyString)]+copy.copy(
									getattr(self,KeyStringsListKeyString)
								)
			self.__setattr__(
								PathStringsListKeyString,
								PathStringsList
							)
			getattr(self,PathStringsListKeyString).reverse()

			#Set the PathString
			PathStringKeyString=self.NodedNodedString+"PathString"
			PathString=SYS.Pather.PathingString.join(getattr(self,PathStringsListKeyString))
			self.__setattr__(
								PathStringKeyString,
								PathString
							)
			self.NodedPathString=PathString

		#Return self
		return self

#</DefineClass>

#<DefineAttestingFunctions>
def attest_parent():

	#Short expression and set in the appended manner
	Parenter=SYS.ParenterClass().__setitem__(
		'<Group>MyChildNode',
		SYS.NoderClass().__setitem__(
			'<Group>MyGrandChildNode',
			SYS.NoderClass()
		)
	).parent('Group')

	#Return Parenter
	return Parenter

#</DefineAttestingFunctions>

