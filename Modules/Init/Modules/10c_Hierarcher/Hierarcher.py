#<ImportModules>
import collections
import copy
import numpy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class HierarcherClass(SYS.JoinerClass):
	
	#<DefineHookMethods>
	def modelAfter(self,**_ModelingVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Alias
		JoinedModeledDict=self.ModeledDict['JoinedModeledDict']

		#Alias
		HierarchingNodeString=self.ModeledDict['HierarchingNodeString']

		#Nodify with the HierarchingNodeString
		if HierarchingNodeString!="":

			#Debug
			self.debug('In the ModeledClass we add the joined columns')

			#Alias
			HierarchedNodifiedNodedString=self.ModeledDict['HierarchedNodifiedNodedString']
			JoinedModeledString=JoinedModeledDict['ModelString']
			JoinedRetrievingTupleKeyString=self.ModeledDict['JoinedRetrievingTupleKeyString']

			#Set the JoinedNodifiedRetrievingTupleKeyStringsList
			JoinedNodifiedRetrievingTupleKeyStringsList=map(
				lambda __NodifiedKeyStringKeyString:
				HierarchedNodifiedNodedString+__NodifiedKeyStringKeyString+JoinedRetrievingTupleKeyString,
				self.ModeledDict['HierarchedNodifiedOrderedDict'].keys()
			)

			#Alias
			ModeledClass=self.ModeledDict['ModeledClass']

			#Debug
			self.debug('JoinedNodifiedRetrievingTupleKeyStringsList is '+str(
				JoinedNodifiedRetrievingTupleKeyStringsList))

			#Set the Cols for the joined children 
			map(
				lambda __JoinedNodifiedRetrievingTupleKeyString:
				ModeledClass.columns.__setitem__(
					__JoinedNodifiedRetrievingTupleKeyString,
					tables.Int64Col(shape=2)
				),
				JoinedNodifiedRetrievingTupleKeyStringsList
			)

			#Debug
			self.debug('Ok the joined columns were added')

		#Debug
		self.debug('End of the method')

	def rowAfter(self,**_RowingVariablesList):

		#Debug
		self.debug('Start of the method')

		#Alias
		HierarchedNodifiedOrderedDict=self.ModeledDict['HierarchedNodifiedOrderedDict']

		#Check
		if len(HierarchedNodifiedOrderedDict)>0:

			#Debug
			self.debug(
						[
							'We are going to make row all the joined noded children'
						]
					)


			#Alias
			ModelString=self.ModeledDict['ModelString']

			#Row each noded children
			map(
					lambda __Variable:
					__Variable.row(ModelString),
					HierarchedNodifiedOrderedDict.values()
				)

			#Debug
			self.debug(
						[
							'The noded children have rowed'
						]
					)

			#Alias
			HierarchedNodifiedNodedString=self.ModeledDict['HierarchedNodifiedNodedString']
			
			#Debug
			self.debug(
						'We give the JoinedRetrievingTuple to the Parent',
						"self.ModeledDict['JoinedRetrievingTuple'] is "+str(self.ModeledDict['JoinedRetrievingTuple']),
						'HierarchedNodifiedNodedString is '+HierarchedNodifiedNodedString
				)

			#Give to the parent
			ParentPointer=getattr(
									self,
									HierarchedNodifiedNodedString+'ParentPointer'
						)
			if ParentPointer!=None:
				ParentPointer['App_Model_'+self.ModeledDict['ModelingString']+'Dict']['JoinedOrderedDict'][
						getattr(
								self,
								HierarchedNodifiedNodedString+'KeyString'
								)+self.ModeledDict['JoinedRetrievingTupleKeyString']
						]=self.ModeledDict['JoinedRetrievingTuple']

			#Update the self.RowedIdentifiedOrderedDic
			self.ModeledDict['RowedIdentifiedOrderedDict'].update(JoinedOrderedDict)

		#Debug
		self.debug('End of the method')

	def recoverBefore(self,**_LocalRecoveringVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Alias
		HierarchedNodifiedOrderedDict=self.ModeledDict['HierarchedNodifiedOrderedDict']

		#Debug
		self.debug(
					[
						'But first look if we have to recover the children first',
						"len(HierarchedNodifiedOrderedDict) is "+str(
							len(HierarchedNodifiedOrderedDict))
					]
				)

		#Maybe we have to recover the children before
		if len(HierarchedNodifiedOrderedDict)>0:

			self.debug(
						[
							'Yes, we are going to make recover each children before'
						]
					)

			#Alias
			ModelString=self.ModeledDict['ModelString']
			ModelingString=self.ModeledDict['ModelingString']
			HierarchedNodifiedNodedString=self.ModeledDict['HierarchedNodifiedNodedString']

			#Map a Recover
			map(
					lambda __JoinedJoiner:
					__JoinedJoiner.__setitem__(
							'/App_Model_'+ModelingString+'Dict/FindingTuplesList',
							[
								(
									JoinString+JoinedRetrievingTupleKeyString,
									(
										SYS.getIsEqualBool,
										JoinedRecoveredDict[
										HierarchedNodifiedNodedString+getattr(
												__JoinedJoiner,
												HierarchedNodifiedNodedString
												)+JoinedRetrievingTupleKeyString
										]
									)
								)
							]
					).recover(ModelString),
					HierarchedNodifiedOrderedDict.values()
				)

			#Debug
			self.debug('Ok the children are recovered')

		#Debug
		self.debug('End of the method')

	def joinBefore(	
					self,
					_ModelString="",
					**_JoiningVariablesDict
				):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug('We are going to hierarchy')

		#Hierarchy
		self.hierarchy()

		#Debug
		self.debug('hierarchy is done')

		#Debug
		self.debug('End of the method')
	#</DefineHookMethods>

	#<DefineMethods>
	def hierarchy(self,*_HierarchingVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Alias
		if 'HierarchingNodeString' not in self.ModeledDict:
			self.ModeledDict['HierarchingNodeString']=""
		HierarchingNodeString=self.ModeledDict['HierarchingNodeString']

		#Debug
		self.debug(
					[
						'HierarchingNodeString is '+HierarchingNodeString,
						'Look if we have to structure before and join also'
					]
				)

		#Check
		if HierarchingNodeString!="":

			#Maybe we have to structure
			if self.IsGroupedBool==False:

				#Debug
				self.debug(
							[
								'We have to structure first',
								'self.StructuredKeyString is '+str(self.StructuredKeyString
									) if hasattr(self,"StructuredKeyString") else ''
							]
					)
				
				#Structure
				self.structure(HierarchingNodeString)

				#Get the node properties
				HierarchedNodifiedOrderedDict=copy.copy(self.NodifiedOrderedDict)
				HierarchedNodifiedNodedString=self.NodifiedNodedString
				HierarchedNodifiedNodingString=self.NodifiedNodingString

				#Put them in the ModeledDict
				LocalVars=vars()
				map(
						lambda __GettingString:
						self.ModeledDict.__setitem__(__GettingString,LocalVars[__GettingString]),
						[
							'HierarchedNodifiedOrderedDict',
							'HierarchedNodifiedNodedString',
							'HierarchedNodifiedNodingString'
						]
					)

				#Debug
				self.debug("Ok structure is done")
			

		#Check
		if 'HierarchedNodifiedOrderedDict' not in self.ModeledDict:
			self.ModeledDict['HierarchedNodifiedOrderedDict']={}
			self.ModeledDict['HierarchedNodifiedNodedString']=""

		#Debug
		self.debug('End of the method')

		#Return self
		return self
	#<DefineMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_join():

	#Build Hdf groups
	Joiner=SYS.JoinerClass().hdformat().update(
								[
									(
										'App_Structure_ChildJoiner1',
										SYS.JoinerClass().update(
										[
											('App_Model_ParameterizingDict',
														{
															'ColumningTuplesList':
															[
																('MyIntsList',tables.Int64Col(shape=2))
															]
														}
											),
											('MyIntsList',[2,4]),
											('App_Model_ResultingDict',
														{
															'ColumningTuplesList':
															[
																('MyInt',tables.Int64Col())
															]
														}
											),
											('MyInt',1)
										])
									),
									('App_Model_ParameterizingDict',
														{
															'ColumningTuplesList':
															[
																('MyString',tables.StringCol(10)),
															]
														}
									),
									('MyString',"hello")
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':""}),
													('model',{'ArgsVariable':"Parameter"}),
													('table',{'ArgsVariable':""}),
													('row',{'ArgsVariable':""}),
													('flush',{'ArgsVariable':""})
												]
											}
									).update(
										[
											
											('App_Model_ResultingDict',
												{
													'ColumningTuplesList':
													[
														('MyFloat',tables.Float32Col()),
														('MyFloatsList',tables.Float32Col(shape=3))
													],
													'JoiningTuple':("Structure","Parameter")
												}
											),
											('MyFloat',0.1),
											('MyFloatsList',[2.3,4.5,1.1])
										]
									).model("Result"		
									).table(
									).row(
									).flush("Result"
									).close()

	#Return the object itself
	return "Object is : \n"+SYS.represent(
		Joiner
		)+'\n\n\n'+SYS.represent(os.popen('/usr/local/bin/h5ls -dlr '+Joiner.HdformatingPathString
		).read())
#</DefineAttestingFunctions>
