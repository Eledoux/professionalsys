#<ImportModules>
import collections
import copy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
ShapingJoiningString='__'
ShapingTuplingString='_'
BasingLocalTypeString="Recoverer"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class ShaperClass(BasedLocalClass):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.ShapedString=""									#<NotRepresented>
		self.ShapedVariablesList=[] 							#<NotRepresented>
		self.ShapedModelingColumnTuplesList=[] 					#<NotRepresented>
		self.ShapedNotModelingColumnTuplesList=[]  				#<NotRepresented>
		self.ShapedGettingStringsList=[] 						#<NotRepresented>
		self.ShapedColClassAndGettingStringTuplesList=[] 		#<NotRepresented>				
		#<DefineSpecificDict>

	@SYS.HookerClass(**{'BeforeTuplesList':[("Modeler","model")],'AfterTuplesList':[("<TypeString>","shape")]})
	def model(self,**VariablesDict):

		#Get the new ModeledKeyString
		if self.ShapedString!="":
			self.ModeledKeyString=self.ShapedString+ShapingJoiningString+self.ModeledSuffixString

		#Debug
		self.debug(
					[
						'We set the new ModeledKeyString',
						('self.',self,['ShapedString','ModeledKeyString'])
					]
				)

		#Debug
		self.debug(
					[
						'We have to check if this model was already defined',
						'self.ModeledKeyString is '+str(self.ModeledKeyString),
						"self.ModeledClassesOrderedDict.keys() is "+str(
							self.ModeledClassesOrderedDict.keys())
					]
				)

		#Check
		if self.ModeledKeyString in self.ModeledClassesOrderedDict:

			#Debug
			self.debug('Yes it is already modeled')

			#Set IsModeledBool to True
			self.IsModeledBool=True

		else:

			#Debug
			self.debug('Nope, IsModeledBool has to be False')

			#Set IsModeledBool and ModeledKeyString
			self.IsModeledBool=False
			self.ModeledKeyString=self.ShapedString+ShapingJoiningString+self.ModeledSuffixString

			#Debug
			self.debug(
						[
							'Now we set in the good format the shaping columning tuples',
							('self.',self,
									[
										'ShapedGettingStringsList',
										'ShapedColClassAndGettingStringTuplesList'
									]
							)
						]
					)

			#Set also the ShapingColumningTuplesList inside of the ColumningTuplesList
			self.ModelingColumnTuplesList=self.ShapedNotModelingColumnTuplesList+map(
					lambda __ShapedGettingString,__ShapedColClassAndGettingStringTuple:
					(
						__ShapedGettingString,
						__ShapedColClassAndGettingStringTuple[0](
								shape=self[__ShapedColClassAndGettingStringTuple[1]]
							)
					),
					self.ShapedGettingStringsList,
					self.ShapedColClassAndGettingStringTuplesList
				)

			#Debug
			self.debug("Now self.ModelingColumnTuplesList is "+str(
				self.ModelingColumnTuplesList))

		#Debug
		self.debug('End of the method')

	@SYS.HookerClass(**{'BeforeTuplesList':[("Tabularer","tabular")]})
	def tabular(self,**_VariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						'We add the ShapedString to the TabularedSuffix String ?',
						('self.',self,[
											'TabularedSuffixString',
											'ShapedString'
										])
					]
				)

		#Add the ShapedString
		if self.ShapedString!="":

			#Debug
			self.debug('Yes we add')

			#Add
			self.TabularedSuffixString=self.ShapedString+ShapingJoiningString+self.TabularedSuffixString

			#Debug
			self.debug("self.TabularedSuffixString is "+self.TabularedSuffixString)

		#Debug
		self.debug('End of the method')

	@SYS.HookerClass(**{'AfterTuplesList':[("Flusher","flush")]})
	def flush(self,**_VariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						'We are going to check if it was Flushed',
						('self.',self,['FlushedErrorBool'])
					]
			)

		if self.FlushedErrorBool:

			#Debug
			self.debug('So yes we have to retable again...')

			#Set again the ColumnTuplesList
			self.ModelingColumnTuplesList=copy.copy(self.ModelingCopiedColumnTuplesList)

			#Reset some bools
			map(
				lambda __KeyString:
				self.__setitem__(
									__KeyString,
									False
								),
				['ModeledIsBool','TabularedIsBool','TabledIsBool']
			)

			#Table
			self.table()

			#Debug
			self.debug('Ok table again is done, so now we flush')

			#Flush again
			SYS.FlusherClass.flush.im_func.HookerPointer.HookingFunctionPointer(self)

		#Debug
		self.debug('End of the method')
		
	@SYS.SwitcherClass()
	@SYS.HookerClass(**{'BeforeTuplesList':[("<TypeString>","table")]})
	def shape(self,**_VariablesDict):

		#Debug
		self.debug('Start of the method')

		#Set the ShapedOldModelingColumnTuplesList
		self.ShapedOldModelingColumnTuplesList=copy.copy(self.ModelingColumnTuplesList)

		#Debug
		self.debug('We are going to filter the shaping tuples among the columning tuples')

		#Unpack
		[
			self.ShapedModelingColumnTuplesList,
			self.ShapedNotModelingColumnTuplesList
		]=SYS.groupby(
					lambda __ColumnTuple:
					type(__ColumnTuple[1])==tuple,
					self.ModelingColumnTuplesList,
				)

		#Set the ShapedGettingStringsList and ShapedColClassAndGettingStringTuplesList
		FilteredList=map(list,
				SYS.unzip(self.ShapedModelingColumnTuplesList,[0,1])
			)
		if len(FilteredList)>0:
			[
				self.ShapedGettingStringsList,
				self.ShapedColClassAndGettingStringTuplesList
			]=FilteredList

		#Debug
		self.debug(
					('self.',self,[
									'ShapedNotModelingColumnTuplesList',
									'ShapedGettingStringsList',
									'ShapedColClassAndGettingStringTuplesList'
								]
					)
			)

		#Set the ShapingGettingStringsList
		ShapingGettingStringsList=list(set(SYS.unzip(
			self.ShapedColClassAndGettingStringTuplesList,[1]
			)))

		#Debug
		self.debug("ShapingGettingStringsList is "+str(
			ShapingGettingStringsList))

		#Bind with ModeledShapingVariablesList setting
		self.ShapedVariablesList=map(
				lambda  __GettedVariable:
				(__GettedVariable)
				if type(__GettedVariable)==int 
				else tuple(__GettedVariable),
				map(
					lambda __ShapingGettingString:
					self[__ShapingGettingString]
					,
					ShapingGettingStringsList
				)
			)
		
		#Bind with ModeledShapedString setting
		self.ShapedString=ShapingJoiningString.join(
									map(
											lambda __ShapingGettingString,__ShapedVariable:
											ShapingJoiningString+str(
												__ShapingGettingString
												)+ShapingTuplingString+str(
												__ShapedVariable),
											ShapingGettingStringsList,
											self.ShapedVariablesList
										)
		)

		#Debug 
		self.debug(('self',self,['ShapedVariablesList','ShapedString']))
	
		#Debug
		self.debug('End of the method')

		#Return self
		return self

	#</DefineMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_shape():
	Shaper=SYS.ShaperClass().update(
								[

									('UnitsInt',3),
									('MyInt',1),
									('MyString',"hello"),
									('MyIntsList',[4,5,6]),
									('ModelingColumnTuplesList',
												[
													('MyInt',tables.Int64Col()),
													('MyString',tables.StringCol(10)),
													('MyIntsList',(tables.Int64Col,'UnitsInt'))
												]
									),
									('RowedIdentifiedGettingStringsList',['MyInt','MyString'])
								]
								).flush(
								).update(
											[
												('UnitsInt',2),
												('MyIntsList',[3,4])
											]
								).flush(
								).close()
																
	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Shaper
		)+'\n\n\n'+Shaper.hdfview().HdformatedString
#</DefineAttestingFunctions>
