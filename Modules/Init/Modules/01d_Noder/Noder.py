#<ImportModules>
import collections
import copy
import os
import ShareYourSystem as SYS
import six
import sys
#</ImportModules>

#<DefineLocals>
NodingStartString='<'
NodingEndString='>'
BasingLocalTypeString="Pather"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class NoderClass(BasedLocalClass):

	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.NodingNodeString=""				#<NotRepresented>
		self.NodedOrderedDict=None 				#<NotRepresented>
		self.NodedNodedString=""				#<NotRepresented>
		self.NodedNodingString=""				#<NotRepresented>
		self.NodedKeyStringKeyString="" 		#<NotRepresented>
		self.NodedKeyString="" 					#<NotRepresented>
		self.NodedParentPointer=None 			#<NotRepresented>
		self.NodedInt=-1 						#<NotRepresented>
		self.NodedPathString="" 				#<NotRepresented>
		self.NodedGrandParentPointersList=[] 	#<NotRepresented>
		#</DefineSpecificDict>

	def node(self,_NodingNodeString,**_VariablesDict):

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Get the NodedString
		self.NodingNodeString=_NodingNodeString
		self.NodedNodedString=SYS.getDoneStringWithDoString(_NodingNodeString)
		self.NodedNodingString=SYS.getDoingStringWithDoString(_NodingNodeString)
	
		#Debug
		'''
		self.debug(("self.",self,['NodingNodeString']))
		'''

		#Set the Noded OrderedDict and KeyString
		NodedOrderedDictKeyString=self.NodedNodedString+'OrderedDict'
		self.NodedKeyStringKeyString=self.NodedNodedString+'KeyString'

		try:
			self.NodedOrderedDict=getattr(self,NodedOrderedDictKeyString)
		except AttributeError:
			self.__setattr__(NodedOrderedDictKeyString,collections.OrderedDict())
			self.NodedOrderedDict=getattr(self,NodedOrderedDictKeyString)

		try:
			self.NodedKeyString=getattr(self,self.NodedKeyStringKeyString)
		except AttributeError:
			self.__setattr__(self.NodedKeyStringKeyString,"")
			self.NodedKeyString=getattr(self,self.NodedKeyStringKeyString)

		#If this is a set of a tree of nodes then also init the nodifying attributes	
		if 'IsNoderBool' not in _VariablesDict or _VariablesDict['IsNoderBool']:

			NodedParentPointerKeyString=self.NodedNodedString+'ParentPointer'
			NodedIntKeyString=self.NodedNodedString+'Int'
			NodedPathStringKeyString=self.NodedNodedString+'PathString'
			NodedGrandParentPointersListKeyString=self.NodedNodedString+'GrandParentPointersList'

			try:
				self.NodedInt=getattr(self,NodedIntKeyString)
			except AttributeError:
				self.__setattr__(NodedIntKeyString,-1)
				self.NodedInt=getattr(self,NodedIntKeyString)

			try:
				self.NodedParentPointer=getattr(self,NodedParentPointerKeyString)
			except AttributeError:
				self.__setattr__(NodedParentPointerKeyString,None)
				self.NodedParentPointer=getattr(self,NodedParentPointerKeyString)

			try:
				self.NodedPathString=getattr(self,NodedPathStringKeyString)
			except AttributeError:
				self.__setattr__(NodedPathStringKeyString,"/")
				self.NodedPathString=getattr(self,NodedPathStringKeyString)

			try:
				self.NodedGrandParentPointersList=getattr(self,NodedGrandParentPointersListKeyString)
			except AttributeError:
				self.__setattr__(NodedGrandParentPointersListKeyString,[])
				self.NodedGrandParentPointersList=getattr(self,NodedGrandParentPointersListKeyString)

	@SYS.HookerClass(**{'BeforeTuplesList':[(BasedLocalClass,"get")]})
	def get(self,**_VariablesDict):

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Debug
		'''
		self.debug(("self.",self,['GettingKeyVariable']))
		'''

		#Appending set
		if self.GettingKeyVariable.startswith(NodingStartString):

			#Define the NodedStringsList
			NodedStringsList=self.GettingKeyVariable.split(NodingEndString)

			#Define the NodingNodeString
			NodingNodeString=NodingStartString.join(
				NodedStringsList[0].split(NodingStartString)[1:])
			
			#Debug
			'''
			self.debug(
						[
							'NodingNodeString is '+NodingNodeString,
							'We are going to node'
						]
					)
			'''

			#Nodify
			self.node(NodingNodeString,**{'IsNoderBool':False})

			#Define the KeyString
			KeyString=NodingEndString.join(NodedStringsList[1:])

			#Debug
			'''
			self.debug(
							[
								'node is done',
								'KeyString is '+KeyString
							]
				)
			'''

			#Get with a digited KeyString case
			if KeyString.isdigit():

				#Define the GettingInt
				GettingInt=(int)(KeyString)

				#Check if the size is ok
				if GettingInt<len(self.NodedOrderedDict):

					#Get the GettedVariable 
					self.GettedValueVariable=SYS.get(self.NodedOrderedDict,'values',GettingInt)

					#Return
					return {'IsCallingBool':False}

			#Get in the ValueVariablesList
			elif KeyString=="":

				#Get the GettedVariable
				self.GettedValueVariable=self.NodedOrderedDict.items()

				#Return 
				return {'IsCallingBool':False}

			elif KeyString in self.NodedOrderedDict:
				
				#Get the GettedVariable
				self.GettedValueVariable=self.NodedOrderedDict[KeyString]

				#Return 
				return {'IsCallingBool':False}

		#Debug
		'''
		self.debug('End of the method')
		'''

	@SYS.HookerClass(**{'BeforeTuplesList':[(BasedLocalClass,"set")]})
	def set(self,**_VariablesDict):
		""" """

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Appending set
		if self.SettingKeyVariable.startswith(NodingStartString):

			#Define the NodedStringsList
			NodedStringsList=self.SettingKeyVariable.split(NodingEndString)

			#Define the NodingNodeString
			NodingNodeString=NodingStartString.join(
				NodedStringsList[0].split(NodingStartString)[1:])

			#Check if it is an append of Nodes
			IsNoderBool=SYS.NoderClass in type(self.SettingValueVariable).__mro__

			#Debug
			'''
			self.debug(('vars',vars(),['NodingNodeString','IsNoderBool']))
			'''

			#Nodify
			self.node(NodingNodeString,**{'IsNoderBool':IsNoderBool})

			#Define the KeyString
			KeyString=NodingEndString.join(NodedStringsList[1:])

			#Debug
			'''
			self.debug('KeyString is '+KeyString)
			'''

			#Append (or set if it is already in)
			SYS.Pather.setWithDictatedVariableAndKeyVariable(
				self.NodedOrderedDict,
				SYS.Pather.PathingString+KeyString,
				self.SettingValueVariable
			)

			#If it is an object
			if IsNoderBool:

				#Int and Set Child attributes
				self.SettingValueVariable.__setattr__(self.NodedNodedString+'Int',len(self.NodedOrderedDict)-1)
				NodedStringKeyString=self.NodedNodedString+'KeyString'
				self.SettingValueVariable.__setitem__(NodedStringKeyString,KeyString)
				self.SettingValueVariable.__setattr__(self.NodedNodedString+'ParentPointer',self)

				#Init GrandChild attributes
				self.SettingValueVariable.__setattr__(self.NodedNodedString+'PathString',"")
				self.SettingValueVariable.__setattr__(self.NodedNodedString+'GrandParentPointersList',[])

			#Return 
			return {'IsCallingBool':False}

		#Debug
		'''
		self.debug('End of the method')
		'''

#</DefineClass>

#<DefineAttestingFunctions>
def attest_node():

	#Short expression and set in the appended manner
	Noder=SYS.NoderClass().__setitem__('<Sort>MySecondInt',1)

	#Short expression for setting in the append without binding
	Noder['<Sort>MySecondInt']+=1

	#Short expression for setting in the appended manner a structured object
	Noder['<Group>MyChildNode']=SYS.NoderClass()

	#Short expression for setting in the appended manner a structured object
	Noder['<Group>MySecondChildNode']=SYS.NoderClass()
	
	#Set with a deep short string and append string
	Noder['<Group>MyChildNode/MyString']="bonjour"
	
	#Set with a deep deep short string and append string
	Noder['/<Group>MyChildNode/<Group>MyGrandChildNode']=SYS.NoderClass()

	#Return Node
	return SYS.represent(Noder)+'\n'+SYS.represent(Noder['<Group>'])

#</DefineAttestingFunctions>

