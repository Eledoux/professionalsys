#<ImportModules>
import os
import ShareYourSystem as SYS
import six
import sys
#</ImportModules>

#<DefineLocals>
ExecutingString="Exec_"
BasingLocalTypeString="Appender"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class ExecuterClass(BasedLocalClass):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.ExecutingString="" #<NotRepresented>
		#</DefineSpecificDict>

	@SYS.HookerClass(**{'BeforeTuplesList':[(BasedLocalClass,"get")]})
	def get(self,**_VariablesDict):

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Check
		if self.GettingKeyVariable.startswith(ExecutingString):

			#Define the ExecString
			self.ExecutingString=ExecutingString.join(self.GettingKeyVariable.split(ExecutingString)[1:])

			#Debug
			self.debug(('self.',self,['ExecutingString']))

			#Put the output in a local Local Variable
			self.execute()

	@SYS.HookerClass(**{'BeforeTuplesList':[(BasedLocalClass,"set")]})
	def set(self,**_VariablesDict):

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Check
		if type(self.SettingValueVariable) in SYS.StringTypesList and self.SettingValueVariable.startswith(ExecutingString):

			#Define the ExecString
			self.ExecutingString=ExecutingString.join(self.SettingValueVariable.split(ExecutingString)[1:])

			#Debug
			'''
			self.debug(('self.',self,['ExecutingString',"SettingValueVariable"]))
			'''

			#Put the output in a local Local Variable
			self.execute()

		#Debug
		'''
		self.debug('End of the method')
		'''

	def execute(self,_ExecutingString=""):

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Reset maybe
		if _ExecutingString!="":
			self.ExecutingString=_ExecutingString

		#Execute
		six.exec_(self.ExecutingString,locals())

		#Debug
		'''
		self.debug('End of the method')
		'''

		#Return self
		return self

#</DefineClass>

#<DefineAttestingFunctions>
def attest_execute():
	Executer=SYS.ExecuterClass().update(
						[
							('MyInt',0),
							('MySecondInt',"Exec_self.SettingValueVariable=self.MyInt+1")
						]
					)
	return SYS.represent(Executer)+"\n"+str("GettedValueVariable is "+str(
		Executer['Exec_self.GettedValueVariable=self.MyInt-1']))


#</DefineAttestingFunctions>
