#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class GridionClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.GridedParentPointer=None
		#</DefineSpecificDict>

	#</DefineHookMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.GridionClass()
#</DefineAttestingFunctions>
