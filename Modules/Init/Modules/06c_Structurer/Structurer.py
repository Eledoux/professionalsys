#<ImportModules>
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Grouper"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class StructurerClass(BasedLocalClass):
	
	@SYS.HookerClass(**{'AfterTuplesList':[("<TypeString>","hdformat")]})
	def structure(self,_StructuringString,**_VariablesDict):

		#Debug
		self.debug('Start of the method')
		
		#Walk while parentizing and grouping
		self.walk(
					[
						SYS.Noder.NodingStartString+_StructuringString+SYS.Noder.NodingEndString
					],
					**{
							'BeforeUpdatingTuplesList':
							[
								('parent',{'ArgsVariable':_StructuringString}),
								('group',{'ArgsVariable':""})
							]
						}
				)

		#Debug
		self.debug('End of the method')

#</DefineClass>

#<DefineAttestingFunctions>
def attest_structure():

	#Build Hdf groups
	Structurer=SYS.StructurerClass().update(
								[
									(
										'<Graph>ChildStructurer1',
										SYS.StructurerClass().update(
										[
											('<Graph>GrandChildStructurer1',
											SYS.StructurerClass())
										])
									),
									(
										'<Graph>ChildStructurer2',
										SYS.StructurerClass().update(
											[]
										)
									)
								]	
							).structure("Graph").close()

	#Get the h5ls version of the stored hdf
	return "\n\n\n\n"+SYS.represent(
			Structurer
		)+'\n\n\n'+Structurer.hdfview().HdformatedString
#</DefineAttestingFunctions>
