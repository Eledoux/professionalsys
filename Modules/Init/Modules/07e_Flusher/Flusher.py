#<ImportModules>
import collections
import numpy
import os
import ShareYourSystem as SYS
import sys
import tables
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Rower"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class FlusherClass(
					BasedLocalClass,
				):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.FlushedIsBoolsList=[] 			#<NotRepresented>
		self.FlushedErrorBool=False 		#<NotRepresented>
		#</DefineSpecificDict> 

	@SYS.HookerClass(**{'AfterTuplesList':[("<TypeString>","row")]})
	def flush(self,_ModelString="",**_FlushingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					('self.',self,['ModelingModelString','RowedIdentifiedOrderedDict'])
				)

		#Unzip
		FlushedIdentifiedColumningStringsList=self.RowedIdentifiedOrderedDict.keys()
		FlushedIdentifiedVariablesList=self.RowedIdentifiedOrderedDict.values()
		
		#Check if it was already rowed
		self.FlushedIsBoolsList=map(
				lambda __Row:
				all(
					map(
							lambda __FlushedIdentifiedColumningString,__FlushedIdentifiedVariable:
							SYS.getIsEqualBool(
												__Row[__FlushedIdentifiedColumningString],
												__FlushedIdentifiedVariable
											),
							FlushedIdentifiedColumningStringsList,
							FlushedIdentifiedVariablesList
						)
					),
				self.TabledTable.iterrows()
			) if len(FlushedIdentifiedColumningStringsList)>0 else [False]

		#Debug
		self.debug(
					("",vars(),[
								'FlushedIdentifiedColumningStringsList',
								'FlushedIdentifiedVariablesList'
							]
					)
				)

		#Append and Flush if it is new
		if any(self.FlushedIsBoolsList)==False:

			#Get the row
			Row=self.TabledTable.row

			#Set the FlushedModeledInt
			FlushedModeledInt=self.TabledTable.nrows
			
			#Define the FlushingTuplesList
			FlushingTuplesList=[
									('ModeledInt',FlushedModeledInt)
								]+self.RowedIdentifiedOrderedDict.items(
									)+self.RowedNotIdentifiedOrderedDict.items()
				
			#Debug
			self.debug(
						[
							'This is a new row',
							'Colnames are : '+str(self.TabledTable.colnames),
							'FlushingTuplesList is '+str(FlushingTuplesList)
						]
					)

			#Set the FlushingTuples in the Row
			try:

				#Set
				map(
						lambda __FlushingTuple:
						Row.__setitem__(*__FlushingTuple),
						FlushingTuplesList
					)

				#Debug
				self.debug('The Row setting was good, so append flush and set IsFlushedBool to True')

				#Append and Flush
				Row.append()
				self.TabledTable.flush()

			except ValueError:
				
				#Debug
				self.debug('ValueError, there should be a shape not corresponding to the one defined in the table')

				#Set
				self.FlushedErrorBool=True

		else:

			#Debug
			self.debug(
						[
							'This is maybe not an IdentifyingFlusher',
							'Or it is already rowed',
							'self.FlushedIsBoolsList is '+str(self.FlushedIsBoolsList)
						]
					)

		#Debug
		self.debug('End of the method')

#</DefineClass>

#<DefineAttestingFunctions>
def attest_flush():
	Flusher=SYS.FlusherClass().update(								
									[
										('MyInt',0),
										('MyString',"hello"),
										('MyIntsList',[2,4,1]),
										('ModelingColumnTuplesList',
											[
												('MyInt',tables.Int64Col()),
												('MyString',tables.StringCol(10)),
												('MyIntsList',(tables.Int64Col(shape=3)))
											]
										),
										('RowedIdentifiedGettingStringsList',['MyInt','MyString'])
									]).flush(
									).update(
										[
											('MyInt',1),
											('MyString',"bonjour"),
											('MyIntsList',[2,4,6])
										]
									).flush(
									).update(
										[
											('MyInt',1),
											('MyString',"bonjour"),
											('MyIntsList',[0,0,0])
										]
									).flush(
									).close()
																
	#Get the h5ls version of the stored hdf
	return SYS.represent(
						Flusher
						)+'\n\n\n\n'+Flusher.hdfview().HdformatedString	
#</DefineAttestingFunctions>
