#<ImportModules>
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class HdformaterClass(SYS.ParserClass):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.HdformatingModuleString="tables" 	#<NotRepresented>
		self.HdformatingPathString="" 			#<NotRepresented>
		self.HdformatedFilePointer=None 		#<NotRepresented>
		self.HdformatedString="" 				#<NotRepresented>
		#</DefineSpecificDict>

	def hdformat(self):

		#Set the HdformatingPathString
		if self.HdformatingPathString=="":
			self.HdformatingPathString=SYS.getCurrentFolderPathString()+self.__class__.TypeString+'.hdf5'

		#Check for first write
		if os.path.isfile(self.HdformatingPathString)==False:

			#Create the file 
			self.HdformatedFilePointer=sys.modules[self.HdformatingModuleString].File(
								self.HdformatingPathString,'w')
				

			#Close it
			self.HdformatedFilePointer.close()

		if self.HdformatedFilePointer==None or ( 
			(self.HdformatingModuleString=='tables' and self.HdformatedFilePointer.isopen==0
				) or (self.HdformatingModuleString=='h5py' and self.HdformatedFilePointer.mode=='c') ):

			#Open the HdformatedFilePointer
			self.HdformatedFilePointer=sys.modules[self.HdformatingModuleString].File(
				self.HdformatingPathString,'r+')

		#Return self
		return self

	def hdfview(self):

		#Set the HdformatedString
		self.HdformatedString=os.popen(
										'/usr/local/bin/h5ls -dlr '+self.HdformatingPathString
								).read()
		
		#Return self
		return self

	def close(self):

		#Close the HdformatedFilePointer
		if self.HdformatedFilePointer!=None:
			self.HdformatedFilePointer.close()

		#Return self
		return self
	#</DefineHookMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_hdformat():

	#Build Hdf groups
	Hdformater=SYS.HdformaterClass().hdformat().close()

	#Get the h5ls version of the stored hdf
	print(Hdformater)
	print(os.popen('h5ls -dlr '+Hdformater.HdformatingPathString).read())
	return os.popen('h5ls -dlr '+Hdformater.HdformatingPathString).read()
#</DefineAttestingFunctions>
