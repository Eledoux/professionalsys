#<ImportModules>
import collections
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Parenter"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class ParserClass(BasedLocalClass):
	
	#<DefineHookMethods>
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.WalkingDict={} 				#<NotRepresented>
		self.WalkedOrderedDict={} 			#<NotRepresented>
		#</DefineSpecificDict>

	def walk(self,_WalkingGatheringVariablesList,**_WalkingDict):
		
		#Init the TopWalkedOrderedDict
		TopWalkedOrderedDict=None
		if 'IdString' not in _WalkingDict:

			#Define the IdString of this walk
			IdString=str(id(_WalkingDict))

			#Set the _WalkingDict
			_WalkingDict.update(
									{
										'IdString':IdString,
										'TopPointer':self,
									}
								)

			#Define TopWalkedOrderedDictKeyString
			TopWalkedOrderedDictKeyString=IdString+'WalkedOrderedDict'

			#Set the corresponding WalkedOrderedDict
			self.__setattr__(
								TopWalkedOrderedDictKeyString,
								collections.OrderedDict(**
								{
									'WalkedInt':-1,
									'TopWalkedIntsList':['/'],
									'GrabbedVariablesOrderedDict':collections.OrderedDict(),
									'RoutedVariablesList':[],
									'TopPointersList':[self]
								})
							)

			#Alias this Dict
			TopWalkedOrderedDict=getattr(self,TopWalkedOrderedDictKeyString)

		else:

			#Get the information at the top
			TopWalkedOrderedDictKeyString=_WalkingDict['IdString']+'WalkedOrderedDict'
			TopWalkedOrderedDict=getattr(_WalkingDict['TopPointer'],TopWalkedOrderedDictKeyString)
			TopWalkedOrderedDict['WalkedInt']+=1
			TopWalkedOrderedDict['TopWalkedIntsList']+=[str(TopWalkedOrderedDict['WalkedInt'])]
			TopWalkedOrderedDict['TopPointersList']+=[self]

		#Refresh the attributes
		self.WalkingDict=_WalkingDict

		#Maybe Grab things...It is important to put his kind of hook before the recursive walk if we want to set from top to down
		if 'GrabbingVariablesList' in _WalkingDict:
			self.grab(_WalkingDict['GrabbingVariablesList'],TopWalkedOrderedDict)

		#Maybe Route
		if 'RoutingVariablesList' in _WalkingDict:
			self.route(_WalkingDict['RoutingVariablesList'],TopWalkedOrderedDict)

		#An Update just before is possible
		if 'BeforeUpdatingTuplesList' in _WalkingDict:
			self.update(_WalkingDict['BeforeUpdatingTuplesList'])

		#Command an recursive order in other gathered variables
		self.commandAllSetsForEach(
									[
										('walk',{
													'ArgsVariable':_WalkingGatheringVariablesList,
													'KwargsDict':_WalkingDict
													})
									],
									_WalkingGatheringVariablesList
								)

		#An Update just after is possible
		if 'AfterUpdatingTuplesList' in _WalkingDict:
			self.update(_WalkingDict['AfterUpdatingTuplesList'])

		#Retrieve the previous Path
		if len(TopWalkedOrderedDict['TopWalkedIntsList'])>0:
			TopWalkedOrderedDict['TopWalkedIntsList']=TopWalkedOrderedDict['TopWalkedIntsList'][:-1] 
			TopWalkedOrderedDict['TopPointersList']=TopWalkedOrderedDict['TopPointersList'][:-1]

		#Return self
		if _WalkingDict['TopPointer']==self:
			self.WalkedOrderedDict=TopWalkedOrderedDict
			del self[TopWalkedOrderedDictKeyString]
			return self
			
	def grab(self,_GrabbingVariablesList,_TopWalkedOrderedDict):
		SYS.setWithDictatedVariableAndKeyVariable(
									_TopWalkedOrderedDict['GrabbedVariablesOrderedDict'],
									_TopWalkedOrderedDict['TopWalkedIntsList'],
									collections.OrderedDict(
										self.gather(
											_GrabbingVariablesList
										) if len(_GrabbingVariablesList)>0 else {}
									)
								)


	def route(self,_RoutingVariablesList,_TopWalkedOrderedDict):

		RoutedVariablesList=None
		RoutedVariablesList=reduce(
				operator.__add__,
				map(
					lambda __TopPointer:
					zip(*__TopPointer.gather(_RoutingVariablesList))[1],
					_TopWalkedOrderedDict['TopPointersList']
					)
			)
		_TopWalkedOrderedDict['RoutedVariablesList'].append(RoutedVariablesList)

#</DefineClass>

#<DefineAttestingFunctions>
def attest_walk():

	#Build a parentizing groups architecture
	return SYS.ParserClass().update(
								[
									('GroupingPathString',SYS.getCurrentFolderPathString()+'MyParser.hdf5'),
									(
										'<Group>ChildParser1',
										SYS.ParserClass().update(
										[
											('<Group>GrandChildParser1',
											SYS.ParserClass())
										])
									),
									(
										'<Group>ChildParser2',
										SYS.ParserClass().update(
											[]
										)
									)
								]	
							).walk(
										[
											'<Group>'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parent',{'ArgsVariable':"Group"})
												]
											}
									)
#</DefineAttestingFunctions>
