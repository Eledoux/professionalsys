#<ImportModules>
import collections
import copy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
ModelingJoinString='__'
ModelingLinkString='_'
#</DefineLocals>

#<DefineClass>
class ModelerClass(SYS.StructurerClass):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.IsModelingBool=True							#<NotRepresented>
		self.ModeledDict={} 								#<NotRepresented>
		self.ModeledOrderedDict=collections.OrderedDict()	#<NotRepresented>
		#</DefineSpecificDict>

	def model(self,_ModelString="",**_ModelingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Debug
		print('Modeler model method')
		print('_ModelString is ',_ModelString)
		print('')

		#Reinit attributes
		LocalOutputedPointer=self
		if _ModelString=="":
			_ModelString=self.ModeledDict['ModelString']
		else:
			self.ModeledDict=self['App_Model_'+SYS.getDoingStringWithDoString(_ModelString)+'Dict']
			if self.ModeledDict!=None:
				if 'IsModeledBool' not in self.ModeledDict:

					#Set variables
					IsModeledBool=False
					ModelString=_ModelString
					ModelingString=SYS.getDoingStringWithDoString(ModelString)
					ModeledString=SYS.getDoneStringWithDoString(ModelString)
					ModeledSuffixString=SYS.getClassStringWithTypeString(ModeledString+'Model')
					ModeledKeyString=""

					#Put them in the ModeledDict
					LocalVars=vars()
					map(
							lambda __GettingString:
							self.ModeledDict.__setitem__(__GettingString,LocalVars[__GettingString]),
							[
								'IsModeledBool',
								'ModelString',
								'ModelingString',
								'ModeledString',
								'ModeledSuffixString',
								'ModeledKeyString'
							]
						)

		#Set IsModelingBool
		self.IsModelingBool=True
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='model'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**_ModelingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalModelingVariablesDict' in OutputVariable:
							LocalModelingVariablesDict=OutputVariable['LocalModelingVariablesDict']
						if 'LocalOutputedPointer' in OutputVariable:
							LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

					#Check Bool
					if self.IsModelingBool==False:
						return LocalOutputedPointer

		#Debug
		print('END Modeler model method')
		print('_ModelString is ',_ModelString)
		print('')

		#Return the OutputVariable
		return LocalOutputedPointer

	def modelBefore(self,**_ModelingVariablesDict):

		#Debug
		'''
		print('Modeler modelBefore method')
		print("self.ModeledDict['ModelString'] is ",self.ModeledDict['ModelString'])
		print('')
		'''

		#Set the global config of this model if it was not already
		if 'ModelClassesOrderedDict' not in self.ModeledDict:

			#Debug
			print('This is the first model of this Model')
			print('ModelClassesOrderedDict not exists')
			print('')

			'''
			#Get the AlmostShapingColumningTuplesList and NotShapedColumningTuplesList
			[
				self.ModeledDict['AlmostShapingColumningTuplesList'],
				self.ModeledDict['NotShapedColumningTuplesList']
			]=SYS.groupby(
								lambda __ModeledTuple:
								type(__ModeledTuple[1])==tuple,
								self.ModeledDict['ColumningTuplesList']
							)
			'''

			#Debug
			'''
			print("self.ModeledDict['AlmostShapingColumningTuplesList'] is ",self.ModeledDict['AlmostShapingColumningTuplesList'])
			print("self.ModeledDict['NotShapedColumningTuplesList'] is ",self.ModeledDict['NotShapedColumningTuplesList'])
			print('')
			'''

			#Init the ModelClassesOrderedDict
			self.ModeledDict['ModelClassesOrderedDict']=collections.OrderedDict()

		#Debug
		print('We are going to define a ModeledClass for this Model ?')
		print("self.ModeledDict['ModelString'] is ",self.ModeledDict['ModelString'])
		print("self.ModeledDict['IsModeledBool'] is ",self.ModeledDict['IsModeledBool'])
		print('')

		#Check
		if self.ModeledDict['IsModeledBool']==False:

			#Debug
			print('Yes we define a new ModeledClass...')
			print('')

			#Define the ModelClass
			class ModeledClass(tables.IsDescription):

				#Add a tabulared Int (just like a unique KEY in mysql...) 
				ModeledInt=tables.Int64Col()

			#Set the not shaping cols in the ModelClass
			map(
					lambda __NotShapingColumningTuple:
					ModeledClass.columns.__setitem__(
						__NotShapingColumningTuple[0],
						#copy.copy(__NotShapingColumningTuple[1])
						__NotShapingColumningTuple[1]
						),
					self.ModeledDict['ColumningTuplesList']
				)

			#Set a name if it was not already
			if self.ModeledDict['ModeledKeyString']=="":
				self.ModeledDict['ModeledKeyString']=self.ModeledDict['ModeledSuffixString']

			#Alias
			ModeledKeyString=self.ModeledDict['ModeledKeyString']

			#Give a name
			ModeledClass.__name__=ModeledKeyString		

			#Set the ModelClass
			self.ModeledDict['ModelClassesOrderedDict'][ModeledKeyString]=ModeledClass

			#Put local variables in the ModeledDict
			LocalVars=vars()
			map(
					lambda __GettingString:
					self.ModeledDict.__setitem__(__GettingString,LocalVars[__GettingString]),
					[
						'ModeledClass'
					]
				)

	#</DefineHookMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_model():

	#Build Hdf groups
	Modeler=SYS.ModelerClass().update(
								[
									('App_Model_ParameterizingDict',
											{
												'ColumningTuplesList':
												[
													('MyInt',tables.Int64Col()),
													('MyString',tables.StringCol(10)),
													('MyIntsList',tables.Int64Col(shape=3))
												]
											}
									)
								]
								).model('Parameter')
																
	#Return the object itself
	return 'Attest gives : \n\n\n\n'+SYS.represent(
							Modeler
						)+'\n\n\n\n'+SYS.represent(
							Modeler.ModeledClass.__dict__
						)
#</DefineAttestingFunctions>
