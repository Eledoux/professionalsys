#<ImportModules>
import collections
import copy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
ModelingJoinString='__'
ModelingLinkString='_'
BasingLocalTypeString="Structurer"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class ModelerClass(BasedLocalClass):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.ModelingColumnTuplesList=[] 										#<NotRepresented>
		self.ModelingModelString=""												#<NotRepresented>
		self.ModeledCopiedColumnTuplesList=[] 									#<NotRepresented>
		self.ModeledClassesOrderedDict=collections.OrderedDict()				#<NotRepresented>												#<NotRepresented>
		self.ModeledModelingString=""											#<NotRepresented>	
		self.ModeledModeledString=""											#<NotRepresented>
		self.ModeledSuffixString="" 											#<NotRepresented>
		self.ModeledKeyString=""												#<NotRepresented>
		self.ModeledClass=None 													#<NotRepresented>
		#</DefineSpecificDict>

	@SYS.HookerClass(**{'BeforeTuplesList':[(BasedLocalClass,"set")]})
	def set(self,**_VariablesDict):

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#ModeledKeyStrin Binding
		if self.SettingKeyVariable=="ModeledKeyString":

			#Bind with ModeledModelString,ModeledModelingString,ModeledModeledString,ModeledSuffixString setting
			self.ModeledModelString=self.__class__.TypeString.join(self.ModeledKeyString.split(self.__class__.TypeString)[:-1])
			self.ModeledModelingString=SYS.getDoingStringWithDoString(self.ModeledModelString)
			self.ModeledModeledString=SYS.getDoneStringWithDoString(self.ModeledModelString)
			self.ModeledSuffixString=SYS.getClassStringWithTypeString(self.ModeledModelString+'Model')

		if self.SettingKeyVariable=="ModelingColumnTuplesList":
			self.ModeledCopiedColumnTuplesList=copy.copy(self.ModelingColumnTuplesList)

		#Debug
		'''
		self.debug('End of the method')
		'''

	@SYS.SwitcherClass()
	def model(self,**_VariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Debug
		self.debug('Start of the method')

		#Define the ModelClass
		class ModeledClass(tables.IsDescription):

			#Add a tabulared Int (just like a unique KEY in mysql...) 
			ModeledInt=tables.Int64Col()

		#Debug
		self.debug(('self.',self,['ModelingColumnTuplesList']))

		#Set the cols in the ModelClass
		map(
				lambda __ColumnTuple:
				ModeledClass.columns.__setitem__(
					__ColumnTuple[0],
					#copy.copy(__ColumnTuple[1])
					__ColumnTuple[1]
					),
				self.ModelingColumnTuplesList
			)

		#Set a name if it was not already
		if self.ModeledKeyString=="":
			self.ModeledKeyString=self.ModeledSuffixString

		#Give a name
		ModeledClass.__name__=self.ModeledKeyString	

		#Set the ModelClass
		self.ModeledClassesOrderedDict[self.ModeledKeyString]=ModeledClass

		#Set the ModeledClass
		self.ModeledClass=ModeledClass

		#Debug
		self.debug('End of the method')

#</DefineClass>

#<DefineAttestingFunctions>
def attest_model():

	#Build Hdf groups
	Modeler=SYS.ModelerClass().update([
											(
											'ModelingColumnTuplesList',
												[
													('MyInt',tables.Int64Col()),
													('MyString',tables.StringCol(10)),
													('MyIntsList',tables.Int64Col(shape=3))
												]
											)
									]).model()
																
	#Return the object itself
	return 'Attest gives : \n\n\n\n'+SYS.represent(
							Modeler
						)+'\n\n\n\n'+SYS.represent(
							Modeler.ModeledClass.__dict__ if Modeler.ModeledClass!=None else Modeler.ModeledClass
						)
#</DefineAttestingFunctions>
