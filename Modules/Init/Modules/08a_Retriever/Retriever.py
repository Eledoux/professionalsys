#<ImportModules>
import collections
import copy
import itertools
import ShareYourSystem as SYS
import tables
import operator
import os
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Flusher"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class RetrieverClass(BasedLocalClass):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.RetrievingTuple=(-1,-1) 		#<NotRepresented>
		self.RetrievedModeledInt=-1			#<NotRepresented>
		self.RetrievedTable=None 			#<NotRepresented>
		self.RetrievedTuplesList=[] 		#<NotRepresented>
		#</DefineSpecificDict>

	@SYS.HookerClass(**{'AfterTuplesList':[("<TypeString>","table")]})
	def retrieve(self,**_VariablesDict):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						('self.',self,[
										'TabularedKeyStringsList',
										'RetrievedTuplesList'
									])
					]
				)

		#Maybe set RetrievedTuplesList if it was not yet done
		if self.RetrievedTuplesList==[]:

			#Debug
			self.debug('self.RetrievingTuple is '+str(self.RetrievingTuple))

			#Set the RetrievedModeledInt
			self.RetrievedModeledInt=self.RetrievingTuple[1]

			#Define the RetrievedTable
			self.RetrievedTable=self.TabularedOrderedDict[
														self.TabularedKeyStringsList[self.RetrievingTuple[0]]
													]

			#Define the RetrievedRowsList
			self.RetrievedTuplesList=[]
			for __RetrievedRow in self.RetrievedTable.iterrows():
				if __RetrievedRow['ModeledInt']==self.RetrievedModeledInt:
					self.RetrievedTuplesList=map(
												lambda __ColumnString:
												(__ColumnString,__RetrievedRow[__ColumnString]),
												self.RetrievedTable.colnames
											)
					break

		#Debug
		self.debug(
					[
						'self.RetrievedTuplesList is '+str(self.RetrievedTuplesList),
						'Now we update'
					]
				)

		#Update
		self.update(self.RetrievedTuplesList)

		#Debug
		self.debug('Update was done')

		#Debug
		self.debug('End of the method')

#</DefineClass>

#<DefineAttestingFunctions>
def attest_retrieve():

	#Retrieve
	Retriever=SYS.RetrieverClass()
	Retriever.IsDebuggingBool=False
	Retriever.update(
					[
						('ModelingColumnTuplesList',
											[
												#ColumnString 	#Col 	
												('FirstInt',	tables.Int64Col()),
												('SecondInt',	tables.Int64Col())
											]
						),
						('RowedIdentifiedGettingStringsList',['FirstInt','SecondInt'])
					]
		).update(
					[
						('FirstInt',1),
						('SecondInt',2)
					]
		).flush(
		).update(
					[
						('FirstInt',1),
						('SecondInt',3)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',2)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',3)
					]
		).flush()
	Retriever.IsDebuggingBool=True
	Retriever.__setitem__('RetrievingTuple',(0,2)
		).retrieve(	
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Retriever
		)+'\n\n\n'+Retriever.hdfview().HdformatedString
#</DefineAttestingFunctions>
