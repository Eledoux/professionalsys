#<ImportModules>
import collections
import copy
import inspect
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
HookingBeforeString='Before'
HookingAfterString='After'
#</DefineLocals>

#<DefineFunctions>
def getSettedHookedListWithInstanceVariable(_HookedList,_InstanceVariable):

	#Debug
	'''
	print('HookedList is '+str(_HookedList))
	print('')
	'''

	#Set first the class 
	if _HookedList[0]=="<TypeString>":
		_HookedList[0]=_InstanceVariable.__class__
	elif _HookedList[0]=="<BaseTypeString>":	
		_HookedList[0]=_InstanceVariable.__class__.__bases__[0]
	elif type(_HookedList[0])==str:
		_HookedList[0]=getattr(
								SYS,
								SYS.getClassStringWithTypeString(_HookedList[0])
								)

	#Set then the method
	if type(_HookedList[1])==str and hasattr(_HookedList[0],_HookedList[1]):
		_HookedList[1]=getattr(_HookedList[0],_HookedList[1])

	#Return the list
	return _HookedList


#<DefineFunctions>

#<DefineClass>
class HookerClass(object):
	def __init__(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.HookingFunction=None
		self.HookingBeforeTuplesList=[]
		self.HookingAfterTuplesList=[]
		self.HookedBeforeTuplesList=[]
		self.HookedAfterTuplesList=[]
		self.HookingUniqueBool=True
		self.HookedFunctionString=""
		self.HookedBeforeFunctionsList=[]
		self.HookedAfterFunctionsList=[]
		self.HookedFunction=None
		self.HookedClass=None
		self.HookedIsBool=False
		#<DefineSpecificDict>

		#Update with the Kwargs
		map(
				lambda __ItemTuple:
				self.__setattr__('Hooking'+__ItemTuple[0],__ItemTuple[1]),
				_VariablesDict.iteritems()
			)

	def __call__(self,_HookingFunction):

		#Hook
		self.hook(_HookingFunction)

		#Return the HookedFunction
		return self.HookedFunction

	def hook(self,_HookingFunction):

		#Set the  HookedFunction
		self.HookingFunction=_HookingFunction	

		#Set the HookedFunctionString
		self.HookedFunctionString=_HookingFunction.__name__

		#Define the WrappedFunction
		def HookedFunction(*_VariablesList,**_VariablesDict):

			#Debug
			'''
			print('Debugger l.88 : Start of the method')
			print('')
			'''

			#Define an alias of the instance
			InstanceVariable=_VariablesList[0]

			#Init maybe the _VariablesDict
			if 'HookingIsBool' not in _VariablesDict:
				_VariablesDict['HookingIsBool']=True
				_VariablesDict['HookedFunctionsList']=[]
				_VariablesDict['HookingUniqueBool']=self.HookingUniqueBool

			#Init maybe the hooking classes and functions for the first call
			if self.HookedIsBool==False:

				#Set to the HookerPointer the corresponding class
				self.HookedClass=InstanceVariable.__class__

				#Set the name
				self.__class__.__name__=self.HookedClass.__name__

				#Debug
				'''
				print('We have to first set in the class the hooking classes and corresponding functions')
				print(str(self.HookingFunction))
				print(str([self.HookingBeforeTuplesList,self.HookingAfterTuplesList]))
				print('')
				'''

				#Set the self.HookedBeforeClass,self.HookedAfterClass
				[self.HookedBeforeTuplesList,self.HookedAfterTuplesList]=map(
					lambda __HookingTuplesList:
					map(
							lambda __HookedList:
							getSettedHookedListWithInstanceVariable(__HookedList,InstanceVariable),
							map(list,__HookingTuplesList)
					),
					[self.HookingBeforeTuplesList,self.HookingAfterTuplesList]
				)

				#Set the hooking functions
				[self.HookedBeforeFunctionsList,self.HookedAfterFunctionsList]=map(
																			lambda __HookedTuplesList:
																			SYS.unzip(__HookedTuplesList,[1]),
														[self.HookedBeforeTuplesList,self.HookedAfterTuplesList]
														)

				#Set the hooking functions
				[self.HookedBeforeStringsList,self.HookedAfterStringsList]=map(
													lambda __HookedBeforeFunctionsList:
													map(
															lambda __HookedBeforeFunction:
								__HookedBeforeFunction.im_func.__repr__() 
								if hasattr(__HookedBeforeFunction,'im_func')
								else __HookedBeforeFunction.__repr__()
													,__HookedBeforeFunctionsList
													),
													[self.HookedBeforeFunctionsList,self.HookedAfterFunctionsList]
													)

				#Debug
				print('Hooker l.158 : hooked functions are setted and they are : ')
				#print(str([self.HookedBeforeFunctionsList,self.HookedAfterFunctionsList]))
				print(str([self.HookedBeforeStringsList,self.HookedAfterStringsList]))
				print('')

				#Say ok for the setting
				self.HookedIsBool=True

			#After hooks (integrativ loop)
			for __HookedAfterFunction in self.HookedAfterFunctionsList:

				#Check
				if _VariablesDict['HookingIsBool']:
	
					#Check
					if callable(__HookedAfterFunction):
					
						#Check if it is a unique call or not
						IsCalledBool=True
						if _VariablesDict['HookingUniqueBool']:
							if __HookedAfterFunction in _VariablesDict['HookedFunctionsList']:
								IsCalledBool=False

						#Append 
						_VariablesDict['HookedFunctionsList'].append(__HookedAfterFunction)

						#Check for calling
						if IsCalledBool:

							#Debug
							'''
							print('__HookedAfterFunction is called '+str(
												__HookedAfterFunction))
							print('From Module '+str(inspect.getmodule(__HookedAfterFunction)))
							print('')
							'''

							#Call
							OutputVariable=__HookedAfterFunction(*_VariablesList,**_VariablesDict)

							#Update maybe the _VariablesDict
							if type(OutputVariable)==dict:
								_VariablesDict.update(OutputVariable)

				else:

					#Return the instance
					return InstanceVariable

			#Debug
			#print('_HookedFunction is '+str(_HookedFunction))

			#Check if it is a unique call or not
			IsCalledBool=True
			if _VariablesDict['HookingUniqueBool']:
				if self.HookingFunction in _VariablesDict['HookedFunctionsList']:
					IsCalledBool=False

			#Append 
			_VariablesDict['HookedFunctionsList'].append(self.HookingFunction)

			if _VariablesDict['HookingIsBool']:

				#Debug
				'''
				print('self.HookingFunction is called '+str(self.HookingFunction))
				print('From Module '+str(inspect.getmodule(self.HookingFunction)))
				print('')
				'''

				#call the method
				OutputVariable=self.HookingFunction(*_VariablesList,**_VariablesDict)

				#Update maybe the _VariablesDict
				if type(OutputVariable)==dict:
						_VariablesDict.update(OutputVariable)

			else:

				#Return the instance
				return InstanceVariable

			#Before hooks (integrativ loop)
			for __HookedBeforeFunction in self.HookedBeforeFunctionsList:

				#Check
				if _VariablesDict['HookingIsBool']:
					
					#Check
					if callable(__HookedBeforeFunction):

						#Check if it is a unique call or not
						IsCalledBool=True
						if _VariablesDict['HookingUniqueBool']:
							if __HookedBeforeFunction in _VariablesDict['HookedFunctionsList']:
								IsCalledBool=False

						#Append 
						_VariablesDict['HookedFunctionsList'].append(__HookedBeforeFunction)

						#Check for calling
						if IsCalledBool:

							#Debug
							'''
							print('__HookedBeforeFunction is called '+str(__HookedBeforeFunction))
							print('From Module '+str(inspect.getmodule(__HookedBeforeFunction)))
							print('')
							'''

							#Call
							OutputVariable=__HookedBeforeFunction(*_VariablesList,**_VariablesDict)

							#Update maybe the _VariablesDict
							if type(OutputVariable)==dict:
								_VariablesDict.update(OutputVariable)

				else:

					#Return the Instance
					return InstanceVariable

			#Return self for the wrapped method call
			return InstanceVariable

		#Set the name
		if self.HookedClass!=None:
			HookedFunction.__name__=self.HookedFunctionString+'From'+SYS.getTypeStringWithClassString(
				self.HookedClass.__name__)

		#Give a Pointer to the Hooker
		HookedFunction.HookerPointer=self

		#Define a represent function for the hooked function
		def represent():
			RepresentedString=inspect.getmodule(self.HookingFunction
					).__name__+'.Hooked_'+self.HookingFunction.__repr__()
			return RepresentedString

		HookedFunction.__repr__=represent

		#Set
		self.HookedFunction=HookedFunction

		#Return self
		return self

#</DefineClass>

