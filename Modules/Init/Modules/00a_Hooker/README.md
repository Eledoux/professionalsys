#Hooker

::

	import ShareYourSystem as SYS

	#Define a first Class
	class FirstClass(object):
	def printSomething(self,**_VariablesDict):
	print('First')
	SYS.FirstClass=FirstClass

	#Do a test of the hooked method printSomething
	print("Just for control, the object from the FirstClass just says First")
	FirstClass().printSomething()
	print('')

	#Define a derived second Class
	class SecondClass(FirstClass):
	@SYS.HookerClass(**{'AfterTuplesList':[('First','printSomething')]})
	def printSomething(self,**_VariablesDict):
	print('Second')
	SYS.SecondClass=SecondClass

	#Do a test of the hooked method printSomething
	print("Now the object of the SecondClass hooks after with the same named method")
	SecondClass().printSomething()
	print('')

Blabla