#<ImportModules>
import collections
import copy
import inspect
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
DecoratingBeforeString='Before'
DecoratingAfterString='After'
#</DefineLocals>

#<DefineClass>
class DecoratorClass(object):
	def __init__(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.DecoratedMethodString=""
		self.DecoratedBeforeTypeString=""
		self.DecoratedAfterTypeString=""
		self.DecoratedBeforeClass=None
		self.DecoratedAfterClass=None
		self.DecoratedBeforeMethod=None
		self.DecoratedAfterMethod=None
		self.DecoratedMethodsList=[]
		#<DefineSpecificDict>

		#Update with the Kwargs
		map(
				lambda __ItemTuple:
				self.__setattr__('Decorated'+__ItemTuple[0],__ItemTuple[1]),
				_VariablesDict.iteritems()
			)

		#Split into the MethodString, the HookString and the TypeString
		BeforeStringsList=self.DecoratedMethodString.split(DecoratingBeforeString)
		if len(BeforeStringsList)>1:
			self.DecoratedDoString=DecoratingBeforeString.join(BeforeStringsList[:-1])
			BeforeString=DecoratingBeforeString.join(BeforeStringsList[1:])
			AfterStringsList=BeforeString.split(DecoratingAfterString)
		else:
			AfterStringsList=self.DecoratedMethodString.split(DecoratingAfterString)
			self.DecoratedDoString=DecoratingAfterString.join(AfterStringsList[:-1])
		if len(AfterStringsList)>1:
			self.DecoratedAfterTypeString=DecoratingAfterString.join(AfterStringsList[1:])
			DoOrBeforeTypeString=DecoratingAfterString.join(AfterStringsList[:-1])
			if DoOrBeforeTypeString!=self.DecoratedDoString:
				self.DecoratedBeforeTypeString=DoOrBeforeTypeString
		
		#Debug
		print(
				self.DecoratedDoString,
				self.DecoratedBeforeTypeString,
				self.DecoratedAfterTypeString
			)

		#Define the self.DecoratedBeforeClass,self.DecoratedAfterClass
		self.DecoratedBeforeClass,self.DecoratedAfterClass=map(
			lambda __HookString:
			getattr(
				SYS,
				SYS.getClassStringWithTypeString(
					getattr(
							self,
							'Decorated'+__HookString+'TypeString'
							)
				)
			),
			[DecoratingBeforeString,DecoratingAfterString]
		)

		#Debug
		print(self.DecoratedBeforeClass,self.DecoratedAfterClass)

		#Define the self.DecoratedBeforeMethod,self.DecoratedAfterMethod
		self.DecoratedBeforeMethod,self.DecoratedAfterMethod=map(
			lambda __DecoratedClass:
			getattr(
					__DecoratedClass,
					self.DecoratedMethodString
					)
			if hasattr(__DecoratedClass,self.DecoratedMethodString) else None,
			[self.DecoratedBeforeClass,self.DecoratedAfterClass]
		)

		#Debug
		print(self.DecoratedBeforeMethod,self.DecoratedAfterMethod)
	
	def __call__(self,_DecoratedMethod):
		#Define the WrappedMethod
		def WrappedMethod(*ArgsList,**_KwargsDict):

			#Debug
			#print('self.DecoratedAfterMethod is '+str(self.DecoratedAfterMethod))

			#After hook
			if callable(self.DecoratedAfterMethod):
				self.DecoratedAfterMethod(*ArgsList,**_KwargsDict)

			#Debug
			#print('_DecoratedMethod is '+str(_DecoratedMethod))

			#The method
			_DecoratedMethod(*ArgsList,**_KwargsDict)

			#Debug
			#print('self.DecoratedBeforeMethod is '+str(self.DecoratedBeforeMethod))

			#Before hook
			if callable(self.DecoratedBeforeMethod):
				self.DecoratedBeforeMethod(*ArgsList,**_KwargsDict)

			#Return self for the wrapped method call
			return self

		#Return the WrappedMethod
		return WrappedMethod
#</DefineClass>

