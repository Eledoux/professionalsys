#<ImportModules>
import collections
import copy
import inspect
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
HookingBeforeString='Before'
HookingAfterString='After'
#</DefineLocals>

#<DefineClass>
class HookerClass(object):
	def __init__(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.HookingFunctionPointer=None
		self.HookingBeforeTypeString=""
		self.HookingAfterTypeString=""
		self.HookingUniqueBool=True
		self.HookedFunctionString=""
		self.HookedBeforeClass=None
		self.HookedAfterClass=None
		self.HookedBeforeList=None
		self.HookedAfterList=None
		self.HookingBeforeMethodString=""
		self.HookingAfterMethodString=""
		self.HookedBeforeFunction=None
		self.HookedAfterFunction=None
		self.HookedFunctionPointer=None
		self.HookedIsBool=False
		#<DefineSpecificDict>

		#Update with the Kwargs
		map(
				lambda __ItemTuple:
				self.__setattr__('Hooking'+__ItemTuple[0],__ItemTuple[1]),
				_VariablesDict.iteritems()
			)

		#Debug
		'''
		print(
				self.HookingBeforeTypeString,
				self.HookingAfterTypeString
			)
		print('')
		'''

	def __call__(self,_HookingFunction):

		#Hook
		self.hook(_HookingFunction)

		#Return the HookedFunction
		return self.HookedFunctionPointer

	def hook(self,_HookingFunction):

		#Set the  HookedFunction
		self.HookingFunctionPointer=_HookingFunction

		#Set the HookedFunctionString
		self.HookedFunctionString=_HookingFunction.__name__

		#Define the self.HookedBeforeList,self.HookedAfterList
		self.HookedBeforeList,self.HookedAfterList=map(
			lambda __HookingTypeString,__HookingMethodString:
			[
				getattr(
					SYS,
					SYS.getClassStringWithTypeString(
						__HookingTypeString
					)
				)
				,
				'<ToBeSetted>'
			] 
			if __HookingTypeString!=""
			else ['<ToBeSetted>',__HookingMethodString]
			if __HookingMethodString!=""
			else [None,None]
			,
			map(
					lambda __HookingString:
					getattr(self,'Hooking'+__HookingString+'TypeString'),
					[HookingBeforeString,HookingAfterString]
				),
			map(
					lambda __HookingString:
					getattr(self,'Hooking'+__HookingString+'MethodString'),
					[HookingBeforeString,HookingAfterString]
				)
		)

		#Debug
		'''
		print(self.HookedBeforeList,self.HookedAfterList)
		print('')
		'''

		#Define the WrappedFunction
		def HookedFunction(*_VariablesList,**_VariablesDict):

			#Define an alias of the instance
			InstanceVariable=_VariablesList[0]

			if 'IsCallingBool' not in _VariablesDict:
				_VariablesDict['IsCallingBool']=True
				_VariablesDict['CalledClassesList']=[]
				_VariablesDict['HookingUniqueBool']=self.HookingUniqueBool

			#Debug
			'''
			print('self.HookedAfterFunction is '+str(self.HookedAfterFunction))
			print('')
			'''

			#Init maybe the hooking classes and functions for the first call
			if self.HookedIsBool==False:

				#Debug
				print('We have to first set in the class the hooking classes and corresponding functions')
				print(str([self.HookedBeforeList,self.HookedAfterList]))
				print('')

				#Set the self.HookedBeforeClass,self.HookedAfterClass
				map(
					lambda __HookedList:
					map(
						lambda __SettingTuple:
						__HookedList.__setitem__(*__SettingTuple),
						[
							(
								0,
								InstanceVariable.__class__ 
								if __HookedList[0]=="<ToBeSetted>"
								else __HookedList[0]
							),
							(
								1,
								getattr(__HookedList[0],self.HookedFunctionString)
								if __HookedList[1]=="<ToBeSetted>"
								else
								getattr(InstanceVariable.__class__,__HookedList[1])
								if __HookedList[1]!=None
								else
								__HookedList[1]
							)
						]
					),
					[self.HookedBeforeList,self.HookedAfterList]
				)

				#Debug
				print('Then')
				[self.HookedBeforeFunction,self.HookedAfterFunction]=zip(self.HookedBeforeList,self.HookedAfterList)[1]
				print('')

				#self.HookedBeforeFunction,self.HookedAfterFunction=zip(*)

				#Debug
				print('Ok this is setted')
				print(str((self.HookedBeforeClass,self.HookedAfterClass)))
				print(str((self.HookedBeforeFunction,self.HookedAfterFunction)))
				print('')

				#Say ok for the setting
				self.HookedIsBool=True

			#After hook
			if callable(self.HookedAfterFunction
				) and _VariablesDict['IsCallingBool']:
				
				#Check if it is a unique call or not
				IsCalledBool=True
				if _VariablesDict['HookingUniqueBool']:
					if self.HookedAfterFunction.im_class in _VariablesDict['CalledClassesList']:
						IsCalledBool=False

				#Append 
				_VariablesDict['CalledClassesList'].append(self.HookedAfterFunction.im_class)

				#Check for calling
				if IsCalledBool:
					OutputVariable=self.HookedAfterFunction(*_VariablesList,**_VariablesDict)

					#Update maybe the _VariablesDict
					if type(OutputVariable)==dict:
						_VariablesDict.update(OutputVariable)

			#Debug
			#print('_HookedFunction is '+str(_HookedFunction))

			#The method
			if _VariablesDict['IsCallingBool']:

				#call the method
				OutputVariable=self.HookingFunctionPointer(*_VariablesList,**_VariablesDict)

				#Update maybe the _VariablesDict
				if type(OutputVariable)==dict:
						_VariablesDict.update(OutputVariable)

			#Debug
			'''
			print('self.HookedBeforeFunction is '+str(self.HookedBeforeFunction))
			print('')
			'''

			#Before hook
			if callable(self.HookedBeforeFunction
				) and _VariablesDict['IsCallingBool']:
				
				#Check if it is a unique call or not
				IsCalledBool=True
				if _VariablesDict['HookingUniqueBool']:
					if self.HookedBeforeFunction.im_class in _VariablesDict['CalledClassesList']:
						IsCalledBool=False

				#Append 
				_VariablesDict['CalledClassesList'].append(self.HookedBeforeFunction.im_class)

				#Check for calling
				if IsCalledBool:
					OutputVariable=self.HookedBeforeFunction(*_VariablesList,**_VariablesDict)

					#Update maybe the _VariablesDict
					if type(OutputVariable)==dict:
						_VariablesDict.update(OutputVariable)

			#Return self for the wrapped method call
			return InstanceVariable

		#Give a Pointer to the Hooker
		HookedFunction.HookerPointer=self

		#Set
		self.HookedFunctionPointer=HookedFunction

		#Return self
		return self

#</DefineClass>

