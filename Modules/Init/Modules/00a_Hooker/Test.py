#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#Define a first Class
class FirstClass(object):
	def printSomething(self,**_VariablesDict):
		print('First')
SYS.FirstClass=FirstClass

#Do a test of the hooked method printSomething
print("Just for control, the object from the FirstClass just says First")
FirstClass().printSomething()
print('')


#Define a derived second Class
class SecondClass(FirstClass):
	@SYS.HookerClass(**{'AfterTuplesList':[('First','printSomething')]})
	def printSomething(self,**_VariablesDict):
		print('Second')
SYS.SecondClass=SecondClass

#Do a test of the hooked method printSomething
print("Now the object of the SecondClass hooks after with the same named method")
SecondClass().printSomething()
print('')


#Define a derived third Class
class ThirdClass(SecondClass):
	@SYS.HookerClass(**{
							'BeforeTuplesList':[('<BaseTypeString>','printSomething')],
							'AfterTuplesList':[('First','printSomething')]
							#'UniqueBool':False
						})
	def printSomething(self,**_VariablesDict):
		print('Third')
SYS.ThirdClass=ThirdClass

#Do a test of the hooked method printSomething
print("This rebelled object breaks the order by hooking the 'first'...")
ThirdClass().printSomething()
print('')

#Define a derived fourth Class
class FourthClass(ThirdClass):

	def printOtherThing(self,**_VariablesDict):
		print('Fourth : I am before !')
	
	@SYS.HookerClass(**{
							'AfterTuplesList':[('<TypeString>','printOtherThing')],
							'BeforeTuplesList':[('Third','printSomething')]
						})
	def printSomething(self,**_VariablesDict):
		print('Fourth')
SYS.FourthClass=FourthClass

#Do a test of the hooked method printSomething
print('The fourth object adds simply a hooking method from its own class definition')
FourthClass().printSomething()
print('')
