#<ImportModules>
import collections
import copy
import itertools
import ShareYourSystem as SYS
import tables
import operator
import os
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Shaper"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class MergerClass(BasedLocalClass):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.MergingTuplesList=[] 			#<NotRepresented>	
		self.MergedRowedDictsList=[] 		#<NotRepresented>				
		#<DefineSpecificDict>


	@SYS.HookerClass(**{'BeforeTuplesList':[("Shaper","find")],'AfterTuplesList':[("<TypeString>","merge")]})
	def find(self,**_FindingVariablesDict):

		#Debug
		self.debug('Start of the method')

		#Bound the FoundRowedDictsList with the MergedRowedDictsList one
		self.FoundRowedDictsList=self.MergedRowedDictsList

		#Debug
		self.debug('End of the method')

	@SYS.HookerClass(**{'BeforeTuplesList':[("Recoverer","recover")]})
	def recover(self):

		#Debug
		self.debug('Start of the method')

		#Debug
		self.debug(
					[
						'Check if the RecoveredDict has a TabledInt to set in the RetrievingTuple',
						'self.RecoveredDict is '+str(self.RecoveredDict),
						"'TabledInt' in RecoveredDict is "+str('TabledInt' in self.RecoveredDict)
					]
				)

		#Check
		if 'TabledInt' in self.RecoveredDict:

			#Debug
			self.debug('There is indeed a TabledInt')

			#Set the RetrievingTuple
			RetrievingTuple=(self.RecoveredDict['TabledInt'],self.RetrievingTuple[1])

			#Debug
			self.debug('RetrievingTuple is '+str(self.RetrievingTuple))

		#Debug
		self.debug('End of the method')

	def merge(self,**_VariablesDict):

		#Debug
		self.debug(
					('self.',self,[
										'ModelingModelString',
										'MergingTuplesList',
										'TabularedKeyStringsList'
									])
				)

		#Bind with MergedShapingDictsList setting
		MergedShapingDictsList=map(
								lambda __StringsList:
								dict(
									map(
											lambda __ShapingString:
											SYS.getUnSerializedTuple(
												self,__ShapingString.split(
													SYS.Shaper.ShapingTuplingString
												)
											)
											#Remove the suffix and the prefix
											,__StringsList[1:-1] if len(__StringsList)>2 else []
										)
								),
								map(
									lambda __TabularedKeyString:
									__TabularedKeyString.split(SYS.Shaper.ShapingJoiningString),
									self.TabularedKeyStringsList
								)
						)

		#Debug
		self.debug('MergedShapingDictsList is '+str(MergedShapingDictsList))

		#Bind with MergedFilteredShapingDictsList
		MergedFilteredShapingDictsList=SYS.where(
									MergedShapingDictsList,
									self.MergingTuplesList
									)

		#Debug
		self.debug('MergedFilteredShapingDictsList is '+str(MergedFilteredShapingDictsList))

		#Bind with MergedTablesList setting
		MergedTablesList=SYS.filterNone(
									map(
											lambda __Table,__MergedFilteredShapingDict:
											__Table
											if __MergedFilteredShapingDict!=None
											else None,
											self.TabularedOrderedDict.values(),
											MergedFilteredShapingDictsList
									))
								
		MergedRowedDictsListsList=map(
				lambda __MergedTable:
				map(
						lambda __RowedDict:
						dict(__RowedDict,**{
								'TabledInt':int(
												__MergedTable.name.split(SYS.Tablor.TablorString)[1]
											)
							}
						),
						SYS.getRowedDictsListWithTable(__MergedTable)
					),
				MergedTablesList
			)

		#Debug
		self.debug('MergedRowedDictsListsList is '+str(MergedRowedDictsListsList))

		#Reduce
		if len(MergedRowedDictsListsList)>0:
			self.MergedRowedDictsList=reduce(operator.__add__,MergedRowedDictsListsList)

		#Return self
		return self
#</DefineClass>

#<DefineAttestingFunctions>
def attest_retrieve():
	Merger=SYS.MergerClass(
		).update(
					[
						('FirstInt',0),
						('SecondInt',0),
						('ModelingColumnTuplesList',
											[
												#ColumnString 	#Col 	
												('FirstInt',	tables.Int64Col()),
												('SecondInt',	tables.Int64Col())
											],
										
						),
						('RowedIdentifiedGettingStringsList',['FirstInt','SecondInt']),
						('MergingTuplesList',
								[
									('MulInt',(operator.gt,2))
								]
						),
						('FindingTuplesList',
								[
									('FirstInt',(SYS.getIsInBool,[1,2]))
								]
						)
					]
		).update(
					[
						('FirstInt',1),
						('SecondInt',2)
					]
		).flush(
		).update(
					[
						('FirstInt',1),
						('SecondInt',3)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',2)
					]
		).flush(
		).update(
					[
						('FirstInt',2),
						('SecondInt',3)
					]
		).flush(
		).update(
					[
						('FirstInt',3),
						('SecondInt',3)
					]
		).flush(
		).retrieve(	
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Merger
		)+'\n\n\n'+Merger.hdfview().HdformatedString
#</DefineAttestingFunctions>
