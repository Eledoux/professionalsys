#<ImportModules>
import copy
import operator
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Applyier"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class AppenderClass(BasedLocalClass):
	
	#<DefineHookMethods>
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesList):

		#<DefineSpecificDict>
		self.AppendedNodeString=""					#<NotRepresented>
		self.AppendedNodedString=""					#<NotRepresented>
		self.AppendedKeyString=""					#<NotRepresented>
		self.AppendedKeyStringKeyString=""			#<NotRepresented>
		self.AppendedDict={}						#<NotRepresented>
		self.AppendedSettingKeyVariable=""			#<NotRepresented>
		#</DefineSpecificDict>


	def append(self,_AppendingVariable):
		"""Call __setitem__ with a defined SettingVariable contained in the _AppendingVariable"""
		
		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Debug
		'''
		self.debug('_AppendingVariable is '+str(_AppendingVariable))
		'''

		#TuplesList Case
		if SYS.getIsTuplesListBool(
			_AppendingVariable):
				KeyStringsList=SYS.unzip(_AppendingVariable,[0])
				try:
					NodeStringIndexInt=KeyStringsList.index("AppendingNodeString")
					self.AppendedNodeString=_AppendingVariable[NodeStringIndexInt][1]
					self.AppendedNodedString=SYS.getDoneStringWithDoString(self.AppendedNodeString)
					self.AppendedKeyStringKeyString=self.AppendedNodedString+'KeyString'
					try:
						KeyStringIndexInt=KeyStringsList.index(self.AppendedKeyStringKeyString)
						self.AppendedKeyString=_AppendingVariable[KeyStringIndexInt][1]
					except:
						pass
				except:
					pass

		else:

			#Object Case
			if SYS.AppenderClass in type(_AppendingVariable).__mro__ :
				self.AppendedDict=_AppendingVariable.__dict__

			#dict Case
			elif type(_AppendingVariable)==dict:
				self.AppendedDict=_AppendingVariable

			try:
				self.AppendedNodeString=self.AppendedDict["AppendingNodeString"]
				self.AppendedNodedString=SYS.getDoneStringWithDoString(self.AppendedNodeString)
				self.AppendedKeyStringKeyString=self.AppendedNodedString+'KeyString'
				self.AppendedKeyString=self.AppendedDict[self.AppendedKeyStringKeyString]
			except:
				pass

		#Init the SettingVariable and Add the NodifyingString
		self.AppendedSettingKeyVariable=SYS.Noder.NodingStartString+self.AppendedNodeString+SYS.Noder.NodingEndString+self.AppendedKeyString

		#Debug
		'''
		self.debug([
						('self.',self,[
											'AppendedSettingKeyVariable',
											'AppendedNodedString',
											'AppendedKeyString'
										])
					]
				)
		'''

		#Then set
		if self.AppendedSettingKeyVariable!=SYS.Noder.NodingStartString+SYS.Noder.NodingEndString:
			return self.__setitem__(self.AppendedSettingKeyVariable,_AppendingVariable)

		#Debug
		'''
		self.debug('End of the method')
		'''

		#Return self
		return self


	def __add__(self,_AddingVariablesList):
		"""Apply the append to the <_AddingVariablesList>"""

		#Apply
		self.apply('append',_AddingVariablesList)

		#Return 
		return self

#</DefineClass>

#<DefineAttestingFunctions>
def attest_append():

	#Append with a TuplesList
	Appender=SYS.AppenderClass().append([
										('AppendingNodeString',"Structure"),
										('StructuredKeyString',"MyTuplesList"),
										('MyString',"Hello")
									]
									)

	#Append with a dict
	Appender.append({
						'AppendingNodeString':"Structure",
						'StructuredKeyString':"MyDict",
						'MyOtherString':"Bonjour"
						}
					)

	#Append with an Appender
	return Appender.append(SYS.AppenderClass())

def attest_add():

	#Explicit expression
	return SYS.AppenderClass().__add__([
												[
													('AppendingNodeString',"Structure"),
													('StructuredKeyString',"MyTuplesList"),
													('MyString',"Hello")
												],
												{
													'AppendingNodeString':"Structure",
													'StructuredKeyString':"MyDict",
													'MyOtherString':"Bonjour"
												},
												SYS.AppenderClass().update(
														[
															('AppendingNodeString',"Structure"),
															('StructuredKeyString',"MyChildAppender")
														]
													)
										])

#</DefineAttestingFunctions>
