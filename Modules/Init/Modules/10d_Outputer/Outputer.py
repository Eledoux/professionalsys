#<ImportModules>
import collections
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class OutputerClass(
						SYS.HierarcherClass
				):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.IsOutputingBool=True #<NotRepresented>
		#</DefineSpecificDict>

	def rowBefore(self,**_LocalRowingVariablesList):

		#Output before the row !
		self.output()

	#</DefineHookMethods>

	#<DefineMethods>
	def output(self,**_OutputingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Debug
		'''
		print('Outputer output method')
		print('')
		'''

		#Refresh the attributes and set maybe default values in the _OutputingVariablesDict
		LocalOutputingVariablesDict=_OutputingVariablesDict
		LocalOutputedPointer=self
		self.IsOutputingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='output'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalOutputingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalOutputingVariablesDict' in OutputVariable:
							LocalOutputingVariablesDict=OutputVariable['LocalOutputingVariablesDict']
						if 'LocalOutputedPointer' in OutputVariable:
							LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

					#Check Bool
					if self.IsOutputingBool==False:
						return LocalOutputedPointer

		#Return the OutputVariable
		return LocalOutputedPointer
	#</DefineMethods>

#<DefineAttestingFunctions>
def attest_output():

	#Build Hdf groups
	Outputer=SYS.OutputerClass().hdformat().update(
								[
									(
										'App_Structure_ChildOutputer1',
										SYS.OutputerClass().update(
										[
											('App_Model_ParameteringDict',
														{
															'ColumningTuplesList':
															[
																('MyIntsList',tables.Int64Col(shape=2))
															],
															'IsFeaturingBool':True
														}
											),
											('MyIntsList',[2,4]),
											('App_Model_ResultingDict',
														{
															'ColumningTuplesList':
															[
																('MyInt',tables.Int64Col())
															],
															'IsOutputingBool':True,
															'FeaturingModelingString':'Parameter'
														}
											),
											('MyInt',1)
										])
									),
									('App_Model_ParameteringDict',
														{
															'ColumningTuplesList':
															[
																('MyString',tables.StringCol(10)),
															],
															'IsFeaturingBool':True
														}
									),
									('MyString',"hello")
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':""}),
													('model',{'ArgsVariable':"Parameter"}),
													('tabular',{'ArgsVariable':""}),
													('row',{'ArgsVariable':""}),
													('flush',{'ArgsVariable':""})
												]
											}
									).update(
										[
											
											('App_Model_ResultingDict',
												{
													'ColumningTuplesList':
													[
														('MyFloat',tables.Float32Col()),
														('MyFloatsList',tables.Float32Col(shape=3))
													],
													'IsOutputingBool':True,
													'FeaturingModelingString':"Parameter"
												}
											),
											('MyFloat',0.1),
											('MyFloatsList',[2.3,4.5,1.1])
										]
									).model("Result",**{'JoiningNodifyingString':"Structure"}		
									).tabular("Result",
									).row("Result",**{'JoiningRowingString':"Parameter"}
									).flush("Result"
									).close()

	#Return the object itself
	return "Object is : \n"+SYS.represent(
		Outputer
		)+'\n\n\n'+SYS.represent(os.popen('/usr/local/bin/h5ls -dlr '+Outputer.HdformatingPathString
		).read())
#</DefineAttestingFunctions>
