#<ImportModules>
import collections
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class OutputerClass(
						SYS.FeaturerClass,
						SYS.JoinerClass
				):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.OutputingDict={}
		self.IsOutputingBool=True
		#</DefineSpecificDict>

	def modelAfter(self,**_ModelingVariablesDict):

		#Debug
		print('Output modelAfter method')

		#Get the 
		ModelClass=self.ModeledDict['ModelClassesOrderedDict'][self.ModeledShapedString]

		#Add a FeaturedModeledInt for the join with the FeaturedModelTable 
		ModelClass.columns['FeaturedModeledInt']=tables.Int64Col()
		
		#Join maybe
		if 'JoiningString' in _ModelingVariablesDict:
			DoneJoiningString=SYS.getDoneStringWithDoString(_ModelingVariablesDict['JoiningString'])
			DoneJoiningStringKeyStringKeyString=DoneJoiningString+'KeyString'
			OrderedDictKeyString=DoneJoiningString+'OrderedDict'
			if hasattr(self,OrderedDictKeyString):
				JoinedOrderedDict=getattr(self,OrderedDictKeyString)

				#Set the Cols for the joined children 
				map(
					lambda __DoneJoiningStringKeyString:
					ModelClass.columns.__setitem__(
						SYS.getModeledIntColumnStringWithKeyString(
						__DoneJoiningStringKeyString
						),
						tables.Int64Col()
					),
					map(
							lambda __JoinedObject:
							getattr(
									__JoinedObject,
									DoneJoiningStringKeyStringKeyString
									),
							JoinedOrderedDict.values()
						)
				)

	'''

	def output(self,**_OutputingVariablesDict):
		"""Call the Output<HookString> methods and return self.OutputedPointer (self by default)"""

		#Refresh the attributes and set maybe default values in the _OutputingVariablesDict
		map(
			lambda __DefaultTuple:
			_OutputingVariablesDict.__setitem__(__DefaultTuple[0],__DefaultTuple[1])
			if __DefaultTuple[0] not in _OutputingVariablesDict
			else None,
			[
				('IsRowingBool',False)
			]
		)
		LocalOutputingVariablesDict=_OutputingVariablesDict
		LocalOutputedPointer=self
		self.IsOutputingBool=True
		self.OutputedJoinedRowedIntsList=[]
		self.OldFeaturedModeledInt=-1
		self.NewFeaturedModeledInt=-1
		self.OldOutputedModeledInt=-1
		self.NewOutputedModeledInt=-1
		
		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString='output'+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookMethod
					OutputVariable=HookingMethod(self,**LocalOutputingVariablesDict)

					if type(OutputVariable)==dict:
						if 'LocalOutputingVariablesDict' in OutputVariable:
							LocalOutputingVariablesDict=OutputVariable['LocalOutputingVariablesDict']
						if 'LocalOutputedPointer' in OutputVariable:
							LocalOutputedPointer=OutputVariable['LocalOutputedPointer']

					#Check Bool
					if self.IsOutputingBool==False:
						return LocalOutputedPointer

		#Return the OutputVariable
		return LocalOutputedPointer

	def rowBefore(self,**_RowingVariablesDict):
		pass
	#</DefineHookMethods>

	#<DefineMethod>

	#<DefineMethods>
	#</DefineMethods>
'''

#<DefineAttestingFunctions>
def attest_output():

	#Build Hdf groups
	Outputer=SYS.OutputerClass().hdformat().update(
								[
									(
										'App_Structure_ChildOutputer1',
										SYS.OutputerClass().update(
										[
											('App_Model_FeaturingDict',
														{
															'ColumningTuplesList':
															[
																('MyIntsList',tables.Int64Col(shape=2))
															],
															'IsFeaturingBool':True
														}
											),
											('MyIntsList',[2,4]),
											('App_Model_OutputingDict',
														{
															'ColumningTuplesList':
															[
																('MyInt',tables.Int64Col())
															],
															'IsOutputingBool':True
														}
											),
											('MyInt',1)
										])
									),
									('App_Model_FeaturingDict',
														{
															'ColumningTuplesList':
															[
																('MyString',tables.StringCol(10)),
															],
															'IsFeaturingBool':True
														}
									),
									('MyString',"hello")
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':"Structure"}),
													('model',{'ArgsVariable':"Feature"}),
													('tabular',{'ArgsVariable':"Feature"}),
													('row',{'ArgsVariable':"Feature"}),
													('flush',{'ArgsVariable':"Feature"})
												]
											}
									).update(
										[
											
											('App_Model_OutputingDict',
												{
													'ColumningTuplesList':
													[
														('MyFloat',tables.Float32Col()),
														('MyFloatsList',tables.Float32Col(shape=3))
													],
													'IsOutputingBool':True
												}
											),
											('MyFloat',0.1),
											('MyFloatsList',[2.3,4.5,1.1])
										]
									).model("Output",**{'JoiningNodifyingString':"Structure"}		
									).tabular("Output",
									).row("Output",**{'JoiningRowingString':"Feature"}
									).flush("Output"
									).close()

	#Return the object itself
	return "Object is : \n"+SYS.represent(
		Outputer
		)+'\n\n\n'+SYS.represent(os.popen('h5ls -dlr '+Outputer.HdformatingPathString
		).read())
#</DefineAttestingFunctions>
