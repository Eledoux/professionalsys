#<ImportModules>
import collections
import copy
import os
import ShareYourSystem as SYS
import six
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Getter"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class SetterClass(BasedLocalClass):
		
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""		

		#<DefineSpecificDict>
		self.SettingKeyVariable=None 					#<NotRepresented>
		self.SettingValueVariable=None  				#<NotRepresented>
		#</DefineSpecificDict>

	def __setitem__(self,_SettingKeyVariable,_SettingValueVariable):
		"""Call the set<HookString> methods"""

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Refresh the attributes
		self.SettingKeyVariable=_SettingKeyVariable
		self.SettingValueVariable=_SettingValueVariable

		#Set
		self.set()		

		#Debug
		'''
		self.debug('End of the method')
		'''

		#Set
		return self

	@SYS.HookerClass()
	def set(self,**_VariablesDict):
		""" """

		#Debug
		self.debug('Start of the method')

		#Debug
		'''
		self.debug(("self.",self,['SettingKeyVariable','SettingValueVariable']))
		'''
		
		#__setitem__ in the __dict__, this is an utility set
		self.__dict__[self.SettingKeyVariable]=self.SettingValueVariable

		#Debug
		'''
		self.debug('End of the method')
		'''

		#Return self
		return self

#</DefineClass>

#<DefineAttestingFunctions>
def attest_set():
	return SetterClass().__setitem__('MyInt',0)
#</DefineAttestingFunctions>

