#<ImportModules>
import collections
import itertools
import ShareYourSystem as SYS
import tables
import os
#</ImportModules>

#<DefineLocals>
DoString="Scan"
DoneString="Scanned"
DoingString="Scanning"
#</DefineLocals>

#<DefineClass>
class ScannerClass(SYS.MergerClass):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.ScannedTuplesList=[] 			#<NotRepresented>
		self.ScannedGettingStringsList=[] 	#<NotRepresented>
		self.ScannedOrderedDict={} 			#<NotRepresented>
		#</DefineSpecificDict>

	#</DefineHookMethods>

	def scan(self,_ScanningString,**_LocalScanningVariablesDict):

		#Debug
		'''
		print('Scanner scan method')
		print('_ScanningString is ',_ScanningString)
		if hasattr(self,"StructuredKeyString"):
			print('self.StructuredKeyString is ',self.StructuredKeyString)
		print('')
		'''
		
		#Maybe refresh also modeled and tabulared attributes
		if _ScanningString!="":
			self.join(_ScanningString)
		else:
			_ScanningString=self.ModeledModelString

		#Debug
		'''
		print('self.ModeledDict is ',self.ModeledDict)
		print('')
		'''

		#Init maybe the _LocalScanningVariablesDict
		if _LocalScanningVariablesDict=={}:

			#Set IsScanningBool to False
			_LocalScanningVariablesDict['IsScanningBool']=False

			#Look for a JoiningTuple
			if 'JoiningTuple' in self.ModeledDict:

				#Set the JoiningNodifyingString
				_LocalScanningVariablesDict['JoiningNodifyingString']=self.ModeledDict['JoiningTuple'][0]

				#Maybe we have to structure
				if self.IsGroupedBool==False:

					#Debug
					'''
					print('Scanner We have to structure first')
					if hasattr(self,"StructuredKeyString"):
						print('self.StructuredKeyString is ',self.StructuredKeyString)
					'''

					#Structure
					self.structure(_LocalScanningVariablesDict['JoiningNodifyingString'])

		#Nodify if there is the nodified objects
		if 'JoiningNodifyingString' in _LocalScanningVariablesDict:
			if _LocalScanningVariablesDict['JoiningNodifyingString']!="":
				self.nodify(_LocalScanningVariablesDict['JoiningNodifyingString'])
				self.ScannedOrderedDict=self.NodifiedOrderedDict
			else:
				self.ScannedOrderedDict={}

		#Check for a JoiningTuple
		if 'JoiningTuple' in self.ModeledDict:

			#Check if we have the right to scan
			if len(self.NodifiedOrderedDict)==0 or _LocalScanningVariablesDict['IsScanningBool']:

				#Debug
				'''
				print('self.JoinedModeledDict is ',self.JoinedModeledDict)
				print('')
				'''

				#Set the ScannedGettingStringsList
				self.ScannedGettingStringsList=SYS.unzip(self.JoinedModeledDict['ScanningTuplesList'],[0])

				#Scan the values of this model
				self.ScannedTuplesList=list(
												itertools.product(*SYS.unzip(
												self.JoinedModeledDict['ScanningTuplesList'],[1]
												)
										)
				)


				#for Object in self.NodifiedOrderedDict.values():
				#
				#	print('Object.ScannedTuplesList is ')
				#	print(Object.ScannedTuplesList)





				'''
				for __ScannedTuple in self.ScannedTuplesList:


					self.update(
								zip(
										self.ScannedGettingStringsList,
										__ScannedTuple
									)
								)
				'''








				#Debug
				#print('self.ScannedGettingStringsList is ',self.ScannedGettingStringsList)
				#print('self.ScannedTuplesList is ',self.ScannedTuplesList)

				#Do the map update and flush !
				'''
				map(
						lambda __ScannedTuple:
						self.update(
									zip(
											self.ScannedGettingStringsList,
											__ScannedTuple
										)
									).flush(_ScanningString),
						self.ScannedTuplesList
					)
				'''

			else:

				#Debug
				'''
				print('This either a not last level of child or it is not yet authorized to join')
				print('len(self.NodifiedOrderedDict.values()) is ',len(self.NodifiedOrderedDict.values()))
				print("_LocalJoiningVariablesDict['IsScanningBool']",_LocalScanningVariablesDict['IsScanningBool'])
				print('so join the deeper children groups first')
				print('')
				'''

				map(
						lambda __ScanningObject:
						__ScanningObject.scan(
												self.ModeledDict['ModelingString'],
												**_LocalScanningVariablesDict
											),
						self.ScannedOrderedDict.values()
					)

				'''
				print('join',self.ModeledDict['JoiningNodifyingString'])
				print('The deeper children groups are joined now')
				print('So join here !')
				print('')
				'''

				self.scan(
							self.ModeledDict['ModelingString'],
							**dict(
									_LocalScanningVariablesDict,**{'IsScanningBool':True}
								)
						)

		#Return self
		return self

#</DefineClass>

#<DefineAttestingFunctions>
def attest_scan():

	#Bound an output method
	def outputAfter(_Scanner):
		_Scanner.MulInt=_Scanner.FirstInt*_Scanner.SecondInt
	SYS.ScannerClass.HookingMethodStringToMethodsListDict['outputAfter']=[outputAfter]

	#Flush the default output
	Scanner=SYS.ScannerClass(
		).update(
					[
						('FirstInt',0),
						('SecondInt',0),
						('App_Model_ParameterizingDict',{
											'ColumningTuplesList':
											[
												#ColumnString 	#Col 	
												('FirstInt',	tables.Int64Col()),
												('SecondInt',	tables.Int64Col())
											],
											'IsFeaturingBool':True,
											'ScanningTuplesList':
											[
												('FirstInt',[1,2]),
												('SecondInt',[5,6])
											]
										}),
						('App_Model_ResultingDict',{
											'ColumningTuplesList':
											[
												#ColumnString 	#Col 	
												('MulInt',	tables.Int64Col())
											],
											'JoiningTuple':("","Parameter")
										})
					]
		).scan('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Scanner
		)+'\n\n\n'+SYS.represent(
				os.popen('h5ls -dlr '+Scanner.HdformatingPathString).read()
			)
#</DefineAttestingFunctions>
