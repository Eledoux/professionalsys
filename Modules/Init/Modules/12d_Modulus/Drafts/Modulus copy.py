#<ImportModules>
import ShareYourSystem as SYS
import scipy.stats
import numpy as np
from tables import *
import os
#</ImportModules>

#<DefineClass>
class ModulusClass(
						SYS.JoinerClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.ModulusFloat=0.
		self.PowerFloat=0.5
		#</DefineSpecificDict>

		#Define a Structure
		self['App_Structure_RealMul']=SYS.MulClass()
		self['App_Structure_ComplexMul']=SYS.MulClass()

		#Define the features
		self['App_Model_ParameterizingDict']={
										'ColumningTuplesList':
										[
											#ColumnString 	#Col 	
											('PowerFloat',	Float32Col()),
										],
										'IsFeaturingBool':True
									}

		#Define the outputs
		self['App_Model_ResultingDict']={
										'ColumningTuplesList':
										[
											#ColumnString 		#Col 	
											('ModulusFloat',	Float32Col()),
											('ModulusIntsList',	Int64Col(shape=2))
										]
									}

	def outputAfter(self,**_LocalOutputingDict):
		
		
		self.ModulusIntsList=[
								self['App_Group_RealMul'].MulInt,
								#self['App_Group_RealMul'].MulInt
								self['App_Group_ComplexMul'].MulInt
							]

		#Bind with ModuleFloat Setting
		self.ModulusFloat=np.power(
						self['App_Group_RealMul'].MulInt**2+self['App_Group_ComplexMul'].MulInt**2,
						#self['App_Group_RealMul'].MulInt**2+self['App_Group_RealMul'].MulInt**2,
						self.PowerFloat
					)
	#</DefineHookMethods>
	
#</DefineClass>

#<DefineAttestingFunctions>
def attest_store():

	#Flush the default output
	Modulus=SYS.ModulusClass().flush('Result').close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Modulus
		)+'\n\n\n'+SYS.represent(
				os.popen('h5ls -dlr '+Modulus.HdformatingPathString).read()
			)
#</DefineAttestingFunctions>