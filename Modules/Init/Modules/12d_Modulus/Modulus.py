#<ImportModules>
import ShareYourSystem as SYS
import scipy.stats
import numpy as np
from tables import *
import operator
import os
#</ImportModules>

#<DefineClass>
class ModulusClass(
						SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.ModulusFloat=0.
		self.PowerFloat=0.5
		#</DefineSpecificDict>

		#Define a Structure
		self['App_Structure_RealMul']=SYS.MulClass()
		self['App_Structure_RealMul']['App_Model_ParameterizingDict']['ScanningTuplesList']=[]
		self['App_Structure_ComplexMul']=SYS.MulClass()
		self['App_Structure_ComplexMul']['App_Model_ParameterizingDict']['ScanningTuplesList']=[
																		('FirstInt',[5,6]),
																		('SecondInt',[1,2])
																		]
						
		#Define the features
		self['App_Model_ParameterizingDict']={
												'ColumningTuplesList':
												[
													#ColumnString 	#Col 	
													('PowerFloat',	Float32Col()),
												],
												'IsFeaturingBool':True,
												'ScanningTuplesList':
												[
													('PowerFloat',[0.5,1])
												]
									}

		#Define the outputs
		self['App_Model_ResultingDict']={
											'ColumningTuplesList':
											[
												#ColumnString 		#Col 	
												('ModulusFloat',	Float32Col()),
												('ModulusIntsList',	Int64Col(shape=2)),
												('ModulusIntsListsList',Int64Col(shape=(2,2)))
											],
											'JoiningModelString':"Parameter",
											'HierarchingNodeString':"Structure"
									}

	def outputAfter(self,**_LocalOutputingDict):
		
		#Set aliases
		RealMul=self['App_Structure_RealMul']
		ComplexMul=self['App_Structure_ComplexMul']

		#Set the ModulusIntsList (useful to track the sub leveled parameters)
		self.ModulusIntsList=[
								RealMul.MulInt,
								ComplexMul.MulInt
							]

		#Set the ModulusIntsListsList (useful to track the sub leveled parameters)
		self.ModulusIntsListsList=[
										[RealMul.FirstInt,RealMul.SecondInt],
										[ComplexMul.FirstInt,ComplexMul.SecondInt]
								]

		#Bind with ModuleFloat Setting
		self.ModulusFloat=np.power(
						RealMul.MulInt**2+ComplexMul.MulInt**2,
						self.PowerFloat
					)
	#</DefineHookMethods>
	
#</DefineClass>

#<DefineAttestingFunctions>
def attest_flush():


	Modulus=SYS.ModulusClass()
	Modulus.IsDebuggingBool=False
	Modulus.update(
					[
						('PowerFloat',0.5),
						('/App_Structure_RealMul/FirstInt',2),
						('/App_Structure_ComplexMul/FirstInt',2)
					]
		).flush('Result'
		).update(
					[
						('PowerFloat',1.),
					]
		).flush()
	Modulus.IsDebuggingBool=True

	print(" \n\n\n\nENSUITE \n\n\n\n")

	Modulus.update(
					[
						('/App_Structure_RealMul/FirstInt',3),
					]
			).flush(
		)

	'''
		.update(
					[
						('/App_Structure_RealMul/SecondInt',2),
						('/App_Structure_ComplexMul/SecondInt',2)
					]
		).flush(
		).close()
	'''

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Modulus
		)+'\n\n\n'+SYS.represent(
				Modulus.hdfview().HdformatedString
			)

def attest_retrieve():

	#Retrieve
	Modulus=SYS.ModulusClass(
		).update(
					[
						('/App_Model_ResultingDict/RetrievingTuplesList',[
													('ModulusFloat',(operator.gt,3.)),
													#('__IntsList',(SYS.getIsEqualBool,[4,5])),
													#('__JoinedList',(SYS.getIsEqualBool,[0,0]))
													#('__RealMul/FirstInt',(SYS.getIsEqualBool,1))
											])
					]
		).retrieve('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Modulus
		)


def attest_recover():

	#Retrieve
	Modulus=SYS.ModulusClass(
		).update(
					[
						('/App_Model_ResultingDict/RetrievingTuplesList',[
									('ModulusFloat',(operator.gt,3.)),
									#('/RealMul/MulInt',(SYS.getIsEqualBool,2)),
									#('/RealMul/__FirstInt',(SYS.getIsEqualBool,2)),
									#('/ComplexMul/ParameterizedJoinedList',(SYS.getIsEqualBool,[0,0]))
							]),
						#('/App_Structure_RealMul/App_Model_ResultingDict/RetrievingTuplesList',
						#	[
						#			('MulInt',(SYS.getIsEqualBool,2)),
						#			#('__FirstInt',(SYS.getIsEqualBool,2)),
						#			#('/ComplexMul/ParameterizedJoinedList',(SYS.getIsEqualBool,[0,0]))
						#	]),
						#('/App_Structure_ComplexMul/App_Model_ResultingDict/RetrievingTuplesList',
						#	[
						#			('ParameterizedJoinedList',(SYS.getIsEqualBool,[0,0]))
						#	])
					]
		).recover('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Modulus
		)

def attest_scan():

	#Scan
	Modulus=SYS.ModulusClass(
		).scan('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Modulus
		)+'\n\n\n'+SYS.represent(
				os.popen('/usr/local/bin/h5ls -dlr '+Modulus.HdformatingPathString).read()
			)
#</DefineAttestingFunctions>