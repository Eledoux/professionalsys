#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#Define a first Class
class FirstClass(object):

	def __setitem__(self,_a,_b):
		self.__dict__[_a]=_b

	

	


class SecondClass(FirstClass):

	print(self)

	@SYS.BinderClass(**{'MethodString':'set','SelectTuple':('_KeyString',SYS.getIsEqualBool('MyFirstInt'))})
	def setMySecondInt(self,**_VariablesDict):
		self.MySecondInt=self.MyFirstInt+1


MyFirst=FirstClass()
MyFirst['MyFirstInt']=1

print(MyFirst.__dict__)