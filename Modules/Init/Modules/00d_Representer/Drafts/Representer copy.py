#<ImportModules>
import copy
import numpy
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class RepresenterClass(SYS.InitiaterClass):
	
	@SYS.HookerClass(**{'AfterTypeString':"Initiater"})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.IsRepresentingBool=True
		self.RepresentingDict={}
		self.RepresentedTuplesList=[]
		self.NotRepresentingGettingVariablesList=[]
		self.RepresentingKeyVariablesList=[]
		#</DefineSpecificDict>

	def __repr__(self):
		"""Call the repr<HookString> methods and return a SYS printed version of the self.RepresentedDict"""

		#Refresh the attributes
		self.RepresentingDict=copy.deepcopy(SYS.RepresentingDict)
		self.RepresentedTuplesList=self.__dict__.items()
		self.IsRepresentingBool=True

		#Hook methods
		for OrderString in ["Before","After"]:
		
			#Define the HookMethodString
			HookingMethodString="repr"+OrderString

			#Check that there is HookingMethods for it
			if HookingMethodString in self.__class__.HookingMethodStringToMethodsListDict:

				#Call the specific Appended methods 
				for HookingMethod in self.__class__.HookingMethodStringToMethodsListDict[HookingMethodString]:

					#Call the HookingMethod
					HookingMethod(self)

					#Check Bool
					if self.IsRepresentingBool==False:
						return SYS.getRepresentedPointerStringWithRepresentedPointerAndRepresentingDict(
								self,
								self.RepresentedDict
								)+SYS.getRepresentedVariableStringWithRepresentedVariableAndRepresentingDict(
								dict(self.RepresentedTuplesList),
								self.RepresentingDict
								)

		#return the representedVariable
		return SYS.getRepresentedPointerStringWithRepresentedPointerAndRepresentingDict(
				self,
				self.RepresentingDict
				)+SYS.getRepresentedVariableStringWithRepresentedVariableAndRepresentingDict(
				dict(self.RepresentedTuplesList),
				self.RepresentingDict
				)
	
	def reprAfter(self):
		"""
			Hook in the repr of the Object
			for printing in the Console attributes in a more 
			user-friendly manner
		"""

		#Get the MroAppendedStringsList
		MroAppendedStringsList=SYS.getMroKeyStringsListWithClass(self.__class__)
		
		#Remove the class NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.__class__.NotRepresentingGettingStringsList,
										self.RepresentedTuplesList
								)

		#Remove the instance NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.NotRepresentingGettingVariablesList,
										self.RepresentedTuplesList
								)

		#First keeps only the Specific and New attributes
		self.RepresentedTuplesList=map(
									lambda _RepresentedTuple:
									("<Spe>"+_RepresentedTuple[0],_RepresentedTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] in self.__class__.SpecificKeyStringsList,
											self.RepresentedTuplesList
										)
								)+map(
									lambda _NewTuple:
									("<New>"+_NewTuple[0],_NewTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] not in MroAppendedStringsList,
											self.RepresentedTuplesList
										)
								)

		#Add some forced Values with RepresentingKeyVariables
		self.RepresentedTuplesList+=map(
											lambda _KeyVariable:
											("<NotSpe>"+str(_KeyVariable),self[_KeyVariable]),
											self.RepresentingKeyVariablesList
										)

		#Simplify the numpy variables repr
		self.RepresentedTuplesList=map(
										lambda _RepresentedTuple:
										_RepresentedTuple
										if type(_RepresentedTuple[1]) not in [numpy.ndarray] 
										else (
												_RepresentedTuple[0],
												SYS.getRepresentedNumpyArray(_RepresentedTuple[1])
											),
										self.RepresentedTuplesList
									)

#</DefineClass>

