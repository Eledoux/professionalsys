#<ImportModules>
import copy
import numpy
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Initiater"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class RepresenterClass(BasedLocalClass):
	
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):
		
		#<DefineSpecificDict>
		self.RepresentingNotGettingVariablesList=[]
		self.RepresentingKeyVariablesList=[]
		self.RepresentingDict={}
		self.RepresentedTuplesList=[]
		#</DefineSpecificDict>

	def __repr__(self):
		"""Call the repr<HookString> methods and return a SYS printed version of the self.RepresentedDict"""

		#Refresh the attributes
		self.RepresentingDict=copy.deepcopy(SYS.RepresentingDict)
		self.RepresentedTuplesList=self.__dict__.items()

		#Represent
		self.represent()

		#return the representedVariable
		return SYS.getRepresentedPointerStringWithRepresentedPointerAndRepresentingDict(
				self,
				self.RepresentingDict
				)+SYS.getRepresentedVariableStringWithRepresentedVariableAndRepresentingDict(
				dict(self.RepresentedTuplesList),
				self.RepresentingDict
				)
	
	def represent(self):
		
		#Get the MroAppendedStringsList
		MroAppendedStringsList=SYS.getMroKeyStringsListWithClass(self.__class__)
		
		#Remove the class NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.__class__.RepresentingNotGettingStringsList,
										self.RepresentedTuplesList
								)

		#Remove the instance NotRepresented attributes
		self.RepresentedTuplesList=filter(
										lambda _RepresentedTuple:
										_RepresentedTuple[0] not in self.RepresentingNotGettingVariablesList,
										self.RepresentedTuplesList
								)

		#First keeps only the Specific and New attributes
		self.RepresentedTuplesList=map(
									lambda _RepresentedTuple:
									("<Spe>"+_RepresentedTuple[0],_RepresentedTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] in self.__class__.SpecificKeyStringsList,
											self.RepresentedTuplesList
										)
								)+map(
									lambda _NewTuple:
									("<New>"+_NewTuple[0],_NewTuple[1]),
									filter(
											lambda _Tuple:
											_Tuple[0] not in MroAppendedStringsList,
											self.RepresentedTuplesList
										)
								)

		#Add some forced Values with RepresentingKeyVariables
		self.RepresentedTuplesList+=map(
											lambda _KeyVariable:
											("<NotSpe>"+str(_KeyVariable),self[_KeyVariable]),
											self.RepresentingKeyVariablesList
										)

		#Simplify the numpy variables repr
		self.RepresentedTuplesList=map(
										lambda _RepresentedTuple:
										_RepresentedTuple
										if type(_RepresentedTuple[1]) not in [numpy.ndarray] 
										else (
												_RepresentedTuple[0],
												SYS.getRepresentedNumpyArray(_RepresentedTuple[1])
											),
										self.RepresentedTuplesList
									)

#</DefineClass>

