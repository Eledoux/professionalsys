#<ImportModules>
import sys
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Func"
DoingString="Functing"
DoneString="Functed"
#<DefineClass>

#<DefineClass>
class FunctorClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.FunctingFunctionPointer=None
		self.FunctingArgsList=[]
		self.FunctingKwargsDict={}
		#</DefineSpecificDict>
											
	def funcAfter(self,_LocalCallVariable,_LocalCallingVariablesList,**_LocalCallingDict):

		#Define a default OutputVariable
		OutputVariable=None

		#Call the FunctionPointer
		if callable(self.FunctingFunctionPointer):

			if SYS.getIsKwargsFunctionBool(self.FunctingFunctionPointer):
				OutputVariable=self.FunctingFunctionPointer(*_LocalCallingVariablesList,**_LocalCallingDict)
			else:
				OutputVariable=self.FunctingFunctionPointer(*_LocalCallingVariablesList)

		#Set to True
		self.IsCallingBool==True

		#Return the OutputVariable
		return {'LocalCalledPointer':OutputVariable}
		
	#</DefineHookMethods>


#</DefineClass>