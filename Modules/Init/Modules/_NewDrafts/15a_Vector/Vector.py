#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
from tables import *
import time
import operator
import os
#</ImportModules>


#<DefineClass>
class VectorClass(SYS.ScanningObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.UnitsInt=1
		self.FloatsList=[0]*self.UnitsInt
		self.MinFloat=-100.
		self.MaxFloat=100.
		#self['App_Group_Config']=SYS.ConfigClass()
		#</DefineSpecificDict>

		#Define the features
		self.FeaturingTuplesList=[
										#ColumnString 	#GettingVariable 	#Col 			#ScanningVariablesList
										#('UnitsInt',	'UnitsInt',			Int64Col(),		[1,2]),
										('MinFloat',	'MinFloat',			Float32Col(),	[-100.]),
										('MaxFloat',	'MaxFloat',			Float32Col(),	[75.])
									]

		#Define the outputs
		self.OutputingTuplesList=[
										#ColumnString 		#GettingVariable 	#Col 
										('SeedVectorInt',	'SeedVectorInt',	Int64Col()),
										('FloatsList',		'FloatsList',		(Float64Col,'UnitsInt'))
								]

		'''
		#Define the scans
		self.ScanningTuplesList=[	
										#ColumnString 	#GettingVariable 	#ScanningVariablesList
										('UnitsInt',	'UnitsInt',			[1,2])
								]
		
		#Define the arrays
		self.ArrayingTuplesList=[	
										#ColumnString 	#GettingVariable	#ColClass
										('FloatsList',	'FloatsList',		Int64Col)
								]
		'''

	def outputAfter(self,**_LocalOutputingVariablesDict):
		
		#Set 
		self.SeedVectorInt=self['App_Group_Config'].SeedInt

		#Bind with MulIntsList setting
		self.FloatsList=scipy.stats.uniform.rvs(size=self.UnitsInt)

	#</DefineHookMethods>

	#</DefineBindingMethods>
	#</DefineBindingMethods>
	
#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.VectorClass()

def attest_output():

	#Append a default output
	SYS.Config.seed(5)

	#Return the object __repr__
	TestedVariable=SYS.VectorClass().output()
	print(TestedVariable)
	return TestedVariable

def attest_store():

	#Flush the default output
	SYS.Config.seed(5)
	Vector=SYS.VectorClass().organize().output().store().output().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Mul.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

def attest_scan():

	#Flush with all the scanning featuring values
	Vector=SYS.VectorClass().organize().scan().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Vector.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

#</DefineAttestingFunctions>
