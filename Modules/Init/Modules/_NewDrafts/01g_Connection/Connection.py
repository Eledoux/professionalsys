#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
from tables import *
import time
import operator
import os
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class ConnectionClass(SYS.GridionClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.ConnectedIntsTuple=(-1,-1)
		self.PrePointer=None
		self.PostPointer=None
		#</DefineSpecificDict>

	def connect(self):

		if self.GridedParentPointer!=None:

			self.PrePointer,self.PostPointer=map(
									lambda __ConnectedInt:
									SYS.get(self.GridedParentPointer.StructuredOrderedDict,'values',__ConnectedInt),
									self.ConnectedIntsTuple
								)

			print(self.PrePointer)

	#</DefineHookMethods>


	
#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.ConnectionClass()

def attest_output():

	#Append a default output
	SYS.Config.seed(5)

	#Return the object __repr__
	TestedVariable=SYS.ConnectionClass().output()
	print(TestedVariable)
	return TestedVariable

def attest_store():

	#Flush the default output
	SYS.Config.seed(5)
	Connection=SYS.ConnectionClass().organize().output().store().output().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Connection.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

def attest_scan():

	#Flush with all the scanning featuring values
	Connection=SYS.ConnectionClass().organize().scan().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Connection.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

#</DefineAttestingFunctions>
