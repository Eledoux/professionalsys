#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineFunctions>
def getFilteredScannedTuplesListsListWithFilterDict(_ScannedTuplesListsList,_FilterDict):
	return filter(
					lambda __Tuple:
					all(
						map(
								lambda __ItemTuple:
								dict(__Tuple)[__ItemTuple[0]]==__ItemTuple[1] if __ItemTuple[0] in dict(__Tuple) else False,
								_FilterDict.items()
							)
					),
					_ScannedTuplesListsList
		)
SYS.getFilteredScannedTuplesListsListWithFilterDict=getFilteredScannedTuplesListsListWithFilterDict

def getScannedStringsListWithScanningTuplesListsList(_ScanningTuplesListsList):
	return map(
				lambda ListedTuples:
				reduce(lambda AccumulatedTupleString,TupleString:
						AccumulatedTupleString.__add__("_"+TupleString),
						map(
							lambda Tuple: 
							"("+Tuple[0]+","+str(Tuple[1])+'.0' if Tuples[1]==1. else str(Tuple[1])+")",
							ListedTuples
							)
						),
				_ScanningTuplesListsList
				)
SYS.getScannedStringsListWithScanningTuplesListsList=getScannedStringsListWithScanningTuplesListsList


def getNumberVariableWithString(_String):
	try:
		return int(_String)
	except ValueError:
		try:
			return float(_String)
		except ValueError:
			return str(_String)
#</DefineFunctions>

#<DefineClass>
class FileClass(SYS.DirClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.FilePathString=""
		self.FolderPathString=""
		self.ExtensionString=""
		self.FileString=""
		self.FilePointer=None
		self.ModeString="r"
		#</DefineSpecificDict>
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindFolderPathStringAfter(self):

		#Bind with FilePathString setting
		self.setFilePathStringWithFile()

	def bindFileStringAfter(self):

		#Bind with FilePathString setting
		self.setFilePathStringWithFile()

	def bindExtensionStringAfter(self):

		#Bind with FilePathString setting
		self.setFilePathStringWithFile()

	def bindDirStringAfter(self):

		#Bind with FolderPathString setting
		if len(self.DirString)>0:
			self['FolderPathString']=self.DirString+"/" if self.DirString[-1]!="/" else self.DirString

	#<DefineBindingHookMethods>	

	#<DefineMethods>
	def setFilePathStringWithFile(self):
		self['FilePathString']=self.FolderPathString+self.FileString+self.ExtensionString

	def getWithFilterDict(self,_FilterDict):

		#Get the ScannedTuplesListsList
		ScannedTuplesListsList=map(
					lambda TupleStringsList:
					map(
							lambda TupleString:
							tuple(
									map(
										lambda IntAndString:
										str(IntAndString[1]) if IntAndString[0]==0
										else getNumberVariableWithString(IntAndString[1]),
										enumerate(
											TupleString.replace("*","")[1:-1].split(',')
											)
										)
								),
							TupleStringsList[1:]
						),
					map(
						lambda KeyString:
						KeyString.split("_"),
						self.get('/').keys()
					)
				)

		#Get the corresponding FilteredScannedTuplesListsList
		FilteredScannedTuplesListsList=getFilteredScannedTuplesListsListWithFilterDict(
				ScannedTuplesListsList
				,
				_FilterDict
				)

		#Return the SYS.getFilteredScannedTuplesLists
		FilteredKeyStringsList=SYS.getScannedStringsListWithScanningTuplesListsList(
			map(
				lambda FilteredScannedTuplesList:
				map(
						lambda FilteredScannedTuple:
						["*"+FilteredScannedTuple[0],FilteredScannedTuple[1]],
						FilteredScannedTuplesList
					),
				FilteredScannedTuplesListsList
			)
		)
		#print(FilteredKeyStringsList)
		
		#Get the FilteredDataDictsList
		FilteredDataDictsList=map(
			lambda KeyString:
			self.get('/_'+KeyString),
			FilteredKeyStringsList
			)

		#Return the FilteredDataDictsList
		return FilteredDataDictsList

	#<DefineMethods>	

#</DefineClass>