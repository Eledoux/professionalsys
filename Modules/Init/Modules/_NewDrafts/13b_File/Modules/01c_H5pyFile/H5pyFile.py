#<ImportModules>
import ShareYourSystem as SYS
import h5py
#</ImportModules>

#<DefineFunctions>
"""
def setGroupWithIndexStringAndDatabaseFileAndDictingDataItemTuple(_IndexString,_DatabaseFile,_DataItemTuple):

	#Set the group
	#print("Dict",_DataItemTuple)
	if _DataItemTuple[0] not in _DatabaseFile:
		_DatabaseFile.create_group(_DataItemTuple[0]) 

	#set deeper in this group
	setDataWithIndexStringAndDatabaseFileAndDataDict(_IndexString,_DatabaseFile[_DataItemTuple[0]],_DataItemTuple[1])
SYS.setGroupWithIndexStringAndDatabaseFileAndDictingDataItemTuple=setGroupWithIndexStringAndDatabaseFileAndDictingDataItemTuple
"""
"""
def setGroupWithDatabaseFileAndDataDict(_DatabaseFile,_DataDict):

	#Get the MetaKeyStringsList to buid the IdString
	MetaKeyStringsList=filter(lambda KeyString:KeyString[0]=="*",_DataDict.keys())

	#Build the MetaDataString (this is going to be the Id of this set)
	IdString="_"+"_".join(map(lambda MetaKeyString:"("+MetaKeyString+","+str(_DataDict[MetaKeyString])+")",MetaKeyStringsList))

	#Set the group
	#print("Dict",_DataItemTuple)
	if IdString not in _DatabaseFile:
		_DatabaseFile.create_group(IdString) 

	#set deeper in this group
	setDataWithIndexStringAndDatabaseFileAndDataDict(IdString,_DatabaseFile[IdString],_DataDict)
"""
"""
def setGroupWithDatabaseFileAndListingDataItemTuple(_DatabaseFile,_DataItemTuple):

	#Set the group
	print("List",_DataItemTuple)
	if _DataItemTuple[0] not in _DatabaseFile:
		_DatabaseFile.create_group(_DataItemTuple[0]) 

	#set deeper in this group by attributing to each listed dict a "_i" IndexString
	map(
		lambda ListedVariable:
		#setGroupWithIndexStringAndDatabaseFileAndDictingDataItemTuple(
		#	"_"+str(IntAndListedVariable[0]),_DatabaseFile[_DataItemTuple[0]],("_"+str(IntAndListedVariable[0]),IntAndListedVariable[1])
		#	)
		setGroupWithDatabaseFileAndDataDict(_DatabaseFile[_DataItemTuple[0]],ListedVariable)
		,
		_DataItemTuple[1]
		)
"""
"""
def setDataSetWithIndexStringAndDatabaseFileAndDataItemTuple(_IndexString,_DatabaseFile,_DataItemTuple):

	#Set the group
	#print("Dataset",_DataItemTuple)
	if _DataItemTuple[0] not in _DatabaseFile:
		_DatabaseFile.create_group(_DataItemTuple[0]) 

	#set deeper in this group
	_DatabaseFile[_DataItemTuple[0]].create_dataset(_IndexString,data=_DataItemTuple[1])

def setDataWithIndexStringAndDatabaseFileAndDataDict(_IndexString,_DatabaseFile,_DataDict):

	#print('setData',_DataDict)
	map(
			lambda DataItemTuple:
			#Dict Case
			setGroupWithIndexStringAndDatabaseFileAndDictingDataItemTuple(_IndexString,_DatabaseFile,DataItemTuple) 
			if type(DataItemTuple[1])==dict 
			else
				#List of Dict Case
				setGroupWithDatabaseFileAndListingDataItemTuple(_DatabaseFile,DataItemTuple)
				if type(DataItemTuple[1])==list and all(map(lambda ListedVariable:type(ListedVariable)==dict,DataItemTuple[1]))
				#List of several different things : do anything
				else
					None
					if type(DataItemTuple[1])==list and any(map(lambda ListedVariable:type(ListedVariable)!=dict,DataItemTuple[1]))
					#Metadata or Dataset 
					else
						#Metada case
						_DatabaseFile.__setitem__(DataItemTuple[0],DataItemTuple[1]) 
						if DataItemTuple[0][0]=="*"
						else	
						#Dataset case (finally) !
						setDataSetWithIndexStringAndDatabaseFileAndDataItemTuple(_IndexString,_DatabaseFile,DataItemTuple), 
			_DataDict.items()
		) 
"""
"""
def getLastIndexIntWithDatabaseFileAndDataDict(_DatabaseFile,_DataDict):

	return max(
				map(
						lambda DataString:
						max(
								map(
									lambda KeyString:
									(int)(KeyString),
									_DatabaseFile[DataString].keys()
								) 
							) if all(map(lambda String:String.isdigit(),
									_DatabaseFile[DataString].keys()))
							else
							getLastIndexIntWithDatabaseFileAndDataDict(_DatabaseFile[DataString],_DataDict[DataString]),
						_DataDict.keys()
					)
			)
"""

def getIdStringWithDict(_Dict):

	#Get the MetaKeyStringsList to buid the IdString
	MetaKeyStringsList=filter(lambda KeyString:KeyString[0]=="*",sorted(_Dict.keys()))

	#Build the MetaDataString (this is going to be the Id of this set)
	return unicode("_"+"_".join(
							map(
								lambda MetaKeyString:
								"("+MetaKeyString+","+('1.0' if (_Dict[MetaKeyString]==1. and MetaKeyString=='*GammaProbaCon') else str(
									_Dict[MetaKeyString]))+")",
								MetaKeyStringsList
								)
							)
					)
SYS.getIdStringWithDict=getIdStringWithDict

def getDictatingDictWithDataList(_DataList):
	
	return dict(
				map(
					lambda ListedDict:
					(getIdStringWithDict(ListedDict),getDictatingDict(ListedDict)),
					_DataList
				)
			)
SYS.getDictatingDictWithDataList=getDictatingDictWithDataList

def getDictatingDict(_Dict):

	#print('setData',_DataDict)
	DictatingDict=dict(
						map(
							lambda DataItemTuple:
							#Dict Case
							(DataItemTuple[0],getDictatingDict(DataItemTuple[1])) 
							if type(DataItemTuple[1])==dict 
							else
								#List of Dict Case
								("_"+"".join(DataItemTuple[0].split("List")[0:-1])+"Dict",getDictatingDictWithDataList(DataItemTuple[1]))
								if type(DataItemTuple[1])==list and all(map(lambda ListedVariable:type(ListedVariable)==dict,DataItemTuple[1]))
								else
									#List of non Dict Case
									(DataItemTuple[0],DataItemTuple[1])
									if type(DataItemTuple[1])==list and any(map(lambda ListedVariable:type(ListedVariable)!=dict,DataItemTuple[1]))
									else
									#Metadata or Data Case
									(DataItemTuple[0],DataItemTuple[1])
							,_Dict.items()
						)
					)

	return DictatingDict
SYS.getDictatingDict=getDictatingDict


def getListingDict(_Dict):

	#print('setData',_DataDict)
	ListingDict=dict(
						map(
							lambda DataItemTuple:
							#List Case
							(
								"".join(DataItemTuple[0][1:].split("Dict")[:-1])+"List",
								DataItemTuple[1].values()
							) 
							if DataItemTuple[0][0]=="_" and SYS.getCommonSuffixStringWithStringsList(["Dict",DataItemTuple[0]])=="Dict" 
							else
								#Dict Case
								(DataItemTuple[0],getListingDict(DataItemTuple[1]))
								if hasattr(DataItemTuple[1],"keys")
								else
									#Dataset Case
									(DataItemTuple[0],DataItemTuple[1])
							,_Dict.items()
						)
					)
	return ListingDict
SYS.getListingDict=getListingDict

def setDatabaseFileWithAttributeKeyStringAndAttributeVariable(_DatabaseFile,_AttributeKeyString,_AttributeVariable):

	#Set this attribute if not already
	if _AttributeKeyString not in _DatabaseFile:
		_DatabaseFile.__setitem__(_AttributeKeyString,_AttributeVariable)
SYS.setDatabaseFileWithAttributeKeyStringAndAttributeVariable=setDatabaseFileWithAttributeKeyStringAndAttributeVariable

def setDatabaseFileWithCommitStringAndGroupKeyStringAndDataSetVariable(_DatabaseFile,_CommitString,_GroupKeyString,_DataSetVariable):

	#print('Set Dataset ',_GroupKeyString,_DataSetVariable)
	#Set the parent group if not already
	if _GroupKeyString not in _DatabaseFile.keys():
		_DatabaseFile.create_group(_GroupKeyString)

	#Get the IndexIntString
	try:
		IndexIntString=str(
						max(
							map(
								lambda IndexString:
								(int)(IndexString.split("_")[0]),
								_DatabaseFile[_GroupKeyString].keys()
								)
							)+1
						)
	except:
		IndexIntString="0"

	#Build the IndexString
	IndexString=IndexIntString+"_"+_CommitString

	#Set the dataset
	_DatabaseFile[_GroupKeyString].create_dataset(IndexString,data=_DataSetVariable)
SYS.setDatabaseFileWithCommitStringAndGroupKeyStringAndDataSetVariable=setDatabaseFileWithCommitStringAndGroupKeyStringAndDataSetVariable

def setDatabaseFileWithCommitStringAndKeyStringAndDictatingDict(_DatabaseFile,_CommitString,_KeyString,_DictatingDict):

	#Set the group if not already
	if _KeyString not in _DatabaseFile.keys():
		_DatabaseFile.create_group(_KeyString)

	#print("setDatabaseFileWithKeyStringAndDictatingDict",_DictatingDict)
	#Set deeper
	map(
		lambda DataItemTuple:
		#Dict Case with a KeyString
		setDatabaseFileWithCommitStringAndKeyStringAndDictatingDict(
																		_DatabaseFile[_KeyString],
																		_CommitString,
																		DataItemTuple[0],
																		DataItemTuple[1]
																	) 
		if type(DataItemTuple[1])==dict
		else 
			#MetaData case
			setDatabaseFileWithAttributeKeyStringAndAttributeVariable(
																		_DatabaseFile[_KeyString],
																		DataItemTuple[0],
																		DataItemTuple[1]
																	)
			if DataItemTuple[0][0] in ["*","$"]
			else
				#Data case
				setDatabaseFileWithCommitStringAndGroupKeyStringAndDataSetVariable(
																					_DatabaseFile[_KeyString],
																					_CommitString,
																					DataItemTuple[0],
																					DataItemTuple[1]
																				)
		,_DictatingDict.items()
		)
SYS.setDatabaseFileWithCommitStringAndKeyStringAndDictatingDict=setDatabaseFileWithCommitStringAndKeyStringAndDictatingDict

def setDatabaseFileWithCommitStringAndDictatingDict(_DatabaseFile,_CommitString,_DictatingDict):

	#Get the DictedDataDict
	DictedDataDict=getDictatingDict(_DictatingDict)

	#Get the IdString
	IdString=getIdStringWithDict(_DictatingDict)

	#Create a new group if IdString is not there
	if IdString not in _DatabaseFile.keys():

		#Create the group
		_DatabaseFile.create_group(IdString)

	#Set deeper in the Data
	map(
		lambda DataItemTuple:
		#Dict Case with a KeyString
		setDatabaseFileWithCommitStringAndKeyStringAndDictatingDict(_DatabaseFile[IdString],_CommitString,DataItemTuple[0],DataItemTuple[1]) 
		if type(DataItemTuple[1])==dict
		else 
			#Metadata case
			setDatabaseFileWithAttributeKeyStringAndAttributeVariable(_DatabaseFile[IdString],DataItemTuple[0],DataItemTuple[1])
			if DataItemTuple[0][0] in ["*","$"]
			else
			#Data case
			setDatabaseFileWithCommitStringAndGroupKeyStringAndDataSetVariable(_DatabaseFile[IdString],_CommitString,DataItemTuple[0],DataItemTuple[1])
		,_DictatingDict.items()
		)
SYS.setDatabaseFileWithCommitStringAndDictatingDict=setDatabaseFileWithCommitStringAndDictatingDict

def getDataDictWithDictAndIndexInt(_Dict,_IndexInt):

	#Check that this is an iterating object
	if hasattr(_Dict,"items"):

		return dict(
						map(
							lambda ItemTuple:
							#DataCase
							(
								ItemTuple[0],

								filter(
									lambda KeyStringAndValueVariable:
									(int)(KeyStringAndValueVariable[0].split("_")[0])==_IndexInt,
									ItemTuple[1].items()
									)[0][1] if len(filter(
													lambda KeyStringAndValueVariable:
													(int)(KeyStringAndValueVariable[0].split("_")[0])==_IndexInt,
													ItemTuple[1].items()
													))>0 else None
							)
							if ItemTuple[0][0]=="&"
							else
								#MetaData Case or Node Case
								(ItemTuple[0],ItemTuple[1])
								if ItemTuple[0][0] in ["*","$"]
								else
									(ItemTuple[0],getDataDictWithDictAndIndexInt(ItemTuple[1],_IndexInt))
							,_Dict.items()
						)
					)
	else:

		return {}
SYS.getDataDictWithDictAndIndexInt=getDataDictWithDictAndIndexInt
#</DefineFunctions>

#<DefineClass>
class H5pyFileClass(SYS.FileClass):

	#<DefineHookMethods>
	def initAfter(self):
		
		#<DefineSpecificDict>
		self.DictatingDict={}
		self.CommitString=""
		self.ModeString="a"
		self['ExtensionString']=".hdf5"
		#</DefineSpecificDict>


	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindDictatingDictAfter(self):
		self.DictatingDict=SYS.getDictatingDict(self.DictatingDict)

	def bindFilePathStringAfter(self):
		
		#Bind with setFilePointer
		self.setFilePointer()

	def bindModeStringAfter(self):

		#Bind with setFilePointer
		self.setFilePointer()
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def commit(self,_CommitString,_DictatingDict):
		
		#Bind with CommitString setting
		self['CommitString']=_CommitString

		#Bind with DictatingDict setting
		self['DictatingDict']=_DictatingDict

	def get(self,_PathString):

		#Bind with ModeString setting
		if SYS.sys.modules['os'].path.isfile(self.FilePathString):

			#Bind with ModeString setting
			self['ModeString']="r"

			#Read the Database
			return h5py.File(self.FilePathString, "r")[_PathString]

	def push(self):

		#Bind with ModeString setting
		if SYS.sys.modules['os'].path.isfile(self.FilePathString):

			self['ModeString']="a"

		else:

			self['ModeString']="w"

		if self.FilePointer!=None:

			#Get the DictedDataDict
			SYS.setDatabaseFileWithCommitStringAndDictatingDict(self.FilePointer,self.CommitString,self.DictatingDict)

	def delete(self,_PathString):

		if SYS.sys.modules['os'].path.isfile(self.FilePathString):

			#Bind with ModeString setting
			self['ModeString']="a"

			#Delete 
			if _PathString in self.FilePointer:
				del self.FilePointer[_PathString]

	def setFilePointer(self):

		#Open the File Pointer
		self['FilePointer']=h5py.File(self.FilePathString,self.ModeString)

	def getDataDictWithIndexInt(self,_IndexInt):
		return getDataDictWithDictAndIndexInt(self.FilePointer,_IndexInt)


	#</DefineMethods>

#</DefineClass>

