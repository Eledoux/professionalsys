#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
from tables import *
import time
import operator
import os
#</ImportModules>

#<DefineLocals>
HookString="Mul"
#</DefineLocals>

#<DefineClass>
class MulClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.FirstInt=0
		self.SecondInt=0
		self.MulInt=0
		#</DefineSpecificDict>

		#Define the features
		self.FeaturingTuplesList=[
										#ColumnString 	#GettingVariable 	#Col 			#VariablesList
										('FirstInt',	'FirstInt',		Int64Col(),		[0,1]),
										('SecondInt',	'SecondInt',	Int64Col(),		[1])
									]

		#Define the outputs
		self.OutputingTuplesList=[
										#ColumnString 	#GettingVariable 	#Col 
										('MulInt',		'MulInt',			Int64Col())
									]
						
	def outputAfter(self,**_LocalOutputingVariablesDict):
		#Bind with MulIntsList setting
		self.MulInt=self.FirstInt*self.SecondInt
	#</DefineHookMethods>
		
#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.MulClass()

def attest_output():

	#Append a default output
	SYS.Config.seed(5)

	#Return the object __repr__
	TestedVariable=SYS.MulClass().output()
	print(TestedVariable)
	return TestedVariable

def attest_store():

	#Flush the default output
	SYS.Config.seed(5)
	Mul=SYS.MulClass().organize().output().store().output().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Mul.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

def attest_scan():

	#Flush with all the scanning featuring values
	Mul=SYS.MulClass().organize().scan().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Mul.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

#</DefineAttestingFunctions>
