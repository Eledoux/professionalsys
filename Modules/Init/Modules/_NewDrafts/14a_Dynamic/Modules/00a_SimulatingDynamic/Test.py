#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

SimulatingDynamic=SYS.SimulatingDynamicClass().__setitem__(
                    'TestingAttestFunctionStringsList',
                    [
                      #'attest_default', 
                      'attest_output',
                      #'attest_store',
                      #'attest_scan'
                    ]
                  )
#SimulatingDynamic.attest()
#SimulatingDynamic.test()
SimulatingDynamic.output().plotSimulation()