#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
from tables import *
import time
import operator
import os
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class SuperDynamicClass(
						SYS.PerturbatingDynamicClass,
						SYS.SimulatingDynamicClass
					):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>

		#<DefineFeatures>
		#</DefineFeatures>

		#<DefineUtilities>
		#</DefineUtilities>

		#<DefineOutputs>
		#</DefineOutputs>

		#</DefineSpecificDict>

		#<DefineGroupedDict>
		#</DefineGroupedDict>


		#Display just what we want
		self.RepresentingKeyVariablesList = [
												#'GroupedOrderedDict'
											]

		#Define the features
		self.FeaturingTuplesList = [
										#ColumnString 	#GettingVariable 	#Col 			#VariablesList

									]

		#Define the outputs
		self.OutputingTuplesList = [
									#ColumnString 		#GettingVariable 		#Col 
								]
						

	def outputAfter(self,**_LocalOutputingVariablesDict):
		pass

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>	

#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.SuperDynamicClass()

def attest_output():

	#Append a default output
	SYS.Config.seed(5)

	#Return the object __repr__
	TestedVariable=SYS.SuperDynamicClass().output()
	print(TestedVariable)
	return TestedVariable

def attest_store():

	#Flush the default output
	SYS.Config.seed(5)
	SuperDynamic=SYS.SuperDynamicClass().organize().output().store().output().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+SuperDynamic.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

def attest_scan():

	#Flush with all the scanning featuring values
	Dynamic=SYS.MulClass().organize().scan().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Mul.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

#</DefineAttestingFunctions>
