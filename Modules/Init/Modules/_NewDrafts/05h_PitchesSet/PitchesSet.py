#<ImportModules>
import ShareYourSystem as SYS
from tables import *
#</ImportModules>

#<DefineLocals>
HookString="PitchesSet"
#</DefineLocals>

#<DefineClass>
class PitchesSetClass(
					SYS.ConfigClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.NotesInt=3
		self.PitchesInt=7
		self.PitchBoolsListsList=[]
		#</DefineSpecificDict>

		#Bind with TabularingTuplesList setting
		#Set the TabularingTuplesList
		self.apply('tabular',
				[
					{
						'TabularingTypeString':HookString+'ModeledTable',
						'FeaturingTuplesList':
									[
										#ColumnString 	#GettingVariable 	#Col 			#VariablesList
										('PitchesInt',	'PitchesInt',		Int64Col(),		[12]),
										('NotesInt',	'NotesInt',			Int64Col(),		range(1,12)),	
									]
					},
					{	
						'TabularingTypeString':HookString+'UsedTable',
						'FeaturingTuplesList':self['Tab_UseModeledTable']['FeaturedTuplesList'],
						'OutputingTuplesList':
									[
										#ColumnString 	#GettingVariable 	#Col 
										('PitchBoolsListsList','PitchBoolsListsList',Int32Col())
										
									]
					}
				]
			)
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindPitchesIntAfter(self):

		self.setUsedTable()

	def bindNotesIntAfter(self):

		self.setUsedTable()

	def setUsedTable(self):

		print(self.PitchesInt,self.NotesInt)
		self.PitchBoolsListsList=SYS.getFilteredPitchBoolsListsListWithPitchesIntAndNotesInt(
													self.PitchesInt,
													self.NotesInt
											)



		self.TabularedDictsList[-1]={	
											'TabularingTypeString':HookString+'UsedTable',
											'FeaturingTuplesList':self['Tab_UseModeledTable']['FeaturedTuplesList'],
											'OutputingTuplesList':
														[
															#ColumnString 	#GettingVariable 	#Col 
															('PitchBoolsListsList','PitchBoolsListsList',Int32Col(shape=(len(self.PitchBoolsListsList),self.PitchesInt)))
															
														]
						}


	def outputAfter(self,_LocalCallVariable,_LocalCallingVariablesList):

		pass
		
	#<DefineBindingHookMethods>

#</DefineClass>

#<DefineModelClass>

#</DefineModelClass>
