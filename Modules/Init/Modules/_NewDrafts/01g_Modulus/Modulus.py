#<ImportModules>
import ShareYourSystem as SYS
import scipy.stats
import numpy as np
from tables import *
import os
#</ImportModules>

#<DefineClass>
class ModulusClass(
						SYS.ObjectClass
					):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.ModulusFloat=0.
		self.PowerFloat=0.5
		self['App_Group_XMul']=SYS.MulClass().__setitem__('FeaturingTuplesList',[
											('FirstInt',	'FirstInt',		Int64Col(),	[0,1]),
											('SecondInt',	'SecondInt',	Int64Col(),	[1]),
											])
		self['App_Group_YMul']=SYS.MulClass().__setitem__('FeaturingTuplesList',[
											('FirstInt',	'FirstInt',		Int64Col(),	[0,1]),
											('SecondInt',	'SecondInt',	Int64Col(),	[0,1]),
											])
		#</DefineSpecificDict>

		#Define the features
		self.FeaturingTuplesList=[
									#ColumnString 	#GettingVariable 	#Col 			#VariablesList
									('PowerFloat',	'PowerFloat',		Float32Col(),	[0.5,1.]),
								]

		#Define the outputs
		self.OutputingTuplesList=[
										#ColumnString 	#GettingVariable 		#Col 
										('ModulusFloat',	'ModulusFloat',		Float32Col()),
									]

	def outputAfter(self,**_LocalOutputingDict):
		
		#Bind with ModuleFloat Setting
		self.ModulusFloat=np.power(
						self['App_Group_XMul'].MulInt**2+self['App_Group_YMul'].MulInt**2,
					self.PowerFloat
					)
	#</DefineHookMethods>
	
#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.ModulusClass()

def attest_output():

	#Append a default output
	SYS.Config.seed(5)

	#Return the object __repr__
	TestedVariable=SYS.ModulusClass().output()
	print(TestedVariable)
	return TestedVariable

def attest_store():

	#Flush the default output
	SYS.Config.seed(5)
	Modulus=SYS.ModulusClass().organize().output().store().output().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Modulus.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

def attest_scan():

	#Flush with all the scanning featuring values
	Modulus=SYS.ModulusClass().organize().scan().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Modulus.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

#</DefineAttestingFunctions>