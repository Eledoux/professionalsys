/                        Group
/ComplexMul              Group
/ComplexMul/ComplexMulFeaturedTable Dataset {2/Inf}
    Data:
        (0) {MaxFloat=75, MinFloat=-100, ModeledInt=0},
        (1) {MaxFloat=75, MinFloat=-50, ModeledInt=1}
/ComplexMul/ComplexMulOutputedTable Dataset {4/Inf}
    Data:
        (0) {ConfigFeaturedModeledInt=0, FeaturedModeledInt=0, ModeledInt=0, 
        (0)  MulInt=1122, MulIntsList=[-22,-51], SeedMulInt=548},
        (1) {ConfigFeaturedModeledInt=1, FeaturedModeledInt=0, ModeledInt=1, 
        (1)  MulInt=1122, MulIntsList=[-22,-51], SeedMulInt=548},
        (2) {ConfigFeaturedModeledInt=0, FeaturedModeledInt=1, ModeledInt=2, 
        (2)  MulInt=-90, MulIntsList=[6,-15], SeedMulInt=548},
        (3) {ConfigFeaturedModeledInt=1, FeaturedModeledInt=1, ModeledInt=3, 
        (3)  MulInt=-90, MulIntsList=[6,-15], SeedMulInt=548}
/ComplexMul/Config       Group
/ComplexMul/Config/ConfigFeaturedTable Dataset {2/Inf}
    Data:
        (0) {AuthorString="Erwan Ledoux", ModeledInt=0, UseInt=1},
        (1) {AuthorString="Erwan Ledoux", ModeledInt=1, UseInt=2}
/ComplexMul/Config/ConfigOutputedTable Dataset {2/Inf}
    Data:
        (0) {FeaturedModeledInt=0, ModeledInt=0, SeedInt=548},
        (1) {FeaturedModeledInt=1, ModeledInt=1, SeedInt=548}
/FeaturedTable           Dataset {1/Inf}
    Data:
        (0) {ModeledInt=0, PowerFloat=0.5}
/OutputedTable           Dataset {4/Inf}
    Data:
        (0) {ComplexMulFeaturedModeledInt=0, FeaturedModeledInt=0, 
        (0)  ModeledInt=0, ModulusFloat=1359.67, ModulusIntsList=[-768,1122], 
        (0)  RealMulFeaturedModeledInt=0},
        (1) {ComplexMulFeaturedModeledInt=1, FeaturedModeledInt=0, 
        (1)  ModeledInt=1, ModulusFloat=773.255, ModulusIntsList=[-768,-90], 
        (1)  RealMulFeaturedModeledInt=0},
        (2) {ComplexMulFeaturedModeledInt=0, FeaturedModeledInt=0, 
        (2)  ModeledInt=2, ModulusFloat=1135.72, ModulusIntsList=[176,1122], 
        (2)  RealMulFeaturedModeledInt=1},
        (3) {ComplexMulFeaturedModeledInt=1, FeaturedModeledInt=0, 
        (3)  ModeledInt=3, ModulusFloat=197.676, ModulusIntsList=[176,-90], 
        (3)  RealMulFeaturedModeledInt=1}
/RealMul                 Group
/RealMul/Config          Group
/RealMul/Config/ConfigFeaturedTable Dataset {2/Inf}
    Data:
        (0) {AuthorString="Erwan Ledoux", ModeledInt=0, UseInt=1},
        (1) {AuthorString="Erwan Ledoux", ModeledInt=1, UseInt=2}
/RealMul/Config/ConfigOutputedTable Dataset {2/Inf}
    Data:
        (0) {FeaturedModeledInt=0, ModeledInt=0, SeedInt=275},
        (1) {FeaturedModeledInt=1, ModeledInt=1, SeedInt=548}
/RealMul/RealMulFeaturedTable Dataset {2/Inf}
    Data:
        (0) {MaxFloat=75, MinFloat=-100, ModeledInt=0},
        (1) {MaxFloat=75, MinFloat=-50, ModeledInt=1}
/RealMul/RealMulOutputedTable Dataset {4/Inf}
    Data:
        (0) {ConfigFeaturedModeledInt=0, FeaturedModeledInt=0, ModeledInt=0, 
        (0)  MulInt=-768, MulIntsList=[-24,32], SeedMulInt=275},
        (1) {ConfigFeaturedModeledInt=1, FeaturedModeledInt=0, ModeledInt=1, 
        (1)  MulInt=1122, MulIntsList=[-22,-51], SeedMulInt=548},
        (2) {ConfigFeaturedModeledInt=0, FeaturedModeledInt=1, ModeledInt=2, 
        (2)  MulInt=176, MulIntsList=[4,44], SeedMulInt=275},
        (3) {ConfigFeaturedModeledInt=1, FeaturedModeledInt=1, ModeledInt=3, 
        (3)  MulInt=-90, MulIntsList=[6,-15], SeedMulInt=548}
