#<ImportModules>
import collections
import copy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
TablingString='xx'
BasingLocalTypeString="Tabularer"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class TablorClass(
					BasedLocalClass,
				):
	
	#<DefineHookMethods>
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.TabledKeyString="" 	#<NotRepresented>
		self.TabledInt=-1 			#<NotRepresented>
		self.TabledTable=None 		#<NotRepresented>
		#</DefineSpecificDict>

	@SYS.SwitcherClass()
	@SYS.HookerClass(**{'AfterTuplesList':[("<TypeString>","tabular")]})
	def table(self,**_VariablesDict):

		#Debug
		self.debug('Start of the method')
		
		#Debug
		self.debug(
					[
						'We are going to look if this is a new table or not...In order to index it',
						('self.',self,[
											'ModeledKeyString',
											'TabularedKeyStringsList',
											'TabularedSuffixString',
											'TabledKeyString'
										])
					]
				)

		#Get the suffix strings of all the tables and their index
		UnzipList=SYS.unzip(map(
				lambda __StringsList:
				(
					__StringsList[1],
					#SYS.getCommonSuffixStringWithStringsList(
					#	[
					TablingString.join(__StringsList[2:]),
					#		SuffixString
					#	]
					#)
				),
				map(
						lambda __TabledKeyString:
						__TabledKeyString.split(TablingString),
						self.TabularedKeyStringsList
					)
			),[0,1])

		#Unpack if it is possible
		if len(UnzipList)>0:

			#Unpack
			[TabledIntsTuple,TabledSuffixStringsList]=UnzipList

			#Debug
			self.debug(
						[
							'There are already some tables',
							'TabledSuffixStringsList is '+str(TabledSuffixStringsList),
							"self.TabularedSuffixString is "+str(
								self.TabularedSuffixString)
						]
					)

			if self.TabularedSuffixString not in TabledSuffixStringsList:

				#Increment the IndexString
				IndexInt=max(map(int,TabledIntsTuple))+1

				#Stringify
				IndexString=str(IndexInt)

				#Debug
				self.debug('IndexString of this new table is '+str(IndexString))
				
			else:

				#Get the already setted one
				IndexString=self.TabularedKeyStringsList[
					TabledSuffixStringsList.index(self.TabularedSuffixString)
					].split(TablingString)[1]

				#Intify
				IndexInt=(int)(IndexString)

				#Debug
				self.debug('IndexString of this not new table is '+str(IndexString))

		else:

			#Debug
			self.debug('There are no tables here')

			#Set to empty lists 
			[TabledIntsTuple,TabledSuffixStringsList]=[[],[]]

			#Init the list
			IndexInt=0

			#Stringify
			IndexString="0"

		#Bind with TabledKeyString setting
		self.TabledKeyString=TablingString+IndexString+TablingString+self.TabularedSuffixString

		#Set the TabularedInt
		self.TabledInt=IndexInt

		#Debug
		self.debug("self.TabledKeyString is "+str(self.TabledKeyString))

		#Debug
		self.debug(
					[
						'Here we create the table or get it depending if it is new or not',
						'self.TabledKeyString is '+self.TabledKeyString
					]
				)

		#Check
		if self.TabledKeyString!="":

			#Debug
			self.debug(
						[
							('self.',self,['TabledKeyString','TabularedKeyStringsList'])
						]
					)

			#Create the Table if not already
			if self.TabledKeyString not in self.TabularedKeyStringsList:

				#Debug
				self.debug(
							[
								'The table not exists',
							]
						)

				#Create the Table in the hdf5
				self.TabledTable=self.HdformatedFilePointer.create_table(
											self.TabularedGroup,
											self.TabledKeyString,
											self.ModeledClass,
											self.ModeledClass.__doc__ 
											if self.ModeledClass.__doc__!=None 
											else "This is the "+self.ModeledClass.__name__
										)

				#Append in the self.ModeledDict['TabularedKeyStringsList']
				self.TabularedKeyStringsList.append(self.TabledKeyString)

			else:

				#Debug
				self.debug(
								[
									'The table exists',
									"self.TabularedGroup is "+str(self.TabularedGroup)
								]
							)

				#Else just get it 
				self.TabledTable=self.TabularedGroup._f_getChild(self.TabledKeyString)

			#Set the in the TablesOrderedDict
			self.TabularedOrderedDict[self.TabledKeyString]=self.TabledTable

			#Debug
			self.debug("self.TabularedOrderedDict is "+str(self.TabularedOrderedDict))
		
		#Debug
		self.debug('End of the method')

#</DefineClass>

#<DefineAttestingFunctions>
def attest_table():
	Tablor=SYS.TablorClass().update(								
									[
										('MyInt',0),
										('MyString',"hello"),
										('MyIntsList',[2,4,1]),
										('ModelingColumnTuplesList',
											[
												('MyInt',tables.Int64Col()),
												('MyString',tables.StringCol(10)),
												('MyIntsList',(tables.Int64Col(shape=3)))
											]
										)
									]).table().close()
																
	#Get the h5ls version of the stored hdf
	return SYS.represent(
						Tablor
						)+'\n\n\n\n'+Tablor.hdfview().HdformatedString	
#</DefineAttestingFunctions>
