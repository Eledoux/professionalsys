#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
from tables import *
import time
import operator
import os
#</ImportModules>

#<DefineLocals>
HookString="Mul"
#</DefineLocals>

#<DefineClass>
class MulClass(SYS.JoinerClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.FirstInt=1
		self.SecondInt=1
		self.MulInt=1
		#</DefineSpecificDict>

		#Define the features
		self['App_Model_ParameterizingOrderedDict']={

										'ColumningTuplesList':
										[
											#ColumnString 	#Col 	
											('FirstInt',	Int64Col())
											('SecondInt',	Int64Col())
										]
									}

		#Define the outputs
		self.OutputingTuplesList=[
										#ColumnString 	#GettingVariable 	#Col 
										('MulInt',		'MulInt',			Int64Col()),	
										('MulIntsList',	'MulIntsList',		Int64Col(shape=2)),
										('SeedMulInt',	'SeedMulInt',		Int64Col())	
									]

		#Define the stores
		self.StoringTuplesList=[
									('MulIntsListArrayString',	'MulIntsList',	StringCol(100))
								]
						
	def outputAfter(self,**_LocalOutputingVariablesDict):
		
		#Set 
		self.SeedMulInt=self['App_Group_Config'].SeedInt

		#Define the RandomFloatsList
		RandomFloatsList=scipy.stats.uniform.rvs(size=2)

		#Bind with MulIntsList setting
		self['MulIntsList']=map(
									int,
									scipy.floor(
										self.MinFloat+(self.MaxFloat-self.MinFloat)*RandomFloatsList
										)
								)

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindMulIntsListAfter(self):

		#Bind with MulInt setting
		self['MulInt']=reduce(operator.__mul__,self.MulIntsList)
	#</DefineBindingHookMethods>	
#</DefineClass>

#<DefineAttestingFunctions>
def attest_default():
	return SYS.MulClass()

def attest_output():

	#Append a default output
	SYS.Config.seed(5)

	#Return the object __repr__
	TestedVariable=SYS.MulClass().output()
	print(TestedVariable)
	return TestedVariable

def attest_store():

	#Flush the default output
	SYS.Config.seed(5)
	Mul=SYS.MulClass().organize().output().store().output().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Mul.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

def attest_scan():

	#Flush with all the scanning featuring values
	Mul=SYS.MulClass().organize().scan().close()

	#Return the shape of the storing hdf5
	TestedVariable=os.popen('h5ls -dlr '+Mul.GroupingFilePathString).read()
	print(TestedVariable)
	return TestedVariable

#</DefineAttestingFunctions>
