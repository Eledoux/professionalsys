#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import scipy.stats
from tables import *
import time
import operator
import os
#</ImportModules>

#<DefineLocals>
HookString="Mul"
#</DefineLocals>

#<DefineClass>
class MulClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.FirstInt=1
		self.SecondInt=1
		self.MulInt=1
		#</DefineSpecificDict>

		#Define the databases
		self['App_Model_ParameterDatabase']=SYS.DatabaseClass().update(
										[
											('ModelingColumnTuplesList',
												[
													#ColumnString 	#Col 	
													('FirstInt',	Int64Col()),
													('SecondInt',	Int64Col())
												]
											),
											('IsFeaturingBool',True),
											('ScanningTuplesList',
												[
													('FirstInt',[1,2]),
													('SecondInt',[5,6])
												]
											)
										]
									)

		#Define the outputs
		self['App_Model_ResultDatabase']=SYS.DatabaseClass().update(
										[
											('ModelingColumnTuplesList',
												[
													#ColumnString 		#Col 	
													('IntsList', 		Int64Col(shape=2)),
													('MulInt',			Int64Col())
												]
											),
											('JoiningModelString',"Parameter")
										]
									)
						
	def outputAfter(self,**_LocalOutputingVariablesDict):
			
		#Set the IntsList useful to track the parameters
		self.IntsList=[self.FirstInt,self.SecondInt]

		#Set the MulInt
		self.MulInt=self.FirstInt*self.SecondInt

	#</DefineHookMethods>
	
#</DefineClass>

#<DefineAttestingFunctions>
def attest_model():
	Mul=SYS.MulClass()['App_Model_ParameterDatabase'].model()

def attest_flush():

	#Flush the default output
	Mul=SYS.MulClass(
		).__setitem__('FirstInt',2
		).flush('Result'
		).__setitem__('FirstInt',3
		).flush(
		).__setitem__('SecondInt',2
		).flush(
		).__setitem__('SecondInt',3
		).flush(
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Mul
		)+'\n\n\n'+SYS.represent(
				Mul.hdfview().HdformatedString
			)

def attest_retrieve():
	Mul=SYS.MulClass(
		).__setitem__('/App_Model_ResultingDict/RetrievingTuple',(0,3)
		).retrieve('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Mul
		)
	
def attest_find():
	Mul=SYS.MulClass(
		).update(
					[
						('/App_Model_ResultingDict/FindingTuplesList',
											[
												('MulInt',(operator.gt,2))
											]),
						('/App_Model_ParameterizingDict/FindingTuplesList',
											[
												('SecondInt',(operator.gt,2))
											])
					]
		).find('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Mul
		)
	
def attest_recover():

	#Flush the default output
	Mul=SYS.MulClass(
		).update(
					[
						('/App_Model_ResultingDict/FindingTuplesList',
											[
												('MulInt',(operator.gt,2))
											]),
						('/App_Model_ParameterizingDict/FindingTuplesList',
											[
												('SecondInt',(operator.gt,2))
											])
					]
		).recover('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Mul
		)

def attest_scan():

	#Scan
	Mul=SYS.MulClass(
		).scan('Result'
		).close()

	#Return the object and the h5py
	return "\n\n\n\n"+SYS.represent(
			Mul
		)+'\n\n\n'+SYS.represent(
				Mul.hdfview().HdfFormatedString
			)
#</DefineAttestingFunctions>
