#<ImportModules>
import collections
import copy
import os
import ShareYourSystem as SYS
import six
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Tester"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class GetterClass(BasedLocalClass):
		
	@SYS.HookerClass(**{'AfterTuplesList':[(BasingLocalTypeString,"init")]})
	def init(self,**_VariablesDict):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""		

		#<DefineSpecificDict>
		self.GettingKeyVariable=None 				#<NotRepresented>
		self.GettedValueVariable=None  				#<NotRepresented>
		#</DefineSpecificDict>

	def __getitem__(self,_GettingKeyVariable):
		"""Call the get<HookString> methods and return self.GettedValueVariable (None by default)"""
		
		#Refresh the attributes
		self.GettingKeyVariable=_GettingKeyVariable
		self.GettedValueVariable=None

		#Get
		self.get()

		#Return GettedValueVariable
		return self.GettedValueVariable

	def get(self,**_VariablesDict):
		""" """

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Debug
		'''
		self.debug(("self.",self,['GettingKeyVariable']))
		'''
		
		if type(self.GettingKeyVariable) in [str,unicode]:

			#Get safely the Value
			if self.GettingKeyVariable in self.__dict__:

				#__getitem__ in the __dict__
				self.GettedValueVariable=self.__dict__[self.GettingKeyVariable]

				#Stop the getting
				_VariablesDict['IsCallingBool']=False

		#Debug
		'''
		self.debug('Start of the method')
		'''

#</DefineClass>

#<DefineAttestingFunctions>
def attest_get():

	#Define a hooking Class
	class TaggingGetterClass(GetterClass):

		@SYS.HookerClass(**{'BeforeTypeString':"Getter"})
		def get(self,**_VariablesDict):

			#Add to the GettingVariable
			self.GettingKeyVariable='My'+str(self.GettingKeyVariable)

	#Define a TaggingGetter
	TaggingGetter=TaggingGetterClass()
	TaggingGetter.MyInt=1
	
	#Set with a pointer short string
	return TaggingGetter['Int']
#</DefineAttestingFunctions>

