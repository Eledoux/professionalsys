#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Test"
#</DefineLocals>

#<DefineClass>
class TesterClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		
		self.TestedVariable=None
		self.TestedString=""
		self.IsPrintedBool=False
		self.TestingClass=None
		self.TestingString=""
		self.TestingFolderPathString=SYS.getCurrentFolderPathString()+'Tests/'
		self.TestingTypeString=SYS.getTypeStringWithClassString(self.__class__.__name__).split('Tester')[0]
		self.TestedClass=getattr(SYS,SYS.getClassStringWithTypeString(self.TestingTypeString))
		#</DefineSpecificDict>
	
	def testAfter(self,_TestString):

		#Test
		self.setTestedStringWithTestString(_TestString)

		#Return self
		return self

	def testifyAfter(self,_TestString):
		
		#Test first
		self('test',_TestString)

		#Check that there is a Folder Tests
		if SYS.sys.modules['os'].path.isdir(self.TestingFolderPathString)==False:
			SYS.sys.modules['os'].popen('mkdir '+self.TestingFolderPathString)

		#Write
		File=open(self.TestingFolderPathString+_TestString+'.txt','w')
		File.write(self.TestedString)
		File.close()

	def boundAfter(self,_TestString):

		#Define the new test method to be bounded
		def test(_TestingVariable):

			#Get the AssertingString
			AssertingString=self.getAssertingString(_TestString)

			#Get the TestedString
			self('test',_TestString)

			#Print maybe
			if self.IsPrintedBool:
				print("#################")
				print("<"+_TestString+">")
				print("#################")
				print("")
				print('AssertingString is :')
				print(AssertingString)
				print("")

				#Print maybe
				print('AssertedString is :')
				print(self('test',_TestString).TestedString)
				print("")

			#Assert
			_TestingVariable.assertEqual(
					AssertingString,
					self('test',_TestString).TestedString
					)

		test.__name__='test_'+self.TestingTypeString+'_'+_TestString.split('test_')[1]
		setattr(self.TestingClass,test.__name__,test)

	#</DefineHookMethods>

	#<DefineMethods>
	def setUp(self):
		self.TestedVariable=None
		self.TestedString=""

	def setTestedStringWithTestString(self,_TestString):

		#Test the corresponding method
		getattr(self,_TestString)()

		#Set the TestedString
		SYS.PrintingDict['IsIdBool']=False
		self.TestedString=SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(self.TestedVariable,SYS.PrintingDict)
		SYS.PrintingDict['IsIdBool']=False

	def getAssertingString(self,_TestString):

		#Read
		File=open(self.TestingFolderPathString+_TestString+'.txt','r')
		return File.read()
	#</DefineMethods>

#</DefineClass>


