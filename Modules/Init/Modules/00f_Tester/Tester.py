#<ImportModules>
import collections
import copy
import inspect
import os
import ShareYourSystem as SYS
import sys
import unittest
#</ImportModules>

#<DefineLocals>
AttestingString='attest'
BasingLocalTypeString="Debugger"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#Define for each a test method to be bounded
def setUp(_Test):
	_Test.TestedPointer.TestedInt+=1
	_Test.TestedPointer.TestedVariable=None
	_Test.TestedPointer.TestedString=""

#<DefineClass>
class TesterClass(BasedLocalClass):

	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):

		#<DefineSpecificDict>
		self.TestingString=""
		self.TestingIsRepresentedBool=True
		if hasattr(getattr(SYS,self.__module__),'AttestingFunctionStringsList'):
			self.TestingAttestFunctionStringsList=copy.copy(
				getattr(SYS,self.__module__).AttestingFunctionStringsList)
		else:
			self.TestingAttestFunctionStringsList=[]
		self.TestingFolderPathString=SYS.getCurrentFolderPathString()+'Tests/'
		self.TestedClass=None
		self.TestedOrderedDict=collections.OrderedDict()
		self.TestedInt=-1
		self.TestedVariable=None
		self.TestedString=""
		#</DefineSpecificDict>

	def writeAttestWithTestingAttestFunction(self,_TestingAttestFunction):

		#Call the attest function to get the TestedVariable
		self.TestedVariable=_TestingAttestFunction()

		#Bind with TestedString setting
		SYS.RepresentingDict['IsIdBool']=False
		self.TestedString=SYS.getRepresentedVariableStringWithRepresentedVariableAndRepresentingDict(
			self.TestedVariable,SYS.RepresentingDict)
		SYS.RepresentingDict['IsIdBool']=False

		#Write the TestedString
		File=open(self.TestingFolderPathString+_TestingAttestFunction.__name__+'.txt','w')
		File.write(self.TestedString)
		File.close()

	def attest(self):

		#Check that there is a Folder Tests
		if os.path.isdir(self.TestingFolderPathString)==False:
			os.popen('mkdir '+self.TestingFolderPathString)
		else:
			os.popen("cd "+self.TestingFolderPathString+";rm *")

		#Get the TestingAttestFunctionsList
		Module=getattr(SYS,self.__module__)
		TestingAttestFunctionsList=map(
										lambda TestingAttestFunctionString:
										getattr(Module,TestingAttestFunctionString),
										self.TestingAttestFunctionStringsList
									)

		#Write the TestedString made by each function and append an equivalent test method into the test ordered dict
		map(
				lambda __TestingAttestFunction:
				self.writeAttestWithTestingAttestFunction(__TestingAttestFunction),
				TestingAttestFunctionsList
			)

		#Return self
		return self

	def setTestFunctionWithTestingAttestFunction(self,_TestingAttestFunction):

		#Define the TestString
		TestString='at'.join(_TestingAttestFunction.__name__.split('at')[1:])

		def test(_Test):

			#Define the TestingAttestFunction
			TestingAttestFunction=getattr(
									getattr(SYS,_Test.TestedPointer.__module__),
									_Test.TestedPointer.TestingAttestFunctionStringsList[_Test.TestedPointer.TestedInt]
								)

			#Get the AssertedString
			File=open(_Test.TestedPointer.TestingFolderPathString+TestingAttestFunction.__name__+'.txt','r')
			AssertingString=File.read()
			File.close()

			#Call the attest function to get the TestedVariable
			_Test.TestedPointer.TestedVariable=TestingAttestFunction()

			#Bind with TestedString setting
			SYS.RepresentingDict['IsIdBool']=False
			self.TestedString=SYS.getRepresentedVariableStringWithRepresentedVariableAndRepresentingDict(
				self.TestedVariable,SYS.RepresentingDict)
			SYS.RepresentingDict['IsIdBool']=False

			#Represent maybe
			if _Test.TestedPointer.TestingIsRepresentedBool:
				#print("\n###########################################")
				#print("")
				#print('AssertingString is :')
				#print(AssertingString)
				#print("")

				#Represent maybe
				print('AssertedString is :')
				print(_Test.TestedPointer.TestedString)
				print("")

			#Assert
			#print("a",AssertingString)
			#print("b",_Test.TestedPointer.TestedString)

			_Test.assertEqual(
					#1,1
					AssertingString,
					_Test.TestedPointer.TestedString
			)

		#Copy a form of the test function and name it differently
		test.__name__=TestString

		#Append in the Test OrderedDict
		self.TestedOrderedDict[test.__name__]=test

	def test(self):

		#Get the TestingAttestFunctionsList
		Module=getattr(SYS,self.__module__)
		TestingAttestFunctionsList=map(
										lambda TestingAttestFunctionString:
										getattr(Module,TestingAttestFunctionString),
										self.TestingAttestFunctionStringsList
									)

		#Set the tests for each asserting function
		map(
				lambda __TestingAttestFunction:
				self.setTestFunctionWithTestingAttestFunction(__TestingAttestFunction),
				TestingAttestFunctionsList
			)
			
		#Define the TestClass
		class TestClass(unittest.TestCase):				

			#Bind with the Tested object
			TestedPointer=self

			#Bound the setUp function
			locals().__setitem__(setUp.__name__,setUp)
			
			#Bound each testing function
			for TestedKeyString,TestedMethod in self.TestedOrderedDict.items():
				locals().__setitem__(TestedKeyString,TestedMethod)

			try:
				del TestedKeyString,TestedMethod
			except:
				pass

		#Give a name
		TestClass.__name__=SYS.getClassStringWithTypeString(self.__class__.TypeString+'Test')

		#Set to the TestedClass
		self.TestedClass=TestClass

		#Bound to the unittest runner
		TestLoader=unittest.TestLoader().loadTestsFromTestCase(self.TestedClass)
		unittest.TextTestRunner(verbosity=2).run(TestLoader)

#</DefineClass>

#<DefineAttestingFunctions>
def attest_test():

	#Build Hdf groups
	Tester=SYS.TesterClass()
	Tester.MyInt=0

	#Return the object
	print(Tester)
	return Tester
#</DefineAttestingFunctions>

