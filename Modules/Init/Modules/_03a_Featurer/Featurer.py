#<ImportModules>
import collections
import copy
import tables
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class FeaturerClass(
					SYS.FlusherClass
				):
	
	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.FeaturedOrderedDict=collections.OrderedDict #<NotRepresented>
		#</DefineSpecificDict>

	def rowBefore(self):
		pass

	def flushBefore(self):
		pass
	#</DefineHookMethods>

#</DefineClass>

#<DefineAttestingFunctions>
def attest_feature():

	#Build Hdf groups
	Featurer=SYS.FeaturerClass().hdformat().update(
								[
									(
										'App_Structure_ChildGrouper1',
										SYS.GrouperClass().update(
										[
											(
												'App_Structure_GrandChildTablor1',
												SYS.FlusherClass()
											)
										])
									)
								]	
							).walk(
										[
											'StructuredOrderedDict.items()'
										],
										**{
												'BeforeUpdatingTuplesList':
												[
													('parentize',{'ArgsVariable':"Structure"}),
													('group',{'ArgsVariable':"Structure"})
												]
											}
									).update(
								map(
										lambda __Tuple:
										(
											'/App_Structure_ChildGrouper1/App_Structure_GrandChildTablor1/'+__Tuple[0],
											__Tuple[1]
										),
										[
											('MyInt',0),
											('MyString',"hello"),
											('MyIntsList',[2,4,1]),
											('App_Model_FeaturingDict',
														{
															'ColumningTuplesList':
															[
																('MyInt',tables.Int64Col()),
																('MyString',tables.StringCol(10)),
																('MyIntsList',(tables.Int64Col(shape=3)))
															]
														}
											),
											('model',{'ArgsVariable':"Feature"}),
											('tabular',{'ArgsVariable':""}),
											('row',{'ArgsVariable':""}),
											('flush',{'ArgsVariable':""})
										]
									)
							).close()
																
	#Get the h5ls version of the stored hdf
	return SYS.represent(Featurer)+'\n\n\n'+os.popen('h5ls -dlr '+Featurer.HdformatingPathString).read()
	#</DefineMethods>

#</DefineAttestingFunctions>
