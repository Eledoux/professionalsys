#<ImportModules>
import collections
import copy
import inspect
import os
import ShareYourSystem as SYS
import sys
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Debugger"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class BinderClass(BasedLocalClass):

	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):
		
		#<DefineSpecificDict>
		self.BindingFunction=None
		self.BindingConditionTuplesList=[]
		self.BindingHookingString='After'
		self.BindedFunction=None
		self.BindedIsBool=False
		self.BindedIfFunction=None
		self.BindedIfFunctionString=""
		#<DefineSpecificDict>

		#Update with the Kwargs
		map(
				lambda __ItemTuple:
				self.__setattr__('Binding'+__ItemTuple[0],__ItemTuple[1]),
				_VariablesDict.iteritems()
			)

	def __call__(self,_BindingFunction):

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Set the  BindedFunction
		self.BindingFunction=_BindingFunction

		#Set the BindedFunctionString
		self.BindedFunctionString=_BindingFunction.__name__

		#Hook
		self.bind()

		#Debug
		'''
		self.debug('End of the method')
		'''

		#Return the BindedFunction
		return self.BindedFunction

	def bind(self):

		#Debug
		'''
		self.debug('Start of the method')
		'''

		#Define the big binding wrapped method
		def BindedFunction(*_VariablesList,**_VariablesDict):

			#Debug
			'''
			self.debug('Start of the method')
			'''

			#Alias
			InstanceVariable=_VariablesList[0]

			#Hook at the level of the class if it was not already
			if self.BindedIsBool==False:

				#Define the binding methods
				def bindIfBool(*_BindingVariablesList,**_BindingVariablesDict):

					#Debug
					'''
					self.debug('Start of the method')
					'''

					#Alias
					BindedInstanceVariable=_BindingVariablesList[0]

					#Debug
					'''
					self.debug(
								[
									('self.BindingConditionTuplesList is '+str(self.BindingConditionTuplesList)),
									('picked values are '+str(map(
											lambda __BindingTuple:
											BindedInstanceVariable[__BindingTuple[0]],
											self.BindingConditionTuplesList
											)
										)
									)
								]
							)
					'''

					#Check if we have to bind
					BindedAllBoolsList=map(
											lambda __BindingTuple:
											__BindingTuple[1][0](
												BindedInstanceVariable[__BindingTuple[0]],
												__BindingTuple[1][1]
											),
											self.BindingConditionTuplesList
										)
									

					#Debug
					'''
					self.debug('BindedAllBoolsList is '+str(BindedAllBoolsList))
					'''

					#Check
					if all(BindedAllBoolsList):

						#Debug
						'''
						self.debug(
									[
										('Call the binding method'),
										('self.BindingFunction is '+str(self.BindingFunction))
									]
								)
						'''

						#Call the BindingFunction
						self.BindingFunction(*_VariablesList,**_VariablesDict)

						#Debug
						'''
						self.debug('Ok it was called')
						'''

				#Debug
				'''
				self.debug("Hook at the level of the class")
				'''

				#Hook
				self.BindedIfFunction=bindIfBool
				self.BindedIfFunctionString='bind'+str(self.BindingConditionTuplesList)
				setattr(
							InstanceVariable.__class__,
							self.BindedIfFunctionString,
							self.BindedIfFunction
						)
				InstanceVariable.__class__.set.HookerPointer.HookedIsBool=False
				getattr(
							InstanceVariable.__class__.set.HookerPointer,
							'Hooking'+('After' if self.BindingHookingString=='Before' else ('Before' if self.BindingHookingString=='After' else '') ) +'TuplesList'
						).append(
									(InstanceVariable.__class__,self.BindedIfFunction)
								)

				#Say True now
				self.BindedIsBool=True

			#Call the BindingFunction
			self.BindingFunction(*_VariablesList,**_VariablesDict)

			#Debug
			'''
			self.debug('End of the method')
			'''

			#Return self for the wrapped method call
			return InstanceVariable

		#Give a Pointer to the Binder at the BindedFunction
		BindedFunction.BinderPointer=self

		#Define a represent function for the hooked function
		def represent():
			RepresentedString=inspect.getmodule(self.BindingFunction
					).__name__+'.Binded_'+self.BindingFunction.__repr__()
			return RepresentedString
		BindedFunction.__repr__=represent
			
		#Set
		self.BindedFunction=BindedFunction

		#Debug
		'''
		self.debug('End of the method')
		'''

		#Return self
		return self

#</DefineClass>

