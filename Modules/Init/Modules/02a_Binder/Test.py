#<ImportModules>
import ShareYourSystem as SYS
import operator
#</ImportModules>

class DoerClass(SYS.NoderClass):

	@SYS.BinderClass(**{'ConditionTuplesList':[('SettingKeyVariable',(operator.eq,"MyInt"))]})
	def setMyInt(self,**_VariablesDict):
		
		#Print
		self.debug(
					[
						('I increment MyInt !'),
						('MyInt is before '+str(self.MyInt))
					]
				)

		#Increment
		self.MyInt+=1

		#Print
		self.debug(
					[
						('MyInt is now '+str(self.MyInt))
					]
				)

#Get an instance and set a default MyInt
print('')
print("Get an instance and set a default MyInt")
MyDoer=DoerClass().__setitem__('MyInt',3)

#Caa a first time the binding method
print('')
print("Call a first time the binding method")
MyDoer.setMyInt()
print('MyDoer is now')
print(MyDoer)

#Call a setting that not bind
print('')
print('Call a setting that not bind')
MyDoer['MySecondInt']=3

#Call a setting that not bind
print('')
print('Call a setting that binds')
MyDoer['MyInt']=4

#Print finally
print('MyDoer is finally')
print(MyDoer)

#Set a binding method externally of the definition of the class
def setMyString(_InstanceVariable):
	_InstanceVariable.MyString='MyInt is equal to '+str(_InstanceVariable.MyInt)+' here'
MyDoer.__class__.setMyString=SYS.BinderClass(
								**{'BindingTuplesList':[('SettingKeyVariable',(operator.eq,"MyInt"))]}
								)(setMyString)
MyDoer.setMyString()

#Call a setting that not bind
print('')
print('Call a setting that binds')
MyDoer['MyInt']=6

#Print finally
print('MyDoer is finally')
print(MyDoer)

