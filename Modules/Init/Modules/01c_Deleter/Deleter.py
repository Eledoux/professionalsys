#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
BasingLocalTypeString="Setter"
BasedLocalClass=getattr(SYS,SYS.getClassStringWithTypeString(BasingLocalTypeString))
#</DefineLocals>

#<DefineClass>
class DeleterClass(BasedLocalClass):
		
	@SYS.HookerClass(**{'AfterTuplesList':[(BasedLocalClass,"init")]})
	def init(self,**_VariablesDict):
		"""Init all the attributes for the hooking methods and call the init<HookString> methods."""		

		#<DefineSpecificDict>
		self.SettingKeyVariable=None 					#<NotRepresented>
		self.SettingValueVariable=None  				#<NotRepresented>
		#</DefineSpecificDict>

	def __delitem__(self,_DeletingKeyVariable):
		"""Call the set<HookString> methods"""

		#Refresh the attributes
		self.DeletingVariable=_DeletingKeyVariable

		#Set
		return self.delete()

	def delete(self,**_VariablesDict):
		""" """

		#Do the minimal delitem
		if type(self.DeletingVariable) in [str,unicode]:

			#Del Safely the Value
			if self.DeletingVariable in self.__dict__:
				del self.__dict__[self.DeletingVariable]

#</DefineClass>

#<DefineAttestingFunctions>
def attest_deleter():
	return DeleterClass().__setitem__('MyInt',0).__delitem__('MyInt')
#</DefineAttestingFunctions>

