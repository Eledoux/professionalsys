#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class ObjectTesterClass(SYS.TesterClass):
	
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	def test_01_update(self):
		self.TestedVariable=self.TestedClass().update(*[
															('MyInt',0)
														],
													**{
														'MyDict':{'MyString':"hello"}
													}
												)

	def test_02_setA(self):
		self.TestedVariable=self.TestedClass().update(
									*[
										('MyFirstObject',self.TestedClass()),
										('/MyFirstObject/MyChildString',"coucou"),
										('MySecondObject',self.TestedClass()),
										('/MySecondObject',[
																('MyChildString',"hello"),
																('MyInt',0)
															])
									]
								)

	def test_03_getA(self):
		self.TestedVariable=self.TestedClass()['/__class__/TypeString']

	def test_04_pick(self):
		self.TestedVariable=self.TestedClass().update(
											*[
												('MyInt',0),
												('*SecondInt',2)
											],
											**{'MyDict':{'MyString':"hello"}}
											)('pick',['MyInt','*SecondInt','MyDict'])

	def test_05_append(self):
		self.TestedVariable=self.TestedClass()(
												'append',
												('MyDict',{'MyString':"Hello"})
											)

	def test_06_applyAppend(self):
		self.TestedVariable=self.TestedClass()(
												'apply',
												*(
													'append',
													[
														('MyDict',{'MyString':"Hello"}),
														('MyInt',1)
													]
												)
											)

	def test_07_insert(self):
		self.TestedVariable=self.TestedClass()(
												'apply',
												*(
													'append',
													[
														('MyDict',{'MyString':"Hello"}),
														('MyInt',1)
													]
												)
											)(
												'insert',
												*(
													1,
													('MyOtherString',"WTF")
												)
											)

	def test_08_add(self):
		self.TestedVariable=self.TestedClass()+[
													('MyDict',{'MyString':"Hello"}),
													('MyInt',1)
												]

	def test_09_update(self):
		self.TestedVariable=self.TestedClass().update(**{'*MyDict':{'MyString':"Hello"}})

	def test_10_getB(self):
		self.TestedVariable=self.TestedClass()(
												'apply',
												*(
													'append',
													[
														('MyDict',{'MyString':"Hello"}),
														('MyInt',1)
													]
												)
											)(
												'insert',
												*(
													1,
													('MyOtherString',"WTF")
												)
											)['*MyDict']

	def test_11_getC(self):
		self.TestedVariable=self.TestedClass()(
												'apply',
												*(
													'append',
													[
														('MyDict',{'MyString':"Hello"}),
														('MyInt',1)
													]
												)
											)(
												'insert',
												*(
													1,
													('MyOtherString',"WTF")
												)
											)[0]

	def test_12_updateAppend(self):
		self.TestedVariable=self.TestedClass().update(*[
															('append',[('MyString',"hello")]),
															(
																'apply',[
																'append',
																	[
																		('MyInt',0),
																		('MyFloat',0.1)
																	]
																]
															)
														])

#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply','testify',MethodStringsList)
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply','bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>
