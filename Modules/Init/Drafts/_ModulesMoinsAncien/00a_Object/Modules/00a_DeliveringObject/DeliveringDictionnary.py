#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
TagString="*"
#</DefineLocals>

#<DefineClass>
class DeliveringDictionnaryClass(
						SYS.DictionnaryClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.DeliveringTuplesListsList=[]
		#</DefineSpecificDict>
			
	def appendAfter(self,_AppendingTuple):

		#Set inside _AppendingTuple[1] with self.DeliveringTuplesListsList[self.AppendedInt]
		if len(self.DeliveringTuplesListsList)>self.AppendedInt:
			if hasattr(_AppendingTuple[1],'update'):
				if type(_AppendingTuple[1])==dict:
					_AppendingTuple[1].update(dict(self.DeliveringTuplesListsList[self.AppendedInt]))
				else:
					_AppendingTuple[1].update(*self.DeliveringTuplesListsList[self.AppendedInt])

		#Return self
		return self
	#<DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>


#</DefineClass>

