#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class DeepPickingDictionnaryTesterClass(SYS.TesterClass):
	
	def test_0default(self):
		self.TestedVariable=self.TestedClass()

	def test_1deeppick(self):

		#Define an Object
		Object=SYS.ObjectClass()

		#Append a first time
		Object.update(**{
						'MyGrandChildObject':SYS.ObjectClass().update(**{
							'MyString':"hello"
							})
						})
		TestedClass=self.TestedClass().update(**{'PickingVariablesList':[
															'MyGrandChildObject',
															]})(
																'append',
																('MyPickedObjectDict',Object)
															)
														
		#Append a second time
		Object.update(**{
						'MyGrandChildObject':SYS.ObjectClass().update(**{
							'MyString':"coucou"
							})
						})
		self.TestedVariable=TestedClass.update(**{'PickingVariablesList':[
															'MyGrandChildObject',
															]})(
																'append',
																('MyPickedObjectDict',Object)
															)
														






#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply',*('testify',MethodStringsList))
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply',*('bound',MethodStringsList))
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>

