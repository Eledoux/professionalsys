#<ImportModules>
import ShareYourSystem as SYS
import time
#</ImportModules>

#<DefineClass>
class JoinClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.JoinKeyVariablesList=[]
		self.JoinString=""
		self.JoinVariable=None
		#</DefineSpecificDict>
	
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindJoinKeyVariablesListAfter(self):

		#Bind with JoinString setting
		self.setJoinString()
		
	def bindJoinVariableAfter(self):

		#Bind with JoinString setting
		self.setJoinString()

	#</DefineBindingHookMethods>	

	#<DefineMethods>
	def setJoinString(self):
		
		#Join the self.JoinKeyVariablesList with their corresponding getted Values
		self['JoinString']='_'.join(
										filter(
													lambda _JoiningTuple:
													_JoiningTuple!=None,
													map(
														lambda _JoiningVariable:
														'('+str(_JoiningVariable)+','+str(self.JoinVariable[_JoiningVariable])+')' 
														if SYS.getWithDictatedVariableAndKeyVariable(self.JoinVariable,_JoiningVariable)!=None 
														else None,
														self.JoinKeyVariablesList
													)
												)
									)

	#<DefineMethods>	

#</DefineClass>