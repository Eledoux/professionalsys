#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
TagString="*"
#</DefineLocals>

#<DefineClass>
class DeliverClass(
						SYS.DeliveringDictionnaryClass
				):

	#<DefineHookMethods>
	def deliverAfter(self,_DeliveredVariable,*_DeliveredVariablesList):

		#Append then with this _AppendingKeyString
		self('append',_DeliveredVariable)

		#Do for each JoiningVariable
		self('apply',*('append',_DeliveredVariablesList))

		#Return self
		return self
	#<DefineHookMethods>

	#<DefineMethods>

	#</DefineMethods>


#</DefineClass>

