#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
TagString="*"
#</DefineLocals>

#<DefineClass>
class DeliverClass(
						SYS.DictionnaryClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.DeliveringTuplesListsList=[]
		#</DefineSpecificDict>
			
	def deliverAfter(self):

		if len(self.DeliveringTuplesListsList)>=len(self.ValueVariablesList):

			#Deliver for each
			map(
					lambda IntAndDeliveringTuplesList:
					self.ValueVariablesList[IntAndDeliveringTuplesList[0]].update(dict(IntAndDeliveringTuplesList[1])) 
					if type(self.ValueVariablesList[IntAndDeliveringTuplesList[0]])==dict
					else
						self.ValueVariablesList[IntAndDeliveringTuplesList[0]].update(*IntAndDeliveringTuplesList[1]) 	
						if hasattr(self.ValueVariablesList[IntAndDeliveringTuplesList[0]],'update')
						else None,
					enumerate(self.DeliveringTuplesListsList)
				)
	#<DefineHookMethods>

	#<DefineMethods>

	#</DefineMethods>


#</DefineClass>

