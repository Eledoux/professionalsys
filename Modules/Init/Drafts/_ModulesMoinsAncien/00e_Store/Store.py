#<ImportModules>
import ShareYourSystem as SYS
import time
#</ImportModules>

#<DefineClass>
class StoreClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.StoreHdfFile=SYS.HdfFileClass()
		self.GroupTuple=[]
		self.StorePathString=""
		#</DefineSpecificDict>
	
	def getAfter(self,_GettedVariable):

		#Do the shortcut to the JoinVariable for getting
		return self.StoreJoin.JoinVariable[_GettedVariable]

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindGroupTupleAfter(self):

		#Update the StorePathString
		self.StorePathString+=_GroupTuple[0]+"/"

		if self.StorePathString not in self.StoreHdfFile.H5pyFile:
			self.StoreHdfFile.H5pyFile.create_group()

		#Get the possible child GroupTuplesList
		GettedVariable=SYS.getWithDictatedVariableAndKeyVariable(GroupTuple[1],'GroupTuplesList')

		if GettedVariable!=None:
			return SYS.StoreClass().__setitem__('GroupTuplesList',GroupTuple[1]['GroupTuplesList'])

	#</DefineBindingHookMethods>	

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>