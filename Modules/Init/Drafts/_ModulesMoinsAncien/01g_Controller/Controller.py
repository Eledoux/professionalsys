#<ImportModules>
import itertools
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Parse"
DoingString="Parsing"
DoneString="Parsed"
#</DefineLocals>

#<DefineClass>
class ControllerClass(SYS.ParsingDictionnaryClass):

	#<DefineHookMethods>
	def parseAfter(self,_ParsingVariable):
		

		#Init the UpdatingTuplesList and Dict
		UpdatingTuplesList=[]
		UpdatingTuplesDict={}

		#Get the UpdatedTypeStringsList
		UpdatedTypeStringsList=[]
		if SYS.getIsTuplesListBool(_ParsingVariable):
			if _ParsingVariable[0][0]=="UpdatedTypeStringsList":

				#Set the ParsedTypeString
				UpdatedTypeStringsList=_ParsingVariable[0][1]

				#Set ParsingTuplesList
				UpdatingTuplesList=_ParsingVariable

		else:

			#Get the UpdatedTypeStrings
			UpdatedTypeStringsList=SYS.getWithDictatedVariableAndKeyVariable(_ParsingVariable,"UpdatedTypeStringsList")

			#Set the ParsingTuplesDict
			UpdatingTuplesDict=SYS.getDictedDictWithVariable(_ParsingVariable)

		if len(UpdatedTypeStringsList)>0:

			#Define the UpdatedClasses
			UpdatedClassesList=filter(
										lambda _UpdatedClass:
										_UpdatedClass!=None,
										map(
												lambda _UpdatedTypeString:
												getattr(SYS,SYS.getClassStringWithTypeString(_UpdatedTypeString)),
												UpdatedTypeStringsList
											)
									)
			
			if len(UpdatedClassesList)>0:

				#Get all the UpdatedValueVariables
				UpdatedValueVariablesList=filter(
													lambda _ValueVariable:
													any(
															map(
																lambda _UpdatedClass: 
																_UpdatedClass in type(_ValueVariable).__mro__,
																UpdatedClassesList
															)
														),
													self.ValueVariablesList
												)

				#Update these Variables
				map(
						lambda _UpdatedValueVariable:
						_UpdatedValueVariable.update(*UpdatingTuplesList,**UpdatingTuplesDict),
						UpdatedValueVariablesList
					)


		#Get all the ChildParsers
		ChildParsersList=filter(
									lambda _ValueVariable:
									SYS.ParserClass in type(_ValueVariable).__mro__,
									self.ValueVariablesList
								)

		#Parse in the ChildParsers
		map(
				lambda _ChildParser:
				_ChildParser('parse',_ParsingVariable),
				ChildParsersList
			)

		#Return self
		return self
	#</DefineHookMethods>

#</DefineClass>