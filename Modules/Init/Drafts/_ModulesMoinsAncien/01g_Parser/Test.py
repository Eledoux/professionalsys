#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class ParserTesterClass(SYS.TesterClass):
	
	def test_0default(self):
		self.TestedVariable=self.TestedClass()

	def test_1parse(self):
		self.TestedVariable=self.TestedClass()(
										'append',
										(
											'CalculParser',
											{
												'TypeString':"Parser",
												'apply':[
															'append',
															[
																(
																	'Sum',
																	[
																		('TypeString',"Sum"),
																	]
																),
																(
																	'Mul',
																	[
																		('TypeString',"Mul"),
																	]
																)
															]
														]
											}
										)
									)(
										'parse',
										[
											('UpdatedTypeStringsList',['Sum','Mul']),
											('SeedInt',1)
										]

									)
#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply',*('testify',MethodStringsList))
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply',*('bound',MethodStringsList))
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>

