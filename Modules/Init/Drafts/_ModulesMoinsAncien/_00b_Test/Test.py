#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Test"
#</DefineLocals>

#<DefineClass>
class TestClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.AssertString=""
		self.AssertVariable=None
		self.TestMethodString=""
		self.IsPrintedBool=False
		self.TestsFolderPathString=SYS.getCurrentFolderPathString()+'Tests/'
		self.TestTypeString=SYS.getTypeStringWithClassString(self.__class__.__name__).split('Tester')[0]
		self.TestClass=getattr(SYS,SYS.getClassStringWithTypeString(self.TestingTypeString))
		#</DefineSpecificDict>


	def testifyAfter(self,_AssertString):
		
		#Test first
		self('test',_TestString)

		#Check that there is a Folder Tests
		if SYS.sys.modules['os'].path.isdir(self.TestingFolderPathString)==False:
			SYS.sys.modules['os'].popen('mkdir '+self.TestingFolderPathString)

		#Write
		File=open(self.TestingFolderPathString+_TestString+'.txt','w')
		File.write(self.TestedString)
		File.close()

	def boundAfter(self,_TestString):

		#Define the new test method to be bounded
		def test(_TestingVariable):

			#Get the AssertingString
			AssertingString=self.getAssertingString(_TestString)

			#Get the TestedString
			self('test',_TestString)

			#Print maybe
			if self.IsPrintedBool:
				print("#################")
				print("<"+_TestString+">")
				print("#################")
				print("")
				print('AssertingString is :')
				print(AssertingString)
				print("")

				#Print maybe
				print('AssertedString is :')
				print(self('test',_TestString).TestString)
				print("")

			#Assert
			_TestingVariable.assertEqual(
					AssertingString,
					self('test',_TestString).TestString
					)

		test.__name__='test_'+self.TestingTypeString+'_'+_TestString.split('test_')[1]
		setattr(self.TestingClass,test.__name__,test)

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindTestedVariableAfter(self):

		#Bind with TestString setting
		SYS.PrintingDict['IsIdBool']=False
		self['TestString']=SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(self.TestedVariable,SYS.PrintingDict)
		SYS.PrintingDict['IsIdBool']=False
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setUp(self):
		self.AssertVariable=None
		self.AssertString=""

	def getAssertingString(self,_TestString):

		#Read
		File=open(self.TestsFolderPathString+_TestString+'.txt','r')
		return File.read()
	#</DefineMethods>

#</DefineClass>


