#<ImportModules>
import ShareYourSystem as SYS
import time
#</ImportModules>

#<DefineClass>
class ControlClass(
					SYS.ObjectClass
					#SYS.DictionnaryClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.TypeStringsList=[]
		self.VariablesList=[]
		self.ValueVariablesList=[]
		#</DefineSpecificDict>
	
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindTypeStringsListAfter(self):

		#Define the UpdatedClasses
		ControlledClassesList=filter(
									lambda _UpdatedClass:
									_UpdatedClass!=None,
									map(
											lambda _UpdatedTypeString:
											getattr(SYS,SYS.getClassStringWithTypeString(_UpdatedTypeString)),
											UpdatedTypeStringsList
										)
								)

		#Bind with VariablesList setting
		self['VariablesList']=filter(
											lambda _ValueVariable:
											any(
													map(
														lambda _UpdatedClass: 
														_UpdatedClass in type(_ValueVariable).__mro__,
														ControlledClassesList
													)
												),
											self.ValueVariablesList
										)
	#</DefineBindingHookMethods>	

	#<DefineMethods>
	#<DefineMethods>	

#</DefineClass>