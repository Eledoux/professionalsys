#<ImportModules>
import ShareYourSystem as SYS
import time
#</ImportModules>

#<DefineClass>
class CommitClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.CommitTimeString=""
		self.CommitPersonString=""
		self.CommitString=""
		#</DefineSpecificDict>

	def bindAfter(self):

		#Bind with CommitTimeString setting
		self['CommitTimeString']=str(time.ctime(time.time()))
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindCommitTimeStringAfter(self):
		self.setCommitString()
	def bindCommitPersonStringAfter(self):
		self.setCommitString()	
	#</DefineBindingHookMethods>	

	#<DefineMethods>
	def setCommitString(self):
		self['CommitString']=self.CommitPersonString+" at "+self.CommitTimeString
	#<DefineMethods>	

#</DefineClass>