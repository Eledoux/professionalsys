#<ImportModules>
import ShareYourSystem as SYS
import unittest
#</ImportModules>


#<DefineClass>
class TesterClass(SYS.ObjectClass):

	def setUp(self):
		self.TestedVariable=None
		self.TestedString=""
		
	def setTestedStringWithTestString(self,_TestString):

		#Set the TestedString
		SYS.PrintingDict['IsIdBool']=False
		self.TestedString=SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(self.TestedVariable,SYS.PrintingDict)
		SYS.PrintingDict['IsIdBool']=False

	def writeWithTestString(self,_TestString):

		#Set the TestString
		self.setTestedStringWithTestString(_TestString)

		#Write
		File=open(SYS.getCurrentFolderPathString()+_TestString+'.txt','w')
		File.write(TestString)
		File.close()

	def getAssertedString(self,_TestString):

		#Read
		File=open(SYS.getCurrentFolderPathString()+_TestString+'.txt','r')
		return File.read()

	def write(self):

		map(
				lambda TestMethodString:
				getattr(self,TestMethodString)() if TestMethodString!='test_all' else None,
				SYS.getTestMethodStringsListWithClass(self.__class__)	
			)

#</DefineClass>

