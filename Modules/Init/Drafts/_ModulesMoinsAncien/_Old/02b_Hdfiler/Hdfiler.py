#<ImportModules>
import ShareYourSystem as SYS
import h5py
#</ImportModules>

#<DefineLocals>
DoString="Hdfile"
DoingString="Hdfiling"
DoneString="Hdfiler"
#</DefineLocals>

#<DefineFunctions>
def getDictatingDictWithDataList(_DataList):
	
	return dict(
				map(
					lambda ListedDict:
					(getIdStringWithDict(ListedDict),getDictatingDict(ListedDict)),
					_DataList
				)
			)

def getDictatingDict(_Dict):

	#print('setData',_DataDict)
	DictatingDict=dict(
						map(
							lambda DataItemTuple:
							#Dict Case
							(DataItemTuple[0],getDictatingDict(DataItemTuple[1])) 
							if type(DataItemTuple[1])==dict 
							else
								#List of Dict Case
								("_"+"".join(DataItemTuple[0].split("List")[:-1])+"Dict",
									getDictatingDictWithDataList(DataItemTuple[1]))
								if type(DataItemTuple[1])==list and all(
																			map(
																					lambda ListedVariable:
																					type(ListedVariable)==dict,
																					DataItemTuple[1]
																				)
																		)
								else
									#List of non Dict Case
									(DataItemTuple[0],DataItemTuple[1])
									if type(DataItemTuple[1])==list and any(
																			map(
																					lambda ListedVariable:
																					type(ListedVariable)!=dict,
																					DataItemTuple[1]
																				)
																			)
									else
									#Metadata or Data Case
									(DataItemTuple[0],DataItemTuple[1])
							,_Dict.items()
						)
					)

	return DictatingDict

def getNumberVariableWithString(_String):
	try:
		return int(_String)
	except ValueError:
		try:
			return float(_String)
		except ValueError:
			return str(_String)

def getIdStringWithDict(_Dict):

	#Get the MetaKeyStringsList to buid the IdString
	MetaKeyStringsList=filter(
								lambda KeyString:
								SYS.getCommonPrefixStringWithStringsList([KeyString,HdfilingTagString])==HdfilingTagString,
								sorted(_Dict.keys())
							)

	#Build the MetaDataString (this is going to be the Id of this set)
	return unicode("_"+"_".join(
							map(
								lambda MetaKeyString:
								"("+MetaKeyString.split(HdfilingTagString)[1]+","+str(_Dict[MetaKeyString])+")",
								MetaKeyStringsList
								)
							)
					)

def setDatabaseFileWithAttributeKeyStringAndAttributeVariable(_DatabaseFile,_AttributeKeyString,_AttributeVariable):

	#Set this attribute if not already
	if _AttributeKeyString not in _DatabaseFile:
		_DatabaseFile.__setitem__(_AttributeKeyString,_AttributeVariable)

def setDatabaseFileWithCommitStringAndGroupKeyStringAndDataSetVariable(_DatabaseFile,_CommitString,_GroupKeyString,_DataSetVariable):

	#print('Set Dataset ',_GroupKeyString,_DataSetVariable)
	#Set the parent group if not already
	if _GroupKeyString not in _DatabaseFile.keys():
		_DatabaseFile.create_group(_GroupKeyString)

	#Get the IndexIntString
	try:
		IndexIntString=str(
						max(
							map(
								lambda IndexString:
								(int)(IndexString.split("_")[0]),
								_DatabaseFile[_GroupKeyString].keys()
								)
							)+1
						)
	except:
		IndexIntString="0"

	#Build the IndexString
	IndexString=IndexIntString+"_"+_CommitString

	#Set the dataset
	_DatabaseFile[_GroupKeyString].create_dataset(IndexString,data=_DataSetVariable)

def setDatabaseFileWithCommitStringAndKeyStringAndDictatingDict(_DatabaseFile,_CommitString,_KeyString,_DictatingDict):

	#Set the group if not already
	if _KeyString not in _DatabaseFile.keys():
		_DatabaseFile.create_group(_KeyString)

	#print("setDatabaseFileWithKeyStringAndDictatingDict",_DictatingDict)
	#Set deeper
	map(
		lambda DataItemTuple:
		#Dict Case with a KeyString
		setDatabaseFileWithCommitStringAndKeyStringAndDictatingDict(
																		_DatabaseFile[_KeyString],
																		_CommitString,
																		DataItemTuple[0],
																		DataItemTuple[1]
																	) 
		if type(DataItemTuple[1])==dict
		else 
			#MetaData case
			setDatabaseFileWithAttributeKeyStringAndAttributeVariable(
																		_DatabaseFile[_KeyString],
																		DataItemTuple[0],
																		DataItemTuple[1]
																	)
			if any(
					map(
						lambda TagString:
						SYS.getCommonPrefixStringWithStringsList(DataItemTuple[0],TagString)==TagString,
						[HdfilingTagString,"$"]
						)
					)
			else
				#Data case
				setDatabaseFileWithCommitStringAndGroupKeyStringAndDataSetVariable(
																					_DatabaseFile[_KeyString],
																					_CommitString,
																					DataItemTuple[0],
																					DataItemTuple[1]
																				)
		,_DictatingDict.items()
		)

def getDataDictWithDictAndIndexInt(_Dict,_IndexInt):

	#Check that this is an iterating object
	if hasattr(_Dict,"items"):

		return dict(
						map(
							lambda ItemTuple:
							#DataCase
							(
								ItemTuple[0],

								filter(
										lambda KeyStringAndValueVariable:
										(int)(KeyStringAndValueVariable[0].split("_")[0])==_IndexInt,
										ItemTuple[1].items()
										)[0][1] if len(
														filter(
															lambda KeyStringAndValueVariable:
															(int)(KeyStringAndValueVariable[0].split("_")[0])==_IndexInt,
															ItemTuple[1].items()
														)
													)>0 else None
							)
							if SYS.getCommonPrefixStringWithStringsList([ItemTuple[0],HdfiledTagString])==HdfiledTagString
							else
								#MetaData Case or Node Case
								(ItemTuple[0],ItemTuple[1])
								if any(
										map(
											lambda TagString:
											SYS.getCommonPrefixStringWithStringsList([ItemTuple[0],TagString])==TagString,
											[HdfilingTagString,"$"]
											)
										)
								else
									(ItemTuple[0],getDataDictWithDictAndIndexInt(ItemTuple[1],_IndexInt))
							,_Dict.items()
						)
					)
	else:

		return {}
#</DefineFunctions>

#<DefineClass>
class HdfilerClass(
					SYS.UserClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithHdfiler(self):

		#<DefineSpecificDict>
		self.HdfiledDict={}
		self.HdfilingString=""
		#</DefineSpecificDict>

		#Use a File
		self("Use",*[('UsingTypeString',"Hdfiler")],**{'UsedTypeString':"File"})
		self['<Used>OfHdfiler_File'].update({'ModeString':"a",'ExtensionString':".hdf5"})

	def getBeforeBasesWithHdfiler(self,_KeyVariable):

		if type(_KeyVariable) in [str,unicode]:
			if SYS.getCommonPrefixStringWithStringsList(["<Hdfiled>",_KeyVariable])=="<Hdfiled>":

				#Set a short alias for the used File
				File=self['<Used>OfHdfiler_File']

				#Set the HdfiledString
				HdfiledString=_KeyVariable.split("<Hdfiled>")[1]

				#Case where we want to retrieve a previous hdfiled Dict thanks to its index
				if HdfiledString.isdigit():
					return getDataDictWithDictAndIndexInt(self['<Used>OfHdfiler_File'].FilePointer,(int)(HdfiledString))

				else:

					#Bind with ModeString setting
					if SYS.sys.modules['os'].path.isfile(File.FilePathString):

						#Bind with File_ModeString setting
						File['ModeString']="r"

						#Read the Database
						return h5py.File(File.FilePathString, "r")[HdfiledString]

	def callAfterBasesWithHdfiler(self,*_ArgsList,**_KwargsDict):

		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="Hdfile":

				#Reupdate possible attributes
				for KeyString in ['HdfilingString','HdfiledDict']:
					if KeyString in _KwargsDict:
						self[KeyString]=_KwargsDict[KeyString]

				#Set a short alias for the used File
				File=self['<Used>OfHdfiler_File']

				#Push Method
				if _KwargsDict['MethodString']=="push":

					#Bind with ModeString setting
					if SYS.sys.modules['os'].path.isfile(File.FilePathString):
						File['ModeString']="a"
					else:
						File['ModeString']="w"

					if File.FilePointer!=None:

						#Get the DictedDataDict
						DictedDataDict=getDictatingDict(self.HdfiledDict)

						#Get the IdString
						IdString=getIdStringWithDict(self.HdfiledDict)

						#Create a new group if IdString is not there
						if IdString not in File.FilePointer.keys():

							#Create the group
							File.FilePointer.create_group(IdString)

						#Set deeper in the Data
						map(
							lambda DataItemTuple:
							#Dict Case with a KeyString
							setDatabaseFileAndCommitStringAndKeyStringAndDictatingDict(
								File.FilePointer[IdString],
								self.HdfilingString,
								DataItemTuple[0],
								DataItemTuple[1]
								) 
							if type(DataItemTuple[1])==dict
							else 
								#Metadata case
								setDatabaseFileWithAttributeKeyStringAndAttributeVariable(
									File.FilePointer[IdString],
									DataItemTuple[0],
									DataItemTuple[1]
									)
								if any(
										map(
											lambda TagString:
											SYS.getCommonPrefixStringWithStringsList([DataItemTuple[0],TagString])==TagString,
											[HdfilingTagString,"$"]
											)
									)
								else
								#Data case
								setDatabaseFileWithCommitStringAndGroupKeyStringAndDataSetVariable(
									File.FilePointer[IdString],
									self.HdfilingString,
									DataItemTuple[0],
									DataItemTuple[1]
								)
							,self.HdfiledDict.items()
							)

				#Delete Method
				elif _KwargsDict['MethodString']=="delete":

					#Bind with ModeString setting
					self['ModeString']="a"

					#Delete 
					if self.HdfilingString in File.FilePointer:
						del File.FilePointer[self.PathString]

	def reprAfterBasesWithHdfiler(self,_SelfString,PrintedDict):

		#Define the new PrintedDict with SkippedKeyStringsList
		NewPrintedDict=SYS.getDictedDictWithVariable(self["<Hdfiled>/"])

		#Define the new SelfString
		SelfString=SYS.ConsolePrinter.getPrintedVariableString(NewPrintedDict)

		#Return
		return (SelfString,NewPrintedDict)

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>	

	#<DefineMethods>
	def getDataDictWithIndexInt(self,_IndexInt):
		return 
	#<DefineMethods>	

#</DefineClass>