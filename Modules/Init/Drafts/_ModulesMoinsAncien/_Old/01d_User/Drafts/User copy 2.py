#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Use"
DoingString="Using"
DoneString="Used"
DoingKeyString="<Use>"
DoneTagString="<Used>"
#</DefineLocals>

#<DefineClass>
class UserClass(
					SYS.InstancerClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.UsingTypeString=self.TypeString
		self.UsedVariable=None
		#</DefineSpecificDict>

	def useAfter(self,*_ArgsList,**_KwargsDict):

		#Check if it is a binding of an already existing Instance 
		if 'UsedVariable' not in _KwargsDict:

			if 'UsedTypeString' not in _KwargsDict:

				#Define the InstancingTypeString
				InstancingTypeString="Of"+self.UsingTypeString+"_"+self.InstancingTypeString

				#Instancify
				self.update(*[('InstancingTypeString',InstancingTypeString)])('instance')

				#Set the UserPointer to the InstancedVariable
				self.InstancedVariable['Using'+self.UsingTypeString+'Pointer']=self

				#Set an alis
				self.UsedVariable=self.InstancedVariable

			else:

				#Shortcut call
				self.update(
								*[
									('InstancingDict',_KwargsDict['UsedDict'] if 'UsedDict' in _KwargsDict else {}),
									('InstancingTypeString',_KwargsDict['UsedTypeString'])
								]
							)('use')

		#Return self
		return self
	#</DefineHookMethods>

#</DefineClass>
