#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Use"
DoingString="Using"
DoneString="Used"
DoingKeyString="<Use>"
DoneTagString="<Used>"
#</DefineLocals>

#<DefineClass>
class UserClass(
					SYS.UsingDictionnaryClass
				):

	#<DefineHookMethods>
	def useAfter(self,_UsedVariable):

		#Append
		self('append',(None,_UsedVariable))

		#Return self
		return self
	#</DefineHookMethods>

#</DefineClass>
