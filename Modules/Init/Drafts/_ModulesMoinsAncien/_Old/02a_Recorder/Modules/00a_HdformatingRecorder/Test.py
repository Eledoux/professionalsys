#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class RecorderTesterClass(SYS.TesterClass):
	
	def test_0default(self):
		self.TestedVariable=self.TestedClass()

	def test_1record(self):
		self.TestedVariable=self.TestedClass().update(**{
				'PickingVariablesList':['/MyGrandChildObject/MyString'],
				'JoiningVariablesList':['/MyGrandChildObject/MyString']
			})(
					'apply',
					'record',
					[
						SYS.ObjectClass().update(**{
							'MyGrandChildObject':SYS.ObjectClass().update(**{
								'MyString':"bonjour"
								})
							}),
						SYS.ObjectClass().update(**{
								'MyGrandChildObject':SYS.ObjectClass().update(**{
									'MyString':"hello"
									})
								})
					]
				)
#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply',*('testify',MethodStringsList))
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply',*('bound',MethodStringsList))
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>