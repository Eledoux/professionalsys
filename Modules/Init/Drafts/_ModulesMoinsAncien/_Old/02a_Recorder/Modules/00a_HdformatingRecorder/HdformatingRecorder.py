#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>


#<DefineClass>
class HdformatingRecorderClass(
						SYS.RecorderClass
					):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.HdformatingFile=SYS.FileClass()
		#</DefineSpecificDict>

	def recordAfter(self,_RecordedVariable,*_RecordedVariablesList):
		
		#Return self
		return self
	#</DefineHookMethods>

#</DefineClass>


