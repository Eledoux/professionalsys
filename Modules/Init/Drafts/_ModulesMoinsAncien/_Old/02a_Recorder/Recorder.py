#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>


#<DefineClass>
class RecorderClass(
						SYS.RecordingDictionnaryClass
					):

	#<DefineHookMethods>
	def recordAfter(self,_RecordedVariable,*_RecordedVariablesList):

		#Append then with this _AppendingKeyString
		self('append',(None,_RecordedVariable))

		#Do for each JoiningVariable
		self('apply',*('append',_RecordedVariablesList))

		#Return self
		return self
	#</DefineHookMethods>

#</DefineClass>


