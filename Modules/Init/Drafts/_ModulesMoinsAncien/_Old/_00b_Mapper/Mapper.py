#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Map"
#</DefineLocals>

#<DefineClass>
class MapperClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.TestingString=""
		self.TestedVariable=None
		self.TestedString=""
		self.TestingVariable=None
		self.IsPrintedBool=False
		#</DefineSpecificDict>
	
	def testAfter(self,_TestString):

		#Test
		self.setTestedStringWithTestString(_TestString)

		#Return self
		return self

	def testifyAfter(self,_TestString):
		
		#Test first
		self('test',_TestString,*_TestStringsList)

		#Write
		File=open(SYS.getCurrentFolderPathString()+_TestString+'.txt','w')
		File.write(self.TestedString)
		File.close()

	def assertAfter(self,_TestString):

		if hasattr(self.TestingVariable,'assertEqual'):

			#Get the AssertingString
			AssertingString=self.getAssertingString(_TestString)

			#Get the TestedString
			self('test',_TestString)

			#Print maybe
			if self.IsPrintedBool:
				print("<"+_TestString+">")
				print("")
				print('AssertingString is :')
				print(AssertingString)
				print("")

				#Print maybe
				print('AssertedString is :')
				print(self('test',_TestString).TestedString)

			#Assert
			self.TestingVariable.assertEqual(
					AssertingString,
					self('test',_TestString).TestedString
					)

	#</DefineHookMethods>

	#<DefineMethods>
	def setTestedStringWithTestString(self,_TestString):

		#Test the corresponding method
		getattr(self,SYS.getLowedString(DoString)+_TestString)()

		#Set the TestedString
		SYS.PrintingDict['IsIdBool']=False
		self.TestedString=SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(self.TestedVariable,SYS.PrintingDict)
		SYS.PrintingDict['IsIdBool']=False

	def getAssertingString(self,_TestString):

		#Read
		File=open(SYS.getCurrentFolderPathString()+_TestString+'.txt','r')
		return File.read()
	#</DefineMethods>

#</DefineClass>

