#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
TagString="*"
#</DefineLocals>

#<DefineClass>
class DictionnaryClass(
						SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.IndexInt=-1
		self.KeyStringsList=[]
		self.ValueVariablesList=[]
		self.KeyStringToIndexIntDict={}
		#</DefineSpecificDict>
		
	def getBefore(self,_GettingVariable):
		if type(_GettingVariable)==int:
			if _GettingVariable<len(self.ValueVariablesList):
				return {'GettedVariable':self.ValueVariablesList[_GettingVariable]}
		elif SYS.getCommonPrefixStringWithStringsList([_GettingVariable,TagString])==TagString:
			KeyString=_GettingVariable.split(TagString)[1]
			if KeyString in self.KeyStringToIndexIntDict:
				return {'GettedVariable':self.ValueVariablesList[self.KeyStringToIndexIntDict[KeyString]]}
		
	def setBefore(self,_SettingVariable,_SettedVariable):
		
		if SYS.getCommonPrefixStringWithStringsList([_SettingVariable,TagString])==TagString:
			KeyString=_SettingVariable.split(TagString)[1]
			self('append',(KeyString,_SettedVariable))
			self.IsSettingBool=False

	def addAfter(self,_AddedVariable):
		self('apply','append',_AddedVariable)

	def appendBefore(self,_AppendingTuple):
		
		#Set the corresponding AppendedInt
		self.AppendedInt=len(self.KeyStringsList)
						
	def appendAfter(self,_AppendingTuple):

		#Append in the KeyStringsList and the ValueVariablesList
		self.KeyStringsList.append(_AppendingTuple[0])
		self.ValueVariablesList.append(_AppendingTuple[1])

		#Give the AppendedInt as an IndexInt for the ValueVariable
		if hasattr(self.ValueVariablesList[self.AppendedInt],'__setitem__'):
			
			#TuplesList Case
			if SYS.getIsTuplesListBool(self.ValueVariablesList[self.AppendedInt]):
				self.ValueVariablesList[self.AppendedInt].append(('IndexInt',self.AppendedInt))
			else:
				
				#Dictable case
				self.ValueVariablesList[self.AppendedInt]['IndexInt']=self.AppendedInt

		#Update the KeyStringToIndexIntDict
		self.KeyStringToIndexIntDict[_AppendingTuple[0]]=len(self.KeyStringsList)-1

		#Return self
		return self

	def insertBefore(self,_InsertingInt,_InsertingTuple):
		
		#Set the corresponding InsertedInt
		self.InsertedInt=_InsertingInt

	def insertAfter(self,_InsertingInt,_InsertingTuple):

		#Insert in the KeyStringsList and the ValueVariablesList
		self.KeyStringsList.insert(self.InsertedInt,_InsertingTuple[0])
		self.ValueVariablesList.insert(self.InsertedInt,_InsertingTuple[1])
		
		#Give the InsertedInt as an IndexInt for the ValueVariable
		if hasattr(self.ValueVariablesList[self.InsertedInt],'__setitem__'):

			#TuplesList Case
			if SYS.getIsTuplesListBool(self.ValueVariablesList[self.InsertedInt]):
				self.ValueVariablesList[self.InsertedInt].append(('IndexInt',self.InsertedInt))
			else:

				#Dictable case
				self.ValueVariablesList[self.InsertedInt]['IndexInt']=self.InsertedInt

		#Update the KeyStringToIndexIntDict
		self.KeyStringToIndexIntDict[_InsertingTuple[0]]=_InsertingInt

		#Define all the SupTuples
		SupTuplesList=filter(
					lambda KeyStringAndInt:
					KeyStringAndInt[1]>=_InsertingInt and KeyStringAndInt[0]!=_InsertingTuple[0],
					self.KeyStringToIndexIntDict.items()
				)

		#Modify their item
		map(
				lambda SupTuple:
				self.insertWithSupTuple(SupTuple),
				SupTuplesList
			)

		#Return self
		return self

	def itemsAfter(self):
		return zip(self.KeyStringsList,self.ValueVariablesList)
	#<DefineHookMethods>

	#<DefineMethods>
	def insertWithSupTuple(self,_SupTuple):

		#Define the NewIndexInt
		NewIndexInt=_SupTuple[1]+1

		#Change the Value
		self.KeyStringToIndexIntDict[_SupTuple[0]]=NewIndexInt

	#</DefineMethods>


#</DefineClass>

