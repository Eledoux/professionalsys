#<ImportModules>
import ShareYourSystem as SYS
import itertools
#</ImportModules>

#<DefineClass>
class StructuringDictionnaryClass(
								SYS.InstancingDictionnaryClass,
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.StructuringPointersList=[]
		self.StructuringIntsList=[]
		#</DefineSpecificDict>
	
	def appendAfter(self,_AppendingTuple):

		#Add the self.StructuringPointersList plus self
		if hasattr(self.ValueVariablesList[-1],'StructuringPointersList'):
			self.ValueVariablesList[-1]['StructuringPointersList']=[self]+self.StructuringPointersList

		#Return self
		return self
	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindStructuringPointersListAfter(self):

		#Give to all the derived from StructuringDictionnary the StructuringPointersList and StructuringIntsList
		map(
				lambda _IntAndStructuringDictionnary:
				_IntAndStructuringDictionnary[1].__setitem__('StructuringPointersList',[self]+self.StructuringPointersList),
				filter(
						lambda _IntAndVariable:
						SYS.StructuringDictionnaryClass in type(_IntAndVariable[1]).__mro__,
						enumerate(self.ValueVariablesList)
						)
			)

		#Bind with StructuringIntsList setting
		self['StructuringIntsList']=[self.IndexInt]+map(
															lambda StructuringPointer:
															StructuringPointer.IndexInt,
															self.StructuringPointersList
														)[:-1]
	#<DefineBindingHookMethods>
	


#</DefineClass>

