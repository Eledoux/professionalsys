#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class GroupingDictionnaryTesterClass(SYS.TesterClass):
	
	def test_0default(self):
		self.TestedVariable=self.TestedClass()

	def test_1group(self):
		self.TestedVariable=self.TestedClass().update(*[('GroupingHdfFile',
															SYS.HdfFileClass().update(*[
											('FolderPathString',SYS.getCurrentFolderPathString()),
											('FileString','Calcul'),
											('ModeString','r+'),
											('bind',[])
										]))])(
										'append',
										(
											'CalculGroupingDictionnary',
											{
												'TypeString':"GroupingDictionnary",
												'apply':[
															'append',
															[
																(
																	'SumGroupingDictionnary',
																	[
																		('TypeString',"GroupingDictionnary"),
																		('/GroupingScanner',[
																								('ScanningTuplesList',[
																									('SeedIntsList',[1,2]),
																									('MinFloatsList',[-10.,-100.]),
																									('MaxFloatsList',[10.,100.])
																								]),
																								('PickingVariablesList',['SeedInt',
																									'MinFloat',
																									'MaxFloat']),
																								('scan',[SYS.SumClass()])
																							]
																		)
																	]
																),
																(
																	'MulGroupingDictionnary',
																	[
																		('TypeString',"GroupingDictionnary"),
																		('/GroupingScanner',[
																								('ScanningTuplesList',[
																									('SeedIntsList',[1,2]),
																									('MinFloatsList',[-10.,-100.]),
																									('MaxFloatsList',[10.,100.])
																								]),
																								('PickingVariablesList',['SeedInt',
																									'MinFloat',
																									'MaxFloat']),
																								('scan',[SYS.MulClass()])
																							]
																		)
																	]
																)
															]
														]
											}
										)
									)
#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply',*('testify',MethodStringsList))
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply',*('bound',MethodStringsList))
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>

