#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class JoiningDictionnaryClass(
						SYS.DictionnaryClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.JoiningJoin=SYS.JoinClass()
		#</DefineSpecificDict>
			
	def appendAfter(self,_AppendingTuple):
		
		if _AppendingTuple[0]==None:

			#Delete in the self.KeyStringToIndexIntDict
			del self.KeyStringToIndexIntDict[self.KeyStringsList[-1]]

			#Change the KeyString with the join process
			"""
			self.KeyStringsList[-1]=self.JoiningPrefixString+'_'.join(
											filter(
														lambda JoiningTuple:
														JoiningTuple!=None,
														map(
															lambda JoiningVariable:
															'('+str(JoiningVariable)+','+str(_AppendingTuple[1][JoiningVariable])+')' 
															if SYS.getWithDictatedVariableAndKeyVariable(_AppendingTuple[1],JoiningVariable)!=None 
															else None,
															self.JoiningVariablesList
														)
													)
										)
			"""
			#Set the JoinVariable in the Join and get the JoinString
			self.KeyStringsList[-1]=self.JoiningJoin.__setitem__('JoinVariable',_AppendingTuple[1]).JoinString

			#Update the self.KeyStringToIndexIntDict
			self.KeyStringToIndexIntDict[self.KeyStringsList[-1]]=len(self.KeyStringsList)-1
			
	#<DefineHookMethods>


#</DefineClass>

