#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class PickingDictionnaryClass(
						SYS.DictionnaryClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.PickingVariablesList=[]
		#</DefineSpecificDict>

	def appendAfter(self,_AppendingTuple):

		if hasattr(self.ValueVariablesList[self.AppendedInt],'__getitem__') and type(self.ValueVariablesList[self.AppendedInt]) not in [str,dict,list]:
				
			#Define the PickedDict
			PickedDict=dict(
								map(
									lambda PickingVariable:
									(
										str(PickingVariable),
										SYS.getWithDictatedVariableAndKeyVariable(
											self.ValueVariablesList[self.AppendedInt],PickingVariable
											)
									),
									self.PickingVariablesList
								)
							)

			#Change with the picked version of the appended Variable
			self.ValueVariablesList[self.AppendedInt]=PickedDict

		#Return self
		return self

	#</DefineHookMethods>


#</DefineClass>

