#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class DeliveringDictionnaryTesterClass(SYS.TesterClass):
	
	def test_0default(self):
		self.TestedVariable=self.TestedClass()

	def test_1deliver(self):
		self.TestedVariable=self.TestedClass().update(**{
															'DeliveringTuplesListsList':[
																	[('MyString',"hello"),('MyInt',3)],
																	[],
																	[('MyOtherString',"coucou")]
																]
												})(
													'apply',
													*('append',
														[
															('MyFirstDict',{}),
															('YourString',"WTF"),
															('MyObject',SYS.ObjectClass())
														]
													)
												)




#</DefineTestClass>

#<Test>
TestedClass=DeliveringDictionnaryTesterClass
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
#TestedClass()('apply',*('testify',MethodStringsList))
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply',*('bound',MethodStringsList))
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>



#DeliveringDictionnary

"""
DeliveringDictionnary=SYS.DeliveringDictionnaryClass().update(**{
	'DeliveringTuplesListsList':[
							[('MyString',"hello"),('MyInt',3)],
							[],
							[('MyOtherString',"coucou")]
						]})('extend',[('MyFirstDict',{}),('YourString',"WTF"),('MyObject',SYS.ObjectClass())])
print(DeliveringDictionnary)
"""
