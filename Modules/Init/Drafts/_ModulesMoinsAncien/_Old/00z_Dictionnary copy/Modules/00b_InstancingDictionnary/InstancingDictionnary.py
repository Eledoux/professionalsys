#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
TagString="*"
#</DefineLocals>

#<DefineClass>
class InstancingDictionnaryClass(
						SYS.DictionnaryClass
				):

	#<DefineHookMethods>
	def appendAfter(self,_AppendingTuple):

		#Prepare the UpdatingTuplesList UpdatingDict
		UpdatingTuplesList=[]
		UpdatingDict={}
		InstancedTypeString=None

		#Look for a TypeString in the _AppendingTuple[1] if it is a dict or a getitemable Variable
		if type(_AppendingTuple[1])!=list:
			InstancedTypeString=SYS.getWithDictatedVariableAndKeyVariable(_AppendingTuple[1],'TypeString')

		#Get maybe if the _AppendingTuple[1] is a TuplesList and TypeString is the first element
		if InstancedTypeString==None:
			if type(_AppendingTuple[1])==list:
				if len(_AppendingTuple[1])>0:
					if type(_AppendingTuple[1][0])==tuple:
						if len(_AppendingTuple[1][0])==2:
							if _AppendingTuple[1][0][0]=='TypeString':
								InstancedTypeString=_AppendingTuple[1][0][1]
								UpdatingTuplesList=_AppendingTuple[1]
		else:
			
			#Set the UpdatingDict then because the _AppendingTuple[1] is a dictated variable
			UpdatingDict=SYS.getDictWithDictatedVariable(_AppendingTuple[1])

		#Instancify
		if InstancedTypeString!=None:

			#Define the InstancedClassString
			InstancedClass=getattr(
									SYS,
									SYS.getClassStringWithTypeString(InstancedTypeString)
									)

			if InstancedClass!=None:

				#Define the NewVariable
				NewVariable=InstancedClass().update(*UpdatingTuplesList,**UpdatingDict)

				#Change the pointer
				self.ValueVariablesList[self.AppendedInt]=NewVariable

		#Return self
		return self
	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>


#</DefineClass>

