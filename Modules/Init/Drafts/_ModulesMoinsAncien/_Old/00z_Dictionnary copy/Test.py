#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class DictionnaryTesterClass(SYS.TesterClass):
	
	def test_0default(self):
		self.TestedVariable=self.TestedClass()

	def test_1append(self):
		self.TestedVariable=self.TestedClass()(
												'append',
												('MyDict',{'MyString':"Hello"})
											)

	def test_2mapAppend(self):
		self.TestedVariable=self.TestedClass()(
												'apply',
												*(
													'append',
													[
														('MyDict',{'MyString':"Hello"}),
														('MyInt',1)
													]
												)
											)

	def test_3insert(self):
		self.TestedVariable=self.TestedClass()(
												'apply',
												*(
													'append',
													[
														('MyDict',{'MyString':"Hello"}),
														('MyInt',1)
													]
												)
											)(
												'insert',
												*(
													1,
													('MyOtherString',"WTF")
												)
											)

	def test_4add(self):
		self.TestedVariable=self.TestedClass()+[
													('MyDict',{'MyString':"Hello"}),
													('MyInt',1)
												]

	def test_5update(self):
		self.TestedVariable=self.TestedClass().update(**{'*MyDict':{'MyString':"Hello"}})

	def test_6getString(self):
		self.TestedVariable=self.TestedClass()(
												'apply',
												*(
													'append',
													[
														('MyDict',{'MyString':"Hello"}),
														('MyInt',1)
													]
												)
											)(
												'insert',
												*(
													1,
													('MyOtherString',"WTF")
												)
											)['*MyDict']

	def test_7getInt(self):
		self.TestedVariable=self.TestedClass()(
												'apply',
												*(
													'append',
													[
														('MyDict',{'MyString':"Hello"}),
														('MyInt',1)
													]
												)
											)(
												'insert',
												*(
													1,
													('MyOtherString',"WTF")
												)
											)[0]

	def test_8updateAppend(self):
		self.TestedVariable=self.TestedClass().update(*[
															('append',[('MyString',"hello")]),
															('apply',['append',[('MyInt',0),('MyFloat',0.1)]])
														])			

#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply','testify',MethodStringsList)
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply','bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>
