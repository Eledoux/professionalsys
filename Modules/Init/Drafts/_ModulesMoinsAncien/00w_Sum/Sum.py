#<ImportModules>
import ShareYourSystem as SYS
import scipy.stats
#</ImportModules>

#<DefineClass>
class SumClass(SYS.SeedClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.SumIntsList=[0,0]
		self.MinFloat=-100.
		self.MaxFloat=100.
		self.SumInt=0
		#</DefineSpecificDict>

	def bindAfter(self):

		#Bind with SumIntsList setting
		self['SumIntsList']=map(int,scipy.floor(self.MinFloat+(self.MaxFloat-self.MinFloat)*scipy.stats.uniform.rvs(size=2)))

		#Return self
		return self
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindSumIntsListAfter(self):

		#Bind with SumInt setting
		self['SumInt']=sum(self.SumIntsList)
	#</DefineBindingHookMethods>	
	

#</DefineClass>