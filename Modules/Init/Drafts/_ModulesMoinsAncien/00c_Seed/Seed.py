#<ImportModules>
import ShareYourSystem as SYS
import numpy as np
import time
#</ImportModules>

#<DefineClass>
class SeedClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.SeedInt=1
		#</DefineSpecificDict>

	def bindBefore(self):

		#Bind with SeedInt setting
		self['SeedInt']=(int)(time.time())
		
		#Return self
		return self

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindSeedIntAfter(self):

		#Bind with numpy random seed
		np.random.seed(self.SeedInt)
		
	#</DefineBindingHookMethods>	

	#<DefineMethods>
	#<DefineMethods>	

#</DefineClass>