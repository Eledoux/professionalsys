#<ImportModules>
import sys
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SortClass(
					SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.DictionnaryPointer=None
		self.FunctionPointer=None
		self.ListsList=[]
		#</DefineSpecificDict>
		

	def bindAfter(self):

		#Group by
		if self.FunctionPointer!=None:
			self.ListsList=SYS.groupby(self.FunctionPointer,self.DictionnaryPointer('items'))

		#Return self
		return self
	#</DefineHookMethods>


#</DefineClass>