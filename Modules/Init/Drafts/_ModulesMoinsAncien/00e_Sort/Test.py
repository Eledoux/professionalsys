#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class SortTesterClass(SYS.TesterClass):
	
	def test_0default(self):
		self.TestedVariable=self.TestedClass()

	def test_1bind(self):
		self.TestedVariable=self.TestedClass().update(*[
															('FunctionPointer',lambda _Tuple:type(_Tuple[1])==str),
															('DictionnaryPointer',SYS.DictionnaryClass()),
															('/DictionnaryPointer',(
																	'apply',
																	['append',[
																				('MyString',"hello"),
																				('MyInt',0)
																			]
																	]
																)
															)
														])('bind')
														
#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply',*('testify',MethodStringsList))
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply',*('bound',MethodStringsList))
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>

