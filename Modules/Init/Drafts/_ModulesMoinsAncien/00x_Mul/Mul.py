#<ImportModules>
import ShareYourSystem as SYS
import numpy
import scipy.stats
#</ImportModules>

#<DefineClass>
class MulClass(SYS.SeedClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.MulIntsList=[0,0]
		self.MinFloat=-100.
		self.MaxFloat=100.
		self.MulInt=0
		#</DefineSpecificDict>

	def bindAfter(self):

		#Bind with SumIntsList setting
		self['MulIntsList']=map(int,scipy.floor(self.MinFloat+(self.MaxFloat-self.MinFloat)*scipy.stats.uniform.rvs(size=2)))
	
		#Return self
		return self
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindMulIntsListAfter(self):

		#Bind with MulInt setting
		self['MulInt']=numpy.prod(self.MulIntsList)
	#</DefineBindingHookMethods>	
	

#</DefineClass>