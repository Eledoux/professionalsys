#<ImportModules>
import ShareYourSystem as SYS
import time
#</ImportModules>

#<DefineClass>
class GroupClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.GroupHdfFile=SYS.HdfFileClass()
		self.GroupTuplesList=[]
		self.GroupVariable=None
		#</DefineSpecificDict>

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindGroupTuplesListAfter(self):

		#Set the ModeString
		self.GroupHdfFile['ModeString']='w'

		#Then read the group to be written
		map(
				lambda _GroupTuple:
				self.setGroupWithGroupTuple(_GroupTuple),
				self.GroupTuplesList
			)
		
	def bindGroupStringAfter(self):

		if self.GroupVariable==None:
			self.GroupVariable=self.GroupHdfFile.H5pyFile

		print(self.GroupString not in self.GroupVariable,self.GroupVariable)
		if self.GroupString not in self.GroupVariable:
			self.GroupVariable.create_group(self.GroupString)
			print("l",self.GroupVariable.keys())

	#</DefineBindingHookMethods>	

	#<DefineMethods>
	def setGroupWithGroupTuple(self,_GroupTuple):

		#Update the GroupPathString
		self['GroupString']=_GroupTuple[0]

		#Get the possible child GroupTuplesList
		GettedVariable=SYS.getWithDictatedVariableAndKeyVariable(_GroupTuple[1],'GroupTuplesList')

		if GettedVariable!=None:
			return SYS.StoreClass().__setitem__('GroupTuplesList',_GroupTuple[1]['GroupTuplesList'])

	#</DefineMethods>

#</DefineClass>