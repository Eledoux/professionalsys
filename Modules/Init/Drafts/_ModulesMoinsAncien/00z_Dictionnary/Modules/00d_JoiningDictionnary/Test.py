#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class JoiningDictionnaryTesterClass(SYS.TesterClass):
	
	def test_0default(self):
		self.TestedVariable=self.TestedClass()

	def test_1join(self):
		self.TestedVariable=self.TestedClass().update(*[
															#('JoiningVariablesList',['/__class__/TypeString']),
															('/JoiningJoin/JoinKeyVariablesList',['/__class__/TypeString'])
													])(
														'apply',
														*(
															'append',
															[
																(None,SYS.ObjectClass()),
																(None,SYS.DictionnaryClass()),
																(None,SYS.ObjectClass())
															]
														)
													)

#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply',*('testify',MethodStringsList))
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply',*('bound',MethodStringsList))
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>

