#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
TagString="*"
#</DefineLocals>

#<DefineClass>
class RecordingDictionnaryClass(
								SYS.DeepPickingDictionnaryClass,
								SYS.JoiningDictionnaryClass		
				):

	#<DefineHookMethods>
	def appendBefore(self,_AppendingTuple):

		if SYS.ObjectClass in type(_AppendingTuple[1]).__mro__:
			
			#Bind before recording
			_AppendingTuple[1]('bind')

	#</DefineHookMethods>


#</DefineClass>

