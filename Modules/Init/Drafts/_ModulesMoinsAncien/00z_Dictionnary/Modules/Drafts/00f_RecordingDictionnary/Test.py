#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class RecordingDictionnaryTesterClass(SYS.TesterClass):
	
	def test_0default(self):
		self.TestedVariable=self.TestedClass()

	def test_1record(self):

		#Define an Object
		Object=SYS.ObjectClass()

		#Append a first time
		Object.update(**{
						'MyGrandChildObject':SYS.ObjectClass().update(**{
							'MyString':"hello"
							})
						})
		TestedClass=self.TestedClass().update(**{
									'PickingVariablesList':['MyGrandChildObject'],
									'/JoiningJoin/JoinKeyVariablesList':['/MyGrandChildObject/MyString']
									})(
																'apply',
																*(
																	'append',
																	[
																		(None,{'MyInt':0}),
																		('YourString',"WTF"),
																		(None,Object)
																	]
																)
															)
														
		#Append a second time with updating the MyGrandChildObject (but "hello" will replaced by coucou then in the first slot)
		Object.MyGrandChildObject['MyString']="coucou"
		self.TestedVariable=TestedClass(
											'append',
											(None,Object)
										)

	"""
	def test_2record(self):

		#Define an Object
		Commit=SYS.CommitClass()

		#Append a first time
		Object.update(**{
							'MyCommit':SYS.CommitClass()
						})
		TestedClass=self.TestedClass().update(**{'PickingVariablesList':['MyCommit']})(
																'apply',
																*(
																	'append',
																	[
																		('MyFirstDict',{'MyInt':0}),
																		('YourString',"WTF"),
																		('MyPickedObjectDict',Object)
																	]
																)
															)
														
		#Append a second time with updating the MyGrandChildObject (but "hello" will replaced by coucou then in the first slot)
		self.TestedVariable=TestedClass(
											'append',
											('MyPickedObjectDict',Object)
										)
	"""

#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply',*('testify',MethodStringsList))
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply',*('bound',MethodStringsList))
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>

