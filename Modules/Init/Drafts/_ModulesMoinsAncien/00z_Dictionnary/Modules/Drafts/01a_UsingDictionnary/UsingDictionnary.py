#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
TagString="*"
#</DefineLocals>


#<DefineClass>
class UsingDictionnaryClass(
								SYS.JoiningDictionnaryClass,
								SYS.InstancingDictionnaryClass
								
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.UsingTypeString=self.__class__.TypeString
		#</DefineSpecificDict>

		#Set the JoiningKeyStringsList
		self['JoiningKeyStringsList']=['/__class__/TypeString','TypeString']

	def appendBefore(self,_UsingTuple):

		UsedTypeString=SYS.getWithDictatedVariableAndKeyVariable(_UsingTuple[1],'TypeString')
		if UsedTypeString!=None:

			UsedTypeString='Of'+self.UsingTypeString+'_'+UsedTypeString
			_UsingTuple[1]['TypeString']=UsedTypeString

	def appendAfter(self,_UsingTuple):

		#Remove the TypeString
		if hasattr(self.ValueVariablesList[-1],'TypeString'):
			del self.ValueVariablesList[-1]['TypeString']

		#Add the UsingPointer in the UsedVariable
		if hasattr(self.ValueVariablesList[-1],'UsingPointer'):
			self.ValueVariablesList[-1]['UsingPointer']=self

	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>


#</DefineClass>

