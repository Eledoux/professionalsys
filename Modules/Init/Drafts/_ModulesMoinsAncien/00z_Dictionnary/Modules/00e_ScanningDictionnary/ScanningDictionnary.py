#<ImportModules>
import ShareYourSystem as SYS
import itertools
#</ImportModules>

#<DefineClass>
class ScanningDictionnaryClass(
								SYS.DeepPickingDictionnaryClass,
								SYS.JoiningDictionnaryClass,
								SYS.DeliveringDictionnaryClass,
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.ScanningTuplesList=[]
		self.ScanningValueVariablesList=[]
		self.IsScanningBool=True
		self.ScanningPointer=None
		#</DefineSpecificDict>
		
	def appendBefore(self,_AppendingTuple):

		if self.IsScanningBool:

			#Bind with DeliveringTuplesListsList setting
			self['DeliveringTuplesListsList']=map(
										lambda ProductTuple:
										zip(self.JoiningJoin.JoinKeyVariablesList,ProductTuple),
										self.ScanningValueVariablesList
								)

			#Set the IsScanningBool to False
			self.IsScanningBool=False

			#Apply for all these products
			self('apply',*(
							'append',
							[
								(None,_AppendingTuple[1]) for IndexInt in xrange(
												len(self.DeliveringTuplesListsList
													)
												)
							]
						)
				)

			#Set the IsScanningBool to True
			self.IsScanningBool=True

			#Set the append to end
			self.IsCallingBool=False

		#Return self
		return self
	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindScanningTuplesListAfter(self):

		#Bind with JoiningVariablesList setting
		self['/JoiningJoin/JoinKeyVariablesList']=map(
							lambda KeyString:
							SYS.getSingularStringWithPluralString(
								KeyString.split("List")[0]
								),
							map(
								lambda Tuple:
								Tuple[0],
								self.ScanningTuplesList
							)
						)

		#Bind with ScanningValueVariablesList setting
		self['ScanningValueVariablesList']=list(
							itertools.product(
								*map(
										lambda Tuple:
										Tuple[1],
										self.ScanningTuplesList
									)
								)
							)

	#<DefineBindingHookMethods>
	


#</DefineClass>

