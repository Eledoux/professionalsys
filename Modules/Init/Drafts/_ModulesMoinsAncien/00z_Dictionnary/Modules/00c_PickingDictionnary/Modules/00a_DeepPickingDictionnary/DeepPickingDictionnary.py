#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DeepPickingDictionnaryClass(
						SYS.PickingDictionnaryClass
				):

	#<DefineHookMethods>
	def appendAfter(self,_AppendingTuple):

		#Change the PickedDict as a deepcopy
		self.ValueVariablesList[self.AppendedInt]=SYS.sys.modules['copy'].deepcopy(self.ValueVariablesList[self.AppendedInt])

		#Return self
		return self

	#</DefineHookMethods>


#</DefineClass>

