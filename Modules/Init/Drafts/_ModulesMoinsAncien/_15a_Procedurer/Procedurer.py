#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>




#<DefineClass>
class ProcedurerClass(
						SYS.UserClass,
						SYS.PatherClass
					):
    
	#<DefineHookMethods>
	def initAfterBasesWithProcedurer(self):

		#<DefineSpecificDict>
		self.ProceduringTypeString=""
		#</DefineSpecificDict>

		#Use a Scanner
		self(**{
					'TypeDictString':"Use",
					'UsedTypeString':"Scanner",
					'UsingTypeString':"Procedurer"
				})

		#Use a Recorder
		self(**{
					'TypeDictString':"Use",
					'UsedTypeString':"Recorder",
					'UsingTypeString':"Procedurer"
				})

		#Use a Committer
		self(**{
					'TypeDictString':"Use",
					'UsedTypeString':"Committer",
					'UsingTypeString':"Procedurer"
				})

	def callAfterBasesWithProcedurer(self,*_ArgsList,**_KwargsDict):
	
		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="Procedure":

				#Short alias setting
				Scanner=self['<Used>OfProcedurer_Scanner']
				Committer=self['<Used>OfProcedurer_Committer']
				Recorder=self['<Used>OfProcedurer_Recorder']

				#Reupdate possible attributes
				for KeyString in ['ProceduringTypeString']:
					if KeyString in _KwargsDict:
						self[KeyString]=_KwargsDict[KeyString]

				#Reupdate possible attributes
				if "ScannedDict" in _KwargsDict:
					Scanner['ScannedDict']=_KwargsDict['ScannedDict']

				"""
				#Call for each ScannedTuplesList a new recorded Variable 
				RecordersList=map(
						lambda IntAndScannedTuplesList:
							Recorder(**{
									'TypeDictString':"Use",
									'UsedTypeString':self.ProceduringTypeString,
									'UsingTypeString':"Recorder",
									'PrefixString':Scanner.ScannedStringsList[IntAndScannedTuplesList[0]]
							})['<Used>'+Scanner.ScannedStringsList[IntAndScannedTuplesList[0]]].update(
							IntAndScannedTuplesList[1]),
						enumerate(Scanner.ScannedTuplesListsList)
					)
				"""
				
				#Call for each ScannedTuplesList a new RecordedObject Variable 
				RecordedObjectsList=map(
						lambda IntAndScannedTuplesList:
							getattr(
									SYS,
									"OfRecorder_"+SYS.getClassStringWithTypeString(self.ProceduringTypeString)
									)().update(
												IntAndScannedTuplesList[1]
											),
						enumerate(Scanner.ScannedTuplesListsList)
					)

				print(RecordedObjectsList)

				"""
				#Do the Procedure for each
				map(
					lambda Recorder:
					Committer(**{'TypeDictString':"Commit",'HdfiledDict':Recorder.RecordingDict)
					,RecordersList
					)
				"""

	def reprAfterBasesWithProcedurer(self,_SelfString,_PrintedDict):

		#Define the NewPrintedDict
		NewPrintedDict={
							'Recorder':self['<Used>Recorder'],
							'Scanner':self['<Used>Scanner'],
							'Committer':self['<Used>Committer']
		}

		#Define the new SelfString
		SelfString=SYS.ConsolePrinter.getPrintedVariableString(NewPrintedDict)

		#Return
		return (SelfString,NewPrintedDict)

    #</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setProceduringTypeStringAfterBasesWithProcedurer(self):

		#Bind with a set of an Object of ProceduringTypeString in the Recorder
		self['<Used>Recorder'](**{
									'TypeDictString':"Use",
									'UsedTypeString':self.ProceduringTypeString,
									'UsingTypeString':"Recorder"
								})

	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>
#</DefineClass>


