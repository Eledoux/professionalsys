#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>


#<DefineClass>
class InstancerClass(
						SYS.DictionnaryClass
					):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.InstancedVariable=None
		self.InstancingTypeString=""
		self.InstancingPrefixString=""
		#</DefineSpecificDict>

	def instanceBefore(self,*_ArgsList,**_KwargsDict):

		#Init InstancedVariable
		self.InstancedVariable=None

	def instanceAfter(self,*_ArgsList,**_KwargsDict):

		#Instancify
		self['InstancedVariable']=SYS.getTypedVariableWithVariableAndTypeString(
			self.InstancedVariable,
			self.InstancingTypeString
		)

		#Check if it is ok
		if self.InstancedVariable==None:
			print("WARNING in Instancer : didn't find an Instance like "+self.InstancingTypeString)
		else:

			#Define the KeyString
			KeyString=self.InstancingPrefixString+SYS.getTypeStringWithClassString(SYS.getUppedString(type(self.InstancedVariable).__name__))

			#Add a Count if there is already this KeyString
			CountInt=0
			while KeyString in self.KeyStringsList:
				CountInt+=1
				KeyString+='('+str(CountInt)+')'
				
			#Then append
			self('append',(
							KeyString,
							self.InstancedVariable
						)
			)

		#Return self
		return self

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindInstancedVariableAfter(self):

		#Bind with InstancingTypeString setting
		if self.InstancedVariable!=None:

			#Set the InstancingTypeString
			self.InstancingTypeString=self.InstancedVariable.__class__.TypeString
	
			#Give him the InstancingDict
			self.InstancedVariable.update(*self.InstancingTuplesList)

			#Give him the InstancingDict
			self.InstancedVariable.update(**self.InstancingDict)

	def bindInstancingDictAfter(self):

		#Bind with InstancedVariable setting
		if self.InstancedVariable!=None:
			self.InstancedVariable.update(self.InstancingDict)

	def bindInstancingTuplesListAfter(self):

		#Bind with InstancedVariable setting
		if self.InstancedVariable!=None:
			self.InstancedVariable.update(self.InstancingTuplesList)
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>


