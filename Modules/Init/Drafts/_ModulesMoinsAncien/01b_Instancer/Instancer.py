#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>


#<DefineClass>
class InstancerClass(
						SYS.InstancingDictionnaryClass
					):

	#<DefineHookMethods>
	def instanceAfter(self,_InstancedVariable,*_InstancedVariablesList):

		#Append then with this _AppendingKeyString
		self('append',_InstancedVariable)

		#Do for each JoiningVariable
		self('apply',*('append',_InstancedVariablesList))

		#Return self
		return self
	#</DefineHookMethods>

#</DefineClass>


