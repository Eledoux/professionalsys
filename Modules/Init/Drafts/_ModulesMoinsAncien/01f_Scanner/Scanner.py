#<ImportModules>
import itertools
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Scan"
DoingString="Scanning"
DoneString="Scanned"
#</DefineLocals>

#<DefineClass>
class ScannerClass(SYS.ScanningDictionnaryClass):

	#<DefineHookMethods>
	def scanAfter(self,_ScannedVariable):
		
		#Append with a None,_ScannedVariable to launch the scan process
		self(
				'append',
				(None,_ScannedVariable)
			)

		#Return self
		return self
	#</DefineHookMethods>

#</DefineClass>