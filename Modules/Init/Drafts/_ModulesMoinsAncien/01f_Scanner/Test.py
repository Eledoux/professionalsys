#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class ScannerTesterClass(SYS.TesterClass):
	
	def test_0default(self):
		self.TestedVariable=self.TestedClass()

	def test_1scan(self):
		self.TestedVariable=self.TestedClass().update(**{
				'ScanningTuplesList':[
										('FirstParamFloatsList',[0.,0.1]),
										('SecondParamFloatsList',[0.,1.,2.])
									],
				'PickingVariablesList':['FirstParamFloat','SecondParamFloat','MyString']
			})('scan',SYS.ObjectClass().__setitem__('MyString',"hello"))

	def test_2scan(self):
		self.TestedVariable=self.TestedClass().update(*[
															('ScanningTuplesList',[
																('SeedIntsList',[1,2]),
																('MinFloatsList',[-10.,-100.]),
																('MaxFloatsList',[10.,100.])
															]),
															('PickingVariablesList',['SeedInt',
																'MinFloat',
																'MaxFloat']),
															('scan',[SYS.SumClass()])
														])
#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply',*('testify',MethodStringsList))
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply',*('bound',MethodStringsList))
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>

