#<ImportModules>
import ShareYourSystem as SYS
import h5py
#</ImportModules>

#<DefineFunctions>
#</DefineFunctions>

#<DefineClass>
class OfHdfiler_FileClass(
							SYS.FileClass
						):

	#<DefineHookMethods>
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setFilePathStringAfterBasesWithOfHdfiler_File(self):
		
		#Bind with setFilePointerWithOfHdfiler_File
		self.setFilePointerWithOfHdfiler_File()

	def setModeStringAfterBasesWithOfHdfiler_File(self):

		#Bind with setFilePointerWithOfHdfiler_File
		self.setFilePointerWithOfHdfiler_File()
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setFilePointerWithOfHdfiler_File(self):

		#Open the File Pointer
		if self.FilePathString!="":
			self['FilePointer']=h5py.File(self.FilePathString,self.ModeString)
	#</DefineMethods>

#</DefineClass>

