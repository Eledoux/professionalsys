#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class LilypondFileClass(SYS.TextFileClass):

	#<DefineHookMethods>
	def initAfterBasesWithLilypondFile(self):

		#<DefineSpecificDict>
		self.LilypondString=""
		self.ExecPathString="/Applications/LilyPond.app/Contents/Resources/bin/lilypond"
		self.OutputExtensionString=".pdf"
		self.ExtensionString=".ly"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["LilypondFile"]=[
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setLilypondStringAfterBasesWithLilypondFile(self):

		#Bind with TextString setting
		self['TextString']=self.LilypondString

		#Bind with ModeString setting
		self['ModeString']="w"

	def setFilePathStringAfterBasesWithLilypondFile(self):

		self['ModeString']="w"
		self.FilePointer=open(self.FilePathString,self.ModeString)

	def setTextStringAfterBasesWithLilypondFile(self):

		if self.OutputExtensionString==".svg":
			#Bind with exec command
			SYS.sys.modules['os'].popen('exec '+self.ExecPathString+' -fsvg '+self.FilePathString)
		else:
			#Bind with exec command
			SYS.sys.modules['os'].popen('exec '+self.ExecPathString+' '+self.FilePathString)

	#</DefineBindingHookMethods>

#</DefineClass>