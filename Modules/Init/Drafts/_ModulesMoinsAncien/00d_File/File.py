#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class FileClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.FilePathString=""
		self.FolderPathString=""
		self.ExtensionString=""
		self.FileString=""
		self.ModeString="r"
		#</DefineSpecificDict>

	def bindAfter(self):
		self['FilePathString']=self.FolderPathString+self.FileString
		if self.ExtensionString!="":
			self['FilePathString']+="."+self.ExtensionString
	#</DefineHookMethods>	

#</DefineClass>