#<ImportModules>
import ShareYourSystem as SYS
import h5py
#</ImportModules>

#<DefineClass>
class HdfFileClass(SYS.FileClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.H5pyFile=None
		#</DefineSpecificDict>

		#Update the ExtensionString
		self['ExtensionString']="hdf5"

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindAfter(self):

		#If the mode has changed then close
		if hasattr(self.H5pyFile,'mode'):
			if self.H5pyFile.mode!=self.ModeString:
				self.H5pyFile.close()


		#Check for a good FilePathString
		if self.FilePathString!="":

			#Check for first write
			if SYS.sys.modules['os'].path.isfile(self.FilePathString)==False:
				self.H5pyFile=h5py.File(self.FilePathString,'w')
				self.H5pyFile.close()

			#Open with the good Mode so close if it is different
			if hasattr(self.H5pyFile,'mode'):
				if self.H5pyFile.mode!=self.ModeString:
					self.H5pyFile.close()
			
			#Open
			self.H5pyFile=h5py.File(self.FilePathString,self.ModeString)
			
	#</DefineBindingHookMethods>		

#</DefineClass>