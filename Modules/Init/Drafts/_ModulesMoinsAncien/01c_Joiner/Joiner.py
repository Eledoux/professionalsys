#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class JoinerClass(
					SYS.JoiningDictionnaryClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.JoiningKeyStringsList=[]
		self.JoiningPrefixString=""
		#</DefineSpecificDict>
			
	def joinAfter(self,_JoinedVariable,*_JoinedVariablesList):

		#Append then with this _AppendingKeyString
		self('append',(None,_JoinedVariable))

		#Do for each JoiningVariable
		self('apply',*(
							'append',
							map(
								lambda _JoiningVariable:
								(None,_JoiningVariable),
								_JoinedVariablesList)
							)
		)

		#Return self
		return self
	#<DefineHookMethods>

#</DefineClass>

