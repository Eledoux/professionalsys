#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
#</DefineLocals>

#<DefineClass>
class JoinerClass(
					SYS.DictionnaryClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.JoiningKeyStringsList=[]
		self.JoiningPrefixString=""
		#</DefineSpecificDict>
			
	def joinBefore(self,_JoiningVariable,*_JoiningVariablesList):

		#Get the _AppendingKeyString		
		_AppendingKeyString=self.JoiningPrefixString+'_'.join(
										map(
												lambda JoiningKeyString:
												'('+JoiningKeyString+','+str(_JoiningVariable[JoiningKeyString])+')' 
												if hasattr(_JoiningVariable,'__getitem__') and _JoiningVariable[JoiningKeyString]!=None else None,
												self.JoiningKeyStringsList
											)
									)

		#Append then with this _AppendingKeyString
		self('append',(_AppendingKeyString,_JoiningVariable))

		#Do for each JoiningVariable
		map(
				lambda JoiningVariable:
				self('join',JoiningVariable),
				_JoiningVariablesList
			)

		#Return self
		return self
		

	#<DefineHookMethods>

	#<DefineMethods>

	#</DefineMethods>


#</DefineClass>

