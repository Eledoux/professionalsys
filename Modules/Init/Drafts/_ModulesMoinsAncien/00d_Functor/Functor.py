#<ImportModules>
import sys
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Func"
DoingString="Functing"
DoneString="Functed"
#<DefineClass>

#<DefineClass>
class FunctorClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.FunctionPointer=None
		self.ArgsList=[]
		self.KwargsDict={}
		#</DefineSpecificDict>
											
	def funcAfter(self,_CallString,*_ArgsList,**_KwargsDict):

		#Define a default OutputVariable
		OutputVariable=None

		#Call the FunctionPointer
		if callable(self.FunctionPointer):

			if SYS.getIsKwargsFunctionBool(self.FunctionPointer):
				OutputVariable=self.FunctionPointer(*self.ArgsList,**self.KwargsDict)
			else:
				OutputVariable=self.FunctionPointer(*self.ArgsList)

		#Return the OutputVariable
		return OutputVariable
		
	#</DefineHookMethods>


#</DefineClass>