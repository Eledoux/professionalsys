#<ImportModules>
import ShareYourSystem as SYS
import time
#</ImportModules>

#<DefineClass>
class PickClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.PickKeyVariablesList=[]
		self.PickVariable=None
		self.PickDict={}
		#</DefineSpecificDict>
	
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindPickKeyVariablesListAfter(self):

		#Bind with JoinString setting
		self.setPickDict()
		
	def bindPickVariableAfter(self):

		#Bind with JoinString setting
		self.setPickDict()

	#</DefineBindingHookMethods>	

	#<DefineMethods>
	def setPickDict(self):
		
		#Get all the getted Values corresponding to the PickKeyVariablesList
		self['PickedDict']=dict(
								map(
									lambda PickingKeyVariable:
									(
										str(PickingKeyVariable),
										SYS.getWithDictatedVariableAndKeyVariable(
											self.PickVariable,PickingVariable
											)
									),
									self.PickingKeyVariablesList
								)
							)
	#<DefineMethods>	

#</DefineClass>