#<ImportModules>
import ShareYourSystem as SYS
import scipy.stats
#</ImportModules>

#<DefineClass>
class CalculClass(
					SYS.UseClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self('apply',*('append',[
							('Sum',SYS.SumClass()),
							('Mul',SYS.MulClass()),
						]
					)
			)
		self.CalculInt=0
		self.CalculString="Develop"
		#</DefineSpecificDict>

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	'''
	def bindSeedIntAfter(self):

			#Bind with CalculInt setting
			if self.CalculString=="Factor":
				self.CalculInt=self.CalculMul.__setitem__(
					'MulIntsList',
					[
						self.CalculSum.__setitem__('SeedInt',self.SeedInt).SumInt,
						self.CalculSum.SumInt
					]
					).MulInt
			elif self.CalculString=="Develop":
				self.CalculInt=self.CalculSum.__setitem__(
					'SumIntsList',
					[
						self.CalculMul.__setitem__('SeedInt',self.SeedInt+1).MulInt,
						self.CalculMul.MulInt
					]
					).SumInt
	'''
	#</DefineBindingHookMethods>	
	

#</DefineClass>