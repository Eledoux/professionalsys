#<ImportModules>
import ShareYourSystem as SYS
import itertools
#</ImportModules>

#<DefineClass>
class StructurerClass(
						SYS.InstancerClass,
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.StructuringParentPointersList=[]
		self.StructuringChildPointersList=[]
		self.StructuringIntsList=[]
		#</DefineSpecificDict>
	
	def appendAfter(self):

		#bind with StructuringChildStructurersList setting
		self['StructuringChildPointersList']=filter(
														lambda __AppendedValueVariable:
														SYS.StructurerClass in type(__AppendedValueVariable).__mro__,
														self.AppendedValueVariablesList
													)

		#Define a Short cut
		AppendedVariable=self.AppendedValueVariablesList[self.AppendedInt]

		#Add the self.StructuringParentPointersList plus self
		if hasattr(AppendedVariable,'StructuringParentPointersList'):
			AppendedVariable['StructuringParentPointersList']=[self]+self.StructuringParentPointersList

	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindStructuringPointersListAfter(self):

		#Give to all the derived from StructuringObject the StructuringPointersList and StructuringIntsList
		map(
				lambda __StructuringChildStructurerPointer:
				__StructuringChildStructurerPointer.__setitem__('StructuringPointersList',[self]+self.StructuringPointersList),
				self.StructuringChildStructurerPointersList
			)

		#Bind with StructuringIntsList setting
		self['StructuringIntsList']=[self.AppendedInt]+map(
															lambda StructuringPointer:
															StructuringPointer.AppendedInt,
															self.StructuringPointersList
														)[:-1]
	#<DefineBindingHookMethods>
	


#</DefineClass>

