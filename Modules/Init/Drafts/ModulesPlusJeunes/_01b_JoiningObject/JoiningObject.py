#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>


def getIsPickableVariableBool(_Variable):
	Type=type(_Variable)
	return SYS.ObjectClass in Type.__mro__ or Type==dict or SYS.getIsTuplesListBool(_Variable)


def getPickedTuplesListWithPickableVariable(_PickableVariable):
	Type=type(_Variable)
	if SYS.ObjectClass in Type.__mro__:
		self.AppendedVariablesList[self.AppendedInt].pick()
			

#<DefineClass>
class JoiningObjectClass(
					SYS.ObjectClass
				):

	#<DefineHookMethods>
	def appendBefore(self):

		#Change the AppendingVariable into a default tuple
		self.AppendingVariable=(None,self.AppendingVariable)

	def appendAfter(self):
		
		#Check that is an Object
		if getIsPickableVariableBool(self.AppendedVariablesList[self.AppendedInt]):

			#Delete in the self.KeyStringToAppendedIntDict
			del self.AppendedStringToAppendedIntDict[self.AppendedStringsList[-1]]

			#Define the Type
			Type=type(self.AppendedVariablesList[self.AppendedInt])
			
			#Get the joined PickedTulplesList
			PickedTulplesList=[]
			if SYS.ObjectClass in Type.__mro__:
				PickedTulplesList=self.AppendedVariablesList[self.AppendedInt].pick()
			elif Type==dict:
				PickedTulplesList=self.AppendedVariablesList[self.AppendedInt].PickingKeyVariablesList
			elif SYS.getIsTuplesListBool(self.AppendedVariablesList[self.AppendedInt]):
				PickedTulplesList=self.AppendedVariablesList[self.AppendedInt].PickingKeyVariablesList
				
			#Build the JoinString and change the AppendedString
			self.AppendedStringsList[self.AppendedInt]="_".join(
				map(
					lambda _PickedTuple:
					"("+str(_PickedTuple[0])+","+str(_PickedTuple[1])+")",
					PickedTulplesList
					)
				)

			#Update the self.AppendedStringToAppendedIntDict
			self.AppendedStringToAppendedIntDict[self.AppendedStringsList[-1]]=len(self.AppendedStringsList)-1	
	#<DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>


#</DefineClass>

