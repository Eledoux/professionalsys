#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class JoinClass(SYS.ObjectClass):
	"""
		Join is an Object

		The attributes are :

		-UpdatedPointer that has to be a BasedObject of a dict

		-OrderedVariablesList is a <TuplesList>
		to be defined, 

		-UpdatingTuplesList is the OrderedVariablesList 
		plus the picked tuples from the UpdatedPointer.DeliveringKeyVariablesList

		Once the UpdatedPointer is setted, it is updated with UpdatingTuplesList
	"""

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.KeyVariablesList=[]
		self.KeyString=""
		self.PickedPointer=None
		#</DefineSpecificDict>

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindKeyVariablesListAfter(self):

		#Bind with JoinString setting
		self.setKeyString()
		
	def bindValuePointerAfter(self):

		#Bind with JoinString setting
		self.setKeyString()

	#</DefineBindingHookMethods>	

	#<DefineMethods>
	def setKeyString(self):
		
		#Join the self.KeyVariablesList with their corresponding getted Values
		self['KeyString']='_'.join(
									filter(
												lambda _KeyTuple:
												_KeyTuple!=None,
												map(
													lambda _KeyVariable:
													'('+str(_KeyVariable)+','+str(self.ValuePointer[_KeyVariable])+')' 
													if SYS.getWithDictatedVariableAndKeyVariable(self.ValuePointer,_KeyVariable)!=None 
													else None,
													self.KeyVariablesList
												)
											)
									)
	#<DefineMethods>	

#</DefineClass>