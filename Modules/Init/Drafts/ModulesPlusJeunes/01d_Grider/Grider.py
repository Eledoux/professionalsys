#<ImportModules>
import ShareYourSystem as SYS
import itertools
#</ImportModules>

#<DefineClass>
class GriderClass(
						SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.GridingTuplesList=[]
		self.GridingValueVariablesList=[]
		#self.IsScanningBool=True
		#</DefineSpecificDict>
		
	def appendBefore(self):

		#Update the value to be picked
		self.update(zip(self.PickingKeyVariablesList,self.GridingValueVariablesList[self.AppendedInt]))
		
		#Set PickingKeyVariablesList in the AppendingVariable to make the join
		self.AppendingVariable['PickingKeyVariablesList']=self.PickingKeyVariablesList
		
		#Link
		self.link(self.AppendingVariable)
	#<DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindGridingTuplesListAfter(self):

		#Bind with KeyStringsList setting
		self['PickingKeyVariablesList']=map(
										lambda _KeyString:
										SYS.getSingularStringWithPluralString(
											_KeyString.split("List")[0]
											),
										map(
											lambda _Tuple:
											_Tuple[0],
											self.GridingTuplesList
										)
									)

		#Bind with ValueVariablesList setting
		self['GridingValueVariablesList']=list(
												itertools.product(
													*map(
															lambda _Tuple:
															_Tuple[1],
															self.GridingTuplesList
														)
													)
											)
	#</DefineBindingHookMethods>

#</DefineClass>
