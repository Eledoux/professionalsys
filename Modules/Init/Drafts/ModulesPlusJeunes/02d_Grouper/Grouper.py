#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class GrouperClass(
						SYS.StructurerClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.GroupingHdfFile=None
		self.GroupingHdfFilePointer=None
		self.GroupingPathString="/"
		self.GroupingIsOverWritingBool=True
		#</DefineSpecificDict>
		
		#Update the RepresentingKeyVariablesList
		self.RepresentingKeyVariablesList+=['StructuringParentPointersList']

	def appendAfter(self):

		#Define a Short cut
		AppendedVariable=self.AppendedValueVariablesList[self.AppendedInt]

		#Build a short alias to the HdfFile of the uppest Parent
		if hasattr(AppendedVariable,'StructuringParentPointersList'):
			if len(AppendedVariable.StructuringParentPointersList)>0:
				AppendedVariable['GroupingHdfFilePointer']=AppendedVariable.StructuringParentPointersList[-1].GroupingHdfFile

			

	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindStructuringChildPointersListAfter(self):

		#bind with self.RelayedKeyVariablesList setting
		self.RelayingKeyVariablesList=map(lambda __StructuringChildPointer:
												'*'+__StructuringChildPointer.StructuredAppendedKeyString,
												self.StructuringChildPointersList
												)
		self.RelayedKeyVariablesList=['/']+self.RelayingKeyVariablesList
		

	def bindStructuringParentPointersListAfter(self):

		#Bind with GroupingPathString setting
		self.setGroupingPathString()

	def bindStructuringIntsListAfter(self):

		#Bind with GroupingPathString setting
		self.setGroupingPathString()

	def bindGroupingPathStringAfter(self):

		#Bind with setGroupingHdfFile
		self.setGroupsInGroupingHdfFile()

	def bindGroupingHdfFilePointerAfter(self):

		#Bind with GroupingHdfFile setting
		self.setGroupsInGroupingHdfFile()

		#Give to all the derived from StructuringObject the StructuringPointersList and StructuringIntsList
		map(
				lambda _IntAndStructuringObject:
				_IntAndStructuringObject[1].update([
															('GroupingHdfFilePointer',self.GroupingHdfFilePointer),
															('bind',[])
													]),
				filter(
						lambda _IntAndVariable:
						SYS.StructurerClass in type(_IntAndVariable[1]).__mro__,
						enumerate(self.AppendedValueVariablesList)
						)
			)


	#<DefineBindingHookMethods>
	
	#<DefineMethods>
	def setGroupingPathString(self):

		#Check that the lengths are good
		if len(self.StructuringIntsList)==len(self.StructuringParentPointersList):

			#Get the ReversedGroupingPathStringsLis
			ReversedGroupingPathStringsList=map(
											lambda __StructuringParentPointer,AppendedInt:
											__StructuringParentPointer.AppendedKeyStringsList[AppendedInt],
											self.StructuringParentPointersList,
											self.StructuringIntsList
										)

			#Reverse
			ReversedGroupingPathStringsList.reverse()
									
			#Set the GroupingPathString	
			self['GroupingPathString']="/"+"/".join(ReversedGroupingPathStringsList)

	def setGroupsInGroupingHdfFile(self):

		#Init the Group inside the file
		if self.GroupingHdfFilePointer!=None:

			#pass
			self.GroupingHdfFilePointer.create_group(self.GroupingPathString)

	def datasetAfter(self,_LocalCallVariable,_LocalCallingVariablesList):

		#Case where there is not anymore tructuringChildPointers
		if len(self.StructuringChildPointersList)==0:


			
			print("OUI",self.StructuredAppendedKeyString)

	#</DefineMethods>
#</DefineClass>

