#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class GrouperClass(
						SYS.StructurerClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.GroupingHdfFile=None
		self.GroupingHdfFilePointer=None
		self.GroupingPathString=""
		self.GroupingIsOverWritingBool=True
		#</DefineSpecificDict>
		
	def appendAfter(self,_AppendingTuple):

		#Build a short alias to the HdfFile of the uppest Parent
		if len(self.ValueVariablesList[-1].StructuringPointersList)>0:
			self.ValueVariablesList[-1]['GroupingHdfFilePointer']=self.ValueVariablesList[-1].StructuringPointersList[-1].GroupingHdfFile
		
		#Return self
		return self

	def bindAfter(self):

		#Alias the H5pyGroup
		H5pyGroupPointer=self.GroupingHdfFilePointer.H5pyFile[self.GroupingPathString]

		#Create a group for each
		map(
				lambda KeyVariable:
				H5pyGroupPointer.create_group(KeyVariable)
				if KeyVariable not in H5pyGroupPointer and self.GroupingIsOverWritingBool
				else None,
				self.GroupingScanner.KeyStringsList
			)

	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindStructuringPointersListAfter(self):

		#Bind with GroupingPathString setting
		self.setGroupingPathString()

	def bindStructuringIntsListAfter(self):

		#Bind with GroupingPathString setting
		self.setGroupingPathString()

	def bindGroupingPathStringAfter(self):

		#Bind with setGroupingHdfFile
		self.setGroupsInGroupingHdfFile()

	def bindGroupingHdfFilePointerAfter(self):

		#Bind with GroupingHdfFile setting
		self.setGroupsInGroupingHdfFile()

		#Give to all the derived from StructuringObject the StructuringPointersList and StructuringIntsList
		map(
				lambda _IntAndStructuringObject:
				_IntAndStructuringObject[1].update(*[
															('GroupingHdfFilePointer',self.GroupingHdfFilePointer),
															('bind',[])
														]),
				filter(
						lambda _IntAndVariable:
						SYS.StructuringObjectClass in type(_IntAndVariable[1]).__mro__,
						enumerate(self.ValueVariablesList)
						)
			)


	#<DefineBindingHookMethods>
	
	#<DefineMethods>
	def setGroupingPathString(self):

		#Check that the lengths are good
		if len(self.StructuringIntsList)==len(self.StructuringPointersList):

			#Get the ReversedGroupingPathStringsLis
			ReversedGroupingPathStringsList=map(
											lambda StructuringPointer,AppendedInt:
											StructuringPointer.KeyStringsList[AppendedInt],
											self.StructuringPointersList,
											self.StructuringIntsList
										)

			#Reverse
			ReversedGroupingPathStringsList.reverse()
									
			#Set the GroupingPathString	
			self['GroupingPathString']="/".join(ReversedGroupingPathStringsList)

	def setGroupsInGroupingHdfFile(self):

		#Init the Group inside the file
		if self.GroupingHdfFilePointer!=None:

			#Check if the Path exists
			if self.GroupingPathString not in self.GroupingHdfFilePointer.H5pyFile:

				#Set all the intermediate Paths before
				GroupingPathStringsList=self.GroupingPathString.split('/')
				CountInt=0
				PathString=GroupingPathStringsList[0]
				GroupPointer=self.GroupingHdfFilePointer.H5pyFile

				#Set the PathString from the top to the down (integrativ loop)
				for GroupingPathString in GroupingPathStringsList:

					#Create the group if not already
					if GroupingPathString not in GroupPointer:
						GroupPointer.create_group(GroupingPathString)

					#Go deeper
					GroupPointer=GroupPointer[GroupingPathString]
		
	def setDatasetsInGroupingHdfFileWithScannedTuple(self,_ScannedTuple):

		#Define the GroupPointer	
		GroupPointer=self.GroupingHdfFilePointer.H5pyFile[self.GroupingPathString+'/'+_ScannedTuple[0]]

		#Then write the Datasets
		map(
				lambda _DatasetTuple:
				GroupPointer.create_dataset(_DatasetTuple[0],data=_DatasetTuple[1])
				if _DatasetTuple[0] not in GroupPointer and self.GroupingIsOverWritingBool
				else None,
				_ScannedTuple[1].items()
			)


	#</DefineMethods>

#</DefineClass>

