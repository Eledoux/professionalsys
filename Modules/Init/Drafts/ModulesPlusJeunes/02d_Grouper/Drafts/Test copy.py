#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class GrouperTesterClass(SYS.TesterClass):
	
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	def test_01_group(self):
		self.TestedVariable=self.TestedClass().update(*[('GroupingHdfFile',
															SYS.HdfFileClass().update(*[
											('FolderPathString',SYS.getCurrentFolderPathString()),
											('FileString','Calcul'),
											('ModeString','r+'),
											('bind',[])
										]))]).append(
										(
											'CalculGrouper',
											[
												('TypeString',"Grouper"),
												('__add__',[
															(
																'SumGroupingDictionnary',
																[
																	('TypeString',"GroupingDictionnary"),
																	('/GroupingScanner',[
																							('ScanningTuplesList',[
																								('SeedIntsList',[1,2]),
																								('MinFloatsList',[-10.,-100.]),
																								('MaxFloatsList',[10.,100.])
																							]),
																							('PickingVariablesList',['SeedInt',
																								'MinFloat',
																								'MaxFloat']),
																							('scan',[SYS.SumClass()])
																						]
																	)
																]
															),
															(
																'MulGroupingDictionnary',
																[
																	('TypeString',"GroupingDictionnary"),
																	('/GroupingScanner',[
																							('ScanningTuplesList',[
																								('SeedIntsList',[1,2]),
																								('MinFloatsList',[-10.,-100.]),
																								('MaxFloatsList',[10.,100.])
																							]),
																							('PickingVariablesList',['SeedInt',
																								'MinFloat',
																								'MaxFloat']),
																							('scan',[SYS.MulClass()])
																						]
																	)
																]
															)
														]
													)	
											]
										)
									)
#</DefineTestClass>

#<Test>

#Get the local test class and the tested method strings
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)

#Write the solutions to the tests
SYS.sys.modules['os'].popen("cd "+TestedClass().TestingFolderPathString+";rm *")
TestedClass().apply('testify',MethodStringsList)

#Test
TestedClass().update([
						('IsPrintedBool',True),
						('TestingClass',SYS.TestClass)
					]
					).apply('bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()

#</Test>

