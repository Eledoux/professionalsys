#<ImportModules>
import ShareYourSystem as SYS
import os
#</ImportModules>

#<DefineTestClass>
class GrouperTesterClass(SYS.TesterClass):
	
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	def test_01_group(self):

		#Build a Grouper with subGroupers
		self.TestedClass().update(
									[
										('GroupingHdfFile',
													SYS.HdfFileClass().update([
												('FolderPathString',SYS.getCurrentFolderPathString()),
												('FileString','Calcul'),
												('bind',[])
												]
											)
										)
									]
								).append(
								(
									'CalculGrouper',
									[
										('TypeString',"Grouper"),
										('__add__',[
													(
														'SumGrouper',
														[
															('TypeString',"Grouper"),
														]
													),
													(
														'MulGrouper',
														[
															('TypeString',"Grouper"),
														]
													)
												]
											)	
									]
								)
							)

		#Display the hdf5 with h5ls
		self.TestedVariable=os.popen('h5ls -rd Calcul.hdf5').read()

	def test_02_dataset(self):

		#Build a Grouper with subGroupers
		self.TestedVariable=self.TestedClass().update(
									[
										('GroupingHdfFile',
													SYS.HdfFileClass().update([
												('FolderPathString',SYS.getCurrentFolderPathString()),
												('FileString','Calcul'),
												('bind',[])
												]
											)
										)
									]
								).append(
								(
									'CalculGrouper',
									[
										('TypeString',"Grouper"),
										('__add__',[
													(
														'SumGrouper',
														[
															('TypeString',"Grouper"),
														]
													),
													(
														'MulGrouper',
														[
															('TypeString',"Grouper"),
														]
													)
												]
											)	
									]
								)
							).relay([('dataset',[])])

#</DefineTestClass>

#<Test>

#Get the local test class and the tested method strings
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)

#Write the solutions to the tests
SYS.sys.modules['os'].popen("cd "+TestedClass().TestingFolderPathString+";rm *")
TestedClass().apply('testify',MethodStringsList)

#Test
TestedClass().update([
						('IsPrintedBool',True),
						('TestingClass',SYS.TestClass)
					]
					).apply('bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()

#</Test>

