#<ImportModules>
import ShareYourSystem as SYS
import numpy
import scipy.stats
import tables
#</ImportModules>

#<DefineClass>
class MulClass(SYS.UseClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.MulIntsList=[0,0]
		self.MinFloat=-100.
		self.MaxFloat=100.
		self.MulInt=0
		#</DefineSpecificDict>

	def bindAfter(self):

		#Bind with SumIntsList setting
		self['MulIntsList']=map(int,scipy.floor(self.MinFloat+(self.MaxFloat-self.MinFloat)*scipy.stats.uniform.rvs(size=2)))
	
		#Return self
		return self
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindMulIntsListAfter(self):

		#Bind with MulInt setting
		self['MulInt']=numpy.prod(self.MulIntsList)
	#</DefineBindingHookMethods>	
	
#</DefineClass>

#<DefineModelClass>

#Get the just defined Class
ModeledClass=filter(lambda Variable:type(Variable[1])==type,globals().items())[0][1]

#Define the OutputingKeyStringsList
ModeledClass.OutputingKeyStringsList=['SumIntsList','SumInt']

class SumTable(tables.IsDescription):

	#Get the OutputingKeyStringsList for making them like a IsDescription Type
	for KeyString in ModeledClass.OutputingKeyStringsList:

		#Get the Col 
		Col=SYS.getColTypeStringWithKeyString(KeyString)
		if Col!=None:
			locals().__setitem__(KeyString,Col)

	#Delete the local variables 
	del Col

#</DefineModelClass>

