#<ImportModules>
import ShareYourSystem as SYS
import itertools
#</ImportModules>

#<DefineClass>
class RepeaterClass(
						SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.RepeatingSeedIntsList=[1,2]
		self.IsRepeatingBool=True
		#</DefineSpecificDict>

	def appendBefore(self):

		if self.IsRepeatingBool:

			LocalAppendingVariable=self.AppendingVariable

			self.IsRepeatingBool=False
			map(
					lambda _RepeatingSeedInt:
					self.append(
									(
										('SeedInt',_RepeatingSeedInt),
										LocalAppendingVariable.__setitem__('SeedInt',_RepeatingSeedInt)('bind',[])
									)
								),
					self.RepeatingSeedIntsList
				)
			self.IsRepeatingBool=True

			self.IsAppendingBool=False

	def appendAfter(self):

		if self.IsRepeatingBool==False:

			self.AppendedValueVariablesList[self.AppendedInt]=self.AppendedValueVariablesList[self.AppendedInt].output()

	#<DefineHookMethods>

#</DefineClass>
