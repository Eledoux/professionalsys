#<ImportModules>
import ShareYourSystem as SYS
import h5py
import tables
import sys
#</ImportModules>

#<DefineClass>
class HdfFileClass(SYS.FileClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.FilePointer=None
		self.ModuleString="tables"
		#</DefineSpecificDict>

		#Update the ExtensionString
		self['ExtensionString']=".hdf5"

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindAfter(self,_LocalCallVariable,_LocalCallingVariablesList):

		#Check for a good FilePathString
		if self.FilePathString!="":

			#Check for first write
			if SYS.sys.modules['os'].path.isfile(self.FilePathString)==False:
				self.FilePointer=sys.modules[self.ModuleString].File(self.FilePathString,'w')
				self.FilePointer.close()

	def bindModeStringAfter(self):

		#Check that it is a good FilePathString
		if self.FilePathString!="":

			#Check that it is the good mode
			if hasattr(self.FilePointer,'mode'):
				if self.FilePointer.mode!=self.ModeString:
					#Close
					self.FilePointer.close()

					#Reopen
					self.FilePointer=sys.modules[self.ModuleString].File(self.FilePathString,self.ModeString)

			else:

				#Open
				self.FilePointer=sys.modules[self.ModuleString].File(self.FilePathString,self.ModeString)

	#</DefineBindingHookMethods>	

	#<DefineMethods>	
	def create_group(self,_GroupPathString):

		#Pass in write mode
		self['ModeString']='r+'

		#Make sure that the first char is /
		if _GroupPathString[0]!="/":
			_GroupPathString="/"+_GroupPathString

		#Check if the Path exists
		if _GroupPathString not in self.FilePointer:

			#Set all the intermediate Paths before
			GroupPathStringsList=_GroupPathString.split('/')[1:]
			ParsingGroupPathString="/"

			#Set the PathString from the top to the down (integrativ loop)
			for GroupPathString in GroupPathStringsList:

				#Go deeper
				NewParsingGroupPathString=ParsingGroupPathString+GroupPathString

				#Create the group if not already
				if NewParsingGroupPathString not in self.FilePointer:
					if self.ModuleString=="tables":
						self.FilePointer.create_group(ParsingGroupPathString,GroupPathString)
					elif self.ModuleString=="h5py":
						Group=self.FilePointer[ParsingGroupPathString]
						Group.create_group(GroupPathString)
				
				#Prepare the next group	
				ParsingGroupPathString=NewParsingGroupPathString+'/'

		#Close
		self.FilePointer.close()

		#Return self
		return self
		
	#<DefineMethods>	
#</DefineClass>