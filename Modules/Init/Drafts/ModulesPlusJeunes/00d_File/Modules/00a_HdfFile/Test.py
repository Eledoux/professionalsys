#<ImportModules>
import ShareYourSystem as SYS
import os
#</ImportModules>

#<DefineTestClass>
class HdfFileTesterClass(SYS.TesterClass):
	
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	def test_01_bind(self):
		self.TestedVariable=self.TestedClass().update([
															('FolderPathString',SYS.getCurrentFolderPathString()),
															('FileString',"TestTables"),
													])('bind',[])

	def test_02_groupTables(self):

		#Build some groups
		HdfFile=self.TestedClass().update([
												('FolderPathString',SYS.getCurrentFolderPathString()),
												('FileString',"TestTables"),
										])('bind',[]).create_group('/ChildGroup/SecondChild/')

		#Display the hdf5 with h5ls
		self.TestedVariable=os.popen('h5ls -rd TestTables.hdf5').read()

	def test_03_groupH5py(self):

		#Build some groups
		self.TestedVariable=self.TestedClass().update([
															('ModulesString','h5py'),
															('FolderPathString',SYS.getCurrentFolderPathString()),
															('FileString',"TestH5py"),
													])('bind',[]).create_group('/ChildGroup/SecondChild/')

		#Display the hdf5 with h5ls
		self.TestedVariable=os.popen('h5ls -rd TestH5py.hdf5').read()

#</DefineTestClass>

#<Test>

#Get the local test class and the tested method strings
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)

#Write the solutions to the tests
SYS.sys.modules['os'].popen("cd "+TestedClass().TestingFolderPathString+";rm *")
TestedClass().apply('testify',MethodStringsList)

#Test
TestedClass().update([
						('IsPrintedBool',True),
						('TestingClass',SYS.TestClass)
					]
					).apply('bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()

#</Test>
