#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class FileClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.FilePathString=""
		self.FolderPathString=""
		self.ExtensionString=""
		self.FileString=""
		self.ModeString="r"
		#</DefineSpecificDict>

	def bindAfter(self,_LocalCallVariable,_LocalCallingVariablesList):
		
		#Bind with FilePathString setting
		_FilePathString=self.FolderPathString+self.FileString
		if self.ExtensionString!="":
			_FilePathString+=self.ExtensionString
		self['FilePathString']=_FilePathString
	#</DefineHookMethods>	

#</DefineClass>