#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Sort"
DoingString="Sorting"
DoneString="Sorted"
#<DefineClass>

#<DefineClass>
class SortingObjectClass(
								SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.DictionnaryPointer=None
		self.FunctionPointer=None
		#</DefineSpecificDict>
		
	def appendAfter(self,_AppendingTuple):

		#Filter the itemized Variables
		FilteredTuplesList=filter(self.FunctionPointer,self('items'))
		
		#Return self
		return self
	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

