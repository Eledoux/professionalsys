#<ImportModules>
import ShareYourSystem as SYS
import itertools
#</ImportModules>

#<DefineClass>
class ScanClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.TuplesList=[]
		self.ValueVariablesList=[]
		self.KeyStringsList=[]
		self.ValuePointer=None
		#</DefineSpecificDict>
	
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindTuplesListAfter(self):

		#Bind with KeyStringsList setting
		self['KeyStringsList']=map(
										lambda KeyString:
										SYS.getSingularStringWithPluralString(
											KeyString.split("List")[0]
											),
										map(
											lambda Tuple:
											Tuple[0],
											self.TuplesList
										)
									)

		#Bind with ValueVariablesList setting
		self['ValueVariablesList']=list(
							itertools.product(
								*map(
										lambda Tuple:
										Tuple[1],
										self.TuplesList
									)
								)
							)

	def bindValueVariablesListAfter(self):
		self.setInValuePointer()

	def bindValuePointerAfter(self):
		self.setInValuePointer()
	#</DefineBindingHookMethods>		

	#<DefineMethods>
	def setInValuePointer(self):

		if SYS.ScannerClass in type(self.ValuePointer).__mro__:

			#Set the Scanner/Join/KeyVariablesList
			self.ValuePointer.Join['KeyVariablesList']=self.KeyStringsList

			#Set the Scanner/Delivery/TuplesList
			self.ValuePointer.Delivery['TuplesList']=map(
															lambda ProductTuple:
															zip(
																	self.ValuePointer.Join.KeyVariablesList,
																	ProductTuple
															),
															self.ValueVariablesList
														)


	#</DefineMethods>

#</DefineClass>