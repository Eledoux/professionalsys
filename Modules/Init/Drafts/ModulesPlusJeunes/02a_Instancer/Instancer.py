#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancerClass(
						SYS.ObjectClass
				):

	#<DefineHookMethods>
	def appendBefore(self):

		#Get the Value
		KeyVariable=None
		if type(self.AppendingVariable)==tuple and len(self.AppendingVariable)==2:
			KeyVariable=self.AppendingVariable[0]
			ValueVariable=self.AppendingVariable[1]
		else:
			ValueVariable=self.AppendingVariable

		#Check that it has to be instanced
		if SYS.ObjectClass not in type(ValueVariable).__mro__:

			#Prepare the UpdatingTuplesList UpdatingDict
			UpdatingTuplesList=[]
			InstancedTypeString=None

			#Look for a TypeString in the self.AppendingVariable
			if type(ValueVariable)!=list:
				InstancedTypeString=SYS.getWithDictatedVariableAndKeyVariable(ValueVariable,'TypeString')

			#Get maybe if the _AppendingTuple[1] is a TuplesList and TypeString is the first element
			if InstancedTypeString==None:
				if SYS.getIsTuplesListBool(ValueVariable):
					if ValueVariable[0][0]=='TypeString':
						InstancedTypeString=ValueVariable[0][1]
						UpdatingTuplesList=ValueVariable

			#Instancify
			if InstancedTypeString!=None:

				#Define the InstancedClassString
				InstancedClass=getattr(
										SYS,
										SYS.getClassStringWithTypeString(InstancedTypeString)
										)

				if InstancedClass!=None:

					#Append the instanced Variable
					self.append(
								(KeyVariable,InstancedClass().update(UpdatingTuplesList)) if KeyVariable!=None
								else InstancedClass().update(UpdatingTuplesList)
							)

					#Stop the setting at this level
					self.IsSettingBool=False
	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>


#</DefineClass>

