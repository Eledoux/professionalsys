#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class InstancerTesterClass(SYS.TesterClass):
	
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	def test_01_add(self):
		self.TestedVariable=self.TestedClass().__add__(
													[
										('MyFirstDict',{}),
										('YourString',"WTF"),
										('MyFirstObject',{'TypeString':"Object"}),
										('MySecondObject',[
															('TypeString',"Object"),
															('MyString',"help"),
															('ChildObject',SYS.ObjectClass()),
															('/ChildObject/ChildString',"allo")
														])
									]
								)
#</DefineTestClass>

#<Test>

#Get the local test class and the tested method strings
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)

#Write the solutions to the tests
SYS.sys.modules['os'].popen("cd "+TestedClass().TestingFolderPathString+";rm *")
TestedClass().apply('testify',MethodStringsList)

#Test
TestedClass().update([
						('IsPrintedBool',True),
						('TestingClass',SYS.TestClass)
					]
					).apply('bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()

#</Test>

