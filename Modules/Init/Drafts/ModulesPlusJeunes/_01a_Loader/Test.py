#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class LoaderTesterClass(SYS.TesterClass):
	
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	def test_01_append(self):
		self.TestedVariable=self.TestedClass().update([
								('MyInt',0),
								('MyGettedObject',SYS.ObjectClass().update([
													('MyFirstFloat',0.1)
													]
													)),
								('MyCopiedObject',SYS.ObjectClass().update([
													('MySecondFloat',0.2)
													]
													)),
								('MyString',"hello"),
								('PickingKeyVariablesList',['MyInt','MyGettedObject','MyString']),
								('ScanningKeyVariablesList',['MyInt','MyCopiedObject','MyString']),
						]).append(('MyAppendedObject',SYS.ObjectClass()))

		#Change the Getted and Copied objects
		self.TestedVariable.MyGettedObject.MyFirstFloat=0.5
		self.TestedVariable.MyCopiedObject.MySecondFloat=0.6


#</DefineTestClass>

#<Test>

#Get the local test class and the tested method strings
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)

#Write the solutions to the tests
SYS.sys.modules['os'].popen("cd "+TestedClass().TestingFolderPathString+";rm *")
TestedClass().apply('testify',MethodStringsList)

#Test
TestedClass().update([
						('IsPrintedBool',True),
						('TestingClass',SYS.TestClass)
					]
					).apply('bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()

#</Test>


