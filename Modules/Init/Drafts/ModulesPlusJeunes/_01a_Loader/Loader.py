#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class LoaderClass(
						SYS.ObjectClass
				):

	#<DefineHookMethods>			
	def appendAfter(self):

		#Link and deliver to the last appended Variable
		self.link(self.AppendedVariablesList[self.AppendedInt])
		self.deliver(self.AppendedVariablesList[self.AppendedInt])

	#<DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>


#</DefineClass>

