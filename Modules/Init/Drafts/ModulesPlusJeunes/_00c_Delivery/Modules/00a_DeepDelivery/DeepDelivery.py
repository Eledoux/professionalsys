#<ImportModules>
import copy
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DeepDeliveryClass(SYS.ObjectClass):
	"""
		DeepDelivery is a Delivery that deepcopy the things to deliver
	"""

	#<DefineHookMethods>
	def addAfter(self):

		#Set the UpdatingTuplesList from the items of the self
		self.UpdatingTuplesList=copy.deepcopy(self.UpdatingTuplesList)
	#</DefineHookMethods>

#</DefineClass>