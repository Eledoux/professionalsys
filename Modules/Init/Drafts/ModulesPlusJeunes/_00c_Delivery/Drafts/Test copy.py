#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class DeliveryTesterClass(SYS.TesterClass):
	
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	def test_01_update(self):
		self.TestedVariable=self.TestedClass().update(*[
							('TuplesList',[
											('MyString',"hello"),
											('MyInt',3)
										]
							),
							('MyFloat',0.1),
							('MyObject',SYS.ObjectClass().update(**{
								'DeliveringKeyVariablesList':['MyFloat']
								})),
							('ValuePointer','/MyObject')
						]
					)['/MyObject']
#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
SYS.sys.modules['os'].popen("cd "+TestedClass().TestingFolderPathString+";rm *")
TestedClass()('apply','testify',MethodStringsList)
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply','bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>
