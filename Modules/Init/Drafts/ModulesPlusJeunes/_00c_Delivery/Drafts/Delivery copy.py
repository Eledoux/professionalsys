#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DeliveryClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.TuplesList=[]
		self.ValuePointer=None
		#</DefineSpecificDict>
	
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindValuePointerAfter(self):

		#Bind with setInValuePointer
		self.setInValuePointer()

	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setInValuePointer(self):

		#Add also the DeliveringKeyVariablesList specific to the delivered object
		if hasattr(self.ValuePointer,'DeliveringKeyVariablesList'):
			self.TuplesList+=self('pick',self.ValuePointer.DeliveringKeyVariablesList)

		#Update in the ValuePointer
		if hasattr(self.ValuePointer,'update'):

			#Update with the TuplesListsList
			if type(self.ValuePointer)==dict:
				self.ValuePointer.update(dict(self.TuplesList))
			else:
				self.ValuePointer.update(*self.TuplesList)

	#</DefineMethods>
#</DefineClass>