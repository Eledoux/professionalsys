#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DeliveryClass(SYS.ObjectClass):
	"""
		Delivery is an Object

		The attributes are :

		-UpdatedPointer that has to be a BasedObject of a dict

		-OrderedVariablesList is a <TuplesList>
		to be defined, 

		-UpdatingTuplesList is the OrderedVariablesList 
		plus the picked tuples from the UpdatedPointer.DeliveringKeyVariablesList

		HookMethods:

		-bindAfter : if the UpdatedPointer is defined, it is updated with UpdatingTuplesList
	"""

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.UpdatingTuplesList=[]
		#</DefineSpecificDict>
	
	def useAfter(self):

		#Update in the UpdatedPointer
		if hasattr(self.UsedPointer,'update'):

			#Add the DeliveringKeyVariablesList specific to the delivered object
			if SYS.ObjectClass in type(self.UsedPointer).__mro__:
				self.UpdatingTuplesList+=self('pick',self.UsedPointer.DeliveringKeyVariablesList)

			#Update with the UpdatingTuplesList
			if type(self.UsedPointer)==dict:
				self.UsedPointer.update(dict(self.UpdatingTuplesList))
			else:
				self.UsedPointer.update(*self.UpdatingTuplesList)
	
	#</DefineHookMethods>

#</DefineClass>