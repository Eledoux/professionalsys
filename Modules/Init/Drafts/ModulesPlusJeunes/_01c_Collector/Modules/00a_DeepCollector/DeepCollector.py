#<ImportModules>
import copy
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DeepCollectorClass(
						SYS.CollectorClass
				):

	#<DefineHookMethods>
	def appendBefore(self):

		#Explicit the self.CallingString
		AppendingKeyVariable=self.CallingList[0][0]
		AppendingValueVariable=self.CallingList[0][1]

		#Change the PickedDict as a deepcopy
		self.CallingList=tuple(
			[tuple([self.CallingList[0][0],copy.deepcopy(AppendingValueVariable)])]
			)

	#</DefineHookMethods>


#</DefineClass>

