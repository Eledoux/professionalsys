#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class CollectorClass(
						SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.Collection=SYS.CollectionClass()
		self.CollectingKeyVariablesListsList=[]
		#</DefineSpecificDict>

	def appendAfter(self):

		#Set the Deliver and bind
		self.OrderedVariablesList[self.OrderedInt]=self.Collection.update(*[
								(
									'KeyVariablesList',
									self.CollectingKeyVariablesListsList[self.OrderedInt] 
								) if len(self.CollectingKeyVariablesListsList)>self.OrderedInt else (),
								('ValuePointer',self.OrderedVariablesList[self.OrderedInt])
							]).TuplesList

	def collectAfter(self):
		self('append',self.CallingList[0])
	#</DefineHookMethods>


#</DefineClass>

