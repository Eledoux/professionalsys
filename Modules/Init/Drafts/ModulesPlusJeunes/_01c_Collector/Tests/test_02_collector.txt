< (CollectorClass)>
{ 
   /'<NotSpe>KeyStringsList' : ['MyCollectedObjectDict', 'MyOtherCollectedDict']
   /'<NotSpe>ValueVariablesList' : 
   /[ 
   /   /'0' : 
   /   /[ 
   /   /   /'0' : ('/MyGrandChildObject/MyString', 'hello')
   /   / ]
   /   /'1' : 
   /   /[ 
   /   /   /'0' : ('MyFloat', 0.1)
   /   / ]
   / ]
   /'<Spe>CollectingKeyVariablesListsList' : 
   /[ 
   /   /'0' : ['/MyGrandChildObject/MyString']
   /   /'1' : ['MyFloat']
   / ]
   /'<Spe>Collection' : < (CollectionClass)>
   /{ 
   /   /'<NotSpe>KeyStringsList' : []
   /   /'<NotSpe>ValueVariablesList' : []
   /   /'<Spe>KeyVariablesList' : ['MyFloat']
   /   /'<Spe>TuplesList' : 
   /   /[ 
   /   /   /'0' : ('MyFloat', 0.1)
   /   / ]
   /   /'<Spe>ValuePointer' : < (dict)>
   / }
 }