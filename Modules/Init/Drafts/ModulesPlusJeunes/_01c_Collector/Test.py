#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class CollectorTesterClass(SYS.TesterClass):
	
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	def test_01_appendSamely(self):
		self.TestedVariable=self.TestedClass().update(**{
										'/Collection/KeyVariablesList':['MyFloat']
										})(
											'apply',
											'append',
											[
												('MyCollectedObjectDict',
													SYS.ObjectClass().update(**{
														'MyGrandChildObject':SYS.ObjectClass().update(**{
															'MyString':"hello"
															}),
														'MyFloat':0.2
														})
												),
												('MyOtherCollectedDict',
													{'MyFloat':0.1}
												)
											]
										)

	def test_02_appendNotSamely(self):
		self.TestedVariable=self.TestedClass().update(**{
										'CollectingKeyVariablesListsList':[
																['/MyGrandChildObject/MyString'],
																['MyFloat']
															]
										})(
											'apply',
											'append',
											[
												('MyCollectedObjectDict',
													SYS.ObjectClass().update(**{
														'MyGrandChildObject':SYS.ObjectClass().update(**{
															'MyString':"hello"
															})
														})
												),
												('MyOtherCollectedDict',
													{'MyFloat':0.1}
												)
											]
										)

	def test_03_collect(self):
		self.TestedVariable=self.TestedClass().update(**{
										'CollectingKeyVariablesListsList':[
																['/MyGrandChildObject/MyString'],
																['MyFloat']
															]
										})(
											'collect',
											('MyCollectedObjectDict',
													SYS.ObjectClass().update(**{
														'MyGrandChildObject':SYS.ObjectClass().update(**{
															'MyString':"hello"
															})
														})
											)
										)

	def test_04_applyCollect(self):
		self.TestedVariable=self.TestedClass().update(**{
										'CollectingKeyVariablesListsList':[
																['/MyGrandChildObject/MyString'],
																['MyFloat']
															]
										})(
											'apply',
											'collect',
											[
												('MyCollectedObjectDict',
													SYS.ObjectClass().update(**{
														'MyGrandChildObject':SYS.ObjectClass().update(**{
															'MyString':"hello"
															})
														})
												),
												('MyOtherCollectedDict',
													{'MyFloat':0.1}
												)
											]
										)

	def test_05_collectNotDeeply(self):

		#Define an Object
		Object=SYS.ObjectClass()

		#Append a first time
		Object.update(**{
						'MyGrandChildObject':SYS.ObjectClass().update(**{
							'MyString':"hello"
							})
						})
		TestedClass=self.TestedClass().update(**{'/Collection/KeyVariablesList':[
													'MyGrandChildObject'
													]})(
															'collect',
															('MyPickedObjectDict',Object)
														)
														
		#Append a second time with updating the MyGrandChildObject (but "hello" will replaced by coucou then in the first slot)
		Object.MyGrandChildObject['MyString']="coucou"
		self.TestedVariable=TestedClass(
											'collect',
											('MyPickedObjectDict',Object)
										)
#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply',*('testify',MethodStringsList))
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply',*('bound',MethodStringsList))
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>

