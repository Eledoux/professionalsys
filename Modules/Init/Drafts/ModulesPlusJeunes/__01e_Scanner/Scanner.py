#<ImportModules>
import ShareYourSystem as SYS
import itertools
#</ImportModules>

#<DefineClass>
class ScannerClass(
						SYS.DeepCollectorClass,
						SYS.JoinerClass,
						SYS.DeliverClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.Scan=SYS.ScanClass().__setitem__('ValuePointer',self)
		self.IsScanningBool=True
		#</DefineSpecificDict>

		#Represent the other helper objects
		self.RepresentingKeyVariablesList+=['Join','Collection','Delivery']
		
	def appendBefore(self):

		if self.IsScanningBool:

			#Explicit the self.CallingString
			AppendingKeyVariable=self.CallingList[0][0]
			AppendingValueVariable=self.CallingList[0][1]

			#Set the IsScanningBool to False
			self.IsScanningBool=False

			#Apply for all these products
			self('apply','append',
							[
								(None,AppendingValueVariable) for IndexInt in xrange(
												len(self.Delivery.TuplesList)
												)
							]
				)

			#Set the IsScanningBool to True
			self.IsScanningBool=True

			#Set the append to end
			self.IsCallingBool=False
	#<DefineHookMethods>
	


#</DefineClass>

