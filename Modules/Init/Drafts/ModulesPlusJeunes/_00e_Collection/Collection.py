#<ImportModules>
import copy
import ShareYourSystem as SYS
import time
#</ImportModules>

#<DefineClass>
class CollectionClass(SYS.ObjectClass):
	"""
		Collection is an Object

		The attributes are :

		-PickedPointer that has to be a BasedObject of a dict

		-OrderedVariablesList is a <PickingKeyVariablesList>
		to be defined, 

		-PickingKeyVariablesList is the OrderedVariablesList 
		plus the picked tuples from the PickedPointer.PickingKeyVariablesList

		Once the PickedPointer is setted, it is picked with PickingKeyVariablesList
	"""

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.PickingKeyVariablesList=[]
		self.PickedKeyVariablesList=[]
		self.PickedPointer=None
		self.IsCollectingBool=True
		#</DefineSpecificDict>

	def appendBefore(self):

		if self.IsCollectingBool:
			self.IsCollectingBool=False
			self('append',(str(len(self.OrderedVariablesList)),self.CallingList[0]))
			self.IsCollectingBool=True
			self.IsCallingBool=False

	def bindAfter(self):
		
		#Set the PickingKeyVariablesList from the items of the self
		self.PickingKeyVariablesList=copy.copy(self.OrderedVariablesList)

		#Add the PickingKeyVariablesList specific to the picked object
		if SYS.ObjectClass in type(self.PickedPointer).__mro__:
			self.PickingKeyVariablesList+=self.PickedPointer.PickingKeyVariablesList

		#Pick for the dict or Object case
		if type(self.PickedPointer)==dict:
			self.PickedKeyVariablesList=map(
					lambda _PickingVariable:
					(
						str(_PickingVariable),
						self.PickedPointer[_PickingVariable]
					),
					filter(
							lambda _KeyVariable:
							_KeyVariable in self.PickedPointer,
							self.PickingKeyVariablesList
					)
				)
		else:
			self.PickedKeyVariablesList=self.PickedPointer('pick',self.PickingKeyVariablesList)

	#<DefineMethods>	

#</DefineClass>