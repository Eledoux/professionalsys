#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class CollectionTesterClass(SYS.TesterClass):
	
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	def test_01_update(self):
		self.TestedVariable=self.TestedClass()('apply','append',['MyInt','MyString']).update(*[
											(
												'PickedPointer',
												SYS.ObjectClass().update(*
													[
														('PickingKeyVariablesList',['/MyChildObject']),
														('MyChildObject',SYS.ObjectClass()),
														('MyInt',0),
														('MyString',"hello")
													]
												)
											)
										]
									)('bind',[])
#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
TestedClass()('apply','testify',MethodStringsList)
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply','bound',MethodStringsList)
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>
