#<ImportModules>
import ShareYourSystem as SYS
import time
#</ImportModules>

#<DefineClass>
class CollectionClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.KeyVariablesList=[]
		self.ValuePointer=None
		self.TuplesList=[]
		#</DefineSpecificDict>
	
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def bindKeyVariablesListAfter(self):

		#Bind with JoinString setting
		self.setTuplesList()
		
	def bindValuePointerAfter(self):

		#Bind with JoinString setting
		self.setTuplesList()

	#</DefineBindingHookMethods>	

	#<DefineMethods>
	def setTuplesList(self):
		
		#Refresh TuplesListsList
		self.TuplesList=[]

		#Chek if self.ValuePointer is callable
		if callable(self.ValuePointer):
			
			#Pick the already PickingKeyVariablesList specific to the picked object
			if hasattr(self.ValuePointer,'PickingKeyVariablesList'):
				self.TuplesList+=self.ValuePointer('pick',self.ValuePointer.PickingKeyVariablesList)

			#Pick with the KeyVariablesList
			self.TuplesList+=self.ValuePointer('pick',self.KeyVariablesList)

		elif type(self.ValuePointer)==dict:
			self.TuplesList=map(
					lambda _PickingVariable:
					(
						str(_PickingVariable),
						self.ValuePointer[_PickingVariable]
					),
					filter(
							lambda _KeyVariable:
							_KeyVariable in self.ValuePointer,
							self.KeyVariablesList
					)
				)
	#<DefineMethods>	

#</DefineClass>