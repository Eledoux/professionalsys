#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DeliverClass(
						SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.DeliveringTuplesListsList=[]
		#</DefineSpecificDict>
			
	def appendAfter(self):

		#Bind with DeliveringTuplesList setting
		self['DeliveringTuplesList']=self.DeliveringTuplesListsList[self.OrderedInt] 


		#Set the Deliver and bind
		self.Delivery.update(*[
								(
									'TuplesList',
									self.DeliveringTuplesListsList[self.OrderedInt] 
								) if len(self.DeliveringTuplesListsList)>self.OrderedInt else (),
								('ValuePointer',self.OrderedVariablesList[self.OrderedInt])
							])


	def deliverAfter(self):
		self('append',self.CallingList[0])


	#<DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>


#</DefineClass>

