#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineTestClass>
class DeliverTesterClass(SYS.TesterClass):
	
	def test_00_default(self):
		self.TestedVariable=self.TestedClass()

	def test_01_appendSamely(self):
		self.TestedVariable=self.TestedClass().update(**{
															'/Deliver/TuplesList':
																	[('MyString',"hello"),('MyInt',3)]
												})(
													'apply',
													'append',
													[
														('MyFirstDict',{}),
														('YourString',"WTF"),
														('MyObject',SYS.ObjectClass())
													]
												)

	def test_02_appendNotSamely(self):
		self.TestedVariable=self.TestedClass().update(**{
													'DeliveringTuplesListsList':[
															[('MyString',"hello"),('MyInt',3)],
															[],
															[('MyOtherString',"coucou")]
														],
												})(
													'apply',
													'append',
													[
														('MyFirstDict',{}),
														('YourString',"WTF"),
														('MyObject',SYS.ObjectClass())
													]
												)

	def test_03_deliver(self):
		self.TestedVariable=self.TestedClass().update(**{
															'DeliveringTuplesListsList':[
																	[('MyString',"hello"),('MyInt',3)],
																	[],
																	[],
																	[('MyOtherString',"coucou")]
																]
												})(
													'deliver',
													('MyFirstDict',{'MyFloat':0.1}),
												)(
													'apply',
													'deliver',
													[
														('MySecondDict',{}),
														('YourString',"WTF"),
														('MyObject',SYS.ObjectClass())
													]
												)
#</DefineTestClass>

#<Test>
TestedClass=filter(lambda Variable:type(Variable[1])==type,locals().items())[0][1]
MethodStringsList=SYS.getTestMethodStringsListWithClass(TestedClass)
SYS.sys.modules['os'].popen("cd "+TestedClass().TestingFolderPathString+";rm *")
TestedClass()('apply',*('testify',MethodStringsList))
TestedClass().update(**{
							'IsPrintedBool':True,
							'TestingClass':SYS.TestClass
						}
					)('apply',*('bound',MethodStringsList))
class Test(SYS.TestClass):pass
SYS.sys.modules['unittest'].main()
#</Test>


