#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class GrinderClass(
					SYS.GriderClass
				):

	#<DefineHookMethods>
	def initAfter(self):

		#<DefineSpecificDict>
		self.IsGrindingBool=True
		#<DefineSpecificDict>

	def appendBefore(self):

		if self.IsGrindingBool:

			#Set PickingKeyVariablesList in the AppendingVariable
			self.AppendingVariable['PickingKeyVariablesList']=self.PickingKeyVariablesList

			#Do the add of the repeated appended object
			self.IsGrindingBool=False
			self.__add__([self.AppendingVariable]*len(self.GridingValueVariablesList))
			self.IsGrindingBool=True

			#Stop the setting at this level
			self.IsAppendingBool=False

	def appendAfter(self):

		if self.IsGrindingBool==False:

			#Do a bind call to set up things in the functified Object
			self.AppendingVariable[1]('bind',[])
			
			#Then scan
			self.AppendedVariablesList[self.AppendedInt]=self.AppendingVariable[1].scan()
	#<DefineHookMethods>

#</DefineClass>

