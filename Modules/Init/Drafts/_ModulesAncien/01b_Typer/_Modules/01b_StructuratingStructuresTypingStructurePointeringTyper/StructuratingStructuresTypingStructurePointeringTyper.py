#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StructuratingStructuresTypingStructurePointeringTyperClass(
							SYS.TyperClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithStructuratingStructuresTypingStructurePointeringTyper(self):
		
		#<DefineSpecificDict>
		self.StructuratingStructuresTypingStructurePointer=None
		#</DefineSpecificDict>

		#Update the DerivingTypeString
		self['DerivingTypeString']="Structure"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuratingStructuresTypingStructurePointeringTyper"]=[
													"StructuratingStructuresTypingStructurePointer"
												]

	#</DefineHookMethods>

	#<DefineBindingMethods>
	def setStructuratingStructuresTypingStructurePointerAfterBasesWithStructuratingStructuresTypingStructurePointeringTyper(self):

		#Bind with StructuratingStructuresPointer_StructuratingStructuresTypeString setting
		self.setStructuratingStructuresTypingStructurePointer_StructuratingStructuresTypeStringWithStructuratingStructuresTypingStructurePointeringTyper()

	def setDerivedTypeStringAfterBasesWithStructuratingStructuresTypingStructurePointeringTyper(self):

		#Bind with StructuratingStructuresPointer_StructuratingStructuresTypeString setting
		self.setStructuratingStructuresTypingStructurePointer_StructuratingStructuresTypeStringWithStructuratingStructuresTypingStructurePointeringTyper()
	#</DefineBindingMethods>

	#<DefineMethods>
	def setStructuratingStructuresTypingStructurePointer_StructuratingStructuresTypeStringWithStructuratingStructuresTypingStructurePointeringTyper(self):
		if self.StructuratingStructuresTypingStructurePointer!=None:
			self.StructuratingStructuresTypingStructurePointer['StructuratingStructuresTypeString']=self.DerivedTypeString
	#</DefineMethods>

#</DefineClass>

