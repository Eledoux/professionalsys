#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class PointeringTyperClass(
							SYS.TyperClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithPointeringTyper(self):
		
		#<DefineSpecificDict>
		self.TypedPointer=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["PointeringTyper"]=[
														"TypedPointer"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setDerivedTypeStringAfterBasesWithPointeringTyper(self):

		#Bind with TypedPointer setting
		self['TypedPointer']=SYS.getTypedVariableWithVariableAndTypeString(self.TypedPointer,self.DerivedTypeString)




	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

