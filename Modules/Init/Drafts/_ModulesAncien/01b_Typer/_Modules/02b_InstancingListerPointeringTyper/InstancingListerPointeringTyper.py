#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancingListerPointeringTyperClass(
											SYS.TyperClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithInstancingListerPointeringTyper(self):
		
		#<DefineSpecificDict>
		self.InstancingTypingListerPointer=None
		#</DefineSpecificDict>

		#Update the DerivingTypeString
		self['DerivingTypeString']="Object"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingDicterPointeringTyper"]=[
													"InstancingTypingListerPointer"
												]

	#</DefineHookMethods>

	#<DefineBindingMethods>
	def setInstancingTypingListerPointerAfterBasesWithInstancingListerPointeringTyper(self):

		#Bind with InstancingTypingListerPointer_InstancedListedBaseTypeString setting
		self.setInstancingTypingListerPointer_InstancedListedBaseTypeStringWithInstancingListerPointeringTyper()

	def setDerivedTypeStringAfterBasesWithInstancingListerPointeringTyper(self):

		#Bind with InstancingTypingListerPointer_InstancedListedBaseTypeString setting
		self.setInstancingTypingListerPointer_InstancedListedBaseTypeStringWithInstancingListerPointeringTyper()
	#</DefineBindingMethods>

	#<DefineMethods>
	def setInstancingTypingListerPointer_InstancedListedBaseTypeStringWithInstancingListerPointeringTyper(self):
		if self.InstancingTypingListerPointer!=None:
			self.InstancingTypingListerPointer['InstancedListedBaseTypeString']=self.DerivedTypeString
	

	def setDerivingTypeStringWithInstancedListedBaseTypeStringWithInstancingListerPointeringTyper(self,_InstancedListedBaseTypeString):

		#Freeze the setDerivedTypeStringAfterBasesWithInstancingListerPointeringTyper Method
		self.FrozenMethodStringsList.append('setDerivedTypeStringAfterBasesWithInstancingListerPointeringTyper')

		#Bind with DerivingTypeString setting
		self['DerivingTypeString']=_InstancedListedBaseTypeString
	
		#Reput the Hook
		self.FrozenMethodStringsList.pop(self.FrozenMethodStringsList.index('setDerivedTypeStringAfterBasesWithInstancingListerPointeringTyper'))
	
	#</DefineMethods>

#</DefineClass>

