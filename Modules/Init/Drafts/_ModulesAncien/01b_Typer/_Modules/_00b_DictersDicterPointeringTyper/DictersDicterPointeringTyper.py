#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DictersDicterPointeringTyperClass(
							SYS.DicterPointeringTyperClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithDictersDicterPointeringTyper(self):
		
		#<DefineSpecificDict>
		self.DictersDicterPointer=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["DicterPointeringTyper"]=[
														"DictersDicterPointer"
												]

	#</DefineHookMethods>

	#<DefineBindingMethods>
	def setDicterPointerAfterBasesWithDicterPointeringTyper(self):

		#Bind with DicterPointer_DictionnaryTypeString setting
		self.setDicterPointer_DictionnaryTypeStringWithDicterPointerTyper()

	def setDerivedTypeStringAfterBasesWithDicterPointeringTyper(self):

		#Bind with DicterPointer_DictionnaryTypeString setting
		self.setDicterPointer_DictionnaryTypeStringWithDicterPointerTyper()
	#</DefineBindingMethods>

	#<DefineMethods>
	def setDictersDicterPointer_DerivingBaseTypeStringsListWithDictersDicter(self):

		#Set the DictionnaryTyper_DerivingBaseTypeStringsList in each dictated Object
		if self.DictersDicterPointer!=None:
			map(lambda DictatedDicter:
				DictatedDicter[1].DictionnaryTyper.__setitem__(
					'DerivingBaseTypeStringsList',
					DictatedDicter[1].DictionnaryTyper.DerivingBaseTypeStringsList+[self.DictersDicterTyper.DerivedTypeString]
					),
				getattr(self,self.DictersDicter.DictionnaryKeyString).__iter__())
	#</DefineMethods>

#</DefineClass>

