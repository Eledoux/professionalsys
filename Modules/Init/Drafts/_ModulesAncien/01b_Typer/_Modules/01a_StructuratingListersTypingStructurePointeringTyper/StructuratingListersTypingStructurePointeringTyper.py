#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StructuratingListersTypingStructurePointeringTyperClass(
							SYS.TyperClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithStructuratingListersTypingStructurePointeringTyper(self):
		
		#<DefineSpecificDict>
		self.StructuratingListersTypingStructurePointer=None
		#</DefineSpecificDict>

		#Update the DerivingTypeString
		self['DerivingTypeString']="StructuratingLister"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuratingListersTypingStructurePointeringTyper"]=[
													"StructuratingListersTypingStructurePointer"
												]

	#</DefineHookMethods>

	#<DefineBindingMethods>
	def setStructuratingListersTypingStructurePointerAfterBasesWithStructuratingListersTypingStructurePointeringTyper(self):

		#Bind with StructuratingListersPointer_StructuratingListersTypeString setting
		self.setStructuratingListersTypingStructurePointer_StructuratingListersTypeStringWithStructuratingListersTypingStructurePointeringTyper()

	def setDerivedTypeStringAfterBasesWithStructuratingListersTypingStructurePointeringTyper(self):

		#Bind with StructuratingListersPointer_StructuratingListersTypeString setting
		self.setStructuratingListersTypingStructurePointer_StructuratingListersTypeStringWithStructuratingListersTypingStructurePointeringTyper()
	#</DefineBindingMethods>

	#<DefineMethods>
	def setStructuratingListersTypingStructurePointer_StructuratingListersTypeStringWithStructuratingListersTypingStructurePointeringTyper(self):
		if self.StructuratingListersTypingStructurePointer!=None:
			self.StructuratingListersTypingStructurePointer['StructuratingListersTypeString']=self.DerivedTypeString
	#</DefineMethods>

#</DefineClass>

