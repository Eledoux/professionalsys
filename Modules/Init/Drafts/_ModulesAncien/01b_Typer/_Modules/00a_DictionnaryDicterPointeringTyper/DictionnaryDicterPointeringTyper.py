#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DictionnaryDicterPointeringTyperClass(
							SYS.TyperClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithDictionnaryDicterPointeringTyper(self):
		
		#<DefineSpecificDict>
		self.DictionnaryTypedDicterPointer=None
		#</DefineSpecificDict>

		#Update the DerivingTypeString
		self['DerivingTypeString']="DicterPointeringDictionnary"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["DictionnaryDicterPointeringTyper"]=[
													"DictionnaryTypedDicterPointer"
												]

	#</DefineHookMethods>

	#<DefineBindingMethods>
	def setDictionnaryTypedDicterPointerAfterBasesWithDictionnaryDicterPointeringTyper(self):

		#Bind with DicterPointer_DictionnaryTypeString setting
		self.setDictionnaryTypedDicterPointer_DictionnaryTypeStringWithDicterPointerTyper()

	def setDerivedTypeStringAfterBasesWithDictionnaryDicterPointeringTyper(self):

		#Bind with DicterPointer_DictionnaryTypeString setting
		self.setDictionnaryTypedDicterPointer_DictionnaryTypeStringWithDicterPointerTyper()
	#</DefineBindingMethods>

	#<DefineMethods>
	def setDictionnaryTypedDicterPointer_DictionnaryTypeStringWithDictionnaryDicterPointeringTyper(self):
		if self.DictionnaryTypedDicterPointer!=None:
			self.DictionnaryTypedDicterPointer['DictionnaryTypeString']=self.DerivedTypeString
	#</DefineMethods>

#</DefineClass>

