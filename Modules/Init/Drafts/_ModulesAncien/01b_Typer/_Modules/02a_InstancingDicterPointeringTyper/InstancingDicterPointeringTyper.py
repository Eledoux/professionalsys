#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancingDicterPointeringTyperClass(
							SYS.TyperClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithInstancingDicterPointeringTyper(self):
		
		#<DefineSpecificDict>
		self.InstancingTypingDicterPointer=None
		#</DefineSpecificDict>

		#Update the DerivingTypeString
		self['DerivingTypeString']="Object"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingDicterPointeringTyper"]=[
													"InstancingTypingDicterPointer"
												]

	#</DefineHookMethods>

	#<DefineBindingMethods>
	def setInstancingTypingDicterPointerAfterBasesWithInstancingDicterPointeringTyper(self):

		#Bind with DicterPointer_InstancedDictatedBaseTypeString setting
		self.setInstancingTypingDicterPointer_InstancedDictatedBaseTypeStringWithInstancingDicterPointeringTyper()

	def setDerivedTypeStringAfterBasesWithInstancingDicterPointeringTyper(self):

		#Bind with DicterPointer_InstancedDictatedBaseTypeString setting
		self.setInstancingTypingDicterPointer_InstancedDictatedBaseTypeStringWithInstancingDicterPointeringTyper()
	#</DefineBindingMethods>

	#<DefineMethods>
	def setInstancingTypingDicterPointer_InstancedDictatedBaseTypeStringWithInstancingDicterPointeringTyper(self):
		if self.InstancingTypingDicterPointer!=None:
			self.InstancingTypingDicterPointer['InstancedDictatedBaseTypeString']=self.DerivedTypeString
	#</DefineMethods>

#</DefineClass>

