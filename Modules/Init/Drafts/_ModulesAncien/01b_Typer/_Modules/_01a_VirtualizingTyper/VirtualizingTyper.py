#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class VirtualizingTyperClass(
							SYS.TyperClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithVirtualizingTyper(self):
		
		#<DefineSpecificDict>
		self.VirtualClass=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["PointeringTyper"]=[
														"VirtualClass"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setDerivedTypeStringAfterBasesWithVirtualizingTyper(self):


		if self.DerivedTypeString==self.DerivingTypeString and len(self.DerivingBaseTypeStringsList)>0:

			if any(
					map(
						lambda DerivedBaseTypeString:
						getattr(SYS,DerivedBaseTypeString)!=None,
						self.DerivingBaseTypeStringsList
						)
					):

				#Define the VirtualTypeString
				VirtualTypeString=self.DerivedTypeString+str(self.DerivingBaseTypeStringsList)

				"""
				#Build a Virtual Class made from the self.DerivedTypeString and str(self.DerivingBaseTypeStringsList)
				ClassCodeString="class "+SYS.getClassStringWithTypeString(VirtualTypeString)
				ClassCodeString+="("
				ClassCodeString+="SYS."+SYS.getClassStringWithTypeString(self.DerivedTypeString)
				ClassCodeString+=map(
										lambda DerivingBaseTypeString:
										DerivingBaseTypeString
									),
									self.DerivingBaseTypeStringsList)
				"""

				#Add the other bases
				LocalClass.__bases__=tuple(list(LocalClass.__bases__)+map(
					lambda DerivingBaseTypeString:
					getattr(SYS,SYS.getClassStringWithTypeString(DerivingBaseTypeString)),
					self.DerivingBaseTypeStringsList))

				#bind with VirtualClass setting
				self['VirtualClass']=LocalClass

	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

