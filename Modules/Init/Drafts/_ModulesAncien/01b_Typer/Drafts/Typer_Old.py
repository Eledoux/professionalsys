#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class TyperClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithTyper(self):
		
		#<DefineSpecificDict>
		self.DerivedTypeString=""
		self.DerivingTypeString="Core"
		self.DerivingBaseTypeStringsList=[]
		self.IsTypedBool=True
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Typer"]=[
														"DerivedTypeString",
														"DerivingTypeString",
														"DerivingBaseTypeStringsList",
														"IsTypedBool"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setDerivingBaseTypeStringsListAfterBasesWithTyper(self):
		
		#Just use the set to have just one TypeString for each different Type
		self.DerivingBaseTypeStringsList=list(set(self.DerivingBaseTypeStringsList))

		#Bind With DerivedTypeString Setting
		self.setDerivedTypeStringWithTyper()

	def setDerivingTypeStringAfterBasesWithTyper(self):

		#Bind With DerivedTypeString Setting
		self.setDerivedTypeStringWithTyper()

	def setDerivedTypeStringAfterBasesWithTyper(self):

		#Bind with IsTypedBool Setting
		if self.DerivedTypeString=="":
			self['IsTypedBool']=False
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def setDerivedTypeStringWithTyper(self):
		self['DerivedTypeString']=SYS.getTypedStringWithTypeStringAndBaseTypeStringsList(self.DerivingTypeString,
			self.DerivingBaseTypeStringsList)

		
	#</DefineMethods>

#</DefineClass>

