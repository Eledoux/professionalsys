#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Type"
DoingString="Typing"
DoneString="Typed"
#<DefineLocals>

#<DefineClass>
class TyperClass(
					SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfterWithTyper(self):
		
		#<DefineSpecificDict>
		self.TypedTypeString=""
		self.TypingTypeString="Core"
		self.TypingBaseTypeStringsList=[]
		self.IsTypedBool=True
		#</DefineSpecificDict>

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindTypingBaseTypeStringsListAfterWithTyper(self):
		
		#Just use the set to have just one TypeString for each different Type
		self.TypingBaseTypeStringsList=list(set(self.TypingBaseTypeStringsList))

		#Bind With TypedTypeString Setting
		self.setTypedTypeStringWithTyper()

	def bindTypingTypeStringAfterWithTyper(self):

		#Bind With TypedTypeString Setting
		self.setTypedTypeStringWithTyper()

	def bindTypedTypeStringAfterWithTyper(self):

		#Bind with IsTypedBool Setting
		if self.TypedTypeString=="":
			self['IsTypedBool']=False
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def setTypedTypeStringWithTyper(self):
		self['TypedTypeString']=SYS.getTypedStringWithTypeStringAndBaseTypeStringsList(self.TypingTypeString,
			self.TypingBaseTypeStringsList)
	#</DefineMethods>

#</DefineClass>

