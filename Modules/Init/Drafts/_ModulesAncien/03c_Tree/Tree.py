#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class TreeClass(SYS.DicterClass):

	#<DefineHookMethods>
	def initAfterBasesWithTree(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Tree"]=[
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
