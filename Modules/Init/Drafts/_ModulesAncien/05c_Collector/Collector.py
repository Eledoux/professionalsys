#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class CollectorClass(
					SYS.UserClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithCollector(self):

		#<DefineSpecificDict>
		self+={'UsedTypeString':"Structurer"}
		self+={'UsedTypeString':"Sorter","PrefixString":"CollectingItemizers"}
		self+={'UsedTypeString':"Sorter","PrefixString":"CollectedVariables"}
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Collector"]=[]

	def callAfterBasesWithCollector(self,*_ArgsList,**_KwargsDict):

		#Check that the _KwargsDict is ok
		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="Collection":
				if 'CollectedVariablesList' in _KwargsDict:
					
					#Call the Structurer to store this collection
					if type(_KwargsDict['CollectedVariablesList'])==list:
						self['<Used>OfCollector_Structurer'].__add__(_KwargsDict['CollectedVariablesList'])

	'''
	def reprAfterBasesWithSorter(self,SelfString,PrintedDict):

		#Define the new PrintedDict with SkippedKeyStringsList
		NewPrintedDict=self['<Used>OfSorter_Itemizer']

		#Define the new SelfString
		SelfString=SYS.ConsolePrinter.getPrintedVariableString(NewPrintedDict)

		#Return
		return (SelfString,NewPrintedDict)
		
	'''
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>	

	#<DefineMethods>
	#<DefineMethods>	

#</DefineClass>