#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#Define the PluralString
PluralString="Dictionnaries"

#<DefineClass>
class DicterClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithDicter(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Dicter"]=[
												]
	def iterAfterBasesWithDicter(self,_IteratedList):

		#Return the dictated Items with "_"
		return filter(lambda Item:len(Item[0])>0 and Item[0][0]=="_",self.__dict__.items())

	def measureAfterBasesWithDicter(self,_MeasureVariable):

		#Measure only the ones with a KeyString that has a "_" at the beginning
		return len(filter(lambda KeyString:KeyString[0]=="_",self.keys()))
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
