#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancingDicterClass(SYS.DicterClass):

	#<DefineHookMethods>
	def initAfterBasesWithInstancingDicter(self):

		#<DefineSpecificDict>
		self.InstancedDictatedBaseTypeString="Object"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingDicter"]=[
													"InstancedDictatedBaseTypeString"
												]


	def setBeforeBasesWithInstancingDicter(self,_KeyVariable,_ValueVariable):

		#Return an InstancedVariable
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[0]=="_":
					return (_KeyVariable,SYS.getTypedVariableWithVariableAndTypeString(_ValueVariable,self.InstancedDictatedBaseTypeString))

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setInstancedDictatedBaseTypeStringAfterBasesWithInstancingDicter(self):

		#Get the possible DicterClassString
		ClassString=SYS.getClassStringWithTypeString(self.InstancedDictatedBaseTypeString)

		#Bind with DicterTypeString setting
		Class=getattr(SYS,ClassString)
		if Class==None:
			self['InstancedDictatedBaseTypeString']="Object"
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
