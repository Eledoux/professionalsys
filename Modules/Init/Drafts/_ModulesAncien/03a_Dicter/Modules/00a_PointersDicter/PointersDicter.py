#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class PointersDicterClass(SYS.DicterClass):
	
	#<DefineHookMethods>	
	def getBeforeBasesWithPointersDicter(self,_KeyVariable):

		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable[0]=="_":

				#Get the SuffixString
				SuffixString=SYS.getCommonSuffixStringWithStringsList(["Pointer",_KeyVariable])

				#Add the Pointer word
				if SuffixString=="":

					#Return with the NewKeyVariable
					return self[_KeyVariable+"Pointer"]

	def setBeforeBasesWithPointersDicter(self,_KeyVariable,_ValueVariable):

		#Return an InstancedVariable
		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable[0]=="_":
				if hasattr(_ValueVariable,'__dict__'):
					return (_KeyVariable+"Pointer",_ValueVariable)

	#</DefineHookMethods>
	#<DefineBindingMethods>
	
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

