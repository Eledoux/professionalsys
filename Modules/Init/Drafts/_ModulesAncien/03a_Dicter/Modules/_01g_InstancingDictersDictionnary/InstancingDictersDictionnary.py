#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancingDictersDictionnaryClass(
								SYS.InstancingDictionnaryClass
							):
	
	#<DefineHookMethods>
	def initAfterBasesWithInstancingDictersDictionnary(self):

		#<DefineSpecificDict>
		self['InstancedDictatedBaseTypeString']="InstancingDicter"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingDictersDictionnary"]=[
												]

	#</DefineHookMethods>

	#<DefineBindingMethods>
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

