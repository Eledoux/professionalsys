#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class FilteringDicterClass(SYS.DicterClass):

	#<DefineHookMethods>
	def initAfterBasesWithFilteringDicter(self):

		#<DefineSpecificDict>
		self.FromDicterFilter=SYS.FromDicterFilterClass().update({'FilteringDicterPointer':self})
		self.FromDicterFilterTypeString="FromDicterFilter"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["FilteringDicter"]=[
													"FromDicterFilter",
													"FromDicterFilterTypeString"
												]

	def setBeforeBasesWithFilteringDicter(self,_KeyVariable,_ValueVariable):

		#Case of setting in the Dictionnary
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[0]=="_":

					#Call the FilteringFunctor
					IsDictatedBool=self.FromDicterFilter(*[[_KeyVariable,_ValueVariable]])

					#Set if IsDictatedBool or not
					if IsDictatedBool:
						return (_KeyVariable,_ValueVariable)
					else:
						return ("x"+_KeyVariable,_ValueVariable)
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setFromDicterFilterTypeStringAfterBasesWithFilteringDicter(self):

		#Bind with FilteringFunctor setting
		self.FromDicterFilter=SYS.getTypedVariableWithVariableAndTypeString(
			self.FromDicterFilter,self.FromDicterFilterTypeString)

	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
