#<ImportModules>
import itertools
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Scan"
DoingString="Scanning"
DoneString="Scanned"
ScanningTagString="<Scanning>"
ScannedTagString="<Scanned>"
#</DefineLocals>

#<DefineFunctions>
def getScannedTuplesListsListWithScannedTuplesListAndNonScannedItemTuplesListAndScannedKeyStringsList(_ScannedTuplesList,_NonScannedItemTuplesList,_ScannedKeyStringsList):

	#Do the product between Selected Items
	ProductTuplesList=list(itertools.product(*map(lambda Tuple:Tuple[1],_ScannedTuplesList)))

	#Change the KeyStrings by removig the "List" suffix
	KeyStringsList=map(
						lambda KeyString:
						SYS.getSingularStringWithPluralString(KeyString.split("List")[0]).split(ScannedTagString)[1],
						_ScannedKeyStringsList
						)

	#Zip then
	ScannedTuplesListsList=map(
							lambda ProductTuple:
							zip(KeyStringsList,ProductTuple)+_NonScannedItemTuplesList,
							ProductTuplesList
							)

	#Return 
	return ScannedTuplesListsList	
SYS.getScannedTuplesListsListWithScannedTuplesListAndNonScannedItemTuplesListAndScannedKeyStringsList=getScannedTuplesListsListWithScannedTuplesListAndNonScannedItemTuplesListAndScannedKeyStringsList

def getScannedTuplesListsListWithScanDict(_ScanDict):
	
	#Filter the only ScannedTagString Items
	ScannedTuplesList=filter(
						lambda KeyStringAndValueTuple:
						SYS.getCommonPrefixStringWithStringsList([ScannedTagString,KeyStringAndValueTuple[0]])==ScannedTagString,
						_ScanDict.items()
					)

	#Set the Scanned
	ScannedKeyStringsList=map(lambda Tuple:Tuple[0],ScannedTuplesList)

	#Define also the non Scanned Items
	NonScannedItemTuplesList=filter(
								lambda Tuple:
								Tuple[0] not in ScannedKeyStringsList,
								_ScanDict.items()
							)

	#Return 
	return getScannedTuplesListsListWithScannedTuplesListAndNonScannedItemTuplesListAndScannedKeyStringsList(
		ScannedTuplesList,NonScannedItemTuplesList,ScannedKeyStringsList)
SYS.getScannedTuplesListsListWithScanDict=getScannedTuplesListsListWithScanDict

def getScannedTuplesListsListWithScanningTuplesList(_ScanningTuplesList):
	
	#Filter the only ScannedTagString Items
	ScannedTuplesList=filter(
						lambda KeyStringAndValueTuple:
						SYS.getCommonPrefixStringWithStringsList([ScannedTagString,KeyStringAndValueTuple[0]])==ScannedTagString,
						_ScanDict.items()
					)

	#Set the Scanned
	ScannedKeyStringsList=map(lambda Tuple:Tuple[0],ScannedTuplesList)

	#Define also the non Scanned Items
	NonScannedItemTuplesList=filter(
								lambda Tuple:
								Tuple[0] not in ScannedKeyStringsList,
								_ScanDict.items()
							)

	#Return 
	return getScannedTuplesListsListWithScannedTuplesListAndNonScannedItemTuplesListAndScannedKeyStringsList(
		ScannedTuplesList,NonScannedItemTuplesList,ScannedKeyStringsList)
SYS.getScannedTuplesListsListWithScanDict=getScannedTuplesListsListWithScanDict


def getScannedStringsListWithScanningTuplesListsList(_ScanningTuplesListsList):
	return map(
				lambda ListedTuples:
				reduce(lambda AccumulatedTupleString,TupleString:
						AccumulatedTupleString.__add__("_"+TupleString),
						map(
							lambda Tuple: 
							"("+Tuple[0]+","+str(Tuple[1])+")",
							ListedTuples
							)
						),
				_ScanningTuplesListsList
				)
SYS.getScannedStringsListWithScanningTuplesListsList=getScannedStringsListWithScanningTuplesListsList

def getScannedStringsListWithScanDict(_ScanDict):
	return getScannedStringsListWithScanningTuplesListsList(
		getScannedTuplesListsListWithScanDict(_ScanDict)
		)
SYS.getScannedStringsListWithScanDict=getScannedStringsListWithScanDict

def getFilteredScannedTuplesListsListWithFilterDict(_ScannedTuplesListsList,_FilterDict):

	#Define the FilteringKeyStringsSet
	FilteringKeyStringsSet=set(_FilterDict.keys())
	FilteringVariables=_FilterDict.values()

	#First keep only the ones that have the FilteringKeyStringsSet
	FilteredScannedTuplesListsList=filter(
			lambda TuplesList:
			 FilteringKeyStringsSet.issubset(set(map(lambda Tuple:Tuple[0],TuplesList))),
			_ScannedTuplesListsList
		)

	
	#Then track the ones with the good values
	FilteredScannedTuplesListsList=filter(
											lambda TuplesList:
											map(
												lambda IndexInt:
												TuplesList[IndexInt][1]
												,
												map(
													lambda FilteringKeyString:
													map(lambda Tuple:Tuple[0],TuplesList).index(FilteringKeyString),
													list(FilteringKeyStringsSet)
													)
												)==FilteringVariables,
											FilteredScannedTuplesListsList
										)


	#Return 
	return FilteredScannedTuplesListsList
SYS.getFilteredScannedTuplesListsListWithFilterDict=getFilteredScannedTuplesListsListWithFilterDict

def getFilteredKeyStringsListWithFilterDict(_KeyStringsList,_FilterDict):

	print(map(
				lambda KeyString:
				KeyString.split("_"),
				_KeyStringsList
			))

	return getFilteredScannedTuplesListsListWithFilterDict(
		map(
				lambda KeyString:
				KeyString.split("_"),
				_KeyStringsList
			),
		_FilterDict)
SYS.getFilteredKeyStringsListWithFilterDict=getFilteredKeyStringsListWithFilterDict

#</DefineFunctions>

#<DefineClass>
class ScannerClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithScanner(self):

		#<DefineSpecificDict>
		self.ScanningTuplesList=[]
		self.ScannedTuplesListsList=[]
		self.ScannedStringsList=[]
		#</DefineSpecificDict>
	
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def setScanningTuplesListAfterBasesWithScanner(self):

		#Bind with ScannedTuplesListsList setting
		self.setScannedTuplesListsListWithScanner()

	def setScannedTuplesListsListAfterBasesWithScanner(self):

		#Bind with ScannedStringsList
		self.setScannedStringsList()
	#<DefineBindingHookMethods>	

	#<DefineMethods>
	def setScannedTuplesListsListWithScanner(self):

		#Do the product
		ProductTuplesList=list(itertools.product(*map(lambda Tuple:Tuple[1],self.ScanningTuplesList)))

		#Change the KeyStrings by removig the "List" suffix
		KeyStringsList=map(
							lambda ScanningTuple:
							SYS.getSingularStringWithPluralString(ScanningTuple[0].split("List")[0]),
							self.ScanningTuplesList
							)

		#Zip then
		self['ScannedTuplesListsList']=map(
								lambda ProductTuple:
								zip(KeyStringsList,ProductTuple),
								ProductTuplesList
								)

	def setScannedStringsList(self):

		#Concatenate the ScannedTuples into one string for each set
		self['ScannedStringsList']=map(
				lambda ScannedTuplesList:
				"_".join(map(
						lambda ScannedTuple:
						"("+str(ScannedTuple[0])+","+str(ScannedTuple[1])+")",
						ScannedTuplesList
					)),
				self.ScannedTuplesListsList
				)
	#<DefineMethods>	

#</DefineClass>