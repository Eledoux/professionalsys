#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancerClass(
					SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfterWithInstancer(self):

		#<DefineSpecificDict>
		self.InstancedVariable=None
		self.InstancingTypeString=""
		self.InstancingDict={}
		self.InstancingTuplesList=[]
		#</DefineSpecificDict>


	def callAfterWithInstancer(self,_CallString,*_ArgsList,**_KwargsDict):

		if _CallString=="Instance":

			#Instancify
			self.InstancedVariable=SYS.getTypedVariableWithVariableAndTypeString(
				self.InstancedVariable,
				self.InstancingTypeString
			)

			#Check if it is ok
			if self.InstancedVariable==None:
				print("WARNING in Instancer : didn't find an Instance like "+self.InstancingTypeString)
			else:
				#Give him the InstancingDict
				self.InstancedVariable.update(self.InstancingTuplesList)

				#Give him the InstancingDict
				self.InstancedVariable.update(self.InstancingDict)

		#Return self
		return self

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindInstancedVariableAfterWithInstancer(self):

		#Bind with InstancingTypeString setting
		self.InstancingTypeString=self.InstancedVariable.TypeString
	
	def bindInstancingDictAfterWithInstancer(self):

		#Bind with InstancedVariable setting
		if self.InstancedVariable!=None:
			self.InstancedVariable.update(self.InstancingDict)

	def bindInstancingTuplesListAfterWithInstancer(self):

		#Bind with InstancedVariable setting
		if self.InstancedVariable!=None:
			self.InstancedVariable.update(self.InstancingTuplesList)
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
