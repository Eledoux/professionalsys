#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancerClass(
					SYS.InstancerClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithInstancer(self):

		#<DefineSpecificDict>
		self.InstancedVariable=None
		self.InstancingTypeString=""
		self.InstancingDict={}
		#</DefineSpecificDict>

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setInstancedVariableAfterBasesWithInstancer(self):

		#Bind with InstancingTypeString setting
		self.InstancingTypeString=self.InstancedVariable.TypeString
		
	def setInstancingTypeStringAfterBasesWithInstancer(self):

		#Bind with InstancedVariable setting
		self.InstancedVariable=SYS.getTypedVariableWithVariableAndTypeString(
			self.InstancedVariable,
			self.InstancingTypeString
		)

		#Check if it is ok
		if self.InstancedVariable==None:
			print("WARNING in Instancer : didn't find an Instance like "+self.InstancingTypeString)

	def setInstancingDictAfterBasesWithInstancer(self):

		#Bind with InstancedVariable setting
		if self.InstancedVariable!=None:
			self.InstancedVariable.update(self.InstancingDict)
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
