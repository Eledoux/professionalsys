#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class RecordedObjectClass(
							SYS.UsedObjectClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithRecordedObject(self):

		#<DefineSpecificDict>
		self.IsRecordedBool=False
		#</DefineSpecificDict>
	
	def callAfterBasesWithRecordedObject(self,*_ArgsList,**_KwargsDict):

		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="Record":

				if self.UsingRecorderPointer!=None:

					#Get the RecordedString
					IdString=str(id(self))
					if IdString in self.UsingRecorderPointer.IdStringToUsedKeyStringDict:

						#Define the RecordedString
						RecordedString=self.UsingRecorderPointer.IdStringToUsedKeyStringDict[IdString]

						#Bind with UsingRecorderPointer_RecordedDict setting
						self.UsingRecorderPointer(**{
														'TypeDictString':"Record",
														'RecordedPointer':self,
														'RecordedString':RecordedString,
														'RecordingKeyStringsList':self.StimulatingStringsList,
														'RecordedKeyStringsList':self.StimulatedStringsList
													}
												)

		#Return self
		return self
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
