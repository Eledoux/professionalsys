#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class IdentifiedObjectClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithIdentifiedObject(self):

		#<DefineSpecificDict>
		self.IdentitiesDictionnary=SYS.DictionnaryClass()
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["IdentifiedObject"]=[
													"IdentitiesDictionnary"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
