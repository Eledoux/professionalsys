#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SortedStructuratingObjectClass(
								SYS.SortedStructuratingStructuresPointeringObjectClass
								):

	#<DefineHookMethods>
	def initAfterBasesWithSortedStructuratingObject(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SortedStructuratingObject"]=[
												]
	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

