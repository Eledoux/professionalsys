#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DictatedObjectClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithDictatedObject(self):

		#<DefineSpecificDict>
		self.DictionnariesPointersList=[]
		self.DictatedString=""
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["DictatedObject"]=[
													"DictionnariesPointersList",
													"DictatedString"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setGrandParentPointersListAfterBasesWithDictatedObject(self):

		#Bind with DicterPointersList setting
		self['DicterPointersList']=filter(lambda Pointer:Pointer!=None,map(lambda GrandParentPointer:getattr(GrandParentPointer,GrandParentPointer.DictKeyString) if hasattr(GrandParentPointer,"DictKeyString") else None,self.GrandParentPointersList))

	def setDicterPointersListAfterBasesWithDictatedObject(self):
		
		#Bind with DictatedPointers setting
		self.setDictatedPointersWithDictatedObject()

	def setDictatedStringAfterBasesWithDictatedObject(self):
		
		#Bind with DictatedPointers setting
		self.setDictatedPointersWithDictatedObject()

	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def setDictatedPointersWithDictatedObject(self):

		#Define the IsSettedBoolsList
		IsSettedBoolsList=map(lambda DicterPointer:self in DicterPointer.values(),self.DicterPointersList)

		#Define the IndexIntsList
		IndexIntsList=map(lambda DicterPointer,IsSettedBool:DicterPointer.values().index(self) if IsSettedBool else None,self.DicterPointersList,IsSettedBoolsList)

		#Define the OldKeyStringsList
		OldKeyStringsList=map(lambda DicterPointer,IndexInt:DicterPointer.keys()[IndexInt] if IndexInt!=None else None,self.DicterPointersList,IndexIntsList)

		#Define the NewKeyString
		NewKeyString=self.DictatedString+"Pointer"
		
		#Set or rename
		map(lambda DicterPointer,IndexInt,OldKeyString:DicterPointer.__setitem__(NewKeyString,self) if IndexInt==None else DicterPointer.rename(OldKeyString,NewKeyString),self.DicterPointersList,IndexIntsList,OldKeyStringsList)

	#</DefineMethods>

#</DefineClass>
