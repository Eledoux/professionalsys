#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DictatedObjectClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithDictatedObject(self):

		#<DefineSpecificDict>
		self.DictatingDicterPointersList=[]
		self.DictatingStringsList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["DictatedObject"]=[
													"DictatingDicterPointersList",
													"DictatingStringsList"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
