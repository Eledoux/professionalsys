#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class NamedObjectClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithNamedObject(self):

		#<DefineSpecificDict>
		self.NamedVariable=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["NamedObject"]=[
													"NamedVariable"
												]
	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

