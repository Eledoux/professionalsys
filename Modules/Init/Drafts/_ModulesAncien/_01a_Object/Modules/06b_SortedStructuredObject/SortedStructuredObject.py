#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SortedStructuredObjectClass(
								SYS.SortedStructuratingStructuresPointeringObjectClass,
								SYS.StructuredObjectClass
							):

	#<DefineHookMethods>
	def initAfterBasesWithSortedStructuredObject(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SortedStructuredObject"]=[
												]
	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

