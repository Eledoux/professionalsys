#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StructuredObjectClass(
								SYS.StructuratingStructuresPointeringObjectClass,
								SYS.ListedObjectClass
							):

	#<DefineHookMethods>
	def initAfterBasesWithStructuredObject(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuredObject"]=[
												]
	#</DefineHookMethods>

	#<DefineMethods>
	def setListingListerPointersListAfterBasesWithStructuredObject(self):

		#Bind with StructuratingStructurePointersList
		if len(self.ListingListerPointersList)>0:
			if hasattr(self.ListingListerPointersList[0],"StructuratingStructurePointersList"):
				self['StructuratingStructurePointersList']=self.ListingListerPointersList[0].StructuratingStructurePointersList


	#</DefineMethods>

#</DefineClass>

