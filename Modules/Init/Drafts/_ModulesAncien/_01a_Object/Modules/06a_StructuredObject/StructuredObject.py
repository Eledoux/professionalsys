#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StructuredObjectClass(
								SYS.ObjectClass
							):

	#<DefineHookMethods>
	def initAfterBasesWithStructuredObject(self):

		#<DefineSpecificDict>
		self.ParentStructurerPointersList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuredObject"]=[
												]
	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

