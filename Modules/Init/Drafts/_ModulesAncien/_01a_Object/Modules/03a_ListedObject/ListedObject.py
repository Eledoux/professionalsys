#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ListedObjectClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithListedObject(self):

		#<DefineSpecificDict>
		self.ListingListerPointersList=[]
		self.ListingIntsList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["ListedObject"]=[
													"ListingListerPointersList",
													"ListingIntsList"
												]
	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

