#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SortedStructuratingStructuresPointeringObjectClass(
															SYS.StructuratingStructuresPointeringObjectClass
														):

	#<DefineHookMethods>
	def initAfterBasesWithSortedStructuratingStructuresPointeringObject(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SortedStructuratingStructuresPointeringObject"]=[
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setStructuratingStructurePointersListAfterBasesWithSortedStructuratingStructuresPointeringObject(self):

		#Set in each possible sorting StructuratingStructurePointer a sort of this Object
		self.setInEachStructuratingStructurePointer()

	def setListingIntsListAfterBasesWithSortedStructuratingStructuresPointeringObject(self):

		#Set in each possible sorting StructuratingStructurePointer a sort of this Object
		self.setInEachStructuratingStructurePointer()
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setInEachStructuratingStructurePointer(self):

		if len(self.ListingIntsList)==len(self.ListingListerPointersList):

			#Set in each possible sorting StructuratingStructurePointer a sort of this Object
				map(lambda StructuratingStructurePointer:
					StructuratingStructurePointer.StructuratingTypesSorter.__setitem__(
							"*_"+str(self.ListingIntsList),
							self
						) 
					if hasattr(StructuratingStructurePointer,"StructuratingTypesSorter")
					else None,
					self.StructuratingStructurePointersList)
	#</DefineHookMethods>

#</DefineClass>

