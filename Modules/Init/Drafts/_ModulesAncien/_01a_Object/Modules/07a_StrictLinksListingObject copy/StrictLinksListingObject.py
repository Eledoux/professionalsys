#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ObjectClass(SYS.CoreClass):

	#<Define HookMethods>
	def initAfterBasesWithObject(self):

		#<DefineSpecificDict>
		self.SkippedKeyStringsList=[
									"SkippedKeyStringsList",
									"TypeString",
									"AfterBasesCalledTypeStringsList",
									"BeforeBasesCalledTypeStringsList",
									"FrozenMethodStringsList",
									"HookStringToMethodDict",
									"TypeStringToKeyStringsListDict"
								]+["TypeStringToIs"+CommandedString+"BoolDict" for CommandedString in SYS.CommandStringToCommandedStringDict.values()]+[
								"Is"+CommandingString+"Bool" for CommandingString in SYS.CommandStringToCommandingStringDict.values()]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Object"]=[
													"SkippedKeyStringsList"
												]

	def reprAfterBasesWithObject(self,_SelfString,_PrintedDict):

		#Define the NewPrintedDict
		NewPrintedDict=dict(self.items())

		#Define the new PrintedDict with SkippedKeyStringsList
		PrintedDict=dict(filter(lambda Item:Item[0] not in self.SkippedKeyStringsList,NewPrintedDict.items()))

		#Define the new SelfString
		SelfString=SYS.ConsolePrinter.getPrintedVariableString(PrintedDict)

		#Return
		return (SelfString,PrintedDict)

	def setBeforeBasesWithObject(self,_KeyVariable,_ValueVariable):

		#Handle also Setting in the deeper Elements
		KeyVariableStringsList=_KeyVariable.split("_")
		if len(KeyVariableStringsList)>1:

			#if hasattr(self,KeyVariableStringsList[0]):
			GettedVariable=self[KeyVariableStringsList[0]]
			if GettedVariable!=None:
				
				#Set in the first deeper Element with the Cutted KeyVariable
				#getattr(self,KeyVariableStringsList[0])['_'.join(KeyVariableStringsList[1:])]=_ValueVariable
				self[KeyVariableStringsList[0]]['_'.join(KeyVariableStringsList[1:])]=_ValueVariable

				#Stop the Setting at this level
				self.IsSettingBool=False
			
	#</Define HookMethods>

	#<Define Methods>
	def setWithTransformedStringAndKeyString(self,_TransformedString,_KeyString):
		setattr(self,_KeyString,getattr(SYS,"get"+_TransformedString+_KeyString+
			"With"+SYS.getNameStringWithClassString(self.__class__.__name__))(self))
	#</Define HookMethods>

#</DefineClass>

