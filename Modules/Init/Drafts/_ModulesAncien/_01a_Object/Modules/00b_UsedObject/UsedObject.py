#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class UsedObjectClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithUsedObject(self):

		#<DefineSpecificDict>
		self.UserKeyString="Of".join(self.TypeString.split("Of")[1:]).split("_")[0]
		setattr(self,"Using"+self.UserKeyString+"Pointer",None)
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["UsedObject"]=[
													"UserKeyString"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
