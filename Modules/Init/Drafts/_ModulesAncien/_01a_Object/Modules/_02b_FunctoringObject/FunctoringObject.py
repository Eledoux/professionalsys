#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class FunctoringObjectClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithFunctoringObject(self):

		#<DefineSpecificDict>
		self.Functor=SYS.FunctorClass()
		self.FunctorTypeString="Functor"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["NamedObject"]=[
													"Functor"
													"FunctorTypeString"
												]
	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

