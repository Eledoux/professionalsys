#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SortedObjectClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithSortedObject(self):

		#<DefineSpecificDict>
		self.SortedString=""
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SortedObject"]=[
													"SortedString"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
