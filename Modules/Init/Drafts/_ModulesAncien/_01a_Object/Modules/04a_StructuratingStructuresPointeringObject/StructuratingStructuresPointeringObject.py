#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StructuratingStructuresPointeringObjectClass(
													SYS.ObjectClass
											):

	#<DefineHookMethods>
	def initAfterBasesWithStructuratingStructuresPointeringObject(self):

		#<DefineSpecificDict>
		self.StructuratingStructurePointersList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuratingStructuresPointeringObject"]=[
													"StructuratingStructurePointersList"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineHookMethods>

#</DefineClass>

