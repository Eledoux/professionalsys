#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>


#<DefineClass>
class ObjectClass(SYS.HookerClass):

	#<Define HookMethods>
	def initAfterWithObject(self):

		#<DefineSpecificDict>
		self.SkippedKeyStringsList=[
									"SkippedKeyStringsList",
									"TypeString",
									"AfterHookedTypeStringsList",
									"BeforeHookedTypeStringsList",
									"FrozenHookedMethodStringsList",
									"HookStringToMethodDict",
								]+["TypeStringToIs"+CommandedString+"BoolDict" for CommandedString in SYS.CommandStringToCommandedStringDict.values()]+[
								"Is"+CommandingString+"Bool" for CommandingString in SYS.CommandStringToCommandingStringDict.values()]
		#</DefineSpecificDict>

	def reprAfterWithObject(self,_SelfString,_PrintedDict):

		#Define the NewPrintedDict
		NewPrintedDict=dict(self.items())

		#Define the new PrintedDict with SkippedKeyStringsList
		PrintedDict=dict(
						filter(
							lambda Item:
							Item[0] not in self.SkippedKeyStringsList,
							NewPrintedDict.items()
							)
						)

		#Define the new SelfString
		SelfString=SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(PrintedDict,{
																					'AlineaString':"",
																					'LineJumpString':"\n",
																					'ElementString':"   /",
																					'IsPrintedKeysBool':True,
																					'IsPrintedValuesBool':True
																				}
																	)

		#Return
		return (SelfString,PrintedDict)	
	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

