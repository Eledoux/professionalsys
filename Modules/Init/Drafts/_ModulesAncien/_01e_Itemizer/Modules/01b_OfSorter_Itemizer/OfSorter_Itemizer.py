#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class OfSorter_ItemizerClass(
								SYS.UsedObjectClass,
								SYS.ItemizerClass
							):

	

	#<DefineHookMethods>
	def reprAfterBasesWithOfSorter_Itemizer(self,SelfString,PrintedDict):

		#Update
		PrintedDict.update({'UsingSorterPointer':self.UsingSorterPointer})

		#Define the new SelfString
		SelfString=SYS.ConsolePrinter.getPrintedVariableString(PrintedDict)

		#Return
		return (SelfString,PrintedDict)

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>


#</DefineClass>