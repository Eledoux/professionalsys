#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SpecificItemizerClass(
								SYS.ItemizerClass
							):
	
	#<DefineHookMethods>
	def initAfterWithSpecificItemizer(self):

		#<DefineSpecificDict>
		self.SpecificItemizedTypeString=""
		#</DefineSpecificDict>

	#</DefineHookMethods>


#</DefineClass>