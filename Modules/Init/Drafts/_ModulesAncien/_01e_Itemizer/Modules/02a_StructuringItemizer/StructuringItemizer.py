#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class StructuringItemizerClass(
						SYS.ItemizerClass
					):

	#<DefineHookMethods>
	def initAfterBasesWithStructuringItemizer(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuringItemizer"]=[
												]

	def callBeforeBasesWithStructuringItemizer(self,*_ArgsList,**_KwargsDict):

		#Check that the _KwargsDict is ok
		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="Structure":

				#print("uu",_KwargsDict['StructuredVariablesList'])

				#Itemize the one that are a list of things
				StructuredVariablesList=map(
						lambda StructuredVariable:
						SYS.UseClass().update(
							{
								'UserPointer':self
							}
							).update(
							{
								'UsingTypeString':"Structurer",
								'DerivingTypeString':"Structurer"
							}
							)+StructuredVariable
						if type(StructuredVariable)==list
						else
						StructuredVariable		
						,_KwargsDict['StructuredVariablesList']
					)

				#Append these things
				map(
					lambda StructuredVariable:
					self(**{
								'ItemizedVariable':StructuredVariable,
								'MethodString':"append",
								'TypeDictString':"Item"
							}
						),
					StructuredVariablesList
					)

	def addBeforeBasesWithStructuringItemizer(self,_AddedVariable):
		
		#Use the Call method
		self(**{	
			'StructuredVariablesList':_AddedVariable,
			'MethodString':"append",
			'TypeDictString':"Structure"
		})

		#Stop the add to avoid the __add__ with the Itemizer method
		self.IsAddingBool=False

	def reprAfterBasesWithStructuringItemizer(self,_SelfString,_PrintedDict):

		#Define the new PrintedDict with SkippedKeyStringsList
		if 'TemporaryItemizedVariable' in _PrintedDict:
			del _PrintedDict['TemporaryItemizedVariable']


		#Filter the UseInstance and print directly the Structure behind
		NewPrintedDict=dict(
								map(
									lambda ItemTuple:
									(ItemTuple[0].split("_")[0]+"_Structure",ItemTuple[1].InstancedVariable) 
									if ItemTuple[0].split("_")[1]=="UseInstance"
									else ItemTuple,
									_PrintedDict.items()
								)
							)

		#Define the new SelfString
		SelfString=SYS.ConsolePrinter.getPrintedVariableString(NewPrintedDict)

		#Return
		#return (SelfString,NewPrintedDict)
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>	

	#<DefineMethods>
	#<DefineMethods>	

#</DefineClass>