#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class AddingItemizerClass(SYS.ItemizerClass):
	
	#<DefineHookMethods>
	def addAfterBasesWithAddingItemizer(self,_AddedVariable):

		#Case of one single element
		if hasattr(_AddedVariable,"items"):

			#Call with the MethodString append
			self(**{
						"ItemizedVariable":_AddedVariable,
						'MethodString':"append",
						'TypeDictString':"Item"
					}
				)

		elif type(_AddedVariable) in [list,tuple]:

			#Convert the Tuple Case
			if type(_AddedVariable)==tuple:
				_AddedVariable=list(_AddedVariable)

			#Call with the MethodString append
			self(**{
						"ItemizedVariables":_AddedVariable,
						'MethodString':"append",
						'TypeDictString':"Item"
					}
				)
	#</DefineHookMethods>


#</DefineClass>