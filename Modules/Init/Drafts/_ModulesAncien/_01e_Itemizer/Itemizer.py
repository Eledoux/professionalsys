#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineLocals>
DoString="Item"
DoingString="Itemizing"
DoneString="Itemizer"
#<DefineLocals>

#<DefineClass>
class ItemizerClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterWithItemizer(self):

		#<DefineSpecificDict>
		self.KeyStringsList=[]
		self.ValueVariablesList=[]
		self.KeyStringToIndexIntDict={}
		self.ItemizedVariable=None
		self.ItemizingInt=0
		self.ItemizingKeyString=""
		#</DefineSpecificDict>

	def callAfterWithItemizer(self,_CallString,*_ArgsList,**_KwargsDict):

		#Check that the _KwargsDict is ok
		if _CallString=="Item":

			#Single Element Case
			if 'ItemizedVariable' in _KwargsDict:

				#Define the ItemizingKeyString
				ItemizingKeyString=_KwargsDict['PrefixString'] if 'PrefixString' in _KwargsDict else ""
				if hasattr(_KwargsDict['ItemizedVariable'],'TypeString'):
					ItemizingKeyString+=_KwargsDict['ItemizedVariable'].TypeString
				ItemizingKeyString+=type(_KwargsDict['ItemizedVariable']).__name__[0].upper()+type(_KwargsDict['ItemizedVariable']).__name__[1:]
				ItemizingKeyString+=_KwargsDict['SuffixString'] if 'SuffixString' in _KwargsDict else ""

				#Bind with ItemizedVariable setting
				self['ItemizedVariable']=_KwargsDict['ItemizedVariable']

				#Split depending on the type of the method called
				if 'MethodString' in _KwargsDict:

					if _KwargsDict['MethodString']=='append':

						#Append in the KeyStringsList and the ValueVariablesList
						self.KeyStringsList.append(ItemizingKeyString)
						self.ValueVariablesList.append(self.ItemizedVariable)

						#Set the corresponding ItemizingInt
						self.ItemizingInt=len(self.KeyStringsList)-1

						#Define the ItemizingKeyString
						self['ItemizingKeyString']=str(self.ItemizingInt)+"_"+ItemizingKeyString

						#Update the KeyStringToIndexIntDict
						self.KeyStringToIndexIntDict[ItemizingKeyString]=self.ItemizingInt

			#List of Element Case
			if 'ItemizedVariablesList' in _KwargsDict:
				if type(_KwargsDict['ItemizedVariablesList'])==list:

					#Get the _KwargsDict without the ItemizedVariables
					SingleKwargsDict=dict(
										filter(
											lambda ItemTuple:
											ItemTuple[0]!='ItemizedVariablesList',
											_KwargsDict.items()
											)
										)
					
					#Do the MethodString for each
					map(
							lambda ItemizedVariable:
							self("Item",**dict(
											SingleKwargsDict,**{
											'ItemizedVariable':ItemizedVariable
											})
							),
							_KwargsDict['ItemizedVariablesList']
						)

		#Return self
		return self

	def addAfterWithItemizer(self,_AddedVariable):

		#Case of one single element
		if hasattr(_AddedVariable,"items"):

			#Call with the MethodString append
			self("Item",**{
						'ItemizedVariable':_AddedVariable,
						'MethodString':"append",
						'TypeDictString':"Item"
					}
				)

		elif type(_AddedVariable) in [list,tuple]:

			#Convert the Tuple Case
			if type(_AddedVariable)==tuple:
				_AddedVariable=list(_AddedVariable)

			#Call with the MethodString append
			self("Item",**{
						'ItemizedVariablesList':_AddedVariable,
						'MethodString':"append",
						'TypeDictString':"Item"
					}
				)


	def iterAfterWithItemizer(self):

		#Iter only on the KeyStrings and the ValueVariables associated in tuple
		return zip(
					map(
							lambda IndexInt,KeyString:
							str(IndexInt)+"_"+KeyString,
							xrange(len(self.KeyStringsList)),
							self.KeyStringsList,

						),
					self.ValueVariablesList
				).__iter__()

	def reprAfterWithItemizer(self,SelfString,PrintedDict):

		#Define the new PrintedDict with SkippedKeyStringsList
		NewPrintedDict=dict(self.iterAfterWithItemizer())

		#Define the new SelfString
		SelfString=SYS.ConsolePrinter.getPrintedVariableString(NewPrintedDict)

		#Return
		return (SelfString,NewPrintedDict)
	
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>	

	#<DefineMethods>
	#<DefineMethods>	

#</DefineClass>