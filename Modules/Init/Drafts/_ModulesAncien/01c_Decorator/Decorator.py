#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class DecoratorClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithDecorator(self):

		#<DefineSpecificDict>
		self.DecoratedFunction=None
		self.BeforeDecoratingFunction=None
		self.AfterDecoratingFunction=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Decorator"]=[
													"DecoratedFunction",
													"BeforeDecoratingFunction",
													"AfterDecoratingFunction"
												]

	def callAfterBasesWithDecorator(self,*_ArgsList,**_KwargsDict):
		
		for FunctionString in ["BeforeDecoratingFunction","DecoratedFunction","AfterDecoratingFunction"]:
			if callable(getattr(self,FunctionString)):
				SYS.callWithFunction(getattr(self,FunctionString),*_ArgsList,**_KwargsDict)
	#</DefineHookMethods>
#</DefineClass>

