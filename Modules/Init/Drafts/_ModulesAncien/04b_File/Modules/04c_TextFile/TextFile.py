#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class TextFileClass(SYS.FileClass):

	#<DefineHookMethods>
	def initAfterBasesWithTextFile(self):

		#<DefineSpecificDict>
		self.TextString=""
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["TextFile"]=[
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setTextStringAfterBasesWithTextFile(self):

		#Open the File
		File=open(self.FilePathString,'w')

		#Overwrite the txt file 
		print("kkk",self.TextString)
		File.write(self.TextString)

		#Close the File
		File.close()
	#</DefineBindingHookMethods>


#</DefineClass>