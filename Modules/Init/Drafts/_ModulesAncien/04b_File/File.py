#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineFunctions>
def getNumberVariableWithString(_String):
	try:
		return int(_String)
	except ValueError:
		try:
			return float(_String)
		except ValueError:
			return str(_String)
#</DefineFunctions>

#<DefineClass>
class FileClass(SYS.DirClass):

	#<DefineHookMethods>
	def initAfterBasesWithFile(self):

		#<DefineSpecificDict>
		self.FilePathString=""
		self.FolderPathString=""
		self.ExtensionString=""
		self.FileString=""
		self.FilePointer=None
		self.ModeString="r"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["File"]=[
												]
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def setFolderPathStringAfterBasesWithFile(self):

		#Bind with FilePathString setting
		self.setFilePathStringWithFile()

	def setFileStringAfterBasesWithFile(self):

		#Bind with FilePathString setting
		self.setFilePathStringWithFile()

	def setExtensionStringAfterBasesWithFile(self):

		#Bind with FilePathString setting
		self.setFilePathStringWithFile()

	def setDirStringAfterBasesWithFile(self):

		#Bind with FolderPathString setting
		if len(self.DirString)>0:
			self['FolderPathString']=self.DirString+"/" if self.DirString[-1]!="/" else self.DirString

	#<DefineBindingHookMethods>	

	#<DefineMethods>
	def setFilePathStringWithFile(self):
		self['FilePathString']=self.FolderPathString+self.FileString+self.ExtensionString

	def getWithFilterDict(self,_FilterDict):

		#Get the ScannedTuplesListsList
		ScannedTuplesListsList=map(
					lambda TupleStringsList:
					map(
							lambda TupleString:
							tuple(
									map(
										lambda IntAndString:
										str(IntAndString[1]) if IntAndString[0]==0
										else getNumberVariableWithString(IntAndString[1]),
										enumerate(
											TupleString.replace("*","")[1:-1].split(',')
											)
										)
								),
							TupleStringsList[1:]
						),
					map(
						lambda KeyString:
						KeyString.split("_"),
						self.get('/').keys()
					)
				)

		#Get the corresponding FilteredScannedTuplesListsList
		FilteredScannedTuplesListsList=SYS.getFilteredScannedTuplesListsListWithFilterDict(
				ScannedTuplesListsList
				,
				_FilterDict
				)

		#Return the SYS.getFilteredScannedTuplesLists
		FilteredKeyStringsList=SYS.getScannedStringsListWithScanningTuplesListsList(
			map(
				lambda FilteredScannedTuplesList:
				map(
						lambda FilteredScannedTuple:
						["*"+FilteredScannedTuple[0],FilteredScannedTuple[1]],
						FilteredScannedTuplesList
					),
				FilteredScannedTuplesListsList
			)
		)
		#print(FilteredKeyStringsList)
		
		#Get the FilteredDataDictsList
		FilteredDataDictsList=map(
			lambda KeyString:
			self.get('/_'+KeyString),
			FilteredKeyStringsList
			)

		#Return the FilteredDataDictsList
		return FilteredDataDictsList

	#<DefineMethods>	

#</DefineClass>