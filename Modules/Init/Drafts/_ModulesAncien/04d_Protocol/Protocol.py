#<ImportModules>
import itertools
import ShareYourSystem as SYS
#</ImportModules>

#<DefineFunctions>
def getProtocoledDictsListWithProtocolDict(_ProtocolDict):

	#Split between ProtocolTuple,ScanTuple
	ProtocolTuplesList,ScanTuplesList=SYS.groupby(
					lambda ItemTuple:
					SYS.getCommonPrefixStringWithStringsList(["<Protocol>",ItemTuple[0]])=="<Protocol>",
					_ProtocolDict.items()
					)

	#Recursive call for ProtocolTuples
	ProtocoledDictsList=map(
							lambda ProtocolTuple:
							(ProtocolTuple[0],getProtocoledDictsListWithProtocolDict(ProtocolTuple[1])),
							ProtocolTuplesList
							)
	#Then Scan the ProtocoledDictsList
	ScannedProtocoledDictsList=SYS.getScannedTuplesListsListWithScannedTuplesListAndNonScannedItemTuplesList(
				map(lambda ItemTuple:
					("<Scan>"+SYS.getPluralStringWithSingularString(ItemTuple[0])+"List",ItemTuple[1]),
					ProtocoledDictsList
					),[]
					)

	#Scan the ScannedTuples
	ScannedTuplesListsList=SYS.getScannedTuplesListsListWithScannedTuplesListAndNonScannedItemTuplesList(ScanTuplesList,[])

	#Product everything together
	return map(
			lambda ProductTuple:
			dict(
				ProductTuple[0]+ProductTuple[1]
				),
				list(
					itertools.product(*[ScannedProtocoledDictsList,ScannedTuplesListsList])
					)
			)
SYS.getProtocoledDictsListWithProtocolDict=getProtocoledDictsListWithProtocolDict
#</DefineFunctions>

#<DefineClass>
class ProtocolClass(SYS.ObjectClass):
	pass
	#<DefineHookMethods>
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>	

	#<DefineMethods>
	#<DefineMethods>	

#</DefineClass>