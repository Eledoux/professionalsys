#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class FamiliarizerClass(SYS.ParentClass,SYS.GrandChildClass):

	#<DefineHookMethods>
	def setBeforeBasesWithFamiliarizer(self,_KeyVariable,_ValueVariable):

		#If it is a Younger CuriousChild then set 'ParentPointersList'
		if SYS.getCommonSuffixStringWithStringsList([_KeyVariable,"Pointer"])!="Pointer":
			if hasattr(self,"ParentPointersList"):
				if SYS.getIsPythonTypeStringBool(_KeyVariable):
					if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("GrandChild",_ValueVariable):
						_ValueVariable['ParentPointersList']=self.ParentPointersList+[self]

		#Return the modified SettedItem
		return (_KeyVariable,_ValueVariable)
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setParentPointersListsAfterBasesWithFamiliarizer(self):
		
		#Reset all the CuriousChildren ParentPointersList (COULD BE ASYNCHRONOUS)
		map(lambda Item:self.setBeforeBasesWithFamiliarizer(Item[0],Item[1]) if SYS.getIsPythonTypeStringBool(KeyVariable[0]) else None,self.items())

	#</DefineBindingHookMethods>
	
#</DefineClass>
