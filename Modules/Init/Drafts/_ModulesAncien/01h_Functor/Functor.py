#<ImportModules>
import sys
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Function"
DoingString="Functing"
DoneString="Functed"
#<DefineClass>

#<DefineClass>
class FunctorClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterWithFunctor(self):

		#<DefineSpecificDict>
		self.FunctionFunctingPointer=None
		self.FunctingList=[]
		self.FunctingDict={}
		#</DefineSpecificDict>
											
	def callAfterWithFunctor(self,_CallString,*_ArgsList,**_KwargsDict):

		if _CallString==DoString:

			#Define a default OutputVariable
			OutputVariable=None
			#Call the FunctionPointer
			if callable(self.FunctionFunctingPointer):

				if SYS.getIsKwargsFunctionBool(self.FunctionFunctingPointer):

					OutputVariable=self.FunctionFunctingPointer(*self.FunctingList,**self.FunctingDict)
				else:
					OutputVariable=self.FunctionFunctingPointer(*self.FunctingList)

			#Return the OutputVariable
			return OutputVariable

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>


#</DefineClass>