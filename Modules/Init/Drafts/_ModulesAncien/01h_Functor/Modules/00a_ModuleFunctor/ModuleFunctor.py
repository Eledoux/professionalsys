#<ImportModules>
import importlib
import sys
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ModuleFunctorClass(SYS.FunctorClass):

	#<DefineHookMethods>
	def initAfterWithModuleFunctor(self):

		#<DefineSpecificDict>
		self.ModuleFunctingString=""
		self.FunctionFunctingString=""
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

	def setAfterWithModuleFunctor(self,_SettingVariable,_SettedVariable):

		#Special Module String setting
		if _SettingVariable=="ModuleString":
			if _SettedVariable not in sys.modules:
				try:
					importlib.import_module(_SettedVariable)
				except:
					self.IsSettingBool=False

		elif _SettingVariable=="FunctionString":
			if self.ModuleString!="":
				Module=sys.modules[self.ModuleString]
				if hasattr(Module,_SettedVariable)==False:
					if callable(getattr(Module,_SettedVariable))==False:
						self.IsSettingBool=False
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindFunctionFunctingStringAfterWithModuleFunctor(self):

		#Bind with FunctionPointer setting
		self.setFunctionFunctingPointerWithModuleFunctor()

	def bindModuleFunctingStringAfterWithModuleFunctor(self):

		#Bind with FunctionPointer setting
		self.setFunctionFunctingPointerWithModuleFunctor()
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setFunctionFunctingPointerWithModuleFunctor(self):

		if self.ModuleFunctingString!="" and self.FunctionFunctingString!="":
			self['FunctionFunctingPointer']=getattr(sys.modules[self.ModuleFunctingString],self.FunctionFunctingString)

	#</DefineMethods>

#</DefineClass>