#<ImportModules>
import ShareYourSystem as SYS
import time
#</ImportModules>

#<DefineClass>
class CommitterClass(SYS.UserClass):

	#<DefineHookMethods>
	def initAfterBasesWithCommitter(self):

		#<DefineSpecificDict>
		self.CommittedString=""
		#</DefineSpecificDict>

		#Use an Identifier
		self(**{'TypeDictString':"Use",'UsedTypeString':"Identifier",'UsingTypeString':"Committer"})

		#Use a Hdfiler
		self(**{'TypeDictString':"Use",'UsedTypeString':"Hdfiler",'UsingTypeString':"Committer"})


	def callAfterBasesWithCommitter(self,*_ArgsList,**_KwargsDict):

		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="Commit":

				#Short alias setting
				Hdfiler=self['<Used>OfCommitter_Hdfiler']
				Identifier=self['<Used>OfCommitter_Identifier']

				#Reupdate possible attributes
				if "HdfiledDict" in _KwargsDict:
					Hdfiler['HdfiledDict']=_KwargsDict['HdfiledDict']

				if "IdentifiedString" in _KwargsDict:
					Identifier['IdentifiedString']=_KwargsDict['IdentifiedString']

				#Update the CommittedString with the actual time
				self['CommittedString']=self['<Used>OfCommitter_Identifier'].IdentifiedString+" "+str(time.ctime(time.time()))

				#Hdfile this with the CommittedString as the HdfilingString
				Hdfiler(**{
					'TypeDictString':"Hdfile",
					'HdfilingString':self.CommittedString,
					'MethodString':"push"
				}
	)



	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
