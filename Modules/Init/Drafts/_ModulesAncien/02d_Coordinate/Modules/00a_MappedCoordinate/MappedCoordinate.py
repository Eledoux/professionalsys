#<ImportModules>
import operator
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class MappedCoordinateClass(SYS.CoordinateClass):

	#<DefineHookMethods>
	def initAfterBasesWithMappedCoordinate(self):

		#<DefineSpecificDict>
		self.MapperTypeString="Mapper"
		self.CoordinatingMapper=SYS.MapperClass()
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
									]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["MappedCoordinate"]=[
													"MapperTypeString"
													"CoordinatingMapper"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setCoordinatedIntsListAfterBasesWithMappedCoordinate(self):

		#Bind with CoordinatingMapper_VariablesList setting
		self.setCoordinatingMapper_VariablesListWithMappedCoordinate()

	def setMapperTypeStringAfterBasesWithMappedCoordinate(self):

		#Bind with CoordinatingMapper setting
		if hasattr(SYS,self.MapperTypeString):
			MapperClass=getattr(SYS,self.MapperTypeString)
			if type(MapperClass).__name__=="classobj":
				self['CoordinatingMapper']=getattr(SYS,self.MapperTypeString)()
	#<DefineBindingHookMethods>

	#<DefineMethods>
	def setCoordinatingMapper_VariablesListWithMappedCoordinate(self):

		print("bbb")
		#Bind with CoordinatingMapper ListedVariables setting
		self.CoordinatingMapper[self.CoordinatingMapper.ListKeyString]=self.CoordinatedIntsList 

	#<DefineMethods>

#</DefineClass>