#<ImportModules>
import operator
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class CoordinateClass(SYS.GrandChildClass,SYS.NamedObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithCoordinate(self):

		#<DefineSpecificDict>
		self.CoordinatedIntsList=[]
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Coordinate"]=[
													"CoordinatedIntsList"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setGrandParentPointersListAfterBasesWithCoordinate(self):

		#Bind with CoordinatedIntsList setting
		self.setCoordinatedIntsListWithCoordinate()
	
	def setNamedVariableAfterBasesWithCoordinate(self):

		#Bind with CoordinatedIntsList setting
		self.setCoordinatedIntsListWithCoordinate()	

	#<DefineBindingHookMethods>

	#<DefineMethods>
	def setCoordinatedIntsListWithCoordinate(self):

		if len(self.GrandParentPointersList)>0:

			#Get the List of NamedVariable
			NamedVariablesList=[self.NamedVariable]+map(lambda GrandParentPointer:GrandParentPointer['NamedVariable'],self.GrandParentPointersList[:-1])
			
			#Find the Longest Integer Chain
			IsIntBoolsList=map(lambda CoordinateVariable:type(CoordinateVariable)==int,NamedVariablesList)
			if False in IsIntBoolsList:
				FalseInt=IsIntBoolsList.index(False)
				NamedVariablesList=NamedVariablesList[:FalseInt]
			else:
				NamedVariablesList=NamedVariablesList

			#Bind with CoordinateVariablesList Setting
			self['CoordinatedIntsList']=NamedVariablesList

	#</DefineMethods>

#</DefineClass>