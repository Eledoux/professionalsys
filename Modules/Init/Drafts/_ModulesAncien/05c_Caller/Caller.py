#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SorterClass(
					SYS.UserClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithSorter(self):

		#<DefineSpecificDict>
		self.SortingUser+={'UsedTypeString':"Itemizer"}
		self.SortingUser+={'UsedTypeString':"Filter"}
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Sorter"]=[
												]

	def callAfterBasesWithSorter(self,*_ArgsList,**_KwargsDict):

		#Check that the _KwargsDict is ok
		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="Sort":
				if 'SortedVariablesList' in _KwargsDict:
					
					#Call first the Filter
					FilteredVariablesList=self['<Used>OfSorter_Filter'](**{
									'TypeDictString':"Filter",
									'FilteredVariablesList':_KwargsDict['SortedVariablesList']
										}
									)

					#Then call the Itemizer
					self['<Used>OfSorter_Itemizer'](**{
									'MethodString':"append",
									'TypeDictString':"Item",
									'ItemizedVariablesList':FilteredVariablesList
										}
									)

	def reprAfterBasesWithSorter(self,SelfString,PrintedDict):

		#Define the new PrintedDict with SkippedKeyStringsList
		NewPrintedDict=self['<Used>OfSorter_Itemizer']

		#Define the new SelfString
		SelfString=SYS.ConsolePrinter.getPrintedVariableString(NewPrintedDict)

		#Return
		return (SelfString,NewPrintedDict)
		
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>	

	#<DefineMethods>
	#<DefineMethods>	

#</DefineClass>