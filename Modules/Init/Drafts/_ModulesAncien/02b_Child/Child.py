#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ChildClass(SYS.ObjectClass):

	#<Define HookMethods>
	def initAfterBasesWithChild(self):

		#<DefineSpecificDict>
		self.ParentPointer=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Child"]=[
													"ParentPointer",
												]

	#</Define HookMethods>