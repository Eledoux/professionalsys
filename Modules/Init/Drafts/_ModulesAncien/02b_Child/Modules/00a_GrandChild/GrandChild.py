#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class GrandChildClass(SYS.ChildClass):

	#<DefineHookMethods>
	def initAfterBasesWithGrandChild(self):
		
		#<DefineSpecificDict>
		self.GrandParentPointersList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["GrandChild"]=[
													"GrandParentPointersList"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setParentPointerAfterBasesWithGrandChild(self):
		IsAlreadySettedGrandParentPointersList=False
		if hasattr(self,"GrandParentPointersList"):
			if len(self.GrandParentPointersList)>0:
				if hasattr(self,"ParentPointer"):
					if self.GrandParentPointersList[0]==self.ParentPointer:
						IsAlreadySettedGrandParentPointersList=True

		if IsAlreadySettedGrandParentPointersList==False:

			IsParentPointerBool=True
			GrandParent=self
			self['GrandParentPointersList']=[]

			while IsParentPointerBool:
				IsParentPointerBool=False
				if hasattr(GrandParent,"ParentPointer"):
					if GrandParent.ParentPointer!=None:
						self['GrandParentPointersList']+=[GrandParent.ParentPointer]
						IsParentPointerBool=True
						GrandParent=GrandParent.ParentPointer

	def setGrandParentPointersList(self):
		'''
			<Help>
				Bind the ParentPointer with the setGrandParentPointersList
				by looking if Parent has a Parent, a GrandParent...and so on
			</Help>

			<Test>
				#Load SYS module
				import ShareYourSystem as SYS
				#
				#Define a Structure of Parented Objects
				GrandParent=SYS.ParentClass().update({"1Object":SYS.FamilyParentClass().update({"2Object":SYS.GrandChildClass()})});
				#
				#Print the structure
				print("GrandParent, Parent, Child know each other like this :");
				print(GrandParent);
			</Test>

		'''

		IsAlreadySettedGrandParentPointersList=False
		if hasattr(self,"GrandParentPointersList"):
			if len(self.GrandParentPointersList)>0:
				if hasattr(self,"ParentPointer"):
					if self.GrandParentPointersList[0]==self.ParentPointer:
						IsAlreadySettedGrandParentPointersList=True

		if IsAlreadySettedGrandParentPointersList==False:

			IsParentPointerBool=True
			GrandParent=self
			self['GrandParentPointersList']=[]

			while IsParentPointerBool:
				IsParentPointerBool=False
				if hasattr(GrandParent,"ParentPointer"):
					if GrandParent.ParentPointer!=None:
						self['GrandParentPointersList']+=[GrandParent.ParentPointer]
						IsParentPointerBool=True
						GrandParent=GrandParent.ParentPointer
	#</DefineBindingHookMethods>

#</DefineClass>