#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DictatedObjectGrandChildClass(
								SYS.GrandChildClass,
								SYS.ObjectClass
							):

	#<DefineHookMethods>
	def initAfterBasesWithDictatedObjectGrandChild(self):

		#<DefineSpecificDict>
		self.ParentDictionnaryPointer=None
		self.ParentDicterPointer=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"ParentDictionnaryPointer",
													"ParentDicterPointer"
												]

	#</DefineHookMethods>

	#<DefineBindingMethods>
	def setGrandParentPointersListAfterBasesWithDictatedObjectGrandChild(self):

		if len(self.GrandParentPointersList)>1:

			#Bind with ParentDictionnary setting
			if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Dictionnary",self.GrandParentPointersList[0]):	
				self['ParentDictionnaryPointer']=self.GrandParentPointersList[0]

			#Bind with ParentDicter setting
			if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Dicter",self.GrandParentPointersList[1]):
				self['ParentDicterPointer']=self.GrandParentPointersList[1]
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

