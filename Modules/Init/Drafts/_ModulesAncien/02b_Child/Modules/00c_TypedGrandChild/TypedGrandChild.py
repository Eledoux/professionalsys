#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class TypedGrandChildClass(SYS.GrandChildClass):

	#<DefineHookMethods>
	def initAfterBasesWithCuriousChild(self):
		
		#<DefineSpecificDict>
		self.GrandParentTypeStringsList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["TypedGrandChild"]=[
													"GrandParentTypeStringsList"
												]

	def getBeforeBasesWithTypedGrandChild(self,_KeyVariable):
		if hasattr(self,"ParentTypeStringsList"):
			if _KeyVariable in self.ParentTypeStringsList:
				return self.ParentPointersList[self.ParentTypeStringsList.index(_KeyVariable)]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>

	def setGrandParentPointersListAfterBasesWithGrandChild(self):

		#Bind with GrandParentTypeStringsList setting
		self['GrandParentTypeStringsList']=map(lambda Pointer:SYS.getTypeStringWithClassString(Pointer.TypeString),self.ParentPointersList)
	#</DefineBindingHookMethods>

#</DefineClass>