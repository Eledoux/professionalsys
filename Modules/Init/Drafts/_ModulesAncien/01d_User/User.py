#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Use"
DoingString="Using"
DoneString="Used"
DoingKeyString="<Use>"
DoneTagString="<Used>"
#<DefineClass>

class UserClass(
					SYS.PatherClass
				):

	#<DefineHookMethods>
	def initAfterWithUser(self):

		#<DefineSpecificDict>
		self.UsingTyper=SYS.TyperClass()
		self.UsingInstancer=SYS.InstancerClass()
		self.UsingTypeString=self.TypeString
		self.UsedVariable=None
		#</DefineSpecificDict>

	def callAfterWithUser(self,_CallString,*_ArgsList,**_KwargsDict):

		if _CallString=="Use":

			#Check if it is a binding of an already existing Instance 
			if 'UsedVariable' not in _KwargsDict:

				if 'UsedTypeString' not in _KwargsDict:

					#Define the InstancingTypeString
					InstancingTypeString="Of"+self.UsingTypeString+"_"+self.UsingInstancer.InstancingTypeString

					#Instancify
					self.UsingInstancer("Instance",*[('InstancingTypeString',InstancingTypeString)])

					#Set the UserPointer to the InstancedVariable
					self.UsingInstancer.InstancedVariable['Using'+self.UsingTypeString+'Pointer']=self

					#Set an alis
					self.UsedVariable=self.UsingInstancer.InstancedVariable

				else:

					#Shortcut call
					self("Use",*[
								('<Pathed>UsingInstancer',{
													'InstancingDict':_KwargsDict['UsedDict'] if 'UsedDict' in _KwargsDict else {},
													'InstancingTypeString':_KwargsDict['UsedTypeString']
												})
							]
					)

		#Return self
		return self

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	

	#</DefineMethods>

#</DefineClass>
