#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

UseKeyString="<Use>"
UsedKeyString="<Used>"

#<DefineClass>
class UserClass(
					SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithUser(self):

		#<DefineSpecificDict>
		self.IdStringToUsedKeyStringDict={}
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["User"]=[
												]

	def getAfterBasesWithUser(self,_KeyVariable):

		#Case of a Use getting
		if UsedKeyString in _KeyVariable:
			if UsedKeyString==_KeyVariable[:len(UsedKeyString)]:

				#Get the TypeString
				TypeString=_KeyVariable.split(UsedKeyString)[1]

				#Find corresponding Uses 
				GettedKeyStringsList=filter(
											lambda KeyString:
											"Use" in KeyString and TypeString in KeyString,
											self.__dict__.keys()
											)


				if len(GettedKeyStringsList)==1:

					#Get the Use 
					Use=getattr(self,GettedKeyStringsList[0])

					#Check if it is ok
					if hasattr(Use,"InstancedVariable"):
						return Use.InstancedVariable


	def callAfterBasesWithUser(self,*_ArgsList,**_KwargsDict):

		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="Use":

				if "UsedVariable" not in _KwargsDict:

					#Define the KeyString
					KeyString="Of"+(
									_KwargsDict['UsingTypeString'] if 'UsingTypeString' in _KwargsDict else self.TypeString
									)+"_"+(
									_KwargsDict['PrefixString'] if 'PrefixString' in _KwargsDict else ""
									)+_KwargsDict['UsedTypeString']+"Use"
									
					#Set at the KeyString
					setattr(self,KeyString,SYS.UseClass().update(
								{
									'DerivingTypeString':_KwargsDict['UsedTypeString'],
									'UsingTypeString':_KwargsDict['UsingTypeString'] if 'UsingTypeString' in _KwargsDict else self.TypeString,
									'UserPointer':self,
									'InstancingDict':_KwargsDict['InstancingDict'] if 'InstancingDict' in _KwargsDict else {}
								}
								)
							)

				else:
					pass
				
				#Update in the IdIntDictToUsedKeyString
				self.IdStringToUsedKeyStringDict[str(id(getattr(self,KeyString).InstancedVariable))]=_KwargsDict['UsedTypeString']

		#Return self
		return self

	def addAfterBasesWithUser(self,_AddedVariable):

		if hasattr(_AddedVariable,"items"):
			self(**dict(
					{
						'TypeDictString':"Use"
					},
					**_AddedVariable
					)
				)
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	

	#</DefineMethods>

#</DefineClass>
