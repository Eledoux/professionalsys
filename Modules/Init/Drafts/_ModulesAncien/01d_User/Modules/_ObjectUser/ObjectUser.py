#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ObjectUserClass(
								SYS.TypeStringDeriverClass,
								SYS.VariableInstancerClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithObjectUser(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["User"]=[
													"UsedObject"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setInstancedVariableAfterBasesWithObjectUser(self):

		#Bind with UsedObject setting
		self['UsedObject']=self.InstancedVariable
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
