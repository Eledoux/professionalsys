#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class OfStructurer_StructurerClass(
						SYS.UsedObjectClass,
						SYS.StructurerClass
					):
	
	#<DefineHookMethods>
	def initAfterBasesWithOfStructurer_Structurer(self):

		#<DefineSpecificDict>
		self.ParentStructurerPointersList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["OfStructurer_Structurer"]=[
												]

	def reprAfterBasesWithOfStructurer_Structurer(self,_SelfString,_PrintedDict):

		#Filter the UseInstance and print directly the Structure behind
		NewPrintedDict=_PrintedDict
		NewPrintedDict.update({'UsingStructurerPointer':self.UsingStructurerPointer})

		#Define the new SelfString
		SelfString=SYS.ConsolePrinter.getPrintedVariableString(NewPrintedDict)

		#Return
		return (SelfString,_PrintedDict)

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def setUsingStructurerPointerAfterBasesWithOfStructurer_Structurer(self):

		#Bind with ParentStructurerPointersList setting
		self.setParentStructurerPointersListWithOfStructurer_Structurer()

	def setParentStructurerPointersListAfterBasesWithOfStructurer_Structurer(self):

		#Iter and set ParentStructurerPointersList in the Children
		map(	
				lambda StructuredVariable:
				StructuredVariable.InstancedVariable.__setitem__("ParentStructurerPointersList",self.ParentStructurerPointersList+[self])
				if hasattr(StructuredVariable,"InstancedVariable") and hasattr(StructuredVariable.InstancedVariable,"ParentStructurerPointersList")
				else 
					StructuredVariable.__setitem__("ParentStructurerPointersList",self.ParentStructurerPointersList+[self])
					if hasattr(StructuredVariable,"ParentStructurerPointersList")
					else None,
				self.ValueVariablesList
			)

		#Call the Oldest Parent to say that it is done
		map(
				lambda ParentStructurerPointer:
				ParentStructurerPointer['<Used>OfStructurer_Itemizer'](**{
																			'TypeDictString':"Item",
																			'MethodString':"append",
																			'ItemizedVariable':self,
																			'SuffixString':"Pointer"
																		}),
				self.ParentStructurerPointersList
			)

	#<DefineBindingHookMethods>	

	#<DefineMethods>
	def setParentStructurerPointersListWithOfStructurer_Structurer(self):

		#Init with the self.UsingStructurerPointer
		ParentStructurerPointersList=[self.UsingStructurerPointer]

		#Then accumulate Parent possibly
		if hasattr(self.UsingStructurerPointer,"ParentStructurerPointersList"):
			ParentStructurerPointersList+=self.UsingStructurerPointer.ParentStructurerPointersList

		#Set 
		self['ParentStructurerPointersList']=ParentStructurerPointersList
	#<DefineMethods>	

#</DefineClass>