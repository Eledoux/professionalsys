#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class StructurerClass(
						SYS.UserClass,
						SYS.ItemizerClass
					):

	#<DefineHookMethods>
	def initAfterBasesWithStructurer(self):

		#<DefineSpecificDict>
		self(**{
				'TypeDictString':"Use",
				'UsingTypeString':"Structurer",
				'UsedTypeString':"Itemizer"
			})
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Structurer"]=[
												]

	def callBeforeBasesWithStructurer(self,*_ArgsList,**_KwargsDict):

		#Check that the _KwargsDict is ok
		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="Structure":

				#Append self in the StructurerPointersList of the StructuredObjects 
				map(
						lambda StructuredVariable:
						StructuredVariable.__setitem__("ParentStructurerPointersList",StructuredVariable.ParentStructurerPointersList+[self]) 
						if hasattr(StructuredVariable,'ParentStructurerPointersList')
						else None,
						_KwargsDict['StructuredVariablesList']
					)

				#Usify the ones that are a list of things
				StructuredVariablesList=map(
						lambda StructuredVariable:
						SYS.UseClass().update(
							{
								'UserPointer':self
							}
							).update(
							{
								'UsingTypeString':"Structurer",
								'DerivingTypeString':"Structurer"
							}
							)+StructuredVariable
						if type(StructuredVariable)==list
						else
						StructuredVariable		
						,_KwargsDict['StructuredVariablesList']
					)
			

				#Append self in the StructurerPointersList of the StructuredObjects
				map(
						lambda StructuredVariable:
						StructuredVariable.InstancedVariable.__setitem__('UsingStructurerPointer',self) 
						if hasattr(StructuredVariable,"InstancedVariable") and hasattr(StructuredVariable.InstancedVariable,'UsingStructurerPointer')
						else None,
						StructuredVariablesList
					)

				#Append these things
				map(
					lambda StructuredVariable:
					self(**{
								'ItemizedVariable':StructuredVariable,
								'MethodString':"append",
								'TypeDictString':"Item"
							}
						),
					StructuredVariablesList
					)

	def addBeforeBasesWithStructurer(self,_AddedVariable):
		
		#Use the Call method
		self(**{	
			'StructuredVariablesList':_AddedVariable,
			'MethodString':"append",
			'TypeDictString':"Structure"
		})

		#Stop the add to avoid the __add__ with the Itemizer method
		self.IsAddingBool=False

	def reprAfterBasesWithStructurer(self,_SelfString,_PrintedDict):

		#Define the new PrintedDict with SkippedKeyStringsList
		if 'TemporaryItemizedVariable' in _PrintedDict:
			del _PrintedDict['TemporaryItemizedVariable']


		#Filter the UseInstance and print directly the Structure behind
		NewPrintedDict=dict(
								map(
									lambda ItemTuple:
									(ItemTuple[0].split("_")[0]+"_Structure",ItemTuple[1].InstancedVariable) 
									if ItemTuple[0].split("_")[1]=="UseInstance"
									else ItemTuple,
									_PrintedDict.items()
								),
								**dict(
										filter(
												lambda Variable:
												Variable!=None,
												map(
														lambda KeyString:
														(KeyString,getattr(self,KeyString)) if hasattr(self,KeyString) else None,
														[
															"ParentStructurerPointersList",
															"OfStructurer_ItemizerUse"
														]
													)
											)
									)
							)


		#Define the new SelfString
		SelfString=SYS.ConsolePrinter.getPrintedVariableString(NewPrintedDict)

		#Return
		return (SelfString,NewPrintedDict)
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>	

	#<DefineMethods>
	#<DefineMethods>	

#</DefineClass>