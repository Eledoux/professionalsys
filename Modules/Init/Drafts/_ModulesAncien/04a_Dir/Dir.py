#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class DirClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithDir(self):

		#<DefineSpecificDict>
		self.DirString=""
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Dir"]=[
												]
	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>	

	#<DefineMethods>
	#<DefineMethods>	

#</DefineClass>