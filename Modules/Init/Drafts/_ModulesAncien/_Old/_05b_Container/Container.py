#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ContainerClass(
						#SYS.StructuresDicterClass
					):

	#<DefineHookMethods>
	def initAfterBasesWithContainer(self):
		
		#<DefineSpecificDict>
		self.ContainedTypeStringsList=[]
		#</DefineSpecificDict>

		#self['InstancedDictatedBaseTypeString']="SuperStructure"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Container"]=[	
											"ContainedTypeStringsList"
										]	
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setContainedTypeStringsListAfterBasesWithContainer(self):
		
		#Add Structures of the ContainedTypeStings
		map(lambda ContainedTypeString:
			self.__setitem__(
				"_"+SYS.getPluralStringWithSingularString(ContainedTypeString)+self.InstancedDictatedBaseTypeString,
				{}),self.ContainedTypeStringsList
			)
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

