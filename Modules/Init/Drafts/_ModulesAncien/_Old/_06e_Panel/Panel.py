#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class PanelClass(SYS.GridClass,SYS.PatchClass):

	#<DefineHookMethods>
	def initAfterBasesWithPanel(self):
		
		#<DefineSpecificDict>
		self.FigurePointer=None
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
										"CoordinateVariablesList",
										"InteractionsTupler",
									]

		#This is a Grid of Plots
		self['CoupledAgentTypeString']="Plot"
		
		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
								"FigurePointer"
							]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setParentPointersListAfterBasesWithPanel(self):

		#Bind With the FigurePointer Setting
		TypeString="Figure"
		IsPointerBoolsList=map(lambda Pointer:SYS.getIsTypedVariableBoolWithTypeStringAndVariable(TypeString,Pointer),self.ParentPointersList)
		if any(IsPointerBoolsList):
			self[TypeString+"Pointer"]=self.ParentPointersList[IsPointerBoolsList.index(True)]
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

