#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class GridClass(SYS.NetworkClass):

	#<DefineHookMethods>
	def initAfterBasesWithGrid(self):
		#<DefineSpecificDict>
		self['CoupledArrayTypeString']="Matrix"
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
									"NetworkedTableKeyString"
									]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
									"NodesMatrix",
												]								
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
#</DefineClass>

