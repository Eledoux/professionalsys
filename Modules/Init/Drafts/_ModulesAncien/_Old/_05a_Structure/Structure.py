#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StructureClass(
						SYS.ListedObjectClass,
						SYS.StructuratingStructuresPointeringObjectClass,
						SYS.InstancingListerClass,
						SYS.AddingListerClass
					):

	#<DefineHookMethods>
	def initAfterBasesWithStructure(self):
		
		#<DefineSpecificDict>
		self.StructuratingListersTypeString="StructuratingLister"
		self.StructuratingStructuresTypeString=self.TypeString
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[	
												"StructuratingListersTypeString",
												"StructuratingStructuresTypeString",
												"StructuratingStructurePointersList"
										]	
						
	def addBeforeBasesWithStructure(self,_AddedVariable):


		#Special add in the database : add in new subTable or Structure 
		if type(_AddedVariable)==list:

			#Look if it is a List of List
			IsListBoolsList=map(lambda ListedVariable:type(ListedVariable)==list,_AddedVariable)
			IsListsListBoolsList=map(lambda ListedVariable:SYS.getIsListsListBool(ListedVariable),_AddedVariable)
			
			#Set the InstancedListedBaseTypeStringsList
			InstancedListedBaseTypeStringsList=map(lambda ListBool,ListsListBool:
				self.StructuratingStructuresTypeString if ListsListBool else (self.StructuratingListersTypeString if ListBool else "None"),
				IsListBoolsList,
				IsListsListBoolsList)
			self['InstancedListedBaseTypeStringsList']=InstancedListedBaseTypeStringsList

			#Prepare the good number of ListedVariables
			if self.ListedVariablesInt<len(_AddedVariable):
				self['ListedVariablesInt']=len(_AddedVariable)
			
			#Set in the "None" slots of the list the direct values
			map(lambda IndexInt,InstancedListedBaseTypeString,ListedVariable:
				getattr(self,self.ListKeyString).__setitem__(IndexInt,ListedVariable) 
				if InstancedListedBaseTypeString=="None" 
				else None,
				xrange(len(_AddedVariable)),
				self.InstancedListedBaseTypeStringsList,
				_AddedVariable)

			#Add a Pointer on self in the StructurePointersList of each Child Structures, StructuratingLister
			map(lambda Lister,ListedAddedVariable:
				Lister.__setitem__(
									"StructuratingStructurePointersList",
					Lister.StructuratingStructurePointersList+[self]+self.StructuratingStructurePointersList
									) 
				if hasattr(Lister,"StructuratingStructurePointersList") else None,
				getattr(self,self.ListKeyString),_AddedVariable)
			
			#Add into each Child Structures, StructuratingLister
			map(lambda Lister,ListedAddedVariable:
				Lister.__add__(ListedAddedVariable) if hasattr(Lister,"__add__") else None,
				getattr(self,self.ListKeyString),_AddedVariable)
	
			#return an empty _AddedVariable
			return []
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

