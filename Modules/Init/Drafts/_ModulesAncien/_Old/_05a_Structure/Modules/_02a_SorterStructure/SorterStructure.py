#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SorterStructureClass(
								SYS.FamiliarizingStructuratingListersStructureClass,
								SYS.TypesSorterClass,
								SYS.NamingListerClass
								):
	
	#<DefineHookMethods>
	def initAfterBasesWithSorterStructure(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the SortedTypeStringsList
		self['SortedTypeStringsList']=["StructuratingLister"]

		#Update the TypeString of the Listers and the Structures
		self.ListerTypeString="StructuratingLister"

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

