#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SortingStructureClass(
								SYS.SortedStructuratingObjectClass,
								SYS.StructureClass
							):
	
	#<DefineHookMethods>
	def initAfterBasesWithSortingStructure(self):

		#<DefineSpecificDict>
		self.StructuratingTypesSorter=SYS.StructuratingTypesSorterClass().update(
			{'SortingStructurePointer':self}
			).update(
			{'SortedTypeStringsList':["SortingStructure","SortedStructuratingLister","SortedStructuredObject"]}
			)
		#</DefineSpecificDict>

		#Update the StructuratingStructuresTypeString
		self['StructuratingStructuresTypeString']="SortingStructure"

		#Update the StructuratingListersTypeString
		self['StructuratingListersTypeString']="SortedStructuratingLister"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SortingStructure"]=[
												"StructuratingTypesSorter"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

