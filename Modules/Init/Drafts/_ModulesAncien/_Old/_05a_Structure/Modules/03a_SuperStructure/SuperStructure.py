#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SuperStructureClass(
								SYS.SortingStructureClass,
								SYS.SpecificStructureClass
							):

	#<DefineHookMethods>
	def initAfterBasesWithSuperStructure(self):
		
		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SpecificStructure"]=[	
											"SpecificInstancedStructuredBaseTypeString",
											"StructuredIntsListsList"
										]	

		#Update the StructuratingStructuresTypeString 
		self['StructuratingStructuresTypeString']="SuperStructure"

		#Update the StructuratingListersTypeString 
		self['StructuratingListersTypeString']="SuperStructuratingLister"

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

