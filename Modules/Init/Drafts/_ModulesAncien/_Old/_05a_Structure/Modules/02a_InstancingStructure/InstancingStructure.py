#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancingStructureClass(
								SYS.StructureClass
								):

	#<DefineHookMethods>
	def initAfterBasesWithInstancingStructure(self):
		
		#<DefineSpecificDict>
		self.InstancedStructuredBaseTypeStringsList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingStructure"]=[	
											"InstancedStructuredBaseTypeStringsList"
										]	

		#Update the StructuratingStructuresTypeString
		self['StructuratingStructuresTypeString']="InstancingStructure"

		#Update the StructuratingListersTypeString
		self['StructuratingListersTypeString']="InstancingStructuratingLister"
		
	"""	
	def setBeforeBasesWithInstancingStructure(self,_KeyVariable,_ValueVariable):

		#Case of the ListedVariables setting
		if _KeyVariable==self.ListKeyString:
			
			#Set InstancedListedBaseTypeString in each InstancingStructuratingLister
			#
			map(lambda ListedVariable:
				ListedVariable.__setitem__(
					"InstancedListedBaseTypeStringsList",
					self.InstancedStructuredBaseTypeStringsList
					) if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("InstancingStructuratingLister",
					ListedVariable) else
				ListedVariable.__setitem__(
					"InstancedStructuredBaseTypeStringsList",
					self.InstancedStructuredBaseTypeStringsList
					) if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("InstancingStructure",
					ListedVariable)
					else None,
				_ValueVariable
				)

			#Return this
			return (_KeyVariable,_ValueVariable)
	"""
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	"""
	def setInstancedStructuredBaseTypeStringsListAfterBasesWithInstancingStructure(self):

		map(lambda ListedVariable:ListedVariable.__setitem__(
					"InstancedListedBaseTypeStringsList",
					self.InstancedStructuredBaseTypeStringsList
					) if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("InstancingStructuratingLister",
					ListedVariable),
					getattr(self,self.ListKeyString))
	"""
	def setStructuratingStructurePointersListAfterBasesWithInstancingStructure(self):

		#Bind with InstancedStructuredBaseTypeStringsList of the first StructuratingStructurePointer
		if len(self.StructuratingStructurePointersList)>0:
			if hasattr(self.StructuratingStructurePointersList[0],"InstancedStructuredBaseTypeStringsList"):
				self['InstancedStructuredBaseTypeStringsList']=self.StructuratingStructurePointersList[0].InstancedStructuredBaseTypeStringsList
	
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	"""
	def setInstancedListedBaseTypeStringInEachInstancingStructuratingLister(self):

		map(lambda ListedVariable:
				ListedVariable.__setitem__(
					"InstancedListedBaseTypeStringsList",
					self.InstancedStructuredBaseTypeStringsList
					) if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("InstancingStructuratingLister",
					ListedVariable)
			)
	"""
	#</DefineMethods>

#</DefineClass>

