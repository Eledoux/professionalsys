#<Import Modules>
import multiprocessing
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class StructuratingListersTypingStructureClass(
									SYS.StructureClass
									):

	#<DefineHookMethods>
	def initAfterBasesWithStructuratingListersTypingStructure(self):

		#<DefineSpecificDict>
		self.StructuratingListersTypingStructurePointeringTyper=SYS.StructuratingListersTypingStructurePointeringTyperClass().update(
			{'StructuratingListersTypingStructurePointer':self}
			)
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuratingListersTypingStructure"]=[
											"StructuratingListersTypingStructurePointeringTyper"
												]
	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

#</DefineClass>