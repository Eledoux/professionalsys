#<Import Modules>
import multiprocessing
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class FamiliarizingStructuratingListersStructureClass(
									SYS.StructureClass,
									SYS.FamiliarizerClass,
									SYS.ParentListerClass
									):

	#<DefineHookMethods>
	def initAfterBasesWithFamiliarizingStructuratingListersStructure(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the structurating TypeStrings
		self.StructuratingListerTypeString="FamiliarizerAddingLister"
		self.StructureTypeString="FamiliarizingStructure"
		

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["FamiliarizingStructuratingListersStructure"]=[
												]

	

	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

#</DefineClass>