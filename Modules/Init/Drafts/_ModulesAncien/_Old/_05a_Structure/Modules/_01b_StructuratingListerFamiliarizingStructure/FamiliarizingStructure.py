#<Import Modules>
import multiprocessing
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class FamiliarizingStructureClass(
									SYS.StructureClass,
									SYS.FamiliarizerClass,
									SYS.ParentListerClass
									):

	#<DefineHookMethods>
	def initAfterBasesWithFamiliarizingStructure(self):

		#<DefineSpecificDict>
		self.ListerTypeString="FamiliarizerAddingLister"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
												]

	

	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

#</DefineClass>