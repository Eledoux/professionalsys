#<Import Modules>
import multiprocessing
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class StructuratingStructuresAndListersTypingStructureClass(
									SYS.StructuratingStructuresTypingStructureClass,
									SYS.StructuratingListersTypingStructureClass
									):
	pass
	#<DefineHookMethods>
	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

#</DefineClass>