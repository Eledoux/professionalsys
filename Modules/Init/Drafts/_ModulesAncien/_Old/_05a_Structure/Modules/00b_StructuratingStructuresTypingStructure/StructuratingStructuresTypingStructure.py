#<Import Modules>
import multiprocessing
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class StructuratingStructuresTypingStructureClass(
									SYS.StructureClass
									):

	#<DefineHookMethods>
	def initAfterBasesWithStructuratingStructuresTypingStructure(self):

		#<DefineSpecificDict>
		self.StructuratingStructuresTypingStructurePointeringTyper=SYS.StructuratingStructuresTypingStructurePointeringTyperClass().update(
			{'StructuratingStructuresTypingStructurePointer':self}
			)
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuratingStructuresTypingStructure"]=[
											"StructuratingStructuresTypingStructurePointeringTyper"
												]
	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

#</DefineClass>