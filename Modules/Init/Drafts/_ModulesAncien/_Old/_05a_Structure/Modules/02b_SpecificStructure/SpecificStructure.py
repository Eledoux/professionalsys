#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SpecificStructureClass(
								SYS.StructureClass
							):

	#<DefineHookMethods>
	def initAfterBasesWithSpecificStructure(self):
		
		#<DefineSpecificDict>
		self.SpecificInstancedStructuredBaseTypeString="Object"
		self.StructuredIntsListsList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SpecificStructure"]=[	
											"SpecificInstancedStructuredBaseTypeString",
											"StructuredIntsListsList"
										]	

		#Update the StructuratingStructuresTypeString 
		self['StructuratingStructuresTypeString']="SpecificStructure"

		#Update the StructuratingListersTypeString 
		self['StructuratingListersTypeString']="SpecificInstancingStructuratingLister"

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setStructuratingStructurePointersListAfterBasesWithSpecificStructure(self):

		#Bind with SpecificInstancedStructuredBaseTypeString setting
		if len(self.StructuratingStructurePointersList)>0:
			if hasattr(self.StructuratingStructurePointersList[0],"SpecificInstancedStructuredBaseTypeString"):
				self['SpecificInstancedStructuredBaseTypeString']=self.StructuratingStructurePointersList[0].SpecificInstancedStructuredBaseTypeString


	def setStructuredIntsListsListAfterBasesWithSpecificStructure(self):

		#Add the corresponding empty Structure of dict 
		self+=SYS.getStructuredDictsListWithIntsListsList(self.StructuredIntsListsList)
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

