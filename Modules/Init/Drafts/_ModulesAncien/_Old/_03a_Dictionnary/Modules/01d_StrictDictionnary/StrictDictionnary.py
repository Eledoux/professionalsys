#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StrictDictionnaryClass(SYS.DictionnaryClass):
	
	#<DefineHookMethods>	
	def setBeforeBasesWithStrictDictionnary(self,_KeyVariable,_ValueVariable):

		#Return an InstancedVariable
		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable[0]=="_":

				#Get the DictatedTuplesList and the DictatedVariablesList 
				DictatedTuplesList=list(self.__iter__())
				DictatedVariablesList=map(lambda DictatedTuple:DictatedTuple[1],DictatedTuplesList)

				#Look if it alreadySetted
				if _ValueVariable in DictatedVariablesList:

					#Get the IndexInt
					IndexInt=DictatedVariablesList.index(_ValueVariable)

					#Get the OldDictatedKeyString
					OldDictatedKeyString=DictatedTuplesList[IndexInt][0]

					#Rename
					self.rename(OldDictatedKeyString,_KeyVariable)



	#</DefineHookMethods>
	#<DefineBindingMethods>
	
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

