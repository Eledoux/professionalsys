#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DicterPointeringInstancingDictionnaryClass(
								SYS.DicterPointeringDictionnaryClass,
								SYS.InstancingDictionnaryClass
							):
	pass
	#<DefineHookMethods>
	#</DefineHookMethods>

	#<DefineBindingMethods>
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

