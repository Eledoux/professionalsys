#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class FamiliarizerDictionnaryClass(
								SYS.DictionnaryClass,
								SYS.FamiliarizerClass
							):

	#<DefineHookMethods>
	def setAfterBasesWithFamiliarizerDictionnary(self,_KeyVariable,_ValueVariable):

		#Wrap the Dictionnary "_" setting with the ParentPointer specification
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[0:2]=="x_":
					_ValueVariable['ParentPointer']=self

	#</DefineHookMethods>

	#<DefineBindingMethods>
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

