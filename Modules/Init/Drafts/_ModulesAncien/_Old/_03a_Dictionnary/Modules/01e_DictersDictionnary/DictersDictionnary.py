#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DictersDictionnaryClass(
								SYS.InstancingDictionnaryClass
							):
	
	#<DefineHookMethods>
	def initAfterBasesWithDictersDictionnary(self):

		#<DefineSpecificDict>
		self['InstancedDictatedBaseTypeString']="Dicter"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["DictersDictionnary"]=[
												]

	#</DefineHookMethods>

	#<DefineBindingMethods>
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

