#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class PointersDictionnaryClass(SYS.InstancingDictionnaryClass):
	
	#<DefineHookMethods>	
	def getBeforeBasesWithPointersDictionnary(self,_KeyVariable):

		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable[0]=="_":

				#Get the SuffixString
				SuffixString=SYS.getCommonSuffixStringWithStringsList(["Pointer",_KeyVariable])

				#Add the Pointer word
				if SuffixString=="":

					#Return with the NewKeyVariable
					return self[_KeyVariable+"Pointer"]

	def setBeforeBasesWithPointersDictionnary(self,_KeyVariable,_ValueVariable):

		#Return an InstancedVariable
		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable[0]=="_":
				return (_KeyVariable+"Pointer",_ValueVariable)

	#</DefineHookMethods>
	#<DefineBindingMethods>
	
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

