#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancingDictionnaryClass(SYS.DictionnaryClass):

	#<DefineHookMethods>
	def initAfterBasesWithInstancingDictionnary(self):

		#<DefineSpecificDict>
		self.InstancedDictatedBaseTypeString="Object"
		self.NotInstancedKeyStringsList=[
											"InstancedDictatedBaseTypeString",
											"SkippedKeyStringsList",
											"TypeStringToKeyStringsListDict"
										]+self.TypeStringToKeyStringsListDict['Core']
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingDictionnary"]=[
													"InstancedDictatedBaseTypeString",
													"NotInstancedKeyStringsList"
												]

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=["NotInstancedKeyStringsList"]


	def setBeforeBasesWithInstancingDictionnary(self,_KeyVariable,_ValueVariable):

		#Return an InstancedVariable
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[0]=="_":
					return (_KeyVariable,SYS.getTypedVariableWithVariableAndTypeString(_ValueVariable,self.InstancedDictatedBaseTypeString))

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setInstancedDictatedBaseTypeStringAfterBasesWithInstancingDictionnary(self):

		#Get the possible DictionnaryClassString
		ClassString=SYS.getClassStringWithTypeString(self.InstancedDictatedBaseTypeString)

		#Bind with DictionnaryTypeString setting
		Class=getattr(SYS,ClassString)
		if Class==None:
			self['InstancedDictatedBaseTypeString']="Object"
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
