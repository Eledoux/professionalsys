#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DicterPointeringDictionnaryClass(
								SYS.DictionnaryClass
							):

	#<DefineHookMethods>
	def initAfterBasesWithDicterPointeringDictionnary(self):

		#<DefineSpecificDict>
		self.DicterPointer=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"DicterPointer"
												]
	#</DefineHookMethods>

	#<DefineBindingMethods>
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

