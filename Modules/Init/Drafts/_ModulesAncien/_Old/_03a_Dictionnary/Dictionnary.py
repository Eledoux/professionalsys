#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#Define the PluralString
PluralString="Dictionnaries"

#<DefineClass>
class DictionnaryClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithDictionnary(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Dictionnary"]=[
												]
	def iterAfterBasesWithDictionnary(self,_IteratedList):

		#Return the dictated Items with "_"
		return filter(lambda Item:len(Item[0])>0 and Item[0][0]=="_",self.__dict__.items())

	def measureAfterBasesWithDictionnary(self,_MeasureVariable):

		#Measure only the ones with a KeyString that has a "_" at the beginning
		return len(filter(lambda KeyString:KeyString[0]=="_",self.keys()))
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
