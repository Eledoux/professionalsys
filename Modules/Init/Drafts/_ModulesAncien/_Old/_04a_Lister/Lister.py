#<Import Modules>
import multiprocessing
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class ListerClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithLister(self):

		#<DefineSpecificDict>
		self.ListedVariablesInt=0
		self.ListKeyString="VariablesList"
		self.VariablesList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"ListedVariablesInt",
													"ListKeyString",
													"ListedTypeString"
												]

	def getBeforeBasesWithLister(self,_KeyVariable):

		#Check for digit string
		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable.isdigit():
				_KeyVariable=int(_KeyVariable) 

		#Consider the Int Case for accesiing directly Variables in the list
		if type(_KeyVariable)==int:
			if _KeyVariable<self.ListedVariablesInt:
				return getattr(self,self.ListKeyString)[_KeyVariable]

	def setAfterBasesWithLister(self,_KeyVariable,_ValueVariable):

		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable==self.ListKeyString:

				#Bind With ListedVariablesIntSetting
				self['ListedVariablesInt']=len(getattr(self,self.ListKeyString))

			#Return 
			return (_KeyVariable,_ValueVariable)

		if type(_KeyVariable)==int:
			if _KeyVariable<self.ListedVariablesInt:
				TempList=getattr(self,self.ListKeyString)
				self[self.ListKeyString]=TempList[:_KeyVariable]+[_ValueVariable]+(TempList[_KeyVariable+1:] if _KeyVariable<self.ListedVariablesInt-1 else [])
				self.IsSettingBool=False

	def iterAfterBasesWithLister(self,_IteratedList):

		#Iter on the ListedVariables
		return getattr(self,self.ListKeyString)

	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	def setListedVariablesIntAfterBasesWithLister(self):
		
		#Make the List of the Lister of the good size
		if len(getattr(self,self.ListKeyString))<self.ListedVariablesInt:
			self[self.ListKeyString]=getattr(self,self.ListKeyString)+[{} for Int in xrange(self.ListedVariablesInt-len(getattr(self,self.ListKeyString)))]
			
		elif len(getattr(self,self.ListKeyString))>self.ListedVariablesInt:
			setattr(self,self.ListKeyString,getattr(self,self.ListKeyString)[:self.ListedVariablesInt])
	#</DefineBindingHookMethods>

#</DefineClass>