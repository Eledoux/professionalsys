#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class InstancedListedVariablesListerClass(
								SYS.InstancingListerClass
							):
	
	#<DefineHookMethods>
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setInstancedListedBaseTypeStringsListAfterBasesWithInstancingLister(self):

		#Instancify the ListedVariables
		setattr(self,
				self.ListKeyString,
				self.getTypedVariableListWithVariablesList(getattr(self,self.ListKeyString))
			)
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>