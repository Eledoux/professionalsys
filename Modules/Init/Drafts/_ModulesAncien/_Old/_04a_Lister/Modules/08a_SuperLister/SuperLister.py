#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SuperListerClass(
						SYS.InstancingListerClass,
						SYS.AddingListerClass,
						SYS.ParentListerClass,
						SYS.NamingListerClass,
						SYS.RankedListerClass
						):
	
	#<DefineHookMethods>
	#</DefineHookMethods>

	#<DefineHookBindingMethods>
	def setInstancedListedBaseTypeStringsListAfterBasesWithSupertLister(self):
		
		#Get the IsBaseTypeStringBoolsLi
		IsBaseTypeStringBoolsList=map(lambda InstancedListedBaseTypeString:SYS.getIsBaseTypeStringBoolWithBaseTypeStringAndTypeString(self.DemandedListedBaseTypeString,InstancedListedBaseTypeString),self.InstancedListedBaseTypeStringsList)
	
		#Freeze the setInstancedTypeStringsListAfterBasesWithSmartLister Method
		self.FrozenMethodStringsList.append('setInstancedListedBaseTypeStringsListAfterBasesWithSuperLister')

		#Bind with InstancedTypeStringsList Setting again
		self['InstancedListedBaseTypeStringsList']=map(lambda InstancedListedBaseTypeString,Bool:InstancedListedBaseTypeString if Bool else self.DemandedListedBaseTypeString,self.InstancedListedBaseTypeStringsList,IsBaseTypeStringBoolsList)
	
		#Reput the Hook
		self.FrozenMethodStringsList.pop(self.FrozenMethodStringsList.index('setInstancedListedBaseTypeStringsListAfterBasesWithSuperLister'))
	#</DefineHookBindingMethods>

#</DefineClass>