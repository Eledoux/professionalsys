#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class AddingListerClass(SYS.ListerClass):
	
	#<DefineHookMethods>
	def addAfterBasesWithAddingLister(self,_AddedVariable):
		if hasattr(_AddedVariable,"items"):
			self[self.ListKeyString]=getattr(self,self.ListKeyString)+[_AddedVariable]
		elif type(_AddedVariable) in [list,tuple]:
			if type(_AddedVariable)==tuple:
				_AddedVariable=list(_AddedVariable)
			#if all(map(lambda Dict:hasattr(Dict,"items"),_AddedVariable)):
			self[self.ListKeyString]=getattr(self,self.ListKeyString)+_AddedVariable
	#</DefineHookMethods>


#</DefineClass>