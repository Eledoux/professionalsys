#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class ListedBindersListedListerClass(
							SYS.ListedListerClass,
							SYS.SpecificInstancingListerClass,
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithListedBindersListedLister(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the SpecificInstancedListedBaseTypeString
		self['SpecificInstancedListedBaseTypeString']="ListedBinder"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["ListedBindersListedLister"]=[
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>