#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SpecificInstancingStructuratingListerClass(
								SYS.StructuratingListerClass,
								SYS.SpecificInstancingListerClass
						):
	#<DefineHookMethods>
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setStructuratingStructurePointersListAfterBasesWithSpecificInstancingStructuratingLister(self):

		#Bind with SpecificInstancedStructuredTypeString of the first StructuratingStructurePointer
		if len(self.StructuratingStructurePointersList)>0:
			if hasattr(self.StructuratingStructurePointersList[0],"SpecificInstancedStructuredBaseTypeString"):
				self['SpecificInstancedListedBaseTypeString']=self.StructuratingStructurePointersList[0].SpecificInstancedStructuredBaseTypeString
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>