#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SuperStructuratingListerClass(
							SYS.SortedStructuratingListerClass,
							SYS.SpecificInstancingStructuratingListerClass
						):
	
	pass
	#<DefineHookMethods>
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>