#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SuperStructuratingListerClass(
							SYS.SortedStructuratingListerClass,
							SYS.SpecificStructuratingListerClass
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithSuperStructuratingLister(self):
		
		#<DefineSpecificDict>
		self.SortingStructurePointersList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[	
											"SortingStructurePointersList"
										]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setCoordinatedIntsListAfterBasesWithSuperStructuratingLister(self):

		#Bind with SortedString setting
		self['SortedString']=','.join(map(lambda CoordinateInt:str(CoordinateInt),self.CoordinatedIntsList))
	
	def setGrandParentPointersListAfterBasesWithSuperStructuratingLister(self):

		#Bind with SortingStructurePointersList setting
		self['SortingStructurePointersList']=filter(
			lambda GrandParentPointer:
			SYS.getIsTypedVariableBoolWithTypeStringAndVariable("SortingStructure",GrandParentPointer),
			self.GrandParentPointersList)

	def setSortingStructurePointersListAfterBasesWithSuperStructuratingLister(self):

		#Bind with setSortingStructures
		self.setInSortingStructurePointersWithSuperStructuratingLister()

	def setSortedStringAfterBasesWithSuperStructuratingLister(self):

		#Bind with setSortingStructures
		self.setInSortingStructurePointersWithSuperStructuratingLister()
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setInSortingStructurePointersWithSuperStructuratingLister(self):

		#Set the self in the SortingStructures
		if len(self.SortedString)>0:
			map(lambda SortingStructurePointer:
				SortingStructurePointer.__setitem__("*_"+self.SortedString,self),
				self.SortingStructurePointersList)

	#</DefineMethods>

#</DefineClass>