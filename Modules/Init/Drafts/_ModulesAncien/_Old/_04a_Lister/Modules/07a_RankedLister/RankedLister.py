#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class RankedListerClass(SYS.ListerClass):

	#<DefineHookMethods>
	def initAfterBasesWithRankedLister(self):
		
		#<DefineSpecificDict>
		self.PreviousRankedVariablesIntsList=[]
		self.NextRankedVariablesIntsList=[]
		self.RankedVariablesInt=0
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
												"PreviousRankedVariablesIntsList",
												"NextRankedVariablesIntsList",
												"RankedVariablesInt"
												]
	#</DefineHookMethods>

	#<DefineHookBindingMethods>
	def setSorterPointersListAfterBasesWithRankedLister(self):

		#Bind With PreviousRankedVariablesIntsList Setting
		self['PreviousRankedVariablesIntsList']=[0]*len(self.SorterPointersList)

		#Bind With NextRankedVariablesIntsList Setting
		self['NextRankedVariablesIntsList']=[0]*len(self.SorterPointersList)

	def setPreviousRankedVariablesIntsListAfterBasesWithRankedLister(self):

		#Bind with RankedVariablesInt Setting
		if SYS.getIsBaseTypeStringBoolWithBaseTypeStringAndTypeString("Ranker",self.TypeString):
			self['RankedVariablesInt']=self.getRankedVariablesInt()
		else:
			
			#Bind with NextRankedVariablesIntsList Setting
			self.setNextRankedVariablesIntsList()

			#Give to each RankedElement the RankedInt
			map(lambda IntAndListedVariable:IntAndListedVariable[1].__setitem__('RankedIntsList',map(lambda Int:Int+IntAndListedVariable[0],self.PreviousRankedVariablesIntsList)),enumerate(getattr(self,self.ListKeyString)))

	def setRankedVariablesIntAfterBasesWithRankedLister(self):

		#Bind with NextRankedVariablesIntsList Setting
		self.setNextRankedVariablesIntsList()

	def setListedVariablesIntAfterBasesWithRankedLister(self):

		#Bind with the RankedVariablesInt
		self['RankedVariablesInt']=self.ListedVariablesInt

	#<DefineHookBindingMethods>

	#<DefineMethods>
	def setPreviousRankedVariablesIntsList(self):
					
		#Check if this Lister is listed into a Table
		if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Lister",self.ParentPointer):

			#If it is not the first then look for the previous listed one
			if self.ElementVariable>0:
				if hasattr(self.ParentPointer[self.ElementVariable-1],"NextRankedVariablesIntsList"):
					self['PreviousRankedVariablesIntsList']=map(lambda Int:Int,self.ParentPointer[self.ElementVariable-1].NextRankedVariablesIntsList)
					return 
			elif hasattr(self.ParentPointer,'PreviousRankedVariablesIntsList'):

				#Else look for the Parent if it has a PreviousRankedVariablesInt
				self['PreviousRankedVariablesIntsList']=[0]+map(lambda Int:Int,self.ParentPointer.PreviousRankedVariablesIntsList)
				return			
		
		#Set to 0 by default	
		self['PreviousRankedVariablesIntsList']=[0]

	def setAllPreviousRankedVariablesInt(self):

		#setPreviousRankedVariablesInt for this one
		if len(self.SorterPointersList)>0:
			self.setPreviousRankedVariablesIntsList()

		#parse
		map(
			lambda ListedTable:ListedTable.setAllPreviousRankedVariablesInt() 
			if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Ranker",ListedTable) 
			else ListedTable.setPreviousRankedVariablesIntsList() 
				if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Table",ListedTable)
				else None,
				getattr(self,self.ListKeyString))

	def getRankedVariablesInt(self):

		#Bind with RankedVariablesInt Setting
		if SYS.getIsBaseTypeStringBoolWithBaseTypeStringAndTypeString("Ranker",self.TypeString):

			#Set setAllPreviousRankedVariablesInt in each listed RankedLister
			map(lambda ListedTable:
				ListedTable.setAllPreviousRankedVariablesInt(),getattr(self,self.ListKeyString))

			#Get the RankedVariablesIntsList for the Sorter Case
			RankedVariablesIntsList=map(lambda ListedTable:
				ListedTable.getRankedVariablesInt() 
				if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Ranker",ListedTable) 
				else ListedTable.ListedVariablesInt,getattr(self,self.ListKeyString))

			#Compute the sum
			return sum(RankedVariablesIntsList)
		else:

			#Get directly the RankedVariablesInt
			return self.ListedVariablesInt

	def setNextRankedVariablesIntsList(self):

		#Bind with NextRankedVariablesInt Setting
		self['NextRankedVariablesIntsList']=map(lambda PreviousRankedVariablesInt:PreviousRankedVariablesInt+self.RankedVariablesInt,self.PreviousRankedVariablesIntsList)

	#</DefineMethods>

#</DefineClass>

