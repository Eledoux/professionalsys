#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class StructuratingListerClass(
								SYS.ListedObjectClass,
								SYS.StructuratingStructuresPointeringObjectClass,
								SYS.AddingListerClass,
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithStructuratingLister(self):
		
		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuratingLister"]=[	
										]

	def addBeforeBasesWithStructuratingLister(self,_AddedVariable):
		
		#Special add in the database : set the StructuratingStructurePointersLis 
		if type(_AddedVariable)==list:

			#Add a Pointer on self in the StructurePointersList of each Child Structures, StructuratingLister
			map(lambda ListedAddedVariable:
				ListedAddedVariable.__setitem__(
									"StructuratingStructurePointersList",
					ListedAddedVariable.StructuratingStructurePointersList+[self]+self.StructuratingStructurePointersList
									) 
				if hasattr(ListedAddedVariable,"StructuratingStructurePointersList") 
				else None,
				_AddedVariable)
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>