#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SpecificInstancingListerClass(
						SYS.InstancingListerClass,
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithSpecificInstancingLister(self):
		
		#<DefineSpecificDict>
		self.SpecificInstancedListedBaseTypeString="Object"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SpecificInstancingLister"]=[	
											"SpecificInstancedListedBaseTypeString"
										]

	def setBeforeBasesWithSpecificInstancingLister(self,_KeyVariable,_ValueVariable):

		#Special Case of filling the list
		if _KeyVariable==self.ListKeyString:
			if type(_ValueVariable)==list:

				#Set the size of the Listed list
				self.ListedVariablesInt=len(_ValueVariable)

				#Then update the InstancedListedBaseTypeStringsList
				self.setInstancedListedBaseTypeStringsListWithSpecificInstancingLister()
	#</DefineHookMethods>

	#<DefineHookBindingMethods>
	def setSpecificInstancedListedBaseTypeStringsListAfterBasesWithSpecificInstancingLister(self):

		#Bind with InstancedListedBaseTypeStringsList setting
		self.setInstancedListedBaseTypeStringsListWithSpecificInstancingLister()
	#</DefineHookBindingMethods>

	#<DefineMethods>
	def setInstancedListedBaseTypeStringsListWithSpecificInstancingLister(self):
		self['InstancedListedBaseTypeStringsList']=map(
			lambda IndexInt:
			self.SpecificInstancedListedBaseTypeString,
			xrange(self.ListedVariablesInt)
			)

	#<DefineMethods>

#</DefineClass>