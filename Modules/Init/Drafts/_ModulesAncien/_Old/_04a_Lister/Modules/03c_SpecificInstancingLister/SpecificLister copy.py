#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SpecificListerClass(
						SYS.InstancingListerClass,
						SYS.DemandingListerClass
						):
	
	#<DefineHookMethods>
	#</DefineHookMethods>

	#<DefineHookBindingMethods>
	def setInstancedListedBaseTypeStringsListAfterBasesWithSpecificLister(self):
		
		#Get the IsBaseTypeStringBoolsLi
		IsBaseTypeStringBoolsList=map(lambda InstancedTypeString:SYS.getIsBaseTypeStringBoolWithBaseTypeStringAndTypeString(self.DemandeListedBaseTypeString,InstancedTypeString),self.InstancedListedBaseTypeStringsList)
	
		#Freeze the setInstancedListedBaseTypeStringsListAfterBasesWithSmartLister Method
		self.FrozenMethodStringsList.append('setInstancedListedBaseTypeStringsListAfterBasesWithSpecificLister')

		#Bind with InstancedListedBaseTypeStringsList Setting again
		self['InstancedListedBaseTypeStringsList']=map(lambda InstancedTypeString,Bool:InstancedTypeString if Bool else self.DemandedListedBaseTypeString,self.InstancedListedBaseTypeStringsList,IsBaseTypeStringBoolsList)
	
		#Reput the Hook
		self.FrozenMethodStringsList.pop(self.FrozenMethodStringsList.index('setInstancedListedBaseTypeStringsListAfterBasesWithSpecificLister'))
	#</DefineHookBindingMethods>

#</DefineClass>