#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SortingListerClass(
							SYS.ListerClass,
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithSortingLister(self):

		#<DefineSpecificDict>
		self.ListingSorter=SYS.SorterClass()
		self.ListingSorterTypeString="Sorter"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SortingLister"]=[
												"ListingSorter",
												"ListingSorterTypeString"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setListingSorterTypeStringAfterBasesWithSortingLister(self):

		#Bind with ListingSorter setting
		self['ListingSorter']=SYS.getTypedVariableWithVariableAndTypeString(self.ListingSorter,self.ListingSorterTypeString)


	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>