#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class InstancingTypingListerClass(SYS.InstancingListerClass):
	
	#<DefineHookMethods>
	def initAfterBasesWithInstancingTypingLister(self):

		#<DefineSpecificDict>
		self.InstancingListerPointeringTypersList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingTypingLister"]=[
													"InstancingListerPointeringTypersList"
												]

	#<DefineBindingHookMethods>
	def setInstancedListedBaseTypeStringsListAfterBasesWithInstancingTypingLister(self):

		#Look for the difference of Length
		DiffLengthInt=len(self.InstancedListedBaseTypeStringsList)-len(self.InstancingListerPointeringTypersList)

		#extend if it is bigger
		if DiffLengthInt>0:
			self.InstancingListerPointeringTypersList.extend(
				map(
						lambda Int:
						SYS.InstancingListerPointeringTyperClass().update({'InstancingTypingListerPointer':self}),
						xrange(DiffLengthInt)
					)
				)

		#Bind with FreeDerivedTypeString setting of the DerivingTypeString setting
		map(lambda IndexInt,InstancedDictatedBaseTypeString:
			self.InstancingListerPointeringTypersList[IndexInt].setDerivingTypeStringWithInstancedListedBaseTypeStringWithInstancingListerPointeringTyper(InstancedDictatedBaseTypeString),
			xrange(len(self.InstancedListedBaseTypeStringsList)),
			self.InstancedListedBaseTypeStringsList)

		if DiffLengthInt<0:
			self.InstancingListerPointeringTypersList=self.InstancingListerPointeringTypersList[:len(self.InstancedListedBaseTypeStringsList)]

	#</DefineBindingHookMethods>

#</DefineClass>