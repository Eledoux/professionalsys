#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class StructuratingListerClass(
							SYS.AddingListerClass,
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithStructuratingLister(self):
		
		#<DefineSpecificDict>
		self.StructurePointersList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuratingLister"]=[
											"StructurePointersList"	
										]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>