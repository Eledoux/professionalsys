#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class ListersListerClass(SYS.SpecificInstancingListerClass):
	
	#<DefineHookMethods>
	def initAfterBasesWithListersLister(self):
		
		#<DefineSpecificDict>
		self['SpecificInstancedListedBaseTypeString']="Lister"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["ListersLister"]=[	
										]

	#</DefineHookMethods>


#</DefineClass>