#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class AddingInstancingListerClass(
									SYS.InstancingListerClass,
									SYS.AddingListerClass
								):
	pass
	#<DefineHookMethods>
	#</DefineHookMethods>
	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

#</DefineClass>