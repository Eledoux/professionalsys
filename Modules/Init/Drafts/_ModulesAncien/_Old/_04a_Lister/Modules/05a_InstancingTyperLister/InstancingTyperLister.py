#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class InstancingTyperListerClass(SYS.InstancingListerClass):
	
	#<DefineHookMethods>
	def initAfterBasesWithInstancingTyperLister(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingTyperLister"]=[
												]

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

#</DefineClass>