#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class FamiliarizerAddingListerClass(
										SYS.AddingListerClass,
										SYS.FamiliarizerListerClass
									):
	pass
	#<DefineHookMethods>
	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

#</DefineClass>