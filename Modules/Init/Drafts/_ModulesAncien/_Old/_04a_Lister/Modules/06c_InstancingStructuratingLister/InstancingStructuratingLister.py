#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class InstancingStructuratingListerClass(
								SYS.StructuratingListerClass,
								SYS.InstancingListerClass
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithInstancingStructuratingLister(self):
		
		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingStructuratingLister"]=[	
										]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setStructuratingStructurePointersListAfterBasesWithInstancingStructuratingLister(self):

		#Bind with InstancedStructuredBaseTypeStringsList of the first StructuratingStructurePointer
		if len(self.StructuratingStructurePointersList)>0:
			if hasattr(self.StructuratingStructurePointersList[0],"InstancedStructuredBaseTypeStringsList"):
				self['InstancedListedBaseTypeStringsList']=self.StructuratingStructurePointersList[0].InstancedStructuredBaseTypeStringsList
			
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>