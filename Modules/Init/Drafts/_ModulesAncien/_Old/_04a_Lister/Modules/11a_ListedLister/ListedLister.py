#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class ListedListerClass(
							SYS.ListerClass,
							SYS.ListedObjectClass
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithListedBindersLister(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the SpecificInstancedListedBaseTypeString
		self['SpecificInstancedListedBaseTypeString']="ListedBinder"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["ListedBindersLister"]=[
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setAfterBasesWithListedLister(self,_KeyVariable,_ValueVariable):

		#Bind with setInEachWithListedLister
		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable==self.ListKeyString:

				self.setInEachWithListedLister()

	def setListingListerPointersList(self):

		#Bind with setInEachWithListedLister
		self.setInEachWithListedLister()
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setInEachWithListedLister(self):

		#Give to each possible ListedObject a ListingListerPointer to self
		map(lambda ListedVariable,IndexInt:
			ListedVariable.update(
					{
						'ListingListerPointersList':[self]+self.ListingListerPointersList,
						'ListingIntsList':[IndexInt]+self.ListingIntsList
					}
					) 
			if hasattr(ListedVariable,"ListingListerPointersList")
			else None,
			getattr(self,self.ListKeyString),
			xrange(self.ListedVariablesInt)
			)

	#</DefineMethods>

#</DefineClass>