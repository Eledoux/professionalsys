#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SortedStructuratingListerClass(
								SYS.StructuratingListerClass,
								SYS.SortedStructuratingObjectClass
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithSortedStructuratingLister(self):
		
		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SortedStructuratingLister"]=[	
										]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>