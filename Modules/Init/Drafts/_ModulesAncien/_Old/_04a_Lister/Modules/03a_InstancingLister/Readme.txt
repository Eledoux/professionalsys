The InstancingLister is a Lister
that instancifies at each special 
index of the List the object into 
an object having a specific InstancedTypeString
(but it can be a derived Class from this)