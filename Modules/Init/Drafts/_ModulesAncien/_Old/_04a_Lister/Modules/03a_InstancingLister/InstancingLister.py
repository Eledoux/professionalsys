#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class InstancingListerClass(SYS.ListerClass):
	
	#<DefineHookMethods>
	def initAfterBasesWithInstancingLister(self):

		#<DefineSpecificDict>
		self.InstancedListedBaseTypeStringsList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"InstancedListedBaseTypeStringsList"
												]

	#<DefineBindingHookMethods>
	def setBeforeBasesWithInstancingLister(self,_KeyVariable,_ValueVariable):

		#Special Case of filling the list
		if _KeyVariable==self.ListKeyString:
			if type(_ValueVariable)==list:

				#If InstancedListedBaseTypeStringsList has a smaller size fill it with None
				if len(self.InstancedListedBaseTypeStringsList)<len(_ValueVariable):
					self['InstancedListedBaseTypeStringsList']=self.InstancedListedBaseTypeStringsList+map(
						lambda Int:_ValueVariable[len(self.InstancedListedBaseTypeStringsList)+Int].TypeString if 
						hasattr(_ValueVariable[len(self.InstancedListedBaseTypeStringsList)+Int],"TypeString") 
						else "None",
						xrange(len(_ValueVariable)-len(self.InstancedListedBaseTypeStringsList)))
				
				#Instancify the ListedVariables
				TypedVariableList=self.getTypedVariableListWithVariablesList(_ValueVariable)

				"""
				TypedVariableList=map(
						lambda ListedVariable,ListedInstancedTypeString:
						SYS.getTypedVariableWithVariableAndTypeString(ListedVariable,ListedInstancedTypeString if ListedInstancedTypeString!=None else ""),
						_ValueVariable,self.InstancedListedBaseTypeStringsList)
				"""

				#Return to set Hook method
				return (_KeyVariable,TypedVariableList)

			#Set the IsSettingBool to False
			self.IsSettingBool=False

	"""
	def setInstancedListedBaseTypeStringsListAfterBasesWithInstancingLister(self):

		#Instancify the ListedVariables
		setattr(self,
				self.ListKeyString,
				self.getTypedVariableListWithVariablesList(getattr(self,self.ListKeyString))
			)
	"""
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def getTypedVariableListWithVariablesList(self,_VariablesList):

		#For each try to type with the self.InstancedListedBaseTypeStringsList
	 	return map(lambda ListedVariable,ListedInstancedTypeString:
			SYS.getTypedVariableWithVariableAndTypeString(ListedVariable,ListedInstancedTypeString) 
			if ListedInstancedTypeString!=None else "",
			_VariablesList,
			self.InstancedListedBaseTypeStringsList)

	#</DefineMethods>

#</DefineClass>