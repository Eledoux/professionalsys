#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class ParentListerClass(SYS.ListerClass):
	
	#<DefineHookMethods>
	def setAfterBasesWithParentLister(self,_KeyVariable,_ValueVariable):	
		
		#Get the WordStringsList
		if _KeyVariable==self.ListKeyString:
			
			#Give the ParentPointer to each Listed Child
			map(lambda IntAndIterValueVariable:_ValueVariable[IntAndIterValueVariable[0]].__setitem__("ParentPointer",self) if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Child",IntAndIterValueVariable[1]) else None,enumerate(_ValueVariable))
						
			#Return 
			return (_KeyVariable,_ValueVariable)
	#</DefineHookMethods>

#</DefineClass>