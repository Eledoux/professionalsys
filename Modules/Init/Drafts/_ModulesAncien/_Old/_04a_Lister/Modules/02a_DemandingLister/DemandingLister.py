#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class DemandingListerClass(SYS.ListerClass):
	
	#<DefineHookMethods>
	def initAfterBasesWithDemandingLister(self):
		
		#<DefineSpecificDict>
		self['DemandedListedBaseTypeString']="Object"
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"DemandedListedBaseTypeString"
												]

	def setBeforeBasesWithDemandingLister(self,_KeyVariable,_ValueVariable):

		#Case of filling the List
		if _KeyVariable==self.ListKeyString:

			#Check if the Variables respects the DemandedListedBaseTypeString
			IsBaseTypeStringBoolsList=map(lambda Variable:SYS.getIsBaseTypeStringBoolWithBaseTypeStringAndTypeString(self.DemandedListedBaseTypeString,Variable.TypeString if hasattr(Variable,"TypeString") else "Variable"),_ValueVariable)

			#Filter the ones that don't respect the DemandedListedBaseTypeString
			NewValueVariable=map(lambda ListedVariable,IsBaseTypeStringBool:ListedVariable if IsBaseTypeStringBool else None,_ValueVariable,IsBaseTypeStringBoolsList)

			#Return 
			return (_KeyVariable,NewValueVariable)

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setDemandedListedBaseTypeStringAfterBasesWithDemandingLister(self):

		#Bind With ListKeyString Setting
		self['ListKeyString']=SYS.getPluralStringWithSingularString(self.DemandedListedBaseTypeString)+"List"

	def setListKeyStringBeforeBasesWithDemandingLister(self):

		#Bind With Temporary OldListKeyString Setting
		self.OldListKeyString=self.ListKeyString

	def setListKeyStringAfterBasesWithDemandingLister(self):

		#Set the new ListKeyString
		if hasattr(self,'OldListKeyString'):
			self.rename(self.OldListKeyString,self.ListKeyString)

		#Delete the OldListedTypeString
		del self.OldListKeyString
	#</DefineBindingHookMethods>

#</DefineClass>