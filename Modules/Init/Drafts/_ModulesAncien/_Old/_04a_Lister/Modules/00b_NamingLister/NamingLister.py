#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class NamingListerClass(SYS.ListerClass):
	
	#<DefineHookMethods>
	def setAfterBasesWithNamingLister(self,_KeyVariable,_ValueVariable):	
		
		#Get the WordStringsList
		if _KeyVariable==self.ListKeyString:
			
			#Give the ElementVariable to each Listed Elements
			map(lambda IntAndIterValueVariable:_ValueVariable[IntAndIterValueVariable[0]].__setitem__("NamedVariable",IntAndIterValueVariable[0]) if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("NamedObject",IntAndIterValueVariable[1]) else None,enumerate(_ValueVariable))
			
			#Return 
			return (_KeyVariable,_ValueVariable)
	#</DefineHookMethods>

#</DefineClass>