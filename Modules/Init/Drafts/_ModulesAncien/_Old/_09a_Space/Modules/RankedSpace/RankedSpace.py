#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class RankedSpaceClass(SYS.SpaceClass):

	#<DefineHookMethods>
	def initAfterBasesWithSpace(self):
		
		#<DefineSpecificDict>
		self['ArrayedTypeString']="RankedCoordinate"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
							
											]

	def getBeforeBasesWithRankedSpace(self,_KeyVariable):

		#Special RankedCoordinate Getting
		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable[0]=="#":
				return self.getRankedCoordinateWithRankedString(_KeyVariable)
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def getRankedCoordinateWithRankedString(self,_RankedString):

		#Get the IntString
		IntString=_RankedString[1:]

		#Check that it is a digit
		if IntString.isdigit():

			#Define the RankedInt
			RankedInt=int(IntString)

			#Check that this is a good query
			if RankedInt<self.ArrayedElementsInt and RankedInt>=0:

				#Get the ArrayedIntsList
				ArrayedIntsList=SYS.getArrayedIntsListWithArrayedLengthIntsListAndProdIntsListAndRankedInt(self.ArrayedLengthIntsList,self.SubArrayedElementsIntsList,RankedInt)

				#Return 
				return self[ArrayedIntsList]

	#</DefineMethods>

#</DefineClass>

