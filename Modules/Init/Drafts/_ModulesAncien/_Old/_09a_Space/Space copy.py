#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SpaceClass(SYS.ArrayClass):

	#<DefineHookMethods>
	def initAfterBasesWithSpace(self):
		
		#<DefineSpecificDict>
		self['ArrayedTypeString']="Coordinate"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

