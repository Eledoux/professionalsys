#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SpaceClass(SYS.SpecificDatabaseClass):

	#<DefineHookMethods>
	def initAfterBasesWithSpace(self):
		
		#<DefineSpecificDict>
		self['DatabasedTypeString']="Coordinate"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

