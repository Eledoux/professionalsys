#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class PlotClass(SYS.PatchClass):

	#<DefineHookMethods>
	def initAfterBasesWithPlot(self):
		
		#<DefineSpecificDict>
		self.FigurePointer=None
		self.PylabSubplotgrid=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
									"FigurePointer",
									"PylabSubplotGrid"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setPanelPointerAfterBasesWithPlot(self):

		#Bind With FigurePointer Setting
		self.FigurePointer=self.PanelPointer.FigurePointer

	def setPylabSubplotgridAfterBasesWithPlot(self):
		
		#Bind With PylabSubplotgrid.figure Setting
		self.PylabSubplotgrid.figure=self.FigurePointer

	def setPatchedRow0IntAfterBasesWithPlot(self):
		self.setSubplotgrid()
	def setPatchedRowsIntAfterBasesWithPlot(self):
		self.setSubplotgrid()
	def setPatchedCol0IntAfterBasesWithPlot(self):
		self.setSubplotgrid()
	def setPatchedColsIntAfterBasesWithPlot(self):
		self.setSubplotgrid()

	def setParentPointersListAfterBasesWithPlot(self):

		#Bind With the FigurePointer Setting
		TypeString="Figure"
		IsPointerBoolsList=map(lambda Pointer:SYS.getIsTypedVariableBoolWithTypeStringAndVariable(TypeString,Pointer),self.ParentPointersList)
		if any(IsPointerBoolsList):
			self[TypeString+"Pointer"]=self.ParentPointersList[IsPointerBoolsList.index(True)]	
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def setSubplotgrid(self):
		self['PylabSubplotgrid']=plt.subplot2grid((self.FigurePointer.PatchedRowsInt,self.FigurePointer.PatchedColsInt),(self.PatchedRow0Int,self.PatchedCol0Int),rowspan=self.PatchedRowsInt,colspan=self.PatchedColsInt)
	#</DefineMethods>

#</DefineClass>

