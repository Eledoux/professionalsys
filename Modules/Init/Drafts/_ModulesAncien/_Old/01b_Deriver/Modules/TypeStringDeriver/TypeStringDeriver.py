#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class TypeStringDeriverClass(SYS.DeriverClass):

	#<DefineHookMethods>
	def initAfterBasesWithTypeStringDeriver(self):
		
		#<DefineSpecificDict>
		self.DerivedTypeString=""
		self.DerivingTypeString="Core"
		self.DerivingBaseTypeStringsList=[]
		self.IsTypedBool=True
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["TypeStringDeriver"]=[
														"DerivedTypeString",
														"DerivingTypeString",
														"DerivingBaseTypeStringsList",
														"IsTypedBool"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setDerivingBaseTypeStringsListAfterBasesWithTypeStringDeriver(self):
		
		#Just use the set to have just one TypeString for each different Type
		self.DerivingBaseTypeStringsList=list(set(self.DerivingBaseTypeStringsList))

		#Bind With DerivedTypeString Setting
		self.setDerivedTypeStringWithTypeStringDeriver()

	def setDerivingTypeStringAfterBasesWithTypeStringDeriver(self):

		#Bind With DerivedTypeString Setting
		self.setDerivedTypeStringWithTypeStringDeriver()

	def setDerivedTypeStringAfterBasesWithTypeStringDeriver(self):

		#Bind with IsTypedBool Setting
		if self.DerivedTypeString=="":
			self['IsTypedBool']=False
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def setDerivedTypeStringWithTypeStringDeriver(self):
		self['DerivedTypeString']=SYS.getTypedStringWithTypeStringAndBaseTypeStringsList(self.DerivingTypeString,
			self.DerivingBaseTypeStringsList)
	#</DefineMethods>

#</DefineClass>

