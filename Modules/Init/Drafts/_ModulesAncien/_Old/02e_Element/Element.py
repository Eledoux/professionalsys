#<ImportModules>
import operator
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ElementClass(
					SYS.DictatedObjectClass,
					SYS.FamiliarizerClass,
					SYS.IdentifierClass,
					SYS.SetterClass,
					SYS.IndexClass,
					SYS.TaggerClass,
					SYS.NamedObjectClass
					):

	#<DefineHookMethods>
	def initAfterBasesWithElement(self):

		#<DefineSpecificDict>
		self.ElementVariable=None
		self.SkippedKeyStringsList+=[
									#"ElementVariable",
									"IdentityInt",
									#"ParentPointersList",
									"ParentPointer",
									"ParentTypeStringsList",
									"SkippedKeyStringsList"
							]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Element"]=[
													"ElementVariable"
												]

	def setBeforeBasesWithElement(self,_KeyVariable,_ValueVariable):
		'''
			<Help>
				Add an ElementVariable into the _ValueVariable
			</Help>

			<Test>
			</Test>
		'''
		if type(_KeyVariable) in [str,unicode]:

			#Special ElementVariable Setting
			if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Element",_ValueVariable) and SYS.getWordStringsListWithString(_KeyVariable)[-1]!="Pointer":
				_ValueVariable['ElementVariable']=_KeyVariable 

	def renameAfterBasesWithElement(self,_OldKeyVariable,_NewKeyVariable):
		if hasattr(self,_NewKeyVariable):

			#Get the NewNamedVariable
			NewNamedVariable=getattr(self,_NewKeyVariable)

			#Update the ElementString
			if hasattr(NewNamedVariable,"ElementVariable"):
				NewNamedVariable['ElementVariable']=_NewKeyVariable
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>


#</DefineClass>