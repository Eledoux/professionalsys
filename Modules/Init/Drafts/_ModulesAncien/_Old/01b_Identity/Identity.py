#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#Set the PluralString
PluralString="Identities"

#<DefineClass>
class IdentityClass(SYS.ObjectClass):

	#<Define HookMethods>
	def initAfterBasesWithIdentity(self):

		#<DefineSpecificDict>
		self.IdentityString=""
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Identity"]=[
												]

	#</Define HookMethods>

	#<Define Methods>
	#</Define HookMethods>

#</DefineClass>

