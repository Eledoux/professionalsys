#<Import Modules>
import operator
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class CursorClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithCursor(self):

		#<DefineSpecificDict>
		self.CursorIntsList=[]
		self.CursorLengthIntsList=[]
		self.CursorDiffIntsList=[]
		self.CursorInt=-1
		self.CursorDimensionInt=0
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
										"CursorDimensionInt",
										"setCursorLengthIntsListAfterBasesWithCursor"
									]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Cursor"]=[
													"CursorIntsList",
													"CursorLengthIntsList",
													"CursorInt"
													"CursorDimensionInt"
												]
	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	def setCursorLengthIntsListAfterBasesWithCursor(self):

		#Bind With CursorDimensionInt Setting
		self['CursorDimensionInt']=len(self.CursorLengthIntsList)

		#Bind With CursorIntsList Setting
		self['CursorIntsList']=[0 for Int in xrange(self.CursorDimensionInt)]

	def setCursorIntsListAfterBasesWithCursor(self):

		#Bind with CursorDiffIntsList Setting
		self.setCursorDiffIntsListWithCursor()

	#<DefineBindingHookMethods>

	#<DefineMethods>
	def setCursorIntsListWithShiftingInt(self,_ShiftingInt):

		"""
		print("avant",self.CursorInt,self.CursorIntsList,_ShiftingInt)

		#Get the new possible Cursor Int
		NewCursorInt=self.CursorIntsList[self.CursorInt]+_ShiftingInt

		#If the Space is enough just shift on the same Dimension
		if NewCursorInt<self.CursorLengthIntsList[self.CursorInt]:
			self.CursorIntsList[self.CursorInt]=NewCursorInt
		else:

			#Else Increment the CursorInt and Position in the CursorIntsList is the opposite of this value
			if -self.CursorInt<self.CursorDimensionInt:
				self.CursorInt+=1

				#Shift with the self.CursorShiftInt
				map(lambda Int:self.CursorIntsList.__setitem__(Int,0),xrange(-self.CursorInt+1,len(self.CursorIntsList)))
				self.CursorIntsList[-self.CursorInt]+=_ShiftingInt
				
		print("apres",self.CursorInt,self.CursorIntsList)
		"""

	def setCursorDiffIntsListWithCursor(self):

		#Compute the difference between CursorLengthIntsList and CursorIntsList
		self['CursorDiffIntsList']=map(operator.sub,self.CursorLengthIntsList,self.CursorIntsList)
	
	"""
	def getLengthInt(self):
		
		return self.CursorLengthIntsList[self.CursorInt]-self.CursorIntsList[self.CursorInt]
	"""
	#</DefineMethods>

#</DefineClass>