#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DictionnaryTypingDicterClass(
							SYS.DicterClass
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithDictionnaryTypingDicter(self):

		#<DefineSpecificDict>
		self.DictionnaryTyper=SYS.DicterPointeringTyperClass().update(
													{"DicterPointer":self}
													)
		#</DefineSpecificDict>

		#Update the DerivingTypeString
		self['DerivingTypeString']="Dictionnary"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["DictionnaryTypingDicter"]=[
													"DictionnaryTyper"
												]
	#</DefineHookMethods>

	#<DefineBindingMethods>	
	def setDerivedTypeStringAfterBasesWithDictionnaryTyperDicter(self):

		#Bind with DictionnaryTypeString setting
		self['DictionnaryTypeString']=self.DerivedTypeString
		
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

