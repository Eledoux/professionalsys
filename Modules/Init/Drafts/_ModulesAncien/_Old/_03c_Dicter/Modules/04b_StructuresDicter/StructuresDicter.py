#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StructuresDicterClass(SYS.InstancingDicterClass):
	
	#<DefineHookMethods>
	def initAfterBasesWithStructuresDicter(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the InstancedDictatedBaseTypeString
		self['InstancedDictatedBaseTypeString']="Structure"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuresDicter"]=[
												]
	#</DefineHookMethods>

	#<DefineBindingMethods>	
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

