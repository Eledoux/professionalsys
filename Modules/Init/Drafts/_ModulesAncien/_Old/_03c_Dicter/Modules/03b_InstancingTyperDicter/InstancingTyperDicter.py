#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancingTyperDicterClass(	
								SYS.InstancingDicterClass,
								SYS.TyperClass
							):
	
	#<DefineHookMethods>
	def initAfterBasesWithInstancingTyperDicter(self):

		#<DefineSpecificDict>
		self['DerivingTypeString']=self.InstancedDictatedBaseTypeString
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingTyperDicter"]=[
												]

	#</DefineHookMethods>

	#<DefineBindingMethods>
	def setDerivedTypeStringAfterBasesWithInstancingTyperDicter(self):

		#Bind with InstancedDictatedBaseTypeString
		self['InstancedDictatedBaseTypeString']=self.DerivedTypeString
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

