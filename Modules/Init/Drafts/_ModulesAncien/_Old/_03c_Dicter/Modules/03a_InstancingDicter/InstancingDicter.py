#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancingDicterClass(SYS.DicterClass):
	
	#<DefineHookMethods>
	def initAfterBasesWithInstancingDicter(self):

		#<DefineSpecificDict>
		self['InstancedDictatedBaseTypeString']="Object"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingDicter"]=[
													"InstancedDictatedBaseTypeString"
												]

		#Update the DictionnaryTypeString
		self['DictionnaryTypeString']="InstancingDictionnary"

	#</DefineHookMethods>

	#<DefineBindingMethods>
	def setInstancedDictatedBaseTypeStringAfterBasesWithInstancingDicter(self):

		#Get the possible DictionnaryTypeString
		DictionnaryTypeString=SYS.getPluralStringWithSingularString(self.InstancedDictatedBaseTypeString)+"Dictionnary"

		#Get the possible DictionnaryClassString
		ClassString=SYS.getClassStringWithTypeString(DictionnaryTypeString)

		#Bind with DictionnaryTypeString setting
		Class=getattr(SYS,ClassString)
		if Class!=None:
			self['DictionnaryTypeString']=DictionnaryTypeString
		else:

			#Bind with InstancedDictatedBaseTypeString in the Dictionnary setting
			getattr(self,self.DictionnaryKeyString)['InstancedDictatedBaseTypeString']=self.InstancedDictatedBaseTypeString

		#Bind With DictionnaryKeyString Setting
		self['DictionnaryKeyString']=SYS.getPluralStringWithSingularString(self.InstancedDictatedBaseTypeString)+"Dictionnary"
	
	def setDictionnaryKeyStringBeforeBasesWithInstancingDicter(self):

		#Bind With Temporary OldListKeyString Setting
		self.OldDictionnaryKeyString=self.DictionnaryKeyString
	
	def setDictionnaryKeyStringAfterBasesWithInstancingDicter(self):

		#Set the new ListKeyString
		if hasattr(self,'OldDictionnaryKeyString'):
			self.rename(self.OldDictionnaryKeyString,self.DictionnaryKeyString)

		#Delete the OldListedTypeString
		del self.OldDictionnaryKeyString
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

