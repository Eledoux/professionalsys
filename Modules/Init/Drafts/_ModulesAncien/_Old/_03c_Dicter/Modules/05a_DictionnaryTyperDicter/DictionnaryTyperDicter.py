#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DictionnaryTyperDicterClass(
							SYS.DicterClass,
							SYS.TyperClass
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithDictionnaryTyperDicter(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the DerivingTypeString
		self['DerivingTypeString']="DicterPointeringDictionnary"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["DictionnaryTyperDicter"]=[
												]
	#</DefineHookMethods>

	#<DefineBindingMethods>	
	def setDerivedTypeStringAfterBasesWithDictionnaryTyperDicter(self):

		#Bind with DictionnaryTypeString setting
		self['DictionnaryTypeString']=self.DerivedTypeString
		
	def setDictionnaryTypeStringAfterBasesWithDictionnaryTyperDicter(self):

		#Freeze the setInstancedTypeStringsListAfterBasesWithSmartLister Method
		self.FrozenMethodStringsList.append('setDerivedTypeStringAfterBasesWithTyperDicter')

		#Bind with DerivingTypeString setting
		self['DerivingTypeString']=self.DictionnaryTypeString

		#Reput the Hook
		self.FrozenMethodStringsList.pop(self.FrozenMethodStringsList.index('setDerivedTypeStringAfterBasesWithTyperDicter'))
	

	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

