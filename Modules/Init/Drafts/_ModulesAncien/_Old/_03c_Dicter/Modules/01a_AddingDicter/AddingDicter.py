#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class AddingDicterClass(SYS.DicterClass):
	
	#<DefineHookMethods>
	def addAfterBasesWithAddingDicter(self,_AddedVariable):
		if hasattr(_AddedVariable,"items"):
			self.update(_AddedVariable)
	#</DefineHookMethods>

	#<DefineBindingMethods>	
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

