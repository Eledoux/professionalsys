#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DictatedDicterClass(SYS.DicterClass):
	
	#<DefineHookMethods>
	def initAfterBasesWithDictatedDicter(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["DictatedDicter"]=[
												]
	#</DefineHookMethods>

	#<DefineBindingMethods>	
		
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

