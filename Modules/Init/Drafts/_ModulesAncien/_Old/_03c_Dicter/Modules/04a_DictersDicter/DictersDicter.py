#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DictersDicterClass(
							SYS.InstancingDicterClass
						):
	
	#<DefineHookMethods>
	def initAfterBasesWithDictersDicter(self):

		#<DefineSpecificDict>
		self['DicterDictionnariesTypeString']="DictersDictionnary"
		#</DefineSpecificDict>

		#Update the InstancedDictatedBaseTypeString
		self['InstancedDictatedBaseTypeString']="Dicter"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["DictersDicter"]=[
													"DicterDictionnariesTypeString"
												]

	def setAfterBasesWithDictersDicter(self,_KeyVariable,_ValueVariable):

		#Set the DictionnaryTypeString in this dictated Object
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[:2]=="x_":
					self[_KeyVariable[1:]].__setitem__('DictionnaryTypeString',self.DicterDictionnariesTypeString)

	#</DefineHookMethods>

	#<DefineBindingMethods>	
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

