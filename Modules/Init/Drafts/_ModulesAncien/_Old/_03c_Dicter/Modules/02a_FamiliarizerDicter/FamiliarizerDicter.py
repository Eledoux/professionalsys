#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class FamiliarizerDicterClass(
								SYS.DicterClass,
								SYS.FamiliarizerClass
							):

	#<DefineHookMethods>
	def initAfterBasesWithFamiliarizerDicter(self):

		#Update the DictionnaryTypeString
		self['DictionnaryTypeString']="FamiliarizerDictionnary"
	#</DefineHookMethods>

	#<DefineBindingMethods>
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

