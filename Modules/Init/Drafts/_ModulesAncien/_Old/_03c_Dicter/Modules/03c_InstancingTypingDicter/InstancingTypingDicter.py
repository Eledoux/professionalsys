#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancingTypingDicterClass(	
								SYS.InstancingDicterClass,
							):
	
	#<DefineHookMethods>
	def initAfterBasesWithInstancingTypingDicter(self):

		#<DefineSpecificDict>
		self.InstancingDicterPointeringTyper=SYS.InstancingDicterPointeringTyperClass().update({'InstancingTypingDicterPointer':self})
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingTypingDicter"]=[
													"InstancingDicterPointeringTyper"
												]

	#</DefineHookMethods>

	#<DefineBindingMethods>
	def setInstancedDictatedBaseTypeStringAfterBasesWithInstancingTypingDicter(self):

		#Freeze the setDerivedTypeStringAfterBasesWithInstancingDicterPointeringTyper Method
		self.InstancingDicterPointeringTyper.FrozenMethodStringsList.append('setDerivedTypeStringAfterBasesWithInstancingDicterPointeringTyper')

		#Bind with DerivingTypeString setting
		self.InstancingDicterPointeringTyper['DerivingTypeString']=self.InstancedListedBaseTypeString

		#Reput the Hook
		self.InstancingDicterPointeringTyper.FrozenMethodStringsList.pop(self.FrozenMethodStringsList.index('setDerivedTypeStringAfterBasesWithInstancingDicterPointeringTyper'))
	
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

