#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DicterClass(SYS.ObjectClass):
	
	#<DefineHookMethods>
	def initAfterBasesWithDicter(self):

		#<DefineSpecificDict>
		self.DictatedVariablesInt=0
		self.DictionnaryKeyString="VariablesDictionnary"
		self.DictionnaryTypeString="DicterPointeringDictionnary"
		self.VariablesDictionnary=SYS.DicterPointeringDictionnaryClass().update({'DicterPointer':self})
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"DictatedVariablesInt",
													"DictionnaryKeyString",
													"DictionnaryTypeString"
												]

	def getBeforeBasesWithDicter(self,_KeyVariable):

		#Wrap the Dictionnary "_" getting
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[0]=="_":
					Dictionnary=getattr(self,self.DictionnaryKeyString)
					if hasattr(Dictionnary,_KeyVariable):
						return Dictionnary[_KeyVariable] 

	def setBeforeBasesWithDicter(self,_KeyVariable,_ValueVariable):

		#Wrap the Dictionnary "_" getting
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[0]=="_":

					#Get the Dictionnary
					Dictionnary=getattr(self,self.DictionnaryKeyString)

					#Set the Dictionnary
					Dictionnary[_KeyVariable]=_ValueVariable
					
					#Set TypeStringToIsSettedBoolDict of Core to False
					return ("x"+_KeyVariable,_ValueVariable)

	def setAfterBasesWithDicter(self,_KeyVariable,_ValueVariable):

		#Wrap the Dictionnary "_" getting
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[0:2]=="x_":

					#Get the Dictionnary
					Dictionnary=getattr(self,self.DictionnaryKeyString)

					#Set the Dictionnary
					Dictionnary[_KeyVariable[1:]]=_ValueVariable

					#Bind with DictatedVariablesInt setting
					self['DictatedVariablesInt']=len(getattr(self,self.DictionnaryKeyString))

					#Return the _KeyVariable and the dictated Object
					return (_KeyVariable,Dictionnary[_KeyVariable[1:]])
	#</DefineHookMethods>

	#<DefineBindingMethods>
	def setDictionnaryTypeStringAfterBasesWithDicter(self):

		#Bind with the Dictionnary setting
		Dictionnary=getattr(self,self.DictionnaryKeyString)
		if Dictionnary.TypeString!=self.DictionnaryTypeString:

			#Keeps only the Dictated Objects
			self[self.DictionnaryKeyString]=SYS.getTypedVariableWithVariableAndTypeString(dict(Dictionnary.__iter__()),self.DictionnaryTypeString).update({'DicterPointer':self})		
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

