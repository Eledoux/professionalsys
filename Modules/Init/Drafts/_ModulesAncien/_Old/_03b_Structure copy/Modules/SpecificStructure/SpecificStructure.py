#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SpecificStructureClass(SYS.StructureClass,SYS.SpecificDatabaseClass):

	#<DefineHookMethods>
	def initAfterBasesWithSpecificStructure(self):
		
		#<DefineSpecificDict>
		self.StructureTypeString="SpecificStructure"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[	
		
										]	
			
	def addBeforeBasesWithSpecificStructure(self,_AddedVariable):

		#Special add for list structure like [ <something to add like in a Table>  ]
		if type(_AddedVariable)==list:

			if len(_AddedVariable)==1:

				if type(_AddedVariable[0])==list:

					#Do and Freeze the addBeforeBasesWithStructure Method
					self.addBeforeBasesWithStructure(_AddedVariable)
					self.FrozenMethodStringsList.append('addBeforeBasesWithStructure')

					#Bind with DatabasedTablesListedVariablesIntsList Setting
					self.DatabasedTablesListedVariablesIntsList=map(lambda Int:0,xrange(len(_AddedVariable[0])))
					self['DatabasedTablesTypeStringsList']=map(lambda TypeString: self.DatabasedTypeString if TypeString==self.TableTypeString else self.TableTypeString,self.InstancedTypeStringsList)
					
					#Transfer the Databased Type to each sub Structures
					map(lambda ListedTable,TypeString: ListedTable.__setitem__("DatabasedTypeString",self.DatabasedTypeString) if TypeString==self.StructureTypeString else None,getattr(self,self.ListKeyString),self.InstancedTypeStringsList)
	
	def addAfterBasesWithSpecificStructure(self,_AddedVariable):

		#Reput the Hook
		if 'addBeforeBasesWithStructure' in self.FrozenMethodStringsList:
			self.FrozenMethodStringsList.pop(self.FrozenMethodStringsList.index('addBeforeBasesWithStructure'))
	
	def setBeforeBasesWithSpecificStructure(self,_KeyVariable,_ValueVariable):

		#Case of DatabasedTablesListedVariablesIntsList
		if _KeyVariable=="DatabasedTablesListedVariablesIntsList":
			
			#Set without setitem DatabasedTablesListedVariablesIntsList
			self.DatabasedTablesListedVariablesIntsList=_ValueVariable
			
			#Do like if it is an add of a corresponding structure of empty dicts
			self+=SYS.getAddedSpecificStructureWithDatabasedTablesListedVariablesIntsList(self.DatabasedTablesListedVariablesIntsList)
			
			#Set IsSettingBool to False
			self.IsSettingBool=False
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

