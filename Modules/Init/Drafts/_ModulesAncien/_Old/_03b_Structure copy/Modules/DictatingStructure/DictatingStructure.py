#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DictatingStructureClass(SYS.StructureClass):

	#<DefineHookMethods>
	def initAfterBasesWithDictatingStructure(self):
		
		#<DefineSpecificDict>
		self.DicterTypeString="Dicter"
		self.DicterKeyString="VariablesDicter"
		self.VariablesDicter=SYS.DicterClass()
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[	
												"DicterTypeString",
												"DicterKeyString",
												"VariablesDicter"
										]
									
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

