#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StructureClass(SYS.AddingDatabaseClass):

	#<DefineHookMethods>
	def initAfterBasesWithStructure(self):
		
		#<DefineSpecificDict>
		self.TableTypeString="Table"
		self.StructureTypeString="Structure"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[	
		
										]	
						
	def addBeforeBasesWithStructure(self,_AddedVariable):

		#Special add in the database : add in new subTable or Structure 
		if type(_AddedVariable)==list:

			if len(_AddedVariable)==1:

				if type(_AddedVariable[0])==list:

					#Look if it is a List of List
					IsListBoolsList=map(lambda ListedVariable:SYS.getIsListListBool(ListedVariable),_AddedVariable[0])

					#Set the TypeStringsList
					InstancedTypeStringsList=map(lambda Bool:self.StructureTypeString if Bool else self.TableTypeString,IsListBoolsList)
					self['InstancedTypeStringsList']=InstancedTypeStringsList

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

