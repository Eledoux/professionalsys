#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancerClass(
					SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithInstancer(self):

		#<DefineSpecificDict>
		self.InstancingPointer=None
		self.InstancedTypeString=""
		self.InstancingKeyString=""
		self.InstancerString="From"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Instanc"]=[
													"InstancingPointer",
													"InstancedTypeString",
													"InstancingKeyString",
													"InstancerString"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setInstancedTypeStringBeforeBasesWithInstancer(self):

		#Delete the previous Instanc
		self.__delitem__(self.InstancingKeyString)

	def setInstancingPointerBeforeBasesWithInstancer(self):

		#Delete the previous Instanc
		self.__delitem__(self.InstancingKeyString)

	def setInstancedTypeStringAfterBasesWithInstancer(self):

		#Bind with InstancingKeyString setting
		self.setInstancingKeyStringWithInstancer()

	def setInstancingPointerAfterBasesWithInstancer(self):

		#Bind with InstancingKeyString setting
		self.setInstancingKeyStringWithInstancer()

	def setInstancingKeyStringAfterBasesWithInstancer(self):

		#Check that the InstancingTypeString is setted
		if self.InstancedTypeString!="":

			#Define the InstancedClassString
			InstancedClassString=SYS.getClassStringWithTypeString(self.InstancedTypeString)

			#Define the InstancedClass
			InstancedClass=getattr(SYS,InstancedClassString)

			#If the Class exists, build an instance of it
			if InstancedClass!=None:
				self[self.InstancingKeyString]=InstancedClass()

	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setInstancingKeyStringWithInstancer(self):

		if hasattr(self.InstancingPointer,"TypeString"):
			self['InstancingKeyString']=self.InstancerString+self.InstancingPointer.TypeString+self.InstancedTypeString
	#</DefineMethods>

#</DefineClass>
