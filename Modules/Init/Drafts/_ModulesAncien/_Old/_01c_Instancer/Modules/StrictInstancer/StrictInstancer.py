#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StrictInstancerClass(
					SYS.InstancerClass
				):

	#<DefineHookMethods>
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setBeforeBasesWithStrictInstancer(self,_KeyVariable,_ValueVariable):

		#Set the strict instancered Type string 
		if _KeyVariable in ["InstancedTypeString","InstancingPointer"]:

			#Set the attribute
			setattr(self,_KeyVariable,_ValueVariable)

			#Set the other things
			self.setWithStrictInstancer()

	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setWithStrictInstancer(self):

		#Set strictly
		if hasattr(self.InstancingPointer,"TypeString"):
			self.InstancedTypeString=self.InstancerString+self.InstancingPointer.TypeString+self.InstancedTypeString

		#rebind the InstancingKeyString setting
		self['InstancingKeyString']=self.InstancedTypeString

		#Set IsSettingBool to False
		self.IsSettingBool=False

	#</DefineMethods>

#</DefineClass>
