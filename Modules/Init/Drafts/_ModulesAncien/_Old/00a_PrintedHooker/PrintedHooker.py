#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>


#<DefineClass>
class PrintedHookerClass(SYS.HookerClass):

	#<Define HookMethods>
	def initAfterWithPrintedHooker(self):

		#<DefineSpecificDict>
		self.NotPrintedKeyStringsList=[
									"NotPrintedKeyStringsList",
									"TypeString",
									"AfterHookedTypeStringsList",
									"BeforeHookedTypeStringsList",
									"FrozenHookedMethodStringsList",
									"HookStringToMethodDict",
								]+["TypeStringToIs"+CommandedString+"BoolDict" for CommandedString in SYS.CommandStringToCommandedStringDict.values()]+[
								"Is"+CommandingString+"Bool" for CommandingString in SYS.CommandStringToCommandingStringDict.values()]
		#</DefineSpecificDict>

	def reprAfterWithPrintedHooker(self,_SelfString,_PrintedDict):

		#Define the NewPrintedDict
		NewPrintedDict=dict(self.items())

		#Define the new PrintedDict with SkippedKeyStringsList
		PrintedDict=dict(
						filter(
							lambda Item:
							Item[0] not in self.NotPrintedKeyStringsList,
							NewPrintedDict.items()
							)
						)

		#Define the new SelfString
		SelfString=SYS.getPrintedVariableStringWithPrintedVariableAndPrintingDict(PrintedDict,{
																					'AlineaString':"",
																					'LineJumpString':"\n",
																					'ElementString':"   /",
																					'IsPrintedKeysBool':True,
																					'IsPrintedValuesBool':True
																				}
																	)

		#Return
		return (SelfString,PrintedDict)	
	#</DefineHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

