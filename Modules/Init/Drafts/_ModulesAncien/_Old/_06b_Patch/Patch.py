#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#Init the Singular to Plural ExceptionNames Dict
PluralString="Patches"

#<DefineClass>
class PatchClass(SYS.NodeClass):

	#<DefineHookMethods>
	def initAfterBasesWithPatch(self):
		
		#<DefineSpecificDict>
		self.PatchedRow0Int=0
		self.PatchedRowsInt=5
		self.PatchedCol0Int=0
		self.PatchedColsInt=5
		self.PatcherPointer=None
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
										"NetworkPointer"
									]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
									
									"PatchedColsInt",
									"PatchedCol0Int",
									"PatchedRowsInt",
									"PatchedRow0Int"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setNetworkPointerAfterBasesWithPatch(self):

		#Bind With PatchedPointer Setting
		self['PatcherPointer']=self.NetworkPointer

	def setPatcherPointerAfterBasesWithPatch(self):

		#Get the MatrixPointer
		MatrixPointer=getattr(self.PatcherPointer,self.PatcherPointer.CoupledArrayKeyString)

		print(MatrixPointer.ArrayLengthIntsList)
		self.PatchedRow0Int=self.PatcherPointer.RowCursorInt
		self.PatchedCol0Int=self.PatcherPointer.ColCursorInt

		print((self.PatcherPointer.PatchedRowsInt-self.PatcherPointer.RowSeparationsInt*MatrixPointer.ArrayLengthIntsList[0])/MatrixPointer.ArrayLengthIntsList[0])
		print((self.PatcherPointer.PatchedColsInt-self.PatcherPointer.RowSeparationsInt*MatrixPointer.ArrayLengthIntsList[0])/MatrixPointer.ArrayLengthIntsList[1])

		#print(getattr(self.PatcherPointer,self.Patcher.CoupledArrayKeyString).ArrayLengthIntsList)
		#print(self.PatcherPointer)

	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

