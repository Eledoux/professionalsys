#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class FigureClass(SYS.GridClass):

	#<DefineHookMethods>
	def initAfterBasesWithFigure(self):
		
		#<DefineSpecificDict>
		import pylab
		self.PylabFigure=pylab.figure()
		#</DefineSpecificDict>

		#This is a Grid of Panels
		self['CoupledAgentTypeString']="Panel"

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
										"CoordinateVariablesList"
									]
		
		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
									"PylabFigure"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setColsIntAfterBasesWithFigure(self):
		self.setPylabFigure()
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def setPylabFigure(self):
		self['PylabFigure']=pylab.figure()
	#</DefineMethods>

#</DefineClass>

