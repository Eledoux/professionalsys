#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SpaceClass(SYS.SpecificStructureClass):

	#<DefineHookMethods>
	def initAfterBasesWithSpace(self):

		#<DefineSpecificDict>
		self['DatabasedTypeString']="MappedCoordinate"
		#</DefineSpecificDict>
	
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

