#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ScalingSpaceClass(SYS.SpaceClass):

	#<DefineHookMethods>
	def initAfterBasesWithScalingSpace(self):

		#<DefineSpecificDict>
		self['ScalingFloatsList']=[]
		#self.SpacingFunctor
		#</DefineSpecificDict>
	
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setScalingFloatsListAfterBasesWithScalingSpace(self):
		self.SpacingFunctor.FunctionPointer
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

