#<ImportModules>
import operator
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class TaggerClass(
					SYS.ObjectClass
					):

	pass
	"""
	#<DefineHookMethods>
	def initAfterBasesWithTagger(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>


		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Namer"]=[
													"NamedVariable"
												]

	def setBeforeBasesWithNamer(self,_KeyVariable,_ValueVariable):
		'''
			<Help>
				Add a NamedVariable into the _ValueVariable
			</Help>

			<Test>
			</Test>
		'''
		if type(_KeyVariable) in [str,unicode]:

			#Special NamedVariable setting
			if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("NamedDicter",_ValueVariable) and SYS.getWordStringsListWithString(_KeyVariable)[-1]!="Pointer":
				_ValueVariable['NamedVariable']=_KeyVariable 

	def renameAfterBasesWithNamer(self,_OldKeyVariable,_NewKeyVariable):
		if hasattr(self,_NewKeyVariable):

			#Get the NewNamedVariable
			NewNamedVariable=getattr(self,_NewKeyVariable)

			#Update the ElementString
			if hasattr(NewNamedVariable,"NamedVariable"):
				NewNamedVariable['NamedVariable']=_NewKeyVariable
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>
	"""


#</DefineClass>