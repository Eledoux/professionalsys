#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class UseClass(
					SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithUse(self):

		#<DefineSpecificDict>
		self.UsingTypeString=""
		self.UsedTypeString=""
		self.BeginUseString="Of"
		self.EndUseString="_"
		self.UseKeyString=""
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Use"]=[
													"UsingPointer",
													"UsedTypeString",
													"BeginUseString",
													"EndUseString",
													"UseKeyString"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setUsingTypeStringAfterBasesWithUse(self):

		#Bind with setUseKeyStringWithUse
		self.setUseKeyStringWithUse()

	def setUsedTypeStringAfterBasesWithUse(self):

		#Bind with setUseKeyStringWithUse
		self.setUseKeyStringWithUse()

	def setBeginUseStringAfterBasesWithUse(self):

		#Bind with setUseKeyStringWithUse
		self.setUseKeyStringWithUse()

	def setEndUseStringAfterBasesWithUse(self):

		#Bind with setUseKeyStringWithUse
		self.setUseKeyStringWithUse()
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setUseKeyStringWithUse(self):

		self['UseKeyString']=self.BeginUseString+self.UsingTypeString+self.EndUseString+self.UsedTypeString
	#</DefineMethods>

#</DefineClass>
