#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancingUseClass(
					SYS.UseClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithInstancingUse(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingUse"]=[
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setUseKeyStringAfterBasesWithInstancingUse(self):

		#Get the corresponding Class
		Class=getattr(SYS,SYS.getClassStringWithTypeString(self.UsedTypeString))

		#Instancify if the Class exists
		if Class!=None:
			self[self.UseKeyString]=Class()

	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
