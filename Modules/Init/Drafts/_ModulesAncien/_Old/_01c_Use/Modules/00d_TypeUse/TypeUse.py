#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class TypeUseClass(
					SYS.UseClass,
					SYS.TypeClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithTyperUse(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["TypeUse"]=[
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setUsingTypeStringAfterBasesWithTypeUse(self):

		#Freeze the setUsingTypeStringAfterBasesWithTypeUse Method
		self.FrozenMethodStringsList.append('setUsingTypeStringAfterBasesWithTypeUse')

		#Bind with TypingTypeString setting
		self['TypingTypeString']=self.UsingTypeString

		#Reput the Hook
		self.FrozenMethodStringsList.pop(self.FrozenMethodStringsList.index('setUsingTypeStringAfterBasesWithTypeUse'))

	def setTypingTypeStringAfterBasesWithTypeUse(self):

		#Bind with UsingTypeString setting
		self['UsingTypeString']=self.TypingTypeString

	def setTypedTypeStringAfterBasesWithTypeUse(self):

		#Bind with UsedTypeString setting
		self['UsedTypeString']=self.TypedTypeString
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
