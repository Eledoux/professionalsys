#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StrictInstancingUseClass(
					SYS.InstancingUseClass
				):

	#<DefineHookMethods>
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setBeforeBasesWithStrictInstancingUse(self,_KeyVariable,_ValueVariable):

		#Set the strict instancered Type string 
		if _KeyVariable in ["UsedTypeString","UsingTypeString"]:

			#Set the attribute
			setattr(self,_KeyVariable,_ValueVariable)

			#Set the other things
			self.setWithStrictInstancingUse()

	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setWithStrictInstancingUse(self):

		#Set strictly
		self.UsedTypeString=self.BeginUseString+self.UsingTypeString+self.EndUseString+self.UsedTypeString

		#rebind the InstancingKeyString setting
		self['UseKeyString']=self.UsedTypeString

		#Set IsSettingBool to False
		self.IsSettingBool=False

	#</DefineMethods>

#</DefineClass>
