#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class PointeringUseClass(
					SYS.UseClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithPointeringUse(self):

		#<DefineSpecificDict>
		self.UsingPointer=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["PointeringUse"]=[
													"UsingPointer"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setUsingPointerAfterBasesWithPointeringUse(self):

		#Bind with UsingTypeString setting
		if hasattr(self.UsingPointer,"TypeString"):
			self['UsingTypeString']=self.UsingPointer.TypeString
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
