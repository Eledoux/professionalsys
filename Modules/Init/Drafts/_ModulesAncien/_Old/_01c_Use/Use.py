#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class UseClass(
					SYS.TypeStringDeriverClass,
					SYS.VariableInstancerClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithUse(self):

		#<DefineSpecificDict>
		self.UserPointer=None
		self.UsedTypeString=""
		self.UsingTypeString=""
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Use"]=[
													"UserPointer"
													"UsedTypeString"
													"UsingTypeString"
												]

	def addAfterBasesWithUse(self,_AddedVariable):

		#Update in the InstancedVariable
		if hasattr(self.InstancedVariable,"items"):
			self.InstancedVariable+=_AddedVariable

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setUsingTypeStringAfterBasesWithUse(self):

		#Bind with InstancedVariable_UsingPointer setting
		self.setInstancedVariable_UsingPointerWithUse()

	def setUsedTypeStringAfterBasesWithUse(self):

		#Bind with InstancingTypeString
		self['InstancingTypeString']=self.UsedTypeString

	def setUserPointerAfterBasesWithUse(self):

		#Bind with UsingTypeString setting
		#if hasattr(self.UserPointer,"TypeString"):
		#	self['UsingTypeString']=self.UserPointer.TypeString

		#Bind with UsedTypeString setting
		self.setUsedTypeStringWithUse()

		#Bind with InstancedVariable_UsingPointer setting
		self.setInstancedVariable_UsingPointerWithUse()

	def setDerivedTypeStringAfterBasesWithUse(self):

		#Bind with UsedTypeString setting
		self.setUsedTypeStringWithUse()

	def setInstancedVariableAfterBasesWithUse(self):
		self.UsedTypeString=self.InstancedVariable.TypeString
		self.DerivingTypeString=self.InstancedVariable.TypeString
		self.DerivingTypeString=self.InstancedVariable.TypeString

	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setUsedTypeStringWithUse(self):
		if self.UsingTypeString!="":
			self['UsedTypeString']="Of"+self.UsingTypeString+"_"+self.DerivedTypeString

	def setInstancedVariable_UsingPointerWithUse(self):
		if self.UsingTypeString!="":
			if hasattr(self.InstancedVariable,
				"Using"+self.UsingTypeString+"Pointer"):
				self.InstancedVariable["Using"+self.UsingTypeString+"Pointer"]=self.UserPointer

	#</DefineMethods>

#</DefineClass>
