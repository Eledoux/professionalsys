The Patch is a "Rectangular" Node that 
has a 2D size given by its
PatchedRowsInt and PatchedColsInt
and PatchedRow0Int PatchedCol0Int as
its top-left corner anchor and cannot support 
any other patch overdressing its area