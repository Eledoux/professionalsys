#<ImportModules>
import numpy
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ShapeClass(SYS.AgentClass):

	#<DefineHookMethods>
	def initAfterBasesWithShape(self):
		
		#<DefineSpecificDict>
		self.ShapedLengthIntsList=[]
		self.ShapedIntsList=[]
		self.ShapedDimensionInt=0
		self.ShaperPointer=None
		self.StructurePointer=None
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
										"CoordinatedVariablesList",
										"CoordinatedIntsList",
										"CouplerPointer",
										"CursorPointer",
										"RankedIntsList",
										"ShaperPointer",
										"ShapedDimensionInt",
										"StructurePointer"
										#"NetworkPointer"
									]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
									"ShapedLengthIntsList",
									"ShapedIntsList"
												]

	def reprAfterBasesWithShape(self,_SelfString,_PrintedDict):
		
		#Define the Keeped Keys
		KeepedKeyStringsList=[
								#"ParentPointersList",
								#"CouplerPointer"
								]
		
		#Define the new PrintedDict
		_PrintedDict.update(dict(map(lambda KeyString:(KeyString,getattr(self,KeyString)),KeepedKeyStringsList)))

		#Define the new SelfString
		NewSelfString=SYS.ConsolePrinter.getPrintedVariableString(_PrintedDict)

		#Return
		return (NewSelfString,_PrintedDict)
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setCouplerPointerAfterBasesWithShape(self):

		#Bind With Shaper Setting
		self['ShaperPointer']=self.CouplerPointer

	def setShaperPointerAfterBasesWithShape(self):

		#Bind with ShaperPointer Setting
		self['StructurePointer']=getattr(self.ShaperPointer,self.ShaperPointer.CoupledStructureKeyString)

		#Bind with CursorPointer Setting
		self['CursorPointer']=self.ShaperPointer.ShapingCursor	

		#Bind with setShape
		self.setShape()



	def setShapedIntsListAfterBasesWithShape(self):

		#Bind with ShapedDimensionInt Setting
		self['ShapedDimensionInt']=len(self.ShapedIntsList)

	def setShapedLengthIntsListAfterBasesWithShape(self):

		#Bind with ShapedDimensionInt Setting
		self['ShapedDimensionInt']=len(self.ShapedLengthIntsList)

		
		self.setShapedDimensionIntWithShapeWithList(self.ShapedLengthIntsList)
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#def setShapedLengthIntsListWithShape(self):

	#	if 

	def setShape(self):

		#Give the ShapedIntsList from the CursorIntsList if it was not yet defined
		#self['ShapedIntsList']=map(lambda Int:Int,self.CursorPointer.CursorIntsList)

		self.ShapedLengthIntsList=map(lambda Int1,Int2:min(Int1,Int2),
					self.CursorPointer.CursorDiffIntsList,
					self.ShaperPointer.EqualShapedLengthIntsList)

		"""
		#
		if self.ShaperPointer.EqualInt==self.ShaperPointer.EqualShapesInt:
			self.ShaperPointer.EqualInt=0

		#Update the EqualInt of the ShaperPointer
		self.ShaperPointer.EqualInt+=1

		#Get the CursorInt
		CursorInt=self.CursorPointer.CursorInt

		#Get the ShapedLengthInt
		ShiftingInt=self.ShapedLengthIntsList[CursorInt]

		#Add the ShapingInt if it is not the last Shape in this dimension
		if self.ShaperPointer.EqualInt<self.ShaperPointer.EqualShapesInt-1:
			ShiftingInt+=self.ShaperPointer.ShapingIntsList[CursorInt]

		#Get the new possible Cursor Int
		NewCursorInt=self.CursorIntsList[self.CursorInt]+_ShiftingInt

		#If the Space is enough just shift on the same Dimension
		if NewCursorInt<self.CursorLengthIntsList[self.CursorInt]:
			self.CursorIntsList[self.CursorInt]=NewCursorInt
		else:

			#Else Increment the CursorInt and Position in the CursorIntsList is the opposite of this value
			if -self.CursorInt<self.CursorDimensionInt:
				self.CursorInt+=1

		#Shift with the self.CursorShiftInt
		map(lambda Int:self.CursorIntsList.__setitem__(Int,0),xrange(-self.CursorInt+1,len(self.CursorIntsList)))
		self.CursorIntsList[-self.CursorInt]+=_ShiftingInt

		#Shift the CursorInt
		print("jjj",self.ShaperPointer.EqualInt,self.ShaperPointer.EqualShapesInt,ShiftingInt)
		self.CursorPointer.setCursorIntsListWithShiftingInt(ShiftingInt)

		#Update the EqualInt of the ShaperPointer
		self.ShaperPointer.EqualInt+=1
		if self.ShaperPointer.EqualInt==self.ShaperPointer.EqualShapesInt:
			self.ShaperPointer.EqualInt=0
		"""
	#</DefineMethods>

#</DefineClass>

