#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class IdentifierClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithIdentifier(self):

		#<DefineSpecificDict>
		self.IdentityInt=id(self)
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Identifier"]=[
													"IdentityInt"
												]
	#</DefineHookMethods>

#<DefineClass>