#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class AgentClass(SYS.ElementClass):

	#<DefineHookMethods>
	def initAfterBasesWithAgent(self):

		#<DefineSpecificDict>
		self.CouplerPointer=None
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
									]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"CouplerPointer"
												]
	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	def setParentPointersListAfterBasesWithAgent(self):

		#Bind With the CouplerPointerSetting
		IsPointerBoolsList=map(lambda Pointer:SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Coupler",Pointer),self.ParentPointersList)
		if any(IsPointerBoolsList):
			self['CouplerPointer']=self.ParentPointersList[IsPointerBoolsList.index(True)]
	#</DefineBindingHookMethods>

#</DefineClass>