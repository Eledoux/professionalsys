#<ImportModules>
import operator
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class IndexClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithIndex(self):

		#<DefineSpecificDict>
		self.SorterPointersList=[]
		self.SortedIntsList=[]
		
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.SkippedKeyStringsList+=[
							]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Index"]=[
													"SortedIntsList",
													"SorterPointersList"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setParentPointersListAfterBasesWithIndex(self):

		#Bind With the SorterPointersList Setting
		self["SorterPointersList"]=filter(
			lambda Pointer:SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Sorter",Pointer),
			self.ParentPointersList)

	def setSorterPointersListAfterBasesWithIndex(self):

		#Bind with SortedIntsList Setting
		self['SortedIntsList']=[0]*len(self.SorterPointersList)

	def setSortedIntsListAfterBasesWithIndex(self):

		#Bind with each SorterPointer.SortedIntToSortedTableDict update
		map(lambda IntAndSorterPointer:IntAndSorterPointer[1].SortedIntToSortedTableDict.__setitem__(str(self.SortedIntsList[IntAndSorterPointer[0]]),self),enumerate(self.SorterPointersList))
	#<DefineBindingHookMethods>


#</DefineClass>