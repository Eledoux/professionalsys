#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>


#<DefineClass>
class PatcherClass(SYS.GridClass,SYS.PatchClass):

	#<DefineHookMethods>
	def initAfterBasesWithPatcher(self):

		#<DefineSpecificDict>
		self['CoupledAgentTypeString']="Patch"
		self.RowSeparationsInt=1
		self.ColSeparationsInt=1
		self.RowCursorInt=0
		self.ColCursorInt=0
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
									]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
									"PatchesMatrix"
												]								
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setArrayLengthIntsListAfterBasesWithPatcher(self):
		print('OOOO')
	#</DefineBindingHookMethods>
#</DefineClass>

