#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class BinderClass(SYS.CoreClass):

	#<Define HookMethods>
	def callBeforeBasesWithBinder(self,_CallString,*_ArgsList,**_KwargsDict):

		if _CallString in ["Set","Bind"]:
			
				#Call the specific Function For each Base
				for CalledTypeString in getattr(self,"BeforeBasesCalledTypeStringsList"):

					BindingHookMethodString="bind"+getKeyStringWithKeyVariable(_KeyVariable)+"BeforeBasesWith"+CalledTypeString 
					if hasattr(self,BindingHookMethodString):
						if BindingHookMethodString not in self.FrozenMethodStringsList:
							if callable(getattr(self,BindingHookMethodString)):

								#Call the BindingHookMethodString
								OutputVariable=getattr(self,BindingHookMethodString)()

								#End the Setting
								if OutputVariable==(None,None):
									return

								#Look if we have to modify the Item
								if OutputVariable!=None and type(OutputVariable)==tuple:
									_KeyVariable,_ValueVariable=OutputVariable
	#</Define HookMethods>

	#<Define Methods>
	#</Define HookMethods>

#</DefineClass>

