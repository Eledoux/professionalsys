#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class DoerClass(
					SYS.ObjectClass
				):

	#<Define HookMethods>
	def initAfterBasesWithDoer(self):

		#<DefineSpecificDict>
		self.IsDoingBool=True
		#</DefineSpecificDict>

	#<Define Methods>										
	def do(self,_DoString,*_ArgsList,**_KwargsDict):
		'''
			<Help>
				Generic Call Method for an Instance in ShareYourSystem
			</Help>

			<Test>
			</Test>
		'''

		#First set the maybe attributes contained in the _KwargsDict
		"""
		map(
				lambda ItemTuple:
				self.__setitem__(ItemTuple[0],ItemTuple[1])
				if self[ItemTuple[0]]!=None
				else None,
				_KwargsDict.items()
			)
		"""

		#Init the OutputVariable
		OutputVariable=None

		#Sort the OrderString Calls
		for OrderString in ["Before","After"]:
		
			#Call the specific Before setItem For each Base
			for CalledTypeString in getattr(self,OrderString+"BasesCalledTypeStringsList"):
				
				#Call the MethodString
				HookMethodString="do"+OrderString+"With"+CalledTypeString
				if hasattr(self,HookMethodString):
					if HookMethodString not in self.FrozenMethodStringsList:
						OutputVariable=getattr(self,HookMethodString)(_DoString,*_ArgsList,**_KwargsDict)

				#Modify the arguments
				if type(OutputVariable)==dict:
					if "_ArgsList" in OutputVariable:
						_ArgsList=OutputVariable["_ArgsList"]
					if "_KwargsDict" in OutputVariable:
						_KwargsDict=OutputVariable["_KwargsDict"]

				#End the Call
				if self.IsDoingBool==False:
					return OutputVariable

		#Reset the IsDoingBool
		self.IsDoingBool=True

		#Set the Transversal Bindings
		return OutputVariable
	#</Define HookMethods>

	
	#</Define HookMethods>

#</DefineClass>

