#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ExampleAClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithExampleA(self):
		
		#<DefineSpecificDict>
		self.ExampleAString="ExampleA"
		#</DefineSpecificDict>

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineMethods>

#</DefineClass>

