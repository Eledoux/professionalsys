#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineLocals>
DoString="Record"
DoingString="Recording"
DoneString="Recorded"
RecordingTagString="<Recording>"
RecordedTagString="<Recorded>"
#</DefineLocals>

#<DefineFunctions>
def getPrintedRecordingDict(_RecordingDict):

	#Define the NewRecordingDict
	return dict(
					map(
						lambda ItemTuple:
						#Special Recorded Key Print
						(ItemTuple[0],"<Recorded "+type(ItemTuple[1]).__name__+">")
						if SYS.getCommonPrefixStringWithStringsList(
						[ItemTuple[0],RecordedTagString]
						)==RecordedTagString
						else 
						#Recursive call for itemizing Variables
						(ItemTuple[0],getPrintedRecordingDict(ItemTuple[1]))
						if hasattr(ItemTuple[1],"items")
						#Else
						else ItemTuple,
						_RecordingDict.items()
					)
				)

#</DefineFunctions>

#<DefineClass>
class RecorderClass(SYS.UserClass):
    
	#<DefineHookMethods>
	def initAfterBasesWithRecorder(self):

		#<DefineSpecificDict>
		self.RecordingDict={}
		self.RecordedPointer=None
		#</DefineSpecificDict>

	def callAfterBasesWithRecorder(self,*_ArgsList,**_KwargsDict):

		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="Use":

				#Init a Dict in the RecordingDict for the UsedTypeString
				self.RecordingDict[_KwargsDict['UsedTypeString']]={}

		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']==DoString:

				#Reupdate possible attributes
				for KeyString in [
									'RecordedPointer',
									'RecordedKeyStringsList',
									'RecordingKeyStringsList',
									'RecordedString'
								]:
					if KeyString in _KwargsDict:
						self[KeyString]=_KwargsDict[KeyString]

				#Check that the RecordedPointer is not None
				if self.RecordedPointer!=None:

					#Record if IsRecordedBool is False
					if self.RecordedPointer.IsRecordedBool==False:

						#Record the recording KeyStrings
						map(
								lambda RecordingKeyString:
								self.RecordingDict[self.RecordedString].__setitem__(
													RecordingTagString+RecordingKeyString,
													getattr(self.RecordedPointer,RecordingKeyString)
												)
								if hasattr(self.RecordedPointer,RecordingKeyString)
								else None,
								self.RecordingKeyStringsList
							)
						
						#Record the recorded KeyStrings
						map(
								lambda RecordedKeyString:
								self.RecordingDict[self.RecordedString].__setitem__(
													RecordedTagString+RecordedKeyString,
													getattr(self.RecordedPointer,RecordedKeyString)
												)
								if hasattr(self.RecordedPointer,RecordedKeyString)
								else None,
								self.RecordedKeyStringsList
							)

						#Set the record to True
						self.RecordedPointer.IsRecordedBool=True

		#Return self
		return self

	def reprAfterBasesWithRecorder(self,_SelfString,_PrintedDict):

		#Define the NewPrintedDict
		NewPrintedDict=getPrintedRecordingDict(self.RecordingDict)

		#Define the new SelfString
		SelfString=SYS.ConsolePrinter.getPrintedVariableString(NewPrintedDict)

		#Return
		return (SelfString,NewPrintedDict)


    #</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>
#</DefineClass>


