#<ImportModules>
import sys
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class FilterClass(SYS.UserClass):

	#<DefineHookMethods>
	def initAfterWithFilter(self):

		#<DefineSpecificDict>
		"""
		self(
				**{
					'TypeDictString':"Use",
					'UsingTypeString':"Filter",
					'UsedTypeString':"Functor"
				}
			)
		"""
		self+={'UsedTypeString':"Functor",'UsingTypeString':"Filter"}
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Filter"]=[
												]
											
	def callAfterBasesWithFilter(self,*_ArgsList,**_KwargsDict):

		#Check that the _KwargsDict is ok
		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="Filter":
				if 'FilteredVariablesList' in _KwargsDict:

					#Call a filter for each Variable in the _ArgsList
					if callable(self['<Used>OfFilter_Functor']):
						return list(
									filter(
											lambda FilteredVariable:
											self['<Used>OfFilter_Functor'](FilteredVariable),
											list(_KwargsDict['FilteredVariablesList'])
											)
									)

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>


#</DefineClass>