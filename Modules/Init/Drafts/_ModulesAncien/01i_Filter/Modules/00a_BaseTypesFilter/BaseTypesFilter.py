#<ImportModules>
import sys
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class BaseTypesFilterClass(SYS.FilterClass):

	#<DefineHookMethods>
	def initAfterBasesWithBaseTypesFilter(self):

		#<DefineSpecificDict>
		self.FilteringBaseTypeStringsList=[]
		#</DefineSpecificDict>


		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["BaseTypesFilter"]=[
														"FilteringBaseTypeStringsList"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setFilteringBaseTypeStringsListAfterBasesWithBaseTypesFilter(self):

		#Update the FunctionPointer in the FromFilterFunctor
		if hasattr(self['<Used>OfFilter_Functor'],"FunctedFunctionPointer"):
			self['<Used>OfFilter_Functor'].FunctedFunctionPointer=lambda _FilteredVariable:self.getIsBaseTypeStringBoolWithBaseTypesFilterWithVariable(_FilteredVariable)

	#<DefineBindingHookMethods>

	#<DefineMethods>
	def getIsBaseTypeStringBoolWithBaseTypesFilterWithVariable(self,_FilteredVariable):

		if hasattr(_FilteredVariable,"TypeString"):

			#Set the BaseTypeStringsSetList of the _ValueVariable.TypeString
			DemandedTypeStringsSetList=[
					_FilteredVariable.TypeString
					]+SYS.TypeStringToBaseTypeStringsSetListLoadedDict[_FilteredVariable.TypeString]

			#Find where it is a BaseType of the FilteringBaseTypeStringsList
			return any(
						map(
						lambda FilteringBaseTypeString:
							FilteringBaseTypeString in DemandedTypeStringsSetList,
							self.FilteringBaseTypeStringsList
						)
					)

		return False
	#<DefineMethod>

#</DefineClass>