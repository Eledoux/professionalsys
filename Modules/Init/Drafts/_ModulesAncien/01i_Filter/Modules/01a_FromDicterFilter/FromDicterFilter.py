#<ImportModules>
import sys
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class FromDicterFilterClass(SYS.FilterClass):

	#<DefineHookMethods>
	def initAfterBasesWithFromDicterFilter(self):

		#<DefineSpecificDict>
		self.FilteringDicterPointer=None
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["FromDicterFilter"]=[
														"FilteringDicterPointer"
												]

	def callBeforeBasesWithFromDicterFilter(self,*_ArgsList,**_KwargsDict):

		#Just take the ValueVariable for being called by the Filter
		return {"_ArgsList":[_ArgsList[0][1]]}
		

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>

	#<DefineMethods>
	#<DefineMethod>

#</DefineClass>