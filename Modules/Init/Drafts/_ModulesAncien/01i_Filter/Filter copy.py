#<ImportModules>
import sys
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class FilterClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithFilter(self):

		#<DefineSpecificDict>
		self.FromFilterFunctor=SYS.FromFilterFunctorClass().update({"FunctoringFilterPointer":self})
		self.FromFilterFunctorTypeString="FromFilterFunctor"
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Filter"]=[
														"FromFilterFunctor",
														"FromFilterFunctorTypeString"	
												]
											
	def callAfterBasesWithFilter(self,*_ArgsList,**_KwargsDict):

		#Call a filter for each Variable in the _ArgsList
		return list(filter(lambda FilteredVariable:self.FromFilterFunctor(FilteredVariable),list(_ArgsList)))

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>


#</DefineClass>