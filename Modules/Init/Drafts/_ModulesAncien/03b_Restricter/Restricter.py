#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class RestricterClass(SYS.DicterClass):

	#<DefineHookMethods>
	def initAfterBasesWithRestricter(self):

		#<DefineSpecificDict>
		self.RestricteringFilter=SYS.FilterClass().update({'RestricterPointer':self})
		self.RestricteringFilterTypeString="Filter"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Restricter"]=[
													"RestricteringFilter",
													"RestricteringFilterTypeString"
												]

	def setBeforeBasesWithRestricter(self,_KeyVariable,_ValueVariable):

		#Case of setting in the Dictionnary
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[0]=="_":

					#Call the RestricteringFunctor
					IsDictatedBool=self.RestricteringFunctor(_KeyVariable,_ValueVariable)

					#Set if IsDictatedBool or not
					if IsDictatedBool:
						return (_KeyVariable,_ValueVariable)
					else:
						return ("x"+_KeyVariable,_ValueVariable)
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setRestricteringRestricterTypeStringAfterBasesWithRestricter(self):

		#Bind with RestricteringFunctor setting
		self.RestricteringFunctor=SYS.getTypedVariableWithVariableAndTypeString(
			self.RestricteringFunctor,self.RestricteringRestricterTypeString)

	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>
