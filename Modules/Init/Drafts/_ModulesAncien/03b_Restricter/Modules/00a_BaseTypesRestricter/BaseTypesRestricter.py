#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class BaseTypesRestricterClass(SYS.RestricterClass):

	#<DefineHookMethods>
	def initAfterBasesWithBaseTypesRestricter(self):

		#<DefineSpecificDict>
		self.RestricteringBaseTypeStringsList=["None"]
		#</DefineSpecificDict>

		#Update the FunctionPointer in the RestricteringFunctor
		self.RestricteringFunctor.FunctionPointer=lambda _KeyVariable,_ValueVariable:self.getIsDictatedBoolWithKeyVariableAndValueVariable(_KeyVariable,_ValueVariable)

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["BaseTypeRestricter"]=[
													"RestricteringBaseTypeStringsList"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def getIsDictatedBoolWithKeyVariableAndValueVariable(self,_KeyVariable,_ValueVariable):

		if hasattr(_ValueVariable,"TypeString"):

			#Set the BaseTypeStringsSetList of the _ValueVariable.TypeString
			DemandedTypeStringsSetList=[
					_ValueVariable.TypeString
					]+SYS.TypeStringToBaseTypeStringsSetListLoadedDict[_ValueVariable.TypeString]

			#Find where it is a BaseType of the RestricteringBaseTypeStringsList
			return any(
						map(
						lambda RestricteringBaseTypeString:
							RestricteringBaseTypeString in DemandedTypeStringsSetList,
							self.RestricteringBaseTypeStringsList
						)
					)

		return False
	#</DefineMethods>

#</DefineClass>
