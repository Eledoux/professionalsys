#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ParentClass(SYS.ObjectClass):

	#<Define HookMethods>
	def setBeforeBasesWithParent(self,_KeyVariable,_ValueVariable):
		'''
			<Help>
				Give to a Child Setted Item a Pointer to the Parent
			</Help>

			<Test>
				#Load ShareYourSystem as SYS
				import ShareYourSystem as SYS
				#
				#
				#Define a GrandParent
				GrandParent=SYS.ParentClass();
				#
				#Define a ParentChild Instance
				class local(SYS.ParentClass,SYS.ChildClass):pass;
				Parent=local();
				#
				#Define a Child
				Child=SYS.ChildClass();
				#
				#Print the GrandParent before setting
				print("The GrandParent before setting is :");
				print(GrandParent);
				#
				#Print the Parent before setting
				print("The Parent before setting is :");
				print(Parent);
				#
				#Print the Child before setting
				print("\nThe Child before setting is :");
				print(Child);
				#
				#Set the relationship
				GrandParent["MyChild"]=Parent
				Parent["MyChild"]=Child
				#
				#
				#Print the GrandParent after setting
				print("\nThe GrandParent after setting is :");
				print(GrandParent);
				#
				#Print the Parent after setting
				print("\nThe Parent after setting is :");
				print(Parent);
				#
				#Print the Child after setting
				print("\nThe Child after setting is :");
				print(Child);
			</Test>
		'''

		#<Debug>
		#DebugString="Start"
		#DebugString+=SYS.ConsoleDebugger['Indent']['LineJumpString']+"_KeyVariable is : "+SYS.ConsoleDebugger.getPrintedVariableString(_KeyVariable)
		#DebugString+=SYS.ConsoleDebugger['Indent']['LineJumpString']+"_ValueVariable is : "+SYS.ConsoleDebugger.getPrintedVariableString(_ValueVariable)
		#SYS.debug(DebugString)
		#</Debug>
		
		#Check that the ValueVariable is iterable
		if SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Child",_ValueVariable):
			
			#Check that it is not a circular setting
			if SYS.getWordStringsListWithString(_KeyVariable)[-1]!="Pointer":
			
				#<Debug>
				#SYS.debug("_ValueVariable has the ChildClass")
				#</Debug>

				#Set a Pointer
				_ValueVariable['ParentPointer']=self

		#Return the modified SettedItem
		return (_KeyVariable,_ValueVariable)
	#</Define HookMethods>
#</DefineClass>