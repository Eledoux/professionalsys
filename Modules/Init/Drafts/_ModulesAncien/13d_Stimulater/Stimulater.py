#<Import Modules>
import ShareYourSystem as SYS
import scipy
np=SYS.sys.modules['numpy']
#</Import Modules>

#<DefineLocals>
DoString="Stimulate"
DoingString="Stimulating"
DoneString="Stimulated"
#</DefineLocals>

#<DefineClass>
class StimulaterClass(SYS.ObjectClass):
    
	#<DefineHookMethods>
	def initAfterBasesWithStimulater(self):
        
		#<DefineSpecificDict>
		self.WaveLengthFloat=0.8                 #(micro meters)
		self.NumericalApertureFloat=0.9
		self.PixelsInt=512
		self.OverSamplingFloat=3.0 # <= 1
		self.TargetString="Spot"
		self.PhotonString="Mono"
		self.GerchbergSaxtonIterationsInt=20
		self.FourierFrequencyStepFloat=1.
		self.FourierSpaceStepFloat=1.
		self.GerchbergSaxtonString="Holographic"
		#self.PlanesInt=256
		self.PlanesInt=5
		self.PlanesStepFloat=0.25                #(micro meters)
		#</DefineSpecificDict>
        
		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Stimulater"]=[]

	def callAfterBasesWithStimulater(self,*_ArgsList,**_KwargsDict):

		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']==DoString:
		
				#Variables Initiation
				self.SpaceStepFloat=self.getSpaceStepFloatWithStimulater()

				#Set the IndexInt in the X,Y Axis
				XIndexIntsArray,YIndexIntsArray=np.indices((self.PixelsInt,self.PixelsInt))-self.PixelsInt/2

				#Set the PupilArray
				self.PupilArray=np.zeros((self.PixelsInt,self.PixelsInt))
				self.PupilArray[np.where(np.hypot(XIndexIntsArray,YIndexIntsArray)/(self.PixelsInt/2.0)<1.0/self.OverSamplingFloat)]=1.0

				#Set the TargetArray
				self.TargetArray=np.zeros((self.PixelsInt,self.PixelsInt))
				if self.TargetString=="Spot":
					self.TargetArray[np.where(np.hypot(XIndexIntsArray,YIndexIntsArray)*self.SpaceStepFloat<5.0)]=1
				if self.TargetString=="Uniform":
					self.TargetArray=np.ones((self.PixelsInt,self.PixelsInt))
				if self.TargetString=="Alaska":
					self.TargetArray=1-np.imread("alaska.png")[:,:,0]

				#Set the SLMArray
				self.setSLMArray()

				#Set the RebArray
				if self.GerchbergSaxtonString=="Holographic":
					self.RebArray=self.get2dFourierTransform(self.SLMArray)
				elif self.GerchbergSaxtonString=="GPC":
					self.RebArray=self.TargetArray  

				#Set the FourierSourceArray
				self.setFourierSourceArray()

		#Return self
		return self
    #</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def get2dFourierTransform(self,_Array):
   		return np.fft.fftshift(
   				np.fft.fft2(
   					np.fft.fftshift(_Array)
   						)
   					)*self.FourierSpaceStepFloat**2

	def get2dInverseFourierTransform(self,_Array):
		return np.fft.ifftshift(
    		np.fft.ifft2(
    			np.fft.ifftshift(_Array)
    			)
    		)*(np.shape(_Array)[0]*self.FourierFrequencyStepFloat)**2

	def getSpaceStepFloatWithStimulater(self):
		return self.WaveLengthFloat/(2.0*self.NumericalApertureFloat)/self.OverSamplingFloat
	
	def setSLMArray(self):

		for IterationInt in xrange(self.GerchbergSaxtonIterationsInt):

			#Print GS IterationInt
			print("GS Iteration",IterationInt)

			#Init case or not
			if IterationInt==0:
				InitialArray=2.*np.pi*scipy.rand(self.PixelsInt,self.PixelsInt)
			else:
				InitialArray=np.angle(
									self.get2dFourierTransform(self.SLMArray)
									)

			#Define the PhaseArray
			PhaseArray=np.angle(
			   			self.get2dInverseFourierTransform(
			   				self.TargetArray*np.exp(1j*InitialArray)
			   					)
			   				)

			#Determine the updated SLMArray
			self.SLMArray=self.PupilArray*np.exp(1j*PhaseArray)

	def getFresnelPropagationArrayWithZFloat(self,_ZFloat,_MomentFloat=1.0):

		#Set Fourier Space
		WaveFrequencyFloat=2*np.pi/self.WaveLengthFloat
		FourierFrequencyStepFloat=1.0/(self.PixelsInt*self.SpaceStepFloat)
		fx,fy=(np.indices(
			(self.PixelsInt,self.PixelsInt))-self.PixelsInt/2.0
		)*self.FourierFrequencyStepFloat
		Q2=np.exp(
			-1j*np.pi**2*2*_ZFloat/_MomentFloat/WaveFrequencyFloat*(fx**2+fy**2)
			)

		#Compute depending on the MomentFloat
		if _MomentFloat==1:
			FresnelPropagationArray=self.get2dInverseFourierTransform(
				Q2*self.get2dFourierTransform(
					self.RebArray)
					)
		else:    
			x,y=(np.indices((self.PixelsInt,self.PixelsInt))-self.PixelsInt/2.0)*self.SpaceStepFloat
			Q1=np.exp(1j*k/2.*(
				1.-_MomentFloat)/_ZFloat*(x**2+y**2)
			)
			Q3=np.exp(1j*WaveFrequencyFloat/2.*(
				_MomentFloat-1.
				)/(_MomentFloat*_ZFloat)*(_MomentFloat**2*(x**2+y**2)))
			FresnelPropagationArray=Q3*self.get2dInverseFourierTransform(
				Q2*self.get2dFourierTransform(Q1*self.RebArray/_MomentFloat
					),self.FourierFrequencyStepFloat
				)  

		#Return FresnelPropagationArray	
		return FresnelPropagationArray

	def setFourierSourceArray(self):

		#Set the FourierSourceArray
		SourceArray=np.zeros((self.PlanesInt,self.PixelsInt,self.PixelsInt),dtype=np.float32)
		for PlaneInt in xrange(self.PlanesInt):
			
			#Print Fresnel Index
			print("Fresnel Index",PlaneInt)

			#Propagate
			ZFloat=(PlaneInt-self.PlanesInt/2.0)*self.PlanesStepFloat
			if (ZFloat==0):
				SourceArray[PlaneInt,:,:]=abs(self.RebArray)
			else:
				SourceArray[PlaneInt,:,:]=abs(self.getFresnelPropagationArrayWithZFloat(ZFloat))

		#Get the Fourier equivalent
		self.FourierSourceArray=np.fft.fftshift(np.fft.fftn(np.fft.fftshift(SourceArray)))

	def plotStimulater():
		matplotlib.pyplot.figure(1)
		ax=matplotlib.pyplot.subplot(221);matplotlib.pyplot.imshow(PupilArray,cmap="gray");ax.set_title("pupil")
		ax=matplotlib.pyplot.subplot(222);matplotlib.pyplot.imshow(TargetArray,cmap="gray");ax.set_title("target")
		ax=matplotlib.pyplot.subplot(223);matplotlib.pyplot.imshow(np.angle(SLMArray),cmap="hsv")
		ax=matplotlib.pyplot.subplot(224);matplotlib.pyplot.imshow(abs(RebArray),cmap="gray")
		matplotlib.pyplot.show()

	#</DefineMethods>
#</DefineClass>


