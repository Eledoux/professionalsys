#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class OfRecorder_StimulaterClass(
									SYS.RecordedObjectClass,
									SYS.StimulaterClass
								):
    
	#<DefineHookMethods>
	def initAfterBasesWithOfRecorder_Stimulater(self):

		self.StimulatingStringsList=[
										"PixelsInt",
										"OverSamplingFloat",
										"WaveLengthFloat",
										"NumericalApertureFloat",
										"OverSamplingFloat",
										"TargetString",
										"GerchbergSaxtonIterationsInt",
										"FourierFrequencyStepFloat",
										"FourierSpaceStepFloat",
										"GerchbergSaxtonString",
										"PhotonString",
										"PlanesInt",
										"PlanesStepFloat"
									]
		self.StimulatedStringsList=[
										"SpaceStepFloat",
										"SLMArray",
										"FourierSourceArray"
									]

	#</DefineHookMethods>

	#<DefineHookMethods>
	

	#</DefineHookMethods>

#</DefineClass>


