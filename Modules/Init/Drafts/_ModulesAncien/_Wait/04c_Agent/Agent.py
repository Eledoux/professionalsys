#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class AgentClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithAgent(self):

		#<DefineSpecificDict>
		self.Toolbox=SYS.ToolboxClass().update({'AgentPointer':self})
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Agent"]=[
												]

	#</DefineHookMethods>
			
#</DefineClass>