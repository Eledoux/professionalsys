#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class BrianedConnectionClass(SYS.ConnectionClass):

	#<DefineHookMethods>
	def initBeforeBasesWithBrianedConnection(self):
		self.BrianConnection=None
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setPrePointerAfterBasesWithBrianedConnection(self):
		self.setBrianConnection()
	def setPostPointerAfterBasesWithBrianedConnection(self):
		self.setBrianConnection()	
	#<DefineBindingHookMethods>

	#<DefineMethods>
	def setBrianConnection(self):
		import brian
		if None not in [self.PrePointer,self.PostPointer]:
			if None not in [self.PrePointer.BrianNeuronGroup,self.PostPointer.BrianNeuronGroup]:
				self['BrianConnection']=brian.Connection(self.PrePointer.BrianNeuronGroup,self.PostPointer.BrianNeuronGroup)
	#<DefineMethods>

#</DefineClass>

