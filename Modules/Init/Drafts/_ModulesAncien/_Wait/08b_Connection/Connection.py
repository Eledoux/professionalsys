#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ConnectionClass(SYS.InteractionClass):
	#<DefineHookMethods>
	def initAfterBasesWithConnection(self):
		#<DefineSpecificDict>
		self.PrePointer=None
		self.PostPointer=None
		self.SkippedKeyStringsList+=["InteractingPointersList","PermutationString"]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"AlineaString",
													"LineJumpString",
													"ElementString"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setInteractingPointersListAfterBasesWithConnection(self):

		#Only for the Permutater of second order
		if len(self.InteractingPointersList)==2:

			#Bind With the PrePointer Setting
			self['PrePointer']=self.InteractingPointersList[0]

			#Bind With the PostPointer Setting
			self['PostPointer']=self.InteractingPointersList[1]

	#<DefineBindingHookMethods>


#</DefineClass>