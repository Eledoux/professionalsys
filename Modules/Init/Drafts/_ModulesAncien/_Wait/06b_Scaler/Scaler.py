#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ScalerClass(SYS.MapperClass):

	#<DefineHookMethods>
	def initAfterBasesWithScaler(self):

		#<DefineSpecificDict>
		self.ScalingFloat=0.
		self['MappedFunctor']=SYS.FunctorClass().update({'FunctionPointer':lambda x:self.ScalingFloat*x})
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"ScalingFloat"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setScalingFloatAfterBasesWithScaler(self):

		#Bind with the FunctedListedVariablesList setting
		self.setFunctedListedVariablesListWithMapper()
	
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>