#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class TableClass(SYS.RankedListerClass):

	#<DefineHookMethods>
	def reprAfterBasesWithTable(self,_SelfString,_PrintedDict):

		#Define the Filtering KeyStrings
		KeyStringsList=[
							"PreviousSortedVariablesIntsList",
							"SortedVariablesInt",
							"NextSortedVariablesIntsList",
							"SortedIntsList",
							"ElementVariable",
							self.ListKeyString
						]
		
		#Set the NewPrintedDict
		NewPrintedDict=dict(filter(lambda Item:Item!=None,map(lambda KeyString:(KeyString,_PrintedDict[KeyString]) if KeyString in _PrintedDict else None,KeyStringsList)))

		#Define the new SelfString
		NewSelfString=SYS.ConsolePrinter.getPrintedVariableString(NewPrintedDict)

		#Return 
		return NewSelfString,NewPrintedDict
	#</DefineHookMethods>
			
#</DefineClass>