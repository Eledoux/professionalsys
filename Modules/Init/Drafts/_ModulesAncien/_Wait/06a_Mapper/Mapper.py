#<ImportModules>
import sys
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class MapperClass(SYS.FunctorClass,SYS.ListerClass):

	#<DefineHookMethods>
	def initAfterBasesWithMapper(self):

		#<DefineSpecificDict>
		self.MappedFunctor=None
		self.FunctedListedVariablesList=[]
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"ListingFunctionPointer",
													"FunctedListedVariablesList"
												]

	def setAfterBasesWithMapper(self,_KeyVariable,_ValueVariable):

		#Bind with the FunctedListedVariablesList setting
		if _KeyVariable in [self.ListKeyString,"MappedFunctor"]:
			self.setFunctedListedVariablesListWithMapper()
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setMappedFunctorAfterBasesWithMapper(self):

		#Bind with FunctionPointer binding
		self['FunctionPointer']=lambda _List:SYS._map(lambda ListedVariable:self.MappedFunctor(ListedVariable),_List)

	#<DefineBindingHookMethods>

	#<DefineMethods>
	def setFunctedListedVariablesListWithMapper(self):
		self['FunctedListedVariablesList']=self(getattr(self,self.ListKeyString))
	#<DefineMethods>

#</DefineClass>