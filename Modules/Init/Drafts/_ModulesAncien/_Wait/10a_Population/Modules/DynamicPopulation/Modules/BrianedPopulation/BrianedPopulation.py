#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class BrianedPopulationClass(SYS.DynamicPopulationClass):
	
	#<DefineHookMethods>
	def initBeforeBasesWithBrianedPopulation(self):
		#<DefineSpecificDict>
		self.BrianNeuronGroup=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"BrianNeuronGroup"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setUnitsIntAfterBasesWithBrianedPopulation(self):
		self.setBrianNeuronGroup()

	def setExpressionStringAfterBasesWithBrianedPopulation(self):
		self.setBrianNeuronGroup()
	#<DefineBindingHookMethods>

	#<DefineMethods>
	def setBrianNeuronGroup(self):
		#Bind With BrianNeuronGroup Setting
		import brian
		self['BrianNeuronGroup']=brian.NeuronGroup(self.UnitsInt,self.ExpressionString)
	#<DefineMethods>
#</DefineClass>