#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

FlagStringsList=["ConstantTime","Partial","Leak","VariableDimension","TimeDimension"]
ExpressionFlagsDict={}
for FlagString in FlagStringsList:
	ExpressionFlagsDict[FlagString]={
										"BeginString":"<"+FlagString+">",
										"EndString":"</"+FlagString+">"
									}

#<DefineClass>
class DynamicPopulationClass(SYS.PopulationClass):
	#<DefineHookMethods>
	def initAfterBasesWithDynamicPopulation(self):
		#<DefineSpecificDict>
		self.VariableString="v"
		self.VariableDimensionString="mV"
		self.ConstantTimeFloat=20.
		self.ExpressionString=""
		self['FlaggedExpressionString']=self.getFlaggedExpressionString()
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"NodesArray",
													"ConnectionsTable"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setFlaggedExpressionStringAfterBasesWithDynamicPopulation(self):
		ExpressionString=self.FlaggedExpressionString
		for FlagString,TagDict in ExpressionFlagsDict.items():
			for TagString in ['BeginString','EndString']:
				ExpressionString=ExpressionString.replace(TagDict[TagString],"")
		self['ExpressionString']=ExpressionString
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def getFlaggedExpressionString(self):
		
		#Init FlaggedExpressionString
		FlaggedExpressionString=""

		#Add Partial
		FlaggedExpressionString+="<Partial>d"+self.VariableString+"/dt</Partial>"

		#Equality and Numerator Separator
		FlaggedExpressionString+="=("

		#Add Leak
		FlaggedExpressionString+="<Leak>-"+self.VariableString+"</Leak>"

		#Numerator Separator
		FlaggedExpressionString+=")/("

		#Add ConstantTime
		FlaggedExpressionString+="<ConstantTime>self.ConstantTimeFloat</ConstantTime>"

		#Time Dimension Separator
		FlaggedExpressionString+=" * "

		#Add TimeDimension
		TimeDimensionString=self.NetworkPointer.TimeDimensionString if self.NetworkPointer!=None else "ms"
		FlaggedExpressionString+="<TimeDimension>"+TimeDimensionString+"</TimeDimension>"

		#Denominator Separator And Dimension Separator
		FlaggedExpressionString+=") : "

		#Add Dimension
		FlaggedExpressionString+="<VariableDimension>"+self.VariableDimensionString+"</VariableDimension>"

		#Return 
		return FlaggedExpressionString
	#</DefineMethods>
#</DefineClass>