#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class PopulationClass(SYS.NodeClass):
	#<DefineHookMethods>
	def initAfterBasesWithPopulation(self):
		#<DefineSpecificDict>
		self.UnitsInt=0
		#</DefineSpecificDict>
	#</DefineHookMethods>
#</DefineClass>