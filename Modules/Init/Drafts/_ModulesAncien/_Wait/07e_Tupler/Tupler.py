#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class TuplerClass(
					#SYS.SpecificDatabaseClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithTupler(self):

		#<DefineSpecificDict>
		self.TupledAgentsInt=0
		self["DemandedBaseTypeString"]="Permutater"
		self['TupledDegreeIntsList']=[]
		#<DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"TupledDegreeIntsList",
													"TupledAgentsInt"
												]

	def getBeforeBasesWithTupler(self,_KeyVariable):

		#Special Permutater CoordinateIntsList Getting
		if type(_KeyVariable) in [str,unicode]:
			if "-" in _KeyVariable:
				
				#Split the _KeyVariable
				KeyVariableStringsList=_KeyVariable.split("-")

				#Get the DegreeInt
				DegreeInt=len(KeyVariableStringsList)

				#Find the corresponding Permutater
				if DegreeInt in self.TupledDegreeIntsList:
					
					#Return
					return getattr(self,self.ListKeyString)[self.TupledDegreeIntsList.index(DegreeInt)][_KeyVariable]


	def reprAfterBasesWithTupler(self,_SelfString,_PrintedDict):
		
		#Define the Keeped Keys
		KeepedKeyStringsList=[
								"TupledDegreeIntsList",
								]
		
		#Define the new PrintedDict
		_PrintedDict.update(dict(map(lambda KeyString:(KeyString,getattr(self,KeyString)),KeepedKeyStringsList)))

		#Define the new SelfString
		NewSelfString=SYS.ConsolePrinter.getPrintedVariableString(_PrintedDict)

		#Return
		return (NewSelfString,_PrintedDict)
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setTupledAgentsIntAfterBasesWithTupler(self):

		#Send the Number of Agents in each Permutater
		map(lambda Permutater:
			Permutater.__setitem__("PermutatedElementsInt",self.TupledAgentsInt),
			getattr(self,self.ListKeyString))

	def setTupledDegreeIntsListAfterBasesWithTupler(self):

		#Adapt the number of Permutaters
		self['DatabasedTablesListedVariablesIntsList']=[0]*len(self.TupledDegreeIntsList)

		#Give for each the TupledDegreeInt
		map(lambda IntAndPermutater:
			IntAndPermutater[1].__setitem__("PermutationDegreeInt",self.TupledDegreeIntsList[IntAndPermutater[0]]),
			enumerate(getattr(self,self.ListKeyString)))
			
	#</DefineBindingHookMethods>
#</DefineClass>