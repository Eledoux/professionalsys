#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class PermutaterClass(
						SYS.SpecificInstancingListerClass
					):

	#<DefineHookMethods>
	def initAfterBasesWithPermutater(self):

		#<DefineSpecificDict>
		self.PermutationDegreeInt=0
		self.PermutatedVariablesInt=0
		#</DefineSpecificDict>

		#Permutater is a Table of Permutations
		self['SpecificInstancedListedBaseTypeString']="Permutation"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"PermutationDegreeInt",
													"PermutatedVariablesInt"
													"PermutationStringToPermutationPointerDict"
												]

	def getBeforeBasesWithPermutater(self,_KeyVariable):

		#Get the corresponding Permutation for a PermutationString
		if type(_KeyVariable) in [str,unicode]:
			if "-" in _KeyVariable:

				#Define the KeyVariableStringsList
				KeyVariableStringsList=_KeyVariable[:-1].split("-")

				#Check that the PermutationStiring has the good length
				if len(KeyVariableStringsList)==self.PermutationDegreeInt:

					#Check that this Permutation exists
					if _KeyVariable in self.PermutationStringToPermutationPointerDict:

						#Return
						return self.PermutationStringToPermutationPointerDict[_KeyVariable]

	def reprAfterBasesWithPermutater(self,_SelfString,_PrintedDict):
		
		#Define the Keeped Keys
		KeepedKeyStringsList=[
								"PermutationDegreeInt",
								"PermutatedVariablesInt",
								#"PermutationStringToPermutationPointerDict"
								]
		
		#Define the new PrintedDict
		_PrintedDict.update(dict(map(lambda KeyString:(KeyString,getattr(self,KeyString)),KeepedKeyStringsList)))

		#Define the new SelfString
		NewSelfString=SYS.ConsolePrinter.getPrintedVariableString(_PrintedDict)

		#Return
		return (NewSelfString,_PrintedDict)

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setPermutationDegreeIntAfterBasesWithPermutater(self):
		self.setContentWithPermutater()

	def setPermutatedVariablesIntAfterBasesWithPermutater(self):
		self.setContentWithPermutater()
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setContentWithPermutater(self):
	
		#Set the StringsList to be permutated
		StringsList=map(lambda Int:str(Int),xrange(self.PermutatedVariablesInt))
		
		#Set the PermutationStringsList thanks to itertools
		PermutationStringsList=map(lambda PermutationTuple:'-'.join(list(PermutationTuple)),SYS.sys.modules['itertools'].product(StringsList,repeat=self.PermutationDegreeInt))
		
		if PermutationStringsList!=['']:

			#Adapt the size
			self["ListedVariablesInt"]=len(PermutationStringsList)
	
			#Update the PermutaterPointer
			map(lambda ListedVariable:
				ListedVariable.__setitem__(
					"PermutaterPointer",
					self
					),
				getattr(self,self.ListKeyString))

			#Update the PermutationString
			map(lambda IntAndVariable:
				IntAndVariable[1].__setitem__(
					"PermutationString",
					PermutationStringsList[IntAndVariable[0]]
					),
				enumerate(getattr(self,self.ListKeyString)))
	
	def getPermutationIntWithPermutationString(self,_CoordinateIntsList):
		return SYS.getPermutationIntWithCoordinateIntsListAndVariablesInt(map(int,_CoordinateIntsList.split("-")),self.PermutatedVariablesInt)

	#</DefineMethods>

#</DefineClass>

