#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StructuratingTypesSorterClass(
					SYS.BaseTypesSorterClass
				):
	
	#<DefineHookMethods>
	def initAfterBasesWithStructuratingTypesSorter(self):

		#<DefineSpecificDict>
		self.SortingStructurePointer=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StructuratingTypesSorter"]=[
													"SortingStructurePointer"
												]
	#</DefineHookMethods>

	#<DefineBindingMethods>		
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

