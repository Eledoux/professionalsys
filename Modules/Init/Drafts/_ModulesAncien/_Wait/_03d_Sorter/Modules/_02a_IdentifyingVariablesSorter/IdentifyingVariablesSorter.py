#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class IdentifyingVariablesSorterClass(
					SYS.SorterClass
				):
	
	#<DefineHookMethods>
	def initAfterBasesWithIdentifyingVariablesSorter(self):

		#<DefineSpecificDict>
		self.IdentifyersList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["IdentifyingVariablesSorter"]=[
												"IdentifyersList"
												]

	def setBeforeBasesWithIdentifyingVariablesSorter(self,_KeyVariable,_ValueVariable):

		#Wrap the Dictionnary "*" sorting
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[0:2]=="*_":
					
					#Init the NewValueVariable
					NewValueVariable=_ValueVariable

					#Call all the Call derived methods for saying how to sort this Variable
					self.__call__(_KeyVariable,NewValueVariable)

					#Return 
					return ("x"+_KeyVariable[2:],NewValueVariable)

	#</DefineHookMethods>

	#<DefineBindingMethods>	
	def setSortedTypeStringsListAfterBasesWithIdentifyingVariablesSorter(self):
		
		#Set associated PointersDicters
		self.__add__(
						dict(
								map(lambda Identifyer:
										(
											"_"+Identifyer.IdentifyerString+"Dicter",
											{}
										),
										self.IdentifyersList
									)
							)
					)	
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

