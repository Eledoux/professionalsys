#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class BaseTypesSorterClass(
					SYS.SorterClass
				):
	
	#<DefineHookMethods>
	def initAfterBasesWithBaseTypesSorter(self):

		#<DefineSpecificDict>
		self.SortedBaseTypeStringsList=[]
		#</DefineSpecificDict>

		#Update the DicterDictionnariesTypeString
		self['DicterDictionnariesTypeString']="BaseTypesFilter"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["BaseTypesSorter"]=[
												"SortedBaseTypeStringsList"
												]

	"""
	def callAfterBasesWithBaseTypesSorter(self,_KeyVariable,_SortedVariable):

		if hasattr(_SortedVariable,"TypeString"):

			#Set the BaseTypeStringsSetList of the _SortedVariable.TypeString
			DemandedTypeStringsSetList=[_SortedVariable.TypeString]+SYS.TypeStringToBaseTypeStringsSetListLoadedDict[_SortedVariable.TypeString]

			#Find where it is a BaseType of the SortedTypeStringsList
			IsGoodSortedTypeBoolsList=map(
						lambda SortedTypeString:
							SortedTypeString in DemandedTypeStringsSetList,
							self.SortedTypeStringsList
						)

			#Get the DerivedSortedTypeStringsList
			DerivedSortedTypeStringsList=filter(
					lambda TypeStringOrNone:
						TypeStringOrNone!=None,
						map(lambda IsGoodSortedTypeBool,IndexInt:
							self.SortedTypeStringsList[IndexInt] if IsGoodSortedTypeBool else None,
							IsGoodSortedTypeBoolsList,xrange(len(IsGoodSortedTypeBoolsList))
					)
				)

			#Set in each associated Dicter
			map(
				lambda DerivedSortedTypeString:
						self["_"+SYS.getPluralStringWithSingularString(DerivedSortedTypeString)+"Dicter"].__setitem__(
										_KeyVariable[1:],
										_SortedVariable
										),
			 			DerivedSortedTypeStringsList
			 	)
	"""
	#</DefineHookMethods>

	#<DefineBindingMethods>	
	def setSortedBaseTypeStringsListAfterBasesWithBaseTypesSorter(self):

		map(lambda SortedBaseTypeString:
				self.__setitem__("_"+SortedBaseTypeString+"BaseTypesFilter",
					{}
					),
				self.SortedBaseTypeStringsList
			)	

		map(lambda SortedBaseTypeString:
				getattr(self,"_"+SortedBaseTypeString+"BaseTypesFilter"),
					{}
					),
				self.SortedBaseTypeStringsList
			)

		'FilteringBaseTypeStringsList':["SortedBaseTypeString"]}

	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

