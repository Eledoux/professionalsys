#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class TypesSorterClass(
					SYS.SorterClass
				):
	
	#<DefineHookMethods>
	def initAfterBasesWithTypesSorter(self):

		
		#<DefineSpecificDict>
		self.SortedTypeStringsList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["TypesSorter"]=[
												"SortedTypeStringsList"
												]

	def setBeforeBasesWithTypesSorter(self,_KeyVariable,_ValueVariable):

		#Wrap the Dictionnary "*" sorting
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[0:2]=="*_":
					
					#Init the NewValueVariable
					NewValueVariable=_ValueVariable

					#Call all the Call derived methods for saying how to sort this Variable
					self.__call__(_KeyVariable,NewValueVariable)

					#Return 
					return ("x"+_KeyVariable[2:],NewValueVariable)

	#</DefineHookMethods>

	#<DefineBindingMethods>	
	def setSortedTypeStringsListAfterBasesWithTypesSorter(self):
		
		#Set associated PointersDicters
		self.__add__(
						dict(
								map(lambda TypeString:
										(
											"_"+SYS.getPluralStringWithSingularString(TypeString)+"Dicter",
											{"InstancedDictatedBaseTypeString":TypeString}
										),
										self.SortedTypeStringsList
									)
							)
					)	
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

