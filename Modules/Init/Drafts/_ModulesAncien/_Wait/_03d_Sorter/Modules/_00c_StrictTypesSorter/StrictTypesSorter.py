#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class StrictTypesSorterClass(
					SYS.TypesSorterClass
				):
	
	#<DefineHookMethods>
	def initAfterBasesWithStrictTypesSorter(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["StrictTypesSorter"]=[
												]

	def callAfterBasesWithStrictTypesSorter(self,_KeyVariable,_ValueVariable):

		#Wrap the Dictionnary "*" sorting
		if type(_KeyVariable) in [str,unicode]:
			if len(_KeyVariable)>0:
				if _KeyVariable[0:2]=="*_":
					
					#Init the NewValueVariable
					NewValueVariable=_ValueVariable

					#Check for the TypeString
					if hasattr(_ValueVariable,"TypeString"):
	
						#Get the TypeString
						TypeString=_ValueVariable.TypeString

						#Look if it is sorted
						if TypeString in self.SortedTypeStringsList:

							#Get the DicterKeyString
							DicterKeyString="_"+SYS.getPluralStringWithSingularString(TypeString)+"Dicter"

							#Set in the found PointersDicter
							self[DicterKeyString][_KeyVariable[1:]]=_ValueVariable

							#Set the NewValueVariable
							NewValueVariable=self[DicterKeyString][_KeyVariable[1:]]
	#</DefineHookMethods>

	#<DefineBindingMethods>		
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

