#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SorterClass(
					#SYS.DictersDicterClass,
					#SYS.AddingDicterClass
				):
	
	#<DefineHookMethods>
	def initAfterBasesWithSorter(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the InstancedDictatedBaseTypeString
		self['InstancedDictatedBaseTypeString']="InstancingDicter"

		#Update the DicterDictionnariesTypeString
		self['DicterDictionnariesTypeString']="Filter"

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Sorter"]=[
												]
	#</DefineHookMethods>

	#<DefineBindingMethods>			
	#</DefineBindingMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

