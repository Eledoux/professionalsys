#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class MatrixClass(SYS.ArrayClass):

	#<DefineHookMethods>
	def initAfterBasesWithMatrix(self):
		
		#<DefineSpecificDict>
		self.RowsInt=0
		self.ColsInt=0
		self.setArrayedLengthIntsListWithMatrix()
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"RowsInt",
													"ColsInt"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setColsIntAfterBasesWithMatrix(self):
		self.setArrayedLengthIntsListWithMatrix()
	def setRowsIntAfterBasesWithMatrix(self):
		self.setArrayedLengthIntsListWithMatrix()

	def setArrayedLengthIntsListWithMatrix(self):

		#Bind With RowsInt Setting
		if self.RowsInt!=self.ArrayedLengthIntsList[0]:
			self['RowsInt']=self.ArrayedLengthIntsList[0]

		#Bind With ColsInt Setting
		if self.ColsInt!=self.ArrayedLengthIntsList[1]:
			self['ColsInt']=self.ArrayedLengthIntsList[1]
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def setArrayedLengthIntsListWithMatrix(self):
		self['ArrayedLengthIntsList']=[self.RowsInt,self.ColsInt]
	#</DefineMethods>

#</DefineClass>

