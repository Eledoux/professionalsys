#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>


#<DefineLocals>
DoString="Debug"
DoingString="Debugging"
DoneString="Debugged"
#<DefineClass>

#<DefineClass>
class DebuggerClass(
						SYS.PrinterClass
					):

	#<Define HookMethods>
	def initAfterWithDebugger(self):

		#<DefineSpecificDict>
		self.Frame=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Debugger"]=[
													"Frame"
												]
	
	def callBeforeWithDebugger(self,*_ArgsList):
		'''
			<Help>
				Get a Debug String
			</Help>
		
			<Test>
				#Load ShareYourSystem as SYS
				import ShareYourSystem as SYS
				#
				#Print a Debug String
				def foo():
					print(SYS.callBeforeBasesWithDebugger(SYS.DebuggerClass()));
				foo();
			</Test>
		'''
		
		#Get the Frame
		if self.Frame==None:

			#Get the close back Frame
			Frame=SYS.inspect.currentframe().f_back

			#Go back until the scope is not found
			while Frame!=None and hasattr(Frame,"f_code"):

				#Get the ScriptPathString
				ScriptPathStringsList=SYS.getWordStringsListWithString(Frame.f_code.co_filename)
				ScriptPathString=''.join(ScriptPathStringsList)

				#Find the scope of the debugged Function
				if SYS.getCommonPrefixStringWithStringsList([SYS.MySYSFolderPathString,ScriptPathString])==SYS.MySYSFolderPathString:
					if SYS.getWordStringsListWithString(ScriptPathStringsList[-1])[-1] in ["Methods.py","Functions.py"]:
						break

				#Either go back in the Frame
				Frame=Frame.f_back
		else:
			Frame=self.Frame

		#Return the Debug Format
		print(self.Indent.AlineaString+"%s , %s, l. %s : "%(ScriptPathString,Frame.f_code.co_name,Frame.f_lineno)+self.Indent.LineJumpString+"<Debug>");

	def callAfterWithDebugger(self,_CallString,*_ArgsList):
		'''
			<Help>
				Get a Debug String
			</Help>
		
			<Test>
				#Load ShareYourSystem as SYS
				import ShareYourSystem as SYS
				#
				#Print a Debug String
				def foo():
					print(SYS.callBeforeBasesWithDebugger(SYS.DebuggerClass()));
				foo();
			</Test>
		'''

		#Check of the CallString
		if _CallString=="Debug":
			print(self.Indent.AlineaString+"</Debug>\n")
	#</Define HookMethods>
#</DefineClass>