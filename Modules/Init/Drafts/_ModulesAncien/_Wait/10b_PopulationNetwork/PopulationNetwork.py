#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class PopulationNetworkClass(SYS.NetworkClass):

	#<DefineHookMethods>
	def initAfterBasesWithPopulationNetwork(self):
		#<DefineSpecificDict>
		self.PopulationsArray=self.NodesArray
		self.PopulationsArray['ArrayedTypeString']="Population"
		self.SkippedKeyStringsList+=["NodesArray"]
		#</DefineSpecificDict>
	#</DefineHookMethods>

	#<DefineHookMethods>
	def setNodesArrayAfterBasesWithNetwork(self):

		#Bind with NodesArray Setting
		self['PopulationsArray']=self.NodesArray
	#</DefineHookMethods>
#</DefineClass>

