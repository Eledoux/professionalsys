#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class NoteClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithBrianedNetwork(self):

		#<DefineSpecificDict>
		self.PitchInt=0
		
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>
#</DefineClass>

