#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Get"
DoingString="Getting"
DoneString="Getted"
#</DefineLocals>

#<DefineClass>
class GetterClass(SYS.DoerClass):

	#<DefineHookMethods>
	def doBeforeWithGetter(self,_DoString,*_ArgsList,**_KwargsDict):

		#DoString checking
		if _DoString==DoString:

			#Set in the __dict__ the _ArgsList
			try:
				return map(
						lambda KeyString:
						self.__getitem__(KeyString),
						_ArgsList
					)
			except KeyError:
				print("_ArgsList has one no existing related KeyString for the Getter")

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>


#</DefineClass>