#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Set"
DoingString="Setting"
DoneString="Setted"
#</DefineLocals>

#<DefineClass>
class SetterClass(SYS.DoerClass):

	#<DefineHookMethods>
	def doBeforeWithSetter(self,_DoString,*_ArgsList,**_KwargsDict):

		#DoString checking
		if _DoString==DoString:
			
			#Set in the __dict__ the _ArgsList
			try:
				map(
						lambda ItemTuple:
						self.__setitem__(ItemTuple[0],ItemTuple[1]),
						_ArgsList
					)
			except IndexError:
				print("_ArgsList is of length 0 for the Setter")

			#Set in the __dict__ _KwargsDict
			self.update(_KwargsDict)

		#Return self
		return self
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>


#</DefineClass>