#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class InstancingSetterClass(SYS.SetterClass):

	#<DefineHookMethods>
	def initAfterBasesWithInstancingSetter(self):

		#<DefineSpecificDict>
		self.InstancedSettedBaseTypeString="InstancingSetter"
		self.NotInstancedSettedKeyStringsList=[]
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
							]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["InstancingSetter"]=[
													"InstancedSettedBaseTypeString",
													"NotInstancedSettedKeyStringsList"
												]

	def setBeforeBasesWithInstancingSetter(self,_KeyVariable,_ValueVariable):

		#Handle also Setting in the deeper Elements
		if _KeyVariable in [str,unicode]:
			if _KeyVariable not in self.NotInstancedSettedKeyStringsList:

				#Get the TypedVariable
				NewValueVariable=SYS.getTypedVariableWithVariableAndTypeString(_ValueVariable,self.InstancedSettedBaseTypeString)

				#Return 
				return (_KeyVariable,NewValueVariable)

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>


#</DefineClass>