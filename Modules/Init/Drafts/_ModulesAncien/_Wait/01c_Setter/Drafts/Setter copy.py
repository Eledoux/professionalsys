#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class SetterClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithSetter(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
							]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Setter"]=[
												]

	def setBeforeBasesWithSetter(self,_KeyVariable,_ValueVariable):

		#Handle also Setting in the deeper Elements
		KeyVariableStringsList=_KeyVariable.split("-")
		if len(KeyVariableStringsList)>1:

			#if hasattr(self,KeyVariableStringsList[0]):
			GettedVariable=self[KeyVariableStringsList[0]]
			if GettedVariable!=None:
				
				#Set in the first deeper Element with the Cutted KeyVariable
				#getattr(self,KeyVariableStringsList[0])['_'.join(KeyVariableStringsList[1:])]=_ValueVariable
				self[KeyVariableStringsList[0]]['-'.join(KeyVariableStringsList[1:])]=_ValueVariable

				#Stop the Setting at this level
				self.IsSettingBool=False
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>


#</DefineClass>