#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SuperScaleClass(
					SYS.ScaleClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithSuperScale(self):

		#<DefineSpecificDict>
		self.IsSuperScaleBool=True
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["SuperScale"]=[
													"IsSuperScaleBool"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setIntervalIntsListAfterBasesWithSuperScale(self):

		#Bind with IsSuperScaleBool setting
		self['IsSuperScaleBool']=not any(map(
				lambda Int:
						self.IntervalIntsList[Int]==1 and 
						self.IntervalIntsList[Int-1]==1,
						xrange(1,len(self.IntervalIntsList))
			)+[self.IntervalIntsList[-1]==1 and self.IntervalIntsList[0]==1])

	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>
#</DefineClass>

