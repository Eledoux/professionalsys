#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

NoteBataveStringsList=['c','d','e','f','g','a','b']


#ais aes

#<DefineClass>
class ScaleClass(
					SYS.ListedObjectClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithScale(self):

		#<DefineSpecificDict>
		self.IntervalIntsList=[]
		self.PitchIntsList=[]
		self.PitchesInt=0
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Scale"]=[
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setPitchBoolsListAfterBasesWithScale(self):

		#Bind with PitchIntsList
		self['PitchIntsList']=map(lambda IntAndPitchBool:
										IntAndPitchBool[0],
										filter(lambda IntAndPitchBool:
												IntAndPitchBool[1]==1,
												enumerate(self.PitchBoolsList)
											)
								)

	def setPitchIntsListAfterBasesWithScale(self):
		
		#Bind with IntervalIntsList setting
		self.setIntervalIntsListWithScale()

	def setPitchesIntAfterBasesWithScale(self):
		
		#Bind with IntervalIntsList setting
		self.setIntervalIntsListWithScale()

	def setListingListerPointersListAfterBasesWithScale(self):

		#Bind with PitchesInt setting
		self['PitchesInt']=self.ListingListerPointersList[0].PitchesInt

	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setIntervalIntsListWithScale(self):

		if len(self.PitchIntsList)>0:
			self['IntervalIntsList']=map(lambda IndexInt:
									self.PitchIntsList[IndexInt]-self.PitchIntsList[IndexInt-1],
									xrange(1,len(self.PitchIntsList))
									)+[(self.PitchesInt-self.PitchIntsList[-1])+self.PitchIntsList[0]]

	#</DefineMethods>
#</DefineClass>

