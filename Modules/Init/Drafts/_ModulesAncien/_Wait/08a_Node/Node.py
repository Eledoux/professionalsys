#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class NodeClass(
					SYS.ObjectClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithNode(self):

		#<DefineSpecificDict>
		#self.NetworkPointer=self.CouplerPointer
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
										"CouplerPointer",
										"RankedIntsList",
										"CoordinatedIntsList"
									]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"NetworkPointer"
												]
	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	def setCouplerPointerAfterBasesWithNode(self):

		#Bind With the CouplerPointer Setting
		self['NetworkPointer']=self.CouplerPointer
	#</DefineBindingHookMethods>

#</DefineClass>