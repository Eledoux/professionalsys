#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class TypeClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithType(self):
		
		#<DefineSpecificDict>
		self.TypedTypeString=""
		self.TypingTypeString=""
		self.TypingBaseTypeStringsList=[]
		self.IsTypedBool=True
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Type"]=[
														"TypedTypeString",
														"TypingTypeString",
														"TypingBaseTypeStringsList",
														"IsTypedBool"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setTypingBaseTypeStringsListAfterBasesWithType(self):
		
		#Just use the set to have just one TypeString for each different Type
		self.TypingBaseTypeStringsList=list(set(self.TypingBaseTypeStringsList))

		#Bind With TypedTypeString Setting
		self.setTypedTypeStringWithType()

	def setTypingTypeStringAfterBasesWithType(self):

		#Bind With TypedTypeString Setting
		self.setTypedTypeStringWithType()

	def setTypedTypeStringAfterBasesWithType(self):

		#Bind with IsTypedBool Setting
		if self.TypedTypeString=="":
			self['IsTypedBool']=False
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def setTypedTypeStringWithType(self):
		self['TypedTypeString']=SYS.getTypedStringWithTypeStringAndBaseTypeStringsList(self.TypingTypeString,
			self.TypingBaseTypeStringsList)

		
	#</DefineMethods>

#</DefineClass>

