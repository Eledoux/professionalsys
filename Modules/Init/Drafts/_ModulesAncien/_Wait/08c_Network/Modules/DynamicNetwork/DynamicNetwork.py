#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class DynamicNetworkClass(SYS.NetworkClass):

	#<DefineHookMethods>
	def initAfterBasesWithDynamicNetwork(self):
		#<DefineSpecificDict>
		self.TimeDimensionString="ms"
		self.RunTimeFloat=100.
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"TimeDimensionString",
													"RunTimeFloat"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def run(self):
		pass
	#<DefineMethods>

#</DefineClass>

