#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class NetworkClass(SYS.MutableCouplerClass):

	#<DefineHookMethods>
	def initAfterBasesWithNetwork(self):
		#<DefineSpecificDict>
		self['CoupledAgentTypeString']='Node'
		self['CoupledInteractionTypeString']='Connection'
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
										"ConnectionsTupler",
										"CoupledAgentKeyString",
										"CoupledAgentTypeString",
										"CoupledArrayTypeString",
										"CoupledArrayKeyString",
										"CoupledInteractionTypeString",
										"CoupledTuplerKeyString",
										"CoupledTuplerTypeString",
										"PluralCoupledAgentTypeString",
										"PluralCoupledInteractionTypeString",
									]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"NodesArray",
													"ConnectionsTable"
												]

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setCoupledInteractionTypeStringAfterBasesWithNetwork(self):

		#Bind with NetworkedTableKeyString Setting
		self['NetworkedTableKeyString']=self.PluralCoupledInteractionTypeString+"Table"

	def setNetworkedTableKeyStringAfterBasesWithNetwork(self):

		#Get the ConnectionsTable
		ConnectionsTable=getattr(self,self.CoupledTuplerKeyString)

		#Check that this is setted with the good order
		if ConnectionsTable.TupledDegreeIntsList!=[2]:
			ConnectionsTable['TupledDegreeIntsList']=[2]

		#Build a ShortCut to the Connections order 2 
		self[self.NetworkedTableKeyString]=getattr(self,self.CoupledTuplerKeyString)[0]

	#</DefineBindingHookMethods>
#</DefineClass>

