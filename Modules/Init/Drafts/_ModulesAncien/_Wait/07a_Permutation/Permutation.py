#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class PermutationClass(
							SYS.IdentifiedObjectClass
					):

	#<DefineHookMethods>
	def initAfterBasesWithPermutation(self):

		#<DefineSpecificDict>
		self.PermutationString=""
		self.PermutaterPointer=None
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
										#"PermutaterPointer"
									]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"PermutationString",
													"PermutaterPointer"
												]
	#</DefineHookMethods>

	#</DefineBindingHookMethods>
	def setParentPointersListAfterBasesWithPermutation(self):

		#Bind With the PermutaterPointer Setting
		TypeString="Permutater"
		IsPointerBoolsList=map(lambda Pointer:SYS.getIsTypedVariableBoolWithTypeStringAndVariable(TypeString,Pointer),self.ParentPointersList)
		if any(IsPointerBoolsList):
			self[TypeString+'Pointer']=self.ParentPointersList[IsPointerBoolsList.index(True)]
	
	def setPermutationStringAfterBasesWithPermutation(self):

		if self.PermutationString!="":
			self.IdentitiesDictionnary["_PermutaterIdentifier"]=SYS.IdentityClass().update(
				{'IdentityString':self.PermutationString})

		"""
		if self.PermutationString!="" and self.PermutaterPointer!=None:
			
			#Bind With PermutationStringToPermutationPointerDict Setting
			self.PermutaterPointer['PermutationStringToPermutationPointerDict'][self.PermutationString]=self
		"""
	#</DefineBindingHookMethods>

#</DefineClass>