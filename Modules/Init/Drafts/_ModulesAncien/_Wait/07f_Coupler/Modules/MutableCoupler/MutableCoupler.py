#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class MutableCouplerClass(SYS.CouplerClass):

	#<DefineHookMethods>
	def initAfterBasesWithMutableCoupler(self):

		#<DefineSpecificDict>
		self.PluralCoupledAgentTypeString="Agents"
		self.PluralCoupledInteractionTypeString="Interactions"
		#<DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"PluralCoupledAgentTypeString",
													"PluralCoupledInteractionTypeString",
												]
	
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setCoupledAgentTypeStringAfterBasesWithMutableCoupler(self):

		#Bind With PluralCoupledTypeString
		self['PluralCoupledAgentTypeString']=SYS.getPluralStringWithSingularString(self.CoupledAgentTypeString)

	def setCoupledInteractionTypeStringAfterBasesWithMutableCoupler(self):

		#Bind With PluralCoupledTypeString
		self['PluralCoupledInteractionTypeString']=SYS.getPluralStringWithSingularString(self.CoupledInteractionTypeString)

	def setPluralCoupledAgentTypeStringAfterBasesWithMutableCoupler(self):
		self.setCoupledStructureKeyString()
		
	def setCoupledStructureTypeStringAfterBasesWithMutableCoupler(self):
		self.setCoupledStructureKeyString()

	def setPluralCoupledInteractionTypeStringAfterBasesWithMutableCoupler(self):
		self.setCoupledTuplerKeyString()
		
	def setCoupledTuplerTypeStringAfterBasesWithMutableCoupler(self):
		self.setCoupledTuplerKeyString()

	def setCoupledStructureKeyStringBeforeBasesWithMutableCoupler(self):

		#Set a Temporary OldCoupleArrayKeyString
		self.OldCoupledStructureKeyString=self.CoupledStructureKeyString

	def setCoupledStructureKeyStringAfterBasesWithMutableCoupler(self):

		#Check that this is a change of type
		if self.OldCoupledStructureKeyString!=self.CoupledStructureKeyString:

			#Reset an Array of <self.CoupledAgentTypeString> Objects 
			self[self.CoupledStructureKeyString]=self.getCoupledStructureWithCoupler()

			#Set the Hooks in the Agents
			self.setCoupledStructureBindingHookMethod()

			#Delete the old Array
			self.__delitem__(self.OldCoupledStructureKeyString)

			#Delete the OldCoupleTuplerKeyString
			del self.OldCoupledStructureKeyString

	def setCoupledTuplerKeyStringBeforeBasesWithMutableCoupler(self):

		#Set a Temporary OldCoupleTuplerKeyString
		self.OldCoupledTuplerKeyString=self.CoupledTuplerKeyString

	def setCoupledTuplerKeyStringAfterBasesWithMutableCoupler(self):

		#Check that this is a change of type
		if self.OldCoupledTuplerKeyString!=self.CoupledTuplerKeyString:

			#Reset a Tupler of <self.CoupledInteractionTypeString> Objects 
			self[self.CoupledTuplerKeyString]=self.getCoupledTuplerWithCoupler()

			#Delete the old Array
			self.__delitem__(self.OldCoupledTuplerKeyString)

			#Delete the OldCoupledTuplerKeyString
			del self.OldCoupledTuplerKeyString

	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setCoupledStructureKeyString(self):

		#Bind with CoupledStructureKeyString Setting
		self['CoupledStructureKeyString']=self.PluralCoupledAgentTypeString+self.CoupledStructureTypeString

	def setCoupledTuplerKeyString(self):

		#Bind with CoupledTuplerKeyString Setting
		self['CoupledTuplerKeyString']=self.PluralCoupledInteractionTypeString+self.CoupledTuplerTypeString
	#</DefineMethods>
#</DefineClass>

