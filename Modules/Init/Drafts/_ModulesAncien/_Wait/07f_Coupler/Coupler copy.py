#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class CouplerClass(SYS.ElementClass):

	#<DefineHookMethods>
	def initAfterBasesWithCoupler(self):

		#<DefineSpecificDict>
		self.CoupledAgentTypeString="Agent"
		self.CoupledStructureTypeString="Structure"
		self.CoupledStructureKeyString="AgentsStructure"
		self['AgentsStructure']=self.getCoupledStructureWithCoupler()
		self.CoupledInteractionTypeString="Interaction"
		self.CoupledTuplerTypeString="Tupler"
		self.CoupledTuplerKeyString="InteractionsTupler"
		self['InteractionsTupler']=self.getCoupledTuplerWithCoupler()
		#<DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"AgentsStructure"
													"CoupledStructureKeyString"
													"CoupledStructureTypeString",
													"CoupledAgentTypeString",
													"CoupledInteractionTypeString",
													"CoupledTuplerTypeString"
													"CoupledTuplerKeyString"
													"InteractionsTupler"
												]
		
		#Set the Hooks in the AgentsStructure
		self.setCoupledStructureBindingHookMethod()
		
	def getBeforeBasesWithCoupler(self,_KeyVariable):

		#Special CoordinatedIntsList Gettings in the Structure or in the Tupler
		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable[0]=="#":
				if '-' not in _KeyVariable:
					return getattr(self,self.CoupledStructureKeyString)[_KeyVariable]
				else:

					#Replace "#"" by ""
					KeyString=_KeyVariable.replace("#","")

					#Return 
					return getattr(self,self.CoupledTuplerKeyString)[KeyString]



	def addAfterBasesWithCoupler(self,_AddedVariable):

		#Add to the <CoupleStructureKeyString> Structure
		self[self.CoupledStructureKeyString]+=_AddedVariable
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>	
	def getCoupledStructureWithCoupler(self):
		if hasattr(self,"CoupledAgentTypeString"):
			ClassString=SYS.getClassStringWithTypeString(self.CoupledStructureTypeString)
			if hasattr(SYS,ClassString):
				return getattr(SYS,ClassString)().update({"ArrayedTypeString":self.CoupledAgentTypeString})

	def getCoupledTuplerWithCoupler(self):
		ClassString=SYS.getClassStringWithTypeString(self.CoupledTuplerTypeString)
		if hasattr(SYS,ClassString):
			return getattr(SYS,ClassString)().update({'DatabasedTypeString':self.CoupledInteractionTypeString})
	
	def setCoupledTupler_TupledAgentsInt(self):
		getattr(self,self.CoupledTuplerKeyString)['TupledAgentsInt']=getattr(self,self.CoupledStructureKeyString).ArrayedElementsInt
	
	"""
	def setCoupledStructureBindingHookMethod(self):
		getattr(self,self.CoupledStructureKeyString).setArrayedLengthIntsListAfterBasesWithArray=SYS.DecoratorClass().update({
														'DecoratedFunction':getattr(self,self.CoupledStructureKeyString).setArrayedLengthIntsListAfterBasesWithArray,
														'AfterDecoratingFunction':self.setCoupledTupler_TupledAgentsInt
														})
	"""

	#</DefineMethods>
#</DefineClass>

