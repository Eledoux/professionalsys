#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class CouplerClass(SYS.ElementClass):

	#<DefineHookMethods>
	def initAfterBasesWithCoupler(self):

		#<DefineSpecificDict>
		self.CoupledSorterKeyString="AgentsCoupledSorter"
		self.CoupledTuplerKeyString="InteractionsCoupledTupler"
		self['InteractionsCoupledTupler']=SYS.CoupledTuplerClass().update({'DatabasedTypeString':'Interaction'})
		self['AgentsCoupledSorter']=SYS.CoupledSorterClass()
		#<DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
														"CoupledSorterKeyString",
														"CoupledTuplerKeyString"
												]
		
	def getBeforeBasesWithCoupler(self,_KeyVariable):

		#Special CoordinatedIntsList Gettings in the Sorter or in the Tupler
		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable[0]=="#":
				if '-' not in _KeyVariable:
					return getattr(self,self.CoupledSorterKeyString)[_KeyVariable]
				else:

					#Replace "#"" by ""
					KeyString=_KeyVariable.replace("#","")

					#Return 
					return getattr(self,self.CoupledTuplerKeyString)[KeyString]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>	
	#</DefineMethods>
#</DefineClass>

