#<Import Modules>
import operator
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class ArrayClass(SYS.RankerClass):

	#<DefineHookMethods>
	def initAfterBasesWithArray(self):

		#<DefineSpecificDict>
		self.ArrayedLengthIntsList=[]
		self.ArrayedElementsInt=0
		self.ArrayedDimensionInt=0
		self.ArrayedTypeString="Element"
		self.SubArrayedElementsIntsList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"ArrayedLengthIntsList",
													"ArrayedElementsInt",
													"ArrayedTypeString",
													"SubArrayedElementsIntsList"
												]

	def addBeforeBasesWithArray(self,_AddedVariable):

		#Check that the _AddedVariable has a shape
		if hasattr(_AddedVariable,"shape"):

			#Check if it is the good Shape
			if self.ArrayedLengthIntsList==list(_AddedVariable.shape):

				#Find the non zero elements
				import numpy
				IndicesIntsList=list(numpy.where(_AddedVariable!=0))

				#Define DimensionInt
				DimensionInt=len(self.ArrayedLengthIntsList)

				#Parse the non zero Elements
				for IndicesInt in xrange(len(IndicesIntsList[0])):

					#Set the IndicesTuple
					IndicesTuple=tuple([IndicesIntsList[Int][IndicesInt] for Int in xrange(DimensionInt)])

					#Get the corresponding Element
					GettedElement=self[IndicesTuple]

					#Add at this IndicesTuple
					if GettedElement!=None:
						self[IndicesTuple]+=_AddedVariable[IndicesTuple]

	def iterBeforeBasesWithArray(self,_IteratedDict):

		#Particular case of the Array with ArrayedDimensionInt==1 : Sublayer is not a specific database so it doesn't do automatically the SpecificDatabase iter method 
		if self.ArrayedDimensionInt==1:

			#Stop after the iter
			self.IsIteratingBool=False

			#Return the ListedVariables
			return getattr(self,self.ListKeyString)
	

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setArrayedLengthIntsListAfterBasesWithArray(self):

		#Set the number of Grided Element
		self['ArrayedElementsInt']=reduce(operator.mul,self.ArrayedLengthIntsList)

		#Set the ArrayedDimensionInt
		self.ArrayedDimensionInt=len(self.ArrayedLengthIntsList)

		#Set the number of SubGridedElements
		self.SubArrayedElementsIntsList=map(lambda Int:reduce(operator.mul,self.ArrayedLengthIntsList[Int+1:]),xrange(len(self.ArrayedLengthIntsList)-1))

		#Define the ArrayTypeString depending on how much dimensions need to be grided
		if len(self.ArrayedLengthIntsList)==1:
			self.FrozenMethodStringsList.append('setListedTypeStringAfterBasesWithDatabase')
			self["DatabasedTypeString"]=self.ArrayedTypeString
			self["ListedTypeString"]=self.ArrayedTypeString
			self["LengthInt"]=self.ArrayedLengthIntsList[0]
			return 
		elif len(self.ArrayedLengthIntsList)==2:
			self['ListedTypeString']="Table"
			self['DatabasedTypeString']=self.ArrayedTypeString
		elif len(self.ArrayedLengthIntsList)==3:
			self['ListedTypeString']="SpecificDatabase"
			self['DatabasedTypeString']="Table"
		else:
			self['ListedTypeString']="Array"
			self['DatabasedTypeString']="Array"

		#Build the local splitting Array
		self['TablesLengthIntsList']=[self.ArrayedLengthIntsList[1] for Int in xrange(self.ArrayedLengthIntsList[0])]

		#Give to each Child a recursive manner for other ChildGrids
		if len(self.ArrayedLengthIntsList)==3:
			for SpecificDatabase in getattr(self,self.ListKeyString):
				for Table in getattr(SpecificDatabase,SpecificDatabase.ListKeyString):
					Table['DatabasedTypeString']=self.ArrayedTypeString
					Table['ListedTypeString']=self.ArrayedTypeString
					Table['LengthInt']=self.ArrayedLengthIntsList[2]
		elif len(self.ArrayedLengthIntsList)>3:
			for Table in getattr(self,self.ListKeyString):
				for Array in Table.ArraysList:
					Array['ArrayedTypeString']=self.ArrayedTypeString
					Array['ArrayedLengthIntsList']=self.ArrayedLengthIntsList[2:]
	#<DefineBindingHookMethods>

	#<DefineMethods>
	def getListersList(self):
		import itertools
		ListerCoordinateIntsTupleIter=itertools.product(*map(lambda LengthInt:xrange(LengthInt),self.ArrayedLengthIntsList[:-1]))
		return map(lambda ListerCoordinateIntsTuple:self[ListerCoordinateIntsTuple],ListerCoordinateIntsTupleIter)
	#</DefineMethods>

#<DefineClass>
