#<Import Modules>
import numpy
import operator
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class ShaperClass(SYS.NetworkClass):

	#<DefineHookMethods>
	def initAfterBasesWithShaper(self):

		#<DefineSpecificDict>
		self['CoupledAgentTypeString']="Shape"
		self['CoupledArrayTypeString']="RankedSpace"
		self.ShapingCursor=SYS.CursorClass()
		self.EqualInt=0
		self.EqualShapesInt=0
		self.EqualShapedLengthIntsList=[]
		self.ShapingIntsList=[]
		#</DefineSpecificDict>

		#Remove KeyStrings for print
		self.SkippedKeyStringsList+=[
									"ConnectionsTable",
									"CoupledStructureKeyString",
									"CoupledStructureTypeString",
									"NetworkedTableKeyString"
									]

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
									"ShaperIntsList",
									"ShapingCursor"
												]	

		#Set a Hook in the ShapingCursor
		self.ShapingCursor.setCursorLengthIntsListAfterBasesWithCursor=SYS.DecoratorClass().update(
						{
							'DecoratedFunction':self.ShapingCursor.setCursorLengthIntsListAfterBasesWithCursor,
							'AfterDecoratingFunction':self.setShapingCursor_CursorLengthIntsListWithShaper
						})

		#Set a Hook in the ShapesStructure
		self.ShapesStructure.setArrayedElementsIntAfterBasesWithStructure=SYS.DecoratorClass().update(
						{
							'DecoratedFunction':lambda :None,
							'AfterDecoratingFunction':self.setEqualShapesIntWithShaper
						})
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setShapingCursor_CursorLengthIntsListWithShaper(self):

		#Bind with ShapingIntsList Setting
		if len(self.ShapingIntsList)!=len(self.ShapingCursor.CursorLengthIntsList):
			self['ShapingIntsList']=[1 for Int in xrange(len(self.ShapingCursor.CursorLengthIntsList))]

	def setEqualShapesIntAfterBasesWithShaper(self):

		#Set the total number of space for each Dimension given the number EqualShapesInt
		TotalShaperIntsList=map(lambda Int:(self.EqualShapesInt-1)*Int,self.ShapingIntsList)

		#Set the total number of occupied space by the shape
		TotalShapedLengthIntsList=map(operator.sub,self.ShapingCursor.CursorLengthIntsList,TotalShaperIntsList)

		#Bind with EqualShapeLengthIntsList Setting
		self['EqualShapedLengthIntsList']=map(lambda Int:Int/self.EqualShapesInt,TotalShapedLengthIntsList)

	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setEqualShapesIntWithShaper(self):

		#Given the number of ArrayedElementsInt and the ShapedDimensionInt, this computes the ideal homogeneous number of Shapes in one Dimension
		self['EqualShapesInt']=int(numpy.floor(numpy.power(self.ShapesStructure.ArrayedElementsInt,1./float(self.ShapingCursor.CursorDimensionInt))))

	#</DefineMethods>

#</DefineClass>

