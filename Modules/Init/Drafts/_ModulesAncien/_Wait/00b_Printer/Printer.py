#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineLocals>
DoString="Print"
DoingString="Printing"
DoneString="Printed"
#</DefineLocals>

#<DefineClass>
class PrinterClass(SYS.HookerClass):

	#<Define HookMethods>
	def initAfterWithPrinter(self):

		#<DefineSpecificDict>
		self.Indent=SYS.IndentClass()
		self.IsPrintedKeysBool=True
		self.IsPrintedValuesBool=True
		#</DefineSpecificDict>

	def callAfterWithPrinter(self,_CallString,*_ArgsList,**_KwargsDict):
		'''
			<Help>
				Call a Printer to display the Variable in a human readable manner
			</Help>
		
			<Test>
				#Load ShareYourSystem as SYS
				import ShareYourSystem as SYS
				#
				#Define a Dict
				Dict={'ThisDict':{'ThingsDict':{'Thing':0},'MyObjectsList':[{'ID':0}]},'Stuff':"Sad"};
				#
				#Print the Dict
				print("The PrintedDictString is :");
				SYS.callAfterBasesWithPrinter(ConsolePrinter,Dict);
			</Test>
		'''

		#Check of the _CallString
		if _CallString==DoString:
			if len(_ArgsList)>0:
				print(self.getPrintedVariableString(_ArgsList[0]))
			else:
				SYS.debug("I have nothing to print !")

	#<Define /HookMethods>

	#<Define Methods>
	def getPrintedDictString(self,_Dict):
		'''
			<Help>
				Get a Stringified version of a dict with alphabetical order
			</Help>

			<Test>
				#Load ShareYourSystem as SYS
				import ShareYourSystem as SYS
				#
				#Define a Dict
				Dict={'ThisDict':{'ThingsDict':{'Thing':0},'MyObjectsList':[{'ID':0}]},'Stuff':"Sad"};
				#
				#Print the Dict
				print("The PrintedDictString is :");
				print(SYS.getPrintedDictStringWithPrinter(SYS.ConsolePrinter,Dict));
			</Test>
		'''
			
		#Init the PrintedDictString
		PrintedDictString='{ '
		
		#Set the ElementString
		self.Indent['ElementString']=SYS.ReprElementString
		
		#Update the AlineaString and check that the binding has worked
		self.Indent.addElementString()
		
		#Do the first Jump
		PrintedDictString+=self.Indent.LineJumpString
		
		#Scan the Items (integrativ loop)
		StringLengthInt=len(_Dict)
		ItemInt=0
		for KeyString,ValueVariable in sorted(_Dict.iteritems(), key=lambda key_value: key_value[0]):
		
			#Only Key Displayed Case
			if self.IsPrintedKeysBool and self.IsPrintedValuesBool==False:
			
				if SYS.getWordStringsListWithNameString(KeyString)[-1]=="Pointer":
				
					#Pointer Case
					PrintedDictString+="'"+KeyString+"'"
				
				else:
					
					#OtherCases
					if hasattr(ValueVariable,'__dict__'):
						PrintedDictString+=SYS.getPrintedPointerString(ValueVariable)
					else:
						PrintedDictString+=str(type(ValueVariable))
			else:
				
				#Get the WordStringsList
				WordStringsList=SYS.getWordStringsListWithString(KeyString)

				#Init the DisplayedValueVariableString
				DisplayedValueVariableString="None"

				if len(WordStringsList)>0:

					#Value is displayed
					if SYS.getWordStringsListWithString(KeyString)[-1]=="Pointer":
					
						#Pointer Case
						DisplayedValueVariableString=SYS.getPrintedPointerString(ValueVariable)

					elif ''.join(SYS.getWordStringsListWithString(KeyString)[-2:])=="PointersList":
					
						#Pointer Case
						DisplayedValueVariableString=str(map(lambda ListedVariable:SYS.getPrintedPointerString(ListedVariable),ValueVariable))
						
				#Special Suffix Cases
				TypeString=type(ValueVariable).__name__
				if TypeString=="instancemethod":
					DisplayedValueVariableString="<"+ValueVariable.__name__+" "+TypeString+">"
							
				elif DisplayedValueVariableString=="None":
						
					#Other Cases
					DisplayedValueVariableString=self.getPrintedVariableString(ValueVariable)
				
				if self.IsPrintedKeysBool==False and self.IsPrintedValuesBool:
				
					#Only Value Case
					PrintedDictString+=DisplayedValueVariableString
			
				elif self.IsPrintedKeysBool and self.IsPrintedValuesBool:
					
					#Key and Value Case
					PrintedDictString+="'"+KeyString+"' : "+DisplayedValueVariableString

			#Separate items
			ItemInt+=1;
			if ItemInt<StringLengthInt:
				PrintedDictString+=self.Indent.LineJumpString

		#Update the AlineaString and check that the binding has worked
		self.Indent.substractElementString()
		
		#return the DictString
		return PrintedDictString+self.Indent.LineJumpString+' }'

	def getPrintedListString(self,_Variable):
		'''
			<Help>
				Get a Stringified version of a list
			</Help>

			<Test>
				#Load ShareYourSystem as SYS
				import ShareYourSystem as SYS
				#
				#Define a Dict
				List=[{'ThisDict':{'ThingsDict':{'Thing':0},'MyObjectsList':[{'ID':0}]},'Stuff':"Sad"},3,{},[{'String':"Haha"}]];
				#
				#Print the Dict
				print("The PrintedListString is :");
				print(SYS.getPrintedListStringWithPrinter(SYS.ConsolePrinter,List));
			</Test>
		'''

		#Init the PrintedDictString
		PrintedListString='[ '
		
		#Set the ElementString
		self.Indent['ElementString']=SYS.ReprElementString

		#Update the AlineaString and check that the binding has worked
		self.Indent.addElementString()

		#Do the first Jump
		PrintedListString+=self.Indent.LineJumpString
		
		#Scan the Items (integrativ loop)
		StringLengthInt=len(_Variable)
		ItemInt=0;
		for ListedVariableInt,ListedVariable in enumerate(_Variable):
		
			#Only Key Displayed Case
			if self.IsPrintedKeysBool and self.IsPrintedValuesBool==False:

				PrintedListString+=str(ListedVariableInt)

			else:
				
				if type(ListedVariable).__name__=="instancemethod":
					DisplayedValueVariableString="instancemethod"
						
				else:
				
					#Other Cases
					DisplayedValueVariableString=self.getPrintedVariableString(ListedVariable)
				
				if self.IsPrintedKeysBool==False and self.IsPrintedValuesBool:
				
					#Only Value Case
					PrintedListString+=DisplayedValueVariableString
			
				elif self.IsPrintedKeysBool and self.IsPrintedValuesBool:
					
					#Key and Value Case
					PrintedListString+="'"+str(ListedVariableInt)+"' : "+DisplayedValueVariableString

			#Separate items
			ItemInt+=1;
			if ItemInt<StringLengthInt:
				PrintedListString+=self.Indent.LineJumpString

		#Update the AlineaString and check that the binding has worked
		self.Indent.substractElementString()
		
		#return the DictString
		return PrintedListString+self.Indent.LineJumpString+' ]'

	def getPrintedVariableString(self,_Variable):
		'''
			<Help>
				Return a special recursive __repr__ of the Variable if it is a Sys Object with an Indent given by the Printer
			</Help>

			<Test>
				#Load ShareYourSystem as SYS
				import ShareYourSystem as SYS
				#
				#Define a Printer
				Printer=SYS.getDefaultPrinterDict;
				#
				#Get the PrintedVariable
				print(SYS.getVariableStringWithPrinter(Printer,{'ThisDict':{'ThingsDict':{'Thing':0},'MyObjectsList':[{'ID':0}]},'Stuff':"Sad"}))
			</Test>
		'''
		
		if "DicterClass" in map(lambda Base:Base.__name__,SYS.getBasesListWithClass(_Variable)):
			return self.Indent.LineJumpString+_Variable.__repr__(**Kwargs)
		elif type(_Variable)==dict:
			return self.Indent.LineJumpString+self.getPrintedDictString(_Variable)
		elif type(_Variable)==list:
			#Check if it is a List of Object or Python Types
			if all(
					map(
						lambda ListedVariable:
						type(ListedVariable) in [float,int,str,unicode,SYS.sys.modules['numpy'].float64],
						_Variable
						)
				)==False:
				return self.Indent.LineJumpString+self.getPrintedListString(_Variable)
			else:
				return str(_Variable)
		elif type(_Variable).__name__=="instancemethod":
			return "instancemethod"
		elif type(_Variable) in [str,unicode]:
			return (self.Indent.AlineaString if (hasattr(self,"VariableString") and self.VariableString=="ConsoleDebugger") else "")+_Variable
		else:
			return repr(_Variable)
	#</Define Methods>
#</DefineClass>
