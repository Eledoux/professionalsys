#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class BrianedNetworkClass(SYS.PopulationNetworkClass,SYS.DynamicNetworkClass):

	#<DefineHookMethods>
	def initAfterBasesWithBrianedNetwork(self):
		#<DefineSpecificDict>
		self.BrianMagicNetwork=None
		self.ClocksTable=SYS.TableClass().update({'ListedTypeString':"BrianClock"})+[
										{'ClockString':"Simulation","TimeStepFloat":0.1,"OrderInt":0},
										{'ClockString':"Current","TimeStepFloat":10.,"OrderInt":1},
										{'ClockString':"Record","TimeStepFloat":0.1,"OrderInt":2}
										]
		self.PopulationsArray['ArrayedTypeString']="BrianedPopulation"
		self.ConnectionsTable['ListedTypeString']="BrianedConnection"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"ClocksTable",
													"BrianMagicNetwork"
												]

		#Set a ArrayLengthIntsListHook in the ConnectionsTable
		self.AgentsArray.setArrayLengthIntsListAfterBasesWithArray=SYS.DecoratorClass().update(
																{
																	'DecoratedFunction':self.AgentsArray.setArrayLengthIntsListAfterBasesWithArray,
																	'AfterDecoratingFunction':self.setBrianMagicNetwork
																})

		#Decorate the run Method
		self.run=SYS.DecoratorClass().update(
											{
												'DecoratedFunction':self.run,
												'AfterDecoratingFunction':self.runWithBrianNetwork
											})


	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setBrianMagicNetwork(self):
		import brian
		self['BrianMagicNetwork']=brian.MagicNetwork()

		for BrianedPopulation in self.AgentsArray.__iter__():
			if BrianedPopulation.BrianNeuronGroup!=None:
				self.BrianMagicNetwork.add(BrianedPopulation.BrianNeuronGroup)

		for BrianedConnection in self.InteractionsTupler.__iter__():
			if BrianedConnection.BrianConnection!=None:
				self.BrianMagicNetwork.add(BrianedConnection.BrianConnection)


	def runWithBrianNetwork(self):
		
		#Call the run of the BrianMagicNetwork
		if self.BrianMagicNetwork!=None:
			import brian
			self.BrianMagicNetwork.run(self.RunTimeFloat*getattr(brian,self.TimeDimensionString))

	#</DefineMethods>
#</DefineClass>

