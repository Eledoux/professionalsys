#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class RankerClass(
						#SYS.SortingStructureClass,
						SYS.RankedListerClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithRanker(self):
		
		#<DefineSpecificDict>
		self.ParentRankerPointer=None
		self.StructureTypeString="Ranker"
		#self.SortedIntToSortedTableDicter=SYS.DicterClass()
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
												]

	def iterAfterBasesWithRanker(self,_IteratedDict):

		#Do a recursive call of the __iter__ in each ListedTable
		import operator
		return reduce(operator.add,map(lambda ListedTable:list(ListedTable.__iter__()),getattr(self,self.ListKeyString)))
	
	def getBeforeBasesWithRanker(self,_KeyVariable):
		
		#Special RankedCoordinate Getting
		if type(_KeyVariable) in [str,unicode]:
			if _KeyVariable[0]=="#":
				if _KeyVariable[1:].isdigit():
					if _KeyVariable[1:] in self.SortedIntToSortedTableDict:
						return self.getSortedTableWithSortedInt(_KeyVariable[1:])

	def addAfterBasesWithRanker(self,_AddedVariable):

		#Bind with setAllPreviousSortedVariablesInt 
		self.setAllPreviousSortedVariablesInt()

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setParentPointersListAfterBasesWithSortedCoordinate(self):

		#Bind With the CouplerPointerSetting
		TypeString="Ranker"
		IsPointerBoolsList=map(lambda Pointer:SYS.getIsTypedVariableBoolWithTypeStringAndVariable(TypeString,Pointer),self.ParentPointersList)
		if any(IsPointerBoolsList):
			self["Parent"+TypeString+"Pointer"]=self.ParentPointersList[IsPointerBoolsList.index(True)]
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def getSortedTableWithSortedInt(self,_SortedInt):

		#Use the SortedIntToSortedVariableDict
		SortedIntString=str(_SortedInt)
		if SortedIntString in self.SortedIntToSortedTableDict:
			return self.SortedIntToSortedTableDict[SortedIntString]
	#</DefineMethods>

#</DefineClass>

