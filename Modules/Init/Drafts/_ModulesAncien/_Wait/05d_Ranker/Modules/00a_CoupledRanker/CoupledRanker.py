#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class CoupledRankerClass(SYS.RankerClass):

	#<DefineHookMethods>
	def initAfterBasesWithCoupledRanker(self):

		#<DefineSpecificDict>
		self.CouplerPointer=None
		self.TuplerPointer=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"CouplerPointer",
													"TuplerPointer"
												]

	def reprAfterBasesWithCoupledRanker(self,_SelfString,_PrintedDict):

		#Define the Filtering KeyStrings
		KeyStringsList=[
							"CouplerPointer",
							"TuplerPointer"
						]
		
		#Set the NewPrintedDict
		_PrintedDict.update(dict(filter(lambda Item:Item!=None,map(lambda KeyString:(KeyString,self.__dict__[KeyString]) if KeyString in self.__dict__ else None,KeyStringsList))))

		#Define the new SelfString
		NewSelfString=SYS.ConsolePrinter.getPrintedVariableString(_PrintedDict)

		#Return 
		return NewSelfString,_PrintedDict

	def addAfterBasesWithCoupledRanker(self,_AddedVariable):

		#Bind with TuplerPointer_TupledAgentsInt
		if self.TuplerPointer!=None:
			self.TuplerPointer['TupledAgentsInt']=len(self.SortedIntToSortedTableDict)
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setParentPointerAfterBasesWithCoupledRanker(self):

		#Bind with CouplerPointerSetting
		if SYS.SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Coupler",self.ParentPointer):
			self['CouplerPointer']=self.ParentPointer

	def setCouplerPointerAfterBasesWithCoupledRanker(self):

		#Bind with TuplerPointerSetting
		TuplerPointer=getattr(self.CouplerPointer,self.CouplerPointer.CoupledTuplerKeyString)
		if SYS.SYS.getIsTypedVariableBoolWithTypeStringAndVariable("CoupledTupler",TuplerPointer):
			self['TuplerPointer']=TuplerPointer
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

