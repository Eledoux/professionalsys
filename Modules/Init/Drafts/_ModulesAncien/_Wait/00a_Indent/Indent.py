#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class IndentClass(SYS.HookerClass):

	#<DefineHookMethods>
	def initAfterWithIndent(self):
		
		#<DefineSpecificDict>
		self.AlineaString=""
		self.LineJumpString="\n"
		self.ElementString="   /"
		#</DefineSpecificDict>

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def bindAlineaStringAfterWithIndent(self):
		
		#Bind With LineJumpString Setting
		self['LineJumpString']='\n'+self.AlineaString
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	def addElementString(self):
		self['AlineaString']+=self.ElementString

	def substractElementString(self):
		self['AlineaString']=self.AlineaString[:-len(self.ElementString)]
	#</DefineMethods>

#</DefineClass>

