#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class InteractionClass(
							#SYS.AgentClass,
							SYS.PermutationClass
						):

	#<DefineHookMethods>
	def initAfterBasesWithInteraction(self):

		#<DefineSpecificDict>
		self.InteractingPointersList=[]
		self.CouplerPointer=None
		self.CoupledSorterPointer=None
		self.SkippedKeyStringsList+=["CouplerPointer"]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"InteractingPointersList",
													"CouplerPointer"
												]
	#<DefineHookMethods>

	#<DefineBindingHookMethods>
	def setParentPointersListAfterBasesWithInteraction(self):

		#Bind With the CouplerPointerSetting
		IsPointerBoolsList=map(lambda Pointer:SYS.getIsTypedVariableBoolWithTypeStringAndVariable("Coupler",Pointer),self.ParentPointersList)
		if any(IsPointerBoolsList):
			self.CouplerPointer=self.ParentPointersList[IsPointerBoolsList.index(True)]

	def setCouplerPointerAfterBasesWithInteraction(self):

		#Bind with CoupledSorterPointer Setting
		if self.CouplerPointer!=None:
			self['CoupledSorterPointer']=getattr(self.CouplerPointer,self.CouplerPointer.CoupledSorterKeyString)

	def setPermutationStringAfterBasesWithInteraction(self):
		if self.CouplerPointer!=None:

			#Get the individual Euclidian index Int of the Agent Grid List
			PermutationStringsList=self.PermutationString.split("-")

			#Bind with InteractingPointersList setting
			self['InteractingPointersList']=map(lambda PermutationString:self.CoupledSorterPointer["#"+PermutationString],PermutationStringsList)

			"""
			#Get shortcuts for ArrayedLengthIntsList and SubGridedAgentsIntsList
			ArrayedLengthIntsList=getattr(self.CouplerPointer,self.CouplerPointer.CoupledSorterKeyString).ArrayedLengthIntsList
			SubArrayedAgentsIntsList=getattr(self.CouplerPointer,self.CouplerPointer.CoupledStructureKeyString).SubArrayedElementsIntsList

			#Retrieve from the Int Index Grid the List of Ints in each TablesList
			ArrayedIntsListsList=[[]]*len(PermutationStringsList)
			for EnumeratedInt,PermutationString in enumerate(PermutationStringsList):
				
				#Get the corresponding Int
				PermutationInt=int(PermutationString)

				#Get the ArrayedIntsListsList
				ArrayedIntsListsList[EnumeratedInt]=SYS.getArrayedIntsListWithArrayedLengthIntsListAndProdIntsListAndRankedInt(ArrayedLengthIntsList,SubArrayedAgentsIntsList,PermutationInt)

			#Get the corresponding Pointers
			self['InteractingPointersList']=map(lambda IntsList:getattr(self.CouplerPointer,self.CouplerPointer.CoupledStructureKeyString)[IntsList],ArrayedIntsListsList)
			"""

	#</DefineBindingHookMethods>
#</DefineClass>

