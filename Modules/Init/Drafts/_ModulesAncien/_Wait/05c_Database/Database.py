#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class DatabaseClass(SYS.TableClass):

	#<DefineHookMethods>
	def initAfterBasesWithDatabase(self):
		
		#<DefineSpecificDict>
		self.DatabasedTablesTypeStringsList=[]
		self["DemandedBaseTypeString"]="Table"
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"DatabasedTablesTypeStringsList"
												]

	def getBeforeBasesWithDatabase(self,_KeyVariable):
		if type(_KeyVariable) in [list,tuple]:
			if type(_KeyVariable)==tuple:
				_KeyVariable=list(_KeyVariable)
			if len(_KeyVariable)==0:
				return getattr(self,self.ListKeyString)
			elif len(_KeyVariable)==1:
				return self[_KeyVariable[0]]
			elif len(_KeyVariable)==2:
				GettedVariable=self[_KeyVariable[0]]
				if GettedVariable!=None:
					return GettedVariable[_KeyVariable[1]]
			else:
				GettedVariable=self[_KeyVariable[0]]
				if GettedVariable!=None:
					return GettedVariable[_KeyVariable[1:]]
	
	def reprAfterBasesWithDatabase(self,_SelfString,_PrintedDict):

		#Define the Filtering KeyStrings
		KeyStringsList=[
							"DatabasedTypeString"
						]
		
		#Set the NewPrintedDict
		_PrintedDict.update(dict(filter(lambda Item:Item!=None,map(lambda KeyString:(KeyString,self.__dict__[KeyString]) if KeyString in self.__dict__ else None,KeyStringsList))))

		#Define the new SelfString
		NewSelfString=SYS.ConsolePrinter.getPrintedVariableString(_PrintedDict)

		#Return 
		return NewSelfString,_PrintedDict

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setDatabasedTablesTypeStringsListAfterBasesWithDatabase(self):

		#Bind with ListedVariablesInt Setting
		if len(getattr(self,self.ListKeyString))!=len(self.DatabasedTablesTypeStringsList):
			self['ListedVariablesInt']=len(self.DatabasedTablesTypeStringsList)

		#Bind with DemandedBaseTypeString Setting in each DatabasedTable
		map(lambda DatabasedTable,DatabasedTablesTypeString:DatabasedTable.__setitem__("DemandedBaseTypeString",DatabasedTablesTypeString) if hasattr(DatabasedTable,"__setitem__") else None,getattr(self,self.ListKeyString),self.DatabasedTablesTypeStringsList)
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>
#</DefineClass>