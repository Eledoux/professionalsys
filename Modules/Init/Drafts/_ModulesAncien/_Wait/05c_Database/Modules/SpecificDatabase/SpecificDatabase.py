#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class SpecificDatabaseClass(SYS.DatabaseClass):

	#<DefineHookMethods>
	def initAfterBasesWithSpecificDatabase(self):

		#<DefineSpecificDict>
		self.DatabasedTypeString="Element"
		self.DatabasedTablesListedVariablesIntsList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"DatabasedTypeString",
													"DatabasedTablesListedVariablesIntsList"
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setListedTypeStringAfterBasesWithSpecificDatabase(self):

		#Freeze the ListedTypeString with the one of the first layer of Table
		Variable=getattr(SYS,SYS.getClassStringWithTypeString(self.ListedTypeString))()
		if hasattr(Variable,"ListedTypeString"):
			self['DatabasedTypeString']=Variable.ListedTypeString

	def setDatabasedTypeStringAfterBasesWithSpecificDatabase(self):
		self.setDatabasedTablesTypeStringsList()

	def setDatabasedTablesIntAfterBasesWithSpecificDatabase(self):
		self.setDatabasedTablesTypeStringsList()

	def setDatabasedTablesListedVariablesIntsListAfterBasesWithSpecificDatabase(self):
		
		#Prepare the number of Table
		if len(self.DatabasedTablesListedVariablesIntsList)!=self.ListedVariablesInt:
			self['ListedVariablesInt']=len(self.DatabasedTablesListedVariablesIntsList)

		#Update the number of ListedVariables in each Table
		map(lambda Table,ListedVariablesInt: Table.update({'DemandedBaseTypeString':self.DatabasedTypeString}).update({'ListedVariablesInt':ListedVariablesInt}),getattr(self,self.ListKeyString),self.DatabasedTablesListedVariablesIntsList)
	#</DefineBindingHookMethods>

	#<DefineMethods>
	def setDatabasedTablesTypeStringsList(self):
		self['DatabasedTablesTypeStringsList']=map(lambda Int:self.DatabasedTypeString,xrange(self.ListedVariablesInt))

	#</DefineMethods>
#<DefineClass>


