#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class AddingDatabaseClass(SYS.DatabaseClass):

	#<DefineHookMethods>									
	def addBeforeBasesWithAddingDatabase(self,_AddedVariable):

		#Special add for list structure like [ <something to add like in a Table>  ]
		if type(_AddedVariable)==list:

			if len(_AddedVariable)==1:

				if type(_AddedVariable[0])==list:

					#Prepare the good number of DatabasedTables
					if self.ListedVariablesInt<len(_AddedVariable[0]):
						self['ListedVariablesInt']=len(_AddedVariable[0])

					#Add into each Table
					map(lambda DatabasedTable,ListedAddedVariable:DatabasedTable.__add__(ListedAddedVariable),getattr(self,self.ListKeyString),_AddedVariable[0])

					#Set the IsAddingBool to False
					#self.IsAddingBool=False

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

