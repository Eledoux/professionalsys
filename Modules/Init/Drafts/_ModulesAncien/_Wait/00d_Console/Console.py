######################################################################
#Import Modules
import ShareYourSystem as SYS

######################################################################
#Define Settings in the SYS framework

#Set the PrintingElementString
SYS.Module.AddDictElementString="_   "
SYS.Module.ReprElementString="   /"

#Set the ConsolePrinter
SYS.Module.ConsolePrinter=SYS.PrinterClass()

#Bind a "print" Attribute to the ConsolePrinter
SYS.Module._print=lambda _PrintedVariable:SYS.Module.ConsolePrinter("Print",_PrintedVariable)

#Set the ConsoleDebugger
SYS.Module.ConsoleDebugger=SYS.DebuggerClass()

#Bind a "print" Attribute to the ConsolePrinter
SYS.Module.debug=lambda _DebuggedVariable:SYS.Module.ConsoleDebugger("Debug",_DebuggedVariable)
