#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>


PitchIntToNotesDict={
						"0":{
								'BatavianStringsList':['c']
							},
						"1":{
								'BatavianStringsList':['cis','des']
							},
						"2":{
								'BatavianStringsList':['d']
							},
						"3":{
								'BatavianStringsList':['dis','ees']
							},
						"4":{
								'BatavianStringsList':['e']
							},
						"5":{
								'BatavianStringsList':['f']
							},
						"6":{
								'BatavianStringsList':['fis','fes']
							},
						"7":{
								'BatavianStringsList':['g']
							},
						"8":{
								'BatavianStringsList':['gis','ges']
							},
						"9":{
								'BatavianStringsList':['a'],
								"ReferedPitchInt":69
							},
						"10":{
								'BatavianStringsList':['ais','bes']
							},
						"11":{
								'BatavianStringsList':['b']
							}	
					}


#<DefineClass>
class NoteClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithNote(self):

		#<DefineSpecificDict>
		self.NoteString=""
		self.PitchInt=0
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Note"]=[
												]
	#</DefineHookMethods>

	#<DefineBindingHookMethods>

	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>
#</DefineClass>

