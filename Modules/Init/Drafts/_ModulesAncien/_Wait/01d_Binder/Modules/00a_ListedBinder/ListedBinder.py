
#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class ListedBinderClass(
							SYS.BinderClass,
							SYS.ListedObjectClass
				):
	#<DefineHookMethods>
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setListingListerPointersListAfterBasesWithListedBinder(self):

		#Bind with InstancingPointer setting
		try:	
			self['InstancingPointer']=self.ListingListerPointersList[1]
		except:
			return
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

