
#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineClass>
class BinderClass(
						#sSYS.StrictInstancerClass,
						SYS.TyperClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithBinder(self):
		pass
	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	def setDerivedTypeStringAfterBasesWithBinder(self):
		
		#Bind with InstancedTypeString setting
		self['InstancedTypeString']=self.DerivedTypeString
	#</DefineBindingHookMethods>
	
	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>

