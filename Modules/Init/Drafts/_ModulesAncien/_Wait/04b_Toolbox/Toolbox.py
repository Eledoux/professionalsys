#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class ToolboxClass(
						SYS.ListedBindersListedListerClass
				):

	#<DefineHookMethods>
	def initAfterBasesWithToolbox(self):

		#<DefineSpecificDict>
		self.AgentPointer=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Toolbox"]=[
											"AgentPointer"
												]

	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	def setAgentPointerAfterBasesWithToolbox(self):

		#Bind with ListingListerPointersList setting
		self["ListingListerPointersList"]=[self.AgentPointer]

	#<DefineBindingHookMethods>		
#</DefineClass>