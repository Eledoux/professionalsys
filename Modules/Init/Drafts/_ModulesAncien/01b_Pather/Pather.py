#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Path"
DoingString="Pathing"
DoneString="Pathed"
DoingTagString="<Pathing>"
DoneTagString="<Pathed>"
PathString="/"
#</DefineLocals>


#<DefineClass>
class PatherClass(
						SYS.ObjectClass
				):

	#<Define HookMethods>
	def getBeforeWithPather(self,_GettingVariable):

		if SYS.getCommonPrefixStringWithStringsList([_GettingVariable,DoneTagString])==DoneTagString:

			#Get the _KeyStringsList
			GettingVariableStringsList=_GettingVariable.split(DoneTagString)[1].split(PathString)

			GettedVariable=self[GettingVariableStringsList[0]]
			if GettedVariable!=None:

				#Define the ChildPathString
				ChildPathString=PathString.join(GettingVariableStringsList[1:])	

				#Direct update in the Child or go deeper with the ChildPathString
				if ChildPathString=="":
					return self[GettingVariableStringsList[0]]
				else:
					#Set in the first deeper Element with the ChildPathString
					return self[GettingVariableStringsList[0]][ChildPathString]

				#Stop the Setting at this level
				self.IsGettingBool=False

	def setBeforeWithPather(self,_SettingVariable,_SettedVariable):

		if SYS.getCommonPrefixStringWithStringsList([_SettingVariable,DoneTagString])==DoneTagString:

			#Get the _KeyStringsList
			SettingVariableStringsList=_SettingVariable.split(DoneTagString)[1].split(PathString)

			GettedVariable=self[SettingVariableStringsList[0]]
			if GettedVariable!=None:
				
				#Define the ChildPathString
				ChildPathString=PathString.join(SettingVariableStringsList[1:])	
				
				#Direct update in the Child or go deeper with the ChildPathString
				if ChildPathString=="":
					if type(_SettedVariable)==list():
						self[SettingVariableStringsList[0]].update(*_SettedVariable)
					elif hasattr(_SettedVariable,"items"):
						self[SettingVariableStringsList[0]].update(**_SettedVariable)
				else:
					#Set in the first deeper Element with the ChildPathString
					self[SettingVariableStringsList[0]][ChildPathString]=_SettedVariable

				#Stop the Setting at this level
				self.IsSettingBool=False

	#</Define HookMethods>

	#<Define Methods>
	#</Define HookMethods>

#</DefineClass>

