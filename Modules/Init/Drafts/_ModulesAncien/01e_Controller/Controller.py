#<ImportModules>
import ShareYourSystem as SYS
#</ImportModules>

#<DefineLocals>
DoString="Control"
DoingString="Controlling"
DoneString="Controlled"
DoneTagString="<Controlled>"
#<DefineClass>

class ControllerClass(
					SYS.UserClass
				):

	#<DefineHookMethods>
	def initAfterWithController(self):

		#<DefineSpecificDict>
		self.GettingStringToControlledVariableDict={}
		self.ControllingTyper=SYS.TyperClass().update(**{'TypingTypeString':"User"})
		self("Use",*[
						('<Pathed>UsingInstancer',{
											'InstancingTypeString':"Itemizer"
										})
					])
		#</DefineSpecificDict>

	def callAfterWithController(self,_CallString,*_ArgsList,**_KwargsDict):

		if _CallString==DoString:

			#Prepare Variables
			PrefixString=_KwargsDict['PrefixString'] if "PrefixString" in _KwargsDict else ""

			if "ControlledTypeString" in _KwargsDict:

				#Itemize a User that use a <ControlledTypeString> Variable
				self.UsedVariable("Item",**{	
						'MethodString':"append",
						'ItemizedVariable':SYS.UserClass()("Use",*[
																	('<Pathed>UsingInstancer',{
																		'InstancingTypeString':_KwargsDict['ControlledTypeString']
																	})
																]
														)
											}
									)

				#Add in the GettingStringToControlledVariableDict
				GettingString=PrefixString+str(self.UsedVariable.ItemizingInt)+"_"+_KwargsDict['ControlledTypeString']
				self.GettingStringToControlledVariableDict[GettingString]=self.UsedVariable.ItemizedVariable.UsedVariable
			

			if "ControlledTypeStringsList" in _KwargsDict:

				map(
						lambda ControlledTypeString:
						#Control for each
						self("Control",**{
											'ControlledTypeString':ControlledTypeString
										}),
						_KwargsDict['ControlledTypeStringsList']
					)

		#Return self
		return self

	def getBeforeWithController(self,_GettingVariable):

		if SYS.getCommonPrefixStringWithStringsList([_GettingVariable,DoneTagString])==DoneTagString:

			GettingString=_GettingVariable.split(DoneTagString)[1]
			try:
				return self.GettingStringToControlledVariableDict[GettingString]
			except KeyError:
				print("Controller has no ControlledObject like "+GettingString)

	#</DefineHookMethods>

	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	

	#</DefineMethods>

#</DefineClass>
