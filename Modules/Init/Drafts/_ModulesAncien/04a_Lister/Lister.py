#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class ListerClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithLister(self):

		#<DefineSpecificDict>
		self.ListedKeyStringsList=[]
		self.ListedVariablesList=[]
		self.TempListedVariable=None
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["Lister"]=[
												]

	def callAfterBasesWithLister(self,*_ArgsList,**_KwargsDict):

		#Check that the _KwargsDict is ok
		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="List":

				#Single Element Case
				if 'ListedVariable' in _KwargsDict:

					#Define the KeyString
					KeyString=_KwargsDict['PrefixString'] if 'PrefixString' in _KwargsDict else ""

					if hasattr(_KwargsDict['ListedVariable'],'TypeString'):
						KeyString+=_KwargsDict['ListedVariable'].TypeString
					KeyString+=type(_KwargsDict['ListedVariable']).__name__[0].upper()+type(_KwargsDict['ListedVariable']).__name__[1:]

					#Bind with TempListedVariable setting
					self['TempListedVariable']=_KwargsDict['ListedVariable']

					#Split depending on the type of the method called
					if 'MethodString' in _KwargsDict:

						if _KwargsDict['MethodString']=='append':
							self.ListedKeyStringsList.append(KeyString)
							self.ListedVariablesList.append(self.TempListedVariable)

				#List of Element Case
				if 'ListedVariables' in _KwargsDict:
					if type(_KwargsDict['ListedVariables'])==list:

						#Get the _KwargsDict without the ListedVariables
						SingleKwargsDict=dict(
											filter(
												lambda ItemTuple:
												ItemTuple[0]!='ListedVariables',
												_KwargsDict.items()
												)
											)
						
						#Do the MethodString for each
						map(
							lambda ListedVariable:
							self(**dict(SingleKwargsDict,**{'ListedVariable':ListedVariable})),
							_KwargsDict['ListedVariables']
							)


	#</DefineHookMethods>
	
	#<DefineBindingHookMethods>
	#<DefineBindingHookMethods>	

	#<DefineMethods>
	#<DefineMethods>	

#</DefineClass>