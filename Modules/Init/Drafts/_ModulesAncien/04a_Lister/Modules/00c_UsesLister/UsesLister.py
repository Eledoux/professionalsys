#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class UsesListerClass(SYS.AddingListerClass):
	
	#<DefineHookMethods>
	def initAfterBasesWithUsesLister(self):

		#<DefineSpecificDict>
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict["UsesLister"]=[
												]

	def setTempListedVariableAfterBasesWithUsesLister(self):

		#"Usify" this dict
		self.TempListedVariable=SYS.UseClass().update(
				dict({'TypeDictString':"Use"},**self.TempListedVariable)
			)
	#<DefineBindingHookMethods>
	#</DefineBindingHookMethods>

	#<DefineMethods>
	#</DefineMethods>

#</DefineClass>