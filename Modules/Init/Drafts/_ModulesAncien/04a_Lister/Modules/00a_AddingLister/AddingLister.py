#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class AddingListerClass(SYS.ListerClass):
	
	#<DefineHookMethods>
	def addAfterBasesWithAddingLister(self,_AddedVariable):

		#Case of one single element
		if hasattr(_AddedVariable,"items"):

			#Call with the MethodString append
			self(**{
						"ListedVariable":_AddedVariable,
						'MethodString':"append",
						'TypeDictString':"List"
					}
				)

		elif type(_AddedVariable) in [list,tuple]:

			#Convert the Tuple Case
			if type(_AddedVariable)==tuple:
				_AddedVariable=list(_AddedVariable)

			#Call with the MethodString append
			self(**{
						"ListedVariables":_AddedVariable,
						'MethodString':"append",
						'TypeDictString':"List"
					}
				)
	#</DefineHookMethods>


#</DefineClass>