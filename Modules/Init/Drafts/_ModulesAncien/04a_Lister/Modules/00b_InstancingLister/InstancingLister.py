#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class InstancingListerClass(SYS.ListerClass):
	
	#<DefineHookMethods>
	def initAfterBasesWithInstancingLister(self):

		#<DefineSpecificDict>
		self.ListedInstancingTypeStringsList=[]
		#</DefineSpecificDict>

		#Update the TypeStringToKeyStringsListDict
		self.TypeStringToKeyStringsListDict[self.TypeString]=[
													"InstancedListedBaseTypeStringsList"
												]

	#<DefineBindingHookMethods>
	def callBeforeBasesWithInstancingLister(self,*_ArgsList,**_KwargsDict):

		#Check that the _KwargsDict is ok
		if 'TypeDictString' in _KwargsDict:
			if _KwargsDict['TypeDictString']=="List":

				#Single Element Case
				if 'ListedVariable' in _KwargsDict:

					pass

	#</DefineBindingHookMethods>

	#<DefineMethods>
	def getTypedVariableListWithVariablesList(self,_VariablesList):

		#For each try to type with the self.InstancedListedBaseTypeStringsList
	 	return map(lambda ListedVariable,ListedInstancedTypeString:
			SYS.getTypedVariableWithVariableAndTypeString(ListedVariable,
				ListedInstancedTypeString) 
			if ListedInstancedTypeString!=None else "",
			_VariablesList,
			self.InstancedListedBaseTypeStringsList)

	#</DefineMethods>

#</DefineClass>