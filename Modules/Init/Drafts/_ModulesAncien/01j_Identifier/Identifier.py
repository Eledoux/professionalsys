#<Import Modules>
import ShareYourSystem as SYS
#</Import Modules>

#<DefineClass>
class IdentifierClass(SYS.ObjectClass):

	#<DefineHookMethods>
	def initAfterBasesWithIdentifier(self):

		#<DefineSpecificDict>
		self.IdentifiedString=""
		#</DefineSpecificDict>
	
	#</DefineHookMethods>
#</DefineClass>

