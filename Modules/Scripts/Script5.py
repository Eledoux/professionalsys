#Adding Delay...

################
#DELAYEDNEURONSNETWORK
################

#Params Definition (and Declaration)
NeuronsInt=0
MembraneTimeFloat=20.
RunTimeFloat=100.
StepTime=0.1
VoltageFloatsArray=np.zeros((NeuronsInt,(int)(RunTimeFloat/StepTime)),dtype=float)
ConnectivityFloat=0.2
SynapticWeigthsArray=sc.stats.bernoulli.rvs(ConnectivityFloat,size=(NeuronsInt,NeuronsInt))
"""
Add the DelayTimeFloat
"""
DelayTimeFloat=1.

#Functions Definition

def setDelayedVoltageFloatsArray():

	"""
	Set the DelayTimeIndexInt
	"""
	DelayTimeIndexInt=(int)(DelayTimeFloat/StepTime)

	#Compute the first order differential equation
	"""
	Change the initial starting IndexInt
	"""
	for TimeIndexInt in xrange(DelayTimeIndexInt,(int)(RunTimeFloat/StepTime)):

		"""
		Consider the Delay
		"""
		VoltageFloatsArray[:,TimeIndexInt]= VoltageFloatsArray[:,TimeIndexInt-1] + (
											#Leak Term
											-VoltageFloatsArray[:,TimeIndexInt-1] + np.dot(SynapticWeigthsArray,VoltageFloatsArray[:,TimeIndexInt-1-DelayTimeIndexInt])
											#Interaction Term 
											) * (StepTime/MembraneTimeFloat)




#Analysis in multi dimension...

for DelayFloatTime in [0,1,10]:

	#Reinit Variables...

	#Init the VoltageFloatsArray
	VoltageFloatsArray=np.array((NeuronsInt,(int)(RunTimeFloat/StepTime)),dtype=float)

	#Set the sparse SynapticWeigthsArray
	SynapticWeigthsArray=sc.stats.bernoulli.rvs(ConnectivityFloat,(NeuronsInt,NeuronsInt))

	#Set the Simulation with the good function
	if DelayFloatTime>0:
		setDelayedVoltageFloatsArray()
	else:
		setVoltageFloatsArray()

	#Analyse
	MeanVoltageFloatsList=mean(VoltageFloatsArray)
	SDVoltageFloatsList=std(VoltageFloatsArray)

	#Record in hard structures...
	"""
	"addLines(open("Data.dat"),locals())"
	"""
