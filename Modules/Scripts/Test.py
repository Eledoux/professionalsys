
import ShareYourSystem as SYS

"""
from webalchemy import config
from webalchemy.mvc import controller

class Item:
    def __init__(self, text):
        self.completed = False
        self.text = text

class DataModel:
    def __init__(self):
        # loading local list of todos
        self.itemlist = JSON.parse(localStorage.getItem('webatodomvcdata')) or []

    def set_all_completed(self, comp_or_not):
        for item in self.itemlist:
            item.completed = comp_or_not
        self.persist()

    def remove_completed(self):
        self.itemlist = self.itemlist.filter(lambda i:  not i.completed)
        self.persist()

    def remove_item(self, i):
        self.itemlist.splice(i, 1)
        self.persist()

    def add_item(self, txt):
        self.itemlist.push(new(Item, txt))
        self.persist()

    def toggle_item_completed(self, i, v):
        self.itemlist[i].completed = v
        self.persist()

    def persist(self):
        localStorage.setItem('webatodomvcdata', JSON.stringify(self.itemlist))

    def calc_completed_and_remaining(self):
        self.completed = 0
        for item in self.itemlist:
            if item.completed:
                self.completed += 1
        self.remaining = self.itemlist.length - self.completed


class ViewModel:
    def __init__(self):
        self.itembeingedited = None

    def new_item_keyup(self, e):
        if e.keyCode == weba.KeyCode.ESC: e.target.blur()
        if e.keyCode == weba.KeyCode.ENTER:
            if e.target.value.trim() != '':
                e.target.app.m.add_item(e.target.value)
                e.target.value = ''

    def edit_keyup(self, e, i):
        if e.keyCode == weba.KeyCode.ESC:
            self.itembeingedited = None
        if e.keyCode != weba.KeyCode.ENTER: return
        self.itembeingedited = None
        e.target.app.m.itemlist[i].text = e.target.value
        if e.target.value.trim() == '':
            e.target.app.m.remove_item(i)

    def editing_item_changed(self, e, i, tothisitem):
        if tothisitem:
            e.focus()
            e.value = e.app.m.itemlist[i].text
        else:
            e.blur()

    def should_hide(self, e, i):
        return (e.app.m.itemlist[i].completed and location.hash == '#/active') or \
           (not e.app.m.itemlist[i].completed and location.hash == '#/completed')

    def finish_editing(self, i):
        if self.itembeingedited == i:
            self.itembeingedited = None

class Settings:
    FREEZE_OUTPUT = 'todomvc.html'

class AppTodoMvc:

    main_html_file_path = 'static/template/index.html'
    config = config.from_object(Settings)

    def initialize(self, **kwargs):
        self.rdoc = kwargs['remote_document']
        self.datamodel = self.rdoc.new(DataModel)
        self.viewmodel = self.rdoc.new(ViewModel)
        self.rdoc.translate(Item)

        controller(self.rdoc, 
            kwargs['main_html'], 
            m=self.datamodel, 
            vm=self.viewmodel,
            prerender=self.datamodel.calc_completed_and_remaining
            )

from webalchemy import server
if __name__ == '__main__':
	server.run(AppTodoMvc)
"""
#from webalchemy import server
#from examples.todomvc.todomvc import AppTodoMvc as app
#server.run(app)

'''
class Decorator(object):
    def __init__(self,*Args,**Kwargs):
        print(Args,Kwargs)
        self.DecoratedFunction=Args[0]
        self.__dict__.update(Kwargs)

    def __call__(self, *args, **kwargs):
        getattr(self,self.String)();return self.DecoratedFunction.__call__(*args, **kwargs)

    def printAfter(self):
        print('hello')




#a=A.a

class Representer(object):

    @Decorator
    def printAfter(self,*args,**kwargs):
        print('tamere')


#A().a()
B().b(())

'''
import copy
#print("eee".startswith('o'))
#print("eee".endswith('pp'))

"""
def setHookedMethodWithClassAndParentClassAndAttributeTuple(_Class,_ParentClass,_AttributeTuple):

    #Unpack
    __KeyString,__ValueVariable=_AttributeTuple

    #Keep only the non built ins methods
    if callable(__ValueVariable) and __KeyString.startswith('__')==False:

        #Split into the MethodString, the HookString and the TypeString
        WordStringsList=SYS.getWordStringsListWithString(__KeyString)
        FilteredList=filter(
                                lambda __Variable:
                                __Variable!=None,
                                map(
                                    lambda __HookString:
                                    __HookString 
                                    if __HookString in WordStringsList
                                    else None,
                                    ['Before','After']
                                )
                            )

        #If there is just one corresponding hook
        if len(FilteredList)==1:

            #Define
            HookString=FilteredList[0]
            IndexInt=WordStringsList.index(HookString)
            MethodString=HookString.join(WordStringsList[:IndexInt])
            TypeString=HookString.join(WordStringsList[IndexInt+1:])

            #Debug
            print(MethodString,HookString,TypeString)

            #If the ParentClass has also this method then hook
            if hasattr(_ParentClass,MethodString):

                #Define
                Method=copy.copy(__ValueVariable)
                ParentMethod=getattr(_ParentClass,MethodString)
                if HookString=='Before':
                    def hookMethod(_Instance):
                        Method(_Instance)
                        ParentMethod(_Instance)
                        return _Instance
                else:
                    def hookMethod(_Instance):
                        ParentMethod(_Instance)
                        Method(_Instance)
                        return _Instance
                #Set
                setattr(_Class,MethodString,hookMethod)


def getHookedClass(_Class):

    #Set
    if len(_Class.__bases__)>0: 
        map(
                lambda __AttributeTuple:
                setHookedMethodWithClassAndParentClassAndAttributeTuple(
                        _Class,
                        _Class.__bases__[0],
                        __AttributeTuple
                ),
                _Class.__dict__.items()
            )

    #Return
    return _Class
            

class InitiaterClass(object):

    def init(self):
        print('Initiater')
        self.a=1

@getHookedClass
class BuilderClass(InitiaterClass):
    
    def initAfterInitiater(self):
        print('Builder')
        self.b=1


@getHookedClass
class MakerClass(BuilderClass):

    def initBeforeBuilder(self):
        print('Maker')
        self.c=1

"""

HookBeforeString='Before'
HookAfterString='After'

def setHookedMethodWithClassAndAttributeTuple(_Class,_AttributeTuple):

    #Unpack
    __KeyString,__ValueVariable=_AttributeTuple

    #Keep only the non built ins methods
    if callable(__ValueVariable) and __KeyString.startswith('__')==False:

        #Debug
        print(__KeyString)

        #Init a MethodString
        MethodString=""

        #Split into the MethodString, the HookString and the TypeString
        BeforeTypeString=""
        AfterTypeString=""
        BeforeStringsList=__KeyString.split(HookBeforeString)
        if len(BeforeStringsList)>1:
            MethodString=HookBeforeString.join(BeforeStringsList[:-1])
            BeforeString=HookBeforeString.join(BeforeStringsList[1:])
            AfterStringsList=BeforeString.split(HookAfterString)
        else:
            AfterStringsList=__KeyString.split(HookAfterString)
            MethodString=HookAfterString.join(AfterStringsList[:-1])

        print(AfterStringsList)
        if len(AfterStringsList)>1:
            AfterTypeString=HookAfterString.join(AfterStringsList[1:])
            MethodOrBeforeTypeString=HookAfterString.join(AfterStringsList[:-1])
            if MethodOrBeforeTypeString!=MethodString:
                BeforeTypeString=MethodOrBeforeTypeString
        else:
           BeforeTypeString=HookBeforeString.join(BeforeString)
        
        #Debug
        print(MethodString,BeforeTypeString,AfterTypeString)

        BeforeMethod=None
        BeforeClass=getattr(SYS,SYS.getClassStringWithTypeString(BeforeTypeString))
        if BeforeClass!=None and BeforeClass in _Class.__bases__:
            if hasattr(BeforeClass,MethodString):
                BeforeMethod=getattr(BeforeClass,MethodString)
        AfterMethod=None     
        AfterClass=getattr(SYS,SYS.getClassStringWithTypeString(BeforeTypeString))
        if AfterClass!=None and AfterClass in _Class.__bases__:
            if hasattr(AfterClass,MethodString):
                AfterMethod=getattr(AfterClass,MethodString)  
                 

        #Debug
        print(BeforeMethod,AfterMethod)

        #If there is just one corresponding hook
        '''
        if len(FilteredList)==1:

            #Define
            HookString=FilteredList[0]
            IndexInt=WordStringsList.index(HookString)
            MethodString=HookString.join(WordStringsList[:IndexInt])
            TypeString=HookString.join(WordStringsList[IndexInt+1:])

            #Debug
            print(MethodString,HookString,TypeString)

            #If the ParentClass has also this method then hook
            if hasattr(_ParentClass,MethodString):

                #Define
                Method=copy.copy(__ValueVariable)
                ParentMethod=getattr(_ParentClass,MethodString)
                if HookString=='Before':
                    def hookMethod(_Instance):
                        Method(_Instance)
                        ParentMethod(_Instance)
                        return _Instance
                else:
                    def hookMethod(_Instance):
                        ParentMethod(_Instance)
                        Method(_Instance)
                        return _Instance
                #Set
                setattr(_Class,MethodString,hookMethod)
        '''

def getHookedClass(_Class):

    #Set
    if len(_Class.__bases__)>0: 
        map(
                lambda __AttributeTuple:
                setHookedMethodWithClassAndAttributeTuple(
                        _Class,
                        __AttributeTuple
                ),
                _Class.__dict__.items()
            )

    #Return
    return _Class


class Decorator(object):

    def __init__(self,_Class):
        self.DecoratingClass=_Class
        self.DecoratingMethodString=""
         
    def hook(self):

        #Split into the MethodString, the HookString and the TypeString
        BeforeTypeString=""
        AfterTypeString=""
        BeforeStringsList=__KeyString.split(HookBeforeString)
        if len(BeforeStringsList)>1:
            MethodString=HookBeforeString.join(BeforeStringsList[:-1])
            BeforeString=HookBeforeString.join(BeforeStringsList[1:])
            AfterStringsList=BeforeString.split(HookAfterString)
        else:
            AfterStringsList=__KeyString.split(HookAfterString)
            MethodString=HookAfterString.join(AfterStringsList[:-1])

        print(AfterStringsList)
        if len(AfterStringsList)>1:
            AfterTypeString=HookAfterString.join(AfterStringsList[1:])
            MethodOrBeforeTypeString=HookAfterString.join(AfterStringsList[:-1])
            if MethodOrBeforeTypeString!=MethodString:
                BeforeTypeString=MethodOrBeforeTypeString
        else:
           BeforeTypeString=HookBeforeString.join(BeforeString)
        
        #Debug
        print(MethodString,BeforeTypeString,AfterTypeString)

        BeforeMethod=None
        BeforeClass=getattr(SYS,SYS.getClassStringWithTypeString(BeforeTypeString))
        if BeforeClass!=None and BeforeClass in _Class.__bases__:
            if hasattr(BeforeClass,MethodString):
                BeforeMethod=getattr(BeforeClass,MethodString)
        AfterMethod=None     
        AfterClass=getattr(SYS,SYS.getClassStringWithTypeString(BeforeTypeString))
        if AfterClass!=None and AfterClass in _Class.__bases__:
            if hasattr(AfterClass,MethodString):
                AfterMethod=getattr(AfterClass,MethodString)  
                 

        #Debug
        print(BeforeMethod,AfterMethod)

        
'''
class HookerClass(DecoraterClass):

    def __init__(self):


    def initAfter(self):

        self.HookedBaseTypeStringsList=map(
                                                lambda __BaseClass:
                                                SYS.getClassStringWithTypeString(__BaseClass.__name__),
                                                self.DecoratingClass.__bases
                                            )
        self.HookedMethodStrings=map(
                                        lambda __AttributeTuple:
                                        __AttributeTuple[0],
                                        filter(
                                                lambda __AttributeTuple:
                                                callable(__AttributeTuple[1]
                                                    ) and 'Before' in __AttributeTuple[0] or 'After' in __AttributeTuple[0],
                                                self.__class__.iteritems()
                                         )
                                    )
'''



class InitiaterClass(object):

    def init(self):
        print('Initiater')
        self.a=1

@getHookedClass
class BuilderClass(InitiaterClass):
    
    def initAfterInitiater(self):
        print('Builder')
        self.b=1


@getHookedClass
class MakerClass(BuilderClass):

    def initBeforeBuilderAfterInitiater(self):
        print('Maker')
        self.c=1
  

print(MakerClass().init().__dict__)



