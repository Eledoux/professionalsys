import os
import numpy as np
import scipy as sc
import scipy.stats
import matplotlib.pyplot
import multiprocessing
import time
import ShareYourSystem as SYS

class RateModelClass():

	def __init__(self,_UnitsInt):

		#Params Definition (and Declaration)
		self.UnitsInt=_UnitsInt
		self.ConstantTimeFloat=20.
		self.RestRateFloat=0.
		self.StartRateFloat=10.
		self.RunTimeFloat=10.
		self.StepTime=0.1
		self.ConnectivityFloat=0.2
		self.WeightFloat=1.

		#Variables Declaration
		self.RateFloatsArray=np.zeros((self.UnitsInt,(int)(self.RunTimeFloat/self.StepTime)),dtype=float)
		self.RateFloatsArray[:,0]=self.StartRateFloat*sc.stats.uniform.rvs(size=self.UnitsInt)
		self.WeigthsArray=1.*sc.stats.bernoulli.rvs(self.ConnectivityFloat,size=(self.UnitsInt,self.UnitsInt))
		self.AnalysisDict={}


	#Methods Definition

	def initSimulation(self):

		#Init the RateFloatsArray
		self.RateFloatsArray=np.zeros((self.UnitsInt,(int)(self.RunTimeFloat/self.StepTime)),dtype=float)
		self.RateFloatsArray[:,0]=self.StartRateFloat*sc.stats.uniform.rvs(size=self.UnitsInt)

		#Set the SynapticWeigthsArray
		self.WeigthsArray=self.WeightFloat*sc.stats.bernoulli.rvs(self.ConnectivityFloat,size=(self.UnitsInt,self.UnitsInt))

		#Init the AnalysisDict
		self.AnalysisDict={}

		#Return self
		return self

	def runSimulation(self):

		#Compute the first order differential equation
		for TimeIndexInt in xrange(1,(int)(self.RunTimeFloat/self.StepTime)):

			self.RateFloatsArray[:,TimeIndexInt]= self.RateFloatsArray[:,TimeIndexInt-1] + (
												#Leak Term
												-self.RateFloatsArray[:,TimeIndexInt-1] + np.tanh(np.dot(
														#Interaction Term
														self.WeigthsArray,self.RateFloatsArray[:,TimeIndexInt-1]))
												) * (self.StepTime/self.ConstantTimeFloat)

		#Return self
		return self

	def plotSimulation(self):
		Figure,Axe=matplotlib.pyplot.subplots()
		Axe.plot(self.StepTime*np.array(range((int)(self.RunTimeFloat/self.StepTime))),self.RateFloatsArray.T)
		Axe.set_xlabel("$t\ (ms)$")
		Axe.set_ylabel("$r_{i}(t)\ ()$")
		Axe.set_title("From Object protocol")
		
		#Return self
		return self

	def recordSimulation(self):

		AnalysisDict={
						#Analysis Data
						"MeanRateFloatsList":np.mean(self.RateFloatsArray),
						"STDRateFloatsList":np.std(self.RateFloatsArray),
						#Corresponding Parameters
						"UnitsInt":self.UnitsInt,
						"ConstantTimeFloat":self.ConstantTimeFloat,
						"RunTimeFloat":self.RunTimeFloat,
						"StepTime":self.StepTime,
						"ConnectivityFloat":self.ConnectivityFloat
		}
		#Or quicker...
		AnalysisDict=dict({
						#Analysis Data
						"MeanRateFloatsList":np.mean(self.RateFloatsArray),
						"STDRateFloatsList":np.std(self.RateFloatsArray)
					},**self.__dict__)
						#"UnitsInt":self.UnitsInt,
						#"ConstantTimeFloat":self.ConstantTimeFloat,
						#"RunTimeFloat":self.RunTimeFloat,
						#"StepTime":self.StepTime,
						#"ConnectivityFloat":self.ConnectivityFloat


		"""
		#################################
		#ENCAPSULATION AVOIDS THE INFINITE HAND WRITING TASK FOR THE SERIALIZATION PROBLEM...
		"""

		#Return self
		return self

	def writeSimulation(self):
		#addData(open("Data.dat"),self.AnalysisDict)
		
		#Return self
		return self

"""
#################################
# (C) PARALLELIZE PROCESSES... 
"""

UnitIntsList=xrange(1,1000)
def doProtocolWithUnitsInt(_UnitsInt):
	RateModelClass(_UnitsInt).initSimulation().runSimulation().recordSimulation().writeSimulation()
MyPool=multiprocessing.Pool(processes=min(100,len(UnitIntsList)))

"""
#Sequential
tic=time.time()
map(lambda UnitsInt:doProtocolWithUnitsInt(UnitsInt),UnitIntsList)
print('Sequential Calcul : '+str(time.time()-tic))
"""

#Parallel
tic=time.time()
MyPool.map(doProtocolWithUnitsInt,UnitIntsList)
print('Paralell Calcul : '+str(time.time()-tic))

#for UnitIntsList=xrange(1,1000) processes=min(99,len(UnitIntsList))
#Sequential Calcul : 0.352154016495
#Paralell Calcul : 0.107304811478

#for UnitIntsList=xrange(1,1000) processes=min(99,len(UnitIntsList))
#Sequential Calcul : 39.8414371014
#Paralell Calcul : 22.0918791294

#doProtocol(10)

