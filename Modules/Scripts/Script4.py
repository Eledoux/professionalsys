import os
import numpy as np
import scipy as sc
import scipy.stats
import matplotlib.pyplot
import multiprocessing
import time
import ShareYourSystem as SYS

class RateModelClass():

	def __init__(self):

		#Params Definition (and Declaration)
		self.UnitsInt=0.
		self.ConstantTimeFloat=20.
		self.RestRateFloat=0.
		self.StartRateFloat=10.
		self.RunTimeFloat=10.
		self.StepTime=0.1
		self.ConnectivityFloat=0.2
		self.WeightFloat=1.

		#Variables Declaration
		self.RateFloatsArray=np.zeros((self.UnitsInt,(int)(self.RunTimeFloat/self.StepTime)),dtype=float)
		self.RateFloatsArray[:,0]=self.StartRateFloat*sc.stats.uniform.rvs(size=self.UnitsInt)
		self.WeigthsArray=1.*sc.stats.bernoulli.rvs(self.ConnectivityFloat,size=(self.UnitsInt,self.UnitsInt))
		self.AnalysisDict={}


	#Methods Definition

	def initSimulation(self):

		#Init the RateFloatsArray
		self.RateFloatsArray=np.zeros((self.UnitsInt,(int)(self.RunTimeFloat/self.StepTime)),dtype=float)
		self.RateFloatsArray[:,0]=self.StartRateFloat*sc.stats.uniform.rvs(size=self.UnitsInt)

		#Set the SynapticWeigthsArray
		self.WeigthsArray=self.WeightFloat*sc.stats.bernoulli.rvs(self.ConnectivityFloat,size=(self.UnitsInt,self.UnitsInt))

		#Init the AnalysisDict
		self.AnalysisDict={}

		#Return self
		return self

	def runSimulation(self):

		#Compute the first order differential equation
		for TimeIndexInt in xrange(1,(int)(self.RunTimeFloat/self.StepTime)):

			self.RateFloatsArray[:,TimeIndexInt]= self.RateFloatsArray[:,TimeIndexInt-1] + (
												#Leak Term
												-self.RateFloatsArray[:,TimeIndexInt-1] + np.tanh(np.dot(
														#Interaction Term
														self.WeigthsArray,self.RateFloatsArray[:,TimeIndexInt-1]))
												) * (self.StepTime/self.ConstantTimeFloat)

		#Return self
		return self

	def plotSimulation(self):
		Figure,Axe=matplotlib.pyplot.subplots()
		Axe.plot(self.StepTime*np.array(range((int)(self.RunTimeFloat/self.StepTime))),self.RateFloatsArray.T)
		Axe.set_xlabel("$t\ (ms)$")
		Axe.set_ylabel("$r_{i}(t)\ ()$")
		Axe.set_title("From Object protocol")
		
		#Return self
		return self

	def recordSimulation(self):

		AnalysisDict={
						#Analysis Data
						"MeanRateFloatsList":np.mean(self.RateFloatsArray),
						"STDRateFloatsList":np.std(self.RateFloatsArray),
						#Corresponding Parameters
						"UnitsInt":self.UnitsInt,
						"ConstantTimeFloat":self.ConstantTimeFloat,
						"RunTimeFloat":self.RunTimeFloat,
						"StepTime":self.StepTime,
						"ConnectivityFloat":self.ConnectivityFloat
		}
		#Or quicker...
		AnalysisDict=dict({
						#Analysis Data
						"MeanRateFloatsList":np.mean(self.RateFloatsArray),
						"STDRateFloatsList":np.std(self.RateFloatsArray)
					},**self.__dict__)
						#"UnitsInt":self.UnitsInt,
						#"ConstantTimeFloat":self.ConstantTimeFloat,
						#"RunTimeFloat":self.RunTimeFloat,
						#"StepTime":self.StepTime,
						#"ConnectivityFloat":self.ConnectivityFloat


		"""
		#################################
		#ENCAPSULATION AVOIDS THE INFINITE HAND WRITING TASK FOR THE SERIALIZATION PROBLEM...
		"""

		#Return self
		return self

	def writeSimulation(self):
		#addData(open("Data.dat"),self.AnalysisDict)
		
		#Return self
		return self

	"""
	#################################
	#(D) SHAPE IN A CUSTOM MANNER THE OBJECT
	"""
	#Special setitem method...
	def __setitem__(self,_KeyVariable,_ValueVariable):

		#Put it in the __dict__
		self.__dict__[_KeyVariable]=_ValueVariable

		#Return self
		return self

	def updateWithTuplesList(_TuplesList):

		#Call the setitem
		map(lambda Tuple:self.__setitem__(Tuple[0],Tuple[1]),_TuplesList)

		#Return self
		return self


"""
#################################
#(E) EASY MULTI PARAMETER SETTING
"""

UnitIntsList=xrange(1,1000)
RestFloatsList=np.arange(0.,1.,0.1)

#In the hard manner....
#def doProtocolWithUnitIntAndRestFloat(_UnitInt,_RestFloat):
#	RateModelClass(_UnitInt,_RestFloat).initSimulation().runSimulation().recordSimulation().writeSimulation()
#Meaning redefining the RateModel Class....

def doProtocolWithUnitIntAndRestFloat(_UnitInt,_RestFloat):
	RateModelClass.__setitem__('UnitInt',_UnitInt).__setitem__('RestFloat',_RestFloat).initSimulation().runSimulation().recordSimulation().writeSimulation()

#Better is using a Generic approach...
def doProtocolWithUpdatingTuplesList(_UpdatingTuplesList):
	RateModelClass.update(UpdatingTuplesList).initSimulation().runSimulation().recordSimulation().writeSimulation()

UpdatingDict={
				'UnitIntsList':xrange(1,10),
				'RestFloatsList':np.arange(0.,1.,0.1)
			}

def getUpdatingTuplesListWithUpdatingDict(_UpdatingDict):
	import itertools
	ProductParamsTuplesList=list(itertools.product(*_UpdatingDict.values()))
	#print(ProductParamsTuplesList)
	UpdatingTuplesList=map(
							lambda ProductParamsTuple:
							zip(ParamsDict.keys(),ProductParamsTuple),
							ProductParamsTuplesList
							)
	#print(UpdatingTuplesList)
	return UpdatingTuplesList

#Do the protocol
map(lambda UpdatingTuplesList:
	doProtocolWithUpdatingTuplesList(UpdatingTuplesList),
	getUpdatingTuplesListWithUpdatingDict(_UpdatingDict))




