################
################
###RATE MODEL###
################
################

################
#MODULE IMPORT
################

import h5py
import os
import numpy as np
import scipy as sc
import scipy.stats
import matplotlib.pyplot
import time
import ShareYourSystem as SYS

#Kill other python processes
try :
	processing.map(lambda IdString:os.popen("kill "+IdString),SYS.getOpenedProcessIdStringsListWithProcessNameString("python")[:-1])
except:
	pass

################
#DEFINITIONS 
################

################
#VARIABLES 

#Ids Definition
AuthorString="Erwan Ledoux"
ModelString="Ratemodel"

#Params Definition (and Declaration) (MetaData)
UnitsInt=100
ConstantTimeFloat=1.
StartRatesFloat=10.
RestRateFloat=0.
RunTimeFloat=10.
StepTimeFloat=0.1
WeightFloat=1.5
NonLinearSlopeFloat=1.

#Variables Definition (... almost Data)
RateFloatsArray=np.zeros((UnitsInt,(int)(RunTimeFloat/StepTimeFloat)),dtype=float);RateFloatsArray[:,0]=(-StartRatesFloat/2.)+StartRatesFloat*sc.stats.uniform.rvs(size=UnitsInt)
WeigthsArray=WeightFloat*np.array(sc.stats.norm.rvs(0.,size=(UnitsInt,UnitsInt)))

#MonitoredVariablesParams
MonitoredRateIntsList=[0,1]

#Grid Parameters search Protocol
#http://scikit-learn.org/stable/modules/generated/sklearn.grid_search.ParameterGrid.html
#http://pypet.readthedocs.org/en/latest/tutorial.html
#Do your own recipe...
DataDict={}
ScanDict={
			"UnitsIntsList":[100],
			"ConstantTimeFloatsList":[1.],
			"NonLinearSlopeFloatsList":np.arange(0.5,1.6,0.5),
			"WeightFloatsList":np.arange(0.,1.1,0.5)
		}

ScannedTuplesListsList=map(lambda __Tuple:
	zip(
		map(
				lambda __KeyString:
				SYS.getSingularStringWithPluralString(
					'List'.join(__KeyString.split('List')[0:-1])
					),
				ScanDict.keys()
			),
		__Tuple
		),
	SYS.getScannedTuplesListWithScanningListsList(ScanDict.values())
	)
#print("Scanned MetaData are : ");SYS._print(ScannedTuplesListsList)


#Storing Variables
SimulationString=AuthorString+" "+time.ctime(time.time())
SimulationH5pyFile=SYS.H5pyFileClass().update([
													('FileString',ModelString),
													('DirString',"/".join(__file__.split("/")[:-1])+"/")
												])

################
#FUNCTIONS

"""
#################################
# (A) VARIABLE NAMES DECOMPOSED IN PREFIXSTRING+TYPESTRING
# (B) FUNCTION/METHODS NAME RESPECTS camelCase, BETTER IF THEY HAVE EXPLICIT NAMES 
	ex: get<GettedVariableString>With<FirstInputVariableString>And<SecondInputVariableString>(
																								_<FirstInputVariableString>,
																								_<SecondInputVariableString>
																							)
		getDictedDictWithVariable(_Variable)

"""

"""
#################################
# (C) WHEN NON EXPLICIT NAMES IS NECESSARY...
# THEN TRY TO RESPECT AN EXPLICIT ASSOCIATED SPACE NAMES
# (D) WHEN THE CODE IS PROCEDURAL, MANIPULATE CAREFULLY THE globals DICT
"""
def initSimulation():

	#Init the SimulationString
	global SimulationString
	SimulationString=AuthorString+" "+time.ctime(time.time())

	#Init the RateFloatsArray
	global RateFloatsArray
	RateFloatsArray=np.zeros((UnitsInt,(int)(RunTimeFloat/StepTimeFloat)),dtype=float)
	RateFloatsArray[:,0]=StartRatesFloat*sc.stats.uniform.rvs(size=UnitsInt)

	#Set the SynapticWeigthsArray
	global WeigthsArray 
	WeigthsArray=WeightFloat*np.array(sc.stats.norm.rvs(0.,size=(UnitsInt,UnitsInt)))

	#Init the Analysis Dictionnaries
	global DataDict
	DataDict={}

def runSimulation():

	#Compute the first order differential equation
	ScaleTime=StepTimeFloat/ConstantTimeFloat
	for TimeIndexInt in xrange(1,(int)(RunTimeFloat/StepTimeFloat)):

		#Write the differential discretized version of the vectorized quation
		PreviousTimeIndexInt=TimeIndexInt-1
		RateFloatsArray[:,TimeIndexInt] = RateFloatsArray[:,PreviousTimeIndexInt] + (

		#Leak Term													#Interaction Term
		(RestRateFloat-RateFloatsArray[:,PreviousTimeIndexInt]) + np.dot(WeigthsArray,np.tanh(NonLinearSlopeFloat*RateFloatsArray[:,PreviousTimeIndexInt]))
																				
																					) * ScaleTime

def plotSimulation():
	Figure,Axe=matplotlib.pyplot.subplots()
	Axe.plot(StepTimeFloat*np.array(range((int)(RunTimeFloat/StepTimeFloat))),RateFloatsArray.T)
	Axe.set_xlabel("$t\ (ms)$")
	Axe.set_ylabel("$r_{i}(t)\ (1)$")
	Axe.set_title("From Procedure protocol")

def recordSimulation():

	#Use the global DataDict
	global DataDict

	#Record the MetaData Parameters
	DataDict.update(
						map(
								lambda ListedKeyString:
								(
									"*"+ListedKeyString,
									globals()[ListedKeyString]
								),
								map(
										lambda KeyString: 
										SYS.getSingularStringWithPluralString(KeyString.split("List")[0]),
										ScanDict.keys()
									)
							)
					)

	#Record the Data
	DataDict.update({

					"&MonitoredRateIntsList":MonitoredRateIntsList,
					"&RateFloatsArray":RateFloatsArray[MonitoredRateIntsList,:],
					"&WeigthsArray":WeigthsArray,
					"StatsDict":{
									"&MeanRateFloatsList":list(np.mean(RateFloatsArray,axis=1)),
									"&STDRateFloatsList":list(np.std(RateFloatsArray,axis=1))
								},
					'''
					"MonitoredFourierTransformsList":map(
															lambda FrequencyFloat:
															{	
																"*FrequencyFloat":FrequencyFloat,
																"MonitoredFourierComponentsDict":dict(
																map(
																	lambda IndexIntAndTuplesList:
																	("Monitored_"+str(MonitoredRateIntsList[IndexIntAndTuplesList[0]])+"Dict",dict(IndexIntAndTuplesList[1])),
							
																	enumerate(
																		map(
																				lambda MonitoredFourierComplexList:
																				[
																					("&AmplitudeFloatsList",abs(MonitoredFourierComplexList)),
																					("&PhaseFloatsList",map(lambda Complex:SYS.getArgumentFloatWithComplex(Complex),MonitoredFourierComplexList))
																				],
																				np.fft.fft(RateFloatsArray[MonitoredRateIntsList,:])
																			)
																		)
																	)
																)
															},
															np.arange(0.1,10.,5.)
															)
					'''
			})

def writeSimulation():

	#print Data
	#SYS._print(DataDict)

	#Commit
	SimulationH5pyFile.commit(SimulationString,DataDict)

	#Print the Storer
	#print(SimulationH5pyFile)
	
	#Push
	SimulationH5pyFile.push()

################
#TESTS EXECUTION
################

'''
WeightFloat=0.
initSimulation();runSimulation();plotSimulation()
WeightFloat=0.5
initSimulation();runSimulation();plotSimulation()
matplotlib.pyplot.show()

'''
################
#PROTOCOLS EXECUTION
################


#Define a Protocol function : set the locals then do the simulation protocol
def doProtocolWithParamTuplesList(_ParamTuplesList):

	#Set in locals()
	map(
			lambda ParamTuple:
			globals().__setitem__(ParamTuple[0],ParamTuple[1]),
			_ParamTuplesList
		)

	#Protocol of init run record and write
	initSimulation;runSimulation();recordSimulation();writeSimulation();

#Do all the scan
map(
		lambda TuplesList:
		doProtocolWithParamTuplesList(TuplesList)
		#None
		,	
		ScannedTuplesListsList
	)

################
#STORING CHECKING
################

#Look the Database
print("Stored h5df Database is");SYS._print(SYS.getDictedDictWithVariable(SimulationH5pyFile.get("/")))


'''
DataDictsList=SimulationH5pyFile.getWithFilterDict(
							{
							    "WeightFloat":1.5,
							    "NonLinearSlopeFloat":1.
							}
						)
print("Data founded corresponding to the query");SYS._print(DataDictsList);
'''
'''
print("First DataDict is :");SYS._print(SYS.getDictedDictWithVariable(DataDictsList[0]))
'''

def plotWithFilterDict(_FilterDict):

	#Get the DataDictsList
	DataDictsList=SimulationH5pyFile.getWithFilterDict(_FilterDict)

	#Look for the single corresponding MetaData set
	if len(DataDictsList)==1:
		DataDict=DataDictsList[0]

		#Set the MetaData in the globals
		map(
				lambda MetaDataKeyString:
				globals().__setitem__(MetaDataKeyString[1:],DataDict[MetaDataKeyString].value)
				if MetaDataKeyString[0]=="*"
				else None,
				DataDict.keys()
			)

		#Init Run Plot Simulation
		initSimulation();runSimulation();plotSimulation();matplotlib.pyplot.show()

	else:
		print("The query is not sufficiently well defined")

		print(DataDictsList)




plotWithFilterDict({
					    "WeightFloat":0.,
					    "NonLinearSlopeFloat":1.
					})



#Get one Particular DataDict
#SYS._print(SimulationH5pyFile.getDataDictWithIndexInt(0))
#SYS._print(SYS.getListingDict(SimulationH5pyFile.getDataDictWithIndexInt(0)))

"""
#################################
# (A) DANGER OF A NON EXHAUSTIVE GOOD REINITIATION
# (B) AMBIGUITIES WITH OTHER LOCAL VARIABLES
"""
