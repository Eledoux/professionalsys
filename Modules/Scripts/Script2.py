import os
import numpy as np
import scipy as sc
import scipy.stats
import matplotlib.pyplot
import ShareYourSystem as SYS

#Kill other python process
try :
	map(lambda IdString:os.popen("kill "+IdString),SYS.getOpenedProcessIdStringsListWithProcessNameString("python")[:-1])
except:
	pass

################
#RATEMODEL CLASS
################


"""
#################################
# (A) DANGER OF A NON EXHAUSTIVE GOOD REINITIATION
# (B) AMBIGUITIES WITH OTHER LOCAL VARIABLES
"""

class RateModelClass():

	def __init__(self,_UnitsInt):

		#Params Definition (and Declaration)
		self.UnitsInt=_UnitsInt
		self.ConstantTimeFloat=20.
		self.RestRateFloat=0.
		self.StartRateFloat=10.
		self.RunTimeFloat=10.
		self.StepTime=0.1
		self.ConnectivityFloat=0.2
		self.WeightFloat=1.

		#Variables Declaration
		self.RateFloatsArray=np.zeros((self.UnitsInt,(int)(self.RunTimeFloat/self.StepTime)),dtype=float)
		self.RateFloatsArray[:,0]=self.StartRateFloat*sc.stats.uniform.rvs(size=self.UnitsInt)
		self.WeigthsArray=1.*sc.stats.bernoulli.rvs(self.ConnectivityFloat,size=(self.UnitsInt,self.UnitsInt))
		self.AnalysisDict={}


	#Methods Definition

	def initSimulation(self):

		#Init the RateFloatsArray
		self.RateFloatsArray=np.zeros((self.UnitsInt,(int)(self.RunTimeFloat/self.StepTime)),dtype=float)
		self.RateFloatsArray[:,0]=self.StartRateFloat*sc.stats.uniform.rvs(size=self.UnitsInt)

		#Set the SynapticWeigthsArray
		self.WeigthsArray=self.WeightFloat*sc.stats.bernoulli.rvs(self.ConnectivityFloat,size=(self.UnitsInt,self.UnitsInt))

		#Init the AnalysisDict
		self.AnalysisDict={}

		#Return self
		return self

	def runSimulation(self):

		#Compute the first order differential equation
		for TimeIndexInt in xrange(1,(int)(self.RunTimeFloat/self.StepTime)):

			self.RateFloatsArray[:,TimeIndexInt]= self.RateFloatsArray[:,TimeIndexInt-1] + (
												#Leak Term
												-self.RateFloatsArray[:,TimeIndexInt-1] + np.tanh(np.dot(
														#Interaction Term
														self.WeigthsArray,self.RateFloatsArray[:,TimeIndexInt-1]))
												) * (self.StepTime/self.ConstantTimeFloat)

		#Return self
		return self

	def plotSimulation(self):
		Figure,Axe=matplotlib.pyplot.subplots()
		Axe.plot(self.StepTime*np.array(range((int)(self.RunTimeFloat/self.StepTime))),self.RateFloatsArray.T)
		Axe.set_xlabel("$t\ (ms)$")
		Axe.set_ylabel("$r_{i}(t)\ ()$")
		Axe.set_title("From Object protocol")
		
		#Return self
		return self

	def recordSimulation(self):

		AnalysisDict={
						#Analysis Data
						"MeanRateFloatsList":np.mean(self.RateFloatsArray),
						"STDRateFloatsList":np.std(self.RateFloatsArray),
						#Corresponding Parameters
						"UnitsInt":self.UnitsInt,
						"ConstantTimeFloat":self.ConstantTimeFloat,
						"RunTimeFloat":self.RunTimeFloat,
						"StepTime":self.StepTime,
						"ConnectivityFloat":self.ConnectivityFloat
		}
		#Or quicker...
		AnalysisDict=dict({
						#Analysis Data
						"MeanRateFloatsList":np.mean(self.RateFloatsArray),
						"STDRateFloatsList":np.std(self.RateFloatsArray)
					},**self.__dict__)
						#"UnitsInt":self.UnitsInt,
						#"ConstantTimeFloat":self.ConstantTimeFloat,
						#"RunTimeFloat":self.RunTimeFloat,
						#"StepTime":self.StepTime,
						#"ConnectivityFloat":self.ConnectivityFloat


		"""
		#################################
		#ENCAPSULATION AVOIDS THE INFINITE HAND WRITING TASK FOR THE SERIALIZATION PROBLEM...
		"""

		#Return self
		return self

	def writeSimulation(self):
		#addData(open("Data.dat"),self.AnalysisDict)
		
		#Return self
		return self

#Analysis in multi dimension with Objects
for UnitsInt in [10]:

	#Do the protocole
	RateModelClass(UnitsInt).initSimulation().runSimulation().plotSimulation().recordSimulation().writeSimulation()

#Plot show
matplotlib.pyplot.show()

