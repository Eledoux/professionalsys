##########################
##########################
###SPARSY DYNAMIC#########
##########################
##########################

################
#MODULE IMPORT
################

import h5py
import os
import numpy as np
import scipy as sc
import scipy.stats
import matplotlib.pyplot
import time
import ShareYourSystem as SYS

#Kill other python processes
try :
	map(lambda IdString:os.popen("kill "+IdString),SYS.getOpenedProcessIdStringsListWithProcessNameString("python")[:-1])
except:
	pass

################
#DEFINITIONS 
################

################
#VARIABLES 

#Ids Definition
AuthorString="Erwan Ledoux"
ModelString="SparsyDynamic"

#Params Definition (and Declaration)
UnitsInt=10
ConstantTimeFloat=10.
StartRatesFloat=10.
RestRateFloat=0.
RunTimeFloat=1.
StepTimeFloat=0.1
ConnectivityFloat=0.2
WeightFloat=1.
MonitoredRateIntsList=[0,1]

#Variables Definition
RateFloatsArray=np.zeros((UnitsInt,(int)(RunTimeFloat/StepTimeFloat)),dtype=float);RateFloatsArray[:,0]=(-StartRatesFloat/2.)+StartRatesFloat*sc.stats.uniform.rvs(size=UnitsInt)
WeigthsArray=WeightFloat*np.array(sc.stats.bernoulli.rvs(ConnectivityFloat,size=(UnitsInt,UnitsInt)))
MetaDataDict={}
DataDict={}

#Storing Variables
SimulationString=AuthorString+" "+time.ctime(time.time())
SimulationH5pyFile=SYS.H5pyFileClass().update({
													'FileString':ModelString,
													'DirString':"/".join(__file__.split("/")[:-1])+"/"
												})

################
#FUNCTIONS

def initSimulation():

	#Init the SimulationString
	SimulationString=AuthorString+" "+time.ctime(time.time())

	#Init the RateFloatsArray
	RateFloatsArray=np.zeros((UnitsInt,(int)(RunTimeFloat/StepTimeFloat)),dtype=float)
	RateFloatsArray[:,0]=StartRatesFloat*sc.stats.uniform.rvs(size=UnitsInt)

	#Set the SynapticWeigthsArray
	WeigthsArray=WeightFloat*sc.stats.bernoulli.rvs(ConnectivityFloat,size=(UnitsInt,UnitsInt))

	#Init the Analysis Dictionnaries
	MetaDataDict={}
	DataDict={}

def runSimulation():

	#Compute the first order differential equation
	ScaleTime=StepTimeFloat/ConstantTimeFloat
	for TimeIndexInt in xrange(1,(int)(RunTimeFloat/StepTimeFloat)):

		#Write the differential discretized version of the vectorized quation
		PreviousTimeIndexInt=TimeIndexInt-1
		RateFloatsArray[:,TimeIndexInt] = RateFloatsArray[:,PreviousTimeIndexInt] + (

		#Leak Term													#Interaction Term
		(RestRateFloat-RateFloatsArray[:,PreviousTimeIndexInt]) + np.tanh(np.dot(WeigthsArray,RateFloatsArray[:,PreviousTimeIndexInt]))
																				
																					) * ScaleTime

def plotSimulation():
	Figure,Axe=matplotlib.pyplot.subplots()
	Axe.plot(StepTimeFloat*np.array(range((int)(RunTimeFloat/StepTimeFloat))),RateFloatsArray.T)
	Axe.set_xlabel("$t\ (ms)$")
	Axe.set_ylabel("$r_{i}(t)\ ()$")
	Axe.set_title("From Procedure protocol")

def recordSimulation():

	global DataDict
	DataDict={

					#MetaData Parameters
					"*UnitsInt":UnitsInt,
					"*ConstantTimeFloat":ConstantTimeFloat,
					"*RunTimeFloat":RunTimeFloat,
					"*StepTimeFloat":StepTimeFloat,
					"*ConnectivityFloat":ConnectivityFloat,

					#Analysis Data
					"&MonitoredRateIntsList":MonitoredRateIntsList,
					"&RateFloatsArray":RateFloatsArray[MonitoredRateIntsList,:],
					"&WeigthsArray":WeigthsArray,
					"StatsDict":{
									"&MeanRateFloatsList":list(np.mean(RateFloatsArray,axis=1)),
									"&STDRateFloatsList":list(np.std(RateFloatsArray,axis=1))
								},
					"MonitoredFourierTransformsList":map(
															lambda FrequencyFloat:
															{	
																"*FrequencyFloat":FrequencyFloat,
																"MonitoredFourierComponentsDict":dict(
																map(
																	lambda IndexIntAndTuplesList:
																	("Monitored_"+str(MonitoredRateIntsList[IndexIntAndTuplesList[0]])+"Dict",dict(IndexIntAndTuplesList[1])),
							
																	enumerate(
																		map(
																				lambda MonitoredFourierComplexList:
																				[
																					("&AmplitudeFloatsList",abs(MonitoredFourierComplexList)),
																					("&PhaseFloatsList",map(lambda Complex:SYS.getArgumentFloatWithComplex(Complex),MonitoredFourierComplexList))
																				],
																				np.fft.fft(RateFloatsArray[MonitoredRateIntsList,:])
																			)
																		)
																	)
																)
															},
															np.arange(0.1,10.,5.)
															)
			}

def writeSimulation():

	#print Data
	#SYS._print(DataDict)

	#Commit
	SimulationH5pyFile.commit(SimulationString,DataDict)

	#Print the Storer
	#print(SimulationH5pyWrapper)
	
	#Push
	SimulationH5pyFile.push()

################
#PROTOCOLS EXECUTION
################

#Define a ScanDict

#LOOK FOR THESE BEFORE...
#http://scikit-learn.org/stable/modules/generated/sklearn.grid_search.ParameterGrid.html
#http://pypet.readthedocs.org/en/latest/tutorial.html

#Do your own recipe...
ScanDict={

			"UnitsIntsList":[10,100],
			"ConstantTimeFloatsList":[10.,20.]
		}

#Get the corresponding ScannedTuplesListsList
ScannedTuplesListsList=SYS.getScannedTuplesListsListWithScanDict(ScanDict)
#SYS._print(ScannedTuplesListsList)

#Define a Protocol function : set the locals then do the simulation protocol
def doProtocolWithParamTuplesList(_ParamTuplesList):

	#Set in locals()
	map(
			lambda ParamTuple:
			globals().__setitem__(ParamTuple[0],ParamTuple[1]),
			_ParamTuplesList
		)

	#initSimulation;runSimulation();plotSimulation();recordSimulation();writeSimulation()
	initSimulation;runSimulation();recordSimulation();writeSimulation();

#Do all the scan
map(
		lambda TuplesList:
		doProtocolWithParamTuplesList(TuplesList)
		#None
		,	
		ScannedTuplesListsList
	)

################
#STORING CHECKING
################

#Look the Database
print("Stored h5df Database is");SYS._print(SYS.getDictedDictWithVariable(SimulationH5pyFile.get("/")))

"""
SYS._print(
	SYS.getDictedDictWithVariable(
		SimulationH5pyFile.get(
									"/".join(
										[
				"/_(*ConnectivityFloat,0.2)_(*ConstantTimeFloat,10.0)_(*RunTimeFloat,1.0)_(*StepTimeFloat,0.1)_(*UnitsInt,10)",
				"MonitoredFourierTransformsDict",
										]
									)
								)
		)
	)
"""

#Plot show
#matplotlib.pyplot.show()

#Get one Particular DataDict
#SYS._print(SimulationH5pyFile.getDataDictWithIndexInt(0))
#SYS._print(SYS.getListingDict(SimulationH5pyFile.getDataDictWithIndexInt(0)))


"""
#################################
# (A) DANGER OF A NON EXHAUSTIVE GOOD REINITIATION
# (B) AMBIGUITIES WITH OTHER LOCAL VARIABLES
"""

